// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>

void export_BackboneScore();
void export_ObjectLoader();
void export_PairwiseFunction();
void export_constraint_constructor();
void export_AllAtomScore();
void export_SCWRL3EnergyFunctions();
void export_DensityScoring();

using namespace boost::python;

BOOST_PYTHON_MODULE(_scoring)
{
  export_BackboneScore(); 
  export_AllAtomScore();
  export_ObjectLoader();
  export_PairwiseFunction();
  export_constraint_constructor();
  export_SCWRL3EnergyFunctions();
  export_DensityScoring();
}
