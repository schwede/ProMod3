// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>
#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/scoring/backbone_overall_scorer.hh>
#include <promod3/scoring/cb_packing_score.hh>
#include <promod3/scoring/cbeta_score.hh>
#include <promod3/scoring/reduced_score.hh>
#include <promod3/scoring/clash_score.hh>
#include <promod3/scoring/hbond_score.hh>
#include <promod3/scoring/ss_agreement_score.hh>
#include <promod3/scoring/torsion_score.hh>
#include <promod3/scoring/pairwise_score.hh>

using namespace promod3;
using namespace promod3::scoring;
using namespace boost::python;

namespace {

  BackboneScoreEnvPtr WrapInitSeq(const ost::seq::SequenceHandle& seqres) {
    ost::seq::SequenceList seq_list = ost::seq::CreateSequenceList();
    seq_list.AddSequence(seqres);
    BackboneScoreEnvPtr env(new BackboneScoreEnv(seq_list));
    return env;
  }

  BackboneScoreEnvPtr WrapInitSeqList(const ost::seq::SequenceList& seqres) {
    BackboneScoreEnvPtr env(new BackboneScoreEnv(seqres));
    return env;
  }

  BackboneScoreEnvPtr WrapInitStr(const String& seqres) {
    ost::seq::SequenceList seq_list = ost::seq::CreateSequenceList();
    ost::seq::SequenceHandle s = ost::seq::CreateSequence("A", seqres);
    seq_list.AddSequence(s);
    BackboneScoreEnvPtr env(new BackboneScoreEnv(seq_list));
    return env;
  }

  BackboneScoreEnvPtr WrapInitStrList(const boost::python::list& seqres) {
    ost::seq::SequenceList seq_list = ost::seq::CreateSequenceList();
    for (uint i = 0; i < boost::python::len(seqres); ++i) {
      String s = boost::python::extract<String>(seqres[i]);
      ost::seq::SequenceHandle sh = ost::seq::CreateSequence("A", s);
      seq_list.AddSequence(sh);
    }
    BackboneScoreEnvPtr env(new BackboneScoreEnv(seq_list));
    return env;
  }
  
  void WrapSetEnvironmentResNum(BackboneScoreEnvPtr env,
                                const loop::BackboneList& bb_list,
                                const ost::mol::ResNum& start_resnum,
                                uint chain_index) {
    uint num = start_resnum.GetNum();
    env->SetEnvironment(bb_list, num, chain_index);
  }

  void WrapSetPsipredPrediction(BackboneScoreEnvPtr env,
                                loop::PsipredPredictionPtr pp){
    env->SetPsipredPrediction(pp);
  }
  void WrapSetPsipredPredictionList(BackboneScoreEnvPtr env,
                                    boost::python::list pp_list){
    std::vector<loop::PsipredPredictionPtr> pp_v;
    core::ConvertListToVector(pp_list, pp_v);
    env->SetPsipredPrediction(pp_v);
  }

  void WrapStashSingle(BackboneScoreEnvPtr env, uint rnum, uint num_residues,
                       uint chain_idx) {
    env->Stash(rnum, num_residues, chain_idx);
  }


  void WrapStashList(BackboneScoreEnvPtr env, const boost::python::list& rnum, 
                     const boost::python::list& num_residues,
                     const boost::python::list& chain_idx) {

    std::vector<uint> v_rnum;
    std::vector<uint> v_num_residues;
    std::vector<uint> v_chain_idx;

    core::ConvertListToVector(rnum, v_rnum);
    core::ConvertListToVector(num_residues, v_num_residues);
    core::ConvertListToVector(chain_idx, v_chain_idx);

    env->Stash(v_rnum, v_num_residues, v_chain_idx);
  }

  BackboneScorerPtr bos_getitem(BackboneOverallScorer& s, const String& key) {
    return s[key];
  }
  void bos_setitem(BackboneOverallScorer& s, const String& key,
                   BackboneScorerPtr item) {
    s[key] = item;
  }
  Real WrapCalculate(BackboneOverallScorer& s, const String& key,
                     uint start_resnum, uint num_residues, uint chain_idx) {
    return s.Calculate(key, start_resnum, num_residues, chain_idx);
  }
  Real WrapCalculateList(BackboneOverallScorer& s, const String& key,
                         const boost::python::list& start_resnum, 
                         const boost::python::list& num_residues, 
                         const boost::python::list& chain_idx) {

    std::vector<uint> v_start_resnum;
    std::vector<uint> v_num_residues;
    std::vector<uint> v_chain_idx;

    core::ConvertListToVector(start_resnum, v_start_resnum);
    core::ConvertListToVector(num_residues, v_num_residues);
    core::ConvertListToVector(chain_idx, v_chain_idx);

    return s.Calculate(key, v_start_resnum, v_num_residues, v_chain_idx);
  }
  Real WrapCalculateLinearCombination(BackboneOverallScorer& s,
                                      const boost::python::dict& weights,
                                      uint start_resnum, 
                                      uint num_residues, 
                                      uint chain_idx) {
    std::map<String, Real> w;
    core::ConvertDictToMap(weights, w);
    return s.CalculateLinearCombination(w, start_resnum, num_residues, 
                                        chain_idx);
  }
  Real WrapCalculateLinearCombinationList(BackboneOverallScorer& s,
                                      const boost::python::dict& weights,
                                      const boost::python::list& start_resnum, 
                                      const boost::python::list& num_residues, 
                                      const boost::python::list& chain_idx) {
    std::map<String, Real> w;
    core::ConvertDictToMap(weights, w);

    std::vector<uint> v_start_resnum;
    std::vector<uint> v_num_residues;
    std::vector<uint> v_chain_idx;

    core::ConvertListToVector(start_resnum, v_start_resnum);
    core::ConvertListToVector(num_residues, v_num_residues);
    core::ConvertListToVector(chain_idx, v_chain_idx);

    return s.CalculateLinearCombination(w, v_start_resnum, v_num_residues, 
                                        v_chain_idx);
  }
  TorsionScorerPtr WrapTorsionScorerInit(
                      const boost::python::list& group_identifier,
                      uint bins_per_dimension) {
    std::vector<String> v_group_identifier;
    core::ConvertListToVector(group_identifier, v_group_identifier);
    TorsionScorerPtr p(new TorsionScorer(v_group_identifier, bins_per_dimension));
    return p;
  }

  Real WrapScoreSingle(BackboneScorerPtr scorer, uint start_resnum, 
                       uint num_residues, uint chain_idx){

    return scorer->CalculateScore(start_resnum, num_residues, chain_idx);
  }
  boost::python::list WrapScoreProfileSingle(BackboneScorerPtr scorer,
                                             uint start_resnum, uint num_residues,
                                             uint chain_idx){
    std::vector<Real> profile;
    scorer->CalculateScoreProfile(start_resnum, num_residues, chain_idx, profile);
    boost::python::list return_list;
    core::AppendVectorToList(profile, return_list);
    return return_list;   
  }
  Real WrapScoreList(BackboneScorerPtr scorer, 
                     boost::python::list& start_resnum,
                     boost::python::list& num_residues,
                     boost::python::list& chain_idx) {

    std::vector<uint> v_start_resnum;
    std::vector<uint> v_num_residues;
    std::vector<uint> v_chain_idx;

    core::ConvertListToVector(start_resnum, v_start_resnum);
    core::ConvertListToVector(num_residues, v_num_residues);
    core::ConvertListToVector(chain_idx, v_chain_idx);

    return scorer->CalculateScore(v_start_resnum, v_num_residues, v_chain_idx);
  }
  boost::python::list WrapScoreProfileList(BackboneScorerPtr scorer,
                                           boost::python::list& start_resnum,
                                           boost::python::list& num_residues,
                                           boost::python::list& chain_idx){

    std::vector<uint> v_start_resnum;
    std::vector<uint> v_num_residues;
    std::vector<uint> v_chain_idx;

    core::ConvertListToVector(start_resnum, v_start_resnum);
    core::ConvertListToVector(num_residues, v_num_residues);
    core::ConvertListToVector(chain_idx, v_chain_idx);

    std::vector<std::vector<Real> > profile;
    scorer->CalculateScoreProfile(v_start_resnum, v_num_residues, v_chain_idx, 
                                  profile);

    boost::python::list return_list;
    for(uint i = 0; i < profile.size(); ++i) {
      boost::python::list l;
      core::AppendVectorToList(profile[i], l);
      return_list.append(l);
    }
    return return_list;   
  }
}

void export_BackboneScore() {

  enum_<PairwiseFunctionType>("PairwiseFunctionType")
    .value("CA_PAIRWISE_FUNCTION", CA_PAIRWISE_FUNCTION)
    .value("CB_PAIRWISE_FUNCTION", CB_PAIRWISE_FUNCTION)
  ;

  // score environment
  class_<BackboneScoreEnv, BackboneScoreEnvPtr>("BackboneScoreEnv", no_init)
    .def("__init__", make_constructor(&WrapInitSeq))
    .def("__init__", make_constructor(&WrapInitSeqList))
    .def("__init__", make_constructor(&WrapInitStr))
    .def("__init__", make_constructor(&WrapInitStrList))
    .def("Copy", &BackboneScoreEnv::Copy)
    .def("SetInitialEnvironment", &BackboneScoreEnv::SetInitialEnvironment,
         (arg("env_structure")))
    .def("SetEnvironment", &BackboneScoreEnv::SetEnvironment,
         (arg("bb_list"), arg("start_resnum"), arg("chain_idx") = 0))
    .def("SetEnvironment", &WrapSetEnvironmentResNum,
         (arg("bb_list"), arg("start_resnum"), arg("chain_idx") = 0))
    .def("ClearEnvironment", &BackboneScoreEnv::ClearEnvironment,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))
    .def("SetPsipredPrediction", &WrapSetPsipredPrediction, (arg("pred")))
    .def("SetPsipredPrediction", &WrapSetPsipredPredictionList, (arg("pred")))
    .def("AddPairwiseFunction", &BackboneScoreEnv::AddPairwiseFunction,
         (arg("function"), arg("function_type")))
    .def("ApplyPairwiseFunction", &BackboneScoreEnv::ApplyPairwiseFunction,
         (arg("chain_idx_one"), arg("resnum_one"), arg("chain_idx_two"),
          arg("resnum_two"), arg("f_idx")))
    .def("Stash", &WrapStashSingle, (arg("start_rnum"), arg("num_residues"), 
                                     arg("chain_idx")=0))
    .def("Stash", &WrapStashList, (arg("start_rnums"), arg("num_residues"), 
                                     arg("chain_indices")=0))    
    .def("Pop", &BackboneScoreEnv::Pop)
    .def("GetSeqres", &BackboneScoreEnv::GetSeqres)
  ;

  // base scorer
  class_<BackboneScorer, boost::noncopyable, BackboneScorerPtr>
    ("BackboneScorer", no_init)
    .def("CalculateScore", &WrapScoreSingle,(arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))
    .def("CalculateScoreProfile", &WrapScoreProfileSingle,(arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))
    .def("CalculateScore", &WrapScoreList,(arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))
    .def("CalculateScoreProfile", &WrapScoreProfileList,(arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))

    ;

  // overall scorer
  class_<BackboneOverallScorer, BackboneOverallScorerPtr>
    ("BackboneOverallScorer", init<>())
    .def("Contains", &BackboneOverallScorer::Contains, (arg("key")))
    .def("Get", &BackboneOverallScorer::Get, (arg("key")))
    .def("__getitem__", &bos_getitem, (arg("key")))
    .def("__setitem__", &bos_setitem, (arg("key"), arg("scorer")))
    .def("AttachEnvironment", &BackboneOverallScorer::AttachEnvironment,
         (arg("env")))
    .def("Calculate", &WrapCalculate,
         (arg("key"), arg("start_resnum"), arg("num_residues"), arg("chain_idx")=0))
    .def("CalculateLinearCombination", &WrapCalculateLinearCombination,
         (arg("linear_weights"), arg("start_resnum"), arg("num_residues"),
          arg("chain_idx")=0))
    .def("Calculate", &WrapCalculateList,
         (arg("key"), arg("start_resnum"), arg("num_residues"), arg("chain_idx")=0))
    .def("CalculateLinearCombination", &WrapCalculateLinearCombinationList,
         (arg("linear_weights"), arg("start_resnum"), arg("num_residues"),
          arg("chain_idx")))
  ;

  // individual scorers
  class_<CBPackingScorer, CBPackingScorerPtr, bases<BackboneScorer> >
    ("CBPackingScorer", init<Real,uint>())
    .def("Load", &CBPackingScorer::Load,(arg("filename"))).staticmethod("Load")
    .def("Save", &CBPackingScorer::Save,(arg("filename")))
    .def("LoadPortable", &CBPackingScorer::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("SavePortable", &CBPackingScorer::SavePortable,(arg("filename")))
    .def("SetEnergy", &CBPackingScorer::SetEnergy,(arg("aa"),arg("count"),arg("energy")))
    .def("DoNormalize", &CBPackingScorer::DoNormalize)
    .def("AttachEnvironment", &CBPackingScorer::AttachEnvironment,(arg("env")))
  ;

  class_<CBetaScorer, CBetaScorerPtr, bases<BackboneScorer> >
    ("CBetaScorer",init<Real,uint,uint>())
    .def("Load", &CBetaScorer::Load,(arg("filename"))).staticmethod("Load")
    .def("Save", &CBetaScorer::Save,(arg("filename")))
    .def("LoadPortable", &CBetaScorer::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("SavePortable", &CBetaScorer::SavePortable,(arg("filename")))
    .def("SetEnergy", &CBetaScorer::SetEnergy,(arg("aa1"),arg("aa2"),arg("bin"),arg("energy")))
    .def("DoInternalScores", &CBetaScorer::DoInternalScores)
    .def("DoExternalScores", &CBetaScorer::DoExternalScores)
    .def("DoNormalize", &CBetaScorer::DoNormalize)
    .def("AttachEnvironment", &CBetaScorer::AttachEnvironment,(arg("env")))
  ;

  class_<ReducedScorer, ReducedScorerPtr, bases<BackboneScorer> >
    ("ReducedScorer", init<Real,uint,uint,uint,uint>())
    .def("Load", &ReducedScorer::Load,(arg("filename"))).staticmethod("Load")
    .def("Save", &ReducedScorer::Save,(arg("filename")))
    .def("LoadPortable", &ReducedScorer::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("SavePortable", &ReducedScorer::SavePortable,(arg("filename")))
    .def("SetEnergy", &ReducedScorer::SetEnergy,(arg("aa1"),arg("aa2"),arg("dist_bin"),arg("alpha_bin"),arg("beta_bin"),arg("gamma_bin"),arg("energy")))
    .def("DoInternalScores", &ReducedScorer::DoInternalScores)
    .def("DoExternalScores", &ReducedScorer::DoExternalScores)
    .def("DoNormalize", &ReducedScorer::DoNormalize)
    .def("AttachEnvironment", &ReducedScorer::AttachEnvironment,(arg("env")))
  ;

  class_<ClashScorer, ClashScorerPtr, bases<BackboneScorer> >
    ("ClashScorer",init<>())
    .def("AttachEnvironment", &ClashScorer::AttachEnvironment,(arg("env")))
    .def("DoInternalScores", &ClashScorer::DoInternalScores)
    .def("DoExternalScores", &ClashScorer::DoExternalScores)
    .def("DoNormalize", &ClashScorer::DoNormalize)
  ;

  class_<HBondScorer, HBondScorerPtr, bases<BackboneScorer> >
    ("HBondScorer", init<Real,Real,Real,Real,Real,Real,Real,Real,uint,uint,uint,uint>())
    .def("Load", &HBondScorer::Load,(arg("filename"))).staticmethod("Load")
    .def("Save", &HBondScorer::Save,(arg("filename")))
    .def("LoadPortable", &HBondScorer::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("SavePortable", &HBondScorer::SavePortable,(arg("filename")))
    .def("SetEnergy", &HBondScorer::SetEnergy,(arg("state"),arg("d_bin"),arg("alpha_bin"),arg("beta_bin"),arg("gamma_bin"),arg("energy")))
    .def("DoInternalScores", &HBondScorer::DoInternalScores)
    .def("DoExternalScores", &HBondScorer::DoExternalScores)
    .def("DoNormalize", &HBondScorer::DoNormalize)
    .def("AttachEnvironment", &HBondScorer::AttachEnvironment,(arg("env")))
  ;

  class_<SSAgreementScorer, SSAgreementScorerPtr, bases<BackboneScorer> >
    ("SSAgreementScorer",init<>())
    .def("Load", &SSAgreementScorer::Load,(arg("filename"))).staticmethod("Load")
    .def("Save", &SSAgreementScorer::Save,(arg("filename")))
    .def("LoadPortable", &SSAgreementScorer::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("SavePortable", &SSAgreementScorer::SavePortable,(arg("filename")))
    .def("SetScore", &SSAgreementScorer::SetScore,(arg("psipred_state"),arg("psipred_cfi"),arg("dssp_state"),arg("score")))
    .def("DoNormalize", &SSAgreementScorer::DoNormalize)
    .def("AttachEnvironment", &SSAgreementScorer::AttachEnvironment,(arg("env")))
  ;

  class_<TorsionScorer, TorsionScorerPtr, bases<BackboneScorer> >
    ("TorsionScorer", no_init)
    .def("__init__",make_constructor(&WrapTorsionScorerInit))
    .def("Load", &TorsionScorer::Load,(arg("filename"))).staticmethod("Load")
    .def("Save", &TorsionScorer::Save,(arg("filename")))
    .def("LoadPortable", &TorsionScorer::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("SavePortable", &TorsionScorer::SavePortable,(arg("filename")))
    .def("SetEnergy", &TorsionScorer::SetEnergy,(arg("group_idx"),arg("phi_bin"),arg("psi_bin"),arg("energy")))
    .def("DoNormalize", &TorsionScorer::DoNormalize)
    .def("AttachEnvironment", &TorsionScorer::AttachEnvironment,(arg("env")))
  ;

  class_<PairwiseScorer, PairwiseScorerPtr, bases<BackboneScorer> >
    ("PairwiseScorer", init<>())
    .def("AttachEnvironment", &PairwiseScorer::AttachEnvironment,(arg("env")))
    .def("DoInternalScores", &PairwiseScorer::DoInternalScores)
    .def("DoExternalScores", &PairwiseScorer::DoExternalScores)
    .def("DoNormalize", &PairwiseScorer::DoNormalize)
  ;

}
