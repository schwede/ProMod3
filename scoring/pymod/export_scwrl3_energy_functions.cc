// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/scoring/scwrl3_energy_functions.hh>
#include <promod3/core/export_helper.hh>

using namespace promod3;
using namespace promod3::scoring;
using namespace boost::python;


void export_SCWRL3EnergyFunctions() {

  def("SCWRL3PairwiseScore", &SCWRL3PairwiseScore, (arg("distance"), arg("summed_vdw_radii")));

  def("SCWRL3DisulfidScore", &SCWRL3DisulfidScore, (arg("ca_pos_one"), arg("cb_pos_one"),
                                                    arg("sg_pos_one"), arg("ca_pos_two"),
                                                    arg("cb_pos_two"), arg("sg_pos_two")));

}
