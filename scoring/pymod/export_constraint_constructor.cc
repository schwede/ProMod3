// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>
#include <promod3/scoring/constraint_constructor.hh>

using namespace boost::python;
using namespace promod3;
using namespace promod3::scoring;

namespace{

void WrapAttachConstraintsSingle(DiscoContainerPtr p, 
	                               promod3::scoring::BackboneScoreEnv& env,
	                               uint chain_idx, Real cluster_thresh,
	                               Real gamma){
  std::vector<uint> indices(1, chain_idx);
  p->AttachConstraints(env, indices, cluster_thresh, gamma);
}

void WrapAttachConstraintsList(DiscoContainerPtr p, 
	                             promod3::scoring::BackboneScoreEnv& env,
	                             const boost::python::list& chain_indices, 
	                             Real cluster_thresh, Real gamma){
  std::vector<uint> v_chain_indices;
  core::ConvertListToVector(chain_indices, v_chain_indices);
  p->AttachConstraints(env, v_chain_indices, cluster_thresh, gamma);
}

void WrapAttachEvolutionaryConstraintsSingle(promod3::scoring::BackboneScoreEnv& env, 
                                             const ost::seq::AlignmentHandle& aln,
                                             uint chain_idx,
                                             promod3::scoring::PairwiseFunctionType ft,
                                             Real fraction_constraints, 
                                             uint constant_function_length,
                                             uint tail_length){

  std::vector<uint>  chain_indices(1, chain_idx);
  promod3::scoring::AttachEvolutionaryConstraints(env, aln, chain_indices, ft, 
                                                  fraction_constraints,
                                                  constant_function_length, 
                                                  tail_length);
}

void WrapAttachEvolutionaryConstraintsList(promod3::scoring::BackboneScoreEnv& env, 
                                           const ost::seq::AlignmentHandle& aln,
                                           const boost::python::list& chain_indices,
                                           promod3::scoring::PairwiseFunctionType ft,
                                           Real fraction_constraints, 
                                           uint constant_function_length,
                                           uint tail_length){

  std::vector<uint> v_chain_indices;
  core::ConvertListToVector(chain_indices, v_chain_indices); 
  promod3::scoring::AttachEvolutionaryConstraints(env, aln, v_chain_indices, ft, 
                                                  fraction_constraints,
                                                  constant_function_length, 
                                                  tail_length);
}

}



void export_constraint_constructor()
{

  class_<DiscoContainer, DiscoContainerPtr>("DiscoContainer", no_init)
    .def(init<const ost::seq::SequenceHandle&, PairwiseFunctionType>(
         (arg("seqres"), arg("function_type")=CA_PAIRWISE_FUNCTION)))
    .def("AddStructuralInfo", &DiscoContainer::AddStructuralInfo, (arg("aln")))
    .def("AttachConstraints", &WrapAttachConstraintsSingle,
         (arg("environment"), arg("chain_idx"), arg("cluster_thresh")=0.50,
          arg("gamma")=70.0))
    .def("AttachConstraints", &WrapAttachConstraintsList,
         (arg("environment"), arg("chain_indices"), arg("cluster_thresh")=0.50,
          arg("gamma")=70.0))
    .def("AttachEvolutionaryConstraints", &WrapAttachEvolutionaryConstraintsSingle,
         (arg("environment"), arg("alignment"), arg("chain_idx")=0,
          arg("function_type")=CB_PAIRWISE_FUNCTION,
          arg("fraction_constraints")=0.1, arg("constant_function_length")=8,
          arg("tail_length")=4))
    .def("AttachEvolutionaryConstraints", &WrapAttachEvolutionaryConstraintsList,
         (arg("environment"), arg("alignment"), arg("chain_indices"),
          arg("function_type")=CB_PAIRWISE_FUNCTION,
          arg("fraction_constraints")=0.1, arg("constant_function_length")=8,
          arg("tail_length")=4))
  ;

}
