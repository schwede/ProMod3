// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>
#include <promod3/scoring/all_atom_score_base.hh>
#include <promod3/scoring/all_atom_overall_scorer.hh>
#include <promod3/scoring/all_atom_interaction_score.hh>
#include <promod3/scoring/all_atom_packing_score.hh>
#include <promod3/scoring/all_atom_clash_score.hh>

using namespace promod3;
using namespace promod3::scoring;
using namespace boost::python;

// WRAPPERS
namespace {

AllAtomScorerPtr aaos_getitem(AllAtomOverallScorer& s, const String& key) {
  return s[key];
}
void aaos_setitem(AllAtomOverallScorer& s, const String& key,
                  AllAtomScorerPtr item) {
  s[key] = item;
}
Real WrapCalculateLinearCombination(AllAtomOverallScorer& s,
                                    const boost::python::dict& weights,
                                    uint start_resnum, uint num_residues,
                                    uint chain_idx) {
  std::map<String, Real> w;
  core::ConvertDictToMap(weights, w);
  return s.CalculateLinearCombination(w, start_resnum, num_residues, chain_idx);
}
Real WrapCalculateLinearCombinationList(AllAtomOverallScorer& s,
                                        const boost::python::dict& weights,
                                        const boost::python::list& start_resnum, 
                                        const boost::python::list& num_residues,
                                        const boost::python::list& chain_idx) {
  std::map<String, Real> w;
  core::ConvertDictToMap(weights, w);
  
  std::vector<uint> v_start_resnum;
  std::vector<uint> v_num_residues;
  std::vector<uint> v_chain_idx;

  core::ConvertListToVector(start_resnum, v_start_resnum);
  core::ConvertListToVector(num_residues, v_num_residues);
  core::ConvertListToVector(chain_idx, v_chain_idx);

  return s.CalculateLinearCombination(w, v_start_resnum, v_num_residues, 
                                      v_chain_idx);
}
Real WrapAACalculateScore(const AllAtomScorer& scorer, uint start_resnum,
                          uint num_residues, uint chain_idx) {
  return scorer.CalculateScore(start_resnum, num_residues, chain_idx);
}  
Real WrapAACalculateScoreList(AllAtomScorer& scorer, 
                              boost::python::list& start_resnum,
                              boost::python::list& num_residues,
                              boost::python::list& chain_idx) {

  std::vector<uint> v_start_resnum;
  std::vector<uint> v_num_residues;
  std::vector<uint> v_chain_idx;

  core::ConvertListToVector(start_resnum, v_start_resnum);
  core::ConvertListToVector(num_residues, v_num_residues);
  core::ConvertListToVector(chain_idx, v_chain_idx);

  return scorer.CalculateScore(v_start_resnum, v_num_residues, v_chain_idx);
}
boost::python::list
WrapAACalculateScoreProfile(const AllAtomScorer& scorer, uint start_resnum,
                            uint num_residues, uint chain_idx) {
  std::vector<Real> profile;
  scorer.CalculateScoreProfile(start_resnum, num_residues, chain_idx, profile);
  boost::python::list return_list;
  core::AppendVectorToList(profile, return_list);
  return return_list;
}
boost::python::list 
WrapAACalculateScoreProfileList(const AllAtomScorer& scorer,
                                boost::python::list& start_resnum,
                                boost::python::list& num_residues,
                                boost::python::list& chain_idx) {

  std::vector<uint> v_start_resnum;
  std::vector<uint> v_num_residues;
  std::vector<uint> v_chain_idx;

  core::ConvertListToVector(start_resnum, v_start_resnum);
  core::ConvertListToVector(num_residues, v_num_residues);
  core::ConvertListToVector(chain_idx, v_chain_idx);

  std::vector<std::vector<Real> > profile;
  scorer.CalculateScoreProfile(v_start_resnum, v_num_residues, v_chain_idx, 
                               profile);

  boost::python::list return_list;
  for(uint i = 0; i < profile.size(); ++i) {
    boost::python::list l;
    core::AppendVectorToList(profile[i], l);
    return_list.append(l);
  }
  return return_list;   
}
} // anon wrapper ns

void export_AllAtomScore() {
  
  // base scorer
  class_<AllAtomScorer, boost::noncopyable, AllAtomScorerPtr>
    ("AllAtomScorer", no_init)
    .def("CalculateScore", WrapAACalculateScore,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx")=0))
    .def("CalculateScore", WrapAACalculateScoreList,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx")))
    .def("CalculateScoreProfile", WrapAACalculateScoreProfile,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx")=0))
    .def("CalculateScoreProfile", WrapAACalculateScoreProfileList,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx")))
  ;

  // overall scorer
  class_<AllAtomOverallScorer, AllAtomOverallScorerPtr>
    ("AllAtomOverallScorer", init<>())
    .def("Contains", &AllAtomOverallScorer::Contains, (arg("key")))
    .def("Get", &AllAtomOverallScorer::Get, (arg("key")))
    .def("__getitem__", &aaos_getitem, (arg("key")))
    .def("__setitem__", &aaos_setitem, (arg("key"), arg("scorer")))
    .def("AttachEnvironment", &AllAtomOverallScorer::AttachEnvironment,
         (arg("env")))
    .def("CalculateLinearCombination", &WrapCalculateLinearCombination,
         (arg("linear_weights"), arg("start_resnum"), arg("num_residues"),
          arg("chain_idx")=0))
    .def("CalculateLinearCombination", &WrapCalculateLinearCombinationList,
         (arg("linear_weights"), arg("start_resnum"), arg("num_residues"),
          arg("chain_idx")))
  ;

  class_<AllAtomInteractionScorer, AllAtomInteractionScorerPtr,
         bases<AllAtomScorer> >
    ("AllAtomInteractionScorer", init<Real,uint,uint>())
    .def("Load", &AllAtomInteractionScorer::Load, (arg("filename")))
    .staticmethod("Load")
    .def("Save", &AllAtomInteractionScorer::Save, (arg("filename")))
    .def("LoadPortable", &AllAtomInteractionScorer::LoadPortable,
         (arg("filename")))
    .staticmethod("LoadPortable")
    .def("SavePortable", &AllAtomInteractionScorer::SavePortable,
         (arg("filename")))
    .def("SetEnergy", &AllAtomInteractionScorer::SetEnergy,
         (arg("aaa1"), arg("aaa2"), arg("bin"), arg("energy")))
    .def("AttachEnvironment", &AllAtomInteractionScorer::AttachEnvironment,
         (arg("env")))
    .def("DoInternalScores", &AllAtomInteractionScorer::DoInternalScores)
    .def("DoExternalScores", &AllAtomInteractionScorer::DoExternalScores)
    .def("DoNormalize", &AllAtomInteractionScorer::DoNormalize)
  ;

  class_<AllAtomPackingScorer, AllAtomPackingScorerPtr,
         bases<AllAtomScorer> >
    ("AllAtomPackingScorer", init<Real,uint>())
    .def("Load", &AllAtomPackingScorer::Load, (arg("filename")))
    .staticmethod("Load")
    .def("Save", &AllAtomPackingScorer::Save, (arg("filename")))
    .def("LoadPortable", &AllAtomPackingScorer::LoadPortable,
         (arg("filename")))
    .staticmethod("LoadPortable")
    .def("SavePortable", &AllAtomPackingScorer::SavePortable,
         (arg("filename")))
    .def("SetEnergy", &AllAtomPackingScorer::SetEnergy,
         (arg("aaa"), arg("count"), arg("energy")))
    .def("AttachEnvironment", &AllAtomPackingScorer::AttachEnvironment,
         (arg("env")))
    .def("DoNormalize", &AllAtomPackingScorer::DoNormalize)
  ;

  class_<AllAtomClashScorer, AllAtomClashScorerPtr,
         bases<AllAtomScorer> >
    ("AllAtomClashScorer",init<>())
    .def("AttachEnvironment", &AllAtomClashScorer::AttachEnvironment,
         (arg("env")))
    .def("DoInternalScores", &AllAtomClashScorer::DoInternalScores)
    .def("DoExternalScores", &AllAtomClashScorer::DoExternalScores)
    .def("DoNormalize", &AllAtomClashScorer::DoNormalize)
  ;

}
