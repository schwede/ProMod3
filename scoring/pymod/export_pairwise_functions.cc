// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/scoring/pairwise_scoring_function.hh>
#include <promod3/core/export_helper.hh>

using namespace promod3;
using namespace promod3::scoring;
using namespace boost::python;

namespace {

ConstraintFunctionPtr WrapConstraintInit(Real min, Real max, 
                                         const boost::python::list& values) {
  std::vector<Real> v_values;
  core::ConvertListToVector(values, v_values);
  ConstraintFunctionPtr p(new ConstraintFunction(min, max, v_values));
  return p;
}

} // ns


void export_PairwiseFunction() {

  class_<PairwiseFunction, PairwiseFunctionPtr, boost::noncopyable>("PairwiseFunction", no_init);

  class_<ConstraintFunction, ConstraintFunctionPtr, bases<PairwiseFunction> >("ConstraintFunction", no_init)
    .def("__init__", make_constructor(&WrapConstraintInit))
    .def("Score", &ConstraintFunction::Score,(arg("distance")))
  ;

  class_<ContactFunction, ContactFunctionPtr, bases<PairwiseFunction> >("ContactFunction", init<Real,Real>())
    .def("Score", &ContactFunction::Score,(arg("distance")))
  ;

}
