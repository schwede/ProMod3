# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


'''Convert files from portable to raw.'''
import sys
import os
from promod3 import scoring

if len(sys.argv) != 4:
  print("usage: python convert_binaries.py PORTABLE_FILE_IN RAW_FILE_OUT CLASS")
  sys.exit(1)

portable_file_in = sys.argv[1]
raw_file_out = sys.argv[2]
class_name = sys.argv[3]

if class_name == "CBPackingScorer":
  my_data = scoring.CBPackingScorer.LoadPortable(portable_file_in)
elif class_name == "CBetaScorer":
  my_data = scoring.CBetaScorer.LoadPortable(portable_file_in)
elif class_name == "ReducedScorer":
  my_data = scoring.ReducedScorer.LoadPortable(portable_file_in)
elif class_name == "HBondScorer":
  my_data = scoring.HBondScorer.LoadPortable(portable_file_in)
elif class_name == "SSAgreementScorer":
  my_data = scoring.SSAgreementScorer.LoadPortable(portable_file_in)
elif class_name == "TorsionScorer":
  my_data = scoring.TorsionScorer.LoadPortable(portable_file_in)
elif class_name == "AllAtomInteractionScorer":
  my_data = scoring.AllAtomInteractionScorer.LoadPortable(portable_file_in)
elif class_name == "AllAtomPackingScorer":
  my_data = scoring.AllAtomPackingScorer.LoadPortable(portable_file_in)
else:
  raise RuntimeError("Cannot convert class " + class_name)

my_data.Save(raw_file_out)
