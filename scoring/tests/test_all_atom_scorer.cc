// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/core/message.hh>
#include <promod3/loop/all_atom_env.hh>
#include <promod3/scoring/scoring_object_loader.hh>
#include <promod3/scoring/all_atom_interaction_score.hh>
#include <promod3/scoring/all_atom_packing_score.hh>
#include <promod3/scoring/all_atom_clash_score.hh>
#include <promod3/scoring/all_atom_overall_scorer.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>
#include <ost/mol/builder.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/seq/sequence_op.hh>
#include <ost/img/map.hh>
#include <cstdio>

BOOST_AUTO_TEST_SUITE( scoring );

using namespace promod3::scoring;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

promod3::loop::AllAtomEnv GenerateAllAtomEnv(
      const String& pdb_name = "data/3DEFA.pdb",
      const String& seqresstr = "MGSLVREWVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQAEGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQVVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHVDAAALEHHHHHH") {

  ost::mol::EntityHandle test_ent = LoadTestStructure(pdb_name);

  ost::seq::SequenceHandle seqres;
  if (seqresstr.empty()) {
    // get from chain A
    seqres = ost::seq::SequenceFromChain("A", test_ent.FindChain("A"));
  } else {
    seqres = ost::seq::CreateSequence("A", seqresstr);
  }
  ost::seq::SequenceList seqres_list = ost::seq::CreateSequenceList();
  seqres_list.AddSequence(seqres);

  promod3::loop::AllAtomEnv env(seqres_list);
  env.SetInitialEnvironment(test_ent);
  return env;
}

// incl. res. with numbers = res_from..res_to-1
promod3::loop::AllAtomPositions GenerateTestPos(
      const String& pdb_name = "data/3DEFA.pdb",
      uint res_from = 214, uint res_to = 223) {

  ost::mol::EntityHandle test_ent = LoadTestStructure(pdb_name);
  ost::mol::ResidueHandleList res_list;
  for (uint i = res_from; i < res_to; ++i) {
    std::stringstream single_res_query;
    single_res_query << "rnum=" << i;
    ost::mol::ResidueHandle res = test_ent.Select(single_res_query.str()).GetResidueList()[0].GetHandle();
    res_list.push_back(res);
  }

  return promod3::loop::AllAtomPositions(res_list);
}

// check scorer calls for def. new_pos
void CheckScorer(AllAtomScorerPtr scorer, const Real exp_score) {
  // test check of chain_idx
  BOOST_CHECK_THROW(scorer->CalculateScore(214, 9, -1), promod3::Error);
  BOOST_CHECK_THROW(scorer->CalculateScore(214, 9, 1), promod3::Error);

  // test check of start resnum
  BOOST_CHECK_THROW(scorer->CalculateScore(0, 9, 0), promod3::Error);
  BOOST_CHECK_THROW(scorer->CalculateScore(-1, 9, 0), promod3::Error);
  BOOST_CHECK_THROW(scorer->CalculateScore(1000, 9, 0), promod3::Error);
  // the first resnum, where 9 residues don't fit anymore...
  BOOST_CHECK_THROW(scorer->CalculateScore(255, 9, 0), promod3::Error);

  // test the actual score outcome
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, 9, 0), exp_score, Real(1e-3));
  // check score profile size
  std::vector<Real> profile;
  scorer->CalculateScoreProfile(214, 9, 0, profile);
  BOOST_CHECK_EQUAL(profile.size(), uint(9));
  scorer->CalculateScoreProfile(214, 3, 0, profile);
  BOOST_CHECK_EQUAL(profile.size(), uint(3));
}

// check overall scorer on a single score
void SanityCheckOverallScorer(AllAtomOverallScorer& scorer,
                              AllAtomScorerPtr single_scorer,
                              const String& key) {
  // ref = single score
  BOOST_CHECK(scorer.Contains(key));
  Real ref_score = single_scorer->CalculateScore(214, 9, 0);
  Real score = scorer[key]->CalculateScore(214, 9, 0);
  BOOST_CHECK_CLOSE(score, ref_score, Real(1e-3));
  std::map<String, Real> weights;
  weights[key] = 1.0;
  score = scorer.CalculateLinearCombination(weights, 214, 9, 0);
  BOOST_CHECK_CLOSE(score, ref_score, Real(1e-3));
}

// attach single scorer to overall one and check it
template <typename ScorerPtr>
void AttachScorer(AllAtomOverallScorer& scorer, ScorerPtr single_scorer,
                  promod3::loop::AllAtomEnv& env, const String& key) {
  single_scorer->AttachEnvironment(env);
  scorer[key] = single_scorer;
  SanityCheckOverallScorer(scorer, single_scorer, key);
}

} // anon ns

BOOST_AUTO_TEST_CASE(test_aa_interaction_score) {
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv();
  AllAtomInteractionScorerPtr scorer = LoadAllAtomInteractionScorer();
  scorer->AttachEnvironment(env);
  // NOTE: not comparable to QMEAN anymore as we halve internal ones...
  CheckScorer(scorer, -3.58627);
}

BOOST_AUTO_TEST_CASE(test_aa_packing_score) {
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv();
  AllAtomPackingScorerPtr scorer = LoadAllAtomPackingScorer();
  scorer->AttachEnvironment(env);
  CheckScorer(scorer, -2.90227);
}

BOOST_AUTO_TEST_CASE(test_aa_clash_score) {
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv();
  AllAtomClashScorerPtr scorer(new AllAtomClashScorer);
  scorer->AttachEnvironment(env);
  CheckScorer(scorer, 0.0);
}

BOOST_AUTO_TEST_CASE(test_aa_clash_score2) {
  // check sthg with actual clashes
  String pdb_name = "data/clash_test.pdb";
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv(pdb_name, "");
  AllAtomClashScorerPtr scorer(new AllAtomClashScorer);
  scorer->AttachEnvironment(env);
  Real clash_score = scorer->CalculateScore(35, 5, 0);
  BOOST_CHECK_CLOSE(clash_score, 14.2627, Real(1e-3));
}

BOOST_AUTO_TEST_CASE(test_aa_clash_score3) {
  // check treatment of disulfid bridges
  String pdb_name = "data/1CRNA.pdb";
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv(pdb_name, "");
  AllAtomClashScorerPtr scorer(new AllAtomClashScorer);
  scorer->AttachEnvironment(env);
  Real clash_score = scorer->CalculateScore(2, 44, 0);
  BOOST_CHECK_CLOSE(clash_score, 0.0, Real(1e-3));
}


BOOST_AUTO_TEST_CASE(test_aa_overall_scorer) {
  // setup env
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv();

  // setup scorer
  AllAtomOverallScorer scorer;
  // setup weights
  std::map<String, Real> weights;
  weights["aa_packing"] = 0.1;
  weights["aa_interaction"] = 0.2;
  weights["aa_clash"] = 0.3;
  weights["intercept"] = 1.0;
  // check for errors
  BOOST_CHECK(!scorer.Contains("aa_interaction"));
  BOOST_CHECK_THROW(scorer.Get("aa_interaction"), promod3::Error);
  BOOST_CHECK_THROW(scorer.CalculateLinearCombination(weights, 214, 9, 0),
                    promod3::Error);

  // attach and sanity check single scores
  AttachScorer(scorer, LoadAllAtomInteractionScorer(), env,
               "aa_interaction");
  AttachScorer(scorer, LoadAllAtomPackingScorer(), env, "aa_packing");
  AttachScorer(scorer, AllAtomClashScorerPtr(new AllAtomClashScorer), env,
               "aa_clash");
  
  // check final score// exp. score from single ones above
  Real exp_score = 0.1 * -2.90227 + 0.2 * -3.58627
                 + 0.3 * 0.0 + 1.0;
  Real calculated_score = scorer.CalculateLinearCombination(weights, 214, 9, 0);
  BOOST_CHECK(std::abs(calculated_score - exp_score) < Real(0.00001));

  // check for more errors
  BOOST_CHECK(!scorer.Contains("meh"));
  BOOST_CHECK_THROW(scorer.Get("meh"), promod3::Error);
  weights["meh"] = 5.0;
  BOOST_CHECK_THROW(scorer.CalculateLinearCombination(weights, 214, 9, 0),
                    promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_aa_normalize) {

  // setup env with something that clashes
  String pdb_name = "data/clash_test.pdb";
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv(pdb_name, "");

  // setup scorer
  AllAtomOverallScorer scorer;
  // setup weights
  std::map<String, Real> weights;
  weights["aa_packing"] = 1.0;
  weights["aa_interaction"] = 1.0;
  weights["aa_clash"] = 1.0;

  AllAtomPackingScorerPtr packing_scorer = LoadAllAtomPackingScorer();
  AllAtomInteractionScorerPtr interaction_scorer = LoadAllAtomInteractionScorer();
  AllAtomClashScorerPtr clash_scorer = AllAtomClashScorerPtr(new AllAtomClashScorer);  

  scorer["aa_packing"] = packing_scorer;
  scorer["aa_interaction"] = interaction_scorer;
  scorer["aa_clash"] = clash_scorer;

  scorer.AttachEnvironment(env);

  Real exp_score = scorer.CalculateLinearCombination(weights, 2, 44, 0);

  packing_scorer->DoNormalize(false);
  interaction_scorer->DoNormalize(false);
  clash_scorer->DoNormalize(false);

  BOOST_CHECK_CLOSE(scorer.CalculateLinearCombination(weights, 2, 44, 0), 
                    44 * exp_score, 1e-3);
}


BOOST_AUTO_TEST_CASE(test_aa_internal_external) {

  // setup env with something that clashes
  String pdb_name = "data/clash_test.pdb";
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv(pdb_name, "");

  // load scorere (only the ones that provide the internal / external mechanism)
  AllAtomInteractionScorerPtr interaction_scorer = 
  LoadAllAtomInteractionScorer();
  AllAtomClashScorerPtr clash_scorer = 
  AllAtomClashScorerPtr(new AllAtomClashScorer);
  interaction_scorer->AttachEnvironment(env);
  clash_scorer->AttachEnvironment(env);

  // lets do the overall score... this should be well tested by now
  Real interaction_score = interaction_scorer->CalculateScore(2, 44, 0);
  Real clash_score = clash_scorer->CalculateScore(2, 44, 0);

  // lets do the internal scores
  interaction_scorer->DoExternalScores(false);
  clash_scorer->DoExternalScores(false);
  Real internal_interaction_score = 
  interaction_scorer->CalculateScore(2, 44, 0);
  Real internal_clash_score =
  clash_scorer->CalculateScore(2, 44, 0);

  // lets do the external scores
  interaction_scorer->DoExternalScores(true);
  clash_scorer->DoExternalScores(true);
  interaction_scorer->DoInternalScores(false);
  clash_scorer->DoInternalScores(false);
  Real external_interaction_score = 
  interaction_scorer->CalculateScore(2, 44, 0);
  Real external_clash_score =
  clash_scorer->CalculateScore(2, 44, 0);

  // the sum between the internal and external score must be the total score
  BOOST_CHECK_CLOSE(internal_interaction_score + external_interaction_score, 
                    interaction_score, Real(1e-3));
  BOOST_CHECK_CLOSE(internal_clash_score + external_clash_score, 
                    clash_score, Real(1e-3));

  // Do internal AND external scores and kill the full environment except the 
  // actual loop to score. As a result, we should get the previously calculated
  // INTERNAL score.
  interaction_scorer->DoInternalScores(true);
  clash_scorer->DoExternalScores(true);
  interaction_scorer->DoInternalScores(true);
  clash_scorer->DoExternalScores(true); 
  promod3::loop::AllAtomEnvPositionsPtr env_pos_only_loop = 
  env.GetEnvironment(2, 44, 0);
  env.ClearEnvironment(1, env.GetIdxHandlerData()->GetChainSize(0), 0);
  env.SetEnvironment(*env_pos_only_loop);
  BOOST_CHECK_CLOSE(interaction_scorer->CalculateScore(2, 44, 0),
                    internal_interaction_score, Real(1e-3));
  BOOST_CHECK_CLOSE(clash_scorer->CalculateScore(2, 44, 0),
                    internal_clash_score, Real(1e-3));
}

BOOST_AUTO_TEST_CASE(test_aa_multiple_stretches) {

  // setup env with something that clashes
  String pdb_name = "data/clash_test.pdb";
  promod3::loop::AllAtomEnv env = GenerateAllAtomEnv(pdb_name, "");

  // setup scorer
  AllAtomOverallScorer scorer;
  // setup weights
  std::map<String, Real> weights;
  weights["aa_packing"] = 1.0;
  weights["aa_interaction"] = 1.0;
  weights["aa_clash"] = 1.0;

  AllAtomPackingScorerPtr packing_scorer = LoadAllAtomPackingScorer();
  AllAtomInteractionScorerPtr interaction_scorer = LoadAllAtomInteractionScorer();
  AllAtomClashScorerPtr clash_scorer = AllAtomClashScorerPtr(new AllAtomClashScorer);  

  scorer["aa_packing"] = packing_scorer;
  scorer["aa_interaction"] = interaction_scorer;
  scorer["aa_clash"] = clash_scorer;

  scorer.AttachEnvironment(env);

  Real exp_score = scorer.CalculateLinearCombination(weights, 2, 44, 0);

  std::vector<uint> start_resnums;
  std::vector<uint> stretch_lengths;
  std::vector<uint> chain_indices;
  uint first_stretch_length = 20;
  start_resnums.push_back(2);
  start_resnums.push_back(2 + first_stretch_length);
  stretch_lengths.push_back(first_stretch_length);
  stretch_lengths.push_back(44 - first_stretch_length);
  chain_indices.push_back(0);
  chain_indices.push_back(0);

  BOOST_CHECK_CLOSE(scorer.CalculateLinearCombination(weights, start_resnums,
                    stretch_lengths, chain_indices), exp_score, 1e-3);
}

BOOST_AUTO_TEST_SUITE_END();
