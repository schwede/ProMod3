// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/scoring/backbone_overall_scorer.hh>
#include <promod3/scoring/scoring_object_loader.hh>
#include <promod3/scoring/clash_score.hh>
#include <promod3/scoring/pairwise_score.hh>
#include <promod3/scoring/density_score.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>
#include <ost/mol/builder.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/seq/sequence_op.hh>
#include <ost/img/map.hh>
#include <cstdio>

BOOST_AUTO_TEST_SUITE( scoring );

using namespace promod3::scoring;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

promod3::scoring::BackboneScoreEnv GenerateBackboneEnv(
      const String& pdb_name = "data/3DEFA.pdb",
      const String& seqresstr = "MGSLVREWVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGG"
                                "VGKSSTVNSLIGEQVVRVSPFQAEGLRPVMVSRTMGGFTINIIDTP"
                                "GLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQV"
                                "VIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLK"
                                "TIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWI"
                                "PNLVKAITDVATNQRKAIHVDAAALEHHHHHH") {

  ost::mol::EntityHandle test_ent = LoadTestStructure(pdb_name);

  ost::seq::SequenceHandle seqres;
  if (seqresstr.empty()) {
    // get from chain A
    seqres = ost::seq::SequenceFromChain("A", test_ent.FindChain("A"));
  } else {
    seqres = ost::seq::CreateSequence("A", seqresstr);
  }
  ost::seq::SequenceList seqres_list = ost::seq::CreateSequenceList();
  seqres_list.AddSequence(seqres);

  promod3::scoring::BackboneScoreEnv env(seqres_list);
  env.SetInitialEnvironment(test_ent);

  return env;
}

// incl. res. with numbers = res_from..res_to-1
promod3::loop::BackboneList GenerateTestBBList(
      const String& pdb_name = "data/3DEFA.pdb",
      uint res_from = 214, uint res_to = 223) {

  ost::mol::EntityHandle test_ent = LoadTestStructure(pdb_name);

  promod3::loop::BackboneList bb_list;
  for (uint i = res_from; i < res_to; ++i) {
    std::stringstream single_res_query;
    single_res_query << "rnum=" << i;
    ost::mol::ResidueHandle res = 
    test_ent.Select(single_res_query.str()).GetResidueList()[0].GetHandle();
    bb_list.push_back(res);
  }

  return bb_list;
}

// add psipred stuff for default case
void SetPsipredPrediction(BackboneScoreEnv& env) {

  // generate a psipred object and attach it to the scorer to calculate the
  // ss_agreement score
  String psipred_ss = "CCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCHHHHHHH";
  psipred_ss += "HHCCCCCCCCCCCCEEEEEEEEEEECCCEEEEEEECCCCCCCCCHHHHHHHHHHHHHCCCC";
  psipred_ss += "CCEEEEEEECCCCCCCHHHHHHHHHHHHHHCCCCCCCEEEEEECCCCCCCCCCCHHHHHHH";
  psipred_ss += "HHHHHHHHHHHHCCCCCCCCCCCCCCEEEEECCCCEEECCCCCEECCCCHHHHHHHHHHHH";
  psipred_ss += "HHHHHCCCCCCCCHHHHHHCCCCC";

  int psipred_conf[] = {9,6,4,0,3,4,1,3,1,1,4,6,7,7,8,9,9,9,9,9,9,9,9,9,9,9,9,9,
                        8,8,6,1,5,8,8,7,2,8,9,9,9,8,8,9,9,9,9,8,8,9,9,9,9,9,9,9,
                        6,7,8,7,6,5,3,3,5,6,6,7,5,4,6,8,8,8,9,9,9,9,8,4,1,8,6,6,
                        8,9,9,9,9,7,9,9,8,4,0,1,2,2,0,3,7,9,9,9,9,9,9,9,9,8,6,3,
                        3,4,7,9,9,8,8,9,9,9,9,8,4,7,8,6,6,5,5,4,7,7,9,9,9,9,9,9,
                        9,9,9,9,8,3,4,2,4,5,7,8,5,8,9,9,9,9,8,6,0,0,1,7,7,4,4,4,
                        6,8,9,9,9,9,9,9,8,7,9,9,9,9,9,9,9,9,9,7,3,3,0,0,0,1,1,2,
                        2,1,1,3,5,8,6,5,8,9,8,6,2,4,5,2,3,5,5,6,8,8,6,3,0,0,7,7,
                        7,2,5,8,9,9,9,9,9,9,9,9,9,9,9,9,9,8,2,7,9,9,6,3,3,1,7,7,
                        7,7,7,5,3,0,2,4,7,9};

  std::vector<char> v_psipred_ss;
  std::vector<int> v_psipred_conf;

  for(int i = 0; i < 262; ++i){
    v_psipred_ss.push_back(psipred_ss[i]);
    v_psipred_conf.push_back(psipred_conf[i]);
  }

  promod3::loop::PsipredPredictionPtr psipred_prediction(
    new promod3::loop::PsipredPrediction(v_psipred_ss, v_psipred_conf));

  env.SetPsipredPrediction(psipred_prediction);
}

// check scorer calls for def. bb_list
void CheckScorerThrows(const promod3::loop::BackboneList& bb_list,
                       BackboneScorerPtr scorer) {

  // test check of chain_idx
  BOOST_CHECK_THROW(scorer->CalculateScore(214, bb_list.size(), -1), promod3::Error);
  BOOST_CHECK_THROW(scorer->CalculateScore(214, bb_list.size(), 1), promod3::Error);

  // test check of start resnum
  BOOST_CHECK_THROW(scorer->CalculateScore(0, bb_list.size(),  0), promod3::Error);
  BOOST_CHECK_THROW(scorer->CalculateScore(-1, bb_list.size(),  0), promod3::Error);
  BOOST_CHECK_THROW(scorer->CalculateScore(1000, bb_list.size(),  0), promod3::Error);
  // the first resnum, where the bb_list doesn't fit anymore...
  BOOST_CHECK_THROW(scorer->CalculateScore(255, bb_list.size(), 0), promod3::Error);

  // check that empty BB list gives score of 0 (always)
  promod3::loop::BackboneList sub_bb_list;
  BOOST_CHECK_EQUAL(scorer->CalculateScore(214, sub_bb_list.size(), 0), Real(0));
  // check that border cases don't break
  if (bb_list.size() > 1) {
    sub_bb_list = bb_list.Extract(0, 1);
    BOOST_CHECK_NO_THROW(scorer->CalculateScore(214, sub_bb_list.size(), 0));
    sub_bb_list = bb_list.Extract(0, 2);
    BOOST_CHECK_NO_THROW(scorer->CalculateScore(214, sub_bb_list.size(), 0));
  }

  std::vector<uint> start_resnum(1);
  std::vector<uint> chain_idx(1);
  std::vector<uint> num_residues(1);
  num_residues[0] = bb_list.size();

  // test check of chain_idx
  start_resnum[0] = 214;
  chain_idx[0] = -1;
  BOOST_CHECK_THROW(scorer->CalculateScore(start_resnum, num_residues, chain_idx), 
                                           promod3::Error);
  chain_idx[0] = 1;
  BOOST_CHECK_THROW(scorer->CalculateScore(start_resnum, num_residues, chain_idx), 
                                           promod3::Error);

  // test check of start resnum
  chain_idx[0] = 0;
  start_resnum[0] = 0;
  BOOST_CHECK_THROW(scorer->CalculateScore(start_resnum, num_residues, chain_idx), 
                                           promod3::Error); 

  start_resnum[0] = -1;
  BOOST_CHECK_THROW(scorer->CalculateScore(start_resnum, num_residues, chain_idx), 
                                           promod3::Error);  

  start_resnum[0] = 1000;
  BOOST_CHECK_THROW(scorer->CalculateScore(start_resnum, num_residues, chain_idx), 
                                           promod3::Error);  

  start_resnum[0] = 255;
  // the first resnum, where the bb_list doesn't fit anymore...
  BOOST_CHECK_THROW(scorer->CalculateScore(start_resnum, num_residues, chain_idx), 
                                           promod3::Error);
}

// check overall scorer on a single score
void SanityCheckOverallScorer(BackboneOverallScorer& scorer,
                              BackboneScorerPtr single_scorer,
                              const promod3::loop::BackboneList& bb_list,
                              const String& key) {
  // ref = single score
  Real ref_score = single_scorer->CalculateScore(214, bb_list.size(), 0);
  Real score = scorer.Calculate(key, 214, bb_list.size(), 0);
  BOOST_CHECK_CLOSE(score, ref_score, Real(1e-3));
  std::map<String, Real> weights;
  weights[key] = 1.0;
  score = scorer.CalculateLinearCombination(weights, 214, bb_list.size(), 0);
  BOOST_CHECK_CLOSE(score, ref_score, Real(1e-3));
}

// attach single scorer to overall one and check it
template <typename ScorerPtr>
void AttachScorer(BackboneOverallScorer& scorer, ScorerPtr single_scorer,
                  const promod3::loop::BackboneList& bb_list,
                  BackboneScoreEnv& env, const String& key) {
  single_scorer->AttachEnvironment(env);
  scorer[key] = single_scorer;
  SanityCheckOverallScorer(scorer, single_scorer, bb_list, key);
}

// Use extras/code_generation/backbone_score_unittest.py
// to obtain the test values.
// orig means: the current environment
// reset means: 8 residues starting from resnum 26 are replaced by a helix as 
//              it is constructed in the default constructor of the
//              BackboneList. Positioning is performed by superposing first
//              helix residue on current residue 26
// deleted means: additionally to the reset, 6 residues starting from resnum
//                83 are removed from the environment      
void GetOrigScores(std::vector<Real>& orig_cb_packing,
                   std::vector<Real>& orig_cb,
                   std::vector<Real>& orig_reduced,
                   std::vector<Real>& orig_torsion,
                   std::vector<Real>& orig_hbond,
                   std::vector<Real>& orig_ss_agreement) {
  orig_cb_packing.push_back(-1.0796);
  orig_cb_packing.push_back(-0.5889);
  orig_cb_packing.push_back(-0.8458);
  orig_cb_packing.push_back(-0.9514);
  orig_cb_packing.push_back(-0.6117);
  orig_cb_packing.push_back(-0.7100);
  orig_cb_packing.push_back(-0.9639);
  orig_cb_packing.push_back(-0.5944);
  orig_cb_packing.push_back(-0.5149);
  orig_cb_packing.push_back(-1.0095);
  orig_cb_packing.push_back(-0.8998);
  orig_cb_packing.push_back(-0.9830);
  orig_cb_packing.push_back(-0.4551);
  orig_cb_packing.push_back(-0.6673);

  orig_cb.push_back(-0.3420);
  orig_cb.push_back(0.2386);
  orig_cb.push_back(-0.4104);
  orig_cb.push_back(0.3401);
  orig_cb.push_back(-0.3413);
  orig_cb.push_back(-0.0710);
  orig_cb.push_back(-0.0911);
  orig_cb.push_back(-0.0182);
  orig_cb.push_back(0.8085);
  orig_cb.push_back(0.4612);
  orig_cb.push_back(0.1394);
  orig_cb.push_back(-0.4826);
  orig_cb.push_back(-0.4971);
  orig_cb.push_back(0.0492);

  orig_reduced.push_back(-3.5540);
  orig_reduced.push_back(0.5257);
  orig_reduced.push_back(-5.0972);
  orig_reduced.push_back(-5.4569);
  orig_reduced.push_back(-1.8684);
  orig_reduced.push_back(-0.9923);
  orig_reduced.push_back(0.5434);
  orig_reduced.push_back(-3.6227);
  orig_reduced.push_back(-0.2690);
  orig_reduced.push_back(-4.1679);
  orig_reduced.push_back(-2.8972);
  orig_reduced.push_back(-4.3749);
  orig_reduced.push_back(-7.2311);
  orig_reduced.push_back(-10.5035);

  orig_torsion.push_back(0.4922);
  orig_torsion.push_back(0.4896);
  orig_torsion.push_back(0.9574);
  orig_torsion.push_back(-0.4831);
  orig_torsion.push_back(-0.5004);
  orig_torsion.push_back(-0.4875);
  orig_torsion.push_back(-0.4739);
  orig_torsion.push_back(-0.7882);
  orig_torsion.push_back(0.3369);
  orig_torsion.push_back(-0.8233);
  orig_torsion.push_back(-0.4457);
  orig_torsion.push_back(-0.4084);
  orig_torsion.push_back(-1.1914);
  orig_torsion.push_back(-1.1170);

  orig_hbond.push_back(-1.5817);
  orig_hbond.push_back(-1.4568);
  orig_hbond.push_back(-2.7138);
  orig_hbond.push_back(-2.9179);
  orig_hbond.push_back(-1.4514);
  orig_hbond.push_back(-1.1016);
  orig_hbond.push_back(-2.6790);
  orig_hbond.push_back(-0.4062);
  orig_hbond.push_back(-0.9520);
  orig_hbond.push_back(-0.9263);
  orig_hbond.push_back(0.0000);
  orig_hbond.push_back(-1.6571);
  orig_hbond.push_back(-1.0162);
  orig_hbond.push_back(-1.7538);

  orig_ss_agreement.push_back(1.0960);
  orig_ss_agreement.push_back(1.0960);
  orig_ss_agreement.push_back(1.0960);
  orig_ss_agreement.push_back(0.9231);
  orig_ss_agreement.push_back(0.9231);
  orig_ss_agreement.push_back(0.6663);
  orig_ss_agreement.push_back(0.2756);
  orig_ss_agreement.push_back(0.4928);
  orig_ss_agreement.push_back(0.7852);
  orig_ss_agreement.push_back(0.7852);
  orig_ss_agreement.push_back(-0.9896);
  orig_ss_agreement.push_back(-0.1106);
  orig_ss_agreement.push_back(1.3440);
  orig_ss_agreement.push_back(1.4422);
}



void GetResetScores(std::vector<Real>& reset_cb_packing,
                    std::vector<Real>& reset_cb,
                    std::vector<Real>& reset_reduced,
                    std::vector<Real>& reset_torsion,
                    std::vector<Real>& reset_hbond,
                    std::vector<Real>& reset_ss_agreement) {
  reset_cb_packing.push_back(-1.0796);
  reset_cb_packing.push_back(-0.4539);
  reset_cb_packing.push_back(-0.8458);
  reset_cb_packing.push_back(-0.5642);
  reset_cb_packing.push_back(-0.6117);
  reset_cb_packing.push_back(-0.7100);
  reset_cb_packing.push_back(-0.9639);
  reset_cb_packing.push_back(-0.9962);
  reset_cb_packing.push_back(-0.5149);
  reset_cb_packing.push_back(-1.0095);
  reset_cb_packing.push_back(-0.8998);
  reset_cb_packing.push_back(-0.9830);
  reset_cb_packing.push_back(-0.4551);
  reset_cb_packing.push_back(-0.6673);

  reset_cb.push_back(-0.2408);
  reset_cb.push_back(0.2206);
  reset_cb.push_back(-0.2013);
  reset_cb.push_back(-0.1437);
  reset_cb.push_back(-0.2173);
  reset_cb.push_back(0.0305);
  reset_cb.push_back(-0.0712);
  reset_cb.push_back(0.2034);
  reset_cb.push_back(0.2860);
  reset_cb.push_back(0.3605);
  reset_cb.push_back(0.0993);
  reset_cb.push_back(-0.3388);
  reset_cb.push_back(-0.4971);
  reset_cb.push_back(0.0492);

  reset_reduced.push_back(-3.3744);
  reset_reduced.push_back(0.4029);
  reset_reduced.push_back(-2.7685);
  reset_reduced.push_back(-4.2790);
  reset_reduced.push_back(-0.0387);
  reset_reduced.push_back(0.2206);
  reset_reduced.push_back(0.4623);
  reset_reduced.push_back(0.0012);
  reset_reduced.push_back(-1.0183);
  reset_reduced.push_back(-1.7337);
  reset_reduced.push_back(-2.8330);
  reset_reduced.push_back(-4.2875);
  reset_reduced.push_back(-7.2311);
  reset_reduced.push_back(-10.5035);

  reset_torsion.push_back(1.0555);
  reset_torsion.push_back(-0.0690);
  reset_torsion.push_back(0.9574);
  reset_torsion.push_back(-0.3147);
  reset_torsion.push_back(-0.8379);
  reset_torsion.push_back(-0.2807);
  reset_torsion.push_back(-0.1003);
  reset_torsion.push_back(4.7277);
  reset_torsion.push_back(0.1711);
  reset_torsion.push_back(-0.8233);
  reset_torsion.push_back(-0.4457);
  reset_torsion.push_back(-0.4084);
  reset_torsion.push_back(-1.1914);
  reset_torsion.push_back(-1.1170);

  reset_hbond.push_back(-1.5992);
  reset_hbond.push_back(-1.9072);
  reset_hbond.push_back(-3.1813);
  reset_hbond.push_back(-2.5196);
  reset_hbond.push_back(-2.9569);
  reset_hbond.push_back(-2.0498);
  reset_hbond.push_back(-1.6036);
  reset_hbond.push_back(-2.1579);
  reset_hbond.push_back(-0.1853);
  reset_hbond.push_back(-0.9263);
  reset_hbond.push_back(0.0000);
  reset_hbond.push_back(-1.6571);
  reset_hbond.push_back(-1.0162);
  reset_hbond.push_back(-1.7538);

  reset_ss_agreement.push_back(1.0960);
  reset_ss_agreement.push_back(1.0960);
  reset_ss_agreement.push_back(1.0960);
  reset_ss_agreement.push_back(0.9231);
  reset_ss_agreement.push_back(0.9231);
  reset_ss_agreement.push_back(0.6663);
  reset_ss_agreement.push_back(-0.7839);
  reset_ss_agreement.push_back(0.5699);
  reset_ss_agreement.push_back(0.7852);
  reset_ss_agreement.push_back(0.7852);
  reset_ss_agreement.push_back(-0.9896);
  reset_ss_agreement.push_back(-0.1106);
  reset_ss_agreement.push_back(1.3440);
  reset_ss_agreement.push_back(1.4422);
}




void GetDeletedScores(std::vector<Real>& deleted_cb_packing,
                      std::vector<Real>& deleted_cb,
                      std::vector<Real>& deleted_reduced,
                      std::vector<Real>& deleted_torsion,
                      std::vector<Real>& deleted_hbond,
                      std::vector<Real>& deleted_ss_agreement) {
  deleted_cb_packing.push_back(-1.0208);
  deleted_cb_packing.push_back(-0.5889);
  deleted_cb_packing.push_back(-0.8458);
  deleted_cb_packing.push_back(-0.9514);
  deleted_cb_packing.push_back(-1.0485);
  deleted_cb_packing.push_back(-0.7100);
  deleted_cb_packing.push_back(-0.9639);
  deleted_cb_packing.push_back(-0.9962);
  deleted_cb_packing.push_back(-0.5149);
  deleted_cb_packing.push_back(-0.4627);
  deleted_cb_packing.push_back(0.0666);
  deleted_cb_packing.push_back(-0.6760);
  deleted_cb_packing.push_back(-0.7296);
  deleted_cb_packing.push_back(-0.9996);

  deleted_cb.push_back(-0.2917);
  deleted_cb.push_back(0.2832);
  deleted_cb.push_back(-0.2013);
  deleted_cb.push_back(-0.0520);
  deleted_cb.push_back(-0.1806);
  deleted_cb.push_back(0.0305);
  deleted_cb.push_back(-0.0712);
  deleted_cb.push_back(0.2034);
  deleted_cb.push_back(0.2911);
  deleted_cb.push_back(0.1522);
  deleted_cb.push_back(-0.1108);
  deleted_cb.push_back(-0.3108);
  deleted_cb.push_back(-0.7100);
  deleted_cb.push_back(0.1760);

  deleted_reduced.push_back(-1.9385);
  deleted_reduced.push_back(0.7795);
  deleted_reduced.push_back(-2.7685);
  deleted_reduced.push_back(-3.7826);
  deleted_reduced.push_back(-0.2241);
  deleted_reduced.push_back(0.2206);
  deleted_reduced.push_back(0.4623);
  deleted_reduced.push_back(0.0012);
  deleted_reduced.push_back(-0.7117);
  deleted_reduced.push_back(-0.1598);
  deleted_reduced.push_back(-0.1166);
  deleted_reduced.push_back(-3.1746);
  deleted_reduced.push_back(-5.6337);
  deleted_reduced.push_back(-6.7428);

  deleted_torsion.push_back(1.0555);
  deleted_torsion.push_back(-0.0690);
  deleted_torsion.push_back(0.9574);
  deleted_torsion.push_back(-0.3147);
  deleted_torsion.push_back(-0.8379);
  deleted_torsion.push_back(-0.2807);
  deleted_torsion.push_back(-0.1003);
  deleted_torsion.push_back(4.7277);
  deleted_torsion.push_back(0.1711);
  deleted_torsion.push_back(-0.8233);
  deleted_torsion.push_back(-0.4457);
  deleted_torsion.push_back(-0.4084);
  deleted_torsion.push_back(-1.1914);
  deleted_torsion.push_back(-1.1170);

  deleted_hbond.push_back(-1.5992);
  deleted_hbond.push_back(-1.9072);
  deleted_hbond.push_back(-3.1813);
  deleted_hbond.push_back(-2.5196);
  deleted_hbond.push_back(-2.9569);
  deleted_hbond.push_back(-2.0498);
  deleted_hbond.push_back(-1.6036);
  deleted_hbond.push_back(-2.1579);
  deleted_hbond.push_back(-0.1853);
  deleted_hbond.push_back(0.0000);
  deleted_hbond.push_back(0.0000);
  deleted_hbond.push_back(0.0000);
  deleted_hbond.push_back(-1.0162);
  deleted_hbond.push_back(0.0000);

  deleted_ss_agreement.push_back(1.0960);
  deleted_ss_agreement.push_back(1.0960);
  deleted_ss_agreement.push_back(1.0960);
  deleted_ss_agreement.push_back(0.9231);
  deleted_ss_agreement.push_back(0.9231);
  deleted_ss_agreement.push_back(0.6663);
  deleted_ss_agreement.push_back(-0.7839);
  deleted_ss_agreement.push_back(0.5699);
  deleted_ss_agreement.push_back(0.7852);
  deleted_ss_agreement.push_back(0.7852);
  deleted_ss_agreement.push_back(0.7295);
  deleted_ss_agreement.push_back(0.3536);
  deleted_ss_agreement.push_back(-0.9247);
  deleted_ss_agreement.push_back(1.4422);

}



} // anon ns


BOOST_AUTO_TEST_CASE(qmean_comparison) {
  // Test all terms, that have an equivalent in qmean.
  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList("data/3DEFA.pdb",
                                                           26, 40);

  CBPackingScorerPtr cb_packing_scorer = LoadCBPackingScorer();
  CBetaScorerPtr cb_scorer = LoadCBetaScorer();
  ReducedScorerPtr reduced_scorer = LoadReducedScorer();
  HBondScorerPtr hbond_scorer = LoadHBondScorer();
  SSAgreementScorerPtr ss_agreement_scorer = LoadSSAgreementScorer();
  SetPsipredPrediction(env);
  TorsionScorerPtr torsion_scorer = LoadTorsionScorer();

  cb_packing_scorer->AttachEnvironment(env);
  cb_scorer->AttachEnvironment(env);
  reduced_scorer->AttachEnvironment(env);
  hbond_scorer->AttachEnvironment(env);
  ss_agreement_scorer->AttachEnvironment(env);
  torsion_scorer->AttachEnvironment(env);

  CheckScorerThrows(bb_list, cb_packing_scorer);
  CheckScorerThrows(bb_list, cb_scorer);
  CheckScorerThrows(bb_list, reduced_scorer);
  CheckScorerThrows(bb_list, hbond_scorer);
  CheckScorerThrows(bb_list, ss_agreement_scorer);
  CheckScorerThrows(bb_list, torsion_scorer);

  // these are the expected score profile values as generated with
  // the python script mentioned above

  std::vector<Real> orig_cb_packing;
  std::vector<Real> orig_cb;
  std::vector<Real> orig_reduced;
  std::vector<Real> orig_torsion;
  std::vector<Real> orig_hbond;
  std::vector<Real> orig_ss_agreement;
  GetOrigScores(orig_cb_packing,
                orig_cb,
                orig_reduced,
                orig_torsion,
                orig_hbond,
                orig_ss_agreement);

  std::vector<Real> reset_cb_packing;
  std::vector<Real> reset_cb;
  std::vector<Real> reset_reduced;
  std::vector<Real> reset_torsion;
  std::vector<Real> reset_hbond;
  std::vector<Real> reset_ss_agreement;
  GetResetScores(reset_cb_packing,
                 reset_cb,
                 reset_reduced,
                 reset_torsion,
                 reset_hbond,
                 reset_ss_agreement);

  std::vector<Real> deleted_cb_packing;
  std::vector<Real> deleted_cb;
  std::vector<Real> deleted_reduced;
  std::vector<Real> deleted_torsion;
  std::vector<Real> deleted_hbond;
  std::vector<Real> deleted_ss_agreement;
  GetDeletedScores(deleted_cb_packing,
                   deleted_cb,
                   deleted_reduced,
                   deleted_torsion,
                   deleted_hbond,
                   deleted_ss_agreement);

  
  std::vector<Real> cb_packing_profile;
  std::vector<Real> cb_profile;
  std::vector<Real> reduced_profile;
  std::vector<Real> torsion_profile;
  std::vector<Real> hbond_profile;
  std::vector<Real> ss_agreement_profile;

  // compare the orig values
  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);
  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);

  Real eps = 0.0001;
  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(orig_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }

  // lets perform the described reset action
  String helix_sequence = "FGKLKQKD";
  std::vector<Real> phi_angles(helix_sequence.size(), -1);
  std::vector<Real> psi_angles(helix_sequence.size(), -0.75);
  promod3::loop::BackboneList bb_list_helix = 
    promod3::loop::BackboneList(helix_sequence, phi_angles, psi_angles);
  //
  geom::Mat4 transform = bb_list_helix.GetTransform(0, bb_list, 0);
  bb_list_helix.ApplyTransform(transform);
  env.SetEnvironment(bb_list_helix, 26);
  bb_list.ReplaceFragment(bb_list_helix, 0, false);

  // compare the reset values
  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);

  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);


  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(reset_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }

  // lets perform the described delete action
  env.ClearEnvironment(83, 6);

  // compare the delete values
  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);
  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);


  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(deleted_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(deleted_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(deleted_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(deleted_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(deleted_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(deleted_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }                                  
}

BOOST_AUTO_TEST_CASE(test_env_stashing) {

  // Same test as above, but this time we use some stash / pop
  // magic from the BackboneScoreEnv
  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList("data/3DEFA.pdb",
                                                           26, 40);

  CBPackingScorerPtr cb_packing_scorer = LoadCBPackingScorer();
  CBetaScorerPtr cb_scorer = LoadCBetaScorer();
  ReducedScorerPtr reduced_scorer = LoadReducedScorer();
  HBondScorerPtr hbond_scorer = LoadHBondScorer();
  SSAgreementScorerPtr ss_agreement_scorer = LoadSSAgreementScorer();
  SetPsipredPrediction(env);
  TorsionScorerPtr torsion_scorer = LoadTorsionScorer();

  cb_packing_scorer->AttachEnvironment(env);
  cb_scorer->AttachEnvironment(env);
  reduced_scorer->AttachEnvironment(env);
  hbond_scorer->AttachEnvironment(env);
  ss_agreement_scorer->AttachEnvironment(env);
  torsion_scorer->AttachEnvironment(env);

  std::vector<Real> orig_cb_packing;
  std::vector<Real> orig_cb;
  std::vector<Real> orig_reduced;
  std::vector<Real> orig_torsion;
  std::vector<Real> orig_hbond;
  std::vector<Real> orig_ss_agreement;
  GetOrigScores(orig_cb_packing,
                orig_cb,
                orig_reduced,
                orig_torsion,
                orig_hbond,
                orig_ss_agreement);

  std::vector<Real> reset_cb_packing;
  std::vector<Real> reset_cb;
  std::vector<Real> reset_reduced;
  std::vector<Real> reset_torsion;
  std::vector<Real> reset_hbond;
  std::vector<Real> reset_ss_agreement;
  GetResetScores(reset_cb_packing,
                 reset_cb,
                 reset_reduced,
                 reset_torsion,
                 reset_hbond,
                 reset_ss_agreement);
  
  std::vector<Real> cb_packing_profile;
  std::vector<Real> cb_profile;
  std::vector<Real> reduced_profile;
  std::vector<Real> torsion_profile;
  std::vector<Real> hbond_profile;
  std::vector<Real> ss_agreement_profile;

  // compare the orig values
  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);
  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);

  Real eps = 0.0001;
  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(orig_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }


  //////////////////////////////////////////////////////////////////////////
  // PERFORM A STASH OPERATION AND CHECK, WHETHER THE SCORING RESULTS ARE //
  // STILL THE SAME                                                       //
  //////////////////////////////////////////////////////////////////////////

  env.Stash(26, bb_list.size(), 0);

  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);
  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);

  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(orig_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }

  // lets perform the described reset action
  String helix_sequence = "FGKLKQKD";
  std::vector<Real> phi_angles(helix_sequence.size(), -1);
  std::vector<Real> psi_angles(helix_sequence.size(), -0.75);
  promod3::loop::BackboneList bb_list_helix = 
    promod3::loop::BackboneList(helix_sequence, phi_angles, psi_angles);
  //
  geom::Mat4 transform = bb_list_helix.GetTransform(0, bb_list, 0);
  bb_list_helix.ApplyTransform(transform);
  env.SetEnvironment(bb_list_helix, 26);
  bb_list.ReplaceFragment(bb_list_helix, 0, false);

  // compare the reset values
  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);

  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);


  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(reset_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(reset_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }

  ////////////////////////////////////////////////////////////////////////////
  // WE PERFORM A POP ACTION AND CHECK WHETHER THE SCORES CORRESPOND TO THE //
  // ORIGINAL SCORES AGAIN                                                  //
  ////////////////////////////////////////////////////////////////////////////

  env.Pop();

  cb_packing_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                           cb_packing_profile);
  cb_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                   cb_profile);
  reduced_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        reduced_profile);
  torsion_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                        torsion_profile);
  hbond_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                      hbond_profile);
  ss_agreement_scorer->CalculateScoreProfile(26, bb_list.size(), 0, 
                                             ss_agreement_profile);

  for(uint i = 0; i < bb_list.size(); ++i) {
    BOOST_CHECK_SMALL(std::abs(orig_cb_packing[i] - 
                               cb_packing_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_cb[i] - 
                               cb_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_reduced[i] - 
                               reduced_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_torsion[i] - 
                               torsion_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_hbond[i] - 
                               hbond_profile[i]), eps);
    BOOST_CHECK_SMALL(std::abs(orig_ss_agreement[i] - 
                               ss_agreement_profile[i]), eps);
  }
}

BOOST_AUTO_TEST_CASE(test_clash_score) {
  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList();
  ClashScorerPtr scorer(new ClashScorer);
  scorer->AttachEnvironment(env);
  //CheckScorer(bb_list, scorer, 0.0);
  CheckScorerThrows(bb_list, scorer);
}

BOOST_AUTO_TEST_CASE(test_clash_score2) {
  // check sthg with actual clashes
  String pdb_name = "data/clash_test.pdb";
  BackboneScoreEnv env = GenerateBackboneEnv(pdb_name, "");
  promod3::loop::BackboneList bb_list = GenerateTestBBList(pdb_name, 35, 40);
  ClashScorerPtr scorer(new ClashScorer);
  scorer->AttachEnvironment(env);
  Real clash_score = scorer->CalculateScore(35, bb_list.size(), 0);
  BOOST_CHECK_CLOSE(clash_score, 0.107386872172, Real(1e-3));

  // test for normalization, additionally to the one in test_normalize
  scorer->DoNormalize(false);
  clash_score = scorer->CalculateScore(35, bb_list.size(), 0);
  BOOST_CHECK_CLOSE(clash_score, bb_list.size() * 0.107386872172, Real(1e-3)); 
}

BOOST_AUTO_TEST_CASE(test_pairwise_score) {

  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList();
  PairwiseScorerPtr scorer(new PairwiseScorer);
  scorer->AttachEnvironment(env);
  //CheckScorer(bb_list, scorer, 0.0);
  CheckScorerThrows(bb_list, scorer);
  
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 0.0,
                    Real(1e-3));


  std::vector<Real> values_one(10,0.0);
  std::vector<Real> values_two(10,0.0);

  //ca specific interaction from SER214 to LYS221
  values_one[5] = 5.0;
  //cb specific interaction from SER214 to LYS221
  values_two[7] = 10.0;
  
  ConstraintFunctionPtr constraint_one(new ConstraintFunction(0.0,10.0,
                                                              values_one));

  ConstraintFunctionPtr constraint_two(new ConstraintFunction(0.0,10.0,
                                                              values_two));

  env.AddPairwiseFunction(constraint_one, CA_PAIRWISE_FUNCTION);
  env.AddPairwiseFunction(constraint_two, CB_PAIRWISE_FUNCTION);

  env.ApplyPairwiseFunction(0, 214, 0, 122, 0); //they are too far apart
                                                //scorer should give 0.0

  //CheckScorer(bb_list, scorer, 0.0);  
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 0.0,
                    Real(1e-3));


  env.ApplyPairwiseFunction(0, 214, 0, 221, 0); //ca_specific interaction for
                                                //residues 214 and 221,
                                                //scorer should now have a
                                                //nonzero value


  //CheckScorer(bb_list, scorer, 0.36586231);
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 0.36586231,
                    Real(1e-3));


  env.ApplyPairwiseFunction(0, 214, 0, 221, 1); //cb_specific interaction, 
                                                //that should also have an influence

  //CheckScorer(bb_list, scorer, 1.12155807);
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 1.12155807,
                    Real(1e-3));                                                         

  //let's finally try to add a contact function
  ContactFunctionPtr contact(new ContactFunction(6.0, 50.0));
  env.AddPairwiseFunction(contact, CA_PAIRWISE_FUNCTION);
  env.AddPairwiseFunction(contact, CB_PAIRWISE_FUNCTION);
  env.ApplyPairwiseFunction(0,216,0,230,2); //the ca positions of residues 216
                                            //and 230 are too far apart, the
                                            //score should therefore not change
                                            //compared to previous check 

  //CheckScorer(bb_list, scorer, 1.12155807);
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 1.12155807,
                    Real(1e-3));  


  env.ApplyPairwiseFunction(0,216,0,228,2); //the ca positions of residues 216
                                            //and 228 are just above 6A, the contact
                                            //therefore doesn't influence the score
 
  //CheckScorer(bb_list, scorer, 1.12155807);
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 1.12155807,
                    Real(1e-3));  

  env.ApplyPairwiseFunction(0,216,0,228,3); //the cb positions of residues 216
                                            //and 228 are within 6A, the contact
                                            //therefore influences the score

  //CheckScorer(bb_list, scorer, 6.67711401);
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 6.67711401,
                    Real(1e-3));    

  // test for normalization, additionally to the one in test_normalize
  scorer->DoNormalize(false);
  BOOST_CHECK_CLOSE(scorer->CalculateScore(214, bb_list.size(), 0), 
                    bb_list.size() * 6.67711401, Real(1e-3));    
}

BOOST_AUTO_TEST_CASE(test_density_score){

  ost::mol::EntityHandle ent = LoadTestStructure("data/3DEFA.pdb");
  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList();

  ost::mol::ResidueHandleList full_res_list = ent.GetResidueList();
  String full_seqres(full_res_list.size(),'-');
  for(uint i = 0; i < full_res_list.size(); ++i){
    full_seqres[i] = full_res_list[i].GetOneLetterCode();
  } 
  promod3::loop::BackboneList full_bb_list(full_seqres, full_res_list);

  ost::img::MapHandle map = full_bb_list.ToDensity(10.0,geom::Vec3(1.0,1.0,1.0),
                                                   5.0, true);

  BOOST_CHECK_CLOSE(promod3::scoring::DensityScore(bb_list, map, 5.0, true),
                                                   0.352861583, Real(1e-3));
}

BOOST_AUTO_TEST_CASE(test_overall_scorer) {
  // setup env
  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList();
  SetPsipredPrediction(env);

  // setup scorer
  BackboneOverallScorer scorer;
  // setup weights
  std::map<String, Real> weights;
  weights["cb_packing"] = 0.1;
  weights["cbeta"] = 0.2;
  weights["reduced"] = 0.3;
  weights["clash"] = 0.4;
  weights["hbond"] = 0.5;
  weights["ss_agreement"] = 0.6;
  weights["torsion"] = 0.7;
  weights["intercept"] = 1.0;
  // check for errors
  BOOST_CHECK_THROW(scorer.Calculate("hbond", 214, bb_list.size(), 0), promod3::Error);
  BOOST_CHECK_THROW(scorer.CalculateLinearCombination(weights, 214, bb_list.size(), 0),
                    promod3::Error);

  // attach and sanity check single scores
  AttachScorer(scorer, LoadCBPackingScorer(), bb_list, env, "cb_packing");
  AttachScorer(scorer, LoadCBetaScorer(), bb_list, env, "cbeta");
  AttachScorer(scorer, LoadReducedScorer(), bb_list, env, "reduced");
  AttachScorer(scorer, ClashScorerPtr(new ClashScorer), bb_list, env, "clash");
  AttachScorer(scorer, LoadHBondScorer(), bb_list, env, "hbond");
  AttachScorer(scorer, LoadSSAgreementScorer(), bb_list, env, "ss_agreement");
  AttachScorer(scorer, LoadTorsionScorer(), bb_list, env, "torsion");

  // check final score// exp. score from single ones above
  Real exp_score = 0.1 * -0.657944619656 + 0.2 * 0.0189835
                 + 0.3 * -0.64918 + 0.4 * 0.0 + 0.5 * -0.482397
                 + 0.6 * 0.498321 + 0.7 * -0.143922114538 + 1.0;
  BOOST_CHECK_CLOSE(scorer.CalculateLinearCombination(weights, 214, bb_list.size(), 0),
                    exp_score, Real(1e-3));
  // check for more errors
  BOOST_CHECK_THROW(scorer.Calculate("meh", 214, bb_list.size(), 0), promod3::Error);
}


BOOST_AUTO_TEST_CASE(test_normalize) {

  // The normalization of the pairwise scorer is tested in the
  // pairwise specific function where we've set actual constraints
  // Also in case of the clash score, a test has been added there
  // since zero is not necessarily an ideal score to test normalization.

  // setup env
  BackboneScoreEnv env = GenerateBackboneEnv();
  promod3::loop::BackboneList bb_list = GenerateTestBBList();
  SetPsipredPrediction(env);

  // setup scorer
  BackboneOverallScorer scorer;
  // setup weights
  std::map<String, Real> weights;
  weights["cb_packing"] = 1.0;
  weights["cbeta"] = 1.0;
  weights["reduced"] = 1.0;
  weights["clash"] = 1.0;
  weights["hbond"] = 1.0;
  weights["ss_agreement"] = 1.0;
  weights["torsion"] = 1.0;

  // attach and sanity check single scores

  // load scorers
  CBPackingScorerPtr cb_packing_scorer = LoadCBPackingScorer();
  CBetaScorerPtr cbeta_scorer = LoadCBetaScorer();
  ReducedScorerPtr reduced_scorer = LoadReducedScorer();
  ClashScorerPtr clash_scorer = ClashScorerPtr(new ClashScorer);
  HBondScorerPtr hbond_scorer = LoadHBondScorer();
  SSAgreementScorerPtr ss_agreement_scorer = LoadSSAgreementScorer();
  TorsionScorerPtr torsion_scorer = LoadTorsionScorer();

  AttachScorer(scorer, cb_packing_scorer, bb_list, env, "cb_packing");
  AttachScorer(scorer, cbeta_scorer, bb_list, env, "cbeta");
  AttachScorer(scorer, reduced_scorer, bb_list, env, "reduced");
  AttachScorer(scorer, clash_scorer, bb_list, env, "clash");
  AttachScorer(scorer, hbond_scorer, bb_list, env, "hbond");
  AttachScorer(scorer, ss_agreement_scorer, bb_list, env, "ss_agreement");
  AttachScorer(scorer, torsion_scorer, bb_list, env, "torsion");

  // check final score// exp. score from single ones above
  Real exp_score = - 0.657944619656 + 0.0189835
                   - 0.64918 + 0.0 - 0.482397
                   + 0.498321 - 0.143922114538;

  BOOST_CHECK_CLOSE(scorer.CalculateLinearCombination(weights, 214, bb_list.size(), 0),
                    exp_score, Real(1e-3));

  cb_packing_scorer->DoNormalize(false);
  cbeta_scorer->DoNormalize(false);
  reduced_scorer->DoNormalize(false);
  clash_scorer->DoNormalize(false);
  hbond_scorer->DoNormalize(false);
  ss_agreement_scorer->DoNormalize(false);
  torsion_scorer->DoNormalize(false);

  exp_score *= bb_list.size();

  BOOST_CHECK_CLOSE(scorer.CalculateLinearCombination(weights, 214, bb_list.size(), 0),
                    exp_score, Real(1e-3));
}


BOOST_AUTO_TEST_CASE(test_internal_external) {

  // check sthg with actual clashes
  String pdb_name = "data/clash_test.pdb";
  BackboneScoreEnv env = GenerateBackboneEnv(pdb_name, "");
  promod3::loop::BackboneList bb_list = GenerateTestBBList(pdb_name, 32, 40);
  BackboneScoreEnv empty_env(env.GetSeqres());

  // load scorers (only the ones that provide an internal / external mechanism)
  CBetaScorerPtr cbeta_scorer = LoadCBetaScorer();
  ReducedScorerPtr reduced_scorer = LoadReducedScorer();
  ClashScorerPtr clash_scorer = ClashScorerPtr(new ClashScorer);
  HBondScorerPtr hbond_scorer = LoadHBondScorer();
  cbeta_scorer->AttachEnvironment(env);
  reduced_scorer->AttachEnvironment(env);
  clash_scorer->AttachEnvironment(env);
  hbond_scorer->AttachEnvironment(env);

  // lets do the overall score... this should be well tested by now
  Real cbeta_score = cbeta_scorer->CalculateScore(32, bb_list.size(), 0);
  Real reduced_score = reduced_scorer->CalculateScore(32, bb_list.size(), 0);
  Real clash_score = clash_scorer->CalculateScore(32, bb_list.size(), 0);
  Real hbond_score = hbond_scorer->CalculateScore(32, bb_list.size(), 0);

  // lets do the internal scores... except clash, they should all be non zero
  cbeta_scorer->DoExternalScores(false);
  reduced_scorer->DoExternalScores(false);
  clash_scorer->DoExternalScores(false);
  hbond_scorer->DoExternalScores(false);
  Real internal_cbeta_score = 
  cbeta_scorer->CalculateScore(32, bb_list.size(), 0);
  Real internal_reduced_score = 
  reduced_scorer->CalculateScore(32, bb_list.size(), 0);
  Real internal_clash_score = 
  clash_scorer->CalculateScore(32, bb_list.size(), 0);
  Real internal_hbond_score = 
  hbond_scorer->CalculateScore(32, bb_list.size(), 0);

  // lets do the external scores
  cbeta_scorer->DoExternalScores(true);
  reduced_scorer->DoExternalScores(true);
  clash_scorer->DoExternalScores(true);
  hbond_scorer->DoExternalScores(true);
  cbeta_scorer->DoInternalScores(false);
  reduced_scorer->DoInternalScores(false);
  clash_scorer->DoInternalScores(false);
  hbond_scorer->DoInternalScores(false);
  Real external_cbeta_score = 
  cbeta_scorer->CalculateScore(32, bb_list.size(), 0);
  Real external_reduced_score = 
  reduced_scorer->CalculateScore(32, bb_list.size(), 0);
  Real external_clash_score = 
  clash_scorer->CalculateScore(32, bb_list.size(), 0);
  Real external_hbond_score = 
  hbond_scorer->CalculateScore(32, bb_list.size(), 0);

  // the sum between the internal and external score must be the total score
  BOOST_CHECK_CLOSE(internal_cbeta_score + external_cbeta_score, 
                    cbeta_score, Real(1e-3));
  BOOST_CHECK_CLOSE(internal_reduced_score + external_reduced_score, 
                    reduced_score, Real(1e-3));
  BOOST_CHECK_CLOSE(internal_clash_score + external_clash_score, 
                    clash_score, Real(1e-3));
  BOOST_CHECK_CLOSE(internal_hbond_score + external_hbond_score, 
                    hbond_score, Real(1e-3));

  // we load new scorers that we attach to the empty env
  CBetaScorerPtr empty_cbeta_scorer = LoadCBetaScorer();
  ReducedScorerPtr empty_reduced_scorer = LoadReducedScorer();
  ClashScorerPtr empty_clash_scorer = ClashScorerPtr(new ClashScorer);
  HBondScorerPtr empty_hbond_scorer = LoadHBondScorer();
  empty_cbeta_scorer->AttachEnvironment(empty_env);
  empty_reduced_scorer->AttachEnvironment(empty_env);
  empty_clash_scorer->AttachEnvironment(empty_env);
  empty_hbond_scorer->AttachEnvironment(empty_env);

  // we do set the actual bb_list in the empty_env
  empty_env.SetEnvironment(bb_list, 32, 0);

  // if everything went well, the full scores should be equal to the previously
  // calculated internal scores
  BOOST_CHECK_CLOSE(empty_cbeta_scorer->CalculateScore(32, bb_list.size(), 0),
                    internal_cbeta_score, Real(1e-3));
  BOOST_CHECK_CLOSE(empty_reduced_scorer->CalculateScore(32, bb_list.size(), 0),
                    internal_reduced_score, Real(1e-3));
  BOOST_CHECK_CLOSE(empty_clash_scorer->CalculateScore(32, bb_list.size(), 0),
                    internal_clash_score, Real(1e-3));
  BOOST_CHECK_CLOSE(empty_hbond_scorer->CalculateScore(32, bb_list.size(), 0),
                    internal_hbond_score, Real(1e-3));
}


BOOST_AUTO_TEST_CASE(test_multiple_stretches) {

  // check sthg with actual clashes
  String pdb_name = "data/clash_test.pdb";
  BackboneScoreEnv env = GenerateBackboneEnv(pdb_name, "");
  promod3::loop::BackboneList bb_list = GenerateTestBBList(pdb_name, 32, 45);
  BackboneScoreEnv empty_env(env.GetSeqres());

  // load scorers
  CBPackingScorerPtr cb_packing_scorer = LoadCBPackingScorer();
  CBetaScorerPtr cbeta_scorer = LoadCBetaScorer();
  ReducedScorerPtr reduced_scorer = LoadReducedScorer();
  ClashScorerPtr clash_scorer = ClashScorerPtr(new ClashScorer);
  HBondScorerPtr hbond_scorer = LoadHBondScorer();
  SSAgreementScorerPtr ss_agreement_scorer = LoadSSAgreementScorer();
  TorsionScorerPtr torsion_scorer = LoadTorsionScorer();

  // setup scorer
  BackboneOverallScorer scorer;
  // setup weights
  std::map<String, Real> weights;
  weights["cb_packing"] = 1.0;
  weights["cbeta"] = 1.0;
  weights["reduced"] = 1.0;
  weights["clash"] = 1.0;
  weights["hbond"] = 1.0;
  weights["ss_agreement"] = 1.0;
  weights["torsion"] = 1.0;

  scorer["cb_packing"] = cb_packing_scorer;
  scorer["cbeta"] = cbeta_scorer;
  scorer["reduced"] = reduced_scorer;
  scorer["clash"] = clash_scorer;
  scorer["hbond"] = hbond_scorer;
  scorer["ss_agreement"] = ss_agreement_scorer;
  scorer["torsion"] = torsion_scorer;
  scorer.AttachEnvironment(env);
  
  std::vector<uint> start_resnums;
  std::vector<uint> stretch_lengths;
  std::vector<uint> chain_indices;
  uint first_stretch_length = 6;
  start_resnums.push_back(32);
  start_resnums.push_back(32 + first_stretch_length);
  stretch_lengths.push_back(first_stretch_length);
  stretch_lengths.push_back(bb_list.size() - first_stretch_length);
  chain_indices.push_back(0);
  chain_indices.push_back(0);

  Real full_score = scorer.CalculateLinearCombination(weights, 32, 
                                                      bb_list.size(), 0);
  Real patchy_score = scorer.CalculateLinearCombination(weights, start_resnums, 
                                                        stretch_lengths, 
                                                        chain_indices);

  BOOST_CHECK_CLOSE(full_score, patchy_score, Real(1e-3));
}

BOOST_AUTO_TEST_SUITE_END();
