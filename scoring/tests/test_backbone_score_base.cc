// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/scoring/backbone_score_base.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>
#include <ost/mol/builder.hh>
#include <ost/mol/xcs_editor.hh>
#include <cstdio>

BOOST_AUTO_TEST_SUITE( scoring );

using namespace promod3::scoring;

BOOST_AUTO_TEST_CASE(test_backbone_score_base_set_env){

  // load an entity to test...
  String pdb_name = "data/3DEFA.pdb";

  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  ost::seq::SequenceHandle seqres = ost::seq::CreateSequence("A","MGSLVREWVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQAEGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQVVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHVDAAALEHHHHHH");
  ost::seq::SequenceHandle invalid_seqres = ost::seq::CreateSequence("A","MUX");
  ost::seq::SequenceHandle wrong_seqres = ost::seq::CreateSequence("A","MGSLVAAAVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQAEGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQVVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHVDAAALEHHHHHH");

  ost::seq::SequenceList seqres_list = ost::seq::CreateSequenceList();
  seqres_list.AddSequence(seqres);
  ost::seq::SequenceList invalid_seqres_list = ost::seq::CreateSequenceList();
  invalid_seqres_list.AddSequence(invalid_seqres);
  ost::seq::SequenceList wrong_seqres_list = ost::seq::CreateSequenceList();
  wrong_seqres_list.AddSequence(wrong_seqres);

  // check whether the consistency checks run through properly...
  BackboneScoreEnv env(seqres_list);
  BOOST_CHECK_NO_THROW(env.SetInitialEnvironment(test_ent));
  BOOST_CHECK_THROW(BackboneScoreEnv tst(invalid_seqres_list), promod3::Error);
  BackboneScoreEnv wrong_env(wrong_seqres_list);
  BOOST_CHECK_THROW(wrong_env.SetInitialEnvironment(test_ent), promod3::Error);
  test_ent.EditXCS().RenumberAllResidues(42, true);
  BOOST_CHECK_THROW(env.SetInitialEnvironment(test_ent), promod3::Error);

  // same thing, this time with BackboneLists
  String bb_list_seq = "LVREWVGF";
  promod3::loop::BackboneList bb_list(bb_list_seq);
  BOOST_CHECK_NO_THROW(env.SetEnvironment(bb_list, 4, 0));
  BOOST_CHECK_THROW(env.SetEnvironment(bb_list, 5, 0), promod3::Error);
  BOOST_CHECK_THROW(wrong_env.SetEnvironment(bb_list, 4, 0), promod3::Error);
}

BOOST_AUTO_TEST_SUITE_END();
