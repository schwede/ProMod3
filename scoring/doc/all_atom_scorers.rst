..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


All Atom Scorers
================================================================================

.. currentmodule:: promod3.scoring


AllAtomOverallScorer class
--------------------------------------------------------------------------------

.. class:: AllAtomOverallScorer

  Container that allows for the storage of multiple scorers identified by a key
  (:class:`str`) and the computation of combined scores.

  Scorers need to be individually set or loaded by 
  :func:`LoadDefaultAllAtomOverallScorer()`

  .. method:: Contains(key)

    :return: True, if a scorer object for this key was already added.
    :rtype:  :class:`bool`
    :param key: Key for desired scorer.
    :type key:  :class:`str`

  .. method:: Get(key)

    :return: Scorer with the given *key* (read-only access).
    :rtype:  :class:`AllAtomScorer`
    :param key: Key for desired scorer.
    :type key:  :class:`str`
    :raises: :exc:`~exceptions.RuntimeError` if there is no scorer with that
             *key*.

  .. method:: AttachEnvironment(env)

    :param env: Link all scorers to this score environment.
    :type env:  :class:`~promod3.loop.AllAtomEnv`

  .. method:: CalculateLinearCombination(linear_weights, start_resnum, \
                                         num_residues, chain_idx)

    Calculate linear combination of scores for the desired loop(s) (extracted 
    from environment) against the set environment (see
    :meth:`AllAtomScorer.CalculateScore`).

    :param linear_weights: Weights for each desired scorer. You can add a
                           constant value to each score by defining a weight
                           with key "intercept".
    :type linear_weights:  :class:`dict` (keys: :class:`str`,
                           values: :class:`float`)
    :param start_resnum: Res. number(s) defining the position(s) in the SEQRES
                         (see :class:`~promod3.loop.AllAtomEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Length of loop(s).
    :type num_residues:  :class:`int` / :class:`list` of :class:`int`
    :param chain_idx: Index of chain the loop(s) belongs to
                      (see :class:`~promod3.loop.AllAtomEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Score for the given set of atoms.
    :rtype:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *linear_weights* has a *key* for
             which no scorer exists or anything raised in
             :meth:`AllAtomScorer.CalculateScore` for any of the used scorers.

  .. method:: __getitem__(key)
              __setitem__(key)

    Allow read/write access (with [*key*]) to scorer object with given key.

    :param key: Key for desired scorer.
    :type key:  :class:`str`

.. function:: LoadDefaultAllAtomOverallScorer()

  :returns: Loads or creates the default scorers accessible through 
            following keys:
            "aa_interaction", "aa_packing", "aa_clash"
  :rtype:   :class:`AllAtomOverallScorer`

AllAtomScorer base class
--------------------------------------------------------------------------------

.. class:: AllAtomScorer

  Base class for all the all atom scorers listed below.

  .. method:: AttachEnvironment(env)

    :param env: Link scorer to this score environment.
    :type env:  :class:`~promod3.loop.AllAtomEnv`

  .. method:: CalculateScore(start_resnum, num_residues, chain_idx)

    Calculates score for the desired loop(s) (extracted from environment) against
    the set environment. Unless otherwise noted in the scorer, a lower "score"
    is better!

    :param start_resnum: Res. number(s) defining the position(s) in the SEQRES
                         (see :class:`~promod3.loop.AllAtomEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Length of loop(s).
    :type num_residues:  :class:`int` / :class:`list` of :class:`int`
    :param chain_idx: Index of chain the loop(s) belongs to
                      (see :class:`~promod3.loop.AllAtomEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Score for the given set of atoms.
    :rtype:  :class:`float`

    :raises: (for most scorers) :exc:`~exceptions.RuntimeError` if scorer was
             never attached to a score environment, if scorer has never been
             properly initialized or if *chain_index* / *start_resnum* lead to
             invalid positions.

  .. method:: CalculateScoreProfile(start_resnum, num_residues, chain_idx=0)

    Calculates per residue scores for the desired loop(s) (extracted from
    environment) against the set environment.

    :param start_resnum: Res. number(s) defining the position(s) in the SEQRES
                         (see :class:`~promod3.loop.AllAtomEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Length of loop(s).
    :type num_residues:  :class:`int`
    :param chain_idx: Index of chain the loop(s) belongs to
                      (see :class:`~promod3.loop.AllAtomEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Scores for the given loop(s), one for each residue.
    :rtype:  :class:`list` of :class:`float` / :class:`list` of :class:`list` 
             of :class:`float`

    :raises: same :exc:`~exceptions.RuntimeError` as :meth:`CalculateScore`.


AllAtomInteractionScorer class
--------------------------------------------------------------------------------

.. class:: AllAtomInteractionScorer(cutoff, bins, seq_sep)

  Inherits all functionality of :class:`AllAtomScorer`. Evaluates a pairwise
  pseudo interaction energy between all atoms which are located within a
  *cutoff* and which are at least *seq_sep* residues apart. An energy is
  assigned to each distance using equally sized bins and distinguishing all
  possible pairs of atoms (usually the energy only differs for chemically
  distinguishable heavy atoms). 
  By default, the scorer calculates the scores by
  including everything, the stuff set in the environment and the coordinates
  in the input loops. You can change this behaviour with the according 
  functions. 

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadAllAtomInteractionScorer`) or by setting all energies (see :meth:`SetEnergy`).

  :param cutoff: Radius in which other atoms are counted.
  :type cutoff:  :class:`float`
  :param bins: Number of equally sized bins to discretize distances (range
               of [0,*cutoff*]).
  :type bins:  :class:`int`
  :param seq_sep: Minimal separation in sequence two residues must have to
                  be considered.
  :type seq_sep:  :class:`int`

  :raises: :exc:`~exceptions.RuntimeError` if *cutoff* < 0, *bins* < 1 or
           *seq_sep* < 1.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`AllAtomInteractionScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetEnergy(aaa1, aaa2, bin, energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every pair of heavy atom types and for every *bin* < *bins*.
    Internal symmetry is enforced => Calling SetEnergy(aaa1, aaa2, bin, energy) is
    equivalent to calling SetEnergy(aaa1, aaa2, bin, energy) AND
    SetEnergy(aaa2, aaa1, bin, energy).


    :param aaa1: Heavy atom type for first interaction partner.
    :type aaa1:  :class:`~promod3.loop.AminoAcidAtom`
    :param aaa2: Heavy atom type for second interaction partner.
    :type aaa2:  :class:`~promod3.loop.AminoAcidAtom`
    :param bin: Discrete bin describing the interaction distance.
    :type bin:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions within the 
                        loop. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions towards the 
                        environment. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues in the input loop. True by default.
    :type do_it:        :class:`bool`


.. function:: LoadAllAtomInteractionScorer()

  :returns: The default predefined AllAtomInteractionScorer (loaded from disk)
  :rtype:   :class:`AllAtomInteractionScorer`


AllAtomPackingScorer class
--------------------------------------------------------------------------------

.. class:: AllAtomPackingScorer(cutoff, max_count)

  Inherits all functionality of :class:`AllAtomScorer`. Evaluates pseudo
  energies by counting surrounding atoms within a certain *cutoff* radius around
  all heavy atoms not belonging to the assessed residue itself. 

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadAllAtomPackingScorer`) or by setting all energies (see
  :meth:`SetEnergy`).

  :param cutoff: Radius in which other atoms are counted.
  :type cutoff:  :class:`float`
  :param max_count: If number of other atoms exceeds *max_count*, it will
                    be set to this number.
  :type max_count:  :class:`int`

  :raises: :exc:`~exceptions.RuntimeError` if *cutoff* < 0, *max_count* < 1.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`AllAtomPackingScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetEnergy(aaa, count, energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every heavy atom type and for every *count* <= *max_count*.

    :param aaa: Heavy atom type for which to set energy.
    :type aaa:  :class:`~promod3.loop.AminoAcidAtom`
    :param count: Number of surrounding atoms for which to set energy.
    :type count:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues in the input loop. True by default.
    :type do_it:        :class:`bool`


.. function:: LoadAllAtomPackingScorer()

  :returns: The default predefined AllAtomPackingScorer (loaded from disk)
  :rtype:   :class:`AllAtomPackingScorer`


AllAtomClashScorer class
--------------------------------------------------------------------------------

.. class:: AllAtomClashScorer

  Inherits all functionality of :class:`AllAtomScorer`. Calculates a simple
  clash score of all atoms against the environment. There is no need to define
  any parameters here as all interaction energies are fixed (see Eq. (11) in
  [canutescu2003b]_). By default, the scorer calculates the scores by
  including everything, the stuff set in the environment and the coordinates
  in the input loops. You can change this behaviour with the according 
  functions. 

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions within the 
                        loop. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions towards the 
                        environment. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues in the input loop. True by default.
    :type do_it:        :class:`bool`
