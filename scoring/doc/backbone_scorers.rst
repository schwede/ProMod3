..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Backbone Scorers
================================================================================

.. currentmodule:: promod3.scoring


BackboneOverallScorer class
--------------------------------------------------------------------------------

.. class:: BackboneOverallScorer

  Container that allows for the storage of multiple scorers identified by a key
  (:class:`str`) and the computation of combined scores.

  Scorers need to be individually set or loaded by 
  :func:`LoadDefaultBackboneOverallScorer()`

  .. method:: Contains(key)

    :return: True, if a scorer object for this key was already added.
    :rtype:  :class:`bool`
    :param key: Key for desired scorer.
    :type key:  :class:`str`

  .. method:: Get(key)

    :return: Scorer with the given *key* (read-only access).
    :rtype:  :class:`BackboneScorer`
    :param key: Key for desired scorer.
    :type key:  :class:`str`
    :raises: :exc:`~exceptions.RuntimeError` if there is no scorer with that
             *key*.

  .. method:: AttachEnvironment(env)

    :param env: Link all scorers to this score environment.
    :type env:  :class:`BackboneScoreEnv`

  .. method:: Calculate(key, start_resnum, num_residues, chain_idx=0)

    Calculate score for one or several stretches of amino acids given the 
    current scoring environment.

    :param key: Key for desired scorer.
    :type key:  :class:`str`
    :param start_resnum: Res. number defining the position in the SEQRES
                         (see :class:`BackboneScoreEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Number of residues in the stretch(es) to score
    :type num_residues: :class:`int` / :class:`list` of :class:`int`
    :param chain_idx: Index of chain the stretch(es) belongs to
                      (see :class:`BackboneScoreEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Score calculated with the desired scorer for the given stretch(es).
    :rtype:  :class:`float` 

    :raises: :exc:`~exceptions.RuntimeError` if there is no scorer with that
             *key* or anything raised in :meth:`BackboneScorer.CalculateScore`.

  .. method:: CalculateLinearCombination(linear_weights, start_resnum,\
                                         num_residues, chain_idx=0)

    Calculate linear combination of scores for one or several stretches of 
    amino acids given the current scoring environment.

    :param linear_weights: Weights for each desired scorer. You can add a
                           constant value to each score by defining a weight
                           with key "intercept".
    :type linear_weights:  :class:`dict` (keys: :class:`str`,
                           values: :class:`float`)
    :param start_resnum: Res. number defining the position in the SEQRES
                         (see :class:`BackboneScoreEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Number of residues in the stretch(es) to score
    :type num_residues: :class:`int` / :class:`list` of :class:`int`
    :param chain_idx: Index of chain the stretch(es) belongs to
                      (see :class:`BackboneScoreEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Linear combination of the scores calculated with the desired
             scorers for the given stretch(es)
    :rtype:  :class:`float` 

    :raises: :exc:`~exceptions.RuntimeError` if *linear_weights* has a *key* for
             which no scorer exists or anything raised in
             :meth:`BackboneScorer.CalculateScore` for any of the used scorers.

  .. method:: __getitem__(key)
              __setitem__(key)

    Allow read/write access (with [*key*]) to scorer object with given key.

    :param key: Key for desired scorer.
    :type key:  :class:`str`

.. function:: LoadDefaultBackboneOverallScorer()

  :returns: Loads or creates the default scorers accessible through 
            following keys:
            "cb_packing", "cbeta", "reduced", "clash", "hbond", "ss_agreement",\
            "torsion", "pairwise"
  :rtype:   :class:`BackboneOverallScorer`


BackboneScorer base class
--------------------------------------------------------------------------------

.. class:: BackboneScorer

  Base class for all the backbone scorers listed below.

  .. method:: AttachEnvironment(env)

    :param env: Link scorer to this score environment.
    :type env:  :class:`BackboneScoreEnv`

  .. method:: CalculateScore(start_resnum, num_residues, chain_idx=0)

    Calculates score for one or several stretches given the structural 
    information in the attached environment. Unless otherwise noted in the 
    scorer, a lower "score" is better!

    :param start_resnum: Res. number defining the position in the SEQRES
                         (see :class:`BackboneScoreEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Number of residues in the stretch(es) to score
    :type num_residues: :class:`int` / :class:`list` of :class:`int`
    :param chain_idx: Index of chain the stretch(es) belongs to
                      (see :class:`BackboneScoreEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Score for the given stretch(es).
    :rtype:  :class:`float`

    :raises: (for most scorers) :exc:`~exceptions.RuntimeError` if scorer was
             never attached to a score environment, if scorer has never been
             properly initialized or if *start_resnum* / *num_residues* / 
             *chain_idx* lead to invalid positions.

  .. method:: CalculateScoreProfile(start_resnum, num_residues, chain_idx=0)

    Calculates per residue scores for one or several stretches given the 
    structural information in the attached environment.

    :param start_resnum: Res. number defining the position in the SEQRES
                         (see :class:`BackboneScoreEnv` for indexing)
    :type start_resnum:  :class:`int` / :class:`list` of :class:`int`
    :param num_residues: Number of residues in the stretch(es) to score
    :type num_residues: :class:`int` / :class:`list` of :class:`int`
    :param chain_idx: Index of chain the stretch(es) belongs to
                      (see :class:`BackboneScoreEnv` for indexing)
    :type chain_idx:  :class:`int` / :class:`list` of :class:`int`

    :return: Scores for the given stretch(es), one for each residue.
    :rtype:  :class:`list` of :class:`float` or :class:`list` of :class:`list`
             of :class:`float`

    :raises: same :exc:`~exceptions.RuntimeError` as :meth:`CalculateScore`.


CBPackingScorer class
--------------------------------------------------------------------------------

.. class:: CBPackingScorer(cutoff, max_count)

  Inherits all functionality of :class:`BackboneScorer`. Evaluates pseudo
  energies by counting the number of other CB positions within a certain
  *cutoff* radius of the CB position of the residue to be evaluated. 

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadCBPackingScorer`) or by setting all energies (see
  :meth:`SetEnergy`).

  :param cutoff: Radius in which other cbeta atoms are counted.
  :type cutoff:  :class:`float`
  :param max_count: If number of other cbeta atoms exceeds *max_count*, it will
                    be set to this number.
  :type max_count:  :class:`int`

  :raises:  :exc:`~exceptions.RuntimeError` if *cutoff* < 0 or *max_count* < 1.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`CBPackingScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.
    
  .. method:: SetEnergy(aa, count, energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every type of amino acids and for every *count* <= *max_count*.

    :param aa: Amino acid for which to set energy.
    :type aa:  :class:`ost.conop.AminoAcid`
    :param count: Number of surrounding CB positions for which to set energy.
    :type count:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`


.. function:: LoadCBPackingScorer()

  :returns: The default predefined CBPackingScorer (loaded from disk)
  :rtype:   :class:`CBPackingScorer`


CBetaScorer class
--------------------------------------------------------------------------------

.. class:: CBetaScorer(cutoff, bins, seq_sep)

  Inherits all functionality of :class:`BackboneScorer`. Evaluates a pairwise
  pseudo interaction energy between CB atoms which are located within a *cutoff*
  and which are at least *seq_sep* residues apart. An energy is assigned to each
  distance using equally sized bins and distinguishing all possible pairs
  of amino acids.  

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadCBetaScorer`) or by setting all energies (see :meth:`SetEnergy`).

  :param cutoff: Radius in which other cbeta atoms are considered.
  :type cutoff:  :class:`float`
  :param bins: Number of equally sized bins to discretize distances (range
               of [0, *cutoff*]).
  :type bins:  :class:`int`
  :param seq_sep: Minimal separation in sequence two cbeta atoms must have to
                  be considered.
  :type seq_sep:  :class:`int`

  :raises: :exc:`~exceptions.RuntimeError` if *cutoff* < 0, *bins* < 1 or
           *seq_sep* < 1.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`CBetaScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetEnergy(aa1, aa2, bin, energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every pair of amino acids and for every *bin* < *bins*.
    Internal symmetry is enforced => Calling SetEnergy(aa1, aa2, bin, energy) is
    equivalent to calling SetEnergy(aa1, aa2, bin, energy) AND
    SetEnergy(aa2, aa1, bin, energy).

    :param aa1: Amino acid for first interaction partner.
    :type aa1:  :class:`ost.conop.AminoAcid`
    :param aa2: Amino acid for second interaction partner.
    :type aa2:  :class:`ost.conop.AminoAcid`
    :param bin: Discrete bin describing the interaction distance.
    :type bin:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions between
                        the scored residues. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions of the scored
                        residues towards the surrounding environment. 
                        True by default.
    :type do_it:        :class:`bool`

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`


.. function:: LoadCBetaScorer()

  :returns: The default predefined CBetaScorer (loaded from disk)
  :rtype:   :class:`CBetaScorer`


ReducedScorer class
--------------------------------------------------------------------------------

.. class:: ReducedScorer(cutoff, dist_bins, angle_bins, dihedral_bins, seq_sep)

  Inherits all functionality of :class:`BackboneScorer`. Evaluates a pairwise
  pseudo interaction energy between the reduced representation of residues.
  Every residue gets represented by its CA position p and a directional
  component ``v = norm(p-n_pos) + norm(p-c_pos)``. Residues with CA
  distance < *cutoff* and which are at least *seq_sep* residues apart are
  considered to be interacting. For interacting residues r1 and r2, we can
  define a line l between p1 and p2. The potential then considers:

  * dist => distance between p1 and p2
  * alpha => angle between v1 and l
  * beta => angle between v2 and l
  * gamma => dihedral between (p1+v1,p1,p2,p2+v2)

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadReducedScorer`) or by setting all energies (see :meth:`SetEnergy`).

  :param cutoff: Radius in which other CA atoms are searched.
  :type cutoff:  :class:`float`
  :param dist_bins: Number of equally sized bins to discretize distances (range
                    of [0, *cutoff*]).
  :type dist_bins:  :class:`int`
  :param angle_bins: Number of equally sized bins to discretize angles (range
                     of [0, pi]).
  :type angle_bins:  :class:`int`
  :param dihedral_bins: Number of equally sized bins to discretize dihedrals
                        (range of [-pi, pi]).
  :type dihedral_bins:  :class:`int`
  :param seq_sep: Minimal separation in sequence two residues must have to
                  be considered.
  :type seq_sep:  :class:`int`

  :raises: :exc:`~exceptions.RuntimeError` if *cutoff* < 0, *dist_bins* < 1,
           *angle_bins* < 1, *dihedral_bins* < 1 or *seq_sep* < 1.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`ReducedScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetEnergy(aa1, aa2, dist_bin, alpha_bin, beta_bin, gamma_bin,\
                        energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every pair of amino acids, every *dist_bin* < *dist_bins*, every
    *alpha_bin* < *angle_bins*, every *beta_bin* < *angle_bins* and every
    *gamma_bin* < *dihedral_bins*.
    Internal symmetry is enforced => Calling 
    SetEnergy(aa1, aa2, dist_bin, alpha_bin, beta_bin, energy) is
    equivalent to calling 
    SetEnergy(aa1, aa2, dist_bin, alpha_bin, beta_bin, energy) AND
    SetEnergy(aa2, aa1, dist_bin, beta_bin, alpha_bin, energy).

    :param aa1: Amino acid for first interaction partner.
    :type aa1:  :class:`ost.conop.AminoAcid`
    :param aa2: Amino acid for second interaction partner.
    :type aa2:  :class:`ost.conop.AminoAcid`
    :param dist_bin: Discrete bin describing the interaction distance.
    :type dist_bin:  :class:`int`
    :param alpha_bin: Discrete bin describing the alpha angle.
    :type alpha_bin:  :class:`int`
    :param beta_bin: Discrete bin describing the beta angle.
    :type beta_bin:  :class:`int`
    :param gamma_bin: Discrete bin describing the gamma dihedral.
    :type gamma_bin:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions between
                        the scored residues. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions of the scored
                        residues towards the surrounding environment. 
                        True by default.
    :type do_it:        :class:`bool`

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`

.. function:: LoadReducedScorer()

  :returns: The default predefined ReducedScorer (loaded from disk)
  :rtype:   :class:`ReducedScorer`


ClashScorer class
--------------------------------------------------------------------------------

.. class:: ClashScorer

  Inherits all functionality of :class:`BackboneScorer`. Calculates a simple
  clash score of a loop itself and with the set environment. There is no need to
  define any parameters here as all interaction energies are fixed (see Eq. (11) 
  in [canutescu2003b]_). 

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions between
                        the scored residues. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions of the scored
                        residues towards the surrounding environment. 
                        True by default.
    :type do_it:        :class:`bool`

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`


HBondScorer class
--------------------------------------------------------------------------------

.. class:: HBondScorer(min_d, max_d, min_alpha, max_alpha, min_beta, max_beta,\
                       min_gamma, max_gamma, d_bins, alpha_bins, beta_bins,\
                       gamma_bins)

  Inherits all functionality of :class:`BackboneScorer`. Evaluates pairwise
  HBond pseudo energies similar to the one defined in the Rosetta energy
  function. It considers the CA, C and O positions from backbone hbond acceptors
  in interaction with the N and H positions from the backbone hbond donors. 4
  Parameters describe their relative orientation.

  * dist => H-O distance
  * alpha => O-H-N angle
  * beta => C-N-H angle
  * gamma => CA-C-O-H dihedral angle

  A pseudo energy function for these parameters is evaluated for three different
  states. State 1 for helical residues, state 2 for extended residues and state
  0 for other residues. If the state of two interacting particles is the same,
  thats the one from which the energy is extracted. In all other cases, the
  energy is extracted from the 0 state.

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadHBondScorer`) or by setting all energies (see :meth:`SetEnergy`).

  :param min_d: Minimal H-O distance to consider interaction
  :type min_d:  :class:`float`
  :param max_d: Maximal H-O distance to consider interaction
  :type max_d:  :class:`float`
  :param min_alpha: Minimal O-H-N angle to consider interaction
  :type min_alpha:  :class:`float`
  :param max_alpha: Maximal O-H-N angle to consider interaction
  :type max_alpha:  :class:`float`
  :param min_beta: Minimal C-N-H angle to consider interaction
  :type min_beta:  :class:`float`
  :param max_beta: Maximal C-N-H angle to consider interaction
  :type max_beta:  :class:`float`
  :param min_gamma: Minimal CA-C-O-H dihedral to consider interaction
  :type min_gamma:  :class:`float`
  :param max_gamma: Maximal CA-C-O-H dihedral to consider interaction
  :type max_gamma:  :class:`float`
  :param d_bins: Number of equally sized bins to discretize H-O distances
                 (range of [*min_d*, *max_d*]).
  :type d_bins:  :class:`int`
  :param alpha_bins: Number of equally sized bins to discretize O-H-N angles
                     (range of [*min_alpha*, *max_alpha*]).
  :type alpha_bins:  :class:`int`
  :param beta_bins: Number of equally sized bins to discretize C-N-H angles
                    (range of [*min_beta*, *max_beta*]).
  :type beta_bins:  :class:`int`
  :param gamma_bins: Number of equally sized bins to discretize CA-C-O-H angles
                     (range of [*min_gamma*, *max_gamma*]).
  :type gamma_bins:  :class:`int`

  :raises: :exc:`~exceptions.RuntimeError` if one of the bin parameters is
           < 1 or a max parameter is smaller than its min counterpart.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`HBondScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetEnergy(state, d_bin, alpha_bin, beta_bin, gamma_bin, energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every state ([0, 1, 2]), every *d_bin* < *d_bins*, every
    *alpha_bin* < *alpha_bins*, every *beta_bin* < *beta_bins* and every
    *gamma_bin* < *gamma_bins*.

    :param state: State describing the actual secondary structure
                  (1: helical, 2: extended, 0: other)
    :type state:  :class:`int`
    :param d_bin: Discrete bin describing the H-O distance.
    :type d_bin:  :class:`int`
    :param alpha_bin: Discrete bin describing the O-H-N angle.
    :type alpha_bin:  :class:`int`
    :param beta_bin: Discrete bin describing the C-N-H angle. 
    :type beta_bin:  :class:`int`
    :param gamma_bin: Discrete bin describing the CA-C-O-H dihedral.
    :type gamma_bin:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions between
                        the scored residues. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions of the scored
                        residues towards the surrounding environment. 
                        True by default.
    :type do_it:        :class:`bool`

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`


.. function:: LoadHBondScorer()

  :returns: The default predefined HBondScorer (loaded from disk)
  :rtype:   :class:`HBondScorer`


SSAgreementScorer class
--------------------------------------------------------------------------------

.. class:: SSAgreementScorer

  Inherits all functionality of :class:`BackboneScorer`. Evaluates a secondary
  structure agreement score. The scorer has a score for each  combination of
  psipred prediction, its confidence and the actually occurring secondary
  structure in the model. In every score evaluation, the secondary structure of
  the loop is estimated by searching for hydrogen bonds leading to a secondary
  structure as defined by dssp. The hbonds are searched internally in the loop
  as well as in the environment. 

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadSSAgreementScorer`) or by setting scores for all possible states
  (see :meth:`SetScore`).

  This scorer assumes that the attached environment has a psipred prediction
  defined (see :meth:`BackboneScoreEnv.SetPsipredPrediction`) as soon as a score
  is to be calculated.

  Note that for this scorer a higher "score" is better! So take care when
  combining this to other scores, where it is commonly the other way around.

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`SSAgreementScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetScore(psipred_state, psipred_confidence, dssp_state, score)

    Setup a single score for a combination of states. Unless a predefined scorer
    is loaded, this must be called for every combination of states.

    :param psipred_state: must be one of ['H', 'E', 'C']
    :type psipred_state:  :class:`str`
    :param psipred_confidence: must be in range [0, 9]
    :type psipred_confidence:  :class:`int`
    :param dssp_state: must be one of ['H', 'E', 'C', 'G', 'B', 'S', 'T', 'I']
    :type dssp_state:  :class:`str`
    :param score: score to be set
    :type score:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`

.. function:: LoadSSAgreementScorer()

  :returns: The default predefined structure agreement scorer (loaded from disk)
  :rtype:   :class:`SSAgreementScorer`


TorsionScorer class
--------------------------------------------------------------------------------

.. class:: TorsionScorer(group_definitions, torsion_bins)

  Inherits all functionality of :class:`BackboneScorer`. Evaluates pseudo
  energies based on the identity of three consecutive residues and the phi/psi
  dihedral angles of the central residue. The first phi and last psi angle get
  determined with the help of the environment if set.

  The scorer needs to be initialized either by loading a predefined scorer (e.g.
  :func:`LoadTorsionScorer`) or by setting all energies (see :meth:`SetEnergy`)
  for each group definition.

  :param group_definitions: List of group definitions defining amino acid
                            triplets (same style as used in the
                            :class:`~promod3.loop.TorsionSampler` class).
  :type group_definitions:  :class:`list` of :class:`str`
  :param torsion_bins: Number of equally sized bins to discretize the torsion
                       angles (range of [0, 2*pi]).
  :type torsion_bins:  :class:`int`

  :raises: :exc:`~exceptions.RuntimeError` if *torsion_bins* < 1 or if there is
           a possible combination of the 20 standard amino acids not matching
           any entry of *group_definitions*

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded scorer
    :rtype:   :class:`TorsionScorer`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: SetEnergy(group_idx, phi_bin, psi_bin, energy)

    Setup one energy value. Unless a predefined scorer is loaded, this must be
    called for every *group_idx* < len(*group_definitions*), every *phi_bin* <
    *torsion_bins* and every *psi_bin* < *torsion_bins*.

    :param group_idx: Index of group definition as set in constructor with
                      numbering starting at 0.
    :type group_idx:  :class:`int`
    :param phi_bin: Discrete bin describing the phi angle.
    :type phi_bin:  :class:`int`
    :param psi_bin: Discrete bin describing the psi angle.
    :type psi_bin:  :class:`int`
    :param energy: Energy to set for those parameters.
    :type energy:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if inputs are invalid

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`

.. function:: LoadTorsionScorer()

  :returns: The default predefined TorsionScorer (loaded from disk)
  :rtype:   :class:`TorsionScorer`


PairwiseScorer class
--------------------------------------------------------------------------------

.. class:: PairwiseScorer

  Inherits all functionality of :class:`BackboneScorer`. Evaluates a list of
  generic pairwise functions (see :class:`PairwiseFunction`). 
  That are set in the attached scoring environment 
  (see :meth:`BackboneScoreEnv.ApplyPairwiseFunction`).

  Note that for this scorer a higher "score" is better! So take care when
  combining this to other scores, where it is commonly the other way around.

  .. method:: DoInternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions between
                        the scored residues. True by default.
    :type do_it:        :class:`bool`

  .. method:: DoExternalScores(do_it) 

    :param do_it:       Whether to include pairwise interactions of the scored
                        residues towards the surrounding environment. 
                        True by default.
    :type do_it:        :class:`bool`, true by default.

  .. method:: DoNormalize(do_it) 

    :param do_it:       Whether to normalize the calculated scores by the number
                        of residues to be scored. True by default.
    :type do_it:        :class:`bool`
