..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Other Scoring Functions
================================================================================

.. currentmodule:: promod3.scoring


Scoring Functions from SCWRL3
--------------------------------------------------------------------------------

.. method:: SCWRL3PairwiseScore(d, Rij)

  Pairwise score from the SCWRL3 sidechain construction algorithm 
  [canutescu2003b]_.

  :param d:             Distance between the two interacting particles
  :param Rij:           Summed hard-sphere radii of the interacting particles.
                        Suggestions from the paper:

                        - carbon: 1.6
                        - oxygen: 1.3
                        - nitrogen: 1.3
                        - sulfur: 1.7

  :type d:              :class:`float`
  :type Rij:            :class:`float`

  :returns:             The score
  :rtype:               :class:`float`


.. method:: SCWRL3DisulfidScore(ca_pos_one, cb_pos_one, sg_pos_one\
                                ca_pos_two, cb_pos_two, sg_pos_two)

  Implements the empirically derived disulfid score from the SCWRL3 sidechain 
  construction algorithm [canutescu2003b]_.

  :param ca_pos_one:    CA carbon position of first amino acid
  :param cb_pos_one:    CB carbon position of first amino acid
  :param sg_pos_one:    SG sulfur position of first amino acid
  :param ca_pos_two:    CA carbon position of second amino acid
  :param cb_pos_two:    CB carbon position of second amino acid
  :param sg_pos_two:    SG sulfur position of second amino acid

  :type ca_pos_one:     :class:`ost.geoom.Vec3`
  :type cb_pos_one:     :class:`ost.geoom.Vec3`
  :type sg_pos_one:     :class:`ost.geoom.Vec3`
  :type ca_pos_two:     :class:`ost.geoom.Vec3`
  :type cb_pos_two:     :class:`ost.geoom.Vec3`
  :type sg_pos_two:     :class:`ost.geoom.Vec3`

  :returns:             The score
  :rtype:               :class:`float`



