..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Backbone Score Environment
================================================================================

.. currentmodule:: promod3.scoring

BackboneScoreEnv class
--------------------------------------------------------------------------------

.. class:: BackboneScoreEnv(seqres)

  The backbone score environment contains and handles all model-specific data
  used by the scorers. It is linked to a (list of) seqres (one per chain) at
  construction. The idea is to initialize it at the beginning of the modelling
  process with all known data (positions extracted from template, psipred
  prediction, etc). All scorers attached to that environment will see that data 
  and can calculate scores accordingly.
  Scoring with this setup is a two step process:

  * Set the positions you want to score in the environment to make it available
    to all attached scorers
  * Call the scorers to get the desired scores

  One problem that might occur is that you mess around with the environment and
  at some point you want to restore the original state. The 
  :class:`BackboneScoreEnv` provides a Stash / Pop mechanism to perform this 
  task.

  :param seqres: Internal SEQRES to be set (single chain or list with one per
                 chain). Whenever setting structural data, consistency with this SEQRES is enforced.
  :type seqres:  :class:`str` / :class:`ost.seq.SequenceHandle` / 
                 :class:`list` of :class:`str` / :class:`ost.seq.SequenceList`

  To access parts of the environment there are two key arguments to consider:

  * *chain_idx*: Index of chain as it occurs in *seqres* (0 for single sequence)
  * *start_resnum*: Residue number defining the position in the SEQRES of chain
    with index *chain_idx*. **The numbering starts for every chain with the
    value 1**.

  .. method:: Copy()
 
    :returns: A copy of the current :class:`BackboneScoreEnv` without any scorers
              attached
    :rtype: :class:`BackboneScoreEnv`

  .. method:: SetInitialEnvironment(env_structure)

    Sets structural environment with which loops to be scored interact. If
    structural data was already set, all the existing data gets cleared first.

    :param env_structure:  Structral data to be set as environment. The chains
                           in *env_structure* are expected to be in the same
                           order as the SEQRES items provided in constructor.
    :type env_structure:   :class:`ost.mol.EntityHandle`

    :raises:  :exc:`~exceptions.RuntimeError` if *env* is inconsistent with
              SEQRES set in constructor. This can be because of corrupt residue
              numbers or sequence mismatches.

  .. method:: SetEnvironment(bb_list, start_resnum, chain_idx=0)

    Add/update structural information in environment. If structural data for
    specific residues is already set, the data gets resetted for these
    positions.

    :param bb_list: Structural data to be set as environment.
    :type bb_list:  :class:`~promod3.loop.BackboneList`
    :param start_resnum: Res. number defining the position in the SEQRES.
    :type start_resnum:  :class:`int` / :class:`ost.mol.ResNum`
    :param chain_idx: Index of chain the structural data belongs to.
    :type chain_idx:  :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` if sequence of *bb_list* is
              inconsistent with previously SEQRES set in constructor or when
              either *start_resnum* or *chain_idx* point to invalid positions in
              the SEQRES.

  .. method:: ClearEnvironment(start_resnum, num_residues, chain_idx=0)

    Clears a stretch of length *num_residues* in the environment in chain 
    with idx *chain_idx* starting from residue number *start_resnum*

    :param start_resnum: Start of stretch to clear
    :type start_resnum:  :class:`int`
    :param num_residues: Length of stretch to clear
    :type num_residues:  :class:`int`
    :param chain_idx: Chain the stretch belongs to
    :type chain_idx:  :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` when either *start_resnum* or 
              *chain_idx* point to invalid positions in the SEQRES.

  .. method:: SetPsipredPrediction(pred)

    Set the psipred prediction, which is necessary to calculate some scores.

    :param pred: Psipred prediction to set (one per chain).
    :type pred:  :class:`PsipredPrediction` / :class:`list` of 
                 :class:`PsipredPrediction`

    :raises: :exc:`~exceptions.RuntimeError` if the number of predictions is
             inconsistent with the number of internal chains or when one of the
             predictions' sizes is inconsistent with the internal SEQRES size.

  .. method:: AddPairwiseFunction(function, function_type)

    Adds a pairwise function that can be used in :meth:`ApplyPairwiseFunction`.

    :param function: Pairwise function to be added.
    :type function:  :class:`PairwiseFunction`
    :param function_type: What distances should be looked at for this function.
    :type function_type:  :class:`PairwiseFunctionType`
    :return: Function index to be used in :meth:`ApplyPairwiseFunction`.
    :rtype:  :class:`int`

  .. method:: ApplyPairwiseFunction(chain_idx_one, resnum_one, chain_idx_two,\
                                    resnum_two, f_idx)

    Define two residues to be evaluated with a given pairwise function. This
    data can then be used by scorers. Note that the pairs are symmetric and so
    there is no need to add them twice with switched residues.

    :param chain_idx_one: Chain index of first residue
    :type chain_idx_one:  :class:`int`
    :param resnum_one:    Res. number of first residue
    :type resnum_one:     :class:`int`
    :param chain_idx_two: Chain index of second residue
    :type chain_idx_two:  :class:`int`
    :param resnum_two:    Res. number of second residue
    :type resnum_two:     :class:`int`
    :param f_idx:         Index of pairwise function (as returned by
                          :meth:`AddPairwiseFunction`)
    :type f_idx:          :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` if any of the chain indices or
              res. numbers is invalid, the interaction partners are the same
              residue or when *f_idx* is an invalid index.

  .. method:: Stash(start_rnum, num_residues, chain_idx) 

    FILO style stashing. You can perform up to 100 stash operations to save
    the current state of certain stretches in the environment. This state can
    be restored by calling :func:`Pop`. In one stash operation you can either 
    stash one stretch by providing integers as input or several stretches by 
    providing lists of integers.

    :param start_rnum:  start rnum of stretch to stash
    :param num_residues: length of stretch to stash
    :param chain_idx:   chain idx of stretch to stash

    :type start_rnum:   :class:`int` / :class:`list` of :class:`int`
    :type num_residues: :class:`int` / :class:`list` of :class:`int`
    :type chain_idx:    :class:`int` / :class:`list` of :class:`int`

  .. method:: Pop()

    Remove and apply the the last stash operation.

  .. method:: GetSeqres()

    :return: SEQRES that was set in constructor (one sequence per chain).
    :rtype:  :class:`ost.seq.SequenceList`


Pairwise function classes
--------------------------------------------------------------------------------

.. class:: PairwiseFunctionType

  Enumerates the pairwise function types. Possible values:

  * *CA_PAIRWISE_FUNCTION* - Evaluate CA atoms distances.
  * *CB_PAIRWISE_FUNCTION* - Evaluate CB atoms distances.

.. class:: PairwiseFunction

  Base class for any pairwise function.

  .. method:: Score(distance)

    :return: Score for a given pairwise distance
    :rtype:  :class:`float`
    :param distance: Pairwise distance
    :type distance:  :class:`float`

.. class:: ConstraintFunction(min_dist, max_dist, values)

  Inherits all functionality of :class:`PairwiseFunction`.
  Defines a constraint function. The score for a distance between *min_dist* and
  *max_dist* is determined by linear interpolation assuming the first and last 
  value exactly lying on *min_dist* and *max_dist*. For distances outside the 
  range defined by *min_dist* and *max_dist*, the returned score is 0.0.

  :param min_dist:    Minimal distance to be considered
  :param max_dist:    Maximal distance to be considered
  :param values:      The possible values that get returned when the distance is
                      between *min_dist* and *max_dist*

  :type min_dist:     :class:`float`
  :type max_dist:     :class:`float`
  :type values:       :class:`list` of :class:`float`

  :raises: :exc:`~exceptions.RuntimeError` if *min_dist* >= *max_dist* or when
           *min_dist* or *max_dist* are negative or when *values* contains no
           elements

.. class:: ContactFunction(max_dist, score)

  Inherits all functionality of :class:`PairwiseFunction`.
  Defines a simple contact function. The score value is *score* if
  distance < *max_dist* and 0.0 otherwise.

  :param max_dist:    Maximal distance to be in contact
  :param score:       Value that gets returned if in contact

  :type max_dist:     :class:`float`
  :type score:        :class:`float`


Convenient construction of pairwise functions
--------------------------------------------------------------------------------

.. class:: DiscoContainer(seqres, [function_type = CA_PAIRWISE_FUNCTION])

  A container collecting structural information until it can be added in form
  of :class:`ConstraintFunction` to a :class:`BackboneScoreEnv` . 
  The constraint functions are built after the principle of QMEANDisCo.

  :param seqres:        The sequence with which all added structures must match
  :param function_type: Whether you want to assess pairwise distances between CA
                        or CB atoms

  :type seqres:         :class:`ost.seq.SequenceHandle`
  :type function_type:  :class:`PairwiseFunctionType` 

  .. function:: AddStructuralInfo(aln)

    :param aln:       Alignment, where first sequence represent the initial 
                      SEQRES and the second sequence the actual structural
                      info. The second sequence must have a view attached.     

    :type aln:        :class:`ost.seq.AlignmentHandle`

    :raises: :exc:`~exceptions.RuntimeError` if number of sequences in *aln* is 
             not two, the SEQRES does not match or when the second sequence in 
             *aln* has no view attached.

  .. function:: AttachConstraints(env, chain_indices, [cluster_thresh = 0.5, gamma = 70.0])

    Generates distance constraints and adds it to *env*. 
    It first clusters the added sequences with a pairwise normalised sequence 
    similarity based on BLOSUM62. It's a hierarchical clustering with with an 
    average distance metric and *cluster_thresh* as threshold to merge two 
    clusters. Constraint functions are built for every cluster and finally
    merged. The influence of the per cluster constraint functions to the overall 
    function depends on the clusters average sequence similarity to the
    target SEQRES. How fast this influence vanishes gets controlled by gamma.
    The higher it is, the faster vanishes the influence of structural info
    from distant clusters. The final pairwise cluster functions get then 
    directly added to the scoring *env*.

    :param env:            The scoring env to which the constraint 
                           should be added
    :param chain_indices:  Indices of chains for which the constraints should 
                           be added
    :param cluster_thresh: Controls the clustering behaviour at the initial
                           stage of constraint construction
    :param gamma:          Controls how fast the influence of constraint
                           functions of distant clusters vanishes

    :type env:             :class:`BackboneScoreEnv`
    :type chain_indices:   :class:`list` of :class:`int`
    :type cluster_thresh:  :class:`float`
    :type gamma:           :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if one of the *chain_indices* is
             invalid or the SEQRES set in *env* for specified chain is 
             inconsistent with SEQRES you initialized the DiscoContainer with
             