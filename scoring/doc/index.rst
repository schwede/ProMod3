..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:mod:`~promod3.scoring` - Loop Scoring
================================================================================

.. module:: promod3.scoring
  :synopsis: Loop Scoring

.. currentmodule:: promod3.scoring

Tools and algorithms to score loops. The scoring system is split between an
environment and scorers.
Several scorers can be attached to the same environment containing the
actual structural data of the current modelling problem.
The environment is updated as the modelling proceeds and manages efficient
spatial lookups to be used by the attached scorers.

In this example, we load a structure, setup a score environment, link a few
scorers to it and finally score some loops:

.. literalinclude:: ../../../tests/doc/scripts/scoring_main.py

Contents:

.. toctree::
   :maxdepth: 2

   backbone_score_env
   backbone_scorers
   all_atom_scorers
   other_scoring_functions
