// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/scoring_object_loader.hh>
#include <promod3/config.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/scoring/clash_score.hh>
#include <promod3/scoring/pairwise_score.hh>
#include <promod3/scoring/all_atom_clash_score.hh>

namespace promod3{ namespace scoring {

CBPackingScorerPtr LoadCBPackingScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadCBPackingScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "cb_packing_scorer.dat");
  CBPackingScorerPtr p = CBPackingScorer::Load(path);
  return p;
}

CBetaScorerPtr LoadCBetaScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadCBetaScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "cbeta_scorer.dat");
  CBetaScorerPtr p = CBetaScorer::Load(path);
  return p;
}

ReducedScorerPtr LoadReducedScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadReducedScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "reduced_scorer.dat");
  ReducedScorerPtr p = ReducedScorer::Load(path);
  return p;
}

HBondScorerPtr LoadHBondScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadHBondScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "hbond_scorer.dat");
  HBondScorerPtr p = HBondScorer::Load(path);
  return p;
}

SSAgreementScorerPtr LoadSSAgreementScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadSSAgreementScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "ss_agreement_scorer.dat");
  SSAgreementScorerPtr p = SSAgreementScorer::Load(path);
  return p;
}

TorsionScorerPtr LoadTorsionScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadTorsionScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "torsion_scorer.dat");
  TorsionScorerPtr p = TorsionScorer::Load(path);
  return p;
}

AllAtomInteractionScorerPtr LoadAllAtomInteractionScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadAllAtomInteractionScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "aa_scorer.dat");
  AllAtomInteractionScorerPtr p = AllAtomInteractionScorer::Load(path);
  return p;
}

AllAtomPackingScorerPtr LoadAllAtomPackingScorer() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "scoring::LoadAllAtomPackingScorer", 2);
  String path = GetProMod3BinaryPath("scoring_data", "aa_packing_scorer.dat");
  AllAtomPackingScorerPtr p = AllAtomPackingScorer::Load(path);
  return p;
}

BackboneOverallScorerPtr LoadDefaultBackboneOverallScorer() {

  BackboneOverallScorerPtr scorer(new BackboneOverallScorer);

  (*scorer)["cb_packing"] = LoadCBPackingScorer();
  (*scorer)["cbeta"] = LoadCBetaScorer();
  (*scorer)["reduced"] = LoadReducedScorer();
  (*scorer)["clash"] = ClashScorerPtr(new ClashScorer);
  (*scorer)["hbond"] = LoadHBondScorer();
  (*scorer)["torsion"] = LoadTorsionScorer();
  (*scorer)["pairwise"] = PairwiseScorerPtr(new PairwiseScorer);

  return scorer;
}

AllAtomOverallScorerPtr LoadDefaultAllAtomOverallScorer(){

  AllAtomOverallScorerPtr scorer(new AllAtomOverallScorer);

  (*scorer)["aa_interaction"] = LoadAllAtomInteractionScorer();
  (*scorer)["aa_packing"] = LoadAllAtomPackingScorer();
  (*scorer)["aa_clash"] = AllAtomClashScorerPtr(new AllAtomClashScorer);

  return scorer;
}

}} //ns
