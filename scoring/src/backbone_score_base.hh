// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_BACKBONE_SCORE_HH
#define PM3_SCORING_BACKBONE_SCORE_HH

#include <ost/seq/sequence_handle.hh>
#include <ost/seq/sequence_list.hh>
#include <promod3/loop/backbone.hh>
#include <promod3/loop/psipred_prediction.hh>
#include <promod3/loop/idx_handler.hh>
#include <promod3/core/runtime_profiling.hh>
#include <boost/shared_ptr.hpp>
#include <promod3/scoring/pairwise_scoring_function.hh>

#include <algorithm> 
#include <stack>


namespace promod3 { namespace scoring {

class BackboneScoreEnv;
class BackboneScoreEnvListener;
class BackboneScorer;

typedef boost::shared_ptr<BackboneScoreEnv> BackboneScoreEnvPtr;
typedef boost::shared_ptr<BackboneScoreEnvListener> BackboneScoreEnvListenerPtr;
typedef boost::shared_ptr<BackboneScorer> BackboneScorerPtr;


enum PairwiseFunctionType {
  CA_PAIRWISE_FUNCTION,
  CB_PAIRWISE_FUNCTION
};


struct PairwiseFunctionData {
  PairwiseFunctionPtr f;
  PairwiseFunctionType ft;
  uint other_idx;
};


class BackboneScoreEnv {

public:

  BackboneScoreEnv(const ost::seq::SequenceList& seqres);

  ~BackboneScoreEnv();

  // methods intended to be used from the outside world

  BackboneScoreEnvPtr Copy() const;

  void SetInitialEnvironment(const ost::mol::EntityHandle& env);

  void SetEnvironment(const loop::BackboneList& bb_list, 
                      uint start_resnum, uint chain_idx = 0);

  void ClearEnvironment(uint start_resnum, uint num_residues, 
                        uint chain_idx = 0);

  void SetPsipredPrediction(loop::PsipredPredictionPtr pp);

  void SetPsipredPrediction(std::vector<loop::PsipredPredictionPtr>& pp);

  uint AddPairwiseFunction(PairwiseFunctionPtr f,
                           PairwiseFunctionType ft);

  void ApplyPairwiseFunction(uint chain_idx_one, uint resnum_one,
                             uint chain_idx_two, uint resnum_two,
                             uint f_idx);

  void Stash(uint start_resnum, uint num_residues, uint chain_idx = 0);

  void Stash(const std::vector<uint>& start_resnum, 
             const std::vector<uint>& num_residues,
             const std::vector<uint>& chain_idx);

  void Pop();

  ost::seq::SequenceList GetSeqres() const { return seqres_; }

  // methods to access internal data intended to be used by the scorer objects

  const loop::IdxHandler* GetIdxHandlerData() const { return &idx_handler_; }

  const ost::conop::AminoAcid* GetAAData() const { return &aa_seqres_[0]; }

  const int* GetEnvSetData() const { return &env_set_[0]; }

  const geom::Vec3* GetNPosData() const { return &n_pos_[0]; }

  const geom::Vec3* GetCAPosData() const { return &ca_pos_[0]; }

  const geom::Vec3* GetCBPosData() const { return &cb_pos_[0]; }

  const geom::Vec3* GetCPosData() const { return &c_pos_[0]; }

  const geom::Vec3* GetOPosData() const { return &o_pos_[0]; }

  const char* GetPsipredPredictionData() const { return &psipred_pred_[0]; }

  const int* GetPsipredConfidenceData() const { return &psipred_cfi_[0]; }

  const std::vector<PairwiseFunctionData>* 
  GetPairwiseFunctionData() const { return &applied_pairwise_functions_[0]; }

  // listener related methods

  // return listener with that ID or NULL if not existing
  BackboneScoreEnvListener* GetListener(const String& id) const;
  // note that ownership of listener goes to this object which will eventually
  // delete the objects
  void AttachListener(BackboneScoreEnvListener* listener);
  // ensure and return unique object of type T with given id (create if needed)
  template <typename T>
  T* UniqueListener(const String& id) {
    T* listener;
    BackboneScoreEnvListener * tst = this->GetListener(id);
    if (tst == NULL) {
      // let's create a new one and attach it
      listener = new T();
      this->AttachListener(listener);
    } else {
      // it's already there, return it
      listener = dynamic_cast<T*>(tst);
    }
    return listener;
  }

private:

  void Stash(const std::vector<uint>& indices);

  ost::seq::SequenceList seqres_;
  loop::IdxHandler idx_handler_;
  std::vector<ost::conop::AminoAcid> aa_seqres_;
  std::vector<char> psipred_pred_;
  std::vector<int> psipred_cfi_;
  // the reason to use a vector with int here is that the STL has an optimized
  // vector<bool> (bitset, min. memory usage) which can't be used as C arrays
  std::vector<int> env_set_; 
  std::vector<geom::Vec3> n_pos_;
  std::vector<geom::Vec3> ca_pos_;
  std::vector<geom::Vec3> cb_pos_;
  std::vector<geom::Vec3> c_pos_;
  std::vector<geom::Vec3> o_pos_;
  // same as above, but to stash things
  std::stack<std::vector<uint> > stashed_indices_;
  std::stack<std::vector<int> > stashed_env_set_; 
  std::stack<std::vector<geom::Vec3> > stashed_n_pos_;
  std::stack<std::vector<geom::Vec3> > stashed_ca_pos_;
  std::stack<std::vector<geom::Vec3> > stashed_cb_pos_;
  std::stack<std::vector<geom::Vec3> > stashed_c_pos_;
  std::stack<std::vector<geom::Vec3> > stashed_o_pos_;
  // every residue gets a vector of pairs 
  // every pair contains: 
  // - a function ptr as a first element, i.e. the pairwise 
  //   function to applied
  // - a residue idx as a second element, the interaction partner
  std::vector<std::vector<PairwiseFunctionData> > applied_pairwise_functions_;
  std::vector<PairwiseFunctionPtr> pairwise_functions_;
  std::vector<PairwiseFunctionType> pairwise_function_types_;


  std::vector<BackboneScoreEnvListener*> listener_;
};


class BackboneScoreEnvListener {

public:

  virtual ~BackboneScoreEnvListener() { }

  virtual void Init(const BackboneScoreEnv& base_env) = 0;

  virtual void SetEnvironment(const std::vector<uint>& idx) = 0;

  virtual void ResetEnvironment(const std::vector<uint>& idx) = 0;

  virtual void ClearEnvironment(const std::vector<uint>& idx) = 0;

  virtual String WhoAmI() const = 0;
};


class BackboneScorer {

public:

  virtual ~BackboneScorer() { }

  virtual Real CalculateScore(uint start_resnum, uint num_residues, 
                              uint chain_idx) const = 0;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues, 
                                     uint chain_idx, 
                                     std::vector<Real>& profile) const = 0;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx) const = 0;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                            const std::vector<uint>& num_residues, 
                            const std::vector<uint>& chain_idx, 
                            std::vector<std::vector<Real> >& profile) const = 0;

  virtual void AttachEnvironment(BackboneScoreEnv& env) { }
};

}}//ns

#endif
