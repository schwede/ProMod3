// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/hbond_score.hh>

namespace promod3{ namespace scoring{

HBondScorer::HBondScorer(Real min_d, Real max_d,
                         Real min_alpha, Real max_alpha,
                         Real min_beta, Real max_beta,
                         Real min_gamma, Real max_gamma,
                         uint d_bins, uint alpha_bins,
                         uint beta_bins, uint gamma_bins): 
                                                  min_d_(min_d),
                                                  max_d_(max_d),
                                                  min_alpha_(min_alpha),
                                                  max_alpha_(max_alpha),
                                                  min_beta_(min_beta),
                                                  max_beta_(max_beta),
                                                  min_gamma_(min_gamma),
                                                  max_gamma_(max_gamma),
                                                  d_bins_(d_bins),
                                                  alpha_bins_(alpha_bins),
                                                  beta_bins_(beta_bins),
                                                  gamma_bins_(gamma_bins),
                                                  do_internal_scores_(true),
                                                  do_external_scores_(true),
                                                  normalize_(true),
                                                  env_(NULL) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::HBondScorer", 2);


  if(d_bins == 0) {
    throw promod3::Error("Number of distance bins must be nonzero!");
  }

  if(alpha_bins == 0) {
    throw promod3::Error("Number of alpha bins must be nonzero!");
  }

  if(beta_bins == 0) {
    throw promod3::Error("Number of beta bins must be nonzero!");
  }

  if(gamma_bins == 0) {
    throw promod3::Error("Number of gamma bins must be nonzero!");
  }

  if(max_d_ <= min_d_) {
    throw promod3::Error("hb_max_d must be larger than hb_min_d");
  }

  if(max_alpha_ <= min_alpha_) {
    throw promod3::Error("hb_max_alpha must be larger than hb_min_alpha");
  }

  if(max_beta_ <= min_beta_) {
    throw promod3::Error("max_beta must be larger than min_beta");
  }

  if(max_gamma_ <= min_gamma_) {
    throw promod3::Error("max_gamma must be larger than min_gamma");
  }

  //bins are nonzero, let's also set the bin sizes...
  d_bin_size_ = (max_d_ - min_d_) / d_bins_;
  alpha_bin_size_ = (max_alpha_ - min_alpha_) / alpha_bins_;
  beta_bin_size_ = (max_beta_ - min_beta_) / beta_bins_;
  gamma_bin_size_ = (max_gamma_ - min_gamma_) / gamma_bins_;

  //allocate energy memory
  uint num_values = 3 * d_bins_ * alpha_bins_ * beta_bins_ * gamma_bins_;
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

HBondScorer::~HBondScorer() {
  delete [] energies_;
}

HBondScorerPtr HBondScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // here starts the score specific stuff

  HBondScorerPtr p(new HBondScorer);

  in_stream.read(reinterpret_cast<char*>(&p->min_d_),sizeof(Real)); 
  in_stream.read(reinterpret_cast<char*>(&p->max_d_),sizeof(Real));           
  in_stream.read(reinterpret_cast<char*>(&p->min_alpha_),sizeof(Real)); 
  in_stream.read(reinterpret_cast<char*>(&p->max_alpha_),sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->min_beta_),sizeof(Real)); 
  in_stream.read(reinterpret_cast<char*>(&p->max_beta_),sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->min_gamma_),sizeof(Real)); 
  in_stream.read(reinterpret_cast<char*>(&p->max_gamma_),sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->d_bins_),sizeof(uint)); 
  in_stream.read(reinterpret_cast<char*>(&p->alpha_bins_),sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->beta_bins_),sizeof(uint)); 
  in_stream.read(reinterpret_cast<char*>(&p->gamma_bins_),sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->d_bin_size_),sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->alpha_bin_size_),sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->beta_bin_size_),sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->gamma_bin_size_),sizeof(Real));

  uint num_values = 3 * p->d_bins_ * p->alpha_bins_ * 
                    p->beta_bins_ * p->gamma_bins_;
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values*sizeof(Real));

  return p;
}

void HBondScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&min_d_),sizeof(Real)); 
  out_stream.write(reinterpret_cast<char*>(&max_d_),sizeof(Real));           
  out_stream.write(reinterpret_cast<char*>(&min_alpha_),sizeof(Real)); 
  out_stream.write(reinterpret_cast<char*>(&max_alpha_),sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&min_beta_),sizeof(Real)); 
  out_stream.write(reinterpret_cast<char*>(&max_beta_),sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&min_gamma_),sizeof(Real)); 
  out_stream.write(reinterpret_cast<char*>(&max_gamma_),sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&d_bins_),sizeof(uint)); 
  out_stream.write(reinterpret_cast<char*>(&alpha_bins_),sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&beta_bins_),sizeof(uint)); 
  out_stream.write(reinterpret_cast<char*>(&gamma_bins_),sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&d_bin_size_),sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&alpha_bin_size_),sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&beta_bin_size_),sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&gamma_bin_size_),sizeof(Real));

  uint num_bytes = 3 * d_bins_ * alpha_bins_ * 
                   beta_bins_ * gamma_bins_ * sizeof(Real);
  out_stream.write(reinterpret_cast<char*>(energies_), num_bytes);
}

HBondScorerPtr HBondScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);
  core::CheckBaseType<bool>(in_stream, true);

  // data
  HBondScorerPtr p(new HBondScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void HBondScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);
  core::WriteBaseType<bool>(out_stream, true);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void HBondScorer::SetEnergy(uint state, uint d_bin, uint alpha_bin,
                            uint beta_bin, uint gamma_bin, Real e) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::SetEnergy", 2);

  if(state >= 3) {
    throw promod3::Error("Invalid state when setting energy for hbond potential!");
  }

  if(d_bin >= d_bins_) {
    throw promod3::Error("Invalid d bin when setting energy for hbond potential!");
  }

  if(alpha_bin >= alpha_bins_) {
    throw promod3::Error("Invalid alpha bin when setting energy for hbond potential!");
  }

  if(beta_bin >= beta_bins_) {
    throw promod3::Error("Invalid beta bin when setting energy for hbond potential!");
  }

  if(gamma_bin >= gamma_bins_) {
    throw promod3::Error("Invalid gamma bin when setting energy for hbond potential!");
  }

  energies_[this->EIdx(state, d_bin, alpha_bin, beta_bin, gamma_bin)] = e;
}

Real HBondScorer::CalculateScore(uint start_resnum, uint num_residues,
                                 uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

 
  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);


  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  // sum them
  Real external_score = 0.0;
  Real internal_score = 0.0;
  for (uint i = 0; i < external_scores.size(); ++i) {
    external_score += external_scores[i];
    internal_score += internal_scores[i];
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && !external_scores.empty()) {
    external_score /= external_scores.size();
  }

  return external_score;
}


void HBondScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                        uint chain_idx,
                                        std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::CalculateScoreProfile", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);

  // get scores
  std::vector<Real> internal_scores;
  this->Score(idx_vector, profile, internal_scores, occupied);

  for(uint i = 0; i < profile.size(); ++i) {
    profile[i] += internal_scores[i];
  }
}


Real HBondScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                 const std::vector<uint>& num_residues,
                                 const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}


void HBondScorer::CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];

    }
  }
}


void HBondScorer::Score(const std::vector<uint>& indices,
                        std::vector<Real>& external_scores,
                        std::vector<Real>& internal_scores,
                        std::vector<bool>& occupied) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondScorer::Score", 2);

  // setup
  uint n_indices = indices.size();

  external_scores.assign(n_indices, 0.0);
  internal_scores.assign(n_indices, 0.0);

  if(n_indices == 0) return;

  HBondSpatialOrganizer::WithinResult within_result;
  std::pair<HBondSpatialOrganizerItem*,Real>* a;
  const int* env_set = env_->GetEnvSetData();
  const loop::IdxHandler* idx_handler = env_->GetIdxHandler();

  for(uint i = 0; i <  n_indices; ++i) {
    int my_idx = indices[i];

    if(!env_set[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    int my_chain_idx = idx_handler->GetChainIdx(my_idx);
    const HBondSpatialOrganizerItem& my_env_data = env_->GetEnvData(my_idx);
    int my_state = my_env_data.state;
    
    if(my_state != 0) {

      int my_chain_start_idx = idx_handler->GetChainIdx(my_chain_idx);
      int my_chain_last_idx = idx_handler->GetChainLastIdx(my_chain_idx);
      bool consistent_state = false;

      if(my_idx > my_chain_start_idx && env_set[my_idx - 1] && 
         env_->GetEnvData(my_idx - 1).state == my_state) {
        consistent_state = true;
      } else if(my_idx < my_chain_last_idx && env_set[my_idx + 1] && 
         env_->GetEnvData(my_idx + 1).state == my_state) {
        consistent_state = true;
      }

      if(!consistent_state) {
         my_state = 0;
      }
    }

    within_result = env_->FindWithin(my_env_data.pos, 9.0);
    a = within_result.first;

    for(uint j = 0; j < within_result.second; ++j) {

      int state = 0;
      int other_idx = a[j].first->idx;
      int other_state = a[j].first->state;

      if(other_state != 0 && other_state == my_state) {

        int other_chain_idx = idx_handler->GetChainIdx(other_idx);
        int other_chain_start_idx = idx_handler->GetChainIdx(other_chain_idx);
        int other_chain_last_idx = idx_handler->GetChainLastIdx(other_chain_idx);

        bool consistent_state = false;

        if(other_idx > other_chain_start_idx && env_set[other_idx - 1] && 
           env_->GetEnvData(other_idx - 1).state == other_state) {
          consistent_state = true;
        } else if(other_idx < other_chain_last_idx && env_set[other_idx + 1] && 
           env_->GetEnvData(other_idx + 1).state == other_state) {
          consistent_state = true;
        }

        if(consistent_state) {
          state = my_state;
        }
      }

      Real e = 0.0;

      //case of being donor for current bb item
      if(!my_env_data.is_proline) {
         e += this->EvalHBondPotential(state, my_env_data.n_pos, 
                                       my_env_data.h_pos,
                                       a[j].first->pos, 
                                       a[j].first->c_pos,
                                       a[j].first->o_pos);
      }

      //case of being acceptor for current bb item
      if(!a[j].first->is_proline) {
         e += this->EvalHBondPotential(state, a[j].first->n_pos, 
                                       a[j].first->h_pos,
                                       my_env_data.pos, 
                                       my_env_data.c_pos,
                                       my_env_data.o_pos);
      }

      if(occupied[other_idx] && do_internal_scores_) {
        internal_scores[i] += e;
      }

      if(!occupied[other_idx] && do_external_scores_) {
        external_scores[i] += e;
      }
    }
  }
}

void HBondScorer::AttachEnvironment(BackboneScoreEnv& env) {
  env_ = env.UniqueListener<HBondEnvListener>("HBondEnvListener");
}

}} // ns
