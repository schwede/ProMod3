// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_ALL_ATOM_INTERACTION_SCORE_HH
#define PM3_SCORING_ALL_ATOM_INTERACTION_SCORE_HH

#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/loop/all_atom_env.hh>
#include <promod3/scoring/all_atom_score_base.hh>
#include <promod3/scoring/all_atom_interaction_env_listener.hh>

namespace promod3 { namespace scoring {

class AllAtomInteractionScorer;
typedef boost::shared_ptr<AllAtomInteractionScorer> AllAtomInteractionScorerPtr;


class AllAtomInteractionScorer : public AllAtomScorer {

public:

  AllAtomInteractionScorer(Real cutoff, uint bins, uint seq_sep);

  virtual ~AllAtomInteractionScorer();

  static AllAtomInteractionScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static AllAtomInteractionScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetEnergy(loop::AminoAcidAtom a, loop::AminoAcidAtom b, uint bin,
                 Real e);

  void DoInternalScores(bool do_it) { do_internal_scores_ = do_it; }

  void DoExternalScores(bool do_it) { do_external_scores_ = do_it; }

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  virtual Real 
  CalculateScore(uint start_resnum, uint num_residues, uint chain_idx) const;

  virtual void
  CalculateScoreProfile(uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  virtual void AttachEnvironment(loop::AllAtomEnv& env);

private:

  AllAtomInteractionScorer(): do_internal_scores_(true),
                              do_external_scores_(true),
                              normalize_(true),
                              attached_environment_(false) { }

  uint EIdx(loop::AminoAcidAtom a, loop::AminoAcidAtom b,
                   uint bin) const {
    return (a * loop::XXX_NUM_ATOMS + b) * bins_ + bin;
  }
  uint NumEnergies() const {
    return loop::XXX_NUM_ATOMS * loop::XXX_NUM_ATOMS * bins_;
  }

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& external_scores,
             std::vector<Real>& internal_scores,
             std::vector<bool>& occupied) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    core::ConvertBaseType<float>(ds, cutoff_);
    core::ConvertBaseType<uint32_t>(ds, bins_);
    core::ConvertBaseType<uint32_t>(ds, seq_sep_);
    uint num_values = NumEnergies();
    if (ds.IsSource()) energies_ = new Real[num_values];
    for (uint i = 0; i < num_values; ++i) {
      core::ConvertBaseType<float>(ds, energies_[i]);
    }
  }

  Real cutoff_;
  uint bins_;
  uint seq_sep_;
  Real* energies_;
  bool do_internal_scores_;
  bool do_external_scores_;
  bool normalize_;

  // environment specific information, set upon calling AttachEnvironment
  bool attached_environment_;
  AllAtomInteractionEnvListenerPtr env_;
  loop::ConstIdxHandlerPtr idx_handler_;
  loop::ConstAllAtomPositionsPtr all_pos_;
};

}} // ns

#endif
