// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/torsion_score.hh>

namespace promod3{ namespace scoring{

TorsionScorer::TorsionScorer(std::vector<String>& group_definitions,
                             uint bins_per_dimension): 
                             num_groups_(group_definitions.size()),
                             bins_per_dimension_(bins_per_dimension),
                             bins_squared_(bins_per_dimension * 
                                           bins_per_dimension),
                             normalize_(true),
                             attached_environment_(false)
                                                 {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::TorsionScorer", 2);

  if(bins_per_dimension < 1) {
    throw promod3::Error("Bins per dimension must be at least one!");
  }

  bin_size_ = (2 * M_PI) / bins_per_dimension_;

  memset(index_mapper_, 0, 8000 * sizeof(uint));

  //Let's check, whether all possible combinations of the 20 standard amino
  //acids are covered
  std::vector<String> single_ids(3);
  for(int i = 0; i < ost::conop::XXX; ++i) {
    for(int j = 0; j < ost::conop::XXX; ++j) {
      for(int k = 0; k < ost::conop::XXX; ++k) {
        single_ids[0] = ost::conop::AminoAcidToResidueName(ost::conop::AminoAcid(i));
        single_ids[1] = ost::conop::AminoAcidToResidueName(ost::conop::AminoAcid(j));
        single_ids[2] = ost::conop::AminoAcidToResidueName(ost::conop::AminoAcid(k));

        bool match;
        bool found_id(false);

        for(uint l = 0; l < group_definitions.size(); ++l) {

          match=true;
          ost::StringRef gid(group_definitions[l].c_str(), group_definitions[l].length());
          std::vector<ost::StringRef> split_definition=gid.split('-'); //split ids
          if(split_definition.size() != 3) {
            throw promod3::Error("Invalid torsion group definition encountered!");
          }
          //iterate over all three positions
          for(int m=0;m<3;++m) {
            //"all" matches every residue
            if(split_definition[m].str().find("all")!=std::string::npos) {
              continue;
            }
            //check, whether currrent residue matches current id position
            if(split_definition[m].str().find(single_ids[m])==std::string::npos) {
              match=false;
              break;
            }
          }
          if(match) {
            index_mapper_[i*400 + j*20 + k] = l;
            found_id = true;
            break;
          }
        }
        if(!found_id) {
          std::stringstream ss;
          ss << "Could not match residues " << single_ids[0] << ", " << single_ids[1];
          ss << " and " << single_ids[2] << " to any of the group_identifiers";
          throw promod3::Error(ss.str());
        }
      }
    }
  }

  //allocate energy memory
  uint num_values = num_groups_ * bins_squared_;
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

TorsionScorer::~TorsionScorer() {
  delete [] energies_;
}

TorsionScorerPtr TorsionScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // here starts the score specific stuff

  TorsionScorerPtr p(new TorsionScorer);

  in_stream.read(reinterpret_cast<char*>(&p->num_groups_),sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->bins_per_dimension_),sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->bins_squared_),sizeof(uint)); 
  in_stream.read(reinterpret_cast<char*>(&p->bin_size_),sizeof(Real));           
  in_stream.read(reinterpret_cast<char*>(p->index_mapper_), 8000 * sizeof(uint));
  uint num_values = p->num_groups_ * p->bins_squared_;
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values * sizeof(Real));

  return p;
}

void TorsionScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&num_groups_),sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&bins_per_dimension_),sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&bins_squared_),sizeof(uint)); 
  out_stream.write(reinterpret_cast<char*>(&bin_size_),sizeof(Real));           
  out_stream.write(reinterpret_cast<char*>(index_mapper_), 8000 * sizeof(uint));
  uint num_values = num_groups_ * bins_squared_;
  out_stream.write(reinterpret_cast<char*>(energies_), num_values * sizeof(Real));
}

TorsionScorerPtr TorsionScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);
  core::CheckBaseType<bool>(in_stream, true);

  // data
  TorsionScorerPtr p(new TorsionScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void TorsionScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);
  core::WriteBaseType<bool>(out_stream, true);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void TorsionScorer::SetEnergy(uint group_idx, uint phi_bin, uint psi_bin, Real e) {

  if(group_idx >= num_groups_) {
    throw promod3::Error("Invalid torsion group idx!");
  }

  if(phi_bin >= bins_per_dimension_ || psi_bin >= bins_per_dimension_) {
    throw promod3::Error("Invalid dihedral angle bin!");
  }

  energies_[this->EIdx(group_idx, phi_bin * bins_per_dimension_ + psi_bin)] = e;
}


Real TorsionScorer::CalculateScore(uint start_resnum, uint num_residues,
                                   uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::CalculateScore", 2);

  if(!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, chain_idx,
                                      idx_vector);

  // get scores and sum them up
  std::vector<Real> scores;
  this->Score(idx_vector, scores);
  Real score = 0.0;
  for(uint i = 0; i < scores.size(); ++i) {
    score += scores[i];
  }

  if(normalize_ && !scores.empty()){
    score /= scores.size();
  }

  return score;
}


void TorsionScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                          uint chain_idx,
                                          std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::CalculateScoreProfile", 2);

  if(!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, chain_idx,
                                      idx_vector);

  // score
  this->Score(idx_vector, profile);
}

Real TorsionScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                   const std::vector<uint>& num_residues,
                                   const std::vector<uint>& chain_idx) const {


  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::CalculateScore", 2);

  if(!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, chain_idx,
                                      indices_vec);

  // get scores and sum them up
  Real score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> scores;
    this->Score(indices_vec[i], scores);
    for (uint j = 0; j < scores.size(); ++j) {
      score += scores[j];
    }
    n_scored_residues += scores.size();
  }
  
  if(normalize_ && n_scored_residues > 0) {
    score /= n_scored_residues;
  }

  return score;
}


void TorsionScorer::CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::CalculateScore", 2);

  if(!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    this->Score(indices_vec[i], profile[i]);
  }
}


void TorsionScorer::Score(const std::vector<uint>& indices,
                          std::vector<Real>& scores) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionScorer::Score", 2);

  // setup
  uint n_indices = indices.size();

  scores.assign(n_indices, 0.0);

  if(n_indices == 0) return;

  Real angle1, angle2;
  uint bin1, bin2;

  for(uint i = 0; i < n_indices; ++i) {
    int my_idx = indices[i];

    if(!env_set_[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    int my_chain_idx = idx_handler_->GetChainIdx(my_idx);
    int my_chain_first_idx = idx_handler_->GetChainStartIdx(my_chain_idx);
    int my_chain_last_idx = idx_handler_->GetChainLastIdx(my_chain_idx);

    if(my_idx > my_chain_first_idx && env_set_[my_idx - 1] &&
       my_idx < my_chain_last_idx && env_set_[my_idx + 1]) {

      angle1 = geom::DihedralAngle(c_pos_[my_idx - 1],
                                   n_pos_[my_idx],
                                   ca_pos_[my_idx], 
                                   c_pos_[my_idx]);

      angle2 = geom::DihedralAngle(n_pos_[my_idx],
                                   ca_pos_[my_idx], 
                                   c_pos_[my_idx],
                                   n_pos_[my_idx + 1]);

      bin1 = std::min(static_cast<uint>((angle1+Real(M_PI))/bin_size_),
                                         bins_per_dimension_-1);
      bin2 = std::min(static_cast<uint>((angle2+Real(M_PI))/bin_size_),
                                         bins_per_dimension_-1);

      scores[i] = energies_[this->EIdx(torsion_group_indices_[my_idx], 
                                       bin1 * bins_per_dimension_ + bin2)];   
    }
  }
}

void TorsionScorer::AttachEnvironment(BackboneScoreEnv& env) {

  idx_handler_ = env.GetIdxHandlerData();
  env_set_ = env.GetEnvSetData();
  n_pos_ = env.GetNPosData();
  ca_pos_ = env.GetCAPosData();
  c_pos_ = env.GetCPosData();
  torsion_group_indices_.assign(idx_handler_->GetNumResidues(),0);

  const ost::conop::AminoAcid* aa_data = env.GetAAData();
  ost::conop::AminoAcid aa_one, aa_two, aa_three;
  uint actual_idx;
  for(uint i = 0; i < idx_handler_->GetNumChains(); ++i) {
    for(uint j = 1; j < idx_handler_->GetChainSize(i)-1; ++j) {
      actual_idx = idx_handler_->GetChainStartIdx(i) + j;
      aa_one = aa_data[actual_idx - 1];
      aa_two = aa_data[actual_idx];
      aa_three = aa_data[actual_idx + 1];
      torsion_group_indices_[actual_idx] = index_mapper_[aa_one * 400 + 
                                                         aa_two * 20 + 
                                                         aa_three];
    }
  }

  attached_environment_ = true;
}

}} // ns
