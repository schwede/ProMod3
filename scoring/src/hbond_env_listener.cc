// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/hbond_env_listener.hh>
#include <set>

namespace promod3{ namespace scoring{

HBondEnvListener::HBondEnvListener(): env_(12.0),
                                      env_data_(NULL) { }

HBondEnvListener::~HBondEnvListener() {
  if(env_data_ != NULL) delete [] env_data_;
}

void HBondEnvListener::Init(const BackboneScoreEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondEnvListener::Init", 2);

  if(env_data_ != NULL){
  	//the env listener has already been initialized at some point...
    //let's clean up first...
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  idx_handler_ = base_env.GetIdxHandlerData();
  env_set_ = base_env.GetEnvSetData();
  uint num_residues = idx_handler_->GetNumResidues();
  env_data_ = new HBondSpatialOrganizerItem[num_residues];
  const ost::conop::AminoAcid* aa_data = base_env.GetAAData();
  n_pos_data_ = base_env.GetNPosData();
  ca_pos_data_ = base_env.GetCAPosData();
  c_pos_data_ = base_env.GetCPosData();
  o_pos_data_ = base_env.GetOPosData();

  for(uint i = 0; i < num_residues; ++i){
    env_data_[i].idx = i;
    env_data_[i].is_proline = (aa_data[i] == ost::conop::PRO);
  }
}

void HBondEnvListener::SetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondEnvListener::SetEnvironment", 2);

  for(std::vector<uint>::const_iterator i = idx.begin();
  	  i != idx.end(); ++i){
  	env_data_[*i].pos = ca_pos_data_[*i];
  	env_data_[*i].n_pos = n_pos_data_[*i];
  	env_data_[*i].c_pos = c_pos_data_[*i];
  	env_data_[*i].o_pos = o_pos_data_[*i];
  	env_.Add(&env_data_[*i], ca_pos_data_[*i]);
  }

  this->UpdateStatesAndHPos(idx);
}

void HBondEnvListener::ResetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondEnvListener::ResetEnvironment", 2);

  geom::Vec3 old_pos;
  for(std::vector<uint>::const_iterator i = idx.begin();
  	  i != idx.end(); ++i){
  	old_pos = env_data_[*i].pos;
    env_data_[*i].pos = ca_pos_data_[*i];
    env_data_[*i].n_pos = n_pos_data_[*i];
    env_data_[*i].c_pos = c_pos_data_[*i];
    env_data_[*i].o_pos = o_pos_data_[*i];
  	env_.Reset(&env_data_[*i], old_pos, ca_pos_data_[*i]);
  }

  this->UpdateStatesAndHPos(idx);
}

void HBondEnvListener::ClearEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "HBondEnvListener::ClearEnvironment", 2);

  for(std::vector<uint>::const_iterator i = idx.begin();
  	  i != idx.end(); ++i){
    env_.Remove(&env_data_[*i], env_data_[*i].pos);
  }  

  this->UpdateStatesAndHPos(idx);
}

void HBondEnvListener::UpdateStatesAndHPos(const std::vector<uint>& idx) {

  // we have no guarentee that the indices are consecutive...
  // we have to do the inefficient way and always add a full triplett 
  // to a set to get a unique set for further processing.
  std::set<uint> stuff_to_check;

  for(std::vector<uint>::const_iterator i = idx.begin();
      i != idx.end(); ++i){

    uint chain_idx = idx_handler_->GetChainIdx(*i);
    uint chain_start_idx = idx_handler_->GetChainIdx(chain_idx);
    uint chain_last_idx = idx_handler_->GetChainLastIdx(chain_idx);

    if(env_set_[*i]) {
      stuff_to_check.insert(*i);
    }

    if(*i > chain_start_idx && env_set_[*i - 1]) {
      stuff_to_check.insert(*i - 1);
    }

    if(*i < chain_last_idx && env_set_[*i + 1]) {
      stuff_to_check.insert(*i + 1);
    }
  }

  for(std::set<uint>::iterator i = stuff_to_check.begin(); 
      i != stuff_to_check.end(); ++i) {
    
    Real phi = -1.0472;
    Real psi = -0.78540;

    uint chain_idx = idx_handler_->GetChainIdx(*i);
    uint chain_start_idx = idx_handler_->GetChainIdx(chain_idx);
    uint chain_last_idx = idx_handler_->GetChainLastIdx(chain_idx);

    if(*i > chain_start_idx && *i < chain_last_idx && 
       env_set_[*i - 1] && env_set_[*i + 1]) {

      phi = geom::DihedralAngle(env_data_[*i-1].c_pos, env_data_[*i].n_pos,
                                env_data_[*i].pos, env_data_[*i].c_pos);
      psi = geom::DihedralAngle(env_data_[*i].n_pos, env_data_[*i].pos,
                                env_data_[*i].c_pos, env_data_[*i+1].n_pos);

      // we only assign a state if both, phi and psi, are set
      env_data_[*i].state = GetHBondState(phi,psi);
      
    }

    core::ConstructAtomPos(env_data_[*i].c_pos, env_data_[*i].pos,
                           env_data_[*i].n_pos, 0.9970, 2.0420,
                           phi + Real(M_PI), env_data_[*i].h_pos);
  }
}

}} // ns
