// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_ALL_ATOM_OVERALL_SCORER_HH
#define PM3_SCORING_ALL_ATOM_OVERALL_SCORER_HH

#include <promod3/scoring/all_atom_score_base.hh>
#include <boost/shared_ptr.hpp>
#include <map>

namespace promod3 { namespace scoring {

class AllAtomOverallScorer;
typedef boost::shared_ptr<AllAtomOverallScorer> AllAtomOverallScorerPtr;

// dummy score for intercept -> always returns 1
class AllAtomInterceptScorer : public AllAtomScorer {
public:
  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const {
    return 1.0;
  }
  virtual void
  CalculateScoreProfile(uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const {
    profile.assign(num_residues, 1.0);
  }
  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx) const { 
    return 1.0; 
  }

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                            const std::vector<uint>& num_residues, 
                            const std::vector<uint>& chain_idx, 
                            std::vector<std::vector<Real> >& profile) const {
    profile.resize(num_residues.size());
    for(uint i = 0; i < num_residues.size(); ++i) {
      profile[i].assign(num_residues[i], 1.0);
    }
  }
};

// simple map of key-scorer pairs
class AllAtomOverallScorer {

public:

  // check existance of scorer
  bool Contains(const String& key) const { return map_.find(key) != map_.end(); }

  // r/o get access (throws exc. if nothing found)
  AllAtomScorerPtr Get(const String& key) const {
    std::map<String, AllAtomScorerPtr>::const_iterator found = map_.find(key);
    if (found == map_.end()) {
      throw promod3::Error("OverallScorer does not contain scorer with key '"
                           + key + "'!");
    } else {
      return found->second;
    }
  }

  // r/w access to map
  AllAtomScorerPtr& operator[](const String& key) { return map_[key]; }

  // attach environment to all scorers
  void AttachEnvironment(loop::AllAtomEnv& env);

  // for convenience: get list of weight/scorer pairs to use on many bb_list
  // -> use special key "intercept" to add a constant value to the score
  std::vector<std::pair<Real, AllAtomScorerPtr> >
  GetWeightedScorers(const std::map<String, Real>& linear_weights) const;

  // linear combination of weighted scores
  // -> use special key "intercept" to add a constant value to the score
  Real CalculateLinearCombination(const std::map<String, Real>& linear_weights,
                                  uint start_resnum, uint num_residues,
                                  uint chain_idx = 0) const;

  Real CalculateLinearCombination(const std::map<String, Real>& linear_weights,
                                  const std::vector<uint>& start_resnum, 
                                  const std::vector<uint>& num_residues,
                                  const std::vector<uint>& chain_idx) const;
private:
  std::map<String, AllAtomScorerPtr> map_;
};

}} // ns

#endif
