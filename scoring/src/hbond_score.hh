// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_HBOND_SCORE_HH
#define PM3_SCORING_HBOND_SCORE_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/scoring/hbond_env_listener.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>
#include <set>

namespace promod3 { namespace scoring {

class HBondScorer;
typedef boost::shared_ptr<HBondScorer> HBondScorerPtr;


class HBondScorer : public BackboneScorer {

public:

  HBondScorer(Real min_d, Real max_d,
              Real min_alpha, Real max_alpha,
              Real min_beta, Real max_beta,
              Real min_gamma, Real max_gamma,
              uint d_bins, uint alpha_bins,
              uint beta_bins, uint gamma_bins);

  virtual ~HBondScorer();

  static HBondScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static HBondScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetEnergy(uint state, uint d_bin, uint alpha_bin, 
                 uint beta_bin, uint gamma_bin, Real e);

  void DoInternalScores(bool do_it) { do_internal_scores_ = do_it; }

  void DoExternalScores(bool do_it) { do_external_scores_ = do_it; }

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  void AttachEnvironment(BackboneScoreEnv& env);

private:

  HBondScorer(): do_internal_scores_(true), do_external_scores_(true),
                 normalize_(true), env_(NULL) { }

  inline uint EIdx(uint state, uint d_bin, uint alpha_bin, uint beta_bin, 
                   uint gamma_bin) const { 

    return  ((((state)*d_bins_+d_bin)*alpha_bins_+alpha_bin)*
            beta_bins_+beta_bin)*gamma_bins_+gamma_bin;
  }

  inline Real EvalHBondPotential(int state, const geom::Vec3& n, 
                                 const geom::Vec3& h, const geom::Vec3& ca, 
                                 const geom::Vec3& c, const geom::Vec3& o) const
  {
    Real d = geom::Distance(h,o);
    if (d < min_d_ || d >= max_d_) return 0.0;
    Real alpha = geom::Angle(n-h,o-h);
    if (alpha < min_alpha_ || alpha >= max_alpha_) return 0.0;
    Real beta = geom::Angle(h-o,c-o);
    if (beta < min_beta_ || beta >= max_beta_) return 0.0;
    Real gamma = geom::DihedralAngle(ca,c,o,h);
    if (gamma < min_gamma_ || gamma >= max_gamma_) return 0.0;
  
    int d_bin = (d - min_d_) / d_bin_size_;
    int alpha_bin = (alpha - min_alpha_) / alpha_bin_size_;
    int beta_bin = (beta - min_beta_) / beta_bin_size_;
    int gamma_bin = (gamma - min_gamma_) / gamma_bin_size_;

    return energies_[this->EIdx(state, d_bin, alpha_bin, beta_bin, gamma_bin)];
  }

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& external_scores,
             std::vector<Real>& internal_scores,
             std::vector<bool>& occupied) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    core::ConvertBaseType<float>(ds, min_d_); 
    core::ConvertBaseType<float>(ds, max_d_);           
    core::ConvertBaseType<float>(ds, min_alpha_); 
    core::ConvertBaseType<float>(ds, max_alpha_);
    core::ConvertBaseType<float>(ds, min_beta_); 
    core::ConvertBaseType<float>(ds, max_beta_);
    core::ConvertBaseType<float>(ds, min_gamma_); 
    core::ConvertBaseType<float>(ds, max_gamma_);
    core::ConvertBaseType<uint32_t>(ds, d_bins_); 
    core::ConvertBaseType<uint32_t>(ds, alpha_bins_);
    core::ConvertBaseType<uint32_t>(ds, beta_bins_); 
    core::ConvertBaseType<uint32_t>(ds, gamma_bins_);
    core::ConvertBaseType<float>(ds, d_bin_size_);
    core::ConvertBaseType<float>(ds, alpha_bin_size_);
    core::ConvertBaseType<float>(ds, beta_bin_size_);
    core::ConvertBaseType<float>(ds, gamma_bin_size_);
    uint num_values = 3 * d_bins_ * alpha_bins_ * beta_bins_ * gamma_bins_;
    if(ds.IsSource()) {
      energies_ = new Real[num_values];
      memset(energies_, 0, num_values * sizeof(Real)); 
    }
    for(uint i = 0; i < num_values; ++i) {
      core::ConvertBaseType<float>(ds, energies_[i]);
    }
  }

  Real min_d_; 
  Real max_d_;           
  Real min_alpha_; 
  Real max_alpha_;
  Real min_beta_; 
  Real max_beta_;
  Real min_gamma_; 
  Real max_gamma_;
  uint d_bins_; 
  uint alpha_bins_;
  uint beta_bins_; 
  uint gamma_bins_;
  Real d_bin_size_;
  Real alpha_bin_size_;
  Real beta_bin_size_;
  Real gamma_bin_size_;
  Real* energies_;  
  bool do_internal_scores_;
  bool do_external_scores_;
  bool normalize_;

  //environment specific information, that will be set upon calling 
  //AttachEnvironment
  HBondEnvListener* env_;
};


}}//ns

#endif
