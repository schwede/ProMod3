// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/cbeta_score.hh>


namespace promod3 { namespace scoring {

CBetaScorer::CBetaScorer(Real cutoff, uint bins, uint seq_sep): 
                         cutoff_(cutoff),
                         bins_(bins),
                         seq_sep_(seq_sep),
                         do_internal_scores_(true),
                         do_external_scores_(true),
                         normalize_(true),
                         env_(NULL) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::CBetaScorer", 2);

  if (cutoff < 0.0) {
    throw promod3::Error("CBeta distance cutoff must not be negative!");
  }
  if (bins == 0) {
    throw promod3::Error("Number of bins must be nonzero!");
  }
  if (seq_sep < 1) {
    throw promod3::Error("CBeta sequence separation must be at least 1!");
  }

  uint num_values = ost::conop::XXX * ost::conop::XXX * bins_;
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

CBetaScorer::~CBetaScorer() {
  delete [] energies_;
}

CBetaScorerPtr CBetaScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);

  // here starts the score specific stuff
  CBetaScorerPtr p(new CBetaScorer);
  in_stream.read(reinterpret_cast<char*>(&p->cutoff_), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->bins_), sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->seq_sep_), sizeof(uint));
  uint num_values = ost::conop::XXX * ost::conop::XXX * p->bins_;
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values*sizeof(Real));

  return p;
}

void CBetaScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&cutoff_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&bins_), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&seq_sep_), sizeof(uint));
  uint num_bytes = ost::conop::XXX * ost::conop::XXX * bins_ * sizeof(Real);
  out_stream.write(reinterpret_cast<char*>(energies_), num_bytes);
}

CBetaScorerPtr CBetaScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);

  // data
  CBetaScorerPtr p(new CBetaScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void CBetaScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void CBetaScorer::SetEnergy(ost::conop::AminoAcid a, ost::conop::AminoAcid b,
                            uint bin, Real e) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::SetEnergy", 2);

  if (a == ost::conop::XXX || b == ost::conop::XXX) {
    throw promod3::Error("Cannot set energy for invalid amino acid!");
  }

  if (bin >= bins_) {
    throw promod3::Error("Cannot set cbeta energy for invalid bin!");
  }

  energies_[this->EIdx(a, b, bin)] = e;
  energies_[this->EIdx(b, a, bin)] = e;
}

Real CBetaScorer::CalculateScore(uint start_resnum, uint num_residues,
                                 uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);

  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  // sum them
  Real external_score = 0.0;
  Real internal_score = 0.0;
  for (uint i = 0; i < external_scores.size(); ++i) {
    external_score += external_scores[i];
    internal_score += internal_scores[i];
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && !external_scores.empty()) {
    external_score /= external_scores.size();
  }

  return external_score;
}


void CBetaScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                        uint chain_idx,
                                        std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::CalculateScoreProfile", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);

  // get scores
  std::vector<Real> internal_scores;
  this->Score(idx_vector, profile, internal_scores, occupied);

  for(uint i = 0; i < profile.size(); ++i) {
    profile[i] += internal_scores[i];
  }
}


Real CBetaScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                 const std::vector<uint>& num_residues,
                                 const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}


void CBetaScorer::CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];
    }
  }
}


void CBetaScorer::Score(const std::vector<uint>& indices,
                        std::vector<Real>& external_scores,
                        std::vector<Real>& internal_scores,
                        std::vector<bool>& occupied) const {


  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaScorer::Score", 2);

  // setup
  uint n_indices = indices.size();

  external_scores.assign(n_indices, 0.0);
  internal_scores.assign(n_indices, 0.0);

  if(n_indices == 0) return;

  const Real inv_bin_size = bins_ / cutoff_;
  // let's reduce the cutoff a bit -> hack to ensure bin always within range
  const Real cutoff = cutoff_ - 0.0001;

  // let's go over every loop residue
  CBetaSpatialOrganizer::WithinResult within_result;
  const loop::IdxHandler* idx_handler = env_->GetIdxHandler();
  const int* env_set = env_->GetEnvSetData();
  std::pair<CBetaSpatialOrganizerItem*,Real>* a;
  for (uint i = 0; i < n_indices; ++i) {
    int my_idx = indices[i];

    if(!env_set[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    int my_chain_idx = idx_handler->GetChainIdx(my_idx);
    ost::conop::AminoAcid my_aa = env_->GetEnvData(my_idx).aa;

    within_result = env_->FindWithin(env_->GetEnvData(my_idx).pos, cutoff);
    a = within_result.first;
    
    for (uint j = 0; j < within_result.second; ++j) {
      int other_idx = int(a[j].first->idx);
      int other_chain_idx = idx_handler->GetChainIdx(other_idx);

      if(my_chain_idx != other_chain_idx || 
         std::abs(my_idx - other_idx) >= seq_sep_) {
        uint bin = a[j].second * inv_bin_size;
        Real e = energies_[this->EIdx(my_aa, a[j].first->aa, bin)];

        if(occupied[other_idx] && do_internal_scores_) {
          internal_scores[i] += e;
        }

        if(!occupied[other_idx] && do_external_scores_) {
          external_scores[i] += e;
        }
      }
    }
  }
}


void CBetaScorer::AttachEnvironment(BackboneScoreEnv& env) {
  env_ = env.UniqueListener<CBetaEnvListener>("CBetaEnvListener");
}

}}
