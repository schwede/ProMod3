// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/all_atom_interaction_score.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/check_io.hh>

namespace promod3 { namespace scoring {

AllAtomInteractionScorer::AllAtomInteractionScorer(
                         Real cutoff, uint bins, uint seq_sep)
                         : cutoff_(cutoff), bins_(bins), seq_sep_(seq_sep),
                           do_internal_scores_(true), do_external_scores_(true),
                           normalize_(true), attached_environment_(false) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                       "AllAtomInteractionScorer::AllAtomInteractionScorer", 2);

  if (cutoff < 0.0) {
    throw promod3::Error("AllAtom distance cutoff must not be negative!");
  }
  if (bins == 0) {
    throw promod3::Error("Number of bins must be nonzero!");
  }
  if (seq_sep < 1) {
    throw promod3::Error("AllAtom sequence separation must be at least 1!");
  }

  uint num_values = NumEnergies();
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

AllAtomInteractionScorer::~AllAtomInteractionScorer() {
  delete [] energies_;
}

AllAtomInteractionScorerPtr
AllAtomInteractionScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);

  // here starts the score specific stuff
  AllAtomInteractionScorerPtr p(new AllAtomInteractionScorer);
  in_stream.read(reinterpret_cast<char*>(&p->cutoff_), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->bins_), sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->seq_sep_), sizeof(uint));
  uint num_values = p->NumEnergies();
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values*sizeof(Real));

  return p;
}

void AllAtomInteractionScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&cutoff_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&bins_), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&seq_sep_), sizeof(uint));
  uint num_bytes = NumEnergies() * sizeof(Real);
  out_stream.write(reinterpret_cast<char*>(energies_), num_bytes);
}

AllAtomInteractionScorerPtr
AllAtomInteractionScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);

  // data
  AllAtomInteractionScorerPtr p(new AllAtomInteractionScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void AllAtomInteractionScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void AllAtomInteractionScorer::SetEnergy(loop::AminoAcidAtom a,
                                         loop::AminoAcidAtom b,
                                         uint bin, Real e) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::SetEnergy", 2);

  if (a == loop::XXX_NUM_ATOMS || b == loop::XXX_NUM_ATOMS) {
    throw promod3::Error("Cannot set energy for invalid heavy atom type!");
  }

  if (bin >= bins_) {
    throw promod3::Error("Cannot set cbeta energy for invalid bin!");
  }

  energies_[this->EIdx(a, b, bin)] = e;
  energies_[this->EIdx(b, a, bin)] = e;
}

Real AllAtomInteractionScorer::CalculateScore(uint start_resnum,
                                              uint num_residues,
                                              uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "AllAtomInteractionScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, idx_vec, occupied);

  // get scores
  std::vector<Real> ex_scores, in_scores;
  this->Score(idx_vec, ex_scores, in_scores, occupied);
  // sum them
  Real ex_score = 0.0;
  Real in_score = 0.0;
  for (uint i = 0; i < num_residues; ++i) {
    ex_score += ex_scores[i];
    in_score += in_scores[i];
  }

  // we count internal scores half as the same pair appears twice
  Real score = ex_score + Real(0.5)*in_score;

  if(normalize_ && !in_scores.empty()) {
    score /= in_scores.size();
  }

  return score;
}

void AllAtomInteractionScorer::CalculateScoreProfile(
                        uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                          "AllAtomInteractionScorer::CalculateScoreProfile", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // get scores
  std::vector<uint> idx_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, idx_vec, occupied);

  std::vector<Real> in_scores;
  this->Score(idx_vec, profile, in_scores, occupied);
  // combine them
  for (uint i = 0; i < num_residues; ++i) {
    profile[i] = profile[i] + in_scores[i];
  }
}

Real AllAtomInteractionScorer::CalculateScore(
                                    const std::vector<uint>& start_resnum, 
                                    const std::vector<uint>& num_residues,
                                    const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec, 
                                      occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}

void AllAtomInteractionScorer::CalculateScoreProfile(
                              const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec, 
                                      occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];
    }
  }
}

void AllAtomInteractionScorer::AttachEnvironment(loop::AllAtomEnv& env) {
  env_ = env.UniqueListener<AllAtomInteractionEnvListener>(
                           "AllAtomInteractionEnvListener");
  idx_handler_ = env.GetIdxHandlerData();
  all_pos_ = env.GetAllPosData();
  attached_environment_ = true;
}

void AllAtomInteractionScorer::Score(const std::vector<uint>& indices,
                                     std::vector<Real>& external_scores,
                                     std::vector<Real>& internal_scores,
                                     std::vector<bool>& occupied) const {

  // setup
  uint n_indices = indices.size();
  external_scores.resize(n_indices);
  internal_scores.resize(n_indices);

  if(n_indices == 0) {
    return;
  }

  const Real inv_bin_size = bins_ / cutoff_;
  // let's reduce the cutoff a bit -> hack to ensure bin always within range
  const Real cutoff = cutoff_ - 0.001;

  // let's go over every residue
  AllAtomInteractionSpatialOrganizer::WithinResult within_result;
  std::pair<AllAtomInteractionSpatialOrganizerItem*,Real>* a;
  // helpers
  int neglect_range = int(seq_sep_) - 1;
  int min_idx_chain, max_idx_chain, start_neglect, end_neglect;

  for (uint i = 0; i < indices.size(); ++i) {
    // init
    const uint res_idx = indices[i];
    Real ex_score = 0.0;
    Real in_score = 0.0;
    // check neglect range for residue
    const uint chain_idx = idx_handler_->GetChainIdx(res_idx);
    min_idx_chain = idx_handler_->GetChainStartIdx(chain_idx);
    max_idx_chain = idx_handler_->GetChainLastIdx(chain_idx);
    start_neglect = std::max(min_idx_chain, int(res_idx) - neglect_range);
    end_neglect = std::min(max_idx_chain, int(res_idx) + neglect_range);
    // loop over all heavy atoms
    const uint first_idx = all_pos_->GetFirstIndex(res_idx);
    const uint last_idx = all_pos_->GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      // only if set
      if (all_pos_->IsSet(idx)) {
        const loop::AminoAcidAtom myaaa = all_pos_->GetAAA(idx);
        within_result = env_->FindWithin(all_pos_->GetPos(idx), cutoff);
        a = within_result.first;
        for (uint j = 0; j < within_result.second; ++j) {
          const int other_res_idx = int(a[j].first->res_idx);
          if (other_res_idx < start_neglect || other_res_idx > end_neglect) {
            const uint bin = a[j].second * inv_bin_size;
            if(occupied[other_res_idx]) {
              // internal interaction
              if(do_internal_scores_) {
                in_score += energies_[this->EIdx(myaaa, a[j].first->aaa, bin)];
              } 
            } else {
              // external interaction
              if(do_external_scores_) {
                ex_score += energies_[this->EIdx(myaaa, a[j].first->aaa, bin)];
              }
            }
          }
        }
      }
    }
    // set values
    external_scores[i] = ex_score;
    internal_scores[i] = in_score;
  }
}

}} // ns
