// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_REDUCED_SCORE_HH
#define PM3_SCORING_REDUCED_SCORE_HH

#include <promod3/scoring/reduced_env_listener.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>

namespace promod3 { namespace scoring {

class ReducedScorer;
typedef boost::shared_ptr<ReducedScorer> ReducedScorerPtr;


class ReducedScorer : public BackboneScorer {

public:

  ReducedScorer(Real cutoff, uint dist_bins, uint angle_bins,
                uint dihedral_bins, uint seq_sep);

  virtual ~ReducedScorer();

  static ReducedScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static ReducedScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetEnergy(ost::conop::AminoAcid a, ost::conop::AminoAcid b,
                 uint dist_bin, uint alpha_bin, uint beta_bin, uint gamma_bin,
                 Real e);

  void DoInternalScores(bool do_it) { do_internal_scores_ = do_it; }

  void DoExternalScores(bool do_it) { do_external_scores_ = do_it; }

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  void AttachEnvironment(BackboneScoreEnv& env);

private:

  ReducedScorer(): do_internal_scores_(true), do_external_scores_(true),
                   normalize_(true), env_(NULL) { }

  inline uint EIdx(ost::conop::AminoAcid a, ost::conop::AminoAcid b,
                   uint dist_bin, uint alpha_bin, uint beta_bin,
                   uint gamma_bin) const {
    return ((((a * ost::conop::XXX + b) * dist_bins_ + dist_bin)
             * angle_bins_ + alpha_bin) * angle_bins_ + beta_bin)
           * dihedral_bins_ + gamma_bin;
  }
  inline uint NumEnergies() const {
    return ost::conop::XXX * ost::conop::XXX * dist_bins_ * angle_bins_
           * angle_bins_ * dihedral_bins_;
  }

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& external_scores,
             std::vector<Real>& internal_scores,
             std::vector<bool>& occupied) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    core::ConvertBaseType<float>(ds, cutoff_);
    core::ConvertBaseType<uint32_t>(ds, dist_bins_);
    core::ConvertBaseType<uint32_t>(ds, angle_bins_);
    core::ConvertBaseType<uint32_t>(ds, dihedral_bins_);
    core::ConvertBaseType<uint32_t>(ds, seq_sep_);
    uint num_values = NumEnergies();
    if (ds.IsSource()) energies_ = new Real[num_values];
    for (uint i = 0; i < num_values; ++i) {
      core::ConvertBaseType<float>(ds, energies_[i]);
    }
  }

  Real cutoff_;
  uint dist_bins_;
  uint angle_bins_;
  uint dihedral_bins_;
  uint seq_sep_;
  Real* energies_;
  bool do_internal_scores_;
  bool do_external_scores_;
  bool normalize_;

  //environment specific information, that will be set upon calling 
  //AttachEnvironment
  ReducedEnvListener* env_;
};


}}//ns

#endif
