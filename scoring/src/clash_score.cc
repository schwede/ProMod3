// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/clash_score.hh>
#include <promod3/scoring/scwrl3_energy_functions.hh>


namespace promod3 { namespace scoring {

ClashScorer::ClashScorer(): do_internal_scores_(true), 
                            do_external_scores_(true), 
                            normalize_(true), 
                            env_(NULL) { 

  for(uint i = 0; i < BB_CLASH_NUM; ++i) {
    for(uint j = 0; j < BB_CLASH_NUM; ++j) {
      clash_scores_[i][j] = new Real[bins_];
    }
  }  

  // setup radii
  Real radius[BB_CLASH_NUM] = {1.6, 1.3, 1.3};

  // estimate clash scores
  for(uint i = 0; i < BB_CLASH_NUM; ++i) {
    for(uint j = i; j < BB_CLASH_NUM; ++j) {
      const Real Rij = radius[i] + radius[j];
      for(uint bin = 0; bin < bins_; ++bin) {
        Real d = static_cast<Real>(bin) / bins_per_a_;
        clash_scores_[i][j][bin] = SCWRL3PairwiseScore(d, Rij);
      }
      if(i != j) {
        memcpy(clash_scores_[j][i], clash_scores_[i][j], bins_ * sizeof(Real));
      }
    }
  }
}


ClashScorer::~ClashScorer() {
  for(uint i = 0; i < BB_CLASH_NUM; ++i) {
    for(uint j = 0; j < BB_CLASH_NUM; ++j) {
      delete [] clash_scores_[i][j];
    }
  }  
}


Real ClashScorer::CalculateScore(uint start_resnum, uint num_residues,
                                 uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);

  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  Real external_score = 0.0;
  Real internal_score = 0.0;
  for(uint i = 0; i < external_scores.size(); ++i) {
    external_score += external_scores[i];
    internal_score += internal_scores[i];
  }

  // all interactions in internal score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && ! external_scores.empty()) {
    external_score /= external_scores.size();
  }
  
  return external_score;
}


void ClashScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                        uint chain_idx,
                                        std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashScorer::CalculateScoreProfile", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);

  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  profile.resize(external_scores.size());
  for(uint i = 0; i < external_scores.size(); ++i) {
    profile[i] = external_scores[i] + internal_scores[i];
  }
}


Real ClashScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                 const std::vector<uint>& num_residues,
                                 const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}


void ClashScorer::CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];

    }
  }
}


void ClashScorer::Score(const std::vector<uint>& indices,
                        std::vector<Real>& external_scores,
                        std::vector<Real>& internal_scores,
                        std::vector<bool>& occupied) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashScorer::Score", 2);

  // setup

  uint n_indices = indices.size();

  external_scores.assign(n_indices, 0.0);
  internal_scores.assign(n_indices, 0.0);

  if(n_indices == 0) return;

  uint seq_sep = 2;
  uint bin;
  Real score;
  const int* env_set = env_->GetEnvSetData();

  // let's go over every loop residue
  ClashSpatialOrganizer::WithinResult within_result;
  std::pair<ClashSpatialOrganizerItem*,Real>* a;

  for (uint i = 0; i < n_indices; ++i) {
    int my_idx = indices[i];

    if(!env_set[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    uint n_particles = env_->IsGlycine(my_idx) ? 4 : 5;
    ClashSpatialOrganizerItem* particle_data = env_->GetParticleData(my_idx);

    for(uint j = 0; j < n_particles; ++j) {
      int clash_type = particle_data[j].clash_type;
      within_result = env_->FindWithin(particle_data[j].pos, 3.2);
      a = within_result.first;

      for(uint k = 0; k < within_result.second; ++k) {
        int other_idx = int(a[k].first->idx);

        if(std::abs(my_idx - other_idx) >= seq_sep) {
          bin = a[k].second * bins_per_a_;
          score = clash_scores_[clash_type][a[k].first->clash_type][bin];

          if(occupied[other_idx] && do_internal_scores_) {
            internal_scores[i] += score;
          }

          if(!occupied[other_idx] && do_external_scores_) {
            external_scores[i] += score;
          }
        }
      }
    }
  }
}

void ClashScorer::AttachEnvironment(BackboneScoreEnv& env) {
  env_ = env.UniqueListener<ClashEnvListener>("ClashEnvListener");
}

}}
