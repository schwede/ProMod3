// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_HBOND_ENV_LISTENER_HH
#define PM3_SCORING_HBOND_ENV_LISTENER_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>
#include <set>

namespace promod3 { namespace scoring {

inline int GetHBondState(Real phi, Real psi){

  //check whether phi/psi pair is typical for
  //helix => -180 < phi < -20 ; -90 < psi < -10
  if(-Real(M_PI) < phi && phi < Real(-0.34907) &&
     -Real(M_PI)/2 < psi && psi < Real(-0.17453)) return 1;

  //check whether phi/psi pair is typical for
  //sheet => -180 < phi < -20 ; 20 < psi < 180 or -180 < psi < -170
  if(-Real(M_PI) < phi && phi < Real(-0.34907) &&
     (( 0.34907 < psi && psi < Real(M_PI)) || 
      (-Real(M_PI) < psi && psi < Real(-2.9671)))) return 2; 

  return 0;
}

struct HBondSpatialOrganizerItem {
  HBondSpatialOrganizerItem() { }

  HBondSpatialOrganizerItem(const geom::Vec3& p, const geom::Vec3& n,
                            const geom::Vec3& h, const geom::Vec3& c,
                            const geom::Vec3& o, uint state, uint i, 
                            uint c_i): pos(p), n_pos(n),
                                       h_pos(h), c_pos(c),
                                       o_pos(o), idx(i) { }

  geom::Vec3 pos; //CA position of residue
  geom::Vec3 n_pos; //donor N
  geom::Vec3 h_pos; //donor H
  geom::Vec3 c_pos; //acceptor C
  geom::Vec3 o_pos; //acceptor O
  uint8_t state;
  uint idx;
  bool is_proline;
};

typedef core::DynamicSpatialOrganizer<HBondSpatialOrganizerItem>
        HBondSpatialOrganizer;

class HBondEnvListener : public BackboneScoreEnvListener{

public:

  HBondEnvListener();

  virtual ~HBondEnvListener();

  virtual void Init(const BackboneScoreEnv& base_env);

  virtual void SetEnvironment(const std::vector<uint>& idx);

  virtual void ResetEnvironment(const std::vector<uint>& idx);

  virtual void ClearEnvironment(const std::vector<uint>& idx);

  virtual String WhoAmI() const { return "HBondEnvListener"; }

  const HBondSpatialOrganizerItem& GetEnvData(uint idx) const {
    return env_data_[idx];
  }

  const int* GetEnvSetData() { return env_set_; }

  const loop::IdxHandler* GetIdxHandler() { return idx_handler_; }
  
  HBondSpatialOrganizer::WithinResult FindWithin(const geom::Vec3& pos,
                                                 Real cutoff) const {
    return env_.FindWithin(pos, cutoff);
  }

private:

  void UpdateStatesAndHPos(const std::vector<uint>& idx);

  HBondSpatialOrganizer env_;  
  HBondSpatialOrganizerItem* env_data_;

  // data ptrs fetched from base env at initialization
  const int* env_set_;
  const loop::IdxHandler* idx_handler_;
  const geom::Vec3* n_pos_data_;
  const geom::Vec3* ca_pos_data_;
  const geom::Vec3* c_pos_data_;
  const geom::Vec3* o_pos_data_;
}; 

}} // ns

#endif
