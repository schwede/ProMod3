// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/reduced_env_listener.hh>


namespace promod3 { namespace scoring {

ReducedEnvListener::ReducedEnvListener(): env_(12.0),
                                          env_data_(NULL) { }

ReducedEnvListener::~ReducedEnvListener() {
  if (env_data_ != NULL) delete [] env_data_;
}

void ReducedEnvListener::Init(const BackboneScoreEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedEnvListener::Init", 2);

  if (env_data_ != NULL) {
    // the env listener has already been initialized at some point...
    // let's clean up first...
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  idx_handler_ = base_env.GetIdxHandlerData();
  uint num_residues = idx_handler_->GetNumResidues();
  env_data_ = new ReducedSpatialOrganizerItem[num_residues];
  const ost::conop::AminoAcid* aa_data = base_env.GetAAData();
  env_set_ = base_env.GetEnvSetData();
  ca_pos_data_ = base_env.GetCAPosData();
  c_pos_data_ = base_env.GetCPosData();
  n_pos_data_ = base_env.GetNPosData();
  
  for (uint i = 0; i < num_residues; ++i) {
    env_data_[i].idx = i;
    env_data_[i].aa = aa_data[i];
  }
}

void ReducedEnvListener::SetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedEnvListener::SetEnvironment", 2);

  geom::Vec3 ca_pos, c_pos, n_pos;
  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    ca_pos = ca_pos_data_[*i];
    c_pos = c_pos_data_[*i];
    n_pos = n_pos_data_[*i];
    env_data_[*i].pos = ca_pos;
    env_data_[*i].axis = geom::Normalize(geom::Normalize(ca_pos-n_pos) +
                                         geom::Normalize(ca_pos-c_pos));
    env_.Add(&env_data_[*i], ca_pos);
  }
}

void ReducedEnvListener::ResetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedEnvListener::ResetEnvironment", 2);

  geom::Vec3 ca_pos, c_pos, n_pos, old_pos;
  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    old_pos = env_data_[*i].pos;
    ca_pos = ca_pos_data_[*i];
    c_pos = c_pos_data_[*i];
    n_pos = n_pos_data_[*i];
    env_data_[*i].pos = ca_pos;
    env_data_[*i].axis = geom::Normalize(geom::Normalize(ca_pos-n_pos) +
                                         geom::Normalize(ca_pos-c_pos));
    env_.Reset(&env_data_[*i], old_pos, ca_pos);
  }
}

void ReducedEnvListener::ClearEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedEnvListener::ClearEnvironment", 2);

  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    env_.Remove(&env_data_[*i], env_data_[*i].pos);
  }
}

}}
