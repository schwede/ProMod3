// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/all_atom_packing_score.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/check_io.hh>

namespace promod3 { namespace scoring {

AllAtomPackingScorer::AllAtomPackingScorer(Real cutoff, uint max_count):
                                           cutoff_(cutoff),
                                           max_count_(max_count),
                                           normalize_(true),
                                           attached_environment_(false) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                       "AllAtomPackingScorer::AllAtomPackingScorer", 2);

  if (cutoff < 0.0) {
    throw promod3::Error("AllAtom distance cutoff must not be negative!");
  }
  if (max_count == 0) {
    throw promod3::Error("Max count must be nonzero!");
  }

  uint num_values = NumEnergies();
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

AllAtomPackingScorer::~AllAtomPackingScorer() {
  delete [] energies_;
}

AllAtomPackingScorerPtr
AllAtomPackingScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPackingScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);

  // here starts the score specific stuff
  AllAtomPackingScorerPtr p(new AllAtomPackingScorer);
  in_stream.read(reinterpret_cast<char*>(&p->cutoff_), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->max_count_), sizeof(uint));
  uint num_values = p->NumEnergies();
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values*sizeof(Real));

  return p;
}

void AllAtomPackingScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPackingScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&cutoff_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&max_count_), sizeof(uint));
  uint num_bytes = NumEnergies() * sizeof(Real);
  out_stream.write(reinterpret_cast<char*>(energies_), num_bytes);
}

AllAtomPackingScorerPtr
AllAtomPackingScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPackingScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);

  // data
  AllAtomPackingScorerPtr p(new AllAtomPackingScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void AllAtomPackingScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPackingScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void AllAtomPackingScorer::SetEnergy(loop::AminoAcidAtom aaa, uint count,
                                     Real e) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPackingScorer::SetEnergy", 2);

  if (aaa == loop::XXX_NUM_ATOMS) {
    throw promod3::Error("Cannot set energy for invalid heavy atom type!");
  }

  if (count > max_count_) {
    throw promod3::Error("Invalid packing count!");
  }

  energies_[this->EIdx(aaa, count)] = e;
}

Real AllAtomPackingScorer::CalculateScore(uint start_resnum, uint num_residues,
                                          uint chain_idx) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "AllAtomPackingScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  std::vector<Real> scores;  
  std::vector<uint> idx_vector = 
  idx_handler_->ToIdxVector(start_resnum, num_residues, chain_idx);

  this->Score(idx_vector, scores);
  // sum / normalize them
  Real score = 0.0;
  for (uint i = 0; i < num_residues; ++i) {
    score += scores[i];
  }

  if(normalize_ && !scores.empty()) {
    score /= scores.size();
  }

  return score;
}

void AllAtomPackingScorer::CalculateScoreProfile(
                        uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "AllAtomPackingScorer::CalculateScoreProfile", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // get scores
  std::vector<uint> idx_vector = 
  idx_handler_->ToIdxVector(start_resnum, num_residues, chain_idx);
  Score(idx_vector, profile);
}

Real AllAtomPackingScorer::CalculateScore(
                                    const std::vector<uint>& start_resnum, 
                                    const std::vector<uint>& num_residues,
                                    const std::vector<uint>& chain_idx) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPackingScorer::CalculateScore", 2);


  // setup
  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  std::vector<std::vector<uint> > indices_vec;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    indices_vec.push_back(idx_handler_->ToIdxVector(start_resnum[i], 
                                                    num_residues[i], 
                                                    chain_idx[i]));
  }

  // get scores and sum them up
  Real score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < indices_vec.size(); ++i) {
    std::vector<Real> scores;
    this->Score(indices_vec[i], scores);
    for(uint j = 0; j < scores.size(); ++j) {
      score += scores[j];
    }
    n_scored_residues += scores.size();
  }

  if(normalize_ && n_scored_residues > 0) {
    score /= n_scored_residues;
  }

  return score;
}

void AllAtomPackingScorer::CalculateScoreProfile(
                              const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::CalculateScoreProfile", 2);

  // setup
  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  // get scores
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<uint> idx_vector = 
    idx_handler_->ToIdxVector(start_resnum[i], num_residues[i], 
                                       chain_idx[i]);
    this->Score(idx_vector, profile[i]);
  }
}

void AllAtomPackingScorer::AttachEnvironment(loop::AllAtomEnv& env) {
  env_ = env.UniqueListener<AllAtomInteractionEnvListener>(
                           "AllAtomInteractionEnvListener");
  idx_handler_ = env.GetIdxHandlerData();
  all_pos_ = env.GetAllPosData();
  attached_environment_ = true;
}

void AllAtomPackingScorer::Score(const std::vector<uint>& indices,
                                 std::vector<Real>& scores) const {
  // setup
  uint n_indices = indices.size();
  scores.resize(n_indices);

  if(n_indices == 0) {
    return;
  }

  // let's go over every residue
  AllAtomInteractionSpatialOrganizer::WithinResult within_result;
  std::pair<AllAtomInteractionSpatialOrganizerItem*,Real>* a;
  for (uint i = 0; i < n_indices; ++i) {
    const uint res_idx = indices[i];
    // loop over all heavy atoms
    Real score = 0.0;
    const uint first_idx = all_pos_->GetFirstIndex(res_idx);
    const uint last_idx = all_pos_->GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      // only if set
      if (all_pos_->IsSet(idx)) {
        // count neighbors which are not in same residue
        uint counter = 0;
        within_result = env_->FindWithin(all_pos_->GetPos(idx), cutoff_);
        a = within_result.first;
        for (uint j = 0; j < within_result.second; ++j) {
          const uint other_idx = int(a[j].first->res_idx);
          if (other_idx < res_idx || other_idx > res_idx) ++counter;
        }
        // score it
        counter = std::min(counter, max_count_);
        score += energies_[this->EIdx(all_pos_->GetAAA(idx), counter)];
      }
    }
    // set values
    scores[i] = score;
  }
}

}} // ns
