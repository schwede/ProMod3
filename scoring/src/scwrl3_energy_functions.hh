// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCWRL3_ENERGY_FUNCTIONS_HH
#define PM3_SCWRL3_ENERGY_FUNCTIONS_HH

#include <ost/geom/vec3.hh>
#include <ost/geom/vecmat3_op.hh>

namespace promod3 { namespace scoring {

inline Real SCWRL3PairwiseScore(Real d, Real Rij) {
  Real e = 0.0;
  if(d < Real(0.8254) * Rij) {
    e = 10.0;
  } else if(d <= Rij) {
    e = 57.273 * (1 - d/Rij);
  }
  return e;
}


inline Real SCWRL3DisulfidScore(const geom::Vec3& ca_pos_one, 
                         const geom::Vec3& cb_pos_one,
                         const geom::Vec3& sg_pos_one, 
                         const geom::Vec3& ca_pos_two, 
                         const geom::Vec3& cb_pos_two, 
                         const geom::Vec3& sg_pos_two) {

  Real d = geom::Distance(sg_pos_one,sg_pos_two);
  Real a1 = geom::Angle(cb_pos_one-sg_pos_one, sg_pos_two-sg_pos_one);
  Real a2 = geom::Angle(sg_pos_one-sg_pos_two, cb_pos_two-sg_pos_two);
  Real x2 = std::abs(geom::DihedralAngle(ca_pos_one, cb_pos_one, sg_pos_one,
                                         sg_pos_two));
  Real x3 = std::abs(geom::DihedralAngle(cb_pos_one, sg_pos_one, sg_pos_two,
                                         cb_pos_two));
  Real x4 = std::abs(geom::DihedralAngle(sg_pos_one, sg_pos_two, cb_pos_two,
                                         ca_pos_two));

  return std::abs(d-2) / Real(0.05)
         + std::abs(a1-Real(1.8151)) / Real(0.087266)
         + std::abs(a2-Real(1.8151)) / Real(0.087266)
         + std::min(std::abs(x2-Real(1.3963)), std::abs(x2-Real(M_PI)))
           / Real(0.17453)
         + std::min(std::abs(x4-Real(1.3963)), std::abs(x4-Real(M_PI)))
           / Real(0.17453)
         + std::abs(x3-Real(M_PI)*Real(0.5)) / Real(0.34907);
}

}}//ns

#endif
