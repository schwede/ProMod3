// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_ALL_ATOM_SCORE_BASE_HH
#define PM3_SCORING_ALL_ATOM_SCORE_BASE_HH

#include <promod3/loop/all_atom_env.hh>
#include <promod3/core/message.hh>
#include <boost/shared_ptr.hpp>

namespace promod3 { namespace scoring {

class AllAtomScorer;
typedef boost::shared_ptr<AllAtomScorer> AllAtomScorerPtr;

/// \brief Base class for all atom scorers.
class AllAtomScorer {

public:

  virtual ~AllAtomScorer() { }

  // score loop (loop and (!) relevant surrounding must be in environment!)
  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const = 0;
  // fill score profile (score per residue), otherwise as above
  virtual void
  CalculateScoreProfile(uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const = 0;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx) const = 0;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                            const std::vector<uint>& num_residues, 
                            const std::vector<uint>& chain_idx, 
                            std::vector<std::vector<Real> >& profile) const = 0;

  virtual void AttachEnvironment(loop::AllAtomEnv& env) { }
};

}} // ns

#endif
