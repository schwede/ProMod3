// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/all_atom_overall_scorer.hh>

namespace promod3 { namespace scoring {

void AllAtomOverallScorer::AttachEnvironment(loop::AllAtomEnv& env) {
  for (std::map<String, AllAtomScorerPtr>::const_iterator i = map_.begin();
       i != map_.end(); ++i) {
    i->second->AttachEnvironment(env);
  }
}

std::vector<std::pair<Real, AllAtomScorerPtr> >
AllAtomOverallScorer::GetWeightedScorers(
                            const std::map<String, Real>& weights) const
{
  std::vector<std::pair<Real, AllAtomScorerPtr> > weighted_scores;
  for (std::map<String, Real>::const_iterator i = weights.begin();
       i != weights.end(); ++i) {
    std::pair<Real, AllAtomScorerPtr> toadd;
    toadd.first = i->second;
    if (i->first == "intercept") {
      toadd.second = AllAtomScorerPtr(new AllAtomInterceptScorer);
    } else {
      // error checking done in Get
      toadd.second = this->Get(i->first);
    }
    weighted_scores.push_back(toadd);
  }
  return weighted_scores;
}

Real AllAtomOverallScorer::CalculateLinearCombination(
                            const std::map<String, Real>& linear_weights,
                            uint start_resnum, uint num_residues,
                            uint chain_idx) const {
  // loop and sum up
  Real score = 0;
  for (std::map<String, Real>::const_iterator i = linear_weights.begin();
       i != linear_weights.end(); ++i) {
    if (i->first == "intercept") {
      score += i->second;
    } else {
      // error checking done by Get
      AllAtomScorerPtr scorer = this->Get(i->first);
      score += i->second * scorer->CalculateScore(start_resnum, num_residues,
                                                  chain_idx);
    }
  }
  return score;
}

Real AllAtomOverallScorer::CalculateLinearCombination(
                            const std::map<String, Real>& linear_weights,
                            const std::vector<uint>& start_resnum, 
                            const std::vector<uint>& num_residues,
                            const std::vector<uint>& chain_idx) const {
  // loop and sum up
  Real score = 0;
  for (std::map<String, Real>::const_iterator i = linear_weights.begin();
       i != linear_weights.end(); ++i) {
    if (i->first == "intercept") {
      score += i->second;
    } else {
      // error checking done by Get
      AllAtomScorerPtr scorer = this->Get(i->first);
      score += i->second * scorer->CalculateScore(start_resnum, num_residues, 
                                                  chain_idx);
    }
  }
  return score;
}

}} // ns
