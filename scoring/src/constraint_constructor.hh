// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_CONSTRAINT_CONSTRUCTOR_HH
#define PM3_SCORING_CONSTRAINT_CONSTRUCTOR_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <ost/seq/alignment_handle.hh>
#include <boost/shared_ptr.hpp>
#include <vector>


namespace promod3 { namespace scoring {

class DiscoContainer;
typedef boost::shared_ptr<DiscoContainer> DiscoContainerPtr;

class DiscoContainer{

public:

  DiscoContainer(const ost::seq::SequenceHandle& seqres, 
                 promod3::scoring::PairwiseFunctionType ft = 
                 promod3::scoring::CA_PAIRWISE_FUNCTION);

  void AddStructuralInfo(const ost::seq::AlignmentHandle& aln);

  void AttachConstraints(promod3::scoring::BackboneScoreEnv& env, 
                         const std::vector<uint>& chain_indices,
                         Real cluster_thresh = 0.50, Real gamma = 70.0); 

private:
  promod3::scoring::PairwiseFunctionType function_type_;
  ost::seq::SequenceHandle seqres_;
  std::vector<std::vector<geom::Vec3> > positions_;
  std::vector<std::vector<bool> > valid_positions_;
  ost::seq::AlignmentList alignments_;
};


void AttachEvolutionaryConstraints(promod3::scoring::BackboneScoreEnv& env, 
                                   const ost::seq::AlignmentHandle& aln,
                                   const std::vector<uint>& chain_indices,
                                   promod3::scoring::PairwiseFunctionType ft=
                                   promod3::scoring::CB_PAIRWISE_FUNCTION,
                                   Real fraction_constraints = 0.1, 
                                   uint constant_function_length = 8,
                                   uint tail_length = 4); 

}} //ns

#endif
