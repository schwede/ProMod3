// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/all_atom_clash_score.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/check_io.hh>
#include <promod3/scoring/scwrl3_energy_functions.hh>

namespace promod3 { namespace scoring {

AllAtomClashScorer::AllAtomClashScorer(): do_internal_scores_(true),
                                          do_external_scores_(true),
                                          normalize_(true),
                                          attached_environment_(false) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                       "AllAtomClashScorer::AllAtomClashScorer", 2);

  // setup clash types
  const loop::AminoAcidLookup& aa_lookup = loop::AminoAcidLookup::GetInstance();
  for (uint idx = 0; idx < loop::XXX_NUM_ATOMS; ++idx) {
    const loop::AminoAcidAtom aaa = loop::AminoAcidAtom(idx);
    const String& element_str = aa_lookup.GetElement(aaa);
    char element = toupper(element_str[0]);
    switch (element) {
      case 'C': {
        clash_type_[idx] = ALL_ATOM_CLASH_C;
        break;
      }
      case 'N': {
        clash_type_[idx] = ALL_ATOM_CLASH_N;
        break;
      }
      case 'O': {
        clash_type_[idx] = ALL_ATOM_CLASH_O;
        break;
      }
      case 'S': {
        clash_type_[idx] = ALL_ATOM_CLASH_S;
        break;
      }
      default: {
        // SHOULD NEVER, EVER HAPPEN
        throw promod3::Error("Invalid element " + element_str);
      }
    }
  }
  // special treatment for disulfid bridges
  clash_type_[loop::CYS_CB] = ALL_ATOM_CLASH_CYS_CB;
  clash_type_[loop::CYS_SG] = ALL_ATOM_CLASH_CYS_SG;

  // setup radii
  Real radius[ALL_ATOM_CLASH_NUM] = {1.6, 1.3, 1.3, 1.7, 1.6, 1.7};
  cutoff_[ALL_ATOM_CLASH_C] = 3.3;
  cutoff_[ALL_ATOM_CLASH_N] = 3.0;
  cutoff_[ALL_ATOM_CLASH_O] = 3.0;
  cutoff_[ALL_ATOM_CLASH_S] = 3.4;
  cutoff_[ALL_ATOM_CLASH_CYS_CB] = 3.3;
  cutoff_[ALL_ATOM_CLASH_CYS_SG] = 3.4;

  // energies for each AllAtomClashType pair
  // -> see Eq. (11) in Canutescu et al., 2003 "A graph-theory algorithm ..."
  const uint num_values = ALL_ATOM_CLASH_NUM * ALL_ATOM_CLASH_NUM * bins_;
  energies_ = new Real[num_values];
  for (uint i = 0; i < ALL_ATOM_CLASH_NUM; ++i) {
    const AllAtomClashType a = AllAtomClashType(i);
    for (uint j = 0; j < ALL_ATOM_CLASH_NUM; ++j) {
      const AllAtomClashType b = AllAtomClashType(j);
      Real Rij = radius[i] + radius[j];
      // special treatment for disulfid bridges
      if (a == ALL_ATOM_CLASH_CYS_SG && b == ALL_ATOM_CLASH_CYS_SG) Rij = 1.5;
      else if (   (a == ALL_ATOM_CLASH_CYS_SG && b == ALL_ATOM_CLASH_CYS_CB)
               || (a == ALL_ATOM_CLASH_CYS_CB && b == ALL_ATOM_CLASH_CYS_SG)) {
        Rij = 2.5;
      }
      for (uint bin = 0; bin < bins_; ++bin) {
        const Real d = static_cast<Real>(bin) / bins_per_a_;
        energies_[this->EIdx(a, b, bin)] = SCWRL3PairwiseScore(d, Rij);
      }
    }
  }
}

AllAtomClashScorer::~AllAtomClashScorer() {
  delete [] energies_;
}

Real AllAtomClashScorer::CalculateScore(uint start_resnum, uint num_residues,
                                        uint chain_idx) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "AllAtomClashScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // get scores
  std::vector<uint> idx_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, idx_vec, occupied);

  // get scores
  std::vector<Real> ex_scores, in_scores;
  this->Score(idx_vec, ex_scores, in_scores, occupied);
  Real ex_score = 0.0;
  Real in_score = 0.0;
  for (uint i = 0; i < num_residues; ++i) {
    ex_score += ex_scores[i];
    in_score += in_scores[i];
  }

  // we count internal scores half as the same pair appears twice
  Real score = ex_score + Real(0.5)*in_score;

  if(normalize_ && !ex_scores.empty()) {
    score /= ex_scores.size();
  }

  return score;
}

void AllAtomClashScorer::CalculateScoreProfile(
                        uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "AllAtomClashScorer::CalculateScoreProfile", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // get scores
  std::vector<uint> idx_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, idx_vec, occupied);

  std::vector<Real> in_scores;
  this->Score(idx_vec, profile, in_scores, occupied);
  // combine them
  for (uint i = 0; i < num_residues; ++i) {
    profile[i] = profile[i] + in_scores[i];
  }
}

Real AllAtomClashScorer::CalculateScore(
                                    const std::vector<uint>& start_resnum, 
                                    const std::vector<uint>& num_residues,
                                    const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomClashScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec, 
                                      occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}

void AllAtomClashScorer::CalculateScoreProfile(
                              const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomClashScorer::CalculateScore", 2);

  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec, 
                                      occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];
    }
  }
}

void AllAtomClashScorer::AttachEnvironment(loop::AllAtomEnv& env) {
  env_ = env.UniqueListener<AllAtomInteractionEnvListener>(
                           "AllAtomInteractionEnvListener");
  idx_handler_ = env.GetIdxHandlerData();
  all_pos_ = env.GetAllPosData();
  attached_environment_ = true;
}

void AllAtomClashScorer::Score(const std::vector<uint>& indices,
                               std::vector<Real>& external_scores,
                               std::vector<Real>& internal_scores,
                               std::vector<bool>& occupied) const {
  
  // setup
  uint n_indices = indices.size();
  external_scores.resize(n_indices);
  internal_scores.resize(n_indices);

  if(n_indices == 0) {
    return;
  }

  // let's go over every atom
  AllAtomInteractionSpatialOrganizer::WithinResult within_result;
  std::pair<AllAtomInteractionSpatialOrganizerItem*,Real>* a;
  // helpers
  int min_idx_chain, max_idx_chain, start_neglect, end_neglect;
  for (uint i = 0; i < indices.size(); ++i) {
    // init
    const uint res_idx = indices[i];
    Real ex_score = 0.0;
    Real in_score = 0.0;
    // check neglect range for residue
    const uint chain_idx = idx_handler_->GetChainIdx(res_idx);
    min_idx_chain = idx_handler_->GetChainStartIdx(chain_idx);
    max_idx_chain = idx_handler_->GetChainLastIdx(chain_idx);
    start_neglect = std::max(min_idx_chain, int(res_idx) - 1);
    end_neglect = std::min(max_idx_chain, int(res_idx) + 1);
    // loop over all heavy atoms
    const uint first_idx = all_pos_->GetFirstIndex(res_idx);
    const uint last_idx = all_pos_->GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      // only if set
      if (all_pos_->IsSet(idx)) {
        // get clash scores
        const AllAtomClashType my_ct = clash_type_[all_pos_->GetAAA(idx)];
        within_result = env_->FindWithin(all_pos_->GetPos(idx), cutoff_[my_ct]);
        a = within_result.first;
        for (uint j = 0; j < within_result.second; ++j) {
          // skip if too close
          const int other_res_idx = int(a[j].first->res_idx);
          if (other_res_idx < start_neglect || other_res_idx > end_neglect) {
            // score it
            const AllAtomClashType other_ct = clash_type_[a[j].first->aaa];
            const uint bin = a[j].second * bins_per_a_;
            if (occupied[other_res_idx]) {
              // internal interaction
              if(do_internal_scores_) {
                in_score += energies_[this->EIdx(my_ct, other_ct, bin)];
              }
            } else {
              // external interaction
              if(do_external_scores_) {
                ex_score += energies_[this->EIdx(my_ct, other_ct, bin)];
              }
            }
          }
        }
      }
    }
    // set values
    external_scores[i] = ex_score;
    internal_scores[i] = in_score;
  }
}

}} // ns
