// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_CB_PACKING_SCORE_HH
#define PM3_SCORING_CB_PACKING_SCORE_HH

#include <promod3/scoring/cbeta_env_listener.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>

namespace promod3 { namespace scoring {

class CBPackingScorer;
typedef boost::shared_ptr<CBPackingScorer> CBPackingScorerPtr;


class CBPackingScorer : public BackboneScorer{

public:

  CBPackingScorer(Real cutoff, uint max_count);

  virtual ~CBPackingScorer();

  static CBPackingScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static CBPackingScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetEnergy(ost::conop::AminoAcid aa, uint count, Real e);

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  void DoNormalize(bool do_it) {normalize_ = do_it; }

  void AttachEnvironment(BackboneScoreEnv& env);

private:

  CBPackingScorer(): normalize_(true),
                     env_(NULL) { }

  inline uint EIdx(ost::conop::AminoAcid aa, uint count) const{ 
    return aa * (max_count_ + 1) + count;
  }

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& scores) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    core::ConvertBaseType<float>(ds, cutoff_);
    core::ConvertBaseType<uint32_t>(ds, max_count_);

    uint num_values = ost::conop::XXX * (max_count_ + 1);
    if(ds.IsSource()){
      energies_ = new Real[num_values];
      memset(energies_, 0, num_values * sizeof(Real)); 
    }
    for(uint i = 0; i < num_values; ++i){
      core::ConvertBaseType<float>(ds, energies_[i]);
    }
  }

  Real cutoff_;
  uint max_count_;
  Real* energies_;
  bool normalize_;

  //environment specific information, that will be set upon calling 
  //AttachEnvironment
  CBetaEnvListener* env_;
};


}}//ns

#endif
