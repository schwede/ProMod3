// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/ss_agreement_env_listener.hh>

namespace promod3{ namespace scoring{

SSAgreementEnvListener::SSAgreementEnvListener(): env_(12.0),
                                      env_data_(NULL) { }

SSAgreementEnvListener::~SSAgreementEnvListener() {
  if(env_data_ != NULL) delete [] env_data_;
}

void SSAgreementEnvListener::Init(const BackboneScoreEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementEnvListener::Init", 2);

  if(env_data_ != NULL){
  	//the env listener has already been initialized at some point...
    //let's clean up first...
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  idx_handler_ = base_env.GetIdxHandlerData();
  uint num_residues = idx_handler_->GetNumResidues();

  env_data_ = new SSAgreementSpatialOrganizerItem[num_residues];
  env_set_ = base_env.GetEnvSetData();

  psipred_pred_ = base_env.GetPsipredPredictionData();;
  psipred_cfi_ = base_env.GetPsipredConfidenceData();
  n_pos_data_ = base_env.GetNPosData();
  ca_pos_data_ = base_env.GetCAPosData();
  c_pos_data_ = base_env.GetCPosData();
  o_pos_data_ = base_env.GetOPosData();
  
  
  const ost::conop::AminoAcid* aa_data = base_env.GetAAData();
  for(uint i = 0; i < num_residues; ++i){
    env_data_[i].idx = i;
    env_data_[i].is_proline = (aa_data[i] == ost::conop::PRO);
    env_data_[i].valid_h_pos = false;
  }

  ca_positions_.assign(num_residues, geom::Vec3());
  donor_for_one_.assign(num_residues, -1);
  donor_for_two_.assign(num_residues, -1);
  connected_to_next_.assign(num_residues, 0);
}

void SSAgreementEnvListener::SetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementEnvListener::SetEnvironment", 2);

  for(std::vector<uint>::const_iterator i = idx.begin();
  	  i != idx.end(); ++i){
  	env_data_[*i].pos = ca_pos_data_[*i];
  	env_data_[*i].n_pos = n_pos_data_[*i];
  	env_data_[*i].c_pos = c_pos_data_[*i];
  	env_data_[*i].o_pos = o_pos_data_[*i];
  	env_.Add(&env_data_[*i], ca_pos_data_[*i]);
  }

  this->Update(idx);
}

void SSAgreementEnvListener::ResetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementEnvListener::ResetEnvironment", 2);

  geom::Vec3 old_pos;
  for(std::vector<uint>::const_iterator i = idx.begin();
  	  i != idx.end(); ++i){
  	old_pos = env_data_[*i].pos;
    env_data_[*i].pos = ca_pos_data_[*i];
    env_data_[*i].n_pos = n_pos_data_[*i];
    env_data_[*i].c_pos = c_pos_data_[*i];
    env_data_[*i].o_pos = o_pos_data_[*i];
  	env_.Reset(&env_data_[*i], old_pos, ca_pos_data_[*i]);
  }

  this->Update(idx);
}

void SSAgreementEnvListener::ClearEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementEnvListener::ClearEnvironment", 2);

  for(std::vector<uint>::const_iterator i = idx.begin();
  	  i != idx.end(); ++i){
    env_.Remove(&env_data_[*i], env_data_[*i].pos);

    // the residue before is not connected to this residue anymore...
    if(*i > 0) {
      // no check for same chain... (in this case its not connected anyway)
      connected_to_next_[*i - 1] = 0;
    }

    // hydrogen position is not valid anymore...
    env_data_[*i].valid_h_pos = false;
    
    // the hydrogen position of the next residue gets invalidated as well
    // again no check for same chain
    if(*i < idx_handler_->GetNumResidues() - 1) {
      env_data_[*i + 1].valid_h_pos = false;
    }

    // its no donor anymore
    donor_for_one_[*i] = -1;
    donor_for_two_[*i] = -1;
  }  

  std::set<int> stuff_to_update;
  for(std::vector<uint>::const_iterator i = idx.begin(); 
      i != idx.end(); ++i) {
    this->AddCloseResidues(*i, stuff_to_update);
  }

  for(std::set<int>::iterator i = stuff_to_update.begin();
      i != stuff_to_update.end(); ++i) {
    this->FindOptimalDonors(*i);
  }
}

void SSAgreementEnvListener::AddCloseResidues(uint idx, 
                                      std::set<int>& close_residues) const {
  SSAgreementSpatialOrganizer::WithinResult within_result = 
  env_.FindWithin(ca_positions_[idx], 9.0);
  std::pair<SSAgreementSpatialOrganizerItem*,Real>* a = within_result.first;
  for (uint j = 0; j < within_result.second; ++j) {
    close_residues.insert(a[j].first->idx);
  }
}

void SSAgreementEnvListener::FindOptimalDonors(uint idx) {

  donor_for_one_[idx] = -1;
  donor_for_two_[idx] = -1;

  if(!env_data_[idx].valid_h_pos) return;

  std::pair<Real, Real> donor_energies = 
  std::make_pair(std::numeric_limits<Real>::max(),
                 std::numeric_limits<Real>::max());
  std::pair<int, int> acceptors = std::make_pair(-1, -1);

  SSAgreementSpatialOrganizer::WithinResult within_result = 
  env_.FindWithin(env_data_[idx].pos, 9.0);
  std::pair<SSAgreementSpatialOrganizerItem*,Real>* a = within_result.first;
  for (uint j = 0; j < within_result.second; ++j) {
    const uint other_idx = a[j].first->idx;
    if((idx > 0  && (other_idx < idx - 1)) || (other_idx > (idx + 1))) {
      Real e = ost::mol::alg::DSSPHBondEnergy(env_data_[idx].h_pos, 
                                              env_data_[idx].n_pos,
                                              env_data_[other_idx].c_pos, 
                                              env_data_[other_idx].o_pos);
      if(e < donor_energies.first) {
        donor_energies.second = donor_energies.first;
        acceptors.second = acceptors.first;
        donor_energies.first = e;
        acceptors.first = other_idx;
      }
      else if(e < donor_energies.second) {
        donor_energies.second = e;
        acceptors.second = other_idx; 
      }
    }
  }

  if(donor_energies.first < -0.5) {
    donor_for_one_[idx] = acceptors.first;
  }
  if(donor_energies.second < -0.5) {
    donor_for_two_[idx] = acceptors.second;
  }
}

void SSAgreementEnvListener::Update(const std::vector<uint>& idx) {

  // assumes env_data_ to be set...

  for(std::vector<uint>::const_iterator i = idx.begin(); i != idx.end(); ++i){
    
    ca_positions_[*i] = env_data_[*i].pos;
    bool last_residue = (*i >= idx_handler_->GetNumResidues() - 1);

    // set connectivity of the residue itself
    if(!last_residue && env_set_[*i + 1] &&
       geom::Length2(env_data_[*i].c_pos - env_data_[*i+1].n_pos) <= 6.25) {
        connected_to_next_[*i] = 1;
    } else {
      connected_to_next_[*i] = 0;
    }
    
    // set connectivity of the residue before
    if(*i > 0) {
      if(env_set_[*i - 1] && 
         geom::Length2(env_data_[*i-1].c_pos - env_data_[*i].n_pos) <= 6.25) {
        connected_to_next_[*i-1] = 1;
      } else {
        connected_to_next_[*i-1] = 0;
      }
    }

    // h_pos of this residue
    if(!env_data_[*i].is_proline) {
      if(*i > 0 && env_set_[*i-1] && connected_to_next_[*i - 1]) {
        geom::Vec3 dir_vec = geom::Normalize(env_data_[*i-1].c_pos -
                                             env_data_[*i-1].o_pos);
        env_data_[*i].h_pos = env_data_[*i].n_pos + dir_vec;
        env_data_[*i].valid_h_pos = true;
      }
      else {
        env_data_[*i].valid_h_pos = false;
      }
    } 

    // the h_pos of the next residue might have changed
    if(connected_to_next_[*i]) {
      if(!env_data_[*i+1].is_proline) {
        geom::Vec3 dir_vec = geom::Normalize(env_data_[*i].c_pos -
                                           env_data_[*i].o_pos);
        env_data_[*i+1].h_pos = env_data_[*i+1].n_pos + dir_vec;
        env_data_[*i+1].valid_h_pos = true;
      } else {
        env_data_[*i+1].valid_h_pos = false;
      }
    } else if(!last_residue) {
      env_data_[*i+1].valid_h_pos = false;
    }
  }

  // and finally update all donor information in the close environment
  std::set<int> stuff_to_update;
  for(std::vector<uint>::const_iterator i = idx.begin(); 
      i != idx.end(); ++i) {
    this->AddCloseResidues(*i, stuff_to_update);
  }

  for(std::set<int>::iterator i = stuff_to_update.begin();
      i != stuff_to_update.end(); ++i) {
    this->FindOptimalDonors(*i);
  }
}

}} // ns
