// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_PAIRWISE_SCORE_HH
#define PM3_PAIRWISE_SCORE_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>

namespace promod3 { namespace scoring {

class PairwiseScorer;
typedef boost::shared_ptr<PairwiseScorer> PairwiseScorerPtr;

class PairwiseScorer : public BackboneScorer {

public:

  PairwiseScorer();

  virtual ~PairwiseScorer();

  void DoInternalScores(bool do_it) { do_internal_scores_ = do_it; }

  void DoExternalScores(bool do_it) { do_external_scores_ = do_it; }

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  void AttachEnvironment(BackboneScoreEnv& env);

private:

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& external_scores,
             std::vector<Real>& internal_scores,
             std::vector<bool>& occupied) const;

  bool do_internal_scores_;
  bool do_external_scores_;
  bool normalize_;

  //environment specific information, that will be set upon calling 
  //AttachEnvironment
  bool attached_environment_;
  const std::vector<PairwiseFunctionData>* pairwise_functions_;
  const loop::IdxHandler* idx_handler_;
  const int* env_set_;
  const geom::Vec3* ca_pos_;
  const geom::Vec3* cb_pos_;
};

}}//ns

#endif
