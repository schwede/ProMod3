// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_SSAGREEMENT_ENV_LISTENER_HH
#define PM3_SCORING_SSAGREEMENT_ENV_LISTENER_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>

#include <ost/mol/alg/sec_struct.hh>

#include <set>

namespace promod3 { namespace scoring {


struct SSAgreementSpatialOrganizerItem {
  SSAgreementSpatialOrganizerItem() { }

  geom::Vec3 pos; //CA position of residue
  geom::Vec3 n_pos; //donor N
  geom::Vec3 h_pos; //donor H
  geom::Vec3 c_pos; //acceptor C
  geom::Vec3 o_pos; //acceptor O
  uint idx;
  bool is_proline;
  bool valid_h_pos;
};

typedef core::DynamicSpatialOrganizer<SSAgreementSpatialOrganizerItem>
        SSAgreementSpatialOrganizer;

class SSAgreementEnvListener : public BackboneScoreEnvListener{

public:

  SSAgreementEnvListener();

  virtual ~SSAgreementEnvListener();

  virtual void Init(const BackboneScoreEnv& base_env);

  virtual void SetEnvironment(const std::vector<uint>& idx);

  virtual void ResetEnvironment(const std::vector<uint>& idx);

  virtual void ClearEnvironment(const std::vector<uint>& idx);

  virtual String WhoAmI() const { return "SSAgreementEnvListener"; }

  const int* GetEnvSetData() { return env_set_; }

  const loop::IdxHandler* GetIdxHandler() { return idx_handler_; }

  const char* GetPsipredPredData() { return psipred_pred_; }

  const int* GetPsipredConfidenceData() { return psipred_cfi_; }

  const std::vector<geom::Vec3>& GetCAPositions() const { return ca_positions_; }
  const std::vector<int>& GetDonorForOne() const { return donor_for_one_; }
  const std::vector<int>& GetDonorForTwo() const { return donor_for_two_; }
  const std::vector<int>& GetConnection() const {return connected_to_next_; }

private:

  // helper function that sets hydrogen positions in env_data_, the ca_positions_, 
  // donor_for_one_, donor_for_two_, and connected_to_next_ for all residues 
  // affected by indices in idx
  void Update(const std::vector<uint>& idx);

  void AddCloseResidues(uint idx, std::set<int>& close_residues) const;

  void FindOptimalDonors(uint idx);

  SSAgreementSpatialOrganizer env_;  
  SSAgreementSpatialOrganizerItem* env_data_;

  // data ptrs fetched from base env at initialization
  const int* env_set_;
  const loop::IdxHandler* idx_handler_;
  const char* psipred_pred_;
  const int* psipred_cfi_;
  const geom::Vec3* n_pos_data_;
  const geom::Vec3* ca_pos_data_;
  const geom::Vec3* c_pos_data_;
  const geom::Vec3* o_pos_data_;

  // thats the input for the ost dssp implementation
  std::vector<geom::Vec3> ca_positions_;
  std::vector<int> donor_for_one_;
  std::vector<int> donor_for_two_;
  std::vector<int> connected_to_next_;
}; 

}} // ns

#endif
