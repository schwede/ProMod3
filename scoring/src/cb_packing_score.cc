// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/cb_packing_score.hh>


namespace promod3 { namespace scoring {

CBPackingScorer::CBPackingScorer(Real cutoff, uint max_count): 
                                 cutoff_(cutoff),
                                 max_count_(max_count),
                                 normalize_(true),
                                 env_(NULL) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::CBPackingScorer", 2);
  
  if (cutoff < 0.0) {
    throw promod3::Error("CBPacking cutoff must not be zero!");
  }
  if (max_count == 0) {
    throw promod3::Error("Max count must be nonzero!");
  }

  uint num_values = ost::conop::XXX * (max_count_ + 1);
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

CBPackingScorer::~CBPackingScorer() {
  delete [] energies_;
}

CBPackingScorerPtr CBPackingScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // here starts the score specific stuff

  CBPackingScorerPtr p(new CBPackingScorer);

  in_stream.read(reinterpret_cast<char*>(&p->cutoff_), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->max_count_), sizeof(uint));
  uint num_values = ost::conop::XXX * (p->max_count_ + 1);
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values*sizeof(Real));

  return p;
}

void CBPackingScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&cutoff_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&max_count_), sizeof(uint));
  uint num_bytes = (ost::conop::XXX * (max_count_ + 1)) * sizeof(Real);
  out_stream.write(reinterpret_cast<char*>(energies_), num_bytes);
}

CBPackingScorerPtr CBPackingScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);
  core::CheckBaseType<bool>(in_stream, true);

  // data
  CBPackingScorerPtr p(new CBPackingScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void CBPackingScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);
  core::WriteBaseType<bool>(out_stream, true);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void CBPackingScorer::SetEnergy(ost::conop::AminoAcid aa, uint count, Real e) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::SetEnergy", 2);

  if (aa == ost::conop::XXX) {
    throw promod3::Error("Cannot set energy for invalid amino acid!");
  }

  if (count > max_count_) {
    throw promod3::Error("Invalid cb packing count!");
  }

  energies_[this->EIdx(aa, count)] = e;
}

Real CBPackingScorer::CalculateScore(uint start_resnum, uint num_residues,
                                     uint chain_idx) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::CalculateScore", 2);

  if(env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  std::vector<Real> scores;  
  std::vector<uint> idx_vector = 
  env_->GetIdxHandler()->ToIdxVector(start_resnum, num_residues, chain_idx);

  this->Score(idx_vector, scores);
  Real score = 0.0;
  for(uint i = 0; i < scores.size(); ++i) {
    score += scores[i];
  }

  if(normalize_ && !scores.empty()) {
    score /= scores.size();
  }

  return score;
}


void CBPackingScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                            uint chain_idx,
                                            std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::CalculateScoreProfile", 2);

  if(env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  std::vector<uint> idx_vector = 
  env_->GetIdxHandler()->ToIdxVector(start_resnum, num_residues, chain_idx);
  this->Score(idx_vector, profile);
}


Real CBPackingScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                     const std::vector<uint>& num_residues,
                                     const std::vector<uint>& chain_idx) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::CalculateScore", 2);

  // setup
  if(env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  std::vector<std::vector<uint> > indices_vec;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    indices_vec.push_back(env_->GetIdxHandler()->ToIdxVector(start_resnum[i], 
                                                    num_residues[i], 
                                                    chain_idx[i]));
  }

  // get scores and sum them up
  Real score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < indices_vec.size(); ++i) {
    std::vector<Real> scores;
    this->Score(indices_vec[i], scores);
    for(uint j = 0; j < scores.size(); ++j) {
      score += scores[j];
    }
    n_scored_residues += scores.size();
  }

  if(normalize_ && n_scored_residues > 0) {
    score /= n_scored_residues;
  }

  return score;
}


void CBPackingScorer::CalculateScoreProfile(
                              const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::CalculateScoreProfile", 2);

  // setup
  if(env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in CBetaScore!");
  }

  // get scores
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<uint> idx_vector = 
    env_->GetIdxHandler()->ToIdxVector(start_resnum[i], num_residues[i], 
                                       chain_idx[i]);
    this->Score(idx_vector, profile[i]);
  }
}


void CBPackingScorer::Score(const std::vector<uint>& indices,
                            std::vector<Real>& scores) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBPackingScorer::Score", 2);

  // setup
  uint n_indices = indices.size();

  scores.assign(n_indices, 0.0);

  if(n_indices == 0) {
    return;
  }

  // then external and sum up score as we go
  CBetaSpatialOrganizer::WithinResult within_result;
  std::pair<CBetaSpatialOrganizerItem*,Real>* a;
  const int* env_set = env_->GetEnvSetData();
  for (uint i = 0; i < indices.size(); ++i) {
    uint my_idx = indices[i];

    if(!env_set[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    ost::conop::AminoAcid my_aa = env_->GetEnvData(my_idx).aa;

    // count neighbours
    uint count = 0;
    within_result = env_->FindWithin(env_->GetEnvData(my_idx).pos, cutoff_);
    a = within_result.first;
    for (uint j = 0; j < within_result.second; ++j) {
      if (my_idx != a[j].first->idx) {
        ++count;
      }
    }
    // score it
    count = std::min(count, max_count_);
    scores[i] = energies_[this->EIdx(my_aa, count)];
  }
}

void CBPackingScorer::AttachEnvironment(BackboneScoreEnv& env) {
  env_ = env.UniqueListener<CBetaEnvListener>("CBetaEnvListener");
}

}}
