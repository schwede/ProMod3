// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/ss_agreement_score.hh>

namespace promod3 { namespace scoring {

SSAgreementScorer::SSAgreementScorer(): normalize_(true),
                                        env_(NULL) {
  uint num_values = 3 * 10 * 8;
  scores_ = new Real[num_values];
  memset(scores_, 0, num_values*sizeof(Real));
}

SSAgreementScorer::~SSAgreementScorer() {
  delete [] scores_;
}

SSAgreementScorerPtr SSAgreementScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // here starts the score specific stuff

  SSAgreementScorerPtr p(new SSAgreementScorer);

  uint num_values = 3 * 10 * 8;
  p->scores_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->scores_), num_values * sizeof(Real));

  return p;
}

void SSAgreementScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // the actual content of the score

  uint num_values = 3 * 10 * 8;
  out_stream.write(reinterpret_cast<char*>(scores_), num_values * sizeof(Real));
}

SSAgreementScorerPtr SSAgreementScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);
  core::CheckBaseType<bool>(in_stream, true);

  // data
  SSAgreementScorerPtr p(new SSAgreementScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void SSAgreementScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);
  core::WriteBaseType<bool>(out_stream, true);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void SSAgreementScorer::SetScore(char psipred_state, int psipred_cfi,
                                 char dssp_state, Real score) {

  uint psipred_idx = GetPsipredIdx(psipred_state);
  uint dssp_idx = GetDSSPIdx(dssp_state);
  if(psipred_cfi < 0 || psipred_cfi > 9){
    throw promod3::Error("Psipred confidence value must be in range [0,9] !");
  }

  scores_[this->SIdx(psipred_idx, psipred_cfi, dssp_idx)] = score;
}


Real SSAgreementScorer::CalculateScore(uint start_resnum, uint num_residues,
                                       uint chain_idx) const{
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::CalculateScore", 2);

  std::vector<uint> v_start_resnum(1, start_resnum);
  std::vector<uint> v_num_residues(1, num_residues);
  std::vector<uint> v_chain_idx(1, chain_idx);
  
  return this->CalculateScore(v_start_resnum, v_num_residues, v_chain_idx);
}

void SSAgreementScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                              uint chain_idx,
                                              std::vector<Real>& profile) const{
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::CalculateScoreProfile", 2);

  std::vector<uint> v_start_resnum(1, start_resnum);
  std::vector<uint> v_num_residues(1, num_residues);
  std::vector<uint> v_chain_idx(1, chain_idx);
  std::vector<std::vector<Real> > scores;

  this->CalculateScoreProfile(v_start_resnum, v_num_residues, v_chain_idx, 
                              scores);
  profile = scores[0];
}


Real SSAgreementScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                    const std::vector<uint>& num_residues,
                                    const std::vector<uint>& chain_idx) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::CalculateScore", 2);

  std::vector<std::vector<Real> > scores;
  this->CalculateScoreProfile(start_resnum, num_residues, chain_idx, scores);

  Real score = 0.0;
  uint n = 0;
  for(uint i = 0; i < scores.size(); ++i){
    for(uint j = 0; j < scores[i].size(); ++j) {
      score += scores[i][j];
    }
    n += scores[i].size();
  }

  if(normalize_ && n > 0) {
    score /= n;
  }

  return score;
}

void SSAgreementScorer::CalculateScoreProfile(
                              const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::CalculateScoreProfile", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in Score Setup!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in Score Setup!");
  }

  std::vector<uint> start_indices(start_resnum.size());
  const loop::IdxHandler* idx_handler = env_->GetIdxHandler();
  for(uint i = 0; i < start_resnum.size(); ++i) {
    start_indices[i] = idx_handler->ToIdx(start_resnum[i], chain_idx[i]);
    if(start_indices[i] + num_residues[i] - 1 > 
       idx_handler->GetChainLastIdx(chain_idx[i])) {
      throw promod3::Error("Invalid start_resnum and loop length!");
    }
  }

  this->Score(start_indices, num_residues, profile);
}


void SSAgreementScorer::Score(const std::vector<uint>& start_indices,
                              const std::vector<uint>& num_residues,
                              std::vector<std::vector<Real> >& scores) const{

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SSAgreementScorer::Score", 2);

  uint n_profiles = start_indices.size();

  if(n_profiles == 0) return;

  scores.assign(n_profiles, std::vector<Real>());

  const int* env_set = env_->GetEnvSetData();
  const char* psipred_pred = env_->GetPsipredPredData();
  const int* psipred_cfi = env_->GetPsipredConfidenceData();

  // check whether the full env is set
  for(uint i = 0; i < start_indices.size(); ++i) {
    for(uint j = 0; j < num_residues[i]; ++j) {
      if(!env_set[start_indices[i] + j]) {
        throw promod3::Error("Must set environment before scoring!");
      }
    }
    scores[i].assign(num_residues[i], 0.0);
  }

  
  for(uint i = 0; i < n_profiles; ++i) {

    if(num_residues[i] == 0) continue;

    String ss  = ost::mol::alg::RawEstimateSS(env_->GetCAPositions(), 
                                              start_indices[i], 
                                              num_residues[i],
	                                            env_->GetDonorForOne(), 
                                              env_->GetDonorForTwo(),
                                              env_->GetConnection());

    for(uint j = 0; j < num_residues[i]; ++j){
      scores[i][j] = scores_[this->SIdx(GetPsipredIdx(psipred_pred[start_indices[i] + j]), 
                                        psipred_cfi[start_indices[i] + j], 
                                        GetDSSPIdx(ss[j]))];
    }
  }
}

void SSAgreementScorer::AttachEnvironment(BackboneScoreEnv& env) {
  env_ = env.UniqueListener<SSAgreementEnvListener>("SSAgreementEnvListener");
}

}} // ns
