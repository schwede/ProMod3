// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_CLASH_ENV_LISTENER_HH
#define PM3_SCORING_CLASH_ENV_LISTENER_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>

namespace promod3 { namespace scoring {

enum BBClashType {
  BB_CLASH_C,
  BB_CLASH_N,
  BB_CLASH_O,
  BB_CLASH_NUM
};

struct ClashSpatialOrganizerItem {
  ClashSpatialOrganizerItem() { }

  ClashSpatialOrganizerItem(BBClashType type, const geom::Vec3& p, uint i)
                           : clash_type(type), pos(p), idx(i) { }
  BBClashType clash_type;
  geom::Vec3 pos;
  uint idx;
};

typedef core::DynamicSpatialOrganizer<ClashSpatialOrganizerItem>
        ClashSpatialOrganizer;

class ClashEnvListener : public BackboneScoreEnvListener {

public:

  ClashEnvListener();

  virtual ~ClashEnvListener();

  virtual void Init(const BackboneScoreEnv& base_env);

  virtual void SetEnvironment(const std::vector<uint>& idx);

  virtual void ResetEnvironment(const std::vector<uint>& idx);

  virtual void ClearEnvironment(const std::vector<uint>& idx);

  virtual String WhoAmI() const { return "ClashEnvListener"; }

  ClashSpatialOrganizer::WithinResult FindWithin(const geom::Vec3& pos,
                                                 Real cutoff) const {
    return env_.FindWithin(pos, cutoff);
  }

  const int* GetEnvSetData() { return env_set_; }

  const loop::IdxHandler* GetIdxHandler() { return idx_handler_; }

  // for internal use in the actual scorer to access required per residue data
  // NO RANGE CHECK!
  ClashSpatialOrganizerItem* GetParticleData(uint idx) { return &env_data_[5*idx]; };
  bool IsGlycine(uint idx) { return is_glycine_[idx]; } 

private:

  ClashSpatialOrganizer env_;
  // here: 5 atoms stored for each index: use env_data_[5*idx + sub_idx]
  bool* is_glycine_;
  ClashSpatialOrganizerItem* env_data_;

  // data ptrs fetched from base env at initialization
  const int* env_set_;
  const loop::IdxHandler* idx_handler_;
  const geom::Vec3* n_pos_data_;
  const geom::Vec3* ca_pos_data_;
  const geom::Vec3* c_pos_data_;
  const geom::Vec3* o_pos_data_;
  const geom::Vec3* cb_pos_data_;
  const ost::conop::AminoAcid* aa_data_;
};

}} //ns

#endif
