// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/cbeta_env_listener.hh>


namespace promod3 { namespace scoring {

CBetaEnvListener::CBetaEnvListener(): env_(12.0),
                                      env_data_(NULL) { }

CBetaEnvListener::~CBetaEnvListener() {
  if (env_data_ != NULL) delete [] env_data_;
}

void CBetaEnvListener::Init(const BackboneScoreEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaEnvListener::Init", 2);

  if (env_data_ != NULL) {
    // the env listener has already been initialized at some point...
    // let's clean up first...
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  idx_handler_ = base_env.GetIdxHandlerData();
  env_set_ = base_env.GetEnvSetData();
  cb_pos_data_ = base_env.GetCBPosData();
  uint num_residues = idx_handler_->GetNumResidues();
  env_data_ = new CBetaSpatialOrganizerItem[num_residues];
  const ost::conop::AminoAcid* aa_data = base_env.GetAAData();

  for (uint i = 0; i < num_residues; ++i) {
    env_data_[i].idx = i;
    env_data_[i].aa = aa_data[i];
  }
}

void CBetaEnvListener::SetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaEnvListener::SetEnvironment", 2);

  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    env_data_[*i].pos = cb_pos_data_[*i];
    env_.Add(&env_data_[*i], cb_pos_data_[*i]);
  }
}

void CBetaEnvListener::ResetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaEnvListener::ResetEnvironment", 2);

  geom::Vec3 old_pos;
  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    old_pos = env_data_[*i].pos;
    env_data_[*i].pos = cb_pos_data_[*i];
    env_.Reset(&env_data_[*i], old_pos, cb_pos_data_[*i]);
  }
}

void CBetaEnvListener::ClearEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CBetaEnvListener::ClearEnvironment", 2);

  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    env_.Remove(&env_data_[*i], env_data_[*i].pos);
  }
}

}}
