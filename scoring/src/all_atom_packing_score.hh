// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_ALL_ATOM_PACKING_SCORE_HH
#define PM3_SCORING_ALL_ATOM_PACKING_SCORE_HH

#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/loop/all_atom_env.hh>
#include <promod3/scoring/all_atom_score_base.hh>
#include <promod3/scoring/all_atom_interaction_env_listener.hh>

namespace promod3 { namespace scoring {

class AllAtomPackingScorer;
typedef boost::shared_ptr<AllAtomPackingScorer> AllAtomPackingScorerPtr;


class AllAtomPackingScorer : public AllAtomScorer {

public:

  AllAtomPackingScorer(Real cutoff, uint max_count);

  virtual ~AllAtomPackingScorer();

  static AllAtomPackingScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static AllAtomPackingScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetEnergy(loop::AminoAcidAtom aaa, uint count, Real e);

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;
  virtual void
  CalculateScoreProfile(uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  virtual void AttachEnvironment(loop::AllAtomEnv& env);

private:

  AllAtomPackingScorer(): normalize_(true), attached_environment_(false) { }

  uint EIdx(loop::AminoAcidAtom aaa, uint count) const {
    return aaa * (max_count_ + 1) + count;
  }
  uint NumEnergies() const {
    return loop::XXX_NUM_ATOMS * (max_count_ + 1);
  }

  void Score(const std::vector<uint>& indices,
             std::vector<Real>& scores) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    core::ConvertBaseType<float>(ds, cutoff_);
    core::ConvertBaseType<uint32_t>(ds, max_count_);
    uint num_values = NumEnergies();
    if (ds.IsSource()) energies_ = new Real[num_values];
    for (uint i = 0; i < num_values; ++i) {
      core::ConvertBaseType<float>(ds, energies_[i]);
    }
  }

  Real cutoff_;
  uint max_count_;
  Real* energies_;
  bool normalize_;

  // environment specific information, set upon calling AttachEnvironment
  bool attached_environment_;
  AllAtomInteractionEnvListenerPtr env_;
  loop::ConstIdxHandlerPtr idx_handler_;
  loop::ConstAllAtomPositionsPtr all_pos_;
};

}} // ns

#endif
