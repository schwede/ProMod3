// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_PAIRWISE_SCORING_FUNCTION_HH
#define PM3_PAIRWISE_SCORING_FUNCTION_HH

#include <ost/base.hh>
#include <promod3/core/message.hh>
#include <vector>


namespace promod3 { namespace scoring {

class PairwiseFunction;
class ConstraintFunction;
class ContactFunction;
typedef boost::shared_ptr<PairwiseFunction> PairwiseFunctionPtr;
typedef boost::shared_ptr<ConstraintFunction> ConstraintFunctionPtr;
typedef boost::shared_ptr<ContactFunction> ContactFunctionPtr;

class PairwiseFunction{
public:

  virtual ~PairwiseFunction() { }

  virtual Real Score(Real distance) const = 0;
};


class ConstraintFunction : public PairwiseFunction{

public:
  ConstraintFunction(Real min, Real max, 
                     const std::vector<Real>& v): values_(v),
                                                  min_dist_(min),
                                                  max_dist_(max),
                                                  num_bins_(v.size() - 1){ 

    if(values_.size() <= 1){
      String err = "Values must be at least of length 2!";
      throw promod3::Error(err);
    }

    if(min >= max){
      String err = "Min distance must be smaller than max distance when ";
      err += "setting up constraint!";
      throw promod3::Error(err);
    }

    if(min < 0.0 || max < 0.0){
      String err = "Min and Max distance must be >= 0.0 when setting ";
      err += "up constraint!";
      throw promod3::Error(err);
    }

    bin_size_ = (max_dist_ - min_dist_) / num_bins_;
  }

  virtual ~ConstraintFunction() { }

  virtual Real Score(Real distance) const{
    if(distance < min_dist_ || distance >= max_dist_) return 0.0;
    Real function_pos = (distance - min_dist_) / bin_size_;
    uint lower_bin = std::floor(function_pos);
    uint upper_bin = std::ceil(function_pos);
    Real w_lower = upper_bin - function_pos;
    Real w_upper = function_pos - lower_bin;
    return w_lower * values_[lower_bin] + w_upper * values_[upper_bin];
  }

private:
  std::vector<Real> values_;
  Real min_dist_;
  Real max_dist_;
  int num_bins_;
  Real bin_size_;
};


class ContactFunction : public PairwiseFunction{

public:
  ContactFunction(Real d, Real s): max_dist_(d), score_(s) { }

  virtual ~ContactFunction() { }
                                                             
  virtual Real Score(Real distance) const { 
  	return (distance < max_dist_ && distance > 0.0) ? score_ : 0.0; 
  }

private:
  Real max_dist_;
  Real score_;
};


}}//ns

#endif
