// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_ALL_ATOM_SCORE_ENV_LISTENER_HH
#define PM3_SCORING_ALL_ATOM_SCORE_ENV_LISTENER_HH

#include <promod3/loop/all_atom_env.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>

namespace promod3 { namespace scoring {

class AllAtomInteractionEnvListener;
typedef boost::shared_ptr<AllAtomInteractionEnvListener>
        AllAtomInteractionEnvListenerPtr;

struct AllAtomInteractionSpatialOrganizerItem {
  AllAtomInteractionSpatialOrganizerItem() { }

  AllAtomInteractionSpatialOrganizerItem(loop::AminoAcidAtom a,
                                         const geom::Vec3& p, uint i)
                                         : aaa(a), pos(p), res_idx(i) { }
  loop::AminoAcidAtom aaa;
  geom::Vec3 pos;
  uint res_idx;
};

typedef core::DynamicSpatialOrganizer<AllAtomInteractionSpatialOrganizerItem>
        AllAtomInteractionSpatialOrganizer;

class AllAtomInteractionEnvListener : public loop::AllAtomEnvListener {

public:

  AllAtomInteractionEnvListener();

  virtual ~AllAtomInteractionEnvListener();

  virtual void Init(const loop::AllAtomEnv& base_env);

  virtual void ResetEnvironment(const loop::AllAtomEnv& base_env);

  virtual void UpdateEnvironment(const loop::AllAtomEnv& base_env, 
                                 const std::vector<uint>& res_indices);

  virtual String WhoAmI() const { return "AllAtomInteractionEnvListener"; }
  
  // get all atoms within cutoff
  // - result.first: std::pair<AllAtomInteractionSpatialOrganizerItem*,Real>*
  // - result.second: uint = size of partners found
  AllAtomInteractionSpatialOrganizer::WithinResult FindWithin(
                                  const geom::Vec3& pos, Real cutoff) const {
    return env_.FindWithin(pos, cutoff);
  }

private:

  // set stuff for one residue
  void SetResidue_(loop::ConstAllAtomPositionsPtr all_pos, const uint res_idx);
  
  AllAtomInteractionSpatialOrganizer env_;
  // 1 item per heavy atom, same numbering as base_env.GetAllPosData()
  AllAtomInteractionSpatialOrganizerItem* env_data_;
  std::vector<bool> is_set_;
};

}} // ns

#endif
