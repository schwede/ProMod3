// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_ALL_ATOM_CLASH_SCORE_HH
#define PM3_SCORING_ALL_ATOM_CLASH_SCORE_HH

#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/loop/all_atom_env.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/scoring/all_atom_score_base.hh>
#include <promod3/scoring/all_atom_interaction_env_listener.hh>

namespace promod3 { namespace scoring {

class AllAtomClashScorer;
typedef boost::shared_ptr<AllAtomClashScorer> AllAtomClashScorerPtr;

enum AllAtomClashType {
  ALL_ATOM_CLASH_C,
  ALL_ATOM_CLASH_N,
  ALL_ATOM_CLASH_O,
  ALL_ATOM_CLASH_S,
  ALL_ATOM_CLASH_CYS_CB,
  ALL_ATOM_CLASH_CYS_SG,
  ALL_ATOM_CLASH_NUM
};

class AllAtomClashScorer : public AllAtomScorer {

public:

  AllAtomClashScorer();

  virtual ~AllAtomClashScorer();

  void DoInternalScores(bool do_it) { do_internal_scores_ = do_it; }

  void DoExternalScores(bool do_it) { do_external_scores_ = do_it; }

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  virtual Real 
  CalculateScore(uint start_resnum, uint num_residues, uint chain_idx) const;

  virtual void
  CalculateScoreProfile(uint start_resnum, uint num_residues, uint chain_idx,
                        std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  virtual void AttachEnvironment(loop::AllAtomEnv& env);

private:

  uint EIdx(AllAtomClashType a, AllAtomClashType b, uint bin) const {
    return (a * ALL_ATOM_CLASH_NUM + b) * bins_ + bin;
  }

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& external_scores,
             std::vector<Real>& internal_scores,
             std::vector<bool>& occupied) const;

  // hard coded bounds (safe for dist. < 3.41 (should be enough...))
  static const uint bins_ = 341;
  static const uint bins_per_a_ = 100;
  AllAtomClashType clash_type_[loop::XXX_NUM_ATOMS];
  Real cutoff_[ALL_ATOM_CLASH_NUM];
  Real* energies_;  
  bool do_internal_scores_;
  bool do_external_scores_;
  bool normalize_;

  // environment specific information, set upon calling AttachEnvironment
  bool attached_environment_;
  AllAtomInteractionEnvListenerPtr env_;
  loop::ConstIdxHandlerPtr idx_handler_;
  loop::ConstAllAtomPositionsPtr all_pos_;
};

}} // ns

#endif
