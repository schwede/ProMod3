// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_DENSITY_SCORE_HH
#define PM3_SCORING_DENSITY_SCORE_HH

#include <ost/img/map.hh>
#include <promod3/loop/density_creator.hh>
#include <promod3/loop/backbone.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace scoring {

Real DensityScore(const promod3::loop::BackboneList& bb_list, 
                  ost::img::MapHandle& map, 
                  Real resolution, bool all_atom);

}}//ns

#endif

