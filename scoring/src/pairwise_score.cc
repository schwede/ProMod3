// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/pairwise_score.hh>

namespace promod3{ namespace scoring{

PairwiseScorer::PairwiseScorer(): do_internal_scores_(true),
                                  do_external_scores_(true),
                                  normalize_(true),
                                  attached_environment_(false) { }

PairwiseScorer::~PairwiseScorer() { }

Real PairwiseScorer::CalculateScore(uint start_resnum, uint num_residues,
                                    uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "PairwiseScorer::CalculateScore", 2);

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, idx_vector, 
                                      occupied);

  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  // sum them
  Real external_score = 0.0;
  Real internal_score = 0.0;
  for (uint i = 0; i < external_scores.size(); ++i) {
    external_score += external_scores[i];
    internal_score += internal_scores[i];
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && !external_scores.empty()) {
    external_score /= external_scores.size();
  }

  return external_score;
}


void PairwiseScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                           uint chain_idx,
                                           std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "PairwiseScorer::CalculateScoreProfile", 2);

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, idx_vector, 
                                      occupied);

  // get scores
  std::vector<Real> internal_scores;
  this->Score(idx_vector, profile, internal_scores, occupied);

  for(uint i = 0; i < profile.size(); ++i) {
    profile[i] += internal_scores[i];
  }
}


Real PairwiseScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                    const std::vector<uint>& num_residues,
                                    const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "PairwiseScorer::CalculateScore", 2);

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec, 
                                      occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}


void PairwiseScorer::CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                                const std::vector<uint>& num_residues,
                                const std::vector<uint>& chain_idx,
                                std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "PairwiseScorer::CalculateScore", 2);

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  idx_handler_->SetupScoreCalculation(start_resnum, num_residues, 
                                      chain_idx, indices_vec, 
                                      occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];
    }
  }
}


void PairwiseScorer::Score(const std::vector<uint>& indices,
                           std::vector<Real>& external_scores,
                           std::vector<Real>& internal_scores,
                           std::vector<bool>& occupied) const {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "PairwiseScorer::Score", 2);

  // setup
  if (!attached_environment_) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  uint n_indices = indices.size();

  external_scores.assign(n_indices, 0.0);
  internal_scores.assign(n_indices, 0.0);

  if(n_indices == 0) return;

  for(uint i = 0; i < n_indices; ++i) {
    uint my_idx = indices[i];

    if(!env_set_[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    const std::vector<PairwiseFunctionData>& functions = 
    pairwise_functions_[my_idx];
    if(!functions.empty()) {
      for(uint j = 0; j < functions.size(); ++j) {

        uint other_idx = functions[j].other_idx;

        if(env_set_[other_idx]) {
          Real val;  

          if(functions[j].ft == CB_PAIRWISE_FUNCTION) {
            val = geom::Distance(cb_pos_[my_idx], cb_pos_[other_idx]);
          }
          else{
            val = geom::Distance(ca_pos_[my_idx], ca_pos_[other_idx]);
          }

          val = functions[j].f->Score(val);

          if(occupied[other_idx] && do_internal_scores_) {
            internal_scores[i] += val;
          }

          if(!occupied[other_idx] && do_external_scores_) {
            external_scores[i] += val;
          }
        }
      }
    }
  }
}

void PairwiseScorer::AttachEnvironment(BackboneScoreEnv& env) {
  pairwise_functions_ = env.GetPairwiseFunctionData();
  idx_handler_ = env.GetIdxHandlerData();
  env_set_ = env.GetEnvSetData();
  ca_pos_ = env.GetCAPosData();
  cb_pos_ = env.GetCBPosData();
  attached_environment_ = true;
}

}} //ns
