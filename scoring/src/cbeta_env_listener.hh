// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_CBETA_ENV_LISTENER_HH
#define PM3_SCORING_CBETA_ENV_LISTENER_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>

namespace promod3 { namespace scoring {

struct CBetaSpatialOrganizerItem {
  CBetaSpatialOrganizerItem() { }

  CBetaSpatialOrganizerItem(ost::conop::AminoAcid a,
                            const geom::Vec3& p, uint i): aa(a), pos(p), idx(i)
  { }
  ost::conop::AminoAcid aa;
  geom::Vec3 pos;
  uint idx;
};

typedef core::DynamicSpatialOrganizer<CBetaSpatialOrganizerItem>
        CBetaSpatialOrganizer;

class CBetaEnvListener : public BackboneScoreEnvListener {

public:

  CBetaEnvListener();

  virtual ~CBetaEnvListener();

  virtual void Init(const BackboneScoreEnv& base_env);

  virtual void SetEnvironment(const std::vector<uint>& idx);

  virtual void ResetEnvironment(const std::vector<uint>& idx);

  virtual void ClearEnvironment(const std::vector<uint>& idx);

  virtual String WhoAmI() const { return "CBetaEnvListener"; }

  const CBetaSpatialOrganizerItem& GetEnvData(uint idx) const {
    return env_data_[idx];
  }

  const int* GetEnvSetData() { return env_set_; }

  const loop::IdxHandler* GetIdxHandler() { return idx_handler_; }

  // get all cbetas within cutoff
  // - result.first: std::pair<CBetaSpatialOrganizerItem*,Real>*
  // - result.second: uint = size of partners found
  CBetaSpatialOrganizer::WithinResult FindWithin(const geom::Vec3& pos,
                                                 Real cutoff) const {
    return env_.FindWithin(pos, cutoff);
  }

private:
  
  CBetaSpatialOrganizer env_;  
  CBetaSpatialOrganizerItem* env_data_;

  // data ptrs fetched from base env at initialization
  const int* env_set_;
  const loop::IdxHandler* idx_handler_;
  const geom::Vec3* cb_pos_data_;
};

}} //ns

#endif
