// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_SCORING_OBJECT_LOADER_HH
#define PROMOD3_SCORING_OBJECT_LOADER_HH

#include <promod3/scoring/cb_packing_score.hh>
#include <promod3/scoring/cbeta_score.hh>
#include <promod3/scoring/reduced_score.hh>
#include <promod3/scoring/hbond_score.hh>
#include <promod3/scoring/ss_agreement_score.hh>
#include <promod3/scoring/torsion_score.hh>
#include <promod3/scoring/all_atom_interaction_score.hh>
#include <promod3/scoring/all_atom_packing_score.hh>
#include <promod3/scoring/backbone_overall_scorer.hh>
#include <promod3/scoring/all_atom_overall_scorer.hh>

namespace promod3 { namespace scoring {

CBPackingScorerPtr LoadCBPackingScorer();

CBetaScorerPtr LoadCBetaScorer();

ReducedScorerPtr LoadReducedScorer();

HBondScorerPtr LoadHBondScorer();

SSAgreementScorerPtr LoadSSAgreementScorer();

TorsionScorerPtr LoadTorsionScorer();

AllAtomInteractionScorerPtr LoadAllAtomInteractionScorer();

AllAtomPackingScorerPtr LoadAllAtomPackingScorer();


BackboneOverallScorerPtr LoadDefaultBackboneOverallScorer();

AllAtomOverallScorerPtr LoadDefaultAllAtomOverallScorer();

}} //ns

#endif
