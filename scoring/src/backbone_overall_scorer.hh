// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_BACKBONE_OVERALL_SCORER_HH
#define PM3_SCORING_BACKBONE_OVERALL_SCORER_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <boost/shared_ptr.hpp>
#include <map>

namespace promod3 { namespace scoring {

class BackboneOverallScorer;
typedef boost::shared_ptr<BackboneOverallScorer> BackboneOverallScorerPtr;

// dummy score for intercept -> always returns 1
class InterceptScorer : public BackboneScorer {
public:

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const { return 1.0; }

  virtual void CalculateScoreProfile(uint start_resnum,
                                     uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const{ 
    profile.assign(num_residues, 1.0);
  }

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx) const { 
    return 1.0; 
  }

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                            const std::vector<uint>& num_residues, 
                            const std::vector<uint>& chain_idx, 
                            std::vector<std::vector<Real> >& profile) const {
    profile.resize(num_residues.size());
    for(uint i = 0; i < num_residues.size(); ++i) {
      profile[i].assign(num_residues[i], 1.0);
    }
  }
};

// simple map of key-scorer pairs
class BackboneOverallScorer {

public:

  // check existance of scorer
  bool Contains(const String& key) const { return map_.find(key) != map_.end(); }

  // r/o get access (throws exc. if nothing found)
  BackboneScorerPtr Get(const String& key) const {
    std::map<String, BackboneScorerPtr>::const_iterator found = map_.find(key);
    if (found == map_.end()) {
      throw promod3::Error("OverallScorer does not contain scorer with key '"
                           + key + "'!");
    } else {
      return found->second;
    }
  }

  // r/w access to map
  BackboneScorerPtr& operator[](const String& key) { return map_[key]; }

  // attach environment to all scorers
  void AttachEnvironment(BackboneScoreEnv& env);

  // for convenience: get list of weight/scorer pairs to use on many bb_list
  // -> use special key "intercept" to add a constant value to the score
  std::vector<std::pair<Real, BackboneScorerPtr> >
  GetWeightedScorers(const std::map<String, Real>& linear_weights) const;

  // calculate one single score
  Real Calculate(const String& key, uint start_resnum, uint num_residues,
                 uint chain_idx = 0) const;

  // linear combination of weighted scores
  // -> use special key "intercept" to add a constant value to the score
  Real CalculateLinearCombination(const std::map<String, Real>& linear_weights,
                                  uint start_resnum, uint num_residues, 
                                  uint chain_idx = 0) const;

  // calculate one single score
  Real Calculate(const String& key,
                 const std::vector<uint>& start_resnum, 
                 const std::vector<uint>& num_residues,
                 const std::vector<uint>& chain_idx) const;


  // calculate one single score
  Real CalculateLinearCombination(const std::map<String, Real>& linear_weights,
                                  const std::vector<uint>& start_resnum, 
                                  const std::vector<uint>& num_residues,
                                  const std::vector<uint>& chain_idx) const;


private:
  std::map<String, BackboneScorerPtr> map_;
};

}} // ns

#endif
