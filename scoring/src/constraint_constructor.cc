// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/constraint_constructor.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/cluster.hh>
#include <promod3/core/geom_base.hh>
#include <ost/seq/alg/subst_weight_matrix.hh>
#include <ost/seq/alg/sequence_similarity.hh>
#include <ost/seq/alg/merge_pairwise_alignments.hh>
#include <ost/mol/atom_view.hh>
#include <ost/mol/entity_view.hh>
#include <ost/seq/alg/contact_prediction_score.hh>
#include <algorithm>
#include <map>

namespace {

void ClusterSequences(const ost::seq::AlignmentHandle& aln,
                      ost::seq::alg::SubstWeightMatrixPtr subst,
                      promod3::core::DistanceMetric metric, 
                      Real cluster_thresh,
                      std::vector<std::vector<uint> >& clusters){

  clusters.clear();

  //clusters all sequences, neglecting the first
  int num_sequences = aln.GetCount() - 1;

  ost::TriMatrix<Real> dist_matrix(num_sequences, Real(0.0));

  //sequence similarity serves as distance for clustering
  for(int i = 0; i < num_sequences; ++i){
    for(int j = i+1; j < num_sequences; ++j){
      Real seq_sim = ost::seq::alg::SequenceSimilarity(aln, subst, true, 
                                                       i+1, j+1);
      dist_matrix.Set(i,j, seq_sim);
    }
  }

  //lets cluster the stuff
  promod3::core::DoClustering(dist_matrix, metric,
                              cluster_thresh, '+', clusters); 


}

void GenerateClusterWeights(const ost::seq::AlignmentHandle& aln, 
                            const std::vector<std::vector<uint> >& clusters, 
                            ost::seq::alg::SubstWeightMatrixPtr subst, 
                            Real gamma, std::vector<Real>& cluster_weights){

  cluster_weights.clear();

  //the average sequence similarity towards SEQRES (first sequence) of 
  //each cluster comes in here
  std::vector<Real> avg_seq_sim;
  for (std::vector<std::vector<uint> >::const_iterator i = clusters.begin(); 
       i != clusters.end(); ++i) {
    Real avg = 0.0;
    for (std::vector<uint>::const_iterator j = i->begin(); j != i->end(); ++j) {
      avg += ost::seq::alg::SequenceSimilarity(aln, subst, true, 0, (*j)+1);
    }
    if (!i->empty()) avg_seq_sim.push_back(avg/i->size());
    else avg_seq_sim.push_back(0.0);
  }

  //lets get the according cluster weights
  for (std::vector<Real>::iterator i = avg_seq_sim.begin();
       i != avg_seq_sim.end(); ++i) {
    cluster_weights.push_back(std::exp(gamma * (*i)));
  }
}

bool GenerateDisCo(const std::vector<std::vector<bool> >& valid_residues,
                   const std::vector<std::vector<geom::Vec3> >& positions,
                   const std::vector<std::vector<uint> >& clusters,
                   const std::vector<Real>& cluster_weights,
                   uint resnum_one, uint resnum_two,
                   std::vector<Real>& disco){

  int idx_one = resnum_one - 1;
  int idx_two = resnum_two - 1;

  //storage for the per cluster functions, it is intended to neglect clusters
  //without position information...
  std::vector<std::vector<Real> > cluster_functions;
  //the effectively used weights, not including the weights from 
  //neglected clusters
  std::vector<Real> weights;

  for (uint cluster_idx = 0; cluster_idx < clusters.size(); ++cluster_idx) {

    std::vector<Real> cluster_function(16, 0.0);
    int num_gaussians = 0;

    for (uint cluster_ele_idx = 0;
         cluster_ele_idx < clusters[cluster_idx].size(); ++cluster_ele_idx) {

      //the sequence we're looking at
      uint seq_idx = clusters[cluster_idx][cluster_ele_idx];

      //check whether both positions are valid
      if( !(valid_residues[seq_idx][idx_one] && 
            valid_residues[seq_idx][idx_two]) ) continue;
      
      Real distance = geom::Length2(positions[seq_idx][idx_one] - 
                                    positions[seq_idx][idx_two]);

      //check, whether positions are close in space
      // (closer than 15 Angstrom)
      if(distance > Real(225.0)) continue;

      distance = std::sqrt(distance);

      //if we reach this point, we actually have a signal!
      //Let's add a gausssian function at this position!
      Real temp;
      for(int i = 0; i < 16; ++i){
        temp = Real(i) - distance;
        cluster_function[i] += std::exp(-Real(0.5) * temp * temp);
      }

      num_gaussians += 1;
    }

    if(num_gaussians == 0) continue; // no signal for this cluster...

    Real factor = Real(1.0) / num_gaussians;
    for(uint i = 0; i < 16; ++i){
      cluster_function[i] *= factor;
    }

    cluster_functions.push_back(cluster_function);
    weights.push_back(cluster_weights[cluster_idx]);
  }

  //if there is no information between the two residues, we directly return
  if(cluster_functions.empty()) return false;

  //normalize the weights
  Real summed_weight = 0.0;
  for(uint i = 0; i < weights.size(); ++i){
    summed_weight += weights[i];
  }
  Real factor = Real(1.0) / summed_weight;
  for(uint i = 0; i < weights.size(); ++i){
    weights[i] *= factor;
  }

  //let's smash everything together...
  disco.assign(16, 0.0);
  for(uint i = 0; i < cluster_functions.size(); ++i){
    for(uint j = 0; j < 16; ++j){
      disco[j] += (weights[i] * cluster_functions[i][j]);
    }
  }

  Real summed_disco = 0.0;
  for(uint i = 0; i < 16; ++i) summed_disco += disco[i];
  
  Real expectation_value = 0.0;
  for(uint i = 0; i < 16; ++i){
    expectation_value += (i * disco[i] / summed_disco);
  }

  if(expectation_value < 12.0) return true;
  else return false;
}

} // anon ns

namespace promod3 { namespace scoring {

DiscoContainer::DiscoContainer(const ost::seq::SequenceHandle& seqres, 
                               promod3::scoring::PairwiseFunctionType ft): 
                                             function_type_(ft),
                                             seqres_(seqres){ 
  if(seqres.GetGaplessString().size() != seqres.GetString().size()){
    throw promod3::Error("Seqres must not contain gaps!");
  }
  //avoid reallocations, empty vectors are not too memory intensive...
  positions_.reserve(1000);
  valid_positions_.reserve(1000);
}
 

void DiscoContainer::AddStructuralInfo(const ost::seq::AlignmentHandle& aln){

  //check how many sequences there are
  if(aln.GetCount() != 2){
    String err = "Number of sequences in alignment must be exactly two! ";
    err += "The first is the SEQRES, the second the template sequence with ";
    err += "attached structure";
    throw promod3::Error(err);
  }

  ost::seq::ConstSequenceHandle seqres = aln.GetSequence(0);
  ost::seq::ConstSequenceHandle template_seq = aln.GetSequence(1);

  //check, whether seqres is consistent
  if(seqres.GetGaplessString() != seqres_.GetString()){
    String err = "First sequence of the alignment must be consistent with the ";
    err += "SEQRES you initialized the DiscoContainer with!";
    throw promod3::Error(err);
  }

  //check, whether there is a view attached to the second sequence
  if(!template_seq.HasAttachedView()){
    String err = "Second sequence in the alignment must have a view attached!";
    throw promod3::Error(err);
  }

  //start to extract the position data
  std::vector<geom::Vec3> positions;
  std::vector<bool> valid_positions;
  positions.reserve(seqres_.GetLength());
  valid_positions.reserve(seqres_.GetLength());

  for(int i = 0; i < aln.GetLength(); ++i){

    //we don't look at columns where the SEQRES has a gap,
    if(seqres[i] == '-') continue;

    ost::mol::ResidueView res = template_seq.GetResidue(i);
    if(!res.IsValid()){
      positions.push_back(geom::Vec3());
      valid_positions.push_back(false);
      continue;
    }

    geom::Vec3 pos; //position to be extracted...

    if(function_type_ == promod3::scoring::CB_PAIRWISE_FUNCTION){
      ost::mol::AtomView cb = res.FindAtom("CB");

      if(cb.IsValid()){
        pos = cb.GetPos();
      }
      else{
        //as a fallback, the cb gets reconstructed using the backbone
        ost::mol::AtomView n = res.FindAtom("N");
        ost::mol::AtomView ca = res.FindAtom("CA");
        ost::mol::AtomView c = res.FindAtom("C");
        if(! (n.IsValid() && ca.IsValid() && c.IsValid())){
          positions.push_back(geom::Vec3());
          valid_positions.push_back(false);
          continue;
        }
        promod3::core::ConstructCBetaPos(n.GetPos(),ca.GetPos(),c.GetPos(),pos);
      }
    } 

    else{
      ost::mol::AtomView ca = res.FindAtom("CA");
      if(!ca.IsValid()){
        positions.push_back(geom::Vec3());
        valid_positions.push_back(false);
        continue;
      }
      pos = ca.GetPos();
    }

    positions.push_back(pos);
    valid_positions.push_back(true);
  }

  positions_.push_back(positions);
  valid_positions_.push_back(valid_positions);

  //once we extracted all the positions, we want to also store the alignment.
  //The problem is, that we don't want the attached EntityView to save memory

  ost::seq::AlignmentHandle new_aln = ost::seq::CreateAlignment();
  ost::seq::SequenceHandle new_seqres = ost::seq::CreateSequence("seqres", 
                                                            seqres.GetString());
  ost::seq::SequenceHandle new_template_seq = ost::seq::CreateSequence("templ_seq", 
                                                      template_seq.GetString());
 
  new_aln.AddSequence(new_seqres);
  new_aln.AddSequence(new_template_seq);

  alignments_.push_back(new_aln);
} 

void DiscoContainer::AttachConstraints(promod3::scoring::BackboneScoreEnv& env, 
                                       const std::vector<uint>& chain_indices,
                                       Real cluster_thresh, Real gamma){

  if(positions_.empty()){
    throw promod3::Error("DiscoContainer is empty!");
  }

  //let's merge the alignments together
  ost::seq::AlignmentHandle full_aln = ost::seq::alg::MergePairwiseAlignments(
                                       alignments_, seqres_);

  ost::seq::SequenceList env_seqres_list = env.GetSeqres();

  for(std::vector<uint>::const_iterator i = chain_indices.begin();
      i != chain_indices.end(); ++i){

    if((*i) >= static_cast<uint>(env_seqres_list.GetCount())){
      String err = "Provided chain idx is larger than number of seqres ";
      err += "sequences in BackboneScoreEnv!";
      throw promod3::Error(err);
    }

    ost::seq::ConstSequenceHandle env_seqres = env_seqres_list[*i];
    if(env_seqres.GetString() != seqres_.GetString()){
      String err = "Seqres of DiscoContainer must be consistent with ";
      err += "seqres of specified chain idx in the BackboneScoreEnv!";
      throw promod3::Error(err);
    }
  }

  //let's cluster
  std::vector<std::vector<uint> > clusters;
  ost::seq::alg::SubstWeightMatrixPtr blosum62(new ost::seq::alg::SubstWeightMatrix);
  blosum62->AssignPreset(ost::seq::alg::SubstWeightMatrix::BLOSUM62);
  ClusterSequences(full_aln, blosum62, promod3::core::AVG_DIST_METRIC, 
                   cluster_thresh, clusters);

  //let's generate the cluster weights
  std::vector<Real> cluster_weights;
  GenerateClusterWeights(full_aln, clusters, blosum62, gamma, cluster_weights);

  //let's add the constraint functions!
  int seqres_length = seqres_.GetLength();
  for(int i = 0; i < seqres_length; ++i){
    for(int j = i + 1; j < seqres_length; ++j){
      std::vector<Real> disco;
      bool contains_info = GenerateDisCo(valid_positions_, positions_, 
                                         clusters, cluster_weights,
                                         i + 1, j + 1, disco);
      if(contains_info){
        promod3::scoring::ConstraintFunctionPtr 
        p(new promod3::scoring::ConstraintFunction(0.0, 15.0, disco));
        uint f_idx = env.AddPairwiseFunction(p, function_type_);
        for(std::vector<uint>::const_iterator ch_it = chain_indices.begin();
            ch_it != chain_indices.end(); ++ch_it){
          env.ApplyPairwiseFunction(*ch_it, i + 1, *ch_it, j + 1, f_idx);
        }
      }
    }
  }  
}

void AttachEvolutionaryConstraints(promod3::scoring::BackboneScoreEnv& env, 
                                   const ost::seq::AlignmentHandle& aln,
                                   const std::vector<uint>& chain_indices,
                                   promod3::scoring::PairwiseFunctionType ft,
                                   Real fraction, 
                                   uint constant_function_length,
                                   uint tail_length){

  /*
  Predicts contacts from a multiple sequence alignment using a combination
  of Mutual Information (*MI*) and the Contact Substitution Score (*CoEvoSc*).
  MI is calculated with the APC and small number corrections as well as with a 
  transformation into Z-scores. The *CoEvoSc* is calculated using the default 
  PairSubstWeightMatrix (see seq.alg.LoadDefaultPairSubstWeightMatrix).
  The final score for a pair of columns *(i,j)* of **aln** is obtained from:
  
  Sc(i,j)=MI(i,j)exp(CoEvoSc(i,j))      if *(i,j)* >=0
  
  Sc(i,j)=MI(i,j)exp(1-CoEvoSc(i,j))    if *(i,j)* <0

  */

  if(fraction <= 0.0 || fraction > 1.0){
    String err = "The fraction of constraints to add should be larger zero ";
    err += "and smaller of equal one!";
    throw promod3::Error(err);
  }

  if(aln.GetCount() < 10){
    String err = "Do you really think contact prediction with such a small ";
    err += "alignment works?";
    throw promod3::Error(err);
  }

  ost::seq::ConstSequenceHandle seqres = aln.GetSequence(0);
  ost::seq::SequenceList env_seqres_list = env.GetSeqres();

  for(std::vector<uint>::const_iterator i = chain_indices.begin();
      i != chain_indices.end(); ++i){

    if((*i) >= static_cast<uint>(env_seqres_list.GetCount())){
      String err = "Provided chain idx is larger than number of seqres ";
      err += "sequences in BackboneScoreEnv!";
      throw promod3::Error(err);
    }

    ost::seq::ConstSequenceHandle env_seqres = env_seqres_list[*i];
    if(env_seqres.GetString() != seqres.GetGaplessString()){
      String err = "First sequence of input alignment must be consistent with ";
      err += "seqres of specified chain idx in the BackboneScoreEnv!";
      throw promod3::Error(err);
    }
  }

  ost::seq::alg::ContactPredictionScoreResult mi = 
  ost::seq::alg::CalculateMutualInformation(aln);
  ost::seq::alg::ContactPredictionScoreResult CoEvoSc = 
  ost::seq::alg::CalculateContactSubstitutionScore(aln);

  uint ncol=aln.GetLength();
  for(uint i = 0; i < ncol; ++i){
    for(uint j = 0; j < ncol; ++j){
      if(mi.matrix[i][j]>=0){
        mi.matrix[i][j]=mi.matrix[i][j]*(std::exp(CoEvoSc.matrix[i][j]));
      }
      else{
        mi.matrix[i][j]=mi.matrix[i][j]*(std::exp(1.-CoEvoSc.matrix[i][j]));
      }
    }
  }

  //we're only interested in columns where the seqres has no gap
  //we need a mapping from the column indices to the position in the seqres
  std::map<uint,uint> idx_resnum_mapper;
  uint current_resnum = 1;
  for(uint i = 0; i < ncol; ++i){
    if(seqres[i] == '-') continue;
    idx_resnum_mapper[i] = current_resnum;
    current_resnum += 1;
  }

  //inner pair: residue number pair
  //outer pair: score and corresponding residue number pair
  std::vector<std::pair<Real,std::pair<uint,uint> > > pairwise_scores;
  for(uint i = 0; i < ncol; ++i){
    if(seqres[i] == '-') continue;
    for(uint j = i + 1; j < ncol; ++j){
      if(seqres[j] == '-') continue;
      pairwise_scores.push_back(std::make_pair(mi.matrix[i][j],
                                std::make_pair(idx_resnum_mapper[i],
                                               idx_resnum_mapper[j])));
    }
  }
  std::sort(pairwise_scores.rbegin(),pairwise_scores.rend());

  uint num_relevant_interactions = std::ceil(fraction * pairwise_scores.size());

  for(uint i = 0; i < num_relevant_interactions; ++i){
    Real score = pairwise_scores[i].first;
    uint resnum_one = pairwise_scores[i].second.first;
    uint resnum_two = pairwise_scores[i].second.second;

    //we first build up the constant part
    std::vector<Real> values(constant_function_length + 1, score);
    //and add a linearly decreasing tail
    for(uint j = 0; j < tail_length; ++j){
      values.push_back(score * (tail_length - j) / (tail_length + 1));
    }

    promod3::scoring::ConstraintFunctionPtr cf(
      new promod3::scoring::ConstraintFunction(0, constant_function_length + 
                                               tail_length, values));
    uint f_idx = env.AddPairwiseFunction(cf,ft);

    for(std::vector<uint>::const_iterator ch_it = chain_indices.begin();
        ch_it != chain_indices.end(); ++ch_it){
      env.ApplyPairwiseFunction(*ch_it, resnum_one, *ch_it, resnum_two, f_idx);
    }
  }
}

}} // ns
