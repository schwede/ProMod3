// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/density_score.hh>

namespace promod3{ namespace scoring{


Real DensityScore(const loop::BackboneList& bb_list, ost::img::MapHandle& map, 
                  Real resolution, bool all_atom) {

  geom::AlignedCuboid bounding_box = bb_list.GetBounds(true);
  geom::Vec3 min = bounding_box.GetMin();
  geom::Vec3 max = bounding_box.GetMax();
  geom::Vec3 sampling = map.GetSpatialSampling();

  //thats the extend of the image we're actually interested in
  uint x_extent = std::ceil((max[0] - min[0]) / sampling[0]);
  uint y_extent = std::ceil((max[1] - min[1]) / sampling[1]);
  uint z_extent = std::ceil((max[2] - min[2]) / sampling[2]); 

  //let's enlarge a bit...
  Real x_overlap = Real(2 * resolution) / sampling[0];
  Real y_overlap = Real(2 * resolution) / sampling[1];
  Real z_overlap = Real(2 * resolution) / sampling[2];
  x_extent += static_cast<uint>(2 * x_overlap);
  y_extent += static_cast<uint>(2 * y_overlap);
  z_extent += static_cast<uint>(2 * z_overlap);

  //let's create a new image, where we will fill the density of the bb_list
  //we make sure, that the grid cubes exactly match the ones in the
  //set map
  ost::img::Size s(x_extent,y_extent,z_extent);

  ost::img::MapHandle bb_list_map = ost::img::CreateImage(s);
  bb_list_map.SetSpatialSampling(sampling);
  
  geom::Vec3 pixel_coord = map.CoordToIndex(min - geom::Vec3(x_overlap,
                                                             y_overlap,
                                                             z_overlap));

  ost::img::Point rounded_pixel_coord(round(pixel_coord[0]),
                                      round(pixel_coord[1]),
                                      round(pixel_coord[2]));

  min = map.IndexToCoord(rounded_pixel_coord);
  bb_list_map.SetAbsoluteOrigin(min);

  //let's fill the density of the BackboneList
  bb_list.InsertInto(bb_list_map, resolution, all_atom);

  uint num_voxels = (s[0] * s[1] * s[2]);

  std::vector<Real> map_values(num_voxels);
  std::vector<Real> bb_list_values(num_voxels);

  uint vec_position = 0;
  ost::img::Point p, p_orig;
  for(uint i = 0; i < s[0]; ++i){
    for(uint j = 0; j < s[1]; ++j){
      for(uint k = 0; k < s[2]; ++k){
        p = ost::img::Point(i,j,k);
        p_orig = p + rounded_pixel_coord;
        //will return 0.0 if outside of map
        map_values[vec_position] = map.GetReal(p_orig);
        bb_list_values[vec_position] = bb_list_map.GetReal(p); 
        ++vec_position;
      }
    }
  }

  //we have to get the means, the standart deviations and the correlation itself...
  Real m_one = 0.0;
  Real m_two = 0.0;

  Real s_one = 0.0;
  Real s_two = 0.0;

  Real corr = 0.0;

  for(uint i = 0; i < num_voxels; ++i){
    m_one += map_values[i];
    m_two += bb_list_values[i];
  }

  m_one /= num_voxels;
  m_two /= num_voxels;

  Real v1, v2;
  for(uint i = 0; i < num_voxels; ++i){
    v1 = map_values[i] - m_one;
    v2 = bb_list_values[i] - m_two;
    s_one += (v1 * v1);
    s_two += (v2 * v2);
    corr += (v1 * v2);  
  }

  corr /= std::sqrt(s_one * s_two);

  return corr;
}

}} //ns
