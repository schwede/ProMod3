// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/clash_env_listener.hh>


namespace promod3 { namespace scoring {

ClashEnvListener::ClashEnvListener(): env_(8.0),
                                      env_data_(NULL) { }

ClashEnvListener::~ClashEnvListener() {
  if (env_data_ != NULL) {
    delete [] is_glycine_;
    delete [] env_data_;
  }
}

void ClashEnvListener::Init(const BackboneScoreEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashEnvListener::Init", 2);

  if (env_data_ != NULL) {
    // the env listener has already been initialized at some point...
    // let's clean up first...
    delete [] is_glycine_;
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  idx_handler_ = base_env.GetIdxHandlerData();
  env_set_ = base_env.GetEnvSetData();
  n_pos_data_ = base_env.GetNPosData();
  ca_pos_data_ = base_env.GetCAPosData();
  c_pos_data_ = base_env.GetCPosData();
  o_pos_data_ = base_env.GetOPosData();
  cb_pos_data_ = base_env.GetCBPosData();
  aa_data_ = base_env.GetAAData();

  uint num_residues = idx_handler_->GetNumResidues();

  env_data_ = new ClashSpatialOrganizerItem[5*num_residues];
  is_glycine_ = new bool[num_residues];

  for (uint i = 0; i < num_residues; ++i) {
    // N
    env_data_[5*i  ].clash_type = BB_CLASH_N;
    env_data_[5*i  ].idx = i;
    // CA
    env_data_[5*i+1].clash_type = BB_CLASH_C;
    env_data_[5*i+1].idx = i;
    // C
    env_data_[5*i+2].clash_type = BB_CLASH_C;
    env_data_[5*i+2].idx = i;
    // O
    env_data_[5*i+3].clash_type = BB_CLASH_O;
    env_data_[5*i+3].idx = i;
    // CB
    env_data_[5*i+4].clash_type = BB_CLASH_C;
    env_data_[5*i+4].idx = i;

    if(aa_data_[i] == ost::conop::GLY) {
      is_glycine_[i] = true;
    }
    else {
      is_glycine_[i] = false;
    }
  }
}

void ClashEnvListener::SetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashEnvListener::SetEnvironment", 2);

  geom::Vec3 pos;
  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    // N
    pos = n_pos_data_[*i];
    env_data_[5*(*i)].pos = pos;
    env_.Add(&env_data_[5*(*i)], pos);
    // CA
    pos = ca_pos_data_[*i];
    env_data_[5*(*i)+1].pos = pos;
    env_.Add(&env_data_[5*(*i)+1], pos);
    // C
    pos = c_pos_data_[*i];
    env_data_[5*(*i)+2].pos = pos;
    env_.Add(&env_data_[5*(*i)+2], pos);
    // O
    pos = o_pos_data_[*i];
    env_data_[5*(*i)+3].pos = pos;
    env_.Add(&env_data_[5*(*i)+3], pos);
    // CB (if necessary)
    if (aa_data_[*i] != ost::conop::GLY) {
      pos = cb_pos_data_[*i];
      env_data_[5*(*i)+4].pos = pos;
      env_.Add(&env_data_[5*(*i)+4], pos);
    }
  }
}

void ClashEnvListener::ResetEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashEnvListener::ResetEnvironment", 2);

  geom::Vec3 pos, old_pos;
  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    // N
    old_pos = env_data_[5*(*i)].pos;
    pos = n_pos_data_[*i];
    env_data_[5*(*i)].pos = pos;
    env_.Reset(&env_data_[5*(*i)], old_pos, pos);
    // CA
    old_pos = env_data_[5*(*i)+1].pos;
    pos = ca_pos_data_[*i];
    env_data_[5*(*i)+1].pos = pos;
    env_.Reset(&env_data_[5*(*i)+1], old_pos, pos);
    // C
    old_pos = env_data_[5*(*i)+2].pos;
    pos = c_pos_data_[*i];
    env_data_[5*(*i)+2].pos = pos;
    env_.Reset(&env_data_[5*(*i)+2], old_pos, pos);
    // O
    old_pos = env_data_[5*(*i)+3].pos;
    pos = o_pos_data_[*i];
    env_data_[5*(*i)+3].pos = pos;
    env_.Reset(&env_data_[5*(*i)+3], old_pos, pos);
    // CB (if necessary)
    if (aa_data_[*i] != ost::conop::GLY) {
      old_pos = env_data_[5*(*i)+4].pos;
      pos = cb_pos_data_[*i];
      env_data_[5*(*i)+4].pos = pos;
      env_.Reset(&env_data_[5*(*i)+4], old_pos, pos);
    }
  }
}

void ClashEnvListener::ClearEnvironment(const std::vector<uint>& idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ClashEnvListener::ClearEnvironment", 2);

  for (std::vector<uint>::const_iterator i = idx.begin();
       i != idx.end(); ++i) {
    uint num_clash_items = (aa_data_[*i] != ost::conop::GLY) ? 5 : 4;
    for (uint j = 0; j < num_clash_items; ++j) {
      env_.Remove(&env_data_[5*(*i)+j], env_data_[5*(*i)+j].pos);
    }
  }
}

}}
