// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/reduced_score.hh>


namespace promod3 { namespace scoring {

namespace {
// HELPERS

inline void GetReducedAngles(const geom::Vec3& v1, const geom::Vec3& v2, 
                             const geom::Vec3& v3, Real& alpha, Real& beta,
                             Real& gamma) {

  // this function expects three vectors.
  // v1: normalized direction vector
  // v2: normalized vector between CA positions
  // v3: normalized direction vector
  // following ranges get ensured:
  // alpha: [0, pi[
  // beta: [0, pi[
  // gamma: [0, 2 * pi[
  // with pi (2*pi respectively) not being included, 
  // range checks for bin calculations are unnecessary

  Real dot_product = geom::Dot(v1, v2);
  dot_product = std::max(Real(-0.999999), dot_product);
  dot_product = std::min(Real(1.0),dot_product);
  alpha = std::acos(dot_product);

  dot_product = -geom::Dot(v2, v3);
  dot_product = std::max(Real(-0.999999), dot_product);
  dot_product = std::min(Real(1.0),dot_product);
  beta = std::acos(dot_product);

  geom::Vec3 v12cross = -geom::Cross(v1, v2);
  geom::Vec3 v23cross = geom::Cross(v2, v3);
  gamma = std::min(std::atan2(-geom::Dot(v1, v23cross),
                              geom::Dot(v12cross,v23cross)) + Real(M_PI), 
                              Real(6.28318));
}




} // anon ns

ReducedScorer::ReducedScorer(Real cutoff, uint dist_bins, uint angle_bins,
                             uint dihedral_bins, uint seq_sep):
                             cutoff_(cutoff),
                             dist_bins_(dist_bins),
                             angle_bins_(angle_bins),
                             dihedral_bins_(dihedral_bins),
                             seq_sep_(seq_sep),
                             do_internal_scores_(true),
                             do_external_scores_(true),
                             normalize_(true),
                             env_(NULL) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::ReducedScorer", 2);

  if (cutoff < 0.0) {
    throw promod3::Error("Reduced cutoff must not be negative!");
  }
  if (dist_bins == 0) {
    throw promod3::Error("Reduced dist bins must be nonzero!");
  }
  if (angle_bins == 0) {
    throw promod3::Error("Reduced angle bins must be nonzero!");
  }
  if (dihedral_bins == 0) {
    throw promod3::Error("Reduced dihedral bins must be nonzero!");
  }
  if (seq_sep == 0) {
    throw promod3::Error("Reduced seq_sep must be at least 1!");
  }

  uint num_values = NumEnergies();
  energies_ = new Real[num_values];
  memset(energies_, 0, num_values * sizeof(Real));
}

ReducedScorer::~ReducedScorer() {
  delete [] energies_;
}

ReducedScorerPtr ReducedScorer::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);

  // here starts the score specific stuff
  ReducedScorerPtr p(new ReducedScorer);
  in_stream.read(reinterpret_cast<char*>(&p->cutoff_), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->dist_bins_), sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->angle_bins_), sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->dihedral_bins_), sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&p->seq_sep_), sizeof(uint));
  uint num_values = p->NumEnergies();
  p->energies_ = new Real[num_values];
  in_stream.read(reinterpret_cast<char*>(p->energies_), num_values*sizeof(Real));

  return p;
}

void ReducedScorer::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);

  // the actual content of the score
  out_stream.write(reinterpret_cast<char*>(&cutoff_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&dist_bins_), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&angle_bins_), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&dihedral_bins_), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&seq_sep_), sizeof(uint));
  uint num_bytes = NumEnergies() * sizeof(Real);
  out_stream.write(reinterpret_cast<char*>(energies_), num_bytes);
}

ReducedScorerPtr ReducedScorer::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);

  // data
  ReducedScorerPtr p(new ReducedScorer);
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void ReducedScorer::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);

  // data
  this->SerializeLoadSave(out_stream);

  out_stream_.close();
}

void ReducedScorer::SetEnergy(ost::conop::AminoAcid a, ost::conop::AminoAcid b,
                              uint dist_bin, uint alpha_bin, uint beta_bin,
                              uint gamma_bin, Real e) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::SetEnergy", 2);

  if (a == ost::conop::XXX || b == ost::conop::XXX) {
    throw promod3::Error("Cannot set energy for invalid amino acid!");
  }
  if (dist_bin >= dist_bins_) {
    throw promod3::Error("Cannot set reduced energy for invalid dist bin");
  }
  if (alpha_bin >= angle_bins_) {
    throw promod3::Error("Cannot set reduced energy for invalid alpha bin");
  }
  if (beta_bin >= angle_bins_) {
    throw promod3::Error("Cannot set reduced energy for invalid beta bin");
  }
  if (gamma_bin >= dihedral_bins_) {
    throw promod3::Error("Cannot set reduced energy for invalid gamma bin");
  }

  energies_[this->EIdx(a, b, dist_bin, alpha_bin, beta_bin, gamma_bin)] = e;
  energies_[this->EIdx(b, a, dist_bin, beta_bin, alpha_bin, gamma_bin)] = e;
}


Real ReducedScorer::CalculateScore(uint start_resnum, uint num_residues,
                                   uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);

  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  // sum them
  Real external_score = 0.0;
  Real internal_score = 0.0;
  for (uint i = 0; i < external_scores.size(); ++i) {
    external_score += external_scores[i];
    internal_score += internal_scores[i];
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && !external_scores.empty()) {
    external_score /= external_scores.size();
  }

  return external_score;
}


void ReducedScorer::CalculateScoreProfile(uint start_resnum, uint num_residues,
                                          uint chain_idx,
                                          std::vector<Real>& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::CalculateScoreProfile", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<uint> idx_vector;
  std::vector<bool> occupied;
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, idx_vector, 
                                               occupied);
  // get scores
  std::vector<Real> external_scores;
  std::vector<Real> internal_scores;
  this->Score(idx_vector, external_scores, internal_scores, occupied);

  profile.resize(external_scores.size());
  for(uint i = 0; i < external_scores.size(); ++i) {
    profile[i] = external_scores[i] + internal_scores[i];
  }
}


Real ReducedScorer::CalculateScore(const std::vector<uint>& start_resnum, 
                                   const std::vector<uint>& num_residues,
                                   const std::vector<uint>& chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores and sum them up
  Real external_score = 0.0;
  Real internal_score = 0.0;
  uint n_scored_residues = 0;
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    this->Score(indices_vec[i], external_scores, internal_scores, occupied);
    for (uint j = 0; j < external_scores.size(); ++j) {
      external_score += external_scores[j];
      internal_score += internal_scores[j];
    }
    n_scored_residues += external_scores.size();
  }

  // all interactions in internal_score have been observed twice!
  external_score += Real(0.5)*internal_score;

  if(normalize_ && n_scored_residues > 0) {
    external_score /= n_scored_residues;
  }

  return external_score;
}


void ReducedScorer::CalculateScoreProfile(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::CalculateScore", 2);

  if (env_ == NULL) {
    String err = "Can only calculate score when environment is attached!";
    throw promod3::Error(err);
  }

  // setup
  std::vector<std::vector<uint> > indices_vec;
  std::vector<bool> occupied; 
  env_->GetIdxHandler()->SetupScoreCalculation(start_resnum, num_residues, 
                                               chain_idx, indices_vec, 
                                               occupied);

  // get scores 
  profile.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    std::vector<Real> external_scores;
    std::vector<Real> internal_scores;
    std::vector<Real>& current_profile = profile[i];
    this->Score(indices_vec[i], current_profile, internal_scores, occupied);
    for (uint j = 0; j < current_profile.size(); ++j) {
      current_profile[j] += internal_scores[j];

    }
  }
}


void ReducedScorer::Score(const std::vector<uint>& indices,
                          std::vector<Real>& external_scores,
                          std::vector<Real>& internal_scores,
                          std::vector<bool>& occupied) const {

  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ReducedScorer::Score", 2);

  // setup
  uint n_indices = indices.size();

  external_scores.assign(n_indices, 0.0);
  internal_scores.assign(n_indices, 0.0);

  if(n_indices == 0) return;

  // let's reduce the cutoff a bit -> hack to ensure bin always within range
  const Real cutoff = cutoff_ - 0.001;
  // get inv sizes for binning
  const Real inv_dist_bin_size = dist_bins_ / cutoff_;
  const Real inv_angle_bin_size = angle_bins_ / Real(M_PI);
  const Real inv_dihedral_bin_size = dihedral_bins_ / (2 * Real(M_PI));

  // some param required in score calculation
  Real alpha;
  Real beta;
  Real gamma;
  uint dist_bin;
  uint alpha_bin;
  uint beta_bin;
  uint gamma_bin;
  Real e;
  geom::Vec3 ca_vec;

  // let's go over every loop residue
  ReducedSpatialOrganizer::WithinResult within_result;
  std::pair<ReducedSpatialOrganizerItem*,Real>* a;
  const loop::IdxHandler* idx_handler = env_->GetIdxHandler();
  const int* env_set = env_->GetEnvSetData();
  for (uint i = 0; i < n_indices; ++i) {
    int my_idx = indices[i];

    if(!env_set[my_idx]) {
      throw promod3::Error("Must set environment before scoring!");
    }

    int my_chain_idx = idx_handler->GetChainIdx(my_idx);
    ost::conop::AminoAcid my_aa = env_->GetEnvData(my_idx).aa;
    geom::Vec3 my_pos = env_->GetEnvData(my_idx).pos;
    geom::Vec3 my_axis = env_->GetEnvData(my_idx).axis;

    within_result = env_->FindWithin(my_pos, cutoff);
    a = within_result.first;

    for (uint j = 0; j < within_result.second; ++j) {
      int other_idx = int(a[j].first->idx);
      int other_chain_idx = idx_handler->GetChainIdx(other_idx);

      if (my_chain_idx != other_chain_idx || 
          std::abs(my_idx - other_idx) >= seq_sep_) {
        ca_vec = a[j].first->pos - my_pos;
        ca_vec *= (Real(1.0) / a[j].second);
        GetReducedAngles(my_axis, ca_vec, a[j].first->axis,
                         alpha, beta, gamma);
        dist_bin = a[j].second * inv_dist_bin_size;
        alpha_bin = alpha * inv_angle_bin_size;
        beta_bin = beta * inv_angle_bin_size;
        gamma_bin = gamma * inv_dihedral_bin_size;

        e = energies_[this->EIdx(my_aa, a[j].first->aa, dist_bin, 
                                 alpha_bin, beta_bin, gamma_bin)];

        if(occupied[other_idx] && do_internal_scores_) {
          internal_scores[i] += e;
        }

        if(!occupied[other_idx] && do_external_scores_) {
          external_scores[i] += e;
        }
      }
    }
  }
  
}

void ReducedScorer::AttachEnvironment(BackboneScoreEnv& env) {
  env_ = env.UniqueListener<ReducedEnvListener>("ReducedEnvListener");
}

}}
