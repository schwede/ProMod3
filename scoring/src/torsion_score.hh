// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_TORSION_SCORE_HH
#define PM3_SCORING_TORSION_SCORE_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>
#include <set>

namespace promod3 { namespace scoring {

class TorsionScorer;
typedef boost::shared_ptr<TorsionScorer> TorsionScorerPtr;


class TorsionScorer : public BackboneScorer{

public:

  TorsionScorer(std::vector<String>& group_definitions,
                uint bins_per_dimension);

  virtual ~TorsionScorer();

  static TorsionScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static TorsionScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetEnergy(uint group_idx, uint phi_bin, uint psi_bin, Real e);

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  void AttachEnvironment(BackboneScoreEnv& env);

private:

  TorsionScorer(): normalize_(true), attached_environment_(false) { }

  inline uint EIdx(uint group_idx, uint torsion_bin) const{ 
    return group_idx * bins_squared_ + torsion_bin;
  }

  void Score(const std::vector<uint>& indices, 
             std::vector<Real>& scores) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, num_groups_); 
    core::ConvertBaseType<uint32_t>(ds, bins_per_dimension_);
    core::ConvertBaseType<uint32_t>(ds, bins_squared_); 
    core::ConvertBaseType<float>(ds, bin_size_);
    for(uint i = 0; i < 8000; ++i){
      core::ConvertBaseType<float>(ds, index_mapper_[i]);
    }
    uint num_values = num_groups_ * bins_squared_;
    if(ds.IsSource()){
      energies_ = new Real[num_values];
      memset(energies_, 0, num_values * sizeof(Real));
    } 
    for(uint i = 0; i < num_values; ++i){
      core::ConvertBaseType<float>(ds, energies_[i]);
    }
  }

  uint num_groups_;
  uint bins_per_dimension_;
  uint bins_squared_;
  Real bin_size_;
  uint index_mapper_[8000];
  Real* energies_;
  bool normalize_;

  // environment specific information, that will be set upon calling 
  // AttachEnvironment
  bool attached_environment_;
  const loop::IdxHandler* idx_handler_;
  const int* env_set_;
  const geom::Vec3* n_pos_;
  const geom::Vec3* ca_pos_;
  const geom::Vec3* c_pos_;
  std::vector<uint> torsion_group_indices_;
};


}}//ns

#endif
