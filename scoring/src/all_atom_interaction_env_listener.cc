// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/all_atom_interaction_env_listener.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace scoring {

AllAtomInteractionEnvListener::AllAtomInteractionEnvListener()
                                      : env_(10.0), env_data_(NULL) { }

AllAtomInteractionEnvListener::~AllAtomInteractionEnvListener() {
  if (env_data_ != NULL) delete [] env_data_;
}

void AllAtomInteractionEnvListener::Init(const loop::AllAtomEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomInteractionEnvListener::Init", 2);

  if (env_data_ != NULL) {
    // the env listener has already been initialized at some point...
    // let's clean up first...
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  // set data
  loop::ConstAllAtomPositionsPtr all_pos = base_env.GetAllPosData();
  const uint num_residues = all_pos->GetNumResidues();
  const uint num_atoms = all_pos->GetNumAtoms();
  // env_data: set fix res. indices and heavy atom type
  env_data_ = new AllAtomInteractionSpatialOrganizerItem[num_atoms];
  for (uint res_idx = 0; res_idx < num_residues; ++res_idx) {
    const uint first_idx = all_pos->GetFirstIndex(res_idx);
    const uint last_idx = all_pos->GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      env_data_[idx].aaa = all_pos->GetAAA(idx);
      env_data_[idx].res_idx = res_idx;
    }
  }
  // is_set: all false for now
  is_set_.assign(num_atoms, false);
}

void AllAtomInteractionEnvListener::ResetEnvironment(
                                      const loop::AllAtomEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                        "AllAtomInteractionEnvListener::ResetEnvironment", 2);

  loop::ConstAllAtomPositionsPtr all_pos = base_env.GetAllPosData();
  for (uint res_idx = 0; res_idx < all_pos->GetNumResidues(); ++res_idx) {
    SetResidue_(all_pos, res_idx);
  }
}

void AllAtomInteractionEnvListener::UpdateEnvironment(
                                      const loop::AllAtomEnv& base_env, 
                                      const std::vector<uint>& res_indices) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                        "AllAtomInteractionEnvListener::UpdateEnvironment", 2);

  loop::ConstAllAtomPositionsPtr all_pos = base_env.GetAllPosData();
  for (uint i = 0; i < res_indices.size(); ++i) {
    SetResidue_(all_pos, res_indices[i]);
  }
}

void AllAtomInteractionEnvListener::SetResidue_(
                                      loop::ConstAllAtomPositionsPtr all_pos,
                                      const uint res_idx) {
  // no assumptions made on input...
  const uint first_idx = all_pos->GetFirstIndex(res_idx);
  const uint last_idx = all_pos->GetLastIndex(res_idx);
  for (uint idx = first_idx; idx <= last_idx; ++idx) {
    if (all_pos->IsSet(idx)) {
      if (is_set_[idx]) {
        // update
        const geom::Vec3 old_pos = env_data_[idx].pos;
        env_data_[idx].pos = all_pos->GetPos(idx);
        env_.Reset(&env_data_[idx], old_pos, all_pos->GetPos(idx));
      } else {
        // set
        env_data_[idx].pos = all_pos->GetPos(idx);
        env_.Add(&env_data_[idx], all_pos->GetPos(idx));
        is_set_[idx] = true;
      }
    } else {
      if (is_set_[idx]) {
        // clear
        env_.Remove(&env_data_[idx], all_pos->GetPos(idx));
        is_set_[idx] = false;
      }
    }
  }
}

}} // ns
