// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SCORING_SS_AGREEMENT_SCORE_HH
#define PM3_SCORING_SS_AGREEMENT_SCORE_HH

#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/scoring/ss_agreement_env_listener.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>

#include <ost/mol/alg/sec_struct.hh>

#include <set>

namespace promod3 { namespace scoring {

class SSAgreementScorer;
typedef boost::shared_ptr<SSAgreementScorer> SSAgreementScorerPtr;

inline int GetDSSPIdx(char dssp_state) {
  switch(dssp_state){
    case 'H': return 0;
    case 'E': return 1;
    case 'C': return 2;
    case 'G': return 3;
    case 'B': return 4;
    case 'S': return 5;
    case 'T': return 6;
    case 'I': return 7;
    default: throw promod3::Error("Invalid dssp state observed!");
  }
}

inline int GetPsipredIdx(char psipred_state) {
  switch(psipred_state){
    case 'H': return 0;
    case 'E': return 1;
    case 'C': return 2;
    default: throw promod3::Error("Invalid psipred state observed!");
  }
}

class SSAgreementScorer : public BackboneScorer {

public:

  SSAgreementScorer();

  virtual ~SSAgreementScorer();

  static SSAgreementScorerPtr Load(const String& filename);

  void Save(const String& filename);

  static SSAgreementScorerPtr LoadPortable(const String& filename);

  void SavePortable(const String& filename);

  void SetScore(char psipred_state, int psipred_cfi,
                char dssp_state, Real score);

  virtual Real CalculateScore(uint start_resnum, uint num_residues,
                              uint chain_idx) const;

  virtual void CalculateScoreProfile(uint start_resnum, uint num_residues,
                                     uint chain_idx,
                                     std::vector<Real>& profile) const;

  virtual Real CalculateScore(const std::vector<uint>& start_resnum, 
                              const std::vector<uint>& num_residues,
                              const std::vector<uint>& chain_idx) const;

  virtual void CalculateScoreProfile(const std::vector<uint>& start_resnum,
                              const std::vector<uint>& num_residues, 
                              const std::vector<uint>& chain_idx,
                              std::vector<std::vector<Real> >& profile) const;

  void DoNormalize(bool do_it) { normalize_ = do_it; }

  void AttachEnvironment(BackboneScoreEnv& env);

private:

  inline uint SIdx(uint psipred_pred_idx, uint psipred_cfi_idx,
                   uint dssp_idx) const{ 
    return psipred_pred_idx * 10 * 8 +
           psipred_cfi_idx * 8 +
           dssp_idx;
  }

  void Score(const std::vector<uint>& start_indices,
             const std::vector<uint>& num_residues,
             std::vector<std::vector<Real> >& scores) const;

  // portable serialization (only stores data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {

    uint num_values = 3 * 10 * 8;
    if(ds.IsSource()){
      scores_ = new Real[num_values];
      memset(scores_, 0, num_values * sizeof(Real)); 
    }
    for(uint i = 0; i < num_values; ++i){
      core::ConvertBaseType<float>(ds, scores_[i]);
    }
  }

  Real* scores_;
  bool normalize_;

  //environment specific information, that will be set upon calling 
  //AttachEnvironment
  SSAgreementEnvListener* env_;
};

}}//ns

#endif
