// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/scoring/backbone_score_base.hh>


namespace promod3 { namespace scoring {

BackboneScoreEnv::BackboneScoreEnv(const ost::seq::SequenceList& seqres): 
                                   seqres_(seqres) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneScoreEnv::BackboneScoreEnv", 2);


  std::vector<size_t> chain_sizes;
  for(ost::seq::SequenceList::Iterator i = seqres_.begin(); 
      i != seqres_.end(); ++i){
    chain_sizes.push_back(i->GetLength());
  }
  idx_handler_ = loop::IdxHandler(chain_sizes);

  for (ost::seq::SequenceList::Iterator i = seqres_.begin(); 
       i != seqres_.end(); ++i) {
    uint seq_length = i->GetLength();
    for (uint j = 0; j < seq_length; ++j) {
      ost::conop::AminoAcid aa = ost::conop::OneLetterCodeToAminoAcid((*i)[j]);
      if (aa == ost::conop::XXX) {
        std::stringstream ss;
        ss << "Invalid one letter code '" << (*i)[j] << "' encountered in "
           << "seqres for BackboneLoopScorer! Can only handle standard amino "
           << "acids!";
        throw promod3::Error(ss.str());
      }      
      aa_seqres_.push_back(aa);
    }
  }

  env_set_.resize(idx_handler_.GetNumResidues(), 0); 
  n_pos_.resize(idx_handler_.GetNumResidues(), geom::Vec3());
  ca_pos_.resize(idx_handler_.GetNumResidues(), geom::Vec3());
  cb_pos_.resize(idx_handler_.GetNumResidues(), geom::Vec3());
  c_pos_.resize(idx_handler_.GetNumResidues(), geom::Vec3());
  o_pos_.resize(idx_handler_.GetNumResidues(), geom::Vec3());
  applied_pairwise_functions_.resize(idx_handler_.GetNumResidues());
  psipred_pred_.assign(idx_handler_.GetNumResidues(),'C');
  psipred_cfi_.assign(idx_handler_.GetNumResidues(), 0);
}

BackboneScoreEnv::~BackboneScoreEnv() {
  for(std::vector<BackboneScoreEnvListener*>::iterator i = listener_.begin();
      i != listener_.end(); ++i){
    delete (*i);
  }
}

BackboneScoreEnvPtr BackboneScoreEnv::Copy() const{

  BackboneScoreEnvPtr return_ptr(new BackboneScoreEnv(seqres_));

  // following internal variables are assigned at construction:
  // - seqres_
  // - idx_handler_
  // - aa_seqres_

  return_ptr->psipred_pred_ = psipred_pred_;
  return_ptr->psipred_cfi_ = psipred_cfi_;
  return_ptr->env_set_ = env_set_;
  return_ptr->n_pos_ = n_pos_;
  return_ptr->ca_pos_ = ca_pos_;
  return_ptr->cb_pos_ = cb_pos_;
  return_ptr->c_pos_ = c_pos_;
  return_ptr->o_pos_ = o_pos_;
  return_ptr->applied_pairwise_functions_ = applied_pairwise_functions_;
  return_ptr->pairwise_functions_ = pairwise_functions_;
  return_ptr->pairwise_function_types_ = pairwise_function_types_;

  return return_ptr;
}

void BackboneScoreEnv::SetInitialEnvironment(const ost::mol::EntityHandle& env) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneScoreEnv::SetInitialEnvironment", 2);


  // check for consistency in seqres and env by simply checking the consistency
  // of the the one letter codes at the according positions
  ost::mol::ChainHandleList chain_list = env.GetChainList();
  ost::mol::ResidueHandleList res_list;

  if (chain_list.size() < idx_handler_.GetNumChains()) {
    throw promod3::Error("Less chains than seqres sequences when setting the "
                         "BackboneScoreEnvironment env!");
  }

  for (uint i = 0; i < idx_handler_.GetNumChains(); ++i) {

    res_list = chain_list[i].GetResidueList();
    uint num;
    uint res_idx;

    for (ost::mol::ResidueHandleList::iterator j = res_list.begin(),
         e = res_list.end(); j != e; ++j) {

      num = j->GetNumber().GetNum();

      if (num == 0) {
        String err = "Observed residue number of 0 in residue ";
        err += j->GetQualifiedName();
        err += ". Per definition, the residue numbers must start from 1! ";
        err += "don't do anything...";
        throw promod3::Error(err);
      }

      if (num > idx_handler_.GetChainSize(i)) {
        String err = "ResNum of res " + j->GetQualifiedName();
        err += " exceeds size of seqres, the loop scorer has been initialized ";
        err += "with! Don't do anything...";
        throw promod3::Error(err);
      }

      res_idx = idx_handler_.ToIdx(num, i);

      char olc = j->GetOneLetterCode();

      if (aa_seqres_[res_idx] != ost::conop::OneLetterCodeToAminoAcid(olc)) {
        String err = "Sequence mismatch when settings loop scorers env! ";
        err += j->GetQualifiedName() + " should be a ";
        err += ost::conop::AminoAcidToResidueName(aa_seqres_[res_idx]);
        throw promod3::Error(err);
      }
    }
  }

  // it could be, that there has already been another structure set...
  // let's clear the environment and notify the listeners to clear their 
  // content at the according positions!

  std::vector<uint> indices_to_delete;
  for (uint i = 0; i < idx_handler_.GetNumResidues(); ++i) {
    if (env_set_[i]) {
      indices_to_delete.push_back(i);
      env_set_[i] = 0;
    }
  }
  for (std::vector<BackboneScoreEnvListener*>::iterator i = listener_.begin();
       i != listener_.end(); ++i) {
    (*i)->ClearEnvironment(indices_to_delete);  
  }
  

  // let's finally set the env data
  ost::mol::AtomHandle n, ca, c, o, cb;
  uint num, res_idx;

  for (uint i = 0; i < idx_handler_.GetNumChains(); ++i) {

    res_list = chain_list[i].GetResidueList();
    for (ost::mol::ResidueHandleList::iterator j = res_list.begin(),
         e = res_list.end(); j != e; ++j) {

      num = j->GetNumber().GetNum();
      res_idx = idx_handler_.ToIdx(num, i);

      n = j->FindAtom("N");
      ca = j->FindAtom("CA");
      c = j->FindAtom("C");
      o = j->FindAtom("O");
      cb = j->FindAtom("CB");

      if (!n.IsValid() || !ca.IsValid() || !c.IsValid() || !o.IsValid()) {
        String err = "At least backbone atoms (N, CA, C and O) must be present";
        err += " for all residues when setting the loop scorers environment!";
        err += (" Failed at: " + j->GetQualifiedName());
        throw promod3::Error(err);
      }

      n_pos_[res_idx] = n.GetPos();
      ca_pos_[res_idx] = ca.GetPos();
      c_pos_[res_idx] = c.GetPos();
      o_pos_[res_idx] = o.GetPos();

      if (!cb.IsValid()) promod3::core::ConstructCBetaPos(n_pos_[res_idx], 
                                                          ca_pos_[res_idx], 
                                                          c_pos_[res_idx], 
                                                          cb_pos_[res_idx]);
      else cb_pos_[res_idx] = cb.GetPos();
      env_set_[res_idx] = 1;
    }
  }

  // notify the listeners about the newly set stuff
  if (!listener_.empty()) {
    std::vector<uint> indices_to_set;
    for(uint i = 0; i < idx_handler_.GetNumResidues(); ++i){
      if(env_set_[i]) indices_to_set.push_back(i);
    }
    for (std::vector<BackboneScoreEnvListener*>::iterator i = listener_.begin();
         i != listener_.end(); ++i){
      (*i)->SetEnvironment(indices_to_set);  
    }
  }
}

void BackboneScoreEnv::SetEnvironment(const loop::BackboneList& bb_list,
                                      uint start_resnum, uint chain_idx) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneScoreEnv::SetEnvironment", 2);

  //let's figure out what information has already been set and what information
  //is new
  std::vector<uint> indices_to_set;
  std::vector<uint> indices_to_reset;

  size_t bb_list_size = bb_list.size();
  for(uint i = 0; i < bb_list_size; ++i){

    uint idx = idx_handler_.ToIdx(start_resnum + i, chain_idx);

    if(bb_list.GetAA(i) != aa_seqres_[idx]){
      String err = "Sequence mismatch when setting environment in ";
      err += "BackboneScoreEnv!";
      throw promod3::Error(err);
    }

    if(env_set_[idx]){
      indices_to_reset.push_back(idx);
    }
    else{
      indices_to_set.push_back(idx);
      env_set_[idx] = 1;
    }

    n_pos_[idx] = bb_list.GetN(i);
    ca_pos_[idx] = bb_list.GetCA(i);
    cb_pos_[idx] = bb_list.GetCB(i);
    c_pos_[idx] = bb_list.GetC(i);
    o_pos_[idx] = bb_list.GetO(i);
  }

  //and call all listeners
  for(std::vector<BackboneScoreEnvListener*>::iterator i = listener_.begin();
      i != listener_.end(); ++i){
    (*i)->SetEnvironment(indices_to_set);
    (*i)->ResetEnvironment(indices_to_reset);
  }  
}

void BackboneScoreEnv::ClearEnvironment(uint start_resnum, uint num_residues, 
                                        uint chain_idx) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneScoreEnv::ClearEnvironment", 2);

  // check/get start index
  uint start_idx = idx_handler_.ToIdx(start_resnum, chain_idx);
  // check size
  if (start_resnum-1 + num_residues > idx_handler_.GetChainSize(chain_idx)) {
    throw promod3::Error("Invalid End ResNum!");
  }

  // modify env_set_ vector and collect cleared ones
  std::vector<uint> indices_to_clear;
  indices_to_clear.reserve(num_residues);
  for (uint i = 0; i < num_residues; ++i) {
    if (env_set_[start_idx + i]) {
      env_set_[start_idx + i] = 0;
      indices_to_clear.push_back(start_idx + i);
    }
  }

  // and call all listeners
  for (std::vector<BackboneScoreEnvListener*>::iterator i = listener_.begin();
       i != listener_.end(); ++i) {
    (*i)->ClearEnvironment(indices_to_clear);
  }
}

void BackboneScoreEnv::SetPsipredPrediction(loop::PsipredPredictionPtr pp) {

  if(idx_handler_.GetNumChains() != 1){
    String err = "Cannot set single psipred prediction for BackboneScoreEnv "; 
    err += "being initialized with multiple chains!";
    throw promod3::Error(err);
  }
  if(pp->size() != idx_handler_.GetChainSize(0)){
    String err = "Size of psipred prediction must be consistent with ";
    err += "seqres you used to initialize the BackboneScoreEnv!";
    throw promod3::Error(err);
  }

  for(uint i = 0; i < idx_handler_.GetChainSize(0); ++i){
    psipred_pred_[i] = pp->GetPrediction(i);
    psipred_cfi_[i] = pp->GetConfidence(i);
  }
}

void BackboneScoreEnv::SetPsipredPrediction(std::vector<loop::PsipredPredictionPtr>& pp) {

  if(idx_handler_.GetNumChains() != pp.size()){
    String err = "Size of psipred predictions must be consistent with number ";
    err += "of chains in BackboneScoreEnv!";
    throw promod3::Error(err);
  }

  for(uint i = 0; i < pp.size(); ++i){
    if(idx_handler_.GetChainSize(i) != pp[i]->size()){
      String err = "Size of psipred predictions must be consistent with size ";
      err += "seqres sequences in BackboneScoreEnv!";
      throw promod3::Error(err);
    }
  }

  int actual_idx = 0;

  for(uint i = 0; i < idx_handler_.GetNumChains(); ++i){
    for(uint j = 0; j < idx_handler_.GetChainSize(i); ++j){
      psipred_pred_[actual_idx] = pp[i]->GetPrediction(j);
      psipred_cfi_[actual_idx] = pp[i]->GetConfidence(j);
      ++actual_idx;
    }
  }
}

uint BackboneScoreEnv::AddPairwiseFunction(PairwiseFunctionPtr f,
                                           PairwiseFunctionType ft) {
  pairwise_functions_.push_back(f);
  pairwise_function_types_.push_back(ft);
  return (pairwise_functions_.size() - 1);
}

void BackboneScoreEnv::ApplyPairwiseFunction(uint chain_idx_one, uint resnum_one,
                                             uint chain_idx_two, uint resnum_two,
                                             uint f_idx) {

  //only check, whether residues are exactly the same and whether the f_idx is
  //valid. The other input gets checked in the ToIdx function
  if(chain_idx_one == chain_idx_two){
    if(resnum_one == resnum_two){
      throw promod3::Error("Cannot add pairwise function for residue with itself!");
    }
  }

  if(f_idx >= pairwise_functions_.size()){
    throw promod3::Error("Invalid function idx when adding pairwise function!");
  } 

  uint idx_one = idx_handler_.ToIdx(resnum_one, chain_idx_one);
  uint idx_two = idx_handler_.ToIdx(resnum_two, chain_idx_two);

  PairwiseFunctionData data_one;
  data_one.f = pairwise_functions_[f_idx];
  data_one.ft = pairwise_function_types_[f_idx];
  data_one.other_idx = idx_two;

  PairwiseFunctionData data_two;
  data_two.f = pairwise_functions_[f_idx];
  data_two.ft = pairwise_function_types_[f_idx];
  data_two.other_idx = idx_one;

  applied_pairwise_functions_[idx_one].push_back(data_one);
  applied_pairwise_functions_[idx_two].push_back(data_two);
}

void BackboneScoreEnv::Stash(uint start_resnum, uint num_residues, 
                             uint chain_idx) {
  std::vector<uint> indices = idx_handler_.ToIdxVector(start_resnum,
                                                       num_residues,
                                                       chain_idx);
  this->Stash(indices);
}

void BackboneScoreEnv::Stash(const std::vector<uint>& start_resnum, 
                             const std::vector<uint>& num_residues,
                             const std::vector<uint>& chain_idx) {

  std::vector<uint> start_indices;
  std::vector<uint> lengths;

  std::vector<uint> indices = idx_handler_.ToIdxVector(start_resnum,
                                                       num_residues,
                                                       chain_idx,
                                                       start_indices,
                                                       lengths);
  this->Stash(indices);
}

void BackboneScoreEnv::Stash(const std::vector<uint>& indices) {

  if(stashed_indices_.size() > 100) {
    throw promod3::Error("You already performed a 100 stash events... "
                         "Cant do more....");
  }

  std::vector<uint> stash_indices;
  std::vector<int> stash_env_set;
  std::vector<geom::Vec3> stash_n_pos;
  std::vector<geom::Vec3> stash_ca_pos;
  std::vector<geom::Vec3> stash_cb_pos;
  std::vector<geom::Vec3> stash_c_pos;
  std::vector<geom::Vec3> stash_o_pos;

  uint n_indices = indices.size();
  stash_indices.reserve(n_indices);
  stash_env_set.reserve(n_indices);
  stash_n_pos.reserve(n_indices);
  stash_ca_pos.reserve(n_indices);
  stash_cb_pos.reserve(n_indices);
  stash_c_pos.reserve(n_indices);
  stash_o_pos.reserve(n_indices);

  for(std::vector<uint>::const_iterator i = indices.begin(); 
      i != indices.end(); ++i) {
    stash_indices.push_back(*i);
    stash_env_set.push_back(env_set_[*i]);
    stash_n_pos.push_back(n_pos_[*i]);
    stash_ca_pos.push_back(ca_pos_[*i]);
    stash_cb_pos.push_back(cb_pos_[*i]);
    stash_c_pos.push_back(c_pos_[*i]);
    stash_o_pos.push_back(o_pos_[*i]);
  }

  stashed_indices_.push(stash_indices);
  stashed_env_set_.push(stash_env_set);
  stashed_n_pos_.push(stash_n_pos);
  stashed_ca_pos_.push(stash_ca_pos);
  stashed_cb_pos_.push(stash_cb_pos);
  stashed_c_pos_.push(stash_c_pos);
  stashed_o_pos_.push(stash_o_pos);
}

void BackboneScoreEnv::Pop() {

  if(stashed_indices_.empty()) {
    throw promod3::Error("Nothing stashed! Cannot pop!");
  }

  std::vector<uint> indices_to_set;
  std::vector<uint> indices_to_reset;
  std::vector<uint> indices_to_clear;
  
  const std::vector<uint>& stash_indices = stashed_indices_.top();
  const std::vector<int>& stash_env_set = stashed_env_set_.top();
  const std::vector<geom::Vec3>& stash_n_pos = stashed_n_pos_.top();
  const std::vector<geom::Vec3>& stash_ca_pos = stashed_ca_pos_.top();
  const std::vector<geom::Vec3>& stash_cb_pos = stashed_cb_pos_.top();
  const std::vector<geom::Vec3>& stash_c_pos = stashed_c_pos_.top();
  const std::vector<geom::Vec3>& stash_o_pos = stashed_o_pos_.top();

  for(uint i = 0; i < stash_indices.size(); ++i) {
    uint env_idx = stash_indices[i];
    n_pos_[env_idx] = stash_n_pos[i];
    ca_pos_[env_idx] = stash_ca_pos[i];
    cb_pos_[env_idx] = stash_cb_pos[i];
    c_pos_[env_idx] = stash_c_pos[i];
    o_pos_[env_idx] = stash_o_pos[i];

    if(env_set_[env_idx] == 1 && stash_env_set[i] == 1) {
      indices_to_reset.push_back(env_idx);
    } else if(env_set_[env_idx] == 1 && stash_env_set[i] == 0) {
      indices_to_clear.push_back(env_idx);
    } else if(env_set_[env_idx] == 0 && stash_env_set[i] == 1) {
      indices_to_set.push_back(env_idx);
    }

    env_set_[env_idx] = stash_env_set[i]; 
  }

  //and call all listeners
  for(std::vector<BackboneScoreEnvListener*>::iterator i = listener_.begin();
      i != listener_.end(); ++i){
    (*i)->SetEnvironment(indices_to_set);
    (*i)->ResetEnvironment(indices_to_reset);
    (*i)->ClearEnvironment(indices_to_clear);
  } 

  stashed_indices_.pop();
  stashed_env_set_.pop(); 
  stashed_n_pos_.pop();
  stashed_ca_pos_.pop();
  stashed_cb_pos_.pop();
  stashed_c_pos_.pop();
  stashed_o_pos_.pop();
}

BackboneScoreEnvListener* BackboneScoreEnv::GetListener(const String& id) const {
  for (uint i = 0; i < listener_.size(); ++i) {
    if (listener_[i]->WhoAmI() == id) return listener_[i];
  }
  return NULL;
}

void BackboneScoreEnv::AttachListener(BackboneScoreEnvListener* listener) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneScoreEnv::AttachListener", 2);

  listener->Init(*this);
  // after being initialized, the listener also has to know, which positions
  // are already set...
  std::vector<uint> indices_to_set;
  for (uint i = 0; i < env_set_.size(); ++i) {
    if (env_set_[i]) indices_to_set.push_back(i);
  }
  listener->SetEnvironment(indices_to_set);
  listener_.push_back(listener);
}

}} //ns
