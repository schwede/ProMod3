#!/usr/bin/env python
import sys

if len(sys.argv) < 4:
  print("USAGE: python scripts/bump-version.py PM3_VERSION OST_VERSION OST_DOCKER_IMAGE_TAG")
  print("-> *_VERSION format is MAJOR.MINOR.PATCH (e.g. 1.9.1)")
  print("-> assumption is that git tags will exist for those *_VERSION")
  print("-> OST_DOCKER_IMAGE_TAG refers to the image in the OpenStructure")
  print("   Docker registry from which the ProMod3 image will bootstrap")
  sys.exit(1)

# split up version number
version_string = sys.argv[1]
version = version_string.split('.')
major, minor, patch = (int(version[0]), int(version[1]), int(version[2]))
ost_version_string = sys.argv[2]
ost_docker_image_tag = sys.argv[3]

# fix CMakeLists
lines = open("CMakeLists.txt").readlines()
for i, line in enumerate(lines):
  if line.startswith("set(PROMOD3_VERSION_MAJOR"):
    lines[i] = "set(PROMOD3_VERSION_MAJOR %d)\n" % major
  elif line.startswith("set(PROMOD3_VERSION_MINOR"):
    lines[i] = "set(PROMOD3_VERSION_MINOR %d)\n" % minor
  elif line.startswith("set(PROMOD3_VERSION_PATCH"):
    lines[i] = "set(PROMOD3_VERSION_PATCH %d)\n" % patch
  elif line.startswith("find_package(OPENSTRUCTURE "):
    lines[i] = "find_package(OPENSTRUCTURE %s REQUIRED\n" % ost_version_string
open("CMakeLists.txt", "w").writelines(lines)

# fix CHANGELOG
lines = open("CHANGELOG").readlines()
for i, line in enumerate(lines):
  if line.startswith("Release") and "X" in line.upper():
    lines[i] = "Release %s\n" % version_string
open("CHANGELOG", "w").writelines(lines)

# fix Docker recipe
lines = open("container/Dockerfile").readlines()
for i, line in enumerate(lines):
  if line.startswith("ARG OPENSTRUCTURE_IMAGE_TAG"):
    lines[i] = 'ARG OPENSTRUCTURE_IMAGE_TAG="%s"\n' % ost_docker_image_tag
  elif line.startswith("ARG PROMOD_VERSION"):
    lines[i] = 'ARG PROMOD_VERSION="%s"\n' % version_string
open("container/Dockerfile", "w").writelines(lines)

# fix Singularity recipe
vfile = "container/Singularity"
lines = open(vfile).readlines()
for i, line in enumerate(lines):
  if line.startswith("From: registry.scicore.unibas.ch/schwede/promod3:"):
    lines[i] = 'From: registry.scicore.unibas.ch/schwede/promod3:'+\
      '%s-OST%s\n' % (version_string, ost_docker_image_tag)
    break
open(vfile, "w").writelines(lines)

# fix doc config
lines = open("doc/conf.py.in").readlines()
for i, line in enumerate(lines):
  if line.startswith(".. |ost_version| replace::"):
    lines[i] = '.. |ost_version| replace:: %s\n' % ost_version_string
open("doc/conf.py.in", "w").writelines(lines)
