#-------------------------------------------------------------------------------
# Authors: Marco Biasini, Stefan Bienert
#
# This file contains a bunch of useful macros to facilitate the build-system
# configuration for the modules.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# map macro
#
# this function emulates a map/dict data type
#-------------------------------------------------------------------------------

function(map COMMAND MAPNAME)
  set (_KEYS ${MAPNAME}_MAP_KEYS )
  set (_VALUES ${MAPNAME}_MAP_VALUES)
  if(${COMMAND} STREQUAL SET)
    list(REMOVE_AT ARGN 0)
    list(FIND ${_KEYS} ${ARGV2} _MAP_INDEX)
    if(_MAP_INDEX EQUAL -1)
      list(APPEND ${_KEYS} ${ARGV2})
      set(${_KEYS} ${${_KEYS}} PARENT_SCOPE)
      set(${_VALUES}_${ARGV2}  ${ARGN} PARENT_SCOPE)
    else()
      set(${_VALUES}_${ARGV2}  ${ARGN} PARENT_SCOPE)
    endif()
  elseif(${COMMAND} STREQUAL GET)
    list(FIND ${_KEYS} ${ARGV2} _MAP_INDEX)
    if(_MAP_INDEX EQUAL -1)
      MESSAGE(FATAL_ERROR "Unknown key: " ${ARGV2})
    endif()
    set(${ARGV3} ${${_VALUES}_${ARGV2}} PARENT_SCOPE)
  elseif(${COMMAND} STREQUAL KEYS)
    set(${ARGV2} ${${_KEYS}} PARENT_SCOPE)
  elseif(${COMMAND} STREQUAL CREATE)
    set(${_KEYS}  "" PARENT_SCOPE)
  elseif(${COMMAND} STREQUAL LENGTH)
    list(LENGTH ${_KEYS} _L)
    set(${ARGV2} ${_L} PARENT_SCOPE)
  else()
    MESSAGE(FATAL_ERROR "Unknown map command:" ${COMMAND})
  endif()
endfunction()


#-------------------------------------------------------------------------------
# check_architecture
#
# detect architecture based on void pointer size. the output is stored in the
# 3 variables OS_32_BITS, OS_64_BITS and CMAKE_NATIVE_ARCH. The former two are
# set to 0 and 1 accordingly and CMAKE_NATIVE_ARCH is set to the 32 or 64 bit.
#-------------------------------------------------------------------------------
macro(check_architecture)
  include(CheckTypeSize)
  check_type_size(void*  SIZEOF_VOID_PTR)
  if(${SIZEOF_VOID_PTR} MATCHES "^8$")
    set(OS_32_BITS 0)
    set(OS_64_BITS 1)
    set(CMAKE_NATIVE_ARCH 64)
  else()
    set(OS_32_BITS 1)
    set(OS_64_BITS 0)
    set(CMAKE_NATIVE_ARCH 32)
  endif()
endmacro(check_architecture)


#-------------------------------------------------------------------------------
# get_python_path(VAR):
# 
# Set variable VAR (in parent scope) to an updated PYTHONPATH env. variable 
# which includes OST and PM3 paths. This can then be used to call python scripts
# with commands such as: sh -c "PYTHONPATH=${VAR} ${Python_EXECUTABLE} ..."
#-------------------------------------------------------------------------------
macro(get_python_path VAR)
  set(${VAR} $ENV{PYTHONPATH})
  if(${VAR})
    set(${VAR} ":${${VAR}}")
  endif(${VAR})
  set(${VAR} "${LIB_STAGE_PATH}/${PYTHON_MODULE_PATH}${${VAR}}")
  set(${VAR} "${OST_PYMOD_PATH}:${${VAR}}")
endmacro(get_python_path)


#-------------------------------------------------------------------------------
# this macro has been adapted from
# http://www.cmake.org/Wiki/CMakeMacroParseArguments
#-------------------------------------------------------------------------------
macro(parse_argument_list PREFIX ARG_NAMES OPT_NAMES)
  set(_DEFAULT_ARGS)
  # reset variables
  foreach(${_ARG_NAME} ${ARG_NAMES})
    set(PREFIX_${_ARG_NAME})
  endforeach()
  foreach(_OPT_NAME ${OPT_NAMES})
    set(PREFIX_${_OPT_NAME} FALSE)
  endforeach()
  set(_CURR_ARG_NAME DEF_ARGS)
  set(_CURR_ARG_LIST)
  # loop over parameter list and split by ARG_NAMES
  foreach(_ARG ${ARGN})
    set(_LARG_NAMES ${ARG_NAMES})  
    list(FIND _LARG_NAMES ${_ARG} _IS_ARG_NAME)
    if(_IS_ARG_NAME GREATER -1)
      set(${PREFIX}_${_CURR_ARG_NAME} ${_CURR_ARG_LIST})
      set(_CURR_ARG_NAME "${_ARG}")
      set(_CURR_ARG_LIST)
    else()
    set(_LOPT_NAMES ${OPT_NAMES})  
      list(FIND _LOPT_NAMES ${_ARG} _IS_OPT_NAME)
      if(_IS_OPT_NAME GREATER -1)
        set(${PREFIX}_${_ARG} TRUE)
      else()
        list(APPEND _CURR_ARG_LIST "${_ARG}")
      endif()
    endif()
  endforeach()
  set(${PREFIX}_${_CURR_ARG_NAME} ${_CURR_ARG_LIST})
endmacro(parse_argument_list)

#-------------------------------------------------------------------------------
# copy_if_different
#
# copies file from source directory to destination directory, but only if its
# content changed.
#-------------------------------------------------------------------------------
macro(copy_if_different FROM_DIR TO_DIR FILES TARGETS TARGET)
  foreach(SRC ${FILES})
      set(SRCFILE ${SRC})
      if("${FROM_DIR}" STREQUAL "" OR "${FROM_DIR}" STREQUAL "./")
          set(FROM ${SRC})
      else()
          set(FROM ${FROM_DIR}/${SRC})
      endif()
      if("${TO_DIR}" STREQUAL "")
          set(TO ${SRCFILE})
      else()
          get_filename_component(TOFILE ${SRC} NAME)      
          set(TO ${TO_DIR}/${TOFILE})
      endif()
      file(MAKE_DIRECTORY  ${TO_DIR})
      add_custom_command(TARGET "${TARGET}" PRE_BUILD
          DEPENDS ${FROM}
          COMMAND ${CMAKE_COMMAND} -E copy_if_different ${FROM} ${TO})
  endforeach()
endmacro(copy_if_different)

#-------------------------------------------------------------------------------
# parse_file_list
#
# this macro splits a list of files with IN_DIR statements and fills them into
# a map where the key is the directory name
#-------------------------------------------------------------------------------
macro(parse_file_list FILELIST FILEMAP)
  set(_EXPECT_IN_DIR FALSE)
  map(CREATE ${FILEMAP})
  set(_CURRENT_LIST)
  foreach(_ITEM ${FILELIST})
    if(_ITEM STREQUAL "IN_DIR")
      set(_EXPECT_IN_DIR TRUE)
    else()
      if(_EXPECT_IN_DIR)
        set(_EXPECT_IN_DIR FALSE)
        map(SET ${FILEMAP} ${_ITEM} ${_CURRENT_LIST})
        set(_CURRENT_LIST)
      else()
        list(APPEND _CURRENT_LIST "${_ITEM}")
      endif()
    endif()
  endforeach()
  if(_CURRENT_LIST)
    map(SET ${FILEMAP} "." ${_CURRENT_LIST})
  endif()
endmacro(parse_file_list)


#-------------------------------------------------------------------------------
# Synopsis:
#   module(NAME name SOURCES source1 source2 HEADERS header1 header2 
#          [IN_DIR dir] [header3 header4 [IN_DIR dir]] [DEPENDS_ON dep1 dep2]
#          [HEADER_OUTPUT_DIR dir]
#          [LINK link_cmds])
# Description:
#   Define a ProMod3 module.
#-------------------------------------------------------------------------------
macro(module)
  #-----------------------------------------------------------------------------
  # deal with arguments
  #-----------------------------------------------------------------------------
  set(_ARGS "NAME;SOURCES;HEADERS;DEPENDS_ON;LINK;HEADER_OUTPUT_DIR;PREFIX")
  set(_ARG_PREFIX promod3)  
  parse_argument_list(_ARG "${_ARGS}" "NO_STATIC" ${ARGN})  
  if(NOT _ARG_NAME)
    message(FATAL_ERROR 
            "invalid use of module(): a module name must be provided")
  endif()

  if(ENABLE_STATIC AND _ARG_NO_STATIC)
    return()
  endif()
  if(_ARG_HEADER_OUTPUT_DIR)
    set(_HEADER_OUTPUT_DIR ${_ARG_HEADER_OUTPUT_DIR})
  else()
    set(_HEADER_OUTPUT_DIR "${_ARG_PREFIX}/${_ARG_NAME}")
  endif()
  set(_LIB_NAME ${_ARG_PREFIX}_${_ARG_NAME})
  string(TOUPPER ${_LIB_NAME} _UPPER_LIB_NAME)  
  #-----------------------------------------------------------------------------
  # create library  
  #-----------------------------------------------------------------------------
  file(MAKE_DIRECTORY ${LIB_STAGE_PATH})
  file(MAKE_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
  file(MAKE_DIRECTORY ${LIBEXEC_STAGE_PATH})
  file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/tests")
  if(NOT TARGET create_stage)
    add_custom_target(create_stage COMMAND ${CMAKE_COMMAND} -E make_directory
                                           ${LIB_STAGE_PATH}
                                   COMMAND ${CMAKE_COMMAND} -E make_directory
                                           ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
                                   COMMAND ${CMAKE_COMMAND} -E make_directory
                                           ${LIBEXEC_STAGE_PATH}
                                   COMMAND ${CMAKE_COMMAND} -E make_directory
                                           "${CMAKE_BINARY_DIR}/tests")
  endif()
  if(_ARG_SOURCES)
    # when there is at least one source file, we build a library
    set(_ABS_SOURCE_NAMES)
    foreach(_SOURCE ${_ARG_SOURCES})
      if(IS_ABSOLUTE ${_SOURCE})
        list(APPEND _ABS_SOURCE_NAMES "${_SOURCE}")
      else()
        list(APPEND _ABS_SOURCE_NAMES "${CMAKE_CURRENT_SOURCE_DIR}/${_SOURCE}")
      endif()
    endforeach()
    if(ENABLE_STATIC AND NOT _ARG_NO_STATIC)
      add_library(${_LIB_NAME} STATIC ${_ABS_SOURCE_NAMES})
    else()
      add_library(${_LIB_NAME} SHARED ${_ABS_SOURCE_NAMES})
    endif()
    set_target_properties(${_LIB_NAME} 
                          PROPERTIES OUTPUT_NAME ${_LIB_NAME}
                                     PROJECT_LABEL ${_ARG_NAME}
                                     EchoString   ${_ARG_NAME}
                                     MODULE_DEPS "${_ARG_DEPENDS_ON}")
    get_target_property(_DEFS ${_LIB_NAME} COMPILE_DEFINITIONS)
    add_dependencies(${_LIB_NAME} create_stage)
    set_target_properties(${_LIB_NAME} PROPERTIES
                          COMPILE_DEFINITIONS PROMOD3_MODULE_${_UPPER_LIB_NAME})
    set_target_properties(${_LIB_NAME} PROPERTIES
                          VERSION ${PROMOD3_VERSION_STRING}
                          SOVERSION
                              ${PROMOD3_VERSION_MAJOR}.${PROMOD3_VERSION_MINOR})
    set_target_properties(${_LIB_NAME} PROPERTIES
                          LIBRARY_OUTPUT_DIRECTORY ${LIB_STAGE_PATH}
                          ARCHIVE_OUTPUT_DIRECTORY ${LIB_STAGE_PATH}
                          RUNTIME_OUTPUT_DIRECTORY ${LIB_STAGE_PATH})
    if(APPLE)
      set_target_properties(${_LIB_NAME} PROPERTIES
                            LINK_FLAGS "-Wl,-rpath,@loader_path"
                            INSTALL_NAME_DIR "@rpath")
    endif()
    if(ENABLE_STATIC)
      install(TARGETS ${_LIB_NAME} ARCHIVE DESTINATION "${LIB_DIR}")
    else()
      install(TARGETS ${_LIB_NAME} LIBRARY DESTINATION "${LIB_DIR}")
    endif()
    if(_ARG_LINK)
      target_link_libraries(${_LIB_NAME} ${_ARG_LINK})
    endif()
    foreach(_DEPENDENCY ${_ARG_DEPENDS_ON})
      get_property(target_type TARGET ${_DEPENDENCY} PROPERTY TYPE)
      if(target_type MATCHES
                      "STATIC_LIBRARY|MODULE_LIBRARY|SHARED_LIBRARY|EXECUTABLE")
        target_link_libraries(${_LIB_NAME} ${_DEPENDENCY})
      else()
        add_dependencies(${_LIB_NAME} ${_DEPENDENCY})
      endif()
    endforeach()
    if(ENABLE_STATIC)
      target_link_libraries(${_LIB_NAME} ${STATIC_LIBRARIES})
    endif()
  else()
    add_custom_target("${_LIB_NAME}" ALL)
    set_target_properties("${_LIB_NAME}" PROPERTIES HEADER_ONLY 1 
                          MODULE_DEPS "${_ARG_DEPENDS_ON}")
  endif()
  add_dependencies("${_LIB_NAME}" config_header)
  #-----------------------------------------------------------------------------
  # stage headers  
  #-----------------------------------------------------------------------------
  if(_ARG_HEADERS)
    stage_and_install_headers("${_ARG_HEADERS}" "${_HEADER_OUTPUT_DIR}"
                              "${_LIB_NAME}")
  endif()
endmacro(module)

#-------------------------------------------------------------------------------
# macro stage_and_install_headers
#-------------------------------------------------------------------------------
macro(stage_and_install_headers HEADERLIST HEADER_OUTPUT_DIR TARGET)
  add_custom_target("${TARGET}_headers" COMMENT "")
  add_dependencies("${TARGET}" "${TARGET}_headers")
  add_dependencies("${TARGET}_headers" create_stage)
  parse_file_list("${HEADERLIST}" _HEADER_MAP)
  map(KEYS _HEADER_MAP _HEADER_MAP_KEYS)
  foreach(_DIR ${_HEADER_MAP_KEYS})
    map(GET _HEADER_MAP ${_DIR} _HEADERS)
    set(_HDR_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/${_DIR}")
    set(_ABS_HEADER_NAMES)
    foreach(_HDR ${_HEADERS})
      list(APPEND _ABS_HEADER_NAMES ${_HDR_SOURCE_DIR}/${_HDR})
    endforeach()
    set(_HDR_STAGE_DIR "${HEADER_OUTPUT_DIR}/${_DIR}")
    set(_FULL_HEADER_DIR "${HEADER_STAGE_PATH}/${_HDR_STAGE_DIR}")
    copy_if_different("" "${_FULL_HEADER_DIR}" "${_ABS_HEADER_NAMES}" ""
                      "${TARGET}_headers")
    install(FILES ${_ABS_HEADER_NAMES} DESTINATION "include/${_HDR_STAGE_DIR}")
  endforeach()
endmacro(stage_and_install_headers)


#-------------------------------------------------------------------------------
# Synopsis
#   executable(NAME exe_name SOURCES source1 source2 LINK link1 link2)
#
# Description:
#  Compile, link and stage a C++ executable
#-------------------------------------------------------------------------------
macro(executable)
  parse_argument_list(_ARG 
                      "NAME;SOURCES;LINK;DEPENDS_ON" "NO_RPATH;STATIC" ${ARGN})
  if(NOT _ARG_NAME)
    message(FATAL_ERROR "invalid use of executable(): a name must be provided")
  endif()
  add_executable(${_ARG_NAME} ${_ARG_SOURCES})
  if(APPLE AND NOT _ARG_NO_RPATH AND NOT ENABLE_STATIC)
    set_target_properties(${_ARG_NAME} PROPERTIES
                          LINK_FLAGS "-Wl,-rpath,@loader_path/../lib/")
  endif()
  if(_ARG_LINK)
    target_link_libraries(${_ARG_NAME} ${_ARG_LINK})
  endif()
  foreach(_DEP ${_ARG_DEPENDS_ON})
    target_link_libraries(${_ARG_NAME} ${_DEP})
  endforeach()
  if(ENABLE_STATIC AND _ARG_STATIC)
    target_link_libraries(${_ARG_NAME} ${STATIC_LIBRARIES})
    if(UNIX AND NOT APPLE)
      if(PROMOD3_GCC_45)
        set_target_properties(${_ARG_NAME}
                              PROPERTIES LINK_SEARCH_END_STATIC TRUE  
                              LINK_FLAGS
                            "-static-libgcc -static-libstdc++ -static -pthread")
      else()
        set_target_properties(${_ARG_NAME}
                              PROPERTIES LINK_SEARCH_END_STATIC TRUE  
                              LINK_FLAGS "-static-libgcc -static -pthread")
      endif()        
    endif()
  endif()
  install(TARGETS ${_ARG_NAME} DESTINATION bin)
endmacro(executable)


#-------------------------------------------------------------------------------
# Synopsis
#   executable_libexec(NAME exe_name SOURCES source1 source2 LINK link1 link2)
#
# Description:
#  Compile, link and stage a C++ executable into the libexec directory
#-------------------------------------------------------------------------------
macro(executable_libexec)
  parse_argument_list(_ARG 
                      "NAME;SOURCES;LINK;DEPENDS_ON" "NO_RPATH;STATIC" ${ARGN})
  if(NOT _ARG_NAME)
    message(FATAL_ERROR "invalid use of executable(): a name must be provided")
  endif()   
  add_executable(${_ARG_NAME} ${_ARG_SOURCES})
  set_target_properties(${_ARG_NAME}
                        PROPERTIES RUNTIME_OUTPUT_DIRECTORY
                       "${LIBEXEC_STAGE_PATH}")  
  set_target_properties(${_ARG_NAME}
                        PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELEASE
                       "${LIBEXEC_STAGE_PATH}")  
  set_target_properties(${_ARG_NAME}
                        PROPERTIES RUNTIME_OUTPUT_DIRECTORY_DEBUG
                       "${LIBEXEC_STAGE_PATH}")  
  if(NOT _ARG_NO_RPATH AND NOT _ARG_STATIC)
    if(APPLE)
      set_target_properties(${_ARG_NAME} PROPERTIES
                            LINK_FLAGS "-Wl,-rpath,@loader_path/../../lib")
    elseif(UNIX)
      set_target_properties(${_ARG_NAME} PROPERTIES INSTALL_RPATH
                                                     "$ORIGIN/../../${LIB_DIR}")
    endif(APPLE)
  endif(NOT _ARG_NO_RPATH AND NOT _ARG_STATIC)
  if(_ARG_LINK)
    target_link_libraries(${_ARG_NAME} ${_ARG_LINK})
  endif()
  if(ENABLE_STATIC AND _ARG_STATIC)
    target_link_libraries(${_ARG_NAME} ${STATIC_LIBRARIES})
    set_target_properties(${_ARG_NAME}
                          PROPERTIES LINK_SEARCH_END_STATIC TRUE)  

  endif()
  foreach(_DEP ${_ARG_DEPENDS_ON})
    target_link_libraries(${_ARG_NAME} ${_DEP})
  endforeach()
  install(TARGETS ${_ARG_NAME} DESTINATION ${LIBEXEC_PATH})
endmacro(executable_libexec)

#-------------------------------------------------------------------------------
# Synopsis:
#   substitute(IN_FILE in_file OUT_FILE out_file DICT a=b c=d)
#
#-------------------------------------------------------------------------------
macro(substitute)
 parse_argument_list(_ARG 
                     "IN_FILE;OUT_FILE;DICT" "" ${ARGN})
  if(NOT _ARG_IN_FILE)
    message(FATAL_ERROR "invalid use of substitute(): no IN_FILE given")
  endif()
  if(NOT _ARG_OUT_FILE)
    message(FATAL_ERROR "invalid use of substitute(): no OUT_FILE given")
  endif()
  set(_SUBST_DICT -DINPUT_FILE=${_ARG_IN_FILE} -DOUT_FILE=${_ARG_OUT_FILE})
  foreach(_SUBST ${_ARG_DICT})
    list(APPEND _SUBST_DICT -D${_SUBST})
  endforeach()
  add_custom_target(subst_${_ARG_OUT_FILE} ALL COMMAND 
                    ${CMAKE_COMMAND} ${_SUBST_DICT}
                    -P ${CMAKE_SOURCE_DIR}/cmake_support/substitute.cmake)
endmacro(substitute)

#-------------------------------------------------------------------------------
# Synopsis:
#   script(NAME script_name INPUT input_name SUBSTITUTE key=val key=val
#          [OUTPUT_DIR dir] [TARGET target])
#-------------------------------------------------------------------------------
macro(script)
  set(_ARG_OUTPUT_DIR bin)
  parse_argument_list(_ARG 
                      "NAME;INPUT;SUBSTITUTE;TARGET;OUTPUT_DIR" "" ${ARGN})
  if(NOT _ARG_NAME)
    message(FATAL_ERROR "invalid use of executable(): a name must be provided")
  endif()
  set(_INPUT ${_ARG_NAME})
  if(_ARG_INPUT)
    set(_INPUT ${_ARG_INPUT})
  endif()
  if(_ARG_SUBSTITUTE)
    if(NOT _ARG_INPUT)
      message(FATAL_ERROR "script() can only substitute when INPUT is present.")
    endif()

    substitute(IN_FILE "${CMAKE_CURRENT_SOURCE_DIR}/${_INPUT}"
               OUT_FILE ${_ARG_NAME}
               DICT ${_ARG_SUBSTITUTE})
  endif()
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${_ARG_NAME}
          DESTINATION ${_ARG_OUTPUT_DIR}
          PERMISSIONS WORLD_EXECUTE GROUP_EXECUTE OWNER_EXECUTE 
                      WORLD_READ GROUP_READ OWNER_READ)
  copy_if_different("./" "${STAGE_DIR}/${_ARG_OUTPUT_DIR}" 
                    "${_ARG_NAME}" "TARGETS" ${_ARG_TARGET})
  add_dependencies(${_ARG_TARGET} subst_${_ARG_NAME})
endmacro(script)

#-------------------------------------------------------------------------------
# Synopsis:
#   pymod(NAME name CPP source1 source2 PY source source2 [IN_DIR dir] 
#         source3 source4 [IN_DIR dir] [TRANSLATE dict END_TRANSLATE]
#         [OUTPUT_DIR dir] [DEPENDS_ON dep1 dep2] [NEED_CONFIG_HEADER])
# 
# Description:
#   Define a python module consisting of C++ type wrappers and/or code written
#   in Python.
# 
# OUTPUT_DIR defines, where in the Python tree the files will be placed
# NEED_CONFIG_HEADER makes the module depending on the config_header target
# DEPENDS_ON defines extra target dependencies
# TRANSLATE If a Python source file needs to be translated by Cmake, provide the
#           dictionary after putting TRANSLATE, assumes a <FILE>.py.in for
#           <FILE>.py
# END_TRANSLATE marks the end of the translation dictionary, needed since doing
#               'a list inside a list' in CMake is awkward
#-------------------------------------------------------------------------------
macro(pymod)
  #-----------------------------------------------------------------------------
  # deal with arguments
  #-----------------------------------------------------------------------------
  set(_ARG_PREFIX promod3)
  parse_argument_list(_ARG 
                     "NAME;CPP;PY;OUTPUT_DIR;DEPENDS_ON"
                     "NEED_CONFIG_HEADER" ${ARGN})
  if(NOT _ARG_NAME)
    message(FATAL_ERROR "invalid use of pymod(): a name must be provided")
  endif()
  if(ENABLE_STATIC)
    return()
  endif()
  if(_ARG_OUTPUT_DIR)
    set(PYMOD_DIR "${PYTHON_MODULE_PATH}/${_ARG_OUTPUT_DIR}")
  else()
    set(PYMOD_DIR
        "${PYTHON_MODULE_PATH}/${_ARG_PREFIX}/${_ARG_NAME}")
  endif()
  set(PYMOD_STAGE_DIR "${LIB_STAGE_PATH}/${PYMOD_DIR}")
  file(MAKE_DIRECTORY ${PYMOD_STAGE_DIR})
  #-----------------------------------------------------------------------------
  # compile and link C++ wrappers
  #-----------------------------------------------------------------------------
  if(_ARG_CPP)
    add_library("_${_ARG_NAME}" MODULE ${_ARG_CPP})
    set_target_properties("_${_ARG_NAME}"
                          PROPERTIES ECHO_STRING
                          "Building Python Module ${_ARG_NAME}")
    set(_PARENT_NAME "${_ARG_PREFIX}_${_ARG_NAME}")
    get_target_property(_CUSTOM_CHECK "${_PARENT_NAME}" HEADER_ONLY)
    if(NOT _CUSTOM_CHECK)
      set(_PARENT_LIB_NAME "${_PARENT_NAME}")
    endif()
    target_link_libraries("_${_ARG_NAME}" ${_PARENT_LIB_NAME} 
                          ${Python_LIBRARIES} ${BOOST_PYTHON_LIBRARIES})
    set_target_properties("_${_ARG_NAME}"
                         PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PYMOD_STAGE_DIR})
    if(NOT ENABLE_STATIC)
      if(_USE_RPATH)
        string(REGEX REPLACE "/[^/]*" "/.." inv_pymod_path "/${PYMOD_DIR}")
        set_target_properties("_${_ARG_NAME}"
                           PROPERTIES INSTALL_RPATH "$ORIGIN${inv_pymod_path}/")
      else()
        set_target_properties("_${_ARG_NAME}"
                              PROPERTIES INSTALL_RPATH "")
      endif()
    endif()
    if(APPLE)
      file(RELATIVE_PATH _REL_PATH "${PYMOD_STAGE_DIR}" "${LIB_STAGE_PATH}")
      set_target_properties("_${_ARG_NAME}" PROPERTIES
                            LINK_FLAGS "-Wl,-rpath,@loader_path/${_REL_PATH}"
                            INSTALL_NAME_DIR "@rpath")
    endif()                          
    if(NOT WIN32)
      set_target_properties("_${_ARG_NAME}"
                          PROPERTIES PREFIX "")
    else()
      set_target_properties("_${_ARG_NAME}"
                          PROPERTIES PREFIX "../")

      set_target_properties("_${_ARG_NAME}"
                          PROPERTIES SUFFIX ".pyd")
    endif()
    install(TARGETS "_${_ARG_NAME}" LIBRARY DESTINATION
            "${LIB_DIR}/${PYMOD_DIR}")
  else()
    add_custom_target("_${_ARG_NAME}" ALL)
  endif()
  add_to_pymod_list(MOD "_${_ARG_NAME}")
  #-----------------------------------------------------------------------------
  # compile python files
  #-----------------------------------------------------------------------------
  if(_ARG_PY)
    set(_PY_FILES)
    set(_EXPECT_IN_DIR FALSE)
    set(_TRANSLATE FALSE)
    foreach(_PY_FILE ${_ARG_PY})
      if(_PY_FILE STREQUAL "IN_DIR")
        set(_EXPECT_IN_DIR TRUE)
      elseif(_PY_FILE STREQUAL "TRANSLATE")
        set(_TRANSLATE TRUE)
        set(_trans_py_source)
      elseif(_PY_FILE STREQUAL "END_TRANSLATE")
        add_custom_target("${_ARG_NAME}_pymod" ALL)
        # translate to build dir
        file(TO_NATIVE_PATH "${PYMOD_STAGE_DIR}/${_trans_py_source}"
          _trans_py_file_to)
        file(TO_NATIVE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${_trans_py_source}.in"
          _trans_py_file_from)
        set(_py_subst_dict -DINPUT_FILE=${_trans_py_file_from})
        list(APPEND _py_subst_dict -DOUT_FILE=${_trans_py_file_to})
        foreach(_subst ${_PY_FILES})
          list(APPEND _py_subst_dict -D${_subst})
        endforeach(_subst ${_PY_FILES})
        add_custom_command(OUTPUT ${_trans_py_file_to}
          MAIN_DEPENDENCY "${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt"
          DEPENDS "${_trans_py_file_from}"
          COMMAND ${CMAKE_COMMAND} ${_py_subst_dict}
          -P ${CMAKE_SOURCE_DIR}/cmake_support/substitute.cmake)
        add_custom_target("${_ARG_NAME}_${_trans_py_source}_pymod" ALL DEPENDS ${_trans_py_file_to})
        add_dependencies("${_ARG_NAME}_pymod" "${_ARG_NAME}_${_trans_py_source}_pymod")
        add_dependencies("_${_ARG_NAME}" "${_ARG_NAME}_pymod")
        include_directories(${Python_INCLUDE_DIRS})
        install(FILES ${_trans_py_file_to} DESTINATION "${LIB_DIR}/${PYMOD_DIR}")
        if(NOT DISABLE_DOCUMENTATION)
          add_doc_dependency(NAME ${_ARG_NAME} DEP "${_ARG_NAME}_${_trans_py_source}_pymod")
          add_py_mod_src(MOD "${_ARG_NAME}_${_trans_py_source}_pymod")
        endif()
        set(_PY_FILES)
      else()
        if(_EXPECT_IN_DIR)
          set(_EXPECT_IN_DIR FALSE)
          set(_DIR ${_PY_FILE})
          set(_ABS_PY_FILES)
          set(_PY_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/${_DIR}")
          foreach(_PY ${_PY_FILES})
            list(APPEND _ABS_PY_FILES "${_PY_SOURCE_DIR}/${_PY}")
          endforeach()
          install(FILES ${_ABS_PY_FILES} DESTINATION
                  "${LIB_DIR}/${PYMOD_DIR}/${_DIR}")
          string(REPLACE "/" "_" _DIR_NO_SLASH "${_DIR}")
          add_custom_target("${_ARG_NAME}_${_DIR_NO_SLASH}_pymod" ALL)
          add_dependencies("_${_ARG_NAME}"
                           "${_ARG_NAME}_${_DIR_NO_SLASH}_pymod")
          copy_if_different("./" "${PYMOD_STAGE_DIR}/${_DIR}" "${_ABS_PY_FILES}"
                            TARGETS "${_ARG_NAME}_${_DIR_NO_SLASH}_pymod")
          if(NOT DISABLE_DOCUMENTATION)
            # add deps
            add_doc_dependency(NAME ${_ARG_NAME}
                               DEP "${_ARG_NAME}_${_DIR_NO_SLASH}_pymod")
            add_doc_dependency(NAME ${_ARG_NAME} DEP "${_ABS_PY_FILES}")
            add_py_mod_src(MOD "${_ARG_NAME}_${_DIR_NO_SLASH}_pymod")
          endif()
          set(_PY_FILES)
        elseif(_TRANSLATE)
          set(_TRANSLATE FALSE)
          set(_trans_py_source ${_PY_FILES})
          set(_PY_FILES "${_PY_FILE}")
        else()
          list(APPEND _PY_FILES "${_PY_FILE}")
        endif()
      endif()
    endforeach()
    if(_PY_FILES)
      if(NOT TARGET "${_ARG_NAME}_pymod")
        add_custom_target("${_ARG_NAME}_pymod" ALL)
      endif(NOT TARGET "${_ARG_NAME}_pymod")
      set(_ABS_PY_FILES)
      foreach(_PY ${_PY_FILES})
        list(APPEND _ABS_PY_FILES "${CMAKE_CURRENT_SOURCE_DIR}/${_PY}")
      endforeach()
      copy_if_different("./" "${PYMOD_STAGE_DIR}" "${_ABS_PY_FILES}" "TARGETS"
                        "${_ARG_NAME}_pymod")
      add_dependencies("_${_ARG_NAME}" "${_ARG_NAME}_pymod")
      include_directories(${Python_INCLUDE_DIRS})
      install(FILES ${_PY_FILES} DESTINATION "${LIB_DIR}/${PYMOD_DIR}")
    endif()
  endif()
  if(TARGET "${_PARENT_NAME}")
    get_target_property(_MOD_DEPS "${_PARENT_NAME}" MODULE_DEPS)
    if(_MOD_DEPS)
      foreach(dep ${_MOD_DEPS})
         add_dependencies("_${_ARG_NAME}" "${dep}")
      endforeach()
    endif()
  endif()
  foreach(_dep ${_ARG_DEPENDS_ON})
    add_dependencies("_${_ARG_NAME}" ${_dep})
  endforeach()
  if(_ARG_NEED_CONFIG_HEADER)
    if(NOT TARGET config_header)
      set(err_msg "Module '${_ARG_NAME}' requires the config_header target")
      set(err_msg "${err_msg} which does not yet exist. Please call")
      set(err_msg "${err_msg} add_subdirectory(<YOUR MODULE>) after ")
      set(err_msg "${err_msg} add_subdirectory(config) in the top-level ")
      set(err_msg "${err_msg} CMakeLists.txt.")
      message(FATAL_ERROR "${err_msg}")
    endif()
    add_dependencies("_${_ARG_NAME}" config_header)
  endif()

  #-----------------------------------------------------------------------------
  # sphinx documentation
  #-----------------------------------------------------------------------------
  if(NOT DISABLE_DOCUMENTATION)
    add_doc_dependency(NAME ${_ARG_NAME} DEP "${_ARG_NAME}_pymod")
    if(_ABS_PY_FILES)
      add_doc_dependency(NAME ${_ARG_NAME} DEP "${_ABS_PY_FILES}")
    endif()
    add_py_mod_src(MOD "${_ARG_NAME}_pymod")
  endif()
endmacro(pymod)

# to provide a dedicated codetest, we add 'codetest' to the make targets and
# link it to 'check' so we can run unit tests without the documentation tests
add_custom_target(check)
add_custom_target(check_xml)
add_custom_target(codetest)
add_dependencies(check codetest)

#-------------------------------------------------------------------------------
# promod3_unittest
#
# define a unit test
#-------------------------------------------------------------------------------
macro(promod3_unittest)
  set(_ARG_PREFIX promod3)
  parse_argument_list(_ARG "MODULE;SOURCES;LINK;DATA;TARGET;BASE_TARGET" "" ${ARGN})
  set(_SOURCES ${_ARG_SOURCES})
  set(CPP_TESTS)
  set(PY_TESTS)
  if(NOT _ARG_MODULE)
    message(FATAL_ERROR
            "invalid use of promod3_unittest(): module name missing")
  endif()
  set(CMAKE_CURRENT_BINARY_DIR "${CMAKE_BINARY_DIR}/tests/${_ARG_MODULE}")
  file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  add_custom_target("test_data_${_ARG_MODULE}")
  if(_ARG_TARGET)
    add_dependencies("test_data_${_ARG_MODULE}" "${_ARG_TARGET}")
  endif(_ARG_TARGET)
  add_custom_command(TARGET "test_data_${_ARG_MODULE}" PRE_BUILD
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR})
  if(_ARG_DATA)
    foreach(dfile ${_ARG_DATA})
      get_filename_component(tdata_path "${dfile}" PATH)
      set(tdata_full_path "${CMAKE_CURRENT_BINARY_DIR}/${tdata_path}")
      if(tdata_path)
        file(MAKE_DIRECTORY "${tdata_full_path}")
        add_custom_command(TARGET "test_data_${_ARG_MODULE}" PRE_BUILD
          COMMAND ${CMAKE_COMMAND} -E make_directory
          "${tdata_full_path}")
      endif()
      get_filename_component(tdata_file "${dfile}" NAME)
      copy_if_different("${CMAKE_CURRENT_SOURCE_DIR}/${tdata_path}"
        "${tdata_full_path}" "${tdata_file}"
        TARGETS "test_data_${_ARG_MODULE}")
      add_unit_test_data_target(DAT "test_data_${_ARG_MODULE}")
    endforeach()
  endif(_ARG_DATA)
  foreach(src ${_SOURCES})
    if(${src} MATCHES "\\.py$")
      list(APPEND PY_TESTS "${src}")
    else()
      list(APPEND CPP_TESTS "${CMAKE_CURRENT_SOURCE_DIR}/${src}")
   endif()
  endforeach()
  set(_SOURCES ${CPP_TESTS})
  set(_test_name "test_suite_${_ARG_MODULE}")
  if(DEFINED CPP_TESTS)
    if(COMPILE_TESTS)
      add_executable(${_test_name} ${_SOURCES})
    else()
      add_executable(${_test_name} EXCLUDE_FROM_ALL ${_SOURCES})
    endif()
    set_target_properties(${_test_name} PROPERTIES
                         RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    set_target_properties(${_test_name} PROPERTIES
                   RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_CURRENT_BINARY_DIR})
    set_target_properties(${_test_name} PROPERTIES
                 RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_CURRENT_BINARY_DIR})
    add_dependencies(${_test_name} "test_data_${_ARG_MODULE}")
    target_link_libraries(${_test_name} ${BOOST_UNIT_TEST_LIBRARIES}
                        "${_ARG_PREFIX}_${_ARG_MODULE}")
    add_custom_target("${_test_name}_run"
                    COMMAND
           PROMOD3_ROOT=${STAGE_DIR} ${CMAKE_CURRENT_BINARY_DIR}/${_test_name}
                    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                    COMMENT "running checks for module ${_ARG_MODULE}"
                    DEPENDS ${_test_name})
    if(TARGET "_${_ARG_MODULE}")
      add_dependencies("${_test_name}_run" "_${_ARG_MODULE}")
    endif()
    set(_xml_test_cmd "PROMOD3_ROOT=${STAGE_DIR}")
    set(_xml_test_cmd ${_xml_test_cmd} ${CMAKE_CURRENT_BINARY_DIR})
    set(_xml_test_cmd "${_xml_test_cmd}/${_test_name}")
    set(_xml_test_cmd ${_xml_test_cmd} "--log_format=xml" "--log_level=all")
    # XML test outputgets an logical OR to 'echo' so if sth fails, make
    # continues and we get output for all unit tests. Just calling 'echo'
    # giveth $?=0.
    add_custom_target("${_test_name}_run_xml"
                    COMMAND ${_xml_test_cmd} > ${_test_name}_log.xml || echo
                    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                    COMMENT "running checks for module ${_ARG_MODULE}"
                    DEPENDS ${_test_name})
    if(TARGET "_${_ARG_MODULE}")
      add_dependencies("${_test_name}_run_xml" "_${_ARG_MODULE}")
    endif()
    add_test("${_test_name}" ${CMAKE_CURRENT_BINARY_DIR}/${_test_name} )
    add_dependencies(check_xml "${_test_name}_run_xml")
    if(_ARG_BASE_TARGET)
      add_dependencies("${_ARG_BASE_TARGET}" "${_test_name}_run")
    else()
      add_dependencies(codetest "${_test_name}_run")
    endif()

    if(_ARG_LINK)
      target_link_libraries("${_test_name}" ${_ARG_LINK})
    endif()

    set_target_properties(${_test_name}
                          PROPERTIES RUNTIME_OUTPUT_DIRECTORY
                          "${CMAKE_CURRENT_BINARY_DIR}")
  endif()

  foreach(py_test ${PY_TESTS})
    set(py_twp "${CMAKE_CURRENT_SOURCE_DIR}/${py_test}")
    if(NOT EXISTS "${py_twp}")
      message(FATAL_ERROR "Python test script does not exist: ${py_twp}")
    endif()
    get_python_path(python_path)
    set (PY_TESTS_CMD "PYTHONPATH=${python_path}  ${Python_EXECUTABLE}")
    add_custom_target("${py_test}_run"
                sh -c "${PY_TESTS_CMD} ${py_twp}"
              WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
              COMMENT "running checks ${py_test}" VERBATIM)
    add_dependencies("${py_test}_run" "test_data_${_ARG_MODULE}")
    if(TARGET "_${_ARG_MODULE}")
      add_dependencies("${py_test}_run" "_${_ARG_MODULE}")
    endif()
    # XML test output gets an logical OR to 'echo' so if sth fails, make
    # continues and we get output for all unit tests. Just calling 'echo'
    # gives $?=0.
    add_custom_target("${py_test}_run_xml"
                sh -c "${PY_TESTS_CMD} ${py_twp} xml || echo"
              WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
              COMMENT "running checks ${py_test}" VERBATIM)
    add_dependencies("${py_test}_run_xml" "test_data_${_ARG_MODULE}")
    if(TARGET "_${_ARG_MODULE}")
      add_dependencies("${py_test}_run_xml" "_${_ARG_MODULE}")
    endif()
    add_dependencies(check_xml "${py_test}_run_xml")
    if(_ARG_BASE_TARGET)
      add_dependencies("${_ARG_BASE_TARGET}" "${py_test}_run")
    else()
      add_dependencies(codetest "${py_test}_run")
    endif()
  endforeach()
endmacro(promod3_unittest)

#-------------------------------------------------------------------------------
# Synopsis:
#   add_unit_test_data_target(DAT target)
#
# Description:
#   Add a data target for a unit test to PM3_UNIT_TEST_DATA.
#   DAT - target
#-------------------------------------------------------------------------------
macro(add_unit_test_data_target)
  parse_argument_list(_ARG "DAT" "" ${ARGN})
  if(NOT _ARG_DAT)
    message(FATAL_ERROR
            "invalid use of add_unit_test_data_target(): target missing")
  endif()
  if(DEFINED PM3_UNIT_TEST_DATA)
    set(_DATATARGETS "${PM3_UNIT_TEST_DATA}")
    set(_NME_FOUND)
    foreach(nme ${PM3_UNIT_TEST_DATA})
      if(${nme} STREQUAL ${_ARG_DAT})
        set(_NME_FOUND TRUE)
      endif()
    endforeach()
    if(NOT _NME_FOUND)
      list(APPEND _DATATARGETS "${_ARG_DAT}")
    endif()
  else()
    set(_DATATARGETS "${_ARG_DAT}")
  endif()
  set(PM3_UNIT_TEST_DATA "${_DATATARGETS}" CACHE INTERNAL "" FORCE)
endmacro(add_unit_test_data_target)

#-------------------------------------------------------------------------------
# make sure the previously detected Python interpreter has the given module
#-------------------------------------------------------------------------------
macro(promod3_find_python_module MODULE)
  if(NOT PYTHON_MODULE_${MODULE})
    message(STATUS "Searching for python module ${MODULE} for ${Python_EXECUTABLE}")
    execute_process(COMMAND ${Python_EXECUTABLE} -c "import ${MODULE}"
                    OUTPUT_QUIET ERROR_QUIET
                    RESULT_VARIABLE _IMPORT_ERROR)
    if(_IMPORT_ERROR)
      message(FATAL_ERROR
              "Could not find python module ${MODULE}. Please install it")
    else()
      message(STATUS "Found python module ${MODULE}")
      set("PYTHON_MODULE_${MODULE}" FOUND CACHE STRING "" FORCE)
    endif()
  endif()
endmacro(promod3_find_python_module)


#-------------------------------------------------------------------------------
# this macro tries to detect a very common problem during configuration stage:
# it makes sure that boost python is linked against the same version of python 
# that we are linking against. 
#-------------------------------------------------------------------------------
macro(promod3_match_boost_python_version)
  include(CheckCXXSourceRuns)
  # this variable may either be a simple library path or list that contains
  # different libraries for different build-options. For example:
  # optimized;<lib1>;debug;<lib2>
  set(_BOOST_PYTHON_LIBRARY ${Boost_PYTHON_LIBRARY})
  list(LENGTH _BOOST_PYTHON_LIBRARY _BP_LENGTH)
  if(_BP_LENGTH GREATER 1)
    list(FIND _BOOST_PYTHON_LIBRARY optimized _OPTIMIZED_INDEX)
    if(_OPTIMIZED_INDEX EQUAL -1)
      message(FATAL_ERROR 
              "Error while trying to get path of boost python library")
    endif()
    math(EXPR _LIB_INDEX 1+${_OPTIMIZED_INDEX})
    list(GET _BOOST_PYTHON_LIBRARY ${_LIB_INDEX} _BP_LIB_PATH)
    set(_BOOST_PYTHON_LIBRARY ${_BP_LIB_PATH})
  endif()
  set(CMAKE_REQUIRED_FLAGS "-I${Python_INCLUDE_DIRS}")
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS} -I${Boost_INCLUDE_DIR}")
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS} ${Python_LIBRARIES}")
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS} ${_BOOST_PYTHON_LIBRARY}")
  check_cxx_source_runs(
"#include <boost/python.hpp>

using namespace boost::python;
                           
int main( int argc, char ** argv ) {
  try {
    Py_Initialize();

    object main_module((
      handle<>(borrowed(PyImport_AddModule(\"__main__\")))));

    object main_namespace = main_module.attr(\"__dict__\");

    handle<> ignored(( PyRun_String( \"print 'Hello, World'\",
                                     Py_file_input,
                                     main_namespace.ptr(),
                                     main_namespace.ptr() ) ));
  } catch( error_already_set ) {
    PyErr_Print();
    return 1;
  }   
  return 0;
}" PYTHON_VERSION_FOUND_MATCHES_PYTHON_VERSION_BOOST_WAS_COMPILED_WITH)

  if(NOT PYTHON_VERSION_FOUND_MATCHES_PYTHON_VERSION_BOOST_WAS_COMPILED_WITH)
     message(FATAL_ERROR "Python versions mismatch!\n"
             "Make sure the python version for boost match the one you "
             "specified during configuration\n")
  endif()
endmacro(promod3_match_boost_python_version)


#-------------------------------------------------------------------------------
# this macro sets up the stage directories
#-------------------------------------------------------------------------------
macro(setup_stage)
  set(STAGE_DIR "${CMAKE_BINARY_DIR}/stage")
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${STAGE_DIR}/bin  )
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${STAGE_DIR}/bin  )
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${STAGE_DIR}/bin  )
  set(HEADER_STAGE_PATH ${STAGE_DIR}/include )
  set(SHARED_DATA_PATH ${STAGE_DIR}/share/promod3  )
  set(ETC_STAGE_PATH ${STAGE_DIR}/etc )

  if(UNIX AND NOT APPLE)
    check_architecture()
  endif()
  set (ARCH ${CMAKE_NATIVE_ARCH})
  if("${ARCH}" MATCHES "64" AND NOT _UBUNTU_LAYOUT)
    set(LIB_DIR lib64  )
    set(LIB_STAGE_PATH "${STAGE_DIR}/lib64"  )
  else()
    set(LIB_DIR lib  )
    set(LIB_STAGE_PATH "${STAGE_DIR}/lib"  )
  endif()
  if(_UBUNTU_LAYOUT)
    set(LIBEXEC_PATH ${LIB_DIR}/promod3/libexec  )
    set(LIBEXEC_STAGE_PATH ${LIB_STAGE_PATH}/promod3/libexec  )
  else()
    set(LIBEXEC_PATH libexec/promod3  )
    set(LIBEXEC_STAGE_PATH ${STAGE_DIR}/libexec/promod3  )
  endif()
 
  include_directories("${HEADER_STAGE_PATH}")
  link_directories(${LIB_STAGE_PATH})

endmacro(setup_stage)

#-------------------------------------------------------------------------------
# get compiler version
#-------------------------------------------------------------------------------
function(get_compiler_version _OUTPUT_VERSION)
  exec_program(${CMAKE_CXX_COMPILER}
               ARGS ${CMAKE_CXX_COMPILER_ARG1} -dumpversion
               OUTPUT_VARIABLE _COMPILER_VERSION
  )
  string(REGEX REPLACE "([0-9])\\.([0-9])(\\.[0-9])?" "\\1\\2"
    _COMPILER_VERSION ${_COMPILER_VERSION})

  set(${_OUTPUT_VERSION} ${_COMPILER_VERSION} PARENT_SCOPE)
endfunction(get_compiler_version)



macro(setup_compiler_flags)
  if("${CMAKE_CXX_COMPILER_ID}" MATCHES "AppleClang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  endif()
  if(CMAKE_COMPILER_IS_GNUCXX)
    get_compiler_version(_GCC_VERSION)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall" )
    if(_GCC_VERSION MATCHES "44")
      # gcc 4.4. is very strict about aliasing rules. the shared_count
      # implementation that is used boost's shared_ptr violates these rules. To
      # silence the warnings and prevent miscompiles, enable
      #  -fno-strict-aliasing
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-strict-aliasing" )
    endif()
    #message(STATUS "GCC VERSION " ${_GCC_VERSION})
    if (_GCC_VERSION LESS "60")
      # for older compilers we need to enable C++11
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    endif()
  endif()
  if(_ENABLE_SSE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -msse4" )
  endif()
endmacro(setup_compiler_flags)

set(_BOOST_MIN_VERSION 1.53)

macro(setup_boost)
  # starting with CMake 3.11 we could use the following instead of the foreach
  # find_package(Boost ${_BOOST_MIN_VERSION} COMPONENTS
  #              python${Python_VERSION_MAJOR}${Python_VERSION_MINOR} REQUIRED)
  # set(BOOST_PYTHON_LIBRARIES ${Boost_LIBRARIES})
  # see https://cmake.org/cmake/help/v3.11/module/FindBoost.html
  foreach(_python_lib_name python${Python_VERSION_MAJOR}${Python_VERSION_MINOR}
                           python${Python_VERSION_MAJOR}.${Python_VERSION_MINOR}
                           python${Python_VERSION_MAJOR}
                           python)
    find_package(Boost ${_BOOST_MIN_VERSION} COMPONENTS ${_python_lib_name} QUIET)
    if(Boost_FOUND)
      message(STATUS "Found Boost package: " ${_python_lib_name})
      set(BOOST_PYTHON_LIBRARIES ${Boost_LIBRARIES})
      break()
    else()
      message(STATUS "Boost package not found: " ${_python_lib_name}
                     ". Trying alternative names!")
    endif()
  endforeach(_python_lib_name)
  if(NOT BOOST_PYTHON_LIBRARIES)
    message(FATAL_ERROR "Failed to find any Boost Python library!")
  endif()
  set(Boost_LIBRARIES)
  find_package(Boost ${_BOOST_MIN_VERSION}
               COMPONENTS unit_test_framework REQUIRED)
  set(BOOST_UNIT_TEST_LIBRARIES ${Boost_LIBRARIES})
  set(Boost_LIBRARIES)
  if(ENABLE_STATIC)
    set(Boost_USE_STATIC_LIBS ON)
  endif()
  find_package(Boost ${_BOOST_MIN_VERSION}
               COMPONENTS filesystem system REQUIRED)
  set(BOOST_LIBRARIES ${Boost_LIBRARIES})
  set(Boost_LIBRARIES)
  find_package(Boost ${_BOOST_MIN_VERSION} COMPONENTS iostreams REQUIRED)
  set(BOOST_IOSTREAM_LIBRARIES ${Boost_LIBRARIES})
  set(Boost_LIBRARIES)
  find_package(Boost ${_BOOST_MIN_VERSION} COMPONENTS program_options REQUIRED)
  set(BOOST_PROGRAM_OPTIONS ${Boost_LIBRARIES})
  set(Boost_LIBRARIES)
  find_package(Boost ${_BOOST_MIN_VERSION} COMPONENTS regex REQUIRED)
  set(BOOST_REGEX_LIBRARIES ${Boost_LIBRARIES})
  set(Boost_LIBRARIES)
endmacro(setup_boost)

#-------------------------------------------------------------------------------
# Documentation to be found in cmake_support/doc/index.rst
#-------------------------------------------------------------------------------
macro(add_doc_dependency)
  parse_argument_list(_ADD_ARG "NAME;DEP" "" ${ARGN})
  if(NOT _ADD_ARG_NAME)
    message(FATAL_ERROR
            "invalid use of add_doc_dependency(): module name missing")
  endif(NOT _ADD_ARG_NAME)
  if(NOT _ADD_ARG_DEP)
    message(FATAL_ERROR
            "invalid use of add_doc_dependency(): dependencies missing")
  endif(NOT _ADD_ARG_DEP)
  if(DEFINED PM3_DOC_DEPS_${_ADD_ARG_NAME})
    set(_DOC_DEPS "${PM3_DOC_DEPS_${_ADD_ARG_NAME}}")
  else()
    set(_DOC_DEPS)
  endif()
  foreach(deps ${_ADD_ARG_DEP})
    # check if already present, add to list, if not
    list(FIND _DOC_DEPS ${deps} _check_red)
    if("${_check_red}" MATCHES "-1")
      list(APPEND _DOC_DEPS "${deps}")
    endif()
  endforeach()
  set(PM3_DOC_DEPS_${_ADD_ARG_NAME} "${_DOC_DEPS}" CACHE INTERNAL "" FORCE)
endmacro(add_doc_dependency)

#-------------------------------------------------------------------------------
# Documentation to be found in cmake_support/doc/index.rst
#-------------------------------------------------------------------------------
macro(add_doc_source)
  parse_argument_list(_ARG "NAME;RST" "" ${ARGN})
  if(NOT _ARG_NAME)
    message(FATAL_ERROR "invalid use of add_doc_source(): module name missing")
  endif()
  if(NOT _ARG_RST)
    message(FATAL_ERROR "invalid use of add_doc_source(): sources missing")
  endif()
  if(DEFINED PM3_RST_${_ARG_NAME})
    set(_DOC_SOURCES "${PM3_RST_${_ARG_NAME}}")
  else()
    set(_DOC_SOURCES)
  endif()
  foreach(rst ${_ARG_RST})
    list(APPEND _DOC_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/${rst}")
  endforeach()
  set(PM3_RST_${_ARG_NAME} "${_DOC_SOURCES}" CACHE INTERNAL "" FORCE)
  add_py_mod_src(MOD ${_ARG_NAME})
endmacro(add_doc_source)

#-------------------------------------------------------------------------------
# Synopsis:
#   add_py_mod_src(MOD module)
#
# Description:
#   Add a module name to PM3_PY_MS. This is also used to fetch dependencies
#   on Python code.
#   MOD - identifier
#-------------------------------------------------------------------------------
macro(add_py_mod_src)
  parse_argument_list(_ARG_DOC "MOD" "" ${ARGN})
  if(NOT _ARG_DOC_MOD)
    message(FATAL_ERROR "invalid use of add_py_mod_src(): sources missing")
  endif()
  if(DEFINED PM3_PY_MS)
    set(_MODULES "${PM3_PY_MS}")
    set(_NME_FOUND)
    foreach(nme ${PM3_PY_MS})
      if(${nme} STREQUAL ${_ARG_DOC_MOD})
        set(_NME_FOUND TRUE)
      endif()
    endforeach()
    if(NOT _NME_FOUND)
      list(APPEND _MODULES "${_ARG_DOC_MOD}")
    endif()
  else()
    set(_MODULES "${_ARG_DOC_MOD}")
  endif()
  set(PM3_PY_MS "${_MODULES}" CACHE INTERNAL "" FORCE)
endmacro(add_py_mod_src)

#-------------------------------------------------------------------------------
# Synopsis:
#   add_to_pymod_list(MOD module)
#
# Description:
#   Add a module name to PM3_PYMODULES.
#   MOD - identifier
#-------------------------------------------------------------------------------
macro(add_to_pymod_list)
  parse_argument_list(_ARG_DOC "MOD" "" ${ARGN})
  if(NOT _ARG_DOC_MOD)
    message(FATAL_ERROR "invalid use of add_to_pymod_list(): module missing")
  endif()
  if(DEFINED PM3_PYMODULES)
    set(_MODULES "${PM3_PYMODULES}")
    set(_NME_FOUND)
    foreach(nme ${PM3_PYMODULES})
      if(${nme} STREQUAL ${_ARG_DOC_MOD})
        set(_NME_FOUND TRUE)
      endif()
    endforeach()
    if(NOT _NME_FOUND)
      list(APPEND _MODULES "${_ARG_DOC_MOD}")
    endif()
  else()
    set(_MODULES "${_ARG_DOC_MOD}")
  endif()
  set(PM3_PYMODULES "${_MODULES}" CACHE INTERNAL "" FORCE)
endmacro(add_to_pymod_list)

#-------------------------------------------------------------------------------
# Synopsis:
#   add_changelog_to_doc(FILE changelog file)
#
# Description:
#   Add a CHANGELOG file to the documentation.
#   FILE - name of the changelog file
#-------------------------------------------------------------------------------
macro(add_changelog_to_doc)
  parse_argument_list(_ARG_CLOG "FILE" "" ${ARGN})
  if(NOT _ARG_CLOG_FILE)
    message(FATAL_ERROR "invalid use of add_changelog_to_doc(): file missing")
  endif()
  set(PM3_DOC_CHANGELOG "${_ARG_CLOG_FILE}" CACHE INTERNAL "" FORCE)
endmacro(add_changelog_to_doc)

#-------------------------------------------------------------------------------
# Synopsis:
#   add_license_to_doc(FILE license file)
#
# Description:
#   Add a LICENSE file to the documentation.
#   FILE - name of the license file
#-------------------------------------------------------------------------------
macro(add_license_to_doc)
  parse_argument_list(_ARG_LICENSE "FILE" "" ${ARGN})
  if(NOT _ARG_LICENSE_FILE)
    message(FATAL_ERROR "invalid use of add_license_to_doc(): file missing")
  endif()
  set(PM3_DOC_LICENSE "${_ARG_LICENSE_FILE}" CACHE INTERNAL "" FORCE)
endmacro(add_license_to_doc)

#-------------------------------------------------------------------------------
# Synopsis:
#   pm_action_init()
#
# Description:
#   Initialise cached variables
#-------------------------------------------------------------------------------
macro(pm_action_init)
  set(PM3_ACTION_NAMES "" CACHE INTERNAL "" FORCE)
endmacro(pm_action_init)

#-------------------------------------------------------------------------------
# Synopsis:
#   pm_action(ACTION TARGET)
#
# Description:
#   Add a script to actions.
#   ACTION script to be added (needs to have permissions to be executed)
#   TARGET make target to add the action to
#-------------------------------------------------------------------------------
macro(pm_action ACTION TARGET)
  copy_if_different("${CMAKE_CURRENT_SOURCE_DIR}"
                    "${STAGE_DIR}/${LIBEXEC_PATH}" 
                    "${ACTION}" "TARGETS" ${TARGET})
  install(FILES "${ACTION}" DESTINATION "${LIBEXEC_PATH}"
          PERMISSIONS WORLD_EXECUTE GROUP_EXECUTE OWNER_EXECUTE 
                      WORLD_READ GROUP_READ OWNER_READ)
  # storing tool names for bash completion
  string(REGEX REPLACE "^pm-" "" stripped_action ${ACTION})
  if(DEFINED PM3_ACTION_NAMES)
    if(${PM3_ACTION_NAMES} MATCHES "${stripped_action}")
      set(_ACTION_NAMES "${PM3_ACTION_NAMES}")
    else()
      if("${PM3_ACTION_NAMES}" STREQUAL "")
        set(_ACTION_NAMES "${stripped_action}")
      else()
        set(_ACTION_NAMES "${PM3_ACTION_NAMES} ${stripped_action}")
      endif()
    endif()
  else()
    set(_ACTION_NAMES "${stripped_action}")
  endif()
  set(PM3_ACTION_NAMES "${_ACTION_NAMES}" CACHE INTERNAL "" FORCE)
endmacro(pm_action)

#-------------------------------------------------------------------------------
# Synopsis:
#   add_module_data(NAME DATALST DATASUBDIR)
#
# Description:
#   Add data to a module. Will be placed in SHARED_DATA_PATH. Your private sub-
#   directory may be specified by DATASUBDIR. DATALST is the list of data files.
#-------------------------------------------------------------------------------
macro(add_module_data)
  parse_argument_list(_AMD_ARG "NAME;DATALIST;DATASUBDIR" "" ${ARGN})
  if(NOT _AMD_ARG_NAME)
    message(FATAL_ERROR
            "invalid use of add_module_data(): a module NAME must be provided")
  endif()
  if(NOT _AMD_ARG_DATALIST)
    message(FATAL_ERROR
            "invalid use of add_module_data(): a DATALIST must be provided")
  endif()
  if(NOT _AMD_ARG_DATASUBDIR)
    set(_amd_datapath "${SHARED_DATA_PATH}")
    set(_amd_datadest "share/promod3/")
  else()
    set(_amd_datapath "${SHARED_DATA_PATH}/${_AMD_ARG_DATASUBDIR}")
    set(_amd_datadest "share/promod3/${_AMD_ARG_DATASUBDIR}")
  endif()
  if(NOT TARGET "_${_AMD_ARG_NAME}")
    set(err_msg "Module '${_AMD_ARG_NAME}': add_module_data() has to be called")
    set(err_msg "${err_msg} after module() and/ or pymod(), this can easily be")
    set(err_msg "${err_msg} fixed by rearranging add_subdirectory() calls in")
    set(err_msg "${err_msg} the top-level CMakeLists.txt of this module")
    message(FATAL_ERROR "${err_msg}")
  endif()
  set(_amd_datatarget "${_AMD_ARG_NAME}_data")
  add_custom_target("${_amd_datatarget}")
  copy_if_different("${CMAKE_CURRENT_SOURCE_DIR}" "${_amd_datapath}"
    "${_AMD_ARG_DATALIST}" "${_amd_datatarget}" "${_amd_datatarget}")
  install(FILES ${_AMD_ARG_DATALIST} DESTINATION "${_amd_datadest}")
  add_dependencies("_${_AMD_ARG_NAME}" "${_amd_datatarget}")
endmacro(add_module_data)

#-------------------------------------------------------------------------------
# Synopsis:
#   convert_module_data(MODULE name FILE file SCRIPT script [ARGS args])
# Description: (details in doc)
#   Convert portable_FILE to SHARED_DATA_PATH/MODULE_data/FILE using the Python
#   script SCRIPT (called as "python SCRIPT in_path out_path ARGS").
#-------------------------------------------------------------------------------
macro(convert_module_data)
  parse_argument_list(_ARG "MODULE;FILE;SCRIPT;ARGS" "" ${ARGN})
  # check
  if(NOT _ARG_MODULE)
    message(FATAL_ERROR "invalid use of convert_module_data(): "
            "a MODULE name must be provided")
  endif()
  if(NOT _ARG_FILE)
    message(FATAL_ERROR "invalid use of convert_module_data(): "
            "a FILE must be provided")
  endif()
  if(NOT _ARG_SCRIPT)
    message(FATAL_ERROR "invalid use of convert_module_data(): "
            "a SCRIPT must be provided")
  endif()
  # setup python path
  get_python_path(_ENV_PY_PATH)

  # setup module and target folder
  set(_DATA_TARGET "${_ARG_MODULE}_data")
  set(_DATA_DIR_TARGET "${_ARG_MODULE}_data_dir")
  set(_TRG_DIR "${SHARED_DATA_PATH}/${_ARG_MODULE}_data")
  if(NOT TARGET "${_DATA_TARGET}")
    add_custom_target("${_DATA_TARGET}" ALL)
    file(MAKE_DIRECTORY  ${_TRG_DIR})
    # used as dep. to ensure folder is there before script is called
    add_custom_target("${_DATA_DIR_TARGET}"
                      COMMAND ${CMAKE_COMMAND} -E make_directory "${_TRG_DIR}")
  endif()

  # setup source and target files
  set(_SRC_PATH "${CMAKE_CURRENT_SOURCE_DIR}/portable_${_ARG_FILE}")
  set(_TRG_PATH "${_TRG_DIR}/${_ARG_FILE}")

  # setup python call
  set(_PY_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${_ARG_SCRIPT}")
  set(_PY_CMD "${Python_EXECUTABLE} '${_PY_PATH}' '${_SRC_PATH}' '${_TRG_PATH}' ${_ARG_ARGS}")

  # set command to run (depends on _MODULE (pymod-target), FILE and SCRIPT)
  # -> here we also make sure "import promod3" is ready
  add_custom_command(
    OUTPUT  "${_TRG_PATH}"
    COMMAND sh -c "PYTHONPATH=${_ENV_PY_PATH} ${_PY_CMD}"
    DEPENDS "${_DATA_DIR_TARGET}" "${_SRC_PATH}" "${_PY_PATH}" "_${_ARG_MODULE}"
            "_init")
  # actual target named generate_...
  set(_TRG_NAME "generate_${_ARG_MODULE}_data_${_ARG_FILE}")
  add_custom_target("${_TRG_NAME}" DEPENDS "${_TRG_PATH}")
  add_dependencies("${_DATA_TARGET}" "${_TRG_NAME}")

  # put install
  install(FILES "${_TRG_PATH}" DESTINATION "share/promod3/${_ARG_MODULE}_data")
endmacro(convert_module_data)

#-------------------------------------------------------------------------------
# Synopsis:
#   find_path_recursive(VARIABLE
#                       NAME file
#                       PATH path
#                       [PATH_SUFFIXES dir1 dir2 ...])
# Description:
#   Find a path to a file in a directory tree. The result is stored in the
#   given variable. Its 'VARIABLE-NOTFOUND' if the file could not be found.
#   PATH defines were to search, PATH_SUFFIXES sub-directories in PATH to limit
#   the search.
#-------------------------------------------------------------------------------
macro(find_path_recursive VARIABLE)
  set(_ARGS "NAME;PATH;PATH_SUFFIXES")
  parse_argument_list(_ARG "${_ARGS}" "" ${ARGN})
  if(NOT _ARG_NAME)
    message(FATAL_ERROR "Wrong use of find_path_recursive, we need a file NAME "
                        "to look for.")
  endif(NOT _ARG_NAME)
  if(NOT _ARG_PATH)
    message(FATAL_ERROR "Wrong use of find_path_recursive, we need a PATH to "
                        "look in.")
  endif(NOT _ARG_PATH)
  # get first level of dirs (needed to deal with PATH_SUFFIXES if present)
  if(_ARG_PATH_SUFFIXES)
    set(_fst_subs)
    foreach(_psuf ${_ARG_PATH_SUFFIXES})
      file(TO_NATIVE_PATH "${_ARG_PATH}/${_psuf}" _psuf_path)
      if(EXISTS ${_psuf_path})
        list(APPEND _fst_subs ${_psuf_path})
      endif(EXISTS ${_psuf_path})
    endforeach()
  else(_ARG_PATH_SUFFIXES)
    file(GLOB _fst_subs ${_ARG_PATH}/*)
  endif(_ARG_PATH_SUFFIXES)
  set(${VARIABLE} "${VARIABLE}-NOTFOUND")
  # iterate, top-down, until first hit
  while(_fst_subs)
    foreach(_fst ${_fst_subs})
      # check if exists and exit
      file(TO_NATIVE_PATH "${_fst}/${_ARG_NAME}" _modpath)
      if(EXISTS ${_modpath})
        # set var and exit
        set(${VARIABLE} ${_fst})
        set(_fst_subs)
        break()
      endif(EXISTS ${_modpath})
    endforeach(_fst ${_fst_subs})
    set(_tmp_dlist)
    foreach(_fst ${_fst_subs})
      # list subdirs
      file(GLOB _snd_subs ${_fst}/*)
      foreach(_snd ${_snd_subs})
        if(IS_DIRECTORY ${_snd})
          list(APPEND _tmp_dlist ${_snd})
        endif()
      endforeach()
    endforeach(_fst ${_fst_subs})
    # swap lists
    set(_fst_subs ${_tmp_dlist})
  endwhile(_fst_subs)
endmacro(find_path_recursive)
