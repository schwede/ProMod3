IF(Python_EXECUTABLE)
  EXEC_PROGRAM ("${Python_EXECUTABLE}"
    ARGS "-c 'import sphinx; print(sphinx.__version__)'"
    OUTPUT_VARIABLE VERSION_STRING
    RETURN_VALUE SPHINX_NOT_FOUND)
  if (SPHINX_NOT_FOUND)
    set(SPHINX_FOUND FALSE)
  else (SPHINX_NOT_FOUND)
    set(SPHINX_FOUND TRUE)
    set(SPHINX_VERSION ${VERSION_STRING})
  endif (SPHINX_NOT_FOUND)
ENDIF(Python_EXECUTABLE)

if (SPHINX_FOUND)
  message(STATUS "Sphinx version: " ${SPHINX_VERSION})
else (SPHINX_FOUND)
  if (Sphinx_FIND_REQUIRED)
    message (FATAL_ERROR "Could not import sphinx in " ${Python_EXECUTABLE}
             " make it available or disable documentation with -DDISABLE_DOCUMENTATION")
  endif (Sphinx_FIND_REQUIRED)
endif (SPHINX_FOUND)

