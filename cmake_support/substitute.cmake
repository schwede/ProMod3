# force configure_file to update ${OUT_FILE}
# -> otherwise it can cause continuous rebuilds if file was just touched
file(REMOVE ${OUT_FILE})
configure_file(${INPUT_FILE} ${OUT_FILE} @ONLY)