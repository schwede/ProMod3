// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/sidechain/scwrl4_rotamer_constructor.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>

BOOST_AUTO_TEST_SUITE( sidechain );

using namespace promod3;
using namespace promod3::sidechain;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

loop::AllAtomPositions GetAllAtomPos(ost::mol::ResidueHandleList& res_list) {
  // setup AllAtomPositions (needs valid OLCs)
  String sequence = "";
  for (uint i = 0; i < res_list.size(); ++i) {
    const RotamerID r_id = TLCToRotID(res_list[i].GetName());
    sequence += RotIDToOLC(r_id);
  }
  return loop::AllAtomPositions(sequence, res_list);
}

void CompareFrameResidues(FrameResidue& fr1, FrameResidue& fr2) {
  BOOST_CHECK_EQUAL(fr1.size(), fr2.size());
  for (uint i = 0; i < fr1.size(); ++i) {
    BOOST_CHECK(fr1[i] == fr2[i]);
  }
}

void CheckBackboneFrameConstruction(ost::mol::ResidueHandleList& res_list,
                                    loop::AllAtomPositions& all_atoms) {
  // check all frames
  BOOST_CHECK_EQUAL(res_list.size(), all_atoms.GetNumResidues());
  FrameResiduePtr fr1, fr2;
  geom::Vec3 cb_pos;
  SCWRL4RotamerConstructor rc;
  for (uint i = 0; i < res_list.size(); ++i) {
    const RotamerID r_id = TLCToRotID(res_list[i].GetName());
    // terminal?
    const bool n_ter = (i == 0);
    const bool c_ter = (i == res_list.size()-1);
    // phi angle
    const Real phi = (n_ter) ? -1.0472 : all_atoms.GetPhiTorsion(i);
    // frame residues
    fr1 = rc.ConstructBackboneFrameResidue(res_list[i], r_id, i,
                                           phi, n_ter, c_ter);
    fr2 = rc.ConstructBackboneFrameResidue(all_atoms, i, r_id, i, phi, n_ter, c_ter);
    CompareFrameResidues(*fr1, *fr2);
  }
}

void CheckSidechainFrameConstruction(ost::mol::ResidueHandleList& res_list,
                                     loop::AllAtomPositions& all_atoms) {
  // check all frames
  SCWRL4RotamerConstructor rc;
  BOOST_CHECK_EQUAL(res_list.size(), all_atoms.GetNumResidues());
  FrameResiduePtr fr1, fr2;
  for (uint i = 0; i < res_list.size(); ++i) {
    const RotamerID r_id = TLCToRotID(res_list[i].GetName());
    fr1 = rc.ConstructSidechainFrameResidue(res_list[i], r_id, i);
    fr2 = rc.ConstructSidechainFrameResidue(all_atoms, i, r_id, i);
    CompareFrameResidues(*fr1, *fr2);
  }
}

} // anon ns

BOOST_AUTO_TEST_CASE(test_backbone_frame_residue_consistency) {
  // same backbone frame residues via OST residues and via AllAtomPositions?
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_sc.pdb");
  ost::mol::ResidueHandleList res_list = test_ent.GetResidueList();
  loop::AllAtomPositions all_atoms = GetAllAtomPos(res_list);
  CheckBackboneFrameConstruction(res_list, all_atoms);
}

BOOST_AUTO_TEST_CASE(test_sidechain_frame_residue_consistency) {
  // same backbone frame residues via OST residues and via AllAtomPositions?
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_sc.pdb");
  ost::mol::ResidueHandleList res_list = test_ent.GetResidueList();
  loop::AllAtomPositions all_atoms = GetAllAtomPos(res_list);;
  CheckSidechainFrameConstruction(res_list, all_atoms);
}

BOOST_AUTO_TEST_SUITE_END();
