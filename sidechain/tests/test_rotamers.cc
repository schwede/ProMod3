// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/sidechain/scwrl4_rotamer_constructor.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <promod3/sidechain/sidechain_object_loader.hh>
#include <promod3/loop/all_atom_positions.hh>
#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>

BOOST_AUTO_TEST_SUITE( sidechain );

using namespace promod3;
using namespace promod3::sidechain;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

loop::AllAtomPositions GetAllAtomPos(ost::mol::ResidueHandleList& res_list) {
  // setup AllAtomPositions (needs valid OLCs)
  String sequence = "";
  for (uint i = 0; i < res_list.size(); ++i) {
    const RotamerID r_id = TLCToRotID(res_list[i].GetName());
    sequence += RotIDToOLC(r_id);
  }
  return loop::AllAtomPositions(sequence, res_list);
}

void CompareRotamers(RRMRotamer& rrm1, RRMRotamer& rrm2) {
  BOOST_CHECK_EQUAL(rrm1.GetInternalEnergy(), rrm2.GetInternalEnergy());
  BOOST_CHECK_EQUAL(rrm1.GetInternalEnergyPrefactor(),
                    rrm2.GetInternalEnergyPrefactor());
  BOOST_CHECK_EQUAL(rrm1.GetFrameEnergy(), rrm2.GetFrameEnergy());
  BOOST_CHECK_EQUAL(rrm1.GetProbability(), rrm2.GetProbability());
  BOOST_CHECK_EQUAL(rrm1.size(), rrm2.size());
  for (uint i = 0; i < rrm1.size(); ++i) {
    BOOST_CHECK(rrm1[i] == rrm2[i]);
  }
}

void CompareRotamers(FRMRotamer& frm1, FRMRotamer& frm2) {
  BOOST_CHECK_EQUAL(frm1.GetInternalEnergy(), frm2.GetInternalEnergy());
  BOOST_CHECK_EQUAL(frm1.GetInternalEnergyPrefactor(),
                    frm2.GetInternalEnergyPrefactor());
  BOOST_CHECK_EQUAL(frm1.GetFrameEnergy(), frm2.GetFrameEnergy());
  BOOST_CHECK_EQUAL(frm1.GetTemperature(), frm2.GetTemperature());
  BOOST_CHECK_EQUAL(frm1.GetProbability(), frm2.GetProbability());
  BOOST_CHECK_EQUAL(frm1.size(), frm2.size());
  for (uint i = 0; i < frm1.size(); ++i) {
    BOOST_CHECK(frm1[i] == frm2[i]);
    BOOST_CHECK(std::equal(frm1.subrotamer_association_begin(i),
                           frm1.subrotamer_association_end(i),
                           frm2.subrotamer_association_begin(i)));
  }
  BOOST_CHECK_EQUAL(frm1.subrotamer_size(), frm2.subrotamer_size());
  BOOST_CHECK(std::equal(frm1.subrotamers_begin(), frm1.subrotamers_end(),
                         frm2.subrotamers_begin()));
  for (uint i = 0; i < frm1.subrotamer_size(); ++i) {
    BOOST_CHECK_EQUAL(frm1.GetFrameEnergy(i), frm2.GetFrameEnergy(i));
  }
}

void CompareRotamerGroups(RRMRotamerGroup& rrm1, RRMRotamerGroup& rrm2) {
  BOOST_CHECK_EQUAL(rrm1.GetMaxP(), rrm2.GetMaxP());
  BOOST_CHECK_EQUAL(rrm1.GetResidueIndex(), rrm2.GetResidueIndex());
  BOOST_CHECK_EQUAL(rrm1.size(), rrm2.size());
  for (uint i = 0; i < rrm1.size(); ++i) {
    CompareRotamers(*rrm1[i], *rrm2[i]);
  }
}

void CompareRotamerGroups(FRMRotamerGroup& frm1, FRMRotamerGroup& frm2) {
  BOOST_CHECK_EQUAL(frm1.GetMaxP(), frm2.GetMaxP());
  BOOST_CHECK_EQUAL(frm1.GetResidueIndex(), frm2.GetResidueIndex());
  BOOST_CHECK_EQUAL(frm1.size(), frm2.size());
  for (uint i = 0; i < frm1.size(); ++i) {
    // FRMRotamerGroup::operator[] returns FRMRotamerPtr !
    CompareRotamers(*(frm1[i]), *(frm2[i]));
  }
}

void CheckRotamerGroupConstruction(ost::mol::ResidueHandleList& res_list,
                                   loop::AllAtomPositions& all_atoms,
                                   BBDepRotamerLibPtr bbd_rot_lib,
                                   RotamerLibPtr rot_lib) {
  // check all frames
  BOOST_CHECK_EQUAL(res_list.size(), all_atoms.GetNumResidues());
  RRMRotamerGroupPtr rrm1, rrm2;
  FRMRotamerGroupPtr frm1, frm2;
  SCWRL4RotamerConstructor rot_constructor;
  for (uint i = 0; i < res_list.size(); ++i) {
    const RotamerID r_id = TLCToRotID(res_list[i].GetName());
    // terminal?
    const bool n_ter = (i == 0);
    const bool c_ter = (i == res_list.size()-1);
    // angles
    const Real phi = (n_ter) ? -1.0472 : all_atoms.GetPhiTorsion(i);
    const Real psi = (c_ter) ? -0.7854 : all_atoms.GetPsiTorsion(i);
    // do it
    // RRM ROTAMER (bbdep)
    rrm1 = rot_constructor.ConstructRRMRotamerGroup(res_list[i], r_id, i, 
                                                    bbd_rot_lib,
                                                    phi, psi);

    rrm2 = rot_constructor.ConstructRRMRotamerGroup(all_atoms, i, r_id, i, 
                                                    bbd_rot_lib, phi, psi);
    CompareRotamerGroups(*rrm1, *rrm2);
    // RRM ROTAMER (non-bbdep)
    rrm1 = rot_constructor.ConstructRRMRotamerGroup(res_list[i], r_id, i, 
                                                    rot_lib);
    rrm2 = rot_constructor.ConstructRRMRotamerGroup(all_atoms, i, r_id, i, 
                                                    rot_lib);
    CompareRotamerGroups(*rrm1, *rrm2);
    // FRM ROTAMER (bbdep)
    frm1 = rot_constructor.ConstructFRMRotamerGroup(res_list[i], r_id, i, 
                                                    bbd_rot_lib, phi, psi);
    frm2 = rot_constructor.ConstructFRMRotamerGroup(all_atoms, i, r_id, i, 
                                                    bbd_rot_lib, phi, psi);
    CompareRotamerGroups(*frm1, *frm2);
    // RRM ROTAMER (non-bbdep)
    frm1 = rot_constructor.ConstructFRMRotamerGroup(res_list[i], r_id, i, 
                                                    rot_lib);
    frm2 = rot_constructor.ConstructFRMRotamerGroup(all_atoms, i, r_id, i, 
                                                    rot_lib);
    CompareRotamerGroups(*frm1, *frm2);
  }
}

void CompareResToAAP(ost::mol::ResidueHandle& res,
                     loop::AllAtomPositions& all_pos, uint res_idx) {
  ost::mol::AtomHandleList atom_list = res.GetAtomList();
  for (uint atom_idx = 0; atom_idx < atom_list.size(); ++atom_idx) {
    const ost::mol::AtomHandle& atom = atom_list[atom_idx];
    geom::Vec3 pos = all_pos.GetPos(all_pos.GetIndex(res_idx, atom.GetName()));
    BOOST_CHECK_EQUAL(pos, atom.GetPos());
  }
}

void CheckRotamerApplyOnResidue(ost::mol::ResidueHandleList& res_list,
                                loop::AllAtomPositions& all_atoms,
                                RotamerLibPtr rot_lib) {
  // check all frames
  BOOST_CHECK_EQUAL(res_list.size(), all_atoms.GetNumResidues());
  RRMRotamerGroupPtr rrm;
  FRMRotamerGroupPtr frm;
  SCWRL4RotamerConstructor rc;
  for (uint i = 0; i < res_list.size(); ++i) {
    // do it
    const RotamerID r_id = TLCToRotID(res_list[i].GetName());
    if (r_id != ALA && r_id != GLY) {
      // RRM ROTAMER (non-bbdep)
      rrm = rc.ConstructRRMRotamerGroup(res_list[i], r_id, i, rot_lib);
      BOOST_CHECK(rrm->size() > 0);
      rrm->ApplyOnResidue(rrm->size()-1, res_list[i], false);
      rrm->ApplyOnResidue(rrm->size()-1, all_atoms, i);
      CompareResToAAP(res_list[i], all_atoms, i);
      // RRM ROTAMER (non-bbdep)
      frm = rc.ConstructFRMRotamerGroup(res_list[i], r_id, i, rot_lib);
      BOOST_CHECK(frm->size() > 0);
      frm->ApplyOnResidue(frm->size()-1, res_list[i], false);
      frm->ApplyOnResidue(frm->size()-1, all_atoms, i);
      CompareResToAAP(res_list[i], all_atoms, i);
    }
  }
  // check exceptions
  RotamerID r_id = TLCToRotID(res_list[0].GetName());
  rrm = rc.ConstructRRMRotamerGroup(res_list[0], r_id, 0, rot_lib);
  frm = rc.ConstructFRMRotamerGroup(res_list[0], r_id, 0, rot_lib);
  // OST-style
  BOOST_CHECK_THROW(rrm->ApplyOnResidue(-1, res_list[0]), promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(-1, res_list[0]), promod3::Error);
  BOOST_CHECK_THROW(rrm->ApplyOnResidue(rrm->size(), res_list[0]),
                    promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(frm->size(), res_list[0]),
                    promod3::Error);
  // AAP-style
  BOOST_CHECK_THROW(rrm->ApplyOnResidue(-1, all_atoms, 0), promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(-1, all_atoms, 0), promod3::Error);
  BOOST_CHECK_THROW(rrm->ApplyOnResidue(rrm->size(), all_atoms, 0),
                    promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(frm->size(), all_atoms, 0),
                    promod3::Error);
  // AAP bad res. idx
  BOOST_CHECK_THROW(frm->ApplyOnResidue(0, all_atoms, -1), promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(0, all_atoms, -1), promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(0, all_atoms, 1), promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(0, all_atoms, 1), promod3::Error);
  BOOST_CHECK_THROW(rrm->ApplyOnResidue(0, all_atoms, res_list.size()),
                    promod3::Error);
  BOOST_CHECK_THROW(frm->ApplyOnResidue(0, all_atoms, res_list.size()),
                    promod3::Error);

}

} // anon ns

BOOST_AUTO_TEST_CASE(test_rotamer_group_consistency) {
  // same backbone frame residues via OST residues and via AllAtomPositions?
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_sc.pdb");
  ost::mol::ResidueHandleList res_list = test_ent.GetResidueList();
  loop::AllAtomPositions all_atoms = GetAllAtomPos(res_list);
  // get all frames without hbonds
  BBDepRotamerLibPtr bbd_rot_lib = LoadBBDepLib();
  RotamerLibPtr rot_lib = LoadLib();
  CheckRotamerGroupConstruction(res_list, all_atoms, bbd_rot_lib,
                                rot_lib);
}

BOOST_AUTO_TEST_CASE(test_rotamer_apply_on_residue) {
  // same ApplyOnResidue to OST residues and AllAtomPositions?
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_sc.pdb");
  ost::mol::ResidueHandleList res_list = test_ent.GetResidueList();
  loop::AllAtomPositions all_atoms = GetAllAtomPos(res_list);
  // check all frames without hbonds
  RotamerLibPtr rot_lib = LoadLib();
  CheckRotamerApplyOnResidue(res_list, all_atoms, rot_lib);
}

BOOST_AUTO_TEST_SUITE_END();
