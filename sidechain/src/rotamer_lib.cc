// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer_lib.hh>
#include <promod3/core/check_io.hh>
#include <promod3/core/runtime_profiling.hh>

namespace{

  bool RotamerLibEntrySorter(const promod3::sidechain::RotamerLibEntry& a, 
                             const promod3::sidechain::RotamerLibEntry& b){
    return a.probability > b.probability;
  }
}


namespace promod3{ namespace sidechain{

void RotamerLib::Save(const String& filename){

  this->MakeStatic();

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, uint64_t, Real, RotamerID/enum, RotamerLibEntry
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<uint64_t>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteTypeSize<RotamerID>(out_stream);
  core::WriteTypeSize<RotamerLibEntry>(out_stream);

  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<RotamerID>(out_stream, ASP);

  // raw data
  uint size = static_data_helper_.size();
  out_stream.write(reinterpret_cast<char*>(&size),sizeof(uint));

  RotamerID id;
  uint64_t current_pos;
  uint num_rotamers;

  for(std::map<RotamerID,std::pair<uint64_t,uint> >::iterator i = static_data_helper_.begin();
      i != static_data_helper_.end(); ++i){
    id = i->first;
    current_pos = i->second.first;
    num_rotamers = i->second.second;
    out_stream.write(reinterpret_cast<char*>(&id),sizeof(RotamerID));
    out_stream.write(reinterpret_cast<char*>(&current_pos),sizeof(uint64_t));
    out_stream.write(reinterpret_cast<char*>(&num_rotamers),sizeof(uint));
  }

  out_stream.write(reinterpret_cast<char*>(&total_num_rotamers_),sizeof(uint64_t));
  out_stream.write(reinterpret_cast<char*>(&static_data_[0]),total_num_rotamers_ * sizeof(RotamerLibEntry));

  out_stream.close();
}

RotamerLibPtr RotamerLib::Load(const String& filename){

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<uint64_t>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckTypeSize<RotamerID>(in_stream);
  core::CheckTypeSize<RotamerLibEntry>(in_stream);

  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<RotamerID>(in_stream, ASP);

  // raw data
  RotamerLibPtr p(new RotamerLib);
  p->readonly_ = true;

  uint size;
  in_stream.read(reinterpret_cast<char*>(&size),sizeof(uint));

  RotamerID id;
  uint64_t current_pos;
  uint num_rotamers;
  for(uint i = 0; i < size; ++i){
    in_stream.read(reinterpret_cast<char*>(&id),sizeof(RotamerID));
    in_stream.read(reinterpret_cast<char*>(&current_pos),sizeof(uint64_t));
    in_stream.read(reinterpret_cast<char*>(&num_rotamers),sizeof(uint));
    p->static_data_helper_[id] = std::make_pair(current_pos,num_rotamers);    
  }

  in_stream.read(reinterpret_cast<char*>(&p->total_num_rotamers_),sizeof(uint64_t));
  p->static_data_ = new RotamerLibEntry[p->total_num_rotamers_];
  in_stream.read(reinterpret_cast<char*>(&p->static_data_[0]),p->total_num_rotamers_ * sizeof(RotamerLibEntry));
  in_stream.close();
  return p;
}

void RotamerLib::SavePortable(const String& filename) {

  this->MakeStatic();

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // here: only base-type-checks needed
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);

  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<uint16_t>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<float>(out_stream);

  // data
  SerializeLoadSave(out_stream);

  out_stream_.close();
}

RotamerLibPtr RotamerLib::LoadPortable(const String& filename) {
  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);

  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<uint16_t>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<float>(in_stream);

  // data
  RotamerLibPtr p(new RotamerLib);
  p->SerializeLoadSave(in_stream);
  
  in_stream_.close();
  return p;
}

void RotamerLib::AddRotamer(RotamerID id, const RotamerLibEntry& entry){

  if(readonly_){
    throw promod3::Error("Cannot add rotamer to static library!");
  }

  //check whether it is the first rotamer of this identity
  std::map<RotamerID, std::vector<RotamerLibEntry> >::iterator it = dynamic_data_.find(id);
  if(it == dynamic_data_.end()){
  	dynamic_data_[id] = std::vector<RotamerLibEntry>();
    it = dynamic_data_.find(id);
  }

  it->second.push_back(entry);
}

std::pair<RotamerLibEntry*,uint> RotamerLib::QueryLib(RotamerID id) const{

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "RotamerLib::QueryLib", 2);

  if(!readonly_){
    throw promod3::Error("Can only read rotamers from static library!");
  }

  //In a first step, specialized residues get mapped to their parent residues
  //if they can't be found in the library
  if(id == HSE || id == HSD){
    if(id == HSE){
      if(static_data_helper_.find(HSE) == static_data_helper_.end()) id = HIS;
    }
    if(id == HSD){
      if(static_data_helper_.find(HSD) == static_data_helper_.end()) id = HIS;
    }
  }

  if(id == CYH || id == CYD){
    if(id == CYH){
      if(static_data_helper_.find(CYH) == static_data_helper_.end()) id = CYS;
    }
    if(id == CYD){
      if(static_data_helper_.find(CYD) == static_data_helper_.end()) id = CYS;
    }
  }

  if(id == CPR || id == TPR){
    if(id == CPR){
      if(static_data_helper_.find(CPR) == static_data_helper_.end()) id = PRO;
    }
    if(id == TPR){
      if(static_data_helper_.find(TPR) == static_data_helper_.end()) id = PRO;
    }
  }

  std::map<RotamerID,std::pair<uint64_t,uint> >::const_iterator it = static_data_helper_.find(id);

  result_buffer_.clear();

  if(it == static_data_helper_.end()){
    return std::make_pair(static_cast<RotamerLibEntry*>(NULL), 0);
  } 

  uint64_t pos = it->second.first;
  for(uint i = 0; i < it->second.second; ++i){
    result_buffer_.push_back(static_data_[pos]);
    ++pos;
  }

  //shouldn't give a segfault, since the number of rotamers for a
  //particular id cannot be empty
  //normalizing of the probabilities is also not necessary, this is
  //done when making the library static
  return std::make_pair(&result_buffer_[0],result_buffer_.size());

}

void RotamerLib::MakeStatic(){

  if(readonly_){
    //it's already static...
    return;
  }
  
  //let's get an idea how many rotamers there are
  total_num_rotamers_ = 0;
  for(std::map<RotamerID, std::vector<RotamerLibEntry> >::iterator it = dynamic_data_.begin();
  	  it != dynamic_data_.end(); ++it){
  	total_num_rotamers_ += it->second.size();
  }

  if(total_num_rotamers_ == 0){
    throw promod3::Error("There are no rotamers added yet!");
  }

  //allocate memory
  static_data_ = new RotamerLibEntry[total_num_rotamers_];

  //and fill the stuff
  uint64_t actual_pos = 0;
  size_t num_rotamers;
  for(std::map<RotamerID, std::vector<RotamerLibEntry> >::iterator it = dynamic_data_.begin();
  	  it != dynamic_data_.end(); ++it){
    std::vector<RotamerLibEntry> temp = it->second;
    num_rotamers = temp.size();
  	if(num_rotamers == 0.0) continue;
    std::sort(temp.begin(), temp.end(), RotamerLibEntrySorter);

    //normalize the probabilities...
    Real summed_prob = 0.0;
    for(uint i = 0; i < num_rotamers; ++i){
      summed_prob += temp[i].probability;
    }
    if(summed_prob == 0.0){
      std::stringstream ss;
      ss << "Rotamer Library contains only zero probability entries for"; 
      ss << " a particular RotamerID!";
      throw promod3::Error(ss.str());
    }
    for(uint i = 0; i < num_rotamers; ++i){
      temp[i].probability /= summed_prob;
    }

  	memcpy(&static_data_[actual_pos], &temp[0], num_rotamers * sizeof(RotamerLibEntry));
  	static_data_helper_[it->first] = std::make_pair(actual_pos,num_rotamers);
  	actual_pos += num_rotamers;
  }

  dynamic_data_.clear();
  readonly_ = true;
}

}} //ns
