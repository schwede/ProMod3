// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/frame.hh>
#include <promod3/core/runtime_profiling.hh>
#include <ost/mol/xcs_editor.hh>

namespace promod3{ namespace sidechain{ 

void FrameResidue::ApplyOnResidue(ost::mol::ResidueHandle& res, bool consider_hydrogens) const{
  ost::mol::XCSEditor ed = res.GetEntity().EditXCS(ost::mol::BUFFERED_EDIT);
  for(uint i = 0; i < n_particles_; ++i){
    const Particle& p = particles_[i];
    const String& particle_name = p.GetName();
    if(particle_name[0] == 'H' && !consider_hydrogens) continue;
    ost::mol::AtomHandle a = res.FindAtom(particle_name);
    if (a.IsValid()){
      ed.SetAtomPos(a,p.GetPos());
    }
    else {
      ed.InsertAtom(res,p.GetName(),p.GetPos(),particle_name.substr(0,1));
    }
  }
}

Frame::Frame(const std::vector<FrameResiduePtr>& frame_residues): 
             frame_residues_(frame_residues){

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "Frame::Frame", 2);

  int overall_size = 0;
  for(std::vector<FrameResiduePtr>::const_iterator i = frame_residues_.begin();
      i != frame_residues_.end(); ++i){
    overall_size += (*i)->size();
  }
  scoring_parameters_.clear();
  residue_indices_.resize(overall_size);

  std::vector<Real> x_particle_positions(overall_size);
  std::vector<Real> y_particle_positions(overall_size);
  std::vector<Real> z_particle_positions(overall_size);
  std::vector<Real> collision_distances(overall_size);

  int particle_counter = 0;
  uint actual_index;
  geom::Vec3 pos;

  for(std::vector<FrameResiduePtr>::const_iterator i = frame_residues_.begin(); 
      i != frame_residues_.end(); ++i){
    actual_index = (*i)->GetResidueIndex();
    for(uint j = 0; j < (*i)->size(); ++j){
      Particle& p = (**i)[j];
      scoring_parameters_.push_back(p.GetSParam());
      residue_indices_[particle_counter] = actual_index;
      pos = p.GetPos();
      x_particle_positions[particle_counter] = pos[0];
      y_particle_positions[particle_counter] = pos[1];
      z_particle_positions[particle_counter] = pos[2];
      collision_distances[particle_counter] = p.GetCollisionDistance();
      ++particle_counter;
    }
  }

  collision_tree_.ResetTree(x_particle_positions,
                            y_particle_positions,
                            z_particle_positions,
                            collision_distances);
}

}}//ns
