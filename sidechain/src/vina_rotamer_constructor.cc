// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <algorithm>
#include <promod3/sidechain/vina_rotamer_constructor.hh>
#include <promod3/sidechain/vina_particle_scoring.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>
#include <ost/conop/conop.hh>


namespace promod3 { namespace sidechain {

struct VINARotamerParam : public RotamerLookupParam {

  VINARotamerParam() {

    mode = HEAVY_ATOM_MODE;
    sample_his_protonation_states = true;

    // overwrite default internal_e_prefactor from base class
    // for now it's actually exactly the same...
    // still needs some thinking/optimization
    internal_e_prefactor[ost::conop::ARG] = 1.0;
    internal_e_prefactor[ost::conop::ASN] = 1.0;
    internal_e_prefactor[ost::conop::ASP] = 1.0;
    internal_e_prefactor[ost::conop::GLN] = 1.0;
    internal_e_prefactor[ost::conop::GLU] = 1.0;
    internal_e_prefactor[ost::conop::LYS] = 1.0;
    internal_e_prefactor[ost::conop::SER] = 1.0;
    internal_e_prefactor[ost::conop::CYS] = 1.0;
    internal_e_prefactor[ost::conop::MET] = 1.0;
    internal_e_prefactor[ost::conop::TRP] = 1.0;
    internal_e_prefactor[ost::conop::TYR] = 1.0;
    internal_e_prefactor[ost::conop::THR] = 1.0;
    internal_e_prefactor[ost::conop::VAL] = 1.0;
    internal_e_prefactor[ost::conop::ILE] = 1.0;
    internal_e_prefactor[ost::conop::LEU] = 1.0;
    internal_e_prefactor[ost::conop::PRO] = 1.0;
    internal_e_prefactor[ost::conop::HIS] = 1.0;
    internal_e_prefactor[ost::conop::PHE] = 1.0;

    particle_types_[promod3::loop::ALA_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::ALA_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::ALA_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::ALA_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::ALA_CB] = C_VINAParticle;
    particle_types_[promod3::loop::ARG_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::ARG_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::ARG_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::ARG_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::ARG_CB] = C_VINAParticle;
    particle_types_[promod3::loop::ARG_CG] = C_VINAParticle;
    particle_types_[promod3::loop::ARG_CD] = C_P_VINAParticle;
    particle_types_[promod3::loop::ARG_NE] = N_D_VINAParticle;
    particle_types_[promod3::loop::ARG_CZ] = C_P_VINAParticle;
    particle_types_[promod3::loop::ARG_NH1] = N_D_VINAParticle;
    particle_types_[promod3::loop::ARG_NH2] = N_D_VINAParticle;
    particle_types_[promod3::loop::ASN_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::ASN_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::ASN_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::ASN_O] = O_A_VINAParticle; 
    particle_types_[promod3::loop::ASN_CB] = C_VINAParticle;
    particle_types_[promod3::loop::ASN_CG] = C_P_VINAParticle;
    particle_types_[promod3::loop::ASN_OD1] = O_A_VINAParticle;
    particle_types_[promod3::loop::ASN_ND2] = N_D_VINAParticle;
    particle_types_[promod3::loop::ASP_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::ASP_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::ASP_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::ASP_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::ASP_CB] = C_VINAParticle;
    particle_types_[promod3::loop::ASP_CG] = C_P_VINAParticle;
    particle_types_[promod3::loop::ASP_OD1] = O_A_VINAParticle;
    particle_types_[promod3::loop::ASP_OD2] = O_A_VINAParticle;
    particle_types_[promod3::loop::GLN_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::GLN_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLN_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLN_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::GLN_CB] = C_VINAParticle;
    particle_types_[promod3::loop::GLN_CG] = C_VINAParticle;
    particle_types_[promod3::loop::GLN_CD] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLN_OE1] = O_A_VINAParticle;
    particle_types_[promod3::loop::GLN_NE2] = N_D_VINAParticle;
    particle_types_[promod3::loop::GLU_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::GLU_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLU_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLU_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::GLU_CB] = C_VINAParticle;
    particle_types_[promod3::loop::GLU_CG] = C_VINAParticle;
    particle_types_[promod3::loop::GLU_CD] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLU_OE1] = O_A_VINAParticle;
    particle_types_[promod3::loop::GLU_OE2] = O_A_VINAParticle;
    particle_types_[promod3::loop::LYS_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::LYS_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::LYS_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::LYS_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::LYS_CB] = C_VINAParticle;
    particle_types_[promod3::loop::LYS_CG] = C_VINAParticle;
    particle_types_[promod3::loop::LYS_CD] = C_VINAParticle;
    particle_types_[promod3::loop::LYS_CE] = C_P_VINAParticle;
    particle_types_[promod3::loop::LYS_NZ] = N_D_VINAParticle;
    particle_types_[promod3::loop::SER_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::SER_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::SER_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::SER_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::SER_CB] = C_P_VINAParticle;
    particle_types_[promod3::loop::SER_OG] = O_AD_VINAParticle;
    particle_types_[promod3::loop::CYS_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::CYS_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::CYS_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::CYS_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::CYS_CB] = C_P_VINAParticle;
    particle_types_[promod3::loop::CYS_SG] = S_VINAParticle;
    particle_types_[promod3::loop::MET_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::MET_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::MET_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::MET_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::MET_CB] = C_VINAParticle;
    particle_types_[promod3::loop::MET_CG] = C_P_VINAParticle;
    particle_types_[promod3::loop::MET_SD] = S_VINAParticle;
    particle_types_[promod3::loop::MET_CE] = C_P_VINAParticle;
    particle_types_[promod3::loop::TRP_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::TRP_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::TRP_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::TRP_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::TRP_CB] = C_VINAParticle;
    particle_types_[promod3::loop::TRP_CG] = C_VINAParticle;
    particle_types_[promod3::loop::TRP_CD1] = C_P_VINAParticle;
    particle_types_[promod3::loop::TRP_CD2] = C_VINAParticle;
    particle_types_[promod3::loop::TRP_CE2] = C_P_VINAParticle;
    particle_types_[promod3::loop::TRP_NE1] = N_D_VINAParticle;
    particle_types_[promod3::loop::TRP_CE3] = C_VINAParticle;
    particle_types_[promod3::loop::TRP_CZ3] = C_VINAParticle;
    particle_types_[promod3::loop::TRP_CH2] = C_VINAParticle;
    particle_types_[promod3::loop::TRP_CZ2] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::TYR_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::TYR_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::TYR_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::TYR_CB] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_CG] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_CD1] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_CD2] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_CE1] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_CE2] = C_VINAParticle;
    particle_types_[promod3::loop::TYR_CZ] = C_P_VINAParticle;
    particle_types_[promod3::loop::TYR_OH] = O_AD_VINAParticle;
    particle_types_[promod3::loop::THR_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::THR_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::THR_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::THR_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::THR_CB] = C_P_VINAParticle;
    particle_types_[promod3::loop::THR_OG1] = O_AD_VINAParticle;
    particle_types_[promod3::loop::THR_CG2]= C_VINAParticle;
    particle_types_[promod3::loop::VAL_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::VAL_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::VAL_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::VAL_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::VAL_CB] = C_VINAParticle;
    particle_types_[promod3::loop::VAL_CG1] = C_VINAParticle;
    particle_types_[promod3::loop::VAL_CG2] = C_VINAParticle;
    particle_types_[promod3::loop::ILE_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::ILE_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::ILE_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::ILE_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::ILE_CB] = C_VINAParticle;
    particle_types_[promod3::loop::ILE_CG1] = C_VINAParticle;
    particle_types_[promod3::loop::ILE_CG2] = C_VINAParticle;
    particle_types_[promod3::loop::ILE_CD1] = C_VINAParticle;
    particle_types_[promod3::loop::LEU_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::LEU_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::LEU_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::LEU_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::LEU_CB] = C_VINAParticle;
    particle_types_[promod3::loop::LEU_CG] = C_VINAParticle;
    particle_types_[promod3::loop::LEU_CD1] = C_VINAParticle;
    particle_types_[promod3::loop::LEU_CD2] = C_VINAParticle;
    particle_types_[promod3::loop::GLY_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::GLY_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLY_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::GLY_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::PRO_N] = N_VINAParticle;
    particle_types_[promod3::loop::PRO_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::PRO_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::PRO_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::PRO_CB] = C_VINAParticle;
    particle_types_[promod3::loop::PRO_CG] = C_VINAParticle;
    particle_types_[promod3::loop::PRO_CD] = C_P_VINAParticle;
    particle_types_[promod3::loop::PHE_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::PHE_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::PHE_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::PHE_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::PHE_CB] = C_VINAParticle;
    particle_types_[promod3::loop::PHE_CG] = C_VINAParticle;
    particle_types_[promod3::loop::PHE_CD1] = C_VINAParticle;
    particle_types_[promod3::loop::PHE_CD2] = C_VINAParticle;
    particle_types_[promod3::loop::PHE_CE1] = C_VINAParticle;
    particle_types_[promod3::loop::PHE_CE2] = C_VINAParticle;
    particle_types_[promod3::loop::PHE_CZ] = C_VINAParticle;

    // hack for histidine... let's just use HSD as default...
    // explicit requests for HSD and HSE will be handled by the
    // GetParticleType function
    particle_types_[promod3::loop::HIS_N] = N_D_VINAParticle;
    particle_types_[promod3::loop::HIS_CA] = C_P_VINAParticle;
    particle_types_[promod3::loop::HIS_C] = C_P_VINAParticle;
    particle_types_[promod3::loop::HIS_O] = O_A_VINAParticle;
    particle_types_[promod3::loop::HIS_CB] = C_VINAParticle;
    particle_types_[promod3::loop::HIS_CG] = C_P_VINAParticle;
    particle_types_[promod3::loop::HIS_ND1] = N_D_VINAParticle;
    particle_types_[promod3::loop::HIS_CD2] = C_P_VINAParticle;
    particle_types_[promod3::loop::HIS_CE1] = C_P_VINAParticle;
    particle_types_[promod3::loop::HIS_NE2] = N_A_VINAParticle;
  }  

  static const VINARotamerParam& Instance() {
    static VINARotamerParam vina_param;
    return vina_param;
  }

  VINAParticleType GetParticleType(RotamerID rot_id, int atom_idx,
                                   bool n_ter) const {

    // all backbone nitrogens are hydrogen bond donors for n-termini 
    // (also proline)
    if(n_ter && atom_idx == promod3::loop::BB_N_INDEX) {
      return N_D_VINAParticle;
    }

    // this is an ugly hack for histidine
    if(rot_id == HSD && atom_idx == promod3::loop::HIS_ND1_INDEX) {
      return N_D_VINAParticle;
    }
    if(rot_id == HSD && atom_idx == promod3::loop::HIS_NE2_INDEX) {
      return N_A_VINAParticle;
    }

    if(rot_id == HSE && atom_idx == promod3::loop::HIS_ND1_INDEX) {
      return N_A_VINAParticle;
    }
    if(rot_id == HSE && atom_idx == promod3::loop::HIS_NE2_INDEX) {
      return N_D_VINAParticle;
    }

    ost::conop::AminoAcid aa = RotIDToAA(rot_id);
    promod3::loop::AminoAcidAtom aaa = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAA(aa, atom_idx);
    return particle_types_[aaa];
  }

  VINAParticleType GetParticleType(const ost::mol::AtomHandle& at) const {
    
    String ele = at.GetElement();
    std::transform(ele.begin(), ele.end(), ele.begin(), ::toupper);

    if(ele == "H" || ele == "D") {
      return INVALID_VINAParticle;
    } 

    if(ele == "C") {
      ost::mol::AtomHandleList bound_to = at.GetBondPartners();
      for(ost::mol::AtomHandleList::const_iterator it = bound_to.begin();
          it != bound_to.end(); ++it) {
        String it_ele = it->GetElement();
        std::transform(it_ele.begin(), it_ele.end(), it_ele.begin(), ::toupper);
        if(!(it_ele == "C" || it_ele == "H" || it_ele == "D")) {
          return C_P_VINAParticle;
        }
      }
      return C_VINAParticle;
    }

    if(ele == "N") {

      bool is_hbond_donor = false;
      bool is_hbond_acceptor = false;

      // check if user defined protonation states
      if(at.HasProp("is_hbond_acceptor") && at.HasProp("is_hbond_donor")) {
        is_hbond_donor = at.GetBoolProp("is_hbond_donor");
        is_hbond_acceptor = at.GetBoolProp("is_hbond_acceptor");
      } else {
        ost::mol::AtomHandleList bound_to = at.GetBondPartners();
        for(ost::mol::AtomHandleList::const_iterator it = bound_to.begin();
            it != bound_to.end(); ++it) {
          String it_ele = it->GetElement();
          std::transform(it_ele.begin(), it_ele.end(), it_ele.begin(), ::toupper);
          if(it_ele == "H" || it_ele == "D") {
            is_hbond_donor = true;
            break;
          }
        }
        // THIS IS WRONG (but works most of the time)
        // needs better logic
        if(at.GetBondCount() < 3) {
          is_hbond_acceptor = true;
        }
      }

      if(is_hbond_acceptor && is_hbond_donor) {
        return N_AD_VINAParticle;
      }
      if(is_hbond_acceptor) {
        return N_A_VINAParticle;
      }
      if(is_hbond_donor) {
        return N_D_VINAParticle;
      }
      return N_VINAParticle;
    }  

    if(ele == "O") {
        
      bool is_hbond_donor = false;
      bool is_hbond_acceptor = false;

      // check if user defined protonation states
      if(at.HasProp("is_hbond_acceptor") && at.HasProp("is_hbond_donor")) {
        is_hbond_donor = at.GetBoolProp("is_hbond_donor");
        is_hbond_acceptor = at.GetBoolProp("is_hbond_acceptor");
      } else {
        ost::mol::AtomHandleList bound_to = at.GetBondPartners();
        for(ost::mol::AtomHandleList::const_iterator it = bound_to.begin();
            it != bound_to.end(); ++it) {
          String it_ele = it->GetElement();
          std::transform(it_ele.begin(), it_ele.end(), it_ele.begin(), ::toupper);
          if(it_ele == "H" || it_ele == "D") {
            is_hbond_donor = true;
            break;
          }
        }
        // let's just assume that every oxygen can be an acceptor
        is_hbond_acceptor = true;
      }

      if(is_hbond_acceptor && is_hbond_donor) {
        return O_AD_VINAParticle;
      }
      if(is_hbond_acceptor) {
        return O_A_VINAParticle;
      }
      if(is_hbond_donor) {
        return O_D_VINAParticle;
      }
      return O_VINAParticle;
    }

    if(ele == "S") {
      return S_VINAParticle;
    }

    if(ele == "P") {
      return P_VINAParticle;
    }

    if(ele == "F") {
      return F_VINAParticle;
    }

    if(ele == "CL") {
      return Cl_VINAParticle;
    }

    if(ele == "BR") {
      return Br_VINAParticle;
    }

    if(ele == "I") {
      return I_VINAParticle;
    }

    if(ele == "MG" || ele == "MN" || ele == "ZN" || 
       ele == "CA" || ele == "FE") {
      return M_VINAParticle;
    }

    return INVALID_VINAParticle;
  }

  VINAParticleType particle_types_[promod3::loop::XXX_NUM_ATOMS];
};


VINARotamerConstructor::VINARotamerConstructor(bool cb_in_sidechain): 
                          RotamerConstructor(cb_in_sidechain,
                                             VINARotamerParam::Instance()) { }

void VINARotamerConstructor::AssignInternalEnergies(RRMRotamerGroupPtr group,
                                                    RotamerID id, 
                                                    uint residue_index,
                                                    Real phi, Real psi,
                                                    bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "VINARotamerConstructor::AssignInternalEnergies", 2);

  Real max_p = group->GetMaxP();
  for(uint i = 0; i < group->size(); ++i){
    RRMRotamerPtr r = (*group)[i];
    Real internal_e_prefactor = r->GetInternalEnergyPrefactor();
    Real probability = r->GetProbability();
    Real e = -internal_e_prefactor * std::log(probability/max_p);
    r->SetInternalEnergy(e);
  }
}


void VINARotamerConstructor::AssignInternalEnergies(FRMRotamerGroupPtr group,
                                                      RotamerID id, 
                                                      uint residue_index,
                                                      Real phi, Real psi,
                                                      bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "VINARotamerConstructor::AssignInternalEnergies", 2);

  Real max_p = group->GetMaxP();
  for(uint i = 0; i < group->size(); ++i){
    FRMRotamerPtr r = (*group)[i];
    Real internal_e_prefactor = r->GetInternalEnergyPrefactor();
    Real probability = r->GetProbability();
    Real e = -internal_e_prefactor * std::log(probability/max_p);
    r->SetInternalEnergy(e);
  }
}


FrameResiduePtr VINARotamerConstructor::ConstructFrameResidueHeuristic(
                                            const ost::mol::ResidueHandle& res,
                                            uint residue_index) {

  std::vector<Particle> particle_vec;
  this->GenerateParticleVec(res, particle_vec);
  FrameResiduePtr ptr(new FrameResidue(particle_vec, residue_index));
  return ptr;
}

RRMRotamerPtr VINARotamerConstructor::ConstructRRMRotamerHeuristic(
                                            const ost::mol::ResidueHandle& res) {

  std::vector<Particle> particle_vec;
  this->GenerateParticleVec(res, particle_vec);
  RRMRotamerPtr ptr(new RRMRotamer(particle_vec));
  return ptr;
}

FRMRotamerPtr VINARotamerConstructor::ConstructFRMRotamerHeuristic(
                                            const ost::mol::ResidueHandle& res) {

  std::vector<Particle> particle_vec;
  this->GenerateParticleVec(res, particle_vec);
  FRMRotamerPtr ptr(new FRMRotamer(particle_vec, 1.0));
  std::vector<int> subrotamer_definition;
  for(uint i = 0; i < particle_vec.size(); ++i) {
    subrotamer_definition.push_back(i);
  }
  ptr->AddSubrotamerDefinition(subrotamer_definition);
  return ptr;
}


void VINARotamerConstructor::ParametrizeParticle(int at_idx, 
                                                 bool is_hydrogen, 
                                                 Particle& particle) {

  if(is_hydrogen) {
    throw promod3::Error("Expect no hydrogen in VINARotamerConstructor!");
  }

  VINAParticleType t;
  geom::Vec3 pos;

  // let's say terminal oxygens can be both, hydrogen bond donors
  // and acceptors
  if(at_idx == -2) {
    t = O_AD_VINAParticle;
    pos = terminal_o_pos_;
  } else if (at_idx == -1){
    t = O_AD_VINAParticle;
    pos = terminal_oxt_pos_;
  } else {
    t = VINARotamerParam::Instance().GetParticleType(id_, at_idx, n_ter_);
    pos = pos_buffer_->GetPos(id_, at_idx);
  }

  VINAParam* p = new VINAParam(t, pos);
  particle.SetSParam(p);
}

void VINARotamerConstructor::GenerateParticleVec(const ost::mol::ResidueHandle& res,
                                                 std::vector<Particle>& particle_vec) const {

  particle_vec.clear();
  ost::mol::AtomHandleList at_list = res.GetAtomList();
  for(ost::mol::AtomHandleList::const_iterator at_it = at_list.begin();
      at_it != at_list.end(); ++at_it) {
    VINAParticleType t = VINARotamerParam::Instance().GetParticleType(*at_it);
    if(t==INVALID_VINAParticle) {
      continue; // hydrogens...
    }
    geom::Vec3 pos = at_it->GetPos();
    VINAParam* p = new VINAParam(t, pos);
    particle_vec.push_back(Particle(at_it->GetName(), p));
  }
}

}} //ns
