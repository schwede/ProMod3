// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer_id.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace sidechain {

RotamerID TLCToRotID(const String& tlc) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "sidechain::TLCToRotID", 2);

  switch (tlc[0]) {

    case 'A': {
      if (tlc == "ARG") return ARG;
      if (tlc == "ASN") return ASN;
      if (tlc == "ASP") return ASP;
      if (tlc == "ALA") return ALA;
      break;
    }

    case 'C': {
      if (tlc == "CYS") return CYS;
      if (tlc == "CYH") return CYH;
      if (tlc == "CYD") return CYD;
      if (tlc == "CPR") return CPR;
      break;
    }

    case 'G': {
      if (tlc == "GLU") return GLU;
      if (tlc == "GLN") return GLN;
      if (tlc == "GLY") return GLY;
      break;
    }

    case 'H': {
      if (tlc == "HIS") return HIS;
      if (tlc == "HSE") return HSE;
      if (tlc == "HSD") return HSD;
      break;
    }

    case 'I': {
      if (tlc == "ILE") return ILE;
      break;
    }

    case 'L': {
      if (tlc == "LEU") return LEU;
      if (tlc == "LYS") return LYS;
      break;
    }

    case 'M': {
      if (tlc == "MET") return MET;
      if (tlc == "MSE") return MET;
      break;
    }

    case 'P': {
      if (tlc == "PRO") return PRO;
      if (tlc == "PHE") return PHE;
      break;
    }

    case 'S': {
      if (tlc == "SER") return SER;
      break;
    }

    case 'T': {
      if (tlc == "THR") return THR;
      if (tlc == "TYR") return TYR;
      if (tlc == "TRP") return TRP;
      if (tlc == "TPR") return TPR;
      break;
    }

    case 'V': {
      if (tlc == "VAL") return VAL;
      break;
    }
  }  
  return XXX;
}

RotamerID AAToRotID(ost::conop::AminoAcid aa) {
  switch (aa) {
    case ost::conop::ALA: return ALA;
    case ost::conop::ARG: return ARG;
    case ost::conop::ASN: return ASN;
    case ost::conop::ASP: return ASP;
    case ost::conop::GLN: return GLN;
    case ost::conop::GLU: return GLU;
    case ost::conop::LYS: return LYS;
    case ost::conop::SER: return SER;
    case ost::conop::CYS: return CYS;
    case ost::conop::MET: return MET;
    case ost::conop::TRP: return TRP;
    case ost::conop::TYR: return TYR;
    case ost::conop::THR: return THR;
    case ost::conop::VAL: return VAL;
    case ost::conop::ILE: return ILE;
    case ost::conop::LEU: return LEU;
    case ost::conop::GLY: return GLY;
    case ost::conop::PRO: return PRO;
    case ost::conop::HIS: return HIS;
    case ost::conop::PHE: return PHE;
    default: return XXX;
  }
}

char RotIDToOLC(RotamerID id) {
  switch (id) {
    case ARG: return 'R'; 
    case ASN: return 'N'; 
    case ASP: return 'D';
    case GLN: return 'Q'; 
    case GLU: return 'E'; 
    case LYS: return 'K';
    case SER: return 'S'; 
    case CYS: return 'C'; 
    case CYH: return 'C';
    case CYD: return 'C'; 
    case MET: return 'M'; 
    case TRP: return 'W';
    case TYR: return 'Y'; 
    case THR: return 'T'; 
    case VAL: return 'V';
    case ILE: return 'I'; 
    case LEU: return 'L'; 
    case PRO: return 'P';
    case CPR: return 'P'; 
    case TPR: return 'P'; 
    case HSD: return 'H';
    case HSE: return 'H'; 
    case HIS: return 'H'; 
    case PHE: return 'F';
    case GLY: return 'G'; 
    case ALA: return 'A'; 
    default: return 'X';
  }
}

ost::conop::AminoAcid RotIDToAA(RotamerID id){
  switch (id) {
    case ARG: return ost::conop::ARG;
    case ASN: return ost::conop::ASN;
    case ASP: return ost::conop::ASP;
    case GLN: return ost::conop::GLN;
    case GLU: return ost::conop::GLU;
    case LYS: return ost::conop::LYS;
    case SER: return ost::conop::SER;
    case CYS: return ost::conop::CYS;
    case CYH: return ost::conop::CYS;
    case CYD: return ost::conop::CYS;
    case MET: return ost::conop::MET;
    case TRP: return ost::conop::TRP;
    case TYR: return ost::conop::TYR;
    case THR: return ost::conop::THR;
    case VAL: return ost::conop::VAL;
    case ILE: return ost::conop::ILE;
    case LEU: return ost::conop::LEU;
    case PRO: return ost::conop::PRO;
    case CPR: return ost::conop::PRO;
    case TPR: return ost::conop::PRO;
    case HIS: return ost::conop::HIS;
    case HSD: return ost::conop::HIS;
    case HSE: return ost::conop::HIS;
    case PHE: return ost::conop::PHE;
    case GLY: return ost::conop::GLY;
    case ALA: return ost::conop::ALA;
    default: return ost::conop::XXX;
  }
}

}}
