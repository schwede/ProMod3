// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/bb_dep_rotamer_lib.hh>
#include <promod3/core/check_io.hh>
#include <promod3/core/runtime_profiling.hh>

namespace{
  bool RotamerLibEntrySorter(const promod3::sidechain::RotamerLibEntry& a, 
                             const promod3::sidechain::RotamerLibEntry& b){
    return a.probability > b.probability;
  }

  inline Real BilinearInterpolation(Real f00, Real f10, Real f01, Real f11, 
                                    Real x, Real y) {
    return f00 + (f10-f00)*x + (f01-f00)*y + (f00-f10-f01+f11)*x*y;
  }

  inline Real BoundAngle(Real a){
    if(a < -Real(M_PI)) return a + 2*Real(M_PI); 
    if(a > Real(M_PI)) return a - 2*Real(M_PI);
    return a;
  }

  inline Real CircularBilinearInterpolation(Real f00, Real f10, Real f01, Real f11, 
                                            Real x, Real y) {
    
    Real interpol_one;
    Real interpol_two;
    Real diff;

    diff = BoundAngle(f10-f00);
    interpol_one = BoundAngle(f00 + diff*x);

    diff = BoundAngle(f11-f01);
    interpol_two = BoundAngle(f01 + diff*x);

    diff = BoundAngle(interpol_two-interpol_one);
    return BoundAngle(interpol_one + diff*y);
  }

  inline Real NearestNeighbourInterpolation(Real f00, Real f10, Real f01, Real f11, 
                                            Real x, Real y) {
    if(x > Real(0.5) && y > Real(0.5)) {
      return f11;
    }
    if(x > Real(0.5)) {
      return f10;
    }
    if(y > Real(0.5)) {
      return f01;
    }
    return f00;
  }
}


namespace promod3{ namespace sidechain{

BBDepRotamerLib::~BBDepRotamerLib(){
  delete [] static_data_;
}


void BBDepRotamerLib::Save(const String& filename){

  this->MakeStatic();

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, bool, uint64_t, Real, RotamerID/enum, RotamerLibEntry
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<bool>(out_stream);
  core::WriteTypeSize<uint64_t>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteTypeSize<RotamerID>(out_stream);
  core::WriteTypeSize<RotamerLibEntry>(out_stream);

  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<bool>(out_stream, true);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<RotamerID>(out_stream, ASP);

  // raw data
  out_stream.write(reinterpret_cast<char*>(&phi_bins_),sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&psi_bins_),sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&interpolate_),sizeof(bool));

  uint size = static_data_helper_.size();
  out_stream.write(reinterpret_cast<char*>(&size),sizeof(uint));

  RotamerID id;
  uint64_t current_pos;
  uint num_rotamers;

  for(std::map<RotamerID,std::pair<uint64_t,uint> >::iterator i = static_data_helper_.begin();
      i != static_data_helper_.end(); ++i){
    id = i->first;
    current_pos = i->second.first;
    num_rotamers = i->second.second;
    out_stream.write(reinterpret_cast<char*>(&id),sizeof(RotamerID));
    out_stream.write(reinterpret_cast<char*>(&current_pos),sizeof(uint64_t));
    out_stream.write(reinterpret_cast<char*>(&num_rotamers),sizeof(uint));
  }

  out_stream.write(reinterpret_cast<char*>(&total_num_rotamers_),sizeof(uint64_t));
  out_stream.write(reinterpret_cast<char*>(&static_data_[0]),total_num_rotamers_ * sizeof(RotamerLibEntry));
  out_stream.close();
}

BBDepRotamerLibPtr BBDepRotamerLib::Load(const String& filename){

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<bool>(in_stream);
  core::CheckTypeSize<uint64_t>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckTypeSize<RotamerID>(in_stream);
  core::CheckTypeSize<RotamerLibEntry>(in_stream);

  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<bool>(in_stream, true);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<RotamerID>(in_stream, ASP);

  // raw data
  uint phi_bins, psi_bins;
  in_stream.read(reinterpret_cast<char*>(&phi_bins),sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(&psi_bins),sizeof(uint));

  BBDepRotamerLibPtr p(new BBDepRotamerLib(phi_bins,psi_bins));
  p->readonly_ = true;

  bool interpolate;
  in_stream.read(reinterpret_cast<char*>(&interpolate),sizeof(bool));
  p->SetInterpolate(interpolate);

  uint size;
  in_stream.read(reinterpret_cast<char*>(&size),sizeof(uint));

  RotamerID id;
  uint64_t current_pos;
  uint num_rotamers;
  for(uint i = 0; i < size; ++i){
    in_stream.read(reinterpret_cast<char*>(&id),sizeof(RotamerID));
    in_stream.read(reinterpret_cast<char*>(&current_pos),sizeof(uint64_t));
    in_stream.read(reinterpret_cast<char*>(&num_rotamers),sizeof(uint));
    p->static_data_helper_[id] = std::make_pair(current_pos, num_rotamers);
  }

  in_stream.read(reinterpret_cast<char*>(&p->total_num_rotamers_),sizeof(uint64_t));
  p->static_data_ = new RotamerLibEntry[p->total_num_rotamers_];
  in_stream.read(reinterpret_cast<char*>(&p->static_data_[0]),p->total_num_rotamers_ * sizeof(RotamerLibEntry));

  return p;
}

void BBDepRotamerLib::SavePortable(const String& filename) {

  this->MakeStatic();

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // here: only base-type-checks needed
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);

  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<bool>(out_stream, true);
  core::WriteBaseType<uint16_t>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<float>(out_stream);

  // data
  core::ConvertBaseType<uint32_t>(out_stream, phi_bins_);
  core::ConvertBaseType<uint32_t>(out_stream, psi_bins_);
  SerializeLoadSave(out_stream);

  out_stream_.close();
}

BBDepRotamerLibPtr BBDepRotamerLib::LoadPortable(const String& filename) {
  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);

  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<bool>(in_stream, true);
  core::CheckBaseType<uint16_t>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<float>(in_stream);

  // data
  uint phi_bins, psi_bins;
  core::ConvertBaseType<uint32_t>(in_stream, phi_bins);
  core::ConvertBaseType<uint32_t>(in_stream, psi_bins);

  BBDepRotamerLibPtr p(new BBDepRotamerLib(phi_bins, psi_bins));
  p->SerializeLoadSave(in_stream);

  in_stream_.close();
  return p;
}

void BBDepRotamerLib::AddRotamer(RotamerID id, uint r1, uint r2, uint r3, uint r4,
                                 uint phi_bin, uint psi_bin, const RotamerLibEntry& rot){

  if(readonly_){
    throw promod3::Error("Cannot add rotamer to static library!");
  }

  if(phi_bin > phi_bins_){
    throw promod3::Error("Invalid phi bin when adding rotamer to library!");
  }

  if(psi_bin > psi_bins_){
    throw promod3::Error("Invalid psi bin when adding rotamer to library!");
  }

  std::vector<uint> configuration(4);
  configuration[0] = r1;
  configuration[1] = r2;
  configuration[2] = r3;
  configuration[3] = r4;

  //check whether it is the first rotamer of this identity
  std::map<RotamerID, std::vector<std::vector<std::pair<std::vector<uint>,RotamerLibEntry> > > >::iterator it = 
  dynamic_data_.find(id);
  if(it == dynamic_data_.end()){
    std::vector<std::vector<std::pair<std::vector<uint>,RotamerLibEntry> > > temp;
    temp.assign(phi_bins_*psi_bins_,std::vector<std::pair<std::vector<uint>,RotamerLibEntry> >());
    dynamic_data_[id] = temp;
    it = dynamic_data_.find(id);
  }

  uint bin = psi_bin*phi_bins_+phi_bin;

  //insert it at the proper position, sorted by the configuration
  std::vector<std::pair<std::vector<uint>,RotamerLibEntry> >::iterator rot_it = dynamic_data_[id][bin].begin();
  std::vector<std::pair<std::vector<uint>,RotamerLibEntry> >::iterator end = dynamic_data_[id][bin].end();

  for(;rot_it != end; ++rot_it){
    if(configuration < rot_it->first){
      dynamic_data_[id][bin].insert(rot_it,std::make_pair(configuration,rot));
      return;
    }
  }
  //we end up here if there are only rotamers with bigger configurations 
  //are present or when there are no other rotamers for this given 
  //identity and backbone angles 
  dynamic_data_[id][bin].push_back(std::make_pair(configuration,rot));
}

void BBDepRotamerLib::MakeStatic(){

  if(readonly_){
    //it's already static...
    return;
  }

  total_num_rotamers_ = 0;

  //we first check following things
  // -every rotamer id must contain the same number of rotamers for every dihedral pair
  // -the configurations must be unique and present for all dihedral pairs

  for(std::map<RotamerID, std::vector<std::vector<std::pair<std::vector<uint>,RotamerLibEntry> > > >::iterator
      i = dynamic_data_.begin(); i != dynamic_data_.end(); ++i){

    //lets extract the data for the first entry and compare to all other entries
    uint num_rotamers = (i->second)[0].size();
    std::set<std::vector<uint> > unique_configurations;
    for(std::vector<std::pair<std::vector<uint>,RotamerLibEntry> >::iterator j = (i->second)[0].begin();
        j != (i->second)[0].end(); ++j){
      unique_configurations.insert(j->first);
    }
    if(unique_configurations.size() != num_rotamers){
      throw promod3::Error("Cannot make library static! Only unique configurations are allowed for specific dihedral pair!");
    }


    for(std::vector<std::vector<std::pair<std::vector<uint>,RotamerLibEntry> > >::iterator j = (i->second).begin();
        j != (i->second).end(); ++j){
      //check for the size
      if(j->size() != num_rotamers){
        throw promod3::Error("Cannot make library static! Must have same number of rotamers for every dihedral pair!");
      }
      //extract all configurations
      std::set<std::vector<uint> > current_configurations;
      for(std::vector<std::pair<std::vector<uint>,RotamerLibEntry> >::iterator k = j->begin();
        k != j->end(); ++k){
        current_configurations.insert(k->first);
      }
      //check whether all necessary configurations are present
      for(std::set<std::vector<uint> >::iterator k = unique_configurations.begin();
          k != unique_configurations.end(); ++k){
        if(current_configurations.find(*k) == current_configurations.end()){
          throw promod3::Error("Cannot make library static! Inconsisten configurations observed!");
        }
      }
    }
    total_num_rotamers_ += (phi_bins_ * psi_bins_ * num_rotamers);
  }

  //All checks passed! Lets throw the dynamic data into static data!
  static_data_ = new RotamerLibEntry[total_num_rotamers_];
  static_data_helper_.clear();
  
  uint64_t current_pos = 0;
  for(std::map<RotamerID, std::vector<std::vector<std::pair<std::vector<uint>,RotamerLibEntry> > > >::iterator i = 
      dynamic_data_.begin(); i != dynamic_data_.end(); ++i){
    uint rot_per_dihedral_pair = (i->second)[0].size();
    static_data_helper_[i->first] = std::make_pair(current_pos,rot_per_dihedral_pair);
    for(uint j = 0; j < phi_bins_*psi_bins_; ++j){
      for(uint k = 0; k < rot_per_dihedral_pair; ++k){
        static_data_[current_pos] = ((i->second)[j][k]).second;
        ++current_pos;
      }
    }
  }

  readonly_=true;
  dynamic_data_.clear();
}

std::pair<RotamerLibEntry*,uint> BBDepRotamerLib::QueryLib(RotamerID id, Real phi, Real psi) const{

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BBDepRotamerLib::QueryLib", 2);

  if(!readonly_){
    throw promod3::Error("Can only read rotamers from static library!");
  }

  //In a first step, specialized residues get mapped to their parent residues
  //if they can't be found in the library
  if(id == HSE || id == HSD){
    if(id == HSE){
      if(static_data_helper_.find(HSE) == static_data_helper_.end()) id = HIS;
    }
    if(id == HSD){
      if(static_data_helper_.find(HSD) == static_data_helper_.end()) id = HIS;
    }
  }

  if(id == CYH || id == CYD){
    if(id == CYH){
      if(static_data_helper_.find(CYH) == static_data_helper_.end()) id = CYS;
    }
    if(id == CYD){
      if(static_data_helper_.find(CYD) == static_data_helper_.end()) id = CYS;
    }
  }

  if(id == CPR || id == TPR){
    if(id == CPR){
      if(static_data_helper_.find(CPR) == static_data_helper_.end()) id = PRO;
    }
    if(id == TPR){
      if(static_data_helper_.find(TPR) == static_data_helper_.end()) id = PRO;
    }
  }

  result_buffer_.clear();

  std::map<RotamerID,std::pair<uint64_t,uint> >::const_iterator it = static_data_helper_.find(id);
  if(it == static_data_helper_.end()){
    return std::make_pair(static_cast<RotamerLibEntry*>(NULL), 0);
  } 

  if(interpolate_){

    uint bin00,bin10,bin01,bin11;
    Real x,y;
    Real summed_probability = 0.0;
    
    this->GetBackboneBin(phi,psi,bin00,bin10,bin01,bin11,x,y);

    uint64_t pos00 = it->second.first + it->second.second * bin00;
    uint64_t pos10 = it->second.first + it->second.second * bin10;
    uint64_t pos01 = it->second.first + it->second.second * bin01;
    uint64_t pos11 = it->second.first + it->second.second * bin11;

    Real probability;
    Real chi1;
    Real chi2;
    Real chi3;
    Real chi4;
    Real sig1;
    Real sig2;
    Real sig3;
    Real sig4;

    for(uint i = 0; i < it->second.second; ++i){

      // Rotameric dihedral angles have clear configurations: GAUCHE_MINUS,
      // GAUCHE_PLUS, TRANS etc. Its therefore possible to interpolate
      // a dihedral angle of same configuration, even the probability in between 
      // them. However, in the non-rotameric case, this assignment is less 
      // clear. Therefore the messy interpolation.

      if(HasNonRotameric(id)) {
        probability = NearestNeighbourInterpolation(static_data_[pos00].probability,
                                                    static_data_[pos10].probability,
                                                    static_data_[pos01].probability,
                                                    static_data_[pos11].probability,
                                                    x,y);

      } else {
        probability = BilinearInterpolation(static_data_[pos00].probability,
                                            static_data_[pos10].probability,
                                            static_data_[pos01].probability,
                                            static_data_[pos11].probability,
                                            x,y);
      }

      summed_probability += probability;

      if(IsRotameric(id, 0)) {
        chi1 = CircularBilinearInterpolation(static_data_[pos00].chi1,
                                             static_data_[pos10].chi1,
                                             static_data_[pos01].chi1,
                                             static_data_[pos11].chi1,
                                             x,y);
        sig1 = BilinearInterpolation(static_data_[pos00].sig1,
                                     static_data_[pos10].sig1,
                                     static_data_[pos01].sig1,
                                     static_data_[pos11].sig1,
                                     x,y);
      } else {
        chi1 = NearestNeighbourInterpolation(static_data_[pos00].chi1,
                                             static_data_[pos10].chi1,
                                             static_data_[pos01].chi1,
                                             static_data_[pos11].chi1,
                                             x,y);
        sig1 = NearestNeighbourInterpolation(static_data_[pos00].sig1,
                                             static_data_[pos10].sig1,
                                             static_data_[pos01].sig1,
                                             static_data_[pos11].sig1,
                                             x,y);        
      }

      if(IsRotameric(id, 1)) {
        chi2 = CircularBilinearInterpolation(static_data_[pos00].chi2,
                                             static_data_[pos10].chi2,
                                             static_data_[pos01].chi2,
                                             static_data_[pos11].chi2,
                                             x,y);
        sig2 = BilinearInterpolation(static_data_[pos00].sig2,
                                     static_data_[pos10].sig2,
                                     static_data_[pos01].sig2,
                                     static_data_[pos11].sig2,
                                     x,y);
      } else {
        chi2 = NearestNeighbourInterpolation(static_data_[pos00].chi2,
                                             static_data_[pos10].chi2,
                                             static_data_[pos01].chi2,
                                             static_data_[pos11].chi2,
                                             x,y);
        sig2 = NearestNeighbourInterpolation(static_data_[pos00].sig2,
                                             static_data_[pos10].sig2,
                                             static_data_[pos01].sig2,
                                             static_data_[pos11].sig2,
                                             x,y);        
      }

      if(IsRotameric(id, 2)) {
        chi3 = CircularBilinearInterpolation(static_data_[pos00].chi3,
                                             static_data_[pos10].chi3,
                                             static_data_[pos01].chi3,
                                             static_data_[pos11].chi3,
                                             x,y);
        sig3 = BilinearInterpolation(static_data_[pos00].sig3,
                                     static_data_[pos10].sig3,
                                     static_data_[pos01].sig3,
                                     static_data_[pos11].sig3,
                                     x,y);
      } else {
        chi3 = NearestNeighbourInterpolation(static_data_[pos00].chi3,
                                             static_data_[pos10].chi3,
                                             static_data_[pos01].chi3,
                                             static_data_[pos11].chi3,
                                             x,y);
        sig3 = NearestNeighbourInterpolation(static_data_[pos00].sig3,
                                             static_data_[pos10].sig3,
                                             static_data_[pos01].sig3,
                                             static_data_[pos11].sig3,
                                             x,y);
      }

      if(IsRotameric(id, 3)) {
        chi4 = CircularBilinearInterpolation(static_data_[pos00].chi4,
                                             static_data_[pos10].chi4,
                                             static_data_[pos01].chi4,
                                             static_data_[pos11].chi4,
                                             x,y);
        sig4 = BilinearInterpolation(static_data_[pos00].sig4,
                                     static_data_[pos10].sig4,
                                     static_data_[pos01].sig4,
                                     static_data_[pos11].sig4,
                                     x,y);
      } else {
        chi4 = NearestNeighbourInterpolation(static_data_[pos00].chi4,
                                             static_data_[pos10].chi4,
                                             static_data_[pos01].chi4,
                                             static_data_[pos11].chi4,
                                             x,y);
        sig4 = NearestNeighbourInterpolation(static_data_[pos00].sig4,
                                             static_data_[pos10].sig4,
                                             static_data_[pos01].sig4,
                                             static_data_[pos11].sig4,
                                             x,y);        
      }

      ++pos00;
      ++pos01;
      ++pos10;
      ++pos11;

      if(probability == 0.0) continue;

      result_buffer_.push_back(RotamerLibEntry(probability, chi1, chi2, chi3, chi4,
                                               sig1, sig2, sig3, sig4));
    }

    for(std::vector<RotamerLibEntry>::iterator i = result_buffer_.begin();
        i != result_buffer_.end(); ++i){
      i->probability /= summed_probability;
    }
  }
  else{
    uint bin = this->GetBackboneBin(phi,psi);
    uint64_t pos = it->second.first + it->second.second * bin;

    for(uint i = 0; i < it->second.second; ++i){
      if(static_data_[pos].probability > 0.0){
        result_buffer_.push_back(static_data_[pos]);
      }
      ++pos;
    }
  }

  if(result_buffer_.empty()){
    RotamerLibEntry* null_ptr = NULL;
    return std::make_pair(null_ptr,0);
  }

  std::sort(result_buffer_.begin(),result_buffer_.end(),RotamerLibEntrySorter);
  
  return std::make_pair(&result_buffer_[0], result_buffer_.size());
}

uint BBDepRotamerLib::GetBackboneBin(Real phi, Real psi) const{

  //enforce proper ranges
  while (phi >= Real(M_PI)) phi -= 2*Real(M_PI);
  while (phi < -Real(M_PI)) phi += 2*Real(M_PI);
  while (psi >= Real(M_PI)) psi -= 2*Real(M_PI);
  while (psi < -Real(M_PI)) psi += 2*Real(M_PI);

  //bring phi and psi values from range[-pi,pi[ to range [0,2pi[
  phi += Real(M_PI);
  psi += Real(M_PI);

  // ugly round => round is in standard library c++ 11
  uint phi_bin = std::floor(phi / phi_bin_size_ + Real(0.5));
  uint psi_bin = std::floor(psi / psi_bin_size_ + Real(0.5));

  if(phi_bin >= phi_bins_) phi_bin = 0;
  if(psi_bin >= psi_bins_) psi_bin = 0;

  return psi_bin*phi_bins_ + phi_bin; 
}

void BBDepRotamerLib::GetBackboneBin(Real phi, Real psi, uint& bin00, uint& bin10, 
                                 uint& bin01, uint& bin11, Real& x, Real& y) const{

  //enforce proper ranges
  while (phi >= Real(M_PI)) phi -= 2*Real(M_PI);
  while (phi < -Real(M_PI)) phi += 2*Real(M_PI);
  while (psi >= Real(M_PI)) psi -= 2*Real(M_PI);
  while (psi < -Real(M_PI)) psi += 2*Real(M_PI);

  //bring phi and psi values from range[-pi,pi[ to range [0,2pi[
  phi += Real(M_PI);
  psi += Real(M_PI);

  uint phi_bin = std::min(static_cast<uint>(phi / phi_bin_size_), phi_bins_ - 1);
  uint psi_bin = std::min(static_cast<uint>(psi / psi_bin_size_), psi_bins_ - 1);

  bin00 = psi_bin*phi_bins_+phi_bin; 

  uint phi_bin_plus_one = phi_bin + 1;
  uint psi_bin_plus_one = psi_bin + 1;
  if(phi_bin_plus_one == phi_bins_) phi_bin_plus_one = 0;
  if(psi_bin_plus_one == psi_bins_) psi_bin_plus_one = 0;

  bin10 = psi_bin * phi_bins_ + phi_bin_plus_one; 
  bin01 = psi_bin_plus_one * phi_bins_ + phi_bin; 
  bin11 = psi_bin_plus_one * phi_bins_ + phi_bin_plus_one; 

  x = (phi - phi_bin * phi_bin_size_)/phi_bin_size_;
  y = (psi - psi_bin * psi_bin_size_)/psi_bin_size_;
}

}}//ns
