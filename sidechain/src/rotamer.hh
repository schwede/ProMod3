// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_HH
#define PROMOD3_ROTAMER_HH

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <promod3/sidechain/particle.hh>
#include <promod3/sidechain/frame.hh>
#include <promod3/loop/all_atom_positions.hh>
#include <promod3/core/message.hh>



namespace promod3 { namespace sidechain {

class RRMRotamer;
class FRMRotamer;
typedef boost::shared_ptr<RRMRotamer> RRMRotamerPtr;
typedef boost::shared_ptr<FRMRotamer> FRMRotamerPtr;


class RRMRotamer {

public:

  RRMRotamer(const std::vector<Particle>& particles, 
             Real probability = 1.0, 
             Real internal_e_prefactor = 1.0);

  void ApplyOnResidue(ost::mol::ResidueHandle& res,
                      bool consider_hydrogens = false,
                      const String& new_res_name = "") const;

  void ApplyOnResidue(loop::AllAtomPositions& all_atom, uint res_idx) const;

  FrameResiduePtr ToFrameResidue(uint res_idx) const;

  Real GetInternalEnergyPrefactor() const { return internal_e_prefactor_; }

  Real GetInternalEnergy() const { return internal_energy_; }

  Real GetFrameEnergy() const { return frame_energy_; }

  Real GetSelfEnergy() const { return internal_energy_ + frame_energy_; }

  Real GetProbability() const { return probability_; }

  void SetInternalEnergyPrefactor(Real prefactor) {
    internal_e_prefactor_ = prefactor;
  }

  void SetInternalEnergy(Real e) { internal_energy_ = e; }

  void SetFrameEnergy(Real e) { frame_energy_ = e; }

  void AddFrameEnergy(Real e) { frame_energy_ += e; }

  void SetProbability(Real p) { probability_ = p; }

  Particle& operator[](size_t index) { return particles_[index]; }

  const Particle& operator[](size_t index) const { return particles_[index]; }

  size_t size() const { return n_particles_; }

  typedef std::vector<Particle>::iterator iterator;
  typedef std::vector<Particle>::const_iterator const_iterator;
  
  iterator begin() { return particles_.begin(); }
  iterator end() { return particles_.end(); }

  const_iterator begin() const { return particles_.begin(); }
  const_iterator end() const { return particles_.end(); }


private:

  std::vector<Particle> particles_;
  size_t n_particles_;
  Real internal_energy_;
  Real frame_energy_;
  Real probability_;
  Real internal_e_prefactor_;
};


class FRMRotamer {

public:

  FRMRotamer(const std::vector<Particle>& particles, Real T, 
             Real probability = 1.0, 
             Real internal_e_prefactor = 1.0);

  void ApplyOnResidue(ost::mol::ResidueHandle& res,
                      bool consider_hydrogens = false,
                      const String& new_res_name = "") const;

  void ApplyOnResidue(loop::AllAtomPositions& all_atom, uint res_idx) const;

  FrameResiduePtr ToFrameResidue(uint res_idx) const;

  RRMRotamerPtr ToRRMRotamer() const;

  void AddSubrotamerDefinition(const std::vector<int>& definition);

  void SetActiveSubrotamer(uint idx);

  uint GetActiveSubrotamer() const { return active_subrotamer_; }

  Real GetInternalEnergyPrefactor() const { return internal_e_prefactor_; }

  Real GetInternalEnergy() const { return internal_energy_; }

  Real GetFrameEnergy() const { return frame_energy_; }

  Real GetFrameEnergy(int index) const { return frame_energies_[index]; }

  Real GetSelfEnergy() const { return internal_energy_ + frame_energy_; }

  Real GetTemperature() const { return T_; }

  Real GetProbability() const { return probability_; }

  void SetInternalEnergyPrefactor(Real prefactor) {
    internal_e_prefactor_ = prefactor;
  }

  void SetInternalEnergy(Real e) { internal_energy_ = e; }

  void SetFrameEnergy(Real e) { frame_energy_ = e; }

  void SetFrameEnergy(Real e, int index) { frame_energies_[index] = e; }

  void AddFrameEnergy(Real e) { frame_energy_ += e; }

  void AddFrameEnergy(Real e, int index) { frame_energies_[index] += e; }

  void SetTemperature(Real T) { T_ = T; }

  void SetProbability(Real p) { probability_ = p; }
  
  Particle& operator[](size_t index) { return particles_[index]; }
  
  const Particle& operator[](size_t index) const { return particles_[index]; }

  size_t size() const { return n_particles_; }

  size_t subrotamer_size() const { return subrotamer_definitions_.size(); }

  typedef std::vector<Particle>::iterator iterator;
  typedef std::vector<Particle>::const_iterator const_iterator;

  typedef std::vector<std::vector<int> >::const_iterator 
  const_subrotamer_iterator;
  typedef std::vector<std::vector<int> >::iterator subrotamer_iterator;

  typedef std::vector<int>::const_iterator 
  const_subrotamer_association_iterator;
  typedef std::vector<int>::iterator subrotamer_association_iterator;
  
  iterator begin() { return particles_.begin(); }
  iterator end() { return particles_.end(); }

  const_iterator begin() const { return particles_.begin(); }
  const_iterator end() const { return particles_.end(); }

  subrotamer_iterator subrotamers_begin() {
    return subrotamer_definitions_.begin();
  }
  subrotamer_iterator subrotamers_end() {
    return subrotamer_definitions_.end();
  }

  const_subrotamer_iterator subrotamers_begin() const {
    return subrotamer_definitions_.begin();
  }
  const_subrotamer_iterator subrotamers_end() const {
    return subrotamer_definitions_.end();
  }

  subrotamer_association_iterator subrotamer_association_begin(int i) {
    return subrotamer_associations_[i].begin();
  }
  subrotamer_association_iterator subrotamer_association_end(int i) {
    return subrotamer_associations_[i].end();
  }

  const_subrotamer_association_iterator
  subrotamer_association_begin(int i) const {
    return subrotamer_associations_[i].begin();
  }
  const_subrotamer_association_iterator
  subrotamer_association_end(int i) const {
    return subrotamer_associations_[i].end();
  }

private:

  std::vector<Particle> particles_;
  size_t n_particles_;
  std::vector<std::vector<int> > subrotamer_definitions_;
  std::vector<std::vector<int> > subrotamer_associations_;
  uint active_subrotamer_;
  Real internal_energy_;
  Real frame_energy_;
  std::vector<Real> frame_energies_;
  Real T_;
  Real probability_;
  Real internal_e_prefactor_;
};

}} // ns

#endif
