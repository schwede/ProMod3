// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_CRUNCHER_HH
#define PROMOD3_ROTAMER_CRUNCHER_HH

#include <math.h>
#include <vector>
#include <limits.h>
#include <fstream>

#include <boost/shared_ptr.hpp>

#include <ost/seq/alignment_handle.hh>
#include <ost/mol/entity_handle.hh>
#include <promod3/sidechain/rotamer_lib_entry.hh>
#include <promod3/sidechain/rotamer_density.hh>


namespace promod3{ namespace sidechain{

class RotamerCruncher;
typedef boost::shared_ptr<RotamerCruncher> RotamerCruncherPtr;


class RotamerCruncher{

public:

  RotamerCruncher();

  void Save(const String& filename);

  static RotamerCruncherPtr Load(const String& filename);

  void Initialize(const String& seqres, const std::vector<Real>& c_weights);

  void AddStructure(const ost::mol::ChainHandle& chain, const ost::seq::AlignmentHandle& aln, uint c);

  void MergeDensities();

  Real GetExtractedProbability(uint seqres_pos, Real chi1, Real chi2 = std::numeric_limits<Real>::quiet_NaN());

  void UpdateRotamerProbabilities(std::vector<RotamerLibEntryPtr>& rotamers, uint seqres_pos, Real sigma);

private:

  String seqres_;
  std::vector<Real> c_weights_;
  std::vector<int> dimensionality_;
  std::vector<std::vector<std::vector<RotamerLibEntry> > > extracted_rotamers_;
  std::vector<RotamerDensityPtr> merged_rot_densities_;
  bool readonly_;
};

}}//ns

#endif
