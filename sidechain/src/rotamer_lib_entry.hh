// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_LIB_ENTRY_HH
#define PROMOD3_ROTAMER_LIB_ENTRY_HH

#include <ost/base.hh>
#include <ost/mol/residue_handle.hh>
#include <ost/mol/residue_view.hh>
#include <ost//mol/atom_handle.hh>

#include <boost/shared_ptr.hpp>

#include <promod3/sidechain/rotamer_id.hh>
#include <promod3/core/message.hh>
#include <promod3/core/portable_binary_serializer.hh>

#include <limits>
#include <fstream>

namespace promod3{ namespace sidechain{


struct RotamerLibEntry;
typedef boost::shared_ptr<RotamerLibEntry> RotamerLibEntryPtr;


struct RotamerLibEntry{

  RotamerLibEntry(Real p, Real c1, Real c2, Real c3, Real c4, 
                  Real s1, Real s2, Real s3, Real s4): probability(p),
                                                   chi1(c1),
                                                   chi2(c2),
                                                   chi3(c3),
                                                   chi4(c4),
                                                   sig1(s1),
                                                   sig2(s2),
                                                   sig3(s3),
                                                   sig4(s4) { }

  RotamerLibEntry(): probability(0.0), 
                     chi1(std::numeric_limits<Real>::quiet_NaN()), 
                     chi2(std::numeric_limits<Real>::quiet_NaN()), 
                     chi3(std::numeric_limits<Real>::quiet_NaN()), 
                     chi4(std::numeric_limits<Real>::quiet_NaN()),
                     sig1(std::numeric_limits<Real>::quiet_NaN()), 
                     sig2(std::numeric_limits<Real>::quiet_NaN()), 
                     sig3(std::numeric_limits<Real>::quiet_NaN()), 
                     sig4(std::numeric_limits<Real>::quiet_NaN()) { }

  static RotamerLibEntryPtr FromResidue(const ost::mol::ResidueHandle& res, 
                                        RotamerID id);

  static RotamerLibEntryPtr FromResidue(const ost::mol::ResidueView& res,
                                        RotamerID id);

  static RotamerLibEntryPtr FromResidue(const ost::mol::ResidueHandle& res);

  static RotamerLibEntryPtr FromResidue(const ost::mol::ResidueView& res);

  bool IsSimilar(RotamerLibEntryPtr other, Real thresh);

  bool IsSimilar(RotamerLibEntryPtr other, Real thresh, RotamerID id);

  bool IsSimilar(RotamerLibEntryPtr other, Real thresh, const String& res_name);

  bool SimilarDihedral(RotamerLibEntryPtr other, uint dihedral_idx,
                       Real thresh);

  bool SimilarDihedral(RotamerLibEntryPtr other, uint dihedral_idx,
                       Real thresh, RotamerID id);

  bool SimilarDihedral(RotamerLibEntryPtr other, uint dihedral_idx,
                       Real thresh, const String& res_name);

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertBaseType<float>(ds, probability);
    core::ConvertBaseType<float>(ds, chi1);
    core::ConvertBaseType<float>(ds, chi2);
    core::ConvertBaseType<float>(ds, chi3);
    core::ConvertBaseType<float>(ds, chi4);
    core::ConvertBaseType<float>(ds, sig1);
    core::ConvertBaseType<float>(ds, sig2);
    core::ConvertBaseType<float>(ds, sig3);
    core::ConvertBaseType<float>(ds, sig4);
  }

  Real probability;
  Real chi1;
  Real chi2;
  Real chi3;
  Real chi4;
  Real sig1;
  Real sig2;
  Real sig3;
  Real sig4;
};


enum DihedralConfiguration {
  INVALID, TRANS, GAUCHE_MINUS, GAUCHE_PLUS, NON_ROTAMERIC
};

bool HasNonRotameric(RotamerID id);

bool IsRotameric(RotamerID id, int dihedral_idx);

DihedralConfiguration GetRotamericConfiguration(Real angle);

DihedralConfiguration GetDihedralConfiguration(const RotamerLibEntry& entry, 
                                               RotamerID id,
                                               uint dihedral_idx);

}} //ns

#endif
