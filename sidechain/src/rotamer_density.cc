// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer_density.hh>

namespace promod3{ namespace sidechain{
  
RotamerDensity1D::RotamerDensity1D(uint num_bins):num_rotamers_(0),
                                                  num_bins_(num_bins){

  if(num_bins_ > 1000){
    std::stringstream ss;
    ss << "You should think of less bins when initializing ";
    ss << "1 dimensional rotamer density! (tried with: "<<num_bins<<")";
    throw promod3::Error(ss.str());
  }

  bin_size_ = 2 * Real(M_PI) / num_bins_;
  data_ = new double[num_bins_];
  memset(data_,0,num_bins_*sizeof(double));
}

RotamerDensity1D::RotamerDensity1D(const std::vector<RotamerDensity1DPtr>& densities,
                                   const std::vector<Real>& weights): num_rotamers_(0){

  if(densities.empty()){
    std::stringstream ss;
    ss << "Require non empty density list to initialize RotamerDensity1D!";
    throw promod3::Error(ss.str());
  }

  if(densities.size() != weights.size()){
    std::stringstream ss;
    ss << "Size of weights and densities must be consistent ";
    ss << "when initializing RotamerDensity1D!";
    throw promod3::Error(ss.str());
  }

  num_bins_ = densities[0]->BinsPerDimension();
  
  for(std::vector<RotamerDensity1DPtr>::const_iterator i = densities.begin(),
      e = densities.end(); i!=e; ++i){
    if(num_bins_ != (*i)->BinsPerDimension()){
      std::stringstream ss;
      ss << "All densities must be consistent regarding num bins per dimension!";
      throw promod3::Error(ss.str());
    }
  }

  bin_size_ = 2 * Real(M_PI) / num_bins_;
  data_ = new double[num_bins_];
  memset(data_,0,num_bins_*sizeof(double));
  std::vector<Real>::const_iterator w = weights.begin();

  for(std::vector<RotamerDensity1DPtr>::const_iterator i = densities.begin(),
      e = densities.end(); i!= e; ++i,++w){

    for(uint j = 0; j < num_bins_; ++j){
      data_[j] += (*w) * (*i)->data_[j];
    }
  }

  this->Normalize();
}

void RotamerDensity1D::Normalize(){

  //sum everything up
  float sum = 0.0;
  for(uint i = 0; i < num_bins_; ++i){
    sum += data_[i];
  }

  //and normalize
  if(sum != 0.0){
    float factor = 1.0/sum;
    for(uint i = 0; i < num_bins_; ++i){
      data_[i] *= factor;
    }
  }
}

void RotamerDensity1D::AddRotamer(const RotamerLibEntry& rot){

  if(rot.chi1 != rot.chi1 || rot.sig1 != rot.sig1){
    std::stringstream ss;
    ss << "Rotamer must have chi1 and sig1 defined when adding to ";
    ss << "1 dimensional rotamer density!";
    throw promod3::Error(ss.str());
  }

  Real chi1 = rot.chi1;
  while (chi1 < -Real(M_PI)) chi1 += 2*Real(M_PI);
  while (chi1 >= Real(M_PI)) chi1 -= 2*Real(M_PI);
  chi1 += Real(M_PI);

  if (rot.sig1 == Real(0)) {
    //the easy case...
    uint bin = chi1/(2*Real(M_PI))*num_bins_;
    data_[bin] += rot.probability;
    ++num_rotamers_;
    return;
  }

  uint central_bin = chi1/(2*Real(M_PI))*num_bins_;
  //estimate the number of bins we have to move to cover at least 3 sig
  uint bins = std::ceil(3*rot.sig1/bin_size_);
  uint bin;
  Real dist;
  Real bin_center;
  Real var = rot.sig1*rot.sig1;
  Real prefac = rot.probability/std::sqrt(2*Real(M_PI)*var);

  bin = central_bin;
  bin_center = bin * bin_size_ + 0.5*bin_size_;
  dist = std::abs(bin_center - chi1);
  dist = std::min(dist, 2*Real(M_PI) - dist);
  data_[bin] += prefac * std::exp(-dist*dist/(2*var));

  for(uint i = 1; i < bins; ++i){
    bin = (central_bin + i) % num_bins_;
    bin_center = bin * bin_size_ + 0.5*bin_size_;
    dist = std::abs(bin_center - chi1);
    dist = std::min(dist, 2*Real(M_PI) - dist);
    data_[bin] += prefac * std::exp(-dist*dist/(2*var));

    bin = (central_bin - i) % num_bins_;
    bin_center = bin * bin_size_ + 0.5*bin_size_;
    dist = std::abs(bin_center - chi1);
    dist = std::min(dist, 2*Real(M_PI) - dist);
    data_[bin] += prefac * std::exp(-dist*dist/(2*var));
  }
  ++num_rotamers_;
}

void RotamerDensity1D::ApplyFactor(Real factor){
  for(uint i = 0; i < num_bins_; ++i){
    data_[i] *= factor;
  }
}

Real RotamerDensity1D::GetP(Real* chi_angles){

  Real chi1 = chi_angles[0];

  while (chi1 < -Real(M_PI)) chi1 += 2*Real(M_PI);
  while (chi1 >= Real(M_PI)) chi1 -= 2*Real(M_PI);
  chi1 += Real(M_PI);
  uint bin = chi1/(2*Real(M_PI))*num_bins_;
  return data_[bin];
}

void RotamerDensity1D::MergeProbabilities(RotamerDensityPtr other){
  
  if(this->Bins() != other->Bins()){
    throw promod3::Error("Inconsistent size when adding Rotamer Densities!");
  }

  double* other_data = other->Data();
  uint other_num_rotamers = other->GetNumRotamers();
  num_rotamers_ += other_num_rotamers;
  for(uint i = 0; i < num_bins_; ++i){
    data_[i] += other_data[i];
  }

}

void RotamerDensity1D::Clear(){
  num_rotamers_ = 0;
  memset(data_,0,num_bins_*sizeof(double));
}

RotamerDensity2D::RotamerDensity2D(uint num_bins):num_rotamers_(0),
                                                  num_bins_(num_bins){

  if(num_bins > 1000){
    std::stringstream ss;
    ss << "You should think of less bins when initializing ";
    ss << "2 dimensional rotamer density! (tried with: "<<num_bins<<")";
    throw promod3::Error(ss.str());
  }

  bin_size_ = 2 * Real(M_PI) / num_bins_;
  data_ = new double[num_bins_ * num_bins_];
  memset(data_,0,num_bins_*num_bins_*sizeof(double));
  
}

RotamerDensity2D::RotamerDensity2D(const std::vector<RotamerDensity2DPtr>& densities,
                                   const std::vector<Real>& weights): num_rotamers_(0){

  if(densities.empty()){
    std::stringstream ss;
    ss << "Require non empty density list to initialize RotamerDensity2D!";
    throw promod3::Error(ss.str());
  }

  if(densities.size() != weights.size()){
    std::stringstream ss;
    ss << "Size of weights and densities must be consistent ";
    ss << "when initializing RotamerDensity2D!";
    throw promod3::Error(ss.str());
  }

  num_bins_ = densities[0]->BinsPerDimension();
  
  for(std::vector<RotamerDensity2DPtr>::const_iterator i = densities.begin(),
      e = densities.end(); i!=e; ++i){
    if(num_bins_ != (*i)->BinsPerDimension()){
      std::stringstream ss;
      ss << "All densities must be consistent regarding num bins per dimension!";
      throw promod3::Error(ss.str());
    }
  }

  bin_size_ = 2 * Real(M_PI) / num_bins_;
  data_ = new double[num_bins_*num_bins_];
  memset(data_,0,num_bins_*num_bins_*sizeof(double));
  std::vector<Real>::const_iterator w = weights.begin();

  for(std::vector<RotamerDensity2DPtr>::const_iterator i = densities.begin(),
      e = densities.end(); i!= e; ++i,++w){

    for(uint j = 0; j < num_bins_*num_bins_; ++j){
      data_[j] += (*w) * (*i)->data_[j];
    }
  }

  this->Normalize();
}

void RotamerDensity2D::Normalize(){
  double sum = 0.0;
  for(uint i = 0; i < num_bins_*num_bins_; ++i){
    sum += data_[i];
  }
  if(sum != 0.0){
    double factor = 1.0/sum;
    for(uint i = 0; i < num_bins_*num_bins_; ++i){
      data_[i] *= factor;
    }
  }
}

void RotamerDensity2D::AddRotamer(const RotamerLibEntry& rot){

  if(rot.chi1 != rot.chi1 || rot.sig1 != rot.sig1 ||
     rot.chi2 != rot.chi2 || rot.sig2 != rot.sig2){
    std::stringstream ss;
    ss << "Rotamer must have chi1, chi2, sig1 and sig2 defined when adding "; 
    ss << "to 2 dimensional rotamer density!";
    throw promod3::Error(ss.str());
  }

  Real chi1 = rot.chi1;
  Real chi2 = rot.chi2;
  while (chi1 < -Real(M_PI)) chi1 += 2*Real(M_PI);
  while (chi1 >= Real(M_PI)) chi1 -= 2*Real(M_PI);
  while (chi2 < -Real(M_PI)) chi2 += 2*Real(M_PI);
  while (chi2 >= Real(M_PI)) chi2 -= 2*Real(M_PI);
  chi1 += Real(M_PI);
  chi2 += Real(M_PI);

  if (rot.sig1 == 0.0 && rot.sig2 == 0.0) {
    //the easy case...
    uint bin_1 = chi1/(2*Real(M_PI))*num_bins_;
    uint bin_2 = chi2/(2*Real(M_PI))*num_bins_;
    data_[bin_2*num_bins_+bin_1] += rot.probability;
    ++num_rotamers_;
    return;
  }

  if (rot.sig1 <= 0.0 || rot.sig2 <= 0.0) {
    std::stringstream ss;
    ss << "Sigmas in rotamer must both be zero or non-zero to add ";
    ss << "to 2d rotamer density! Negative sigmas are also disallowed!";
    throw promod3::Error(ss.str());
  }

  //estimate the number of bins we have to move to cover at least 3 sig
  uint bins_1 = std::max(1,static_cast<int>(std::ceil(3*rot.sig1/bin_size_)));
  uint bins_2 = std::max(1,static_cast<int>(std::ceil(3*rot.sig2/bin_size_)));
  uint start_bin_1 = (static_cast<int>(chi1/(2*Real(M_PI))*num_bins_) - bins_1) % num_bins_; 
  uint start_bin_2 = (static_cast<int>(chi2/(2*Real(M_PI))*num_bins_) - bins_2) % num_bins_; 

  uint bin;
  uint idx;
  Real dist, bin_center;
  Real var_1 = rot.sig1*rot.sig1;
  Real var_2 = rot.sig2*rot.sig2;
  Real prefac = rot.probability/std::sqrt(2*Real(M_PI)*rot.sig1*rot.sig2);

  std::vector<Real> chi1_exp;
  std::vector<uint> chi1_idx;
  for(uint i = 0; i <= 2 * bins_1; ++i){
    bin = (start_bin_1 + i) % num_bins_;
    bin_center = bin * bin_size_ + 0.5 * bin_size_;
    dist = std::abs(bin_center - chi1);;
    dist = std::min(dist, 2*Real(M_PI) - dist);
    chi1_exp.push_back(-dist*dist/(2*var_1));
    chi1_idx.push_back(bin);
  }

  Real chi2_exp;
  for(uint i = 0; i <= 2 * bins_2; ++i){
    bin = (start_bin_2 + i) % num_bins_;
    bin_center = bin * bin_size_ + 0.5 * bin_size_;
    dist = std::abs(bin_center - chi2);;
    dist = std::min(dist, 2*Real(M_PI) - dist);
    chi2_exp = -dist*dist/(2*var_2);
    idx = bin * num_bins_;
    for(uint j = 0; j <= 2*bins_1; ++j){
      data_[idx + chi1_idx[j]] += prefac * std::exp(chi1_exp[j] + chi2_exp);
    }
  }

  ++num_rotamers_;
}

void RotamerDensity2D::ApplyFactor(Real factor){
  for(uint i = 0; i < num_bins_*num_bins_; ++i){
    data_[i] *= factor;
  }
}

Real RotamerDensity2D::GetP(Real* chi_angles){
  Real chi1 = chi_angles[0];
  Real chi2 = chi_angles[1];
  while (chi1 < -Real(M_PI)) chi1 += 2*Real(M_PI);
  while (chi1 >= Real(M_PI)) chi1 -= 2*Real(M_PI);
  while (chi2 < -Real(M_PI)) chi2 += 2*Real(M_PI);
  while (chi2 >= Real(M_PI)) chi2 -= 2*Real(M_PI);
  chi1 += Real(M_PI);
  chi2 += Real(M_PI);
  uint bin = chi2/(2*Real(M_PI)) * num_bins_;
  bin *= num_bins_;
  bin += chi1/(2*Real(M_PI)) * num_bins_;
  return data_[bin];
}

void RotamerDensity2D::MergeProbabilities(RotamerDensityPtr other){
  
  if(this->Bins() != other->Bins()){
    throw promod3::Error("Inconsistent size when adding Rotamer Densities!");
  }

  double* other_data = other->Data();
  uint other_num_rotamers = other->GetNumRotamers();
  num_rotamers_ += other_num_rotamers;
  for(uint i = 0; i < num_bins_*num_bins_; ++i){
    data_[i] += other_data[i];
  }
}

void RotamerDensity2D::Clear(){
  num_rotamers_ = 0;
  memset(data_,0,num_bins_*num_bins_*sizeof(double));
}

}} //ns

