// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_VINA_PARTICLE_SCORING_HH
#define PROMOD3_VINA_PARTICLE_SCORING_HH

#include <ost/geom/mat4.hh>
#include <ost/geom/vecmat3_op.hh>
#include <promod3/sidechain/particle_scoring_base.hh>

namespace promod3{ namespace sidechain{

enum VINAParticleType {
  O_D_VINAParticle,  // oxygen hydrogen bond donor
  N_D_VINAParticle,  // nitrogen hydrogen bond donor
  O_A_VINAParticle,  // oxygen hydrogen bond acceptor
  N_A_VINAParticle,  // nitrogen hydrogen bond acceptor
  O_AD_VINAParticle, // oxygen hydrogen bond acceptor and donor
  N_AD_VINAParticle, // nitrogen hydrogen bond acceptor and donor
  O_VINAParticle,    // oxygen, neither hydrogen bond donor nor acceptor
  N_VINAParticle,    // nitrogen, neither hydrogen bond donor nor acceptor
  S_VINAParticle,    // sulfur 
  P_VINAParticle,    // posphorus
  C_P_VINAParticle,  // polar carbon 
  C_VINAParticle,    // hydrophobic carbon
  F_VINAParticle,    // fluorine
  Cl_VINAParticle,   // chlorine
  Br_VINAParticle,   // bromine
  I_VINAParticle,    // iodine
  M_VINAParticle,    // metals
  INVALID_VINAParticle
};

const Real VINAVDWRadii[18] = {
  1.7, // O_D_VINAParticle
  1.8, // N_D_VINAParticle
  1.7, // O_A_VINAParticle
  1.8, // N_A_VINAParticle
  1.7, // O_AD_VINAParticle
  1.8, // N_AD_VINAParticle
  1.7, // O_VINAParticle
  1.8, // N_VINAParticle
  2.0, // S_VINAParticle
  2.1, // P_VINAParticle
  1.9, // C_P_VINAParticle
  1.9, // C_VINAParticle
  1.5, // F_VINAParticle
  1.8, // Cl_VINAParticle
  2.0, // Br_VINAParticle
  2.2, // I_VINAParticle
  1.2, // M_VINAParticle
  0.0 // INVALID
};

void SetVINAWeightGaussian1(float w);
void SetVINAWeightGaussian2(float w);
void SetVINAWeightRepulsion(float w);
void SetVINAWeightHydrophobic(float w);
void SetVINAWeightHBond(float w);

Real GetVINAWeightGaussian1();
Real GetVINAWeightGaussian2();
Real GetVINAWeightRepulsion();
Real GetVINAWeightHydrophobic();
Real GetVINAWeightHBond();

struct VINAParam : public PScoringParam {

  VINAParam(VINAParticleType t, const geom::Vec3& pos);

  VINAParam(const geom::Vec3& pos, Real r, Real c, 
            bool hydrophobic, bool hbond_acceptor, 
            bool hbond_donor): pos_(pos), radius_(r), collision_distance_(c),
                               hydrophobic_(hydrophobic), 
                               hbond_acceptor_(hbond_acceptor),
                               hbond_donor_(hbond_donor) { }

  virtual ~VINAParam() { }

  virtual const geom::Vec3& GetPos() const { return pos_; }

  virtual Real GetCollisionDistance() const { return collision_distance_; }

  virtual void ApplyTransform(const geom::Mat4& t);

  virtual VINAParam* Copy() const;

  virtual bool EqualTo(PScoringParam* other) const;

  virtual PScoringFunction GetScoringFunction() const { return VINA; }

  geom::Vec3 pos_;
  Real radius_;
  Real collision_distance_;
  bool hydrophobic_;
  bool hbond_acceptor_;
  bool hbond_donor_;
  static Real gaussian1_weight_;
  static Real gaussian2_weight_;
  static Real repulsion_weight_;
  static Real hydrophobic_weight_;
  static Real hbond_weight_;
};

Real VINAPairwiseScore(VINAParam* p_one, VINAParam* p_two);

}} //ns

#endif
