// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/sidechain_connector.hh>
#include <promod3/core/runtime_profiling.hh>

namespace{

void ConnectARG(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle ne = res.FindAtom("NE");
  ost::mol::AtomHandle cz = res.FindAtom("CZ");
  ost::mol::AtomHandle nh1 = res.FindAtom("NH1");
  ost::mol::AtomHandle nh2 = res.FindAtom("NH2");
  ost::mol::AtomHandle he = res.FindAtom("HE");
  ost::mol::AtomHandle hh11 = res.FindAtom("HH11");
  ost::mol::AtomHandle hh12 = res.FindAtom("HH12");
  ost::mol::AtomHandle hh21 = res.FindAtom("HH21");
  ost::mol::AtomHandle hh22 = res.FindAtom("HH22");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd.IsValid()) ed.Connect(cg,cd);
  if(cd.IsValid() && ne.IsValid()) ed.Connect(cd,ne);
  if(ne.IsValid() && cz.IsValid()) ed.Connect(ne,cz);
  if(cz.IsValid() && nh1.IsValid()) ed.Connect(cz,nh1);
  if(cz.IsValid() && nh2.IsValid()) ed.Connect(cz,nh2);
  if(ne.IsValid() && he.IsValid()) ed.Connect(ne,he);
  if(nh1.IsValid() && hh11.IsValid()) ed.Connect(nh1,hh11);
  if(nh1.IsValid() && hh12.IsValid()) ed.Connect(nh1,hh12);
  if(nh2.IsValid() && hh21.IsValid()) ed.Connect(nh2,hh21);
  if(nh2.IsValid() && hh22.IsValid()) ed.Connect(nh2,hh22);
}

void ConnectASN(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle od1 = res.FindAtom("OD1");
  ost::mol::AtomHandle nd2 = res.FindAtom("ND2");
  ost::mol::AtomHandle hd21 = res.FindAtom("HD21");
  ost::mol::AtomHandle hd22 = res.FindAtom("HD22");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && od1.IsValid()) ed.Connect(cg,od1);
  if(cg.IsValid() && nd2.IsValid()) ed.Connect(cg,nd2);
  if(nd2.IsValid() && hd21.IsValid()) ed.Connect(nd2,hd21);
  if(nd2.IsValid() && hd22.IsValid()) ed.Connect(nd2,hd22);
}
       
void ConnectASP(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle od1 = res.FindAtom("OD1");
  ost::mol::AtomHandle od2 = res.FindAtom("OD2");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && od1.IsValid()) ed.Connect(cg,od1);
  if(cg.IsValid() && od2.IsValid()) ed.Connect(cg,od2);
}
      
void ConnectGLN(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle oe1 = res.FindAtom("OE1");
  ost::mol::AtomHandle ne2 = res.FindAtom("NE2");
  ost::mol::AtomHandle he21 = res.FindAtom("HE21");
  ost::mol::AtomHandle he22 = res.FindAtom("HE22");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd.IsValid()) ed.Connect(cg,cd);
  if(cd.IsValid() && oe1.IsValid()) ed.Connect(cd,oe1);
  if(cd.IsValid() && ne2.IsValid()) ed.Connect(cd,ne2);
  if(ne2.IsValid() && he21.IsValid()) ed.Connect(ne2,he21);
  if(ne2.IsValid() && he22.IsValid()) ed.Connect(ne2,he22);
}
      
void ConnectGLU(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle oe1 = res.FindAtom("OE1");
  ost::mol::AtomHandle oe2 = res.FindAtom("OE2");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd.IsValid()) ed.Connect(cg,cd);
  if(cd.IsValid() && oe1.IsValid()) ed.Connect(cd,oe1);
  if(cd.IsValid() && oe2.IsValid()) ed.Connect(cd,oe2);
}
      
void ConnectLYS(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle ce = res.FindAtom("CE");
  ost::mol::AtomHandle nz = res.FindAtom("NZ");
  ost::mol::AtomHandle hz1 = res.FindAtom("HZ1");
  ost::mol::AtomHandle hz2 = res.FindAtom("HZ2");
  ost::mol::AtomHandle hz3 = res.FindAtom("HZ3");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd.IsValid()) ed.Connect(cg,cd);
  if(cd.IsValid() && ce.IsValid()) ed.Connect(cd,ce);
  if(ce.IsValid() && nz.IsValid()) ed.Connect(ce,nz);
  if(nz.IsValid() && hz1.IsValid()) ed.Connect(nz,hz1);
  if(nz.IsValid() && hz2.IsValid()) ed.Connect(nz,hz2);
  if(nz.IsValid() && hz3.IsValid()) ed.Connect(nz,hz3);
}
      
void ConnectSER(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle og = res.FindAtom("OG");
  ost::mol::AtomHandle hg = res.FindAtom("HG");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && og.IsValid()) ed.Connect(cb,og);  
  if(og.IsValid() && hg.IsValid()) ed.Connect(og,hg);
}
      
void ConnectCYS(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle sg = res.FindAtom("SG");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && sg.IsValid()) ed.Connect(cb,sg);
}
      
void ConnectMET(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle sd = res.FindAtom("SD");
  ost::mol::AtomHandle ce = res.FindAtom("CE");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && sd.IsValid()) ed.Connect(cg,sd);
  if(sd.IsValid() && ce.IsValid()) ed.Connect(sd,ce); 
}
      
void ConnectTRP(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");
  ost::mol::AtomHandle cd2 = res.FindAtom("CD2");
  ost::mol::AtomHandle ce2 = res.FindAtom("CE2");
  ost::mol::AtomHandle ne1 = res.FindAtom("NE1");
  ost::mol::AtomHandle ce3 = res.FindAtom("CE3");
  ost::mol::AtomHandle cz3 = res.FindAtom("CZ3");
  ost::mol::AtomHandle ch2 = res.FindAtom("CH2");
  ost::mol::AtomHandle cz2 = res.FindAtom("CZ2");
  ost::mol::AtomHandle he1 = res.FindAtom("HE1");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd1.IsValid()) ed.Connect(cg,cd1);
  if(cg.IsValid() && cd2.IsValid()) ed.Connect(cg,cd2);
  if(cd1.IsValid() && ne1.IsValid()) ed.Connect(cd1,ne1);
  if(ne1.IsValid() && ce2.IsValid()) ed.Connect(ne1,ce2);
  if(ce2.IsValid() && cd2.IsValid()) ed.Connect(ce2,cd2);
  if(cd2.IsValid() && ce3.IsValid()) ed.Connect(cd2,ce3);
  if(ce3.IsValid() && cz3.IsValid()) ed.Connect(ce3,cz3);
  if(cz3.IsValid() && ch2.IsValid()) ed.Connect(cz3,ch2);
  if(ch2.IsValid() && cz2.IsValid()) ed.Connect(ch2,cz2);
  if(cz2.IsValid() && ce2.IsValid()) ed.Connect(cz2,ce2);
  if(ne1.IsValid() && he1.IsValid()) ed.Connect(ne1,he1);
}
      
void ConnectTYR(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){
 
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");
  ost::mol::AtomHandle cd2 = res.FindAtom("CD2");
  ost::mol::AtomHandle ce1 = res.FindAtom("CE1");
  ost::mol::AtomHandle ce2 = res.FindAtom("CE2");
  ost::mol::AtomHandle cz = res.FindAtom("CZ");
  ost::mol::AtomHandle oh = res.FindAtom("OH");
  ost::mol::AtomHandle hh = res.FindAtom("HH");


  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd1.IsValid()) ed.Connect(cg,cd1);
  if(cg.IsValid() && cd2.IsValid()) ed.Connect(cg,cd2);
  if(cd1.IsValid() && ce1.IsValid()) ed.Connect(cd1,ce1);
  if(cd2.IsValid() && ce2.IsValid()) ed.Connect(cd2,ce2);
  if(ce1.IsValid() && cz.IsValid()) ed.Connect(ce1,cz);
  if(ce2.IsValid() && cz.IsValid()) ed.Connect(ce2,cz);
  if(cz.IsValid() && oh.IsValid()) ed.Connect(cz,oh);
  if(oh.IsValid() && hh.IsValid()) ed.Connect(oh,hh);
}
      
void ConnectTHR(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){
  
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle og1 = res.FindAtom("OG1");
  ost::mol::AtomHandle cg2 = res.FindAtom("CG2");
  ost::mol::AtomHandle hg1 = res.FindAtom("HG1");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && og1.IsValid()) ed.Connect(cb,og1);
  if(cb.IsValid() && cg2.IsValid()) ed.Connect(cb,cg2);
  if(og1.IsValid() && hg1.IsValid()) ed.Connect(og1,hg1);
}
      
void ConnectVAL(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg1 = res.FindAtom("CG1");
  ost::mol::AtomHandle cg2 = res.FindAtom("CG2");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg1.IsValid()) ed.Connect(cb,cg1);
  if(cb.IsValid() && cg2.IsValid()) ed.Connect(cb,cg2);
}
      
void ConnectILE(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg1 = res.FindAtom("CG1");
  ost::mol::AtomHandle cg2 = res.FindAtom("CG2");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg1.IsValid()) ed.Connect(cb,cg1);
  if(cb.IsValid() && cg2.IsValid()) ed.Connect(cb,cg2);
  if(cg1.IsValid() && cd1.IsValid()) ed.Connect(cg1,cd1);
}
      
void ConnectLEU(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");
  ost::mol::AtomHandle cd2 = res.FindAtom("CD2");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd1.IsValid()) ed.Connect(cg,cd1);
  if(cg.IsValid() && cd2.IsValid()) ed.Connect(cg,cd2);
}
      
void ConnectPRO(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd.IsValid()) ed.Connect(cg,cd);
  if(cd.IsValid() && n.IsValid()) ed.Connect(cd,n);
}
      
void ConnectHIS(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle nd1 = res.FindAtom("ND1");
  ost::mol::AtomHandle cd2 = res.FindAtom("CD2");
  ost::mol::AtomHandle ce1 = res.FindAtom("CE1");
  ost::mol::AtomHandle ne2 = res.FindAtom("NE2");
  ost::mol::AtomHandle hd1 = res.FindAtom("HD1");
  ost::mol::AtomHandle he2 = res.FindAtom("HE2");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && nd1.IsValid()) ed.Connect(cg,nd1);
  if(cg.IsValid() && cd2.IsValid()) ed.Connect(cg,cd2);
  if(cd2.IsValid() && ne2.IsValid()) ed.Connect(cd2,ne2);
  if(nd1.IsValid() && ce1.IsValid()) ed.Connect(nd1,ce1);
  if(ce1.IsValid() && ne2.IsValid()) ed.Connect(ce1,ne2);
  if(nd1.IsValid() && hd1.IsValid()) ed.Connect(nd1,hd1);
  if(ne2.IsValid() && he2.IsValid()) ed.Connect(ne2,he2);
}
      
void ConnectPHE(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){

  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");
  ost::mol::AtomHandle cd2 = res.FindAtom("CD2");
  ost::mol::AtomHandle ce1 = res.FindAtom("CE1");
  ost::mol::AtomHandle ce2 = res.FindAtom("CE2");
  ost::mol::AtomHandle cz = res.FindAtom("CZ");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
  if(cb.IsValid() && cg.IsValid()) ed.Connect(cb,cg);
  if(cg.IsValid() && cd1.IsValid()) ed.Connect(cg,cd1);
  if(cg.IsValid() && cd2.IsValid()) ed.Connect(cg,cd2);
  if(cd1.IsValid() && ce1.IsValid()) ed.Connect(cd1,ce1);
  if(cd2.IsValid() && ce2.IsValid()) ed.Connect(cd2,ce2);
  if(ce1.IsValid() && cz.IsValid()) ed.Connect(ce1,cz);
  if(ce2.IsValid() && cz.IsValid()) ed.Connect(ce2,cz);
}
      
void ConnectALA(ost::mol::ResidueHandle& res, ost::mol::XCSEditor& ed){
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");

  if(ca.IsValid() && cb.IsValid()) ed.Connect(ca,cb);
}


}//ns



namespace promod3{ namespace sidechain{

void ConnectSidechain(ost::mol::ResidueHandle& res, RotamerID id) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "sidechain::ConnectSidechain", 2);

  ost::mol::XCSEditor ed = res.GetEntity().EditXCS(ost::mol::BUFFERED_EDIT);

  switch (id){
    case ARG :{     
      ConnectARG(res,ed);
      break;
    }

    case ASN :{ 
      ConnectASN(res,ed);
      break;
    }

    case ASP :{
      ConnectASP(res,ed);
      break;
    }

    case GLN :{
      ConnectGLN(res,ed);
      break;
    }

    case GLU :{
      ConnectGLU(res,ed);
      break;
    }

    case LYS :{
      ConnectLYS(res,ed);
      break;
    }

    case SER :{
      ConnectSER(res,ed);
      break;
    }

    case CYS :{
      ConnectCYS(res,ed);
      break;
    }

    case CYH :{
      ConnectCYS(res,ed);
      break;
    }

    case CYD :{
      ConnectCYS(res,ed);
      break;
    }

    case MET : {
      ConnectMET(res,ed);
      break;
    }

    case TRP :{
      ConnectTRP(res,ed);
      break;
    }

    case TYR :{
      ConnectTYR(res,ed);
      break;
    }

    case THR :{
      ConnectTHR(res,ed);
      break;
    }

    case VAL :{
      ConnectVAL(res,ed);
      break;
    }

    case ILE :{
      ConnectILE(res,ed);
      break;
    }

    case LEU :{
      ConnectLEU(res,ed);
      break;
    }

    case PRO :{
      ConnectPRO(res,ed);
      break;
    }

    case CPR :{
      ConnectPRO(res,ed);
      break;
    }

    case TPR :{
      ConnectPRO(res,ed);
      break;
    }

    case HSD :{
      ConnectHIS(res,ed);
      break;
    }

    case HSE :{
      ConnectHIS(res,ed);
      break;
    }

    case HIS :{
      ConnectHIS(res,ed);
      break;
    }

    case PHE :{
      ConnectPHE(res,ed);
      break;
    }

    case ALA :{
      ConnectALA(res,ed);
      break;
    }

    case GLY :{
      break;
    }

    default : 
      break;
  }
}









}}

