// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_PARTICLE_HH
#define PROMOD3_PARTICLE_HH

#include <boost/shared_ptr.hpp>
#include <promod3/sidechain/particle_scoring.hh>
#include <promod3/core/message.hh>

namespace promod3{ namespace sidechain{

class Particle;
typedef boost::shared_ptr<Particle> ParticlePtr;

class Particle {

public:

  Particle(): name_(""), s_param_(NULL) { }

  Particle(const String& name, PScoringParam* s_param): name_(name),
                                                        s_param_(s_param) { }

  Particle(const Particle& other) {
    name_ = other.name_;
    if(other.s_param_ != NULL) {
      s_param_ = other.s_param_->Copy();
    } else {
      s_param_ = NULL;
    }
  }

  ~Particle() {
    // particle takes the ownership of the param ptr
    if(s_param_ != NULL) {
      delete s_param_;
    }
  }

  Particle& operator= (const Particle& other) {
    if(this != &other) {      
      name_ = other.name_;        
      PScoringParam* s_param_2 = NULL;
      if(other.s_param_ != NULL) {
        s_param_2 = other.s_param_->Copy();
      }    
      if(s_param_ != NULL) {
        delete s_param_;      
      }             
      s_param_ = s_param_2;
    }
    return *this;
  }

  bool operator==(const Particle& other) const {
    if(name_ != other.name_) return false;
    if(s_param_ == NULL && other.s_param_ == NULL) return true;
    if(s_param_ != NULL) return s_param_->EqualTo(other.s_param_);
    return false;
  }

  bool operator!=(const Particle& other) const {
     return !(*this == other);
  }

  Real PairwiseScore(const Particle& other) const {
    if(s_param_ == NULL || other.s_param_ == NULL) {
      throw promod3::Error("Try to get pairwise score of particles that are "
                           "not fully initialized");
    }
    return PairwiseParticleScore(s_param_, other.s_param_);
  }

  Real GetCollisionDistance() const { 
    if(s_param_ != NULL) {
      return s_param_->GetCollisionDistance(); 
    } else {
      throw promod3::Error("Try to get scoring parameters from a particle "
                           "that is not fully initialized");
    }
  }

  const geom::Vec3& GetPos() const { 
    if(s_param_ != NULL) {
      return s_param_->GetPos(); 
    } else {
      throw promod3::Error("Try to get scoring parameters from a particle "
                           "that is not fully initialized");
    }
  } 

  const String& GetName() const { return name_; }

  void SetName(const String& name) { name_ = name; }

  PScoringParam* GetSParam() const { return s_param_; }

  void SetSParam(PScoringParam* s_param) { 
    if(s_param_ != NULL) {
      delete s_param_;
    }
    s_param_ = s_param; 
  }

  PScoringFunction GetScoringFunction() const {
    if(s_param_ != NULL) {
      return s_param_->GetScoringFunction(); 
    } else {
      throw promod3::Error("Try to get scoring function from a particle "
                           "that is not fully initialized");
    }    
  }

  void ApplyTransform(geom::Mat4& transform) {
    if(s_param_ != NULL) {
      return s_param_->ApplyTransform(transform); 
    } else {
      throw promod3::Error("Try to transform scoring parameters of a particle "
                           "that is not fully initialized");
    }    
  }

private:
  String name_;
  PScoringParam* s_param_;
};

}}//ns

#endif
