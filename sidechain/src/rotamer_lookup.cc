// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <promod3/sidechain/rotamer_lookup.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace sidechain{

RotamerLookup::RotamerLookup(bool cb_in_sidechain,
                             const RotamerLookupParam& param) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "RotamerLookup::RotamerLookup", 2);

  mode_ = param.mode;
  sample_his_protonation_states_ = param.sample_his_protonation_states;

  // ARG
  sidechain_infos_[ARG].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[ARG].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::ARG];
  sidechain_infos_[ARG].frm_t = param.frm_t[ost::conop::ARG];

  AddInfo(ARG, "CG", promod3::loop::ARG_CG_INDEX, false);
  AddInfo(ARG, "CD", promod3::loop::ARG_CD_INDEX, false);
  AddInfo(ARG, "NE", promod3::loop::ARG_NE_INDEX, false);
  AddInfo(ARG, "CZ", promod3::loop::ARG_CZ_INDEX, false);
  AddInfo(ARG, "NH1", promod3::loop::ARG_NH1_INDEX, false);
  AddInfo(ARG, "NH2", promod3::loop::ARG_NH2_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(ARG, "HE", promod3::loop::ARG_HE_INDEX, true);
    AddInfo(ARG, "HH11", promod3::loop::ARG_HH11_INDEX, true);
    AddInfo(ARG, "HH12", promod3::loop::ARG_HH12_INDEX, true);
    AddInfo(ARG, "HH21", promod3::loop::ARG_HH21_INDEX, true);
    AddInfo(ARG, "HH22", promod3::loop::ARG_HH22_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(ARG, "HB2", promod3::loop::ARG_HB2_INDEX, true);
    AddInfo(ARG, "HB3", promod3::loop::ARG_HB3_INDEX, true);
    AddInfo(ARG, "HG2", promod3::loop::ARG_HG2_INDEX, true);
    AddInfo(ARG, "HG3", promod3::loop::ARG_HG3_INDEX, true);
    AddInfo(ARG, "HD2", promod3::loop::ARG_HD2_INDEX, true);
    AddInfo(ARG, "HD3", promod3::loop::ARG_HB3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(ARG, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(ARG, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.ARG_CA_CB_prefactors);
  AddFRMRotatingParticle(ARG, 0, 0); // CG
  AddFRMRotatingParticle(ARG, 0, 1); // CD
  AddFRMRotatingParticle(ARG, 0, 2); // NE
  AddFRMRotatingParticle(ARG, 0, 3); // CZ
  AddFRMRotatingParticle(ARG, 0, 4); // NH1
  AddFRMRotatingParticle(ARG, 0, 5); // NH2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ARG, 0, 6); // HE
    AddFRMRotatingParticle(ARG, 0, 7); // HH11
    AddFRMRotatingParticle(ARG, 0, 8); // HH12
    AddFRMRotatingParticle(ARG, 0, 9); // HH21
    AddFRMRotatingParticle(ARG, 0, 10); // HH22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ARG, 0, 11); // HB2
    AddFRMRotatingParticle(ARG, 0, 12); // HB3
    AddFRMRotatingParticle(ARG, 0, 13); // HG2
    AddFRMRotatingParticle(ARG, 0, 14); // HG3
    AddFRMRotatingParticle(ARG, 0, 15);  // HD2
    AddFRMRotatingParticle(ARG, 0, 16);  // HD3
  }

  AddFRMRule(ARG, promod3::loop::BB_CB_INDEX, promod3::loop::ARG_CG_INDEX,
             param.ARG_CB_CG_prefactors);
  AddFRMFixParticle(ARG, 1, 0); // CG
  AddFRMRotatingParticle(ARG, 1, 1); // CD
  AddFRMRotatingParticle(ARG, 1, 2); // NE
  AddFRMRotatingParticle(ARG, 1, 3); // CZ
  AddFRMRotatingParticle(ARG, 1, 4); // NH1
  AddFRMRotatingParticle(ARG, 1, 5); // NH2
  
  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ARG, 1, 6); // HE
    AddFRMRotatingParticle(ARG, 1, 7); // HH11
    AddFRMRotatingParticle(ARG, 1, 8); // HH12
    AddFRMRotatingParticle(ARG, 1, 9); // HH21
    AddFRMRotatingParticle(ARG, 1, 10); // HH22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(ARG, 1, 11); // HB2
    AddFRMFixParticle(ARG, 1, 12); // HB3
    AddFRMRotatingParticle(ARG, 1, 13); // HG2
    AddFRMRotatingParticle(ARG, 1, 14); // HG3
    AddFRMRotatingParticle(ARG, 1, 15);  // HD2
    AddFRMRotatingParticle(ARG, 1, 16);  // HD3
  }

  AddFRMRule(ARG, promod3::loop::ARG_CG_INDEX, promod3::loop::ARG_CD_INDEX,
             param.ARG_CG_CD_prefactors);
  AddFRMFixParticle(ARG, 2, 0); // CG
  AddFRMFixParticle(ARG, 2, 1); // CD
  AddFRMRotatingParticle(ARG, 2, 2); // NE
  AddFRMRotatingParticle(ARG, 2, 3); // CZ
  AddFRMRotatingParticle(ARG, 2, 4); // NH1
  AddFRMRotatingParticle(ARG, 2, 5); // NH2
  
  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ARG, 2, 6); // HE
    AddFRMRotatingParticle(ARG, 2, 7); // HH11
    AddFRMRotatingParticle(ARG, 2, 8); // HH12
    AddFRMRotatingParticle(ARG, 2, 9); // HH21
    AddFRMRotatingParticle(ARG, 2, 10); // HH22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(ARG, 2, 11); // HB2
    AddFRMFixParticle(ARG, 2, 12); // HB3
    AddFRMFixParticle(ARG, 2, 13); // HG2
    AddFRMFixParticle(ARG, 2, 14); // HG3
    AddFRMRotatingParticle(ARG, 2, 15);  // HD2
    AddFRMRotatingParticle(ARG, 2, 16);  // HD3
  }


  AddFRMRule(ARG, promod3::loop::ARG_CD_INDEX, promod3::loop::ARG_NE_INDEX,
             param.ARG_CD_NE_prefactors);
  AddFRMFixParticle(ARG, 3, 0); // CG
  AddFRMFixParticle(ARG, 3, 1); // CD
  AddFRMFixParticle(ARG, 3, 2); // NE
  AddFRMRotatingParticle(ARG, 3, 3); // CZ
  AddFRMRotatingParticle(ARG, 3, 4); // NH1
  AddFRMRotatingParticle(ARG, 3, 5); // NH2
  
  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ARG, 3, 6); // HE
    AddFRMRotatingParticle(ARG, 3, 7); // HH11
    AddFRMRotatingParticle(ARG, 3, 8); // HH12
    AddFRMRotatingParticle(ARG, 3, 9); // HH21
    AddFRMRotatingParticle(ARG, 3, 10); // HH22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(ARG, 3, 11); // HB2
    AddFRMFixParticle(ARG, 3, 12); // HB3
    AddFRMFixParticle(ARG, 3, 13); // HG2
    AddFRMFixParticle(ARG, 3, 14); // HG3
    AddFRMFixParticle(ARG, 3, 15);  // HD2
    AddFRMFixParticle(ARG, 3, 16);  // HD3
  }


  if(cb_in_sidechain) {
    int cb_idx = 6;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 11;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 17;
    }
    AddFRMFixParticle(ARG, 0, cb_idx);
    AddFRMFixParticle(ARG, 1, cb_idx);
    AddFRMFixParticle(ARG, 2, cb_idx);
    AddFRMFixParticle(ARG, 3, cb_idx);
  }

  backbone_infos_[ARG].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(ARG, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(ARG, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(ARG, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(ARG, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ARG, "H", promod3::loop::ARG_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ARG, "HA", promod3::loop::ARG_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(ARG, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // ASN
  sidechain_infos_[ASN].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[ASN].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::ASN];
  sidechain_infos_[ASN].frm_t = param.frm_t[ost::conop::ASN];

  AddInfo(ASN, "CG", promod3::loop::ASN_CG_INDEX, false);
  AddInfo(ASN, "OD1", promod3::loop::ASN_OD1_INDEX, false);
  AddInfo(ASN, "ND2", promod3::loop::ASN_ND2_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(ASN, "HD21", promod3::loop::ASN_HD21_INDEX, true);
    AddInfo(ASN, "HD22", promod3::loop::ASN_HD22_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(ASN, "HB2", promod3::loop::ASN_HB2_INDEX, true);
    AddInfo(ASN, "HB3", promod3::loop::ASN_HB3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(ASN, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(ASN, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.ASN_CA_CB_prefactors);
  AddFRMRotatingParticle(ASN, 0, 0); // CG
  AddFRMRotatingParticle(ASN, 0, 1); // OD1
  AddFRMRotatingParticle(ASN, 0, 2); // ND2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ASN, 0, 3); // HD21
    AddFRMRotatingParticle(ASN, 0, 4); // HD22    
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ASN, 0, 5); // HB2
    AddFRMRotatingParticle(ASN, 0, 6); // HB3       
  }

  AddFRMRule(ASN, promod3::loop::BB_CB_INDEX, promod3::loop::ASN_CG_INDEX,
             param.ASN_CB_CG_prefactors);
  AddFRMFixParticle(ASN, 1, 0); // CG
  AddFRMRotatingParticle(ASN, 1, 1); // OD1
  AddFRMRotatingParticle(ASN, 1, 2); // ND2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ASN, 1, 3); // HD21
    AddFRMRotatingParticle(ASN, 1, 4); // HD22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(ASN, 1, 5); // HB2
    AddFRMFixParticle(ASN, 1, 6); // HB3
  }

  if(cb_in_sidechain) {
    int cb_idx = 3;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 5;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 7;
    }
    AddFRMFixParticle(ASN, 0, cb_idx);
    AddFRMFixParticle(ASN, 1, cb_idx);
  }


  backbone_infos_[ASN].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(ASN, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(ASN, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(ASN, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(ASN, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ASN, "H", promod3::loop::ASN_H_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ASN, "HA", promod3::loop::ASN_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(ASN, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // ASP
  sidechain_infos_[ASP].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[ASP].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::ASP];
  sidechain_infos_[ASP].frm_t = param.frm_t[ost::conop::ASP];

  AddInfo(ASP, "CG", promod3::loop::ASP_CG_INDEX, false);
  AddInfo(ASP, "OD1", promod3::loop::ASP_OD1_INDEX, false);
  AddInfo(ASP, "OD2", promod3::loop::ASP_OD2_INDEX, false);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(ASP, "HB2", promod3::loop::ASP_HB2_INDEX, true);
    AddInfo(ASP, "HB3", promod3::loop::ASP_HB3_INDEX, true);     
  }

  if(cb_in_sidechain) {
    AddInfo(ASP, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(ASP, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.ASP_CA_CB_prefactors);
  AddFRMRotatingParticle(ASP, 0, 0); // CG
  AddFRMRotatingParticle(ASP, 0, 1); // OD1
  AddFRMRotatingParticle(ASP, 0, 2); // OD2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ASP, 0, 3); // HB2
    AddFRMRotatingParticle(ASP, 0, 4); // HB3           
  }

  AddFRMRule(ASP, promod3::loop::BB_CB_INDEX, promod3::loop::ASP_CG_INDEX,
             param.ASP_CB_CG_prefactors);
  AddFRMFixParticle(ASP, 1, 0); // CG
  AddFRMRotatingParticle(ASP, 1, 1); // OD1
  AddFRMRotatingParticle(ASP, 1, 2); // OD2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(ASP, 1, 3); // HB2
    AddFRMFixParticle(ASP, 1, 4); // HB3                 
  }

  if(cb_in_sidechain) {
    int cb_idx = 3;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 5;
    }
    AddFRMFixParticle(ASP, 0, cb_idx);
    AddFRMFixParticle(ASP, 1, cb_idx);
  }

  backbone_infos_[ASP].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(ASP, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(ASP, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(ASP, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(ASP, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ASP, "H", promod3::loop::ASP_H_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ASP, "HA", promod3::loop::ASP_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(ASP, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // GLN
  sidechain_infos_[GLN].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[GLN].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::GLN];
  sidechain_infos_[GLN].frm_t = param.frm_t[ost::conop::GLN];

  AddInfo(GLN, "CG", promod3::loop::GLN_CG_INDEX, false);
  AddInfo(GLN, "CD", promod3::loop::GLN_CD_INDEX, false);
  AddInfo(GLN, "OE1", promod3::loop::GLN_OE1_INDEX, false);
  AddInfo(GLN, "NE2", promod3::loop::GLN_NE2_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(GLN, "HE21", promod3::loop::GLN_HE21_INDEX, true);
    AddInfo(GLN, "HE22", promod3::loop::GLN_HE22_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(GLN, "HB2", promod3::loop::GLN_HB2_INDEX, true);
    AddInfo(GLN, "HB3", promod3::loop::GLN_HB3_INDEX, true);
    AddInfo(GLN, "HG2", promod3::loop::GLN_HG2_INDEX, true);
    AddInfo(GLN, "HG3", promod3::loop::GLN_HG3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(GLN, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(GLN, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.GLN_CA_CB_prefactors);
  AddFRMRotatingParticle(GLN, 0, 0); // CG
  AddFRMRotatingParticle(GLN, 0, 1); // CD
  AddFRMRotatingParticle(GLN, 0, 2); // OE1
  AddFRMRotatingParticle(GLN, 0, 3); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(GLN, 0, 4); // HE21
    AddFRMRotatingParticle(GLN, 0, 5); // HE22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(GLN, 0, 6); // HB2
    AddFRMRotatingParticle(GLN, 0, 7); // HB3
    AddFRMRotatingParticle(GLN, 0, 8); // HG2
    AddFRMRotatingParticle(GLN, 0, 9); // HG3
  }

  AddFRMRule(GLN, promod3::loop::BB_CB_INDEX, promod3::loop::GLN_CG_INDEX,
             param.GLN_CB_CG_prefactors);
  AddFRMFixParticle(GLN, 1, 0); // CG
  AddFRMRotatingParticle(GLN, 1, 1); // CD
  AddFRMRotatingParticle(GLN, 1, 2); // OE1
  AddFRMRotatingParticle(GLN, 1, 3); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(GLN, 1, 4); // HE21
    AddFRMRotatingParticle(GLN, 1, 5); // HE22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(GLN, 1, 6); // HB2
    AddFRMFixParticle(GLN, 1, 7); // HB3
    AddFRMRotatingParticle(GLN, 1, 8); // HG2
    AddFRMRotatingParticle(GLN, 1, 9); // HG3
  }

  AddFRMRule(GLN, promod3::loop::GLN_CG_INDEX, promod3::loop::GLN_CD_INDEX,
             param.GLN_CG_CD_prefactors);
  AddFRMFixParticle(GLN, 2, 0); // CG
  AddFRMFixParticle(GLN, 2, 1); // CD
  AddFRMRotatingParticle(GLN, 2, 2); // OE1
  AddFRMRotatingParticle(GLN, 2, 3); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(GLN, 2, 4); // HE21
    AddFRMRotatingParticle(GLN, 2, 5); // HE22
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(GLN, 2, 6); // HB2
    AddFRMFixParticle(GLN, 2, 7); // HB3
    AddFRMFixParticle(GLN, 2, 8); // HG2
    AddFRMFixParticle(GLN, 2, 9); // HG3      
  }

  if(cb_in_sidechain) {
    int cb_idx = 4;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 6;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 10;
    }
    AddFRMFixParticle(GLN, 0, cb_idx);
    AddFRMFixParticle(GLN, 1, cb_idx);
    AddFRMFixParticle(GLN, 2, cb_idx);
  }


  backbone_infos_[GLN].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);

  AddBBInfo(GLN, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(GLN, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(GLN, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(GLN, "O", promod3::loop::BB_O_INDEX, false);


  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(GLN, "H", promod3::loop::GLN_H_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(GLN, "HA", promod3::loop::GLN_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(GLN, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // GLU
  sidechain_infos_[GLU].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[GLU].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::GLU];
  sidechain_infos_[GLU].frm_t = param.frm_t[ost::conop::GLU];

  AddInfo(GLU, "CG", promod3::loop::GLU_CG_INDEX, false);
  AddInfo(GLU, "CD", promod3::loop::GLU_CD_INDEX, false);
  AddInfo(GLU, "OE1", promod3::loop::GLU_OE1_INDEX, false);
  AddInfo(GLU, "OE2", promod3::loop::GLU_OE2_INDEX, false);


  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(GLU, "HB2", promod3::loop::GLU_HB2_INDEX, true);
    AddInfo(GLU, "HB3", promod3::loop::GLU_HB3_INDEX, true);
    AddInfo(GLU, "HG2", promod3::loop::GLU_HG2_INDEX, true);
    AddInfo(GLU, "HG3", promod3::loop::GLU_HG3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(GLU, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(GLU, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.GLU_CA_CB_prefactors);
  AddFRMRotatingParticle(GLU, 0, 0); // CG
  AddFRMRotatingParticle(GLU, 0, 1); // CD
  AddFRMRotatingParticle(GLU, 0, 2); // OE1
  AddFRMRotatingParticle(GLU, 0, 3); // OE2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(GLU, 0, 4); // HB2
    AddFRMRotatingParticle(GLU, 0, 5); // HB3
    AddFRMRotatingParticle(GLU, 0, 6); // HG2
    AddFRMRotatingParticle(GLU, 0, 7); // HG3
  }

  AddFRMRule(GLU, promod3::loop::BB_CB_INDEX, promod3::loop::GLU_CG_INDEX,
             param.GLU_CB_CG_prefactors);
  AddFRMFixParticle(GLU, 1, 0); // CG
  AddFRMRotatingParticle(GLU, 1, 1); // CD
  AddFRMRotatingParticle(GLU, 1, 2); // OE1
  AddFRMRotatingParticle(GLU, 1, 3); // OE2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(GLU, 1, 4); // HB2
    AddFRMFixParticle(GLU, 1, 5); // HB3
    AddFRMRotatingParticle(GLU, 1, 6); // HG2
    AddFRMRotatingParticle(GLU, 1, 7); // HG3
  }

  AddFRMRule(GLU, promod3::loop::GLU_CG_INDEX, promod3::loop::GLU_CD_INDEX,
             param.GLU_CG_CD_prefactors);
  AddFRMFixParticle(GLU, 2, 0); // CG
  AddFRMFixParticle(GLU, 2, 1); // CD
  AddFRMRotatingParticle(GLU, 2, 2); // OE1
  AddFRMRotatingParticle(GLU, 2, 3); // OE2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(GLU, 2, 4); // HB2
    AddFRMFixParticle(GLU, 2, 5); // HB3
    AddFRMFixParticle(GLU, 2, 6); // HG2
    AddFRMFixParticle(GLU, 2, 7); // HG3      
  }

  if(cb_in_sidechain) {
    int cb_idx = 4;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 8;
    }
    AddFRMFixParticle(GLU, 0, cb_idx);
    AddFRMFixParticle(GLU, 1, cb_idx);
    AddFRMFixParticle(GLU, 2, cb_idx);
  }


  backbone_infos_[GLU].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);

  AddBBInfo(GLU, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(GLU, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(GLU, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(GLU, "O", promod3::loop::BB_O_INDEX, false);


  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(GLU, "H", promod3::loop::GLU_H_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(GLU, "HA", promod3::loop::GLU_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(GLU, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // LYS
  sidechain_infos_[LYS].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[LYS].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::LYS];
  sidechain_infos_[LYS].frm_t = param.frm_t[ost::conop::LYS];

  AddInfo(LYS, "CG", promod3::loop::LYS_CG_INDEX,false);
  AddInfo(LYS, "CD", promod3::loop::LYS_CD_INDEX,false);
  AddInfo(LYS, "CE", promod3::loop::LYS_CE_INDEX,false);
  AddInfo(LYS, "NZ", promod3::loop::LYS_NZ_INDEX,false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(LYS, "HZ1", promod3::loop::LYS_HZ1_INDEX,true);
    AddInfo(LYS, "HZ2", promod3::loop::LYS_HZ2_INDEX,true);
    AddInfo(LYS, "HZ3", promod3::loop::LYS_HZ3_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(LYS, "HB2", promod3::loop::LYS_HB2_INDEX, true);
    AddInfo(LYS, "HB3", promod3::loop::LYS_HB3_INDEX, true);
    AddInfo(LYS, "HG2", promod3::loop::LYS_HG2_INDEX, true);
    AddInfo(LYS, "HG3", promod3::loop::LYS_HG3_INDEX, true);
    AddInfo(LYS, "HD2", promod3::loop::LYS_HD2_INDEX, true);
    AddInfo(LYS, "HD3", promod3::loop::LYS_HD3_INDEX, true);
    AddInfo(LYS, "HE2", promod3::loop::LYS_HE2_INDEX, true);
    AddInfo(LYS, "HE3", promod3::loop::LYS_HE3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(LYS, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(LYS, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.LYS_CA_CB_prefactors);
  AddFRMRotatingParticle(LYS, 0, 0); // CG
  AddFRMRotatingParticle(LYS, 0, 1); // CD
  AddFRMRotatingParticle(LYS, 0, 2); // CE
  AddFRMRotatingParticle(LYS, 0, 3); // NZ

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(LYS, 0, 4); // HZ1
    AddFRMRotatingParticle(LYS, 0, 5); // HZ2
    AddFRMRotatingParticle(LYS, 0, 6); // HZ3
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(LYS, 0, 7); // HB2
    AddFRMRotatingParticle(LYS, 0, 8); // HB3
    AddFRMRotatingParticle(LYS, 0, 9); // HG2
    AddFRMRotatingParticle(LYS, 0, 10); // HG3
    AddFRMRotatingParticle(LYS, 0, 11); // HD2
    AddFRMRotatingParticle(LYS, 0, 12); // HD3
    AddFRMRotatingParticle(LYS, 0, 13); // HE2
    AddFRMRotatingParticle(LYS, 0, 14); // HE3
  }

  AddFRMRule(LYS, promod3::loop::BB_CB_INDEX, promod3::loop::LYS_CG_INDEX,
             param.LYS_CB_CG_prefactors);
  AddFRMFixParticle(LYS, 1, 0); // CG
  AddFRMRotatingParticle(LYS, 1, 1); // CD
  AddFRMRotatingParticle(LYS, 1, 2); // CE
  AddFRMRotatingParticle(LYS, 1, 3); // NZ

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(LYS, 1, 4); // HZ1
    AddFRMRotatingParticle(LYS, 1, 5); // HZ2
    AddFRMRotatingParticle(LYS, 1, 6); // HZ3
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(LYS, 1, 7); // HB2
    AddFRMFixParticle(LYS, 1, 8); // HB3
    AddFRMRotatingParticle(LYS, 1, 9); // HG2
    AddFRMRotatingParticle(LYS, 1, 10); // HG3
    AddFRMRotatingParticle(LYS, 1, 11); // HD2
    AddFRMRotatingParticle(LYS, 1, 12); // HD3
    AddFRMRotatingParticle(LYS, 1, 13); // HE2
    AddFRMRotatingParticle(LYS, 1, 14); // HE3      
  }

  AddFRMRule(LYS, promod3::loop::LYS_CG_INDEX, promod3::loop::LYS_CD_INDEX,
             param.LYS_CG_CD_prefactors);
  AddFRMFixParticle(LYS, 2, 0); // CG
  AddFRMFixParticle(LYS, 2, 1); // CD
  AddFRMRotatingParticle(LYS, 2, 2); // CE
  AddFRMRotatingParticle(LYS, 2, 3); // NZ

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(LYS, 2, 4); // HZ1
    AddFRMRotatingParticle(LYS, 2, 5); // HZ2
    AddFRMRotatingParticle(LYS, 2, 6); // HZ3
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(LYS, 2, 7); // HB2
    AddFRMFixParticle(LYS, 2, 8); // HB3
    AddFRMFixParticle(LYS, 2, 9); // HG2
    AddFRMFixParticle(LYS, 2, 10); // HG3
    AddFRMRotatingParticle(LYS, 2, 11); // HD2
    AddFRMRotatingParticle(LYS, 2, 12); // HD3
    AddFRMRotatingParticle(LYS, 2, 13); // HE2
    AddFRMRotatingParticle(LYS, 2, 14); // HE3          
  }

  AddFRMRule(LYS, promod3::loop::LYS_CD_INDEX, promod3::loop::LYS_CE_INDEX,
             param.LYS_CD_CE_prefactors);
  AddFRMFixParticle(LYS, 3, 0); // CG
  AddFRMFixParticle(LYS, 3, 1); // CD
  AddFRMFixParticle(LYS, 3, 2); // CE
  AddFRMRotatingParticle(LYS, 3, 3); // NZ

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(LYS, 3, 4); // HZ1
    AddFRMRotatingParticle(LYS, 3, 5); // HZ2
    AddFRMRotatingParticle(LYS, 3, 6); // HZ3
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(LYS, 3, 7); // HB2
    AddFRMFixParticle(LYS, 3, 8); // HB3
    AddFRMFixParticle(LYS, 3, 9); // HG2
    AddFRMFixParticle(LYS, 3, 10); // HG3
    AddFRMFixParticle(LYS, 3, 11); // HD2
    AddFRMFixParticle(LYS, 3, 12); // HD3
    AddFRMRotatingParticle(LYS, 3, 13); // HE2
    AddFRMRotatingParticle(LYS, 3, 14); // HE3                
  }

  if(cb_in_sidechain) {
    int cb_idx = 4;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 7;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 15;
    }
    AddFRMFixParticle(LYS, 0, cb_idx);
    AddFRMFixParticle(LYS, 1, cb_idx);
    AddFRMFixParticle(LYS, 2, cb_idx);
    AddFRMFixParticle(LYS, 3, cb_idx);
  }

  backbone_infos_[LYS].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(LYS, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(LYS, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(LYS, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(LYS, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(LYS, "H", promod3::loop::LYS_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(LYS, "HA", promod3::loop::LYS_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(LYS, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // SER
  sidechain_infos_[SER].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[SER].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::SER];
  sidechain_infos_[SER].frm_t = param.frm_t[ost::conop::SER];

  AddInfo(SER, "OG", promod3::loop::SER_OG_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(SER, "HG", promod3::loop::SER_HG_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(SER, "HB2", promod3::loop::SER_HB2_INDEX, true);
    AddInfo(SER, "HB3", promod3::loop::SER_HB3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(SER, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddCustomHydrogenInfo(SER, promod3::loop::SER_HG_INDEX, 
                          promod3::loop::BB_CA_INDEX, 
                          promod3::loop::BB_CB_INDEX, 
                          promod3::loop::SER_OG_INDEX,
                          0.96, 1.85, 1);
  }

  AddFRMRule(SER, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.SER_CA_CB_prefactors);
  AddFRMRotatingParticle(SER, 0, 0); // OG

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(SER, 0, 1); // HG
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(SER, 0, 2); // HB2
    AddFRMRotatingParticle(SER, 0, 3); // HB3
  }

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRule(SER, promod3::loop::BB_CB_INDEX, promod3::loop::SER_OG_INDEX,
               param.SER_CB_OG_prefactors);
    AddFRMFixParticle(SER, 1, 0); // OG
    AddFRMRotatingParticle(SER, 1, 1); // HG
    
    if(mode_ == FULL_ATOMIC_MODE) {
      AddFRMFixParticle(SER, 1, 2); // HB2
      AddFRMFixParticle(SER, 1, 3); // HB3    
    }
  }

  if(cb_in_sidechain) {
    int cb_idx = 1;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 2;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 4;
    }
    AddFRMFixParticle(SER, 0, cb_idx);
    if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
      AddFRMFixParticle(SER, 1, cb_idx);
    }
  }

  backbone_infos_[SER].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(SER, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(SER, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(SER, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(SER, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(SER, "H", promod3::loop::SER_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(SER, "HA", promod3::loop::SER_HA_INDEX, true);    
  }

  if(!cb_in_sidechain) {
    AddBBInfo(SER, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // CYS
  sidechain_infos_[CYS].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[CYS].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::CYS];
  sidechain_infos_[CYS].frm_t = param.frm_t[ost::conop::CYS];

  AddInfo(CYS, "SG", promod3::loop::CYS_SG_INDEX, false);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(CYS, "HB2", promod3::loop::CYS_HB2_INDEX, true);
    AddInfo(CYS, "HB3", promod3::loop::CYS_HB3_INDEX, true);
    AddInfo(CYS, "HG", promod3::loop::CYS_HG_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(CYS, "CB", promod3::loop::BB_CB_INDEX, false);  
  }

  AddFRMRule(CYS, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.CYS_CA_CB_prefactors);
  AddFRMRotatingParticle(CYS, 0, 0); // SG

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(CYS, 0, 1); // HB2
    AddFRMRotatingParticle(CYS, 0, 2); // HB3
    AddFRMRotatingParticle(CYS, 0, 3); // HG
  }

  if(cb_in_sidechain) {
    int cb_idx = 1;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 4;
    }
    AddFRMFixParticle(CYS, 0, cb_idx);
  }

  backbone_infos_[CYS].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(CYS, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(CYS, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(CYS, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(CYS, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(CYS, "H", promod3::loop::CYS_H_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(CYS, "HA", promod3::loop::CYS_HA_INDEX,true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(CYS, "CB", promod3::loop::BB_CB_INDEX, false);   
  }


  // CYH
  sidechain_infos_[CYH] = sidechain_infos_[CYS];
  frm_rules_[CYH] = frm_rules_[CYS];
  backbone_infos_[CYH] = backbone_infos_[CYS];


  // CYD
  sidechain_infos_[CYD].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[CYD].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::CYS];
  sidechain_infos_[CYD].frm_t = param.frm_t[ost::conop::CYS];

  AddInfo(CYD, "SG", promod3::loop::CYS_SG_INDEX, false);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(CYD, "HB2", promod3::loop::CYS_HB2_INDEX, true);
    AddInfo(CYD, "HB3", promod3::loop::CYS_HB3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(CYD, "CB", promod3::loop::BB_CB_INDEX, false);  
  }

  AddFRMRule(CYD, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.CYS_CA_CB_prefactors);
  AddFRMRotatingParticle(CYD, 0, 0); // SG

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(CYD, 0, 1); // HB2
    AddFRMRotatingParticle(CYD, 0, 2); // HB3
  }

  if(cb_in_sidechain) {
    int cb_idx = 1;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 3;
    }
    AddFRMFixParticle(CYD, 0, cb_idx);
  }

  backbone_infos_[CYD].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(CYD, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(CYD, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(CYD, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(CYD, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(CYD, "H", promod3::loop::CYS_H_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(CYD, "HA", promod3::loop::CYS_HA_INDEX,true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(CYD, "CB", promod3::loop::BB_CB_INDEX, false);   
  }


  // MET
  sidechain_infos_[MET].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[MET].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::MET];
  sidechain_infos_[MET].frm_t = param.frm_t[ost::conop::MET];

  AddInfo(MET, "CG", promod3::loop::MET_CG_INDEX,false);
  AddInfo(MET, "SD", promod3::loop::MET_SD_INDEX,false);
  AddInfo(MET, "CE", promod3::loop::MET_CE_INDEX,false);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(MET, "HB2", promod3::loop::MET_HB2_INDEX, true);
    AddInfo(MET, "HB3", promod3::loop::MET_HB3_INDEX, true);
    AddInfo(MET, "HG2", promod3::loop::MET_HG2_INDEX, true);
    AddInfo(MET, "HG3", promod3::loop::MET_HG3_INDEX, true);
    AddInfo(MET, "HE1", promod3::loop::MET_HE1_INDEX, true);
    AddInfo(MET, "HE2", promod3::loop::MET_HE2_INDEX, true);
    AddInfo(MET, "HE3", promod3::loop::MET_HE3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(MET, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(MET, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.MET_CA_CB_prefactors);
  AddFRMRotatingParticle(MET, 0, 0); // CG
  AddFRMRotatingParticle(MET, 0, 1); // SD
  AddFRMRotatingParticle(MET, 0, 2); // CE

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(MET, 0, 3); // HB2
    AddFRMRotatingParticle(MET, 0, 4); // HB3
    AddFRMRotatingParticle(MET, 0, 5); // HG2
    AddFRMRotatingParticle(MET, 0, 6); // HG3
    AddFRMRotatingParticle(MET, 0, 7); // HE1
    AddFRMRotatingParticle(MET, 0, 8); // HE2
    AddFRMRotatingParticle(MET, 0, 9); // HE3
  }

  AddFRMRule(MET, promod3::loop::BB_CB_INDEX, promod3::loop::MET_CG_INDEX,
             param.MET_CB_CG_prefactors);
  AddFRMFixParticle(MET, 1, 0); // CG
  AddFRMRotatingParticle(MET, 1, 1); // SD
  AddFRMRotatingParticle(MET, 1, 2); // CE

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(MET, 1, 3); // HB2
    AddFRMFixParticle(MET, 1, 4); // HB3
    AddFRMRotatingParticle(MET, 1, 5); // HG2
    AddFRMRotatingParticle(MET, 1, 6); // HG3
    AddFRMRotatingParticle(MET, 1, 7); // HE1
    AddFRMRotatingParticle(MET, 1, 8); // HE2
    AddFRMRotatingParticle(MET, 1, 9); // HE3      
  }

  AddFRMRule(MET, promod3::loop::MET_CG_INDEX, promod3::loop::MET_SD_INDEX,
             param.MET_CG_SD_prefactors);
  AddFRMFixParticle(MET, 2, 0);
  AddFRMFixParticle(MET, 2, 1);
  AddFRMRotatingParticle(MET, 2, 2);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(MET, 2, 3); // HB2
    AddFRMFixParticle(MET, 2, 4); // HB3
    AddFRMFixParticle(MET, 2, 5); // HG2
    AddFRMFixParticle(MET, 2, 6); // HG3
    AddFRMRotatingParticle(MET, 2, 7); // HE1
    AddFRMRotatingParticle(MET, 2, 8); // HE2
    AddFRMRotatingParticle(MET, 2, 9); // HE3            
  }

  if(cb_in_sidechain) {
    int cb_idx = 3;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 10;
    }
    AddFRMFixParticle(MET, 0, cb_idx);
    AddFRMFixParticle(MET, 1, cb_idx);
    AddFRMFixParticle(MET, 2, cb_idx);
  }

  backbone_infos_[MET].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(MET, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(MET, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(MET, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(MET, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(MET, "H", promod3::loop::MET_H_INDEX, true);      
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(MET, "H", promod3::loop::MET_H_INDEX, true);      
  }

  if(!cb_in_sidechain) {
    AddBBInfo(MET, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // TRP
  sidechain_infos_[TRP].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[TRP].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::TRP];
  sidechain_infos_[TRP].frm_t = param.frm_t[ost::conop::TRP];

  AddInfo(TRP, "CG", promod3::loop::TRP_CG_INDEX,false);
  AddInfo(TRP, "CD1", promod3::loop::TRP_CD1_INDEX,false);
  AddInfo(TRP, "CD2", promod3::loop::TRP_CD2_INDEX,false);
  AddInfo(TRP, "CE2", promod3::loop::TRP_CE2_INDEX,false);
  AddInfo(TRP, "NE1", promod3::loop::TRP_NE1_INDEX,false);
  AddInfo(TRP, "CE3", promod3::loop::TRP_CE3_INDEX,false);
  AddInfo(TRP, "CZ3", promod3::loop::TRP_CZ3_INDEX,false);
  AddInfo(TRP, "CH2", promod3::loop::TRP_CH2_INDEX,false);
  AddInfo(TRP, "CZ2", promod3::loop::TRP_CZ2_INDEX,false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(TRP, "HE1", promod3::loop::TRP_HE1_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(TRP, "HB2", promod3::loop::TRP_HB2_INDEX, true);
    AddInfo(TRP, "HB3", promod3::loop::TRP_HB3_INDEX, true);
    AddInfo(TRP, "HD1", promod3::loop::TRP_HD1_INDEX, true);
    AddInfo(TRP, "HE3", promod3::loop::TRP_HE3_INDEX, true);
    AddInfo(TRP, "HZ2", promod3::loop::TRP_HZ2_INDEX, true);
    AddInfo(TRP, "HZ3", promod3::loop::TRP_HZ3_INDEX, true);
    AddInfo(TRP, "HH2", promod3::loop::TRP_HH2_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(TRP, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(TRP, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.TRP_CA_CB_prefactors);
  AddFRMRotatingParticle(TRP, 0, 0); // CG
  AddFRMRotatingParticle(TRP, 0, 1); // CD1
  AddFRMRotatingParticle(TRP, 0, 2); // CD2
  AddFRMRotatingParticle(TRP, 0, 3); // CE2
  AddFRMRotatingParticle(TRP, 0, 4); // NE1
  AddFRMRotatingParticle(TRP, 0, 5); // CE3
  AddFRMRotatingParticle(TRP, 0, 6); // CZ3
  AddFRMRotatingParticle(TRP, 0, 7); // CH2
  AddFRMRotatingParticle(TRP, 0, 8); // CZ2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(TRP, 0, 9);  // HE1    
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(TRP, 0, 10); // HB2
    AddFRMRotatingParticle(TRP, 0, 11); // HB3
    AddFRMRotatingParticle(TRP, 0, 12); // HD1
    AddFRMRotatingParticle(TRP, 0, 13); // HE3
    AddFRMRotatingParticle(TRP, 0, 14); // HZ2
    AddFRMRotatingParticle(TRP, 0, 15); // HZ3
    AddFRMRotatingParticle(TRP, 0, 16); // HH2
  }

  AddFRMRule(TRP, promod3::loop::BB_CB_INDEX, promod3::loop::TRP_CG_INDEX,
             param.TRP_CB_CG_prefactors);
  AddFRMFixParticle(TRP, 1, 0); // CG
  AddFRMRotatingParticle(TRP, 1, 1); // CD1
  AddFRMRotatingParticle(TRP, 1, 2); // CD2
  AddFRMRotatingParticle(TRP, 1, 3); // CE2
  AddFRMRotatingParticle(TRP, 1, 4); // NE1
  AddFRMRotatingParticle(TRP, 1, 5); // CE3
  AddFRMRotatingParticle(TRP, 1, 6); // CZ3
  AddFRMRotatingParticle(TRP, 1, 7); // CH2
  AddFRMRotatingParticle(TRP, 1, 8); // CZ2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(TRP, 1, 9);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(TRP, 1, 10); // HB2
    AddFRMFixParticle(TRP, 1, 11); // HB3
    AddFRMRotatingParticle(TRP, 1, 12); // HD1
    AddFRMRotatingParticle(TRP, 1, 13); // HE3
    AddFRMRotatingParticle(TRP, 1, 14); // HZ2
    AddFRMRotatingParticle(TRP, 1, 15); // HZ3
    AddFRMRotatingParticle(TRP, 1, 16); // HH2      
  }

  if(cb_in_sidechain) {
    int cb_idx = 9;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 10;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 17;
    }
    AddFRMFixParticle(TRP, 0, cb_idx);
    AddFRMFixParticle(TRP, 1, cb_idx);
  }

  backbone_infos_[TRP].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(TRP, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(TRP, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(TRP, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(TRP, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(TRP, "H", promod3::loop::TRP_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(TRP, "HA", promod3::loop::TRP_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(TRP, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // TYR
  sidechain_infos_[TYR].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[TYR].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::TYR];
  sidechain_infos_[TYR].frm_t = param.frm_t[ost::conop::TYR];

  AddInfo(TYR, "CG", promod3::loop::TYR_CG_INDEX, false);
  AddInfo(TYR, "CD1", promod3::loop::TYR_CD1_INDEX, false);
  AddInfo(TYR, "CD2", promod3::loop::TYR_CD2_INDEX, false);
  AddInfo(TYR, "CE1", promod3::loop::TYR_CE1_INDEX, false);
  AddInfo(TYR, "CE2", promod3::loop::TYR_CE2_INDEX, false);
  AddInfo(TYR, "CZ", promod3::loop::TYR_CZ_INDEX, false);
  AddInfo(TYR, "OH", promod3::loop::TYR_OH_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(TYR, "HH", promod3::loop::TYR_HH_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(TYR, "HB2", promod3::loop::TYR_HB2_INDEX, true);
    AddInfo(TYR, "HB3", promod3::loop::TYR_HB3_INDEX, true);
    AddInfo(TYR, "HD1", promod3::loop::TYR_HD1_INDEX, true);
    AddInfo(TYR, "HD2", promod3::loop::TYR_HD2_INDEX, true);
    AddInfo(TYR, "HE1", promod3::loop::TYR_HE1_INDEX, true);
    AddInfo(TYR, "HE2", promod3::loop::TYR_HE2_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(TYR, "CB", promod3::loop::BB_CB_INDEX, false);
  }
  
  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddCustomHydrogenInfo(TYR, promod3::loop::TYR_HH_INDEX, 
                          promod3::loop::TYR_CE1_INDEX, 
                          promod3::loop::TYR_CZ_INDEX, 
                          promod3::loop::TYR_OH_INDEX,
                          0.96, 1.885, 2);
  }

  AddFRMRule(TYR, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.TYR_CA_CB_prefactors);
  AddFRMRotatingParticle(TYR, 0, 0); // CG
  AddFRMRotatingParticle(TYR, 0, 1); // CD1
  AddFRMRotatingParticle(TYR, 0, 2); // CD2
  AddFRMRotatingParticle(TYR, 0, 3); // CE1
  AddFRMRotatingParticle(TYR, 0, 4); // CE2
  AddFRMRotatingParticle(TYR, 0, 5); // CZ
  AddFRMRotatingParticle(TYR, 0, 6); // OH

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(TYR, 0, 7); // HH    
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(TYR, 0, 8); // HB2
    AddFRMRotatingParticle(TYR, 0, 9); // HB3
    AddFRMRotatingParticle(TYR, 0, 10); // HD1
    AddFRMRotatingParticle(TYR, 0, 11); // HD2
    AddFRMRotatingParticle(TYR, 0, 12); // HE1
    AddFRMRotatingParticle(TYR, 0, 13); // HE2
  }

  AddFRMRule(TYR, promod3::loop::BB_CB_INDEX, promod3::loop::TYR_CG_INDEX,
             param.TYR_CB_CG_prefactors);
  AddFRMFixParticle(TYR, 1, 0); // CG
  AddFRMRotatingParticle(TYR, 1, 1); // CD1
  AddFRMRotatingParticle(TYR, 1, 2); // CD2
  AddFRMRotatingParticle(TYR, 1, 3); // CE1
  AddFRMRotatingParticle(TYR, 1, 4); // CE2
  AddFRMRotatingParticle(TYR, 1, 5); // CZ
  AddFRMRotatingParticle(TYR, 1, 6); // OH

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(TYR, 1, 7); // HH
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(TYR, 1, 8); // HB2
    AddFRMFixParticle(TYR, 1, 9); // HB3
    AddFRMRotatingParticle(TYR, 1, 10); // HD1
    AddFRMRotatingParticle(TYR, 1, 11); // HD2
    AddFRMRotatingParticle(TYR, 1, 12); // HE1
    AddFRMRotatingParticle(TYR, 1, 13); // HE2

  }

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRule(TYR, promod3::loop::TYR_CZ_INDEX, promod3::loop::TYR_OH_INDEX,
               param.TYR_CZ_OH_prefactors);
    AddFRMFixParticle(TYR, 2, 0); // CG
    AddFRMFixParticle(TYR, 2, 1); // CD1
    AddFRMFixParticle(TYR, 2, 2); // CD2
    AddFRMFixParticle(TYR, 2, 3); // CE1
    AddFRMFixParticle(TYR, 2, 4); // CE2
    AddFRMFixParticle(TYR, 2, 5); // CZ
    AddFRMFixParticle(TYR, 2, 6); // OH

    AddFRMRotatingParticle(TYR, 2, 7);

    if(mode_ == FULL_ATOMIC_MODE) {
      AddFRMFixParticle(TYR, 2, 8); // HB2
      AddFRMFixParticle(TYR, 2, 9); // HB3
      AddFRMFixParticle(TYR, 2, 10); // HD1
      AddFRMFixParticle(TYR, 2, 11); // HD2
      AddFRMFixParticle(TYR, 2, 12); // HE1
      AddFRMFixParticle(TYR, 2, 13); // HE2      
    }
  }


  if(cb_in_sidechain) {
    int cb_idx = 7;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 8;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 14;
    }
    AddFRMFixParticle(TYR, 0, cb_idx);
    AddFRMFixParticle(TYR, 1, cb_idx);
    if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
      AddFRMFixParticle(TYR, 2, cb_idx);
    }
  }

  backbone_infos_[TYR].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(TYR, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(TYR, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(TYR, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(TYR, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(TYR, "H", promod3::loop::TYR_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(TYR, "HA", promod3::loop::TYR_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(TYR, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // THR
  sidechain_infos_[THR].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[THR].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::THR];
  sidechain_infos_[THR].frm_t = param.frm_t[ost::conop::THR];

  AddInfo(THR, "OG1", promod3::loop::THR_OG1_INDEX, false);
  AddInfo(THR, "CG2", promod3::loop::THR_CG2_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(THR, "HG1", promod3::loop::THR_HG1_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(THR, "HB", promod3::loop::THR_HB_INDEX, true);
    AddInfo(THR, "HG21", promod3::loop::THR_HG21_INDEX, true);
    AddInfo(THR, "HG22", promod3::loop::THR_HG22_INDEX, true);
    AddInfo(THR, "HG23", promod3::loop::THR_HG23_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(THR, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddCustomHydrogenInfo(THR, promod3::loop::THR_HG1_INDEX, 
                          promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX, 
                          promod3::loop::THR_OG1_INDEX,
                          0.96, 1.85, 1);
  }

  AddFRMRule(THR, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.THR_CA_CB_prefactors);
  AddFRMRotatingParticle(THR, 0, 0); // OG1
  AddFRMRotatingParticle(THR, 0, 1); // CG2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(THR, 0, 2); // HG1
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(THR, 0, 3); // HB  
    AddFRMRotatingParticle(THR, 0, 4); // HG21
    AddFRMRotatingParticle(THR, 0, 5); // HG22  
    AddFRMRotatingParticle(THR, 0, 6); // HG23  
  }

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRule(THR, promod3::loop::BB_CB_INDEX, promod3::loop::THR_OG1_INDEX,
               param.THR_CB_OG1_prefactors);
    AddFRMFixParticle(THR, 1, 0); // OG1
    AddFRMFixParticle(THR, 1, 1); //CG2
    AddFRMRotatingParticle(THR, 1, 2); // HG1

    if(mode_ == FULL_ATOMIC_MODE) {
      AddFRMFixParticle(THR, 1, 3); // HB
      AddFRMFixParticle(THR, 1, 4); // HG21
      AddFRMFixParticle(THR, 1, 5); // HG22
      AddFRMFixParticle(THR, 1, 6); // HG23
    }
  }

  if(cb_in_sidechain) {
    int cb_idx = 2;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 3;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 7;
    }
    AddFRMFixParticle(THR, 0, cb_idx);
    if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
      AddFRMFixParticle(THR, 1, cb_idx);
    }
  }

  backbone_infos_[THR].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(THR, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(THR, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(THR, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(THR, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(THR, "H", promod3::loop::THR_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(THR, "HA", promod3::loop::THR_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(THR, "CB", promod3::loop::BB_CB_INDEX, false); 
  }


  // VAL
  sidechain_infos_[VAL].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[VAL].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::VAL];
  sidechain_infos_[VAL].frm_t = param.frm_t[ost::conop::VAL];

  AddInfo(VAL, "CG1", promod3::loop::VAL_CG1_INDEX, false);
  AddInfo(VAL, "CG2", promod3::loop::VAL_CG2_INDEX, false);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(VAL, "HB", promod3::loop::VAL_HB_INDEX, true);
    AddInfo(VAL, "HG11", promod3::loop::VAL_HG11_INDEX, true);
    AddInfo(VAL, "HG12", promod3::loop::VAL_HG12_INDEX, true);
    AddInfo(VAL, "HG13", promod3::loop::VAL_HG13_INDEX, true);
    AddInfo(VAL, "HG21", promod3::loop::VAL_HG21_INDEX, true);
    AddInfo(VAL, "HG22", promod3::loop::VAL_HG22_INDEX, true);
    AddInfo(VAL, "HG23", promod3::loop::VAL_HG23_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(VAL, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(VAL, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.VAL_CA_CB_prefactors);
  AddFRMRotatingParticle(VAL, 0, 0); // CG1
  AddFRMRotatingParticle(VAL, 0, 1); // CG2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(VAL, 0, 2); // HB
    AddFRMRotatingParticle(VAL, 0, 3); // HG11
    AddFRMRotatingParticle(VAL, 0, 4); // HG12
    AddFRMRotatingParticle(VAL, 0, 5); // HG13
    AddFRMRotatingParticle(VAL, 0, 6); // HG21
    AddFRMRotatingParticle(VAL, 0, 7); // HG22
    AddFRMRotatingParticle(VAL, 0, 8); // HG23
  }

  if(cb_in_sidechain) {
    int cb_idx = 2;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 9;
    }
    AddFRMFixParticle(VAL, 0, cb_idx);
  }

  backbone_infos_[VAL].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(VAL, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(VAL, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(VAL, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(VAL, "O", promod3::loop::BB_O_INDEX, false);
  
  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(VAL, "H", promod3::loop::VAL_H_INDEX, true);      
  }
  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(VAL, "HA", promod3::loop::VAL_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(VAL, "CB", promod3::loop::BB_CB_INDEX, false); 
  }


  // ILE
  sidechain_infos_[ILE].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[ILE].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::ILE];
  sidechain_infos_[ILE].frm_t = param.frm_t[ost::conop::ILE];

  AddInfo(ILE, "CG1", promod3::loop::ILE_CG1_INDEX, false);
  AddInfo(ILE, "CG2", promod3::loop::ILE_CG2_INDEX, false);  
  AddInfo(ILE, "CD1", promod3::loop::ILE_CD1_INDEX, false);  

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(ILE, "HB", promod3::loop::ILE_HB_INDEX, true);
    AddInfo(ILE, "HG12", promod3::loop::ILE_HG12_INDEX, true);
    AddInfo(ILE, "HG13", promod3::loop::ILE_HG13_INDEX, true);
    AddInfo(ILE, "HG21", promod3::loop::ILE_HG21_INDEX, true);
    AddInfo(ILE, "HG22", promod3::loop::ILE_HG22_INDEX, true);
    AddInfo(ILE, "HG23", promod3::loop::ILE_HG23_INDEX, true);
    AddInfo(ILE, "HD11", promod3::loop::ILE_HD11_INDEX, true);
    AddInfo(ILE, "HD12", promod3::loop::ILE_HD12_INDEX, true);
    AddInfo(ILE, "HD13", promod3::loop::ILE_HD13_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(ILE, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(ILE, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.ILE_CA_CB_prefactors);
  AddFRMRotatingParticle(ILE, 0, 0); // CG1
  AddFRMRotatingParticle(ILE, 0, 1); // CG2
  AddFRMRotatingParticle(ILE, 0, 2); // CD1

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(ILE, 0, 3); // HB
    AddFRMRotatingParticle(ILE, 0, 4); // HG12
    AddFRMRotatingParticle(ILE, 0, 5); // HG13
    AddFRMRotatingParticle(ILE, 0, 6); // HG21
    AddFRMRotatingParticle(ILE, 0, 7); // HG22
    AddFRMRotatingParticle(ILE, 0, 8); // HG23
    AddFRMRotatingParticle(ILE, 0, 9); // HD11
    AddFRMRotatingParticle(ILE, 0, 10); // HD12
    AddFRMRotatingParticle(ILE, 0, 11); // HD13
  }


  AddFRMRule(ILE, promod3::loop::BB_CB_INDEX, promod3::loop::ILE_CG1_INDEX,
             param.ILE_CB_CG1_prefactors);
  AddFRMFixParticle(ILE, 1, 0); // CG1
  AddFRMFixParticle(ILE, 1, 1); // CG2
  AddFRMRotatingParticle(ILE, 1, 2); // CD1

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(ILE, 1, 3); // HB
    AddFRMRotatingParticle(ILE, 1, 4); // HG12
    AddFRMRotatingParticle(ILE, 1, 5); // HG13
    AddFRMFixParticle(ILE, 1, 6); // HG21
    AddFRMFixParticle(ILE, 1, 7); // HG22
    AddFRMFixParticle(ILE, 1, 8); // HG23
    AddFRMRotatingParticle(ILE, 1, 9); // HD11
    AddFRMRotatingParticle(ILE, 1, 10); // HD12
    AddFRMRotatingParticle(ILE, 1, 11); // HD13 
  }

  if(cb_in_sidechain) {
    int cb_idx = 3;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 12;
    }
    AddFRMFixParticle(ILE, 0, cb_idx);
    AddFRMFixParticle(ILE, 1, cb_idx);
  }

  backbone_infos_[ILE].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(ILE, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(ILE, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(ILE, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(ILE, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ILE, "H", promod3::loop::ILE_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ILE, "HA", promod3::loop::ILE_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(ILE, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // LEU
  sidechain_infos_[LEU].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[LEU].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::LEU];
  sidechain_infos_[LEU].frm_t = param.frm_t[ost::conop::LEU];
  
  AddInfo(LEU, "CG", promod3::loop::LEU_CG_INDEX, false);
  AddInfo(LEU, "CD1", promod3::loop::LEU_CD1_INDEX, false);  
  AddInfo(LEU, "CD2", promod3::loop::LEU_CD2_INDEX, false);  

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(LEU, "HB2", promod3::loop::LEU_HB2_INDEX, true);
    AddInfo(LEU, "HB3", promod3::loop::LEU_HB3_INDEX, true);
    AddInfo(LEU, "HG", promod3::loop::LEU_HG_INDEX, true);
    AddInfo(LEU, "HD11", promod3::loop::LEU_HD11_INDEX, true);
    AddInfo(LEU, "HD12", promod3::loop::LEU_HD12_INDEX, true);
    AddInfo(LEU, "HD13", promod3::loop::LEU_HD13_INDEX, true);
    AddInfo(LEU, "HD21", promod3::loop::LEU_HD21_INDEX, true);
    AddInfo(LEU, "HD22", promod3::loop::LEU_HD22_INDEX, true);
    AddInfo(LEU, "HD23", promod3::loop::LEU_HD23_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(LEU, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  AddFRMRule(LEU, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.LEU_CA_CB_prefactors);
  AddFRMRotatingParticle(LEU, 0, 0); // CG
  AddFRMRotatingParticle(LEU, 0, 1); // CD1
  AddFRMRotatingParticle(LEU, 0, 2); // CD2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(LEU, 0, 3); // HB2
    AddFRMRotatingParticle(LEU, 0, 4); // HB3
    AddFRMRotatingParticle(LEU, 0, 5); // HG
    AddFRMRotatingParticle(LEU, 0, 6); // HD11
    AddFRMRotatingParticle(LEU, 0, 7); // HD12
    AddFRMRotatingParticle(LEU, 0, 8); // HD13
    AddFRMRotatingParticle(LEU, 0, 9); // HD21
    AddFRMRotatingParticle(LEU, 0, 10); // HD22
    AddFRMRotatingParticle(LEU, 0, 11); // HD23
  }

  AddFRMRule(LEU, promod3::loop::BB_CB_INDEX, promod3::loop::LEU_CG_INDEX,
             param.LEU_CB_CG_prefactors);
  AddFRMFixParticle(LEU, 1, 0); // CG
  AddFRMRotatingParticle(LEU, 1, 1); // CG1
  AddFRMRotatingParticle(LEU, 1, 2); // CG2

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(LEU, 1, 3); // HB2
    AddFRMFixParticle(LEU, 1, 4); // HB3
    AddFRMRotatingParticle(LEU, 1, 5); // HG
    AddFRMRotatingParticle(LEU, 1, 6); // HD11
    AddFRMRotatingParticle(LEU, 1, 7); // HD12
    AddFRMRotatingParticle(LEU, 1, 8); // HD13
    AddFRMRotatingParticle(LEU, 1, 9); // HD21
    AddFRMRotatingParticle(LEU, 1, 10); // HD22
    AddFRMRotatingParticle(LEU, 1, 11); // HD23
  }

  if(cb_in_sidechain) {
    int cb_idx = 3;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 12;
    }
    AddFRMFixParticle(LEU, 0, cb_idx);
    AddFRMFixParticle(LEU, 1, cb_idx);
  }

  backbone_infos_[LEU].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(LEU, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(LEU, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(LEU, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(LEU, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(LEU, "H", promod3::loop::LEU_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(LEU, "HA", promod3::loop::LEU_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(LEU, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // PROLINES DO NOT HAVE FRM DEFINITIONS!
  // large scale benchmarks showed, that varying around chi angles in case
  // of prolines has a negative effect on performance => reduce to one single
  // subrotamer...

  // PRO
  sidechain_infos_[PRO].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[PRO].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::PRO];
  sidechain_infos_[PRO].frm_t = param.frm_t[ost::conop::PRO];
  
  AddInfo(PRO, "CG", promod3::loop::PRO_CG_INDEX, false);
  AddInfo(PRO, "CD", promod3::loop::PRO_CD_INDEX, false); 

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(PRO, "HB2", promod3::loop::PRO_HB2_INDEX, true);
    AddInfo(PRO, "HB3", promod3::loop::PRO_HB3_INDEX, true);
    AddInfo(PRO, "HG2", promod3::loop::PRO_HG2_INDEX, true);
    AddInfo(PRO, "HG3", promod3::loop::PRO_HG3_INDEX, true);
    AddInfo(PRO, "HD2", promod3::loop::PRO_HD2_INDEX, true);
    AddInfo(PRO, "HD3", promod3::loop::PRO_HD3_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(PRO, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  backbone_infos_[PRO].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(PRO, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(PRO, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(PRO, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(PRO, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(PRO, "HA", promod3::loop::PRO_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(PRO, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // CPR
  sidechain_infos_[CPR] = sidechain_infos_[PRO]; 
  frm_rules_[CPR] = frm_rules_[PRO];
  backbone_infos_[CPR] = backbone_infos_[PRO];


  // TPR
  sidechain_infos_[TPR] = sidechain_infos_[PRO];
  frm_rules_[TPR] = frm_rules_[PRO];
  backbone_infos_[TPR] = backbone_infos_[PRO];


  // HSD
  sidechain_infos_[HSD].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[HSD].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::HIS];
  sidechain_infos_[HSD].frm_t = param.frm_t[ost::conop::HIS];

  AddInfo(HSD, "CG", promod3::loop::HIS_CG_INDEX, false);
  AddInfo(HSD, "ND1", promod3::loop::HIS_ND1_INDEX, false);
  AddInfo(HSD, "CD2", promod3::loop::HIS_CD2_INDEX, false);
  AddInfo(HSD, "CE1", promod3::loop::HIS_CE1_INDEX, false);
  AddInfo(HSD, "NE2", promod3::loop::HIS_NE2_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(HSD, "HD1", promod3::loop::HIS_HD1_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(HSD, "HB2", promod3::loop::HIS_HB2_INDEX, true);
    AddInfo(HSD, "HB3", promod3::loop::HIS_HB3_INDEX, true);
    AddInfo(HSD, "HD2", promod3::loop::HIS_HD2_INDEX, true);
    AddInfo(HSD, "HE1", promod3::loop::HIS_HE1_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(HSD, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(HSD, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.HIS_CA_CB_prefactors);
  AddFRMRotatingParticle(HSD, 0, 0); // CG
  AddFRMRotatingParticle(HSD, 0, 1); // ND1
  AddFRMRotatingParticle(HSD, 0, 2); // CD2
  AddFRMRotatingParticle(HSD, 0, 3); // CE1
  AddFRMRotatingParticle(HSD, 0, 4); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSD, 0, 5); // HD1
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSD, 0, 6); // HB2
    AddFRMRotatingParticle(HSD, 0, 7); // HB3
    AddFRMRotatingParticle(HSD, 0, 8); // HD2
    AddFRMRotatingParticle(HSD, 0, 9); // HE1
  }

  AddFRMRule(HSD, promod3::loop::BB_CB_INDEX, promod3::loop::HIS_CG_INDEX,
             param.HIS_CB_CG_prefactors);
  AddFRMFixParticle(HSD, 1, 0); // CG
  AddFRMRotatingParticle(HSD, 1, 1); // ND1
  AddFRMRotatingParticle(HSD, 1, 2); // CD2
  AddFRMRotatingParticle(HSD, 1, 3); // CE1
  AddFRMRotatingParticle(HSD, 1, 4); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSD, 1, 5); // HD1
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSD, 1, 6); // HB2
    AddFRMRotatingParticle(HSD, 1, 7); // HB3
    AddFRMRotatingParticle(HSD, 1, 8); // HD2
    AddFRMRotatingParticle(HSD, 1, 9); // HE1      
  }

  if(cb_in_sidechain) {
    int cb_idx = 5;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 6;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 10;
    }
    AddFRMFixParticle(HSD, 0, cb_idx);
    AddFRMFixParticle(HSD, 1, cb_idx);
  }

  backbone_infos_[HSD].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE || 
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(HSD, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(HSD, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(HSD, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(HSD, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(HSD, "H", promod3::loop::HIS_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(HSD, "HA", promod3::loop::HIS_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(HSD, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // HSE
  sidechain_infos_[HSE].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                         mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[HSE].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::HIS];
  sidechain_infos_[HSE].frm_t = param.frm_t[ost::conop::HIS];

  AddInfo(HSE, "CG", promod3::loop::HIS_CG_INDEX, false);
  AddInfo(HSE, "ND1", promod3::loop::HIS_ND1_INDEX, false);
  AddInfo(HSE, "CD2", promod3::loop::HIS_CD2_INDEX, false);
  AddInfo(HSE, "CE1", promod3::loop::HIS_CE1_INDEX, false);
  AddInfo(HSE, "NE2", promod3::loop::HIS_NE2_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddInfo(HSE, "HE2", promod3::loop::HIS_HE2_INDEX,true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(HSE, "HB2", promod3::loop::HIS_HB2_INDEX, true);
    AddInfo(HSE, "HB3", promod3::loop::HIS_HB3_INDEX, true);
    AddInfo(HSE, "HD2", promod3::loop::HIS_HD2_INDEX, true);
    AddInfo(HSE, "HE1", promod3::loop::HIS_HE1_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(HSE, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(HSE, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.HIS_CA_CB_prefactors);
  AddFRMRotatingParticle(HSE, 0, 0); // CG
  AddFRMRotatingParticle(HSE, 0, 1); // ND1
  AddFRMRotatingParticle(HSE, 0, 2); // CD2
  AddFRMRotatingParticle(HSE, 0, 3); // CE1
  AddFRMRotatingParticle(HSE, 0, 4); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSE, 0, 5); // HE2
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSE, 0, 6); // HB2
    AddFRMRotatingParticle(HSE, 0, 7); // HB3
    AddFRMRotatingParticle(HSE, 0, 8); // HD2
    AddFRMRotatingParticle(HSE, 0, 9); // HE1
  }

  AddFRMRule(HSE, promod3::loop::BB_CB_INDEX, promod3::loop::HIS_CG_INDEX,
             param.HIS_CB_CG_prefactors);
  AddFRMFixParticle(HSE, 1, 0); // CG
  AddFRMRotatingParticle(HSE, 1, 1); // ND1
  AddFRMRotatingParticle(HSE, 1, 2); // CD2
  AddFRMRotatingParticle(HSE, 1, 3); // CE1
  AddFRMRotatingParticle(HSE, 1, 4); // NE2

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSE, 1, 5); // HE2
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(HSE, 1, 6); // HB2
    AddFRMRotatingParticle(HSE, 1, 7); // HB3
    AddFRMRotatingParticle(HSE, 1, 8); // HD2
    AddFRMRotatingParticle(HSE, 1, 9); // HE1      
  }

  if(cb_in_sidechain) {
    int cb_idx = 5;
    if(mode_ == POLAR_HYDROGEN_MODE) {
      cb_idx = 6;
    }
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 10;
    }
    AddFRMFixParticle(HSE, 0, cb_idx);
    AddFRMFixParticle(HSE, 1, cb_idx);
  }

  backbone_infos_[HSE].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE || 
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(HSE, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(HSE, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(HSE, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(HSE, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(HSE, "H", promod3::loop::HIS_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(HSE, "HA", promod3::loop::HIS_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(HSE, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  // HIS
  // this is a bit hacky... we just assign HSD as determined by a fair
  // random number generator.
  sidechain_infos_[HIS] = sidechain_infos_[HSD];
  frm_rules_[HIS] = frm_rules_[HSD];
  backbone_infos_[HIS] = backbone_infos_[HSD];


  // PHE
  sidechain_infos_[PHE].has_hydrogens = (mode_ == FULL_ATOMIC_MODE);
  sidechain_infos_[PHE].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::PHE];
  sidechain_infos_[PHE].frm_t = param.frm_t[ost::conop::PHE];
  
  AddInfo(PHE, "CG", promod3::loop::PHE_CG_INDEX, false);
  AddInfo(PHE, "CD1", promod3::loop::PHE_CD1_INDEX, false);  
  AddInfo(PHE, "CD2", promod3::loop::PHE_CD2_INDEX, false); 
  AddInfo(PHE, "CE1", promod3::loop::PHE_CE1_INDEX, false);  
  AddInfo(PHE, "CE2", promod3::loop::PHE_CE2_INDEX, false); 
  AddInfo(PHE, "CZ", promod3::loop::PHE_CZ_INDEX, false); 

  if(mode_ == FULL_ATOMIC_MODE) {
    AddInfo(PHE, "HB2", promod3::loop::PHE_HB2_INDEX, true);
    AddInfo(PHE, "HB3", promod3::loop::PHE_HB3_INDEX, true);
    AddInfo(PHE, "HD1", promod3::loop::PHE_HD1_INDEX, true);
    AddInfo(PHE, "HD2", promod3::loop::PHE_HD2_INDEX, true);
    AddInfo(PHE, "HE1", promod3::loop::PHE_HE1_INDEX, true);
    AddInfo(PHE, "HE2", promod3::loop::PHE_HE2_INDEX, true);
    AddInfo(PHE, "HZ", promod3::loop::PHE_HZ_INDEX, true);
  }

  if(cb_in_sidechain) {
    AddInfo(PHE, "CB", promod3::loop::BB_CB_INDEX, false);
  }

  AddFRMRule(PHE, promod3::loop::BB_CA_INDEX, promod3::loop::BB_CB_INDEX,
             param.PHE_CA_CB_prefactors);
  AddFRMRotatingParticle(PHE, 0, 0); // CG
  AddFRMRotatingParticle(PHE, 0, 1); // CD1
  AddFRMRotatingParticle(PHE, 0, 2); // CD2
  AddFRMRotatingParticle(PHE, 0, 3); // CE1
  AddFRMRotatingParticle(PHE, 0, 4); // CE2
  AddFRMRotatingParticle(PHE, 0, 5); // CZ

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMRotatingParticle(PHE, 0, 6); // HB2
    AddFRMRotatingParticle(PHE, 0, 7); // HB3
    AddFRMRotatingParticle(PHE, 0, 8); // HD1
    AddFRMRotatingParticle(PHE, 0, 9); // HD2
    AddFRMRotatingParticle(PHE, 0, 10); // HE1
    AddFRMRotatingParticle(PHE, 0, 11); // HE2
    AddFRMRotatingParticle(PHE, 0, 12); // HZ
  }

  AddFRMRule(PHE, promod3::loop::BB_CB_INDEX, promod3::loop::PHE_CG_INDEX,
             param.PHE_CB_CG_prefactors);
  AddFRMFixParticle(PHE, 1, 0); // CG
  AddFRMRotatingParticle(PHE, 1, 1); // CD1
  AddFRMRotatingParticle(PHE, 1, 2); // CD2
  AddFRMRotatingParticle(PHE, 1, 3); // CE1
  AddFRMRotatingParticle(PHE, 1, 4); // CE2
  AddFRMRotatingParticle(PHE, 1, 5); // CZ

  if(mode_ == FULL_ATOMIC_MODE) {
    AddFRMFixParticle(PHE, 1, 6); // HB2
    AddFRMFixParticle(PHE, 1, 7); // HB2
    AddFRMRotatingParticle(PHE, 1, 8); // CD1
    AddFRMRotatingParticle(PHE, 1, 9); // CD2
    AddFRMRotatingParticle(PHE, 1, 10); // CE1
    AddFRMRotatingParticle(PHE, 1, 11); // CE2
    AddFRMRotatingParticle(PHE, 1, 12); // CZ
  }

  if(cb_in_sidechain) {
    int cb_idx = 6;
    if(mode_ == FULL_ATOMIC_MODE) {
      cb_idx = 13;
    }
    AddFRMFixParticle(PHE, 0, cb_idx);
    AddFRMFixParticle(PHE, 1, cb_idx);
  }

  backbone_infos_[PHE].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE || 
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(PHE, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(PHE, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(PHE, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(PHE, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(PHE, "H", promod3::loop::PHE_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(PHE, "HA", promod3::loop::PHE_HA_INDEX, true);
  }

  if(!cb_in_sidechain) {
    AddBBInfo(PHE, "CB", promod3::loop::BB_CB_INDEX, false);
  }


  // ALA

  // The hydrogen situation is a bit special here...
  // If its full atomic, we only put the hydrogens in the sidechains
  // if the CBeta is also there. Otherwise, everything goes into the
  // backbone.
  sidechain_infos_[ALA].has_hydrogens = (mode_ == FULL_ATOMIC_MODE &&
                                         cb_in_sidechain);
  sidechain_infos_[ALA].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::ALA];
  sidechain_infos_[ALA].frm_t = param.frm_t[ost::conop::ALA];

  if(cb_in_sidechain) {
    AddInfo(ALA, "CB", promod3::loop::BB_CB_INDEX, false);
    if(mode_ == FULL_ATOMIC_MODE) {
      AddInfo(ALA, "HB1", promod3::loop::ALA_HB1_INDEX, true);
      AddInfo(ALA, "HB2", promod3::loop::ALA_HB2_INDEX, true);
      AddInfo(ALA, "HB3", promod3::loop::ALA_HB3_INDEX, true);
    }
  }

  backbone_infos_[ALA].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(ALA, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(ALA, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(ALA, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(ALA, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ALA, "H", promod3::loop::ALA_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(ALA, "HA", promod3::loop::ALA_HA_INDEX, true);      
  }

  if(!cb_in_sidechain) {
    AddBBInfo(ALA, "CB", promod3::loop::BB_CB_INDEX, false); 
    if(mode_ == FULL_ATOMIC_MODE) {
      AddBBInfo(ALA, "HB1", promod3::loop::ALA_HB1_INDEX, true);
      AddBBInfo(ALA, "HB2", promod3::loop::ALA_HB2_INDEX, true);
      AddBBInfo(ALA, "HB3", promod3::loop::ALA_HB3_INDEX, true);        
    }
  }


  // GLY
  sidechain_infos_[GLY].has_hydrogens = false;
  sidechain_infos_[GLY].internal_e_prefactor = 
  param.internal_e_prefactor[ost::conop::GLY];
  sidechain_infos_[GLY].frm_t = param.frm_t[ost::conop::GLY];
  
  backbone_infos_[GLY].has_hydrogens = (mode_ == POLAR_HYDROGEN_MODE ||
                                        mode_ == FULL_ATOMIC_MODE);
  AddBBInfo(GLY, "N", promod3::loop::BB_N_INDEX, false);
  AddBBInfo(GLY, "CA", promod3::loop::BB_CA_INDEX, false);
  AddBBInfo(GLY, "C", promod3::loop::BB_C_INDEX, false);
  AddBBInfo(GLY, "O", promod3::loop::BB_O_INDEX, false);

  if(mode_ == POLAR_HYDROGEN_MODE || mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(GLY, "H", promod3::loop::GLY_H_INDEX, true);
  }

  if(mode_ == FULL_ATOMIC_MODE) {
    AddBBInfo(GLY, "HA2", promod3::loop::GLY_HA2_INDEX, true);
    AddBBInfo(GLY, "HA3", promod3::loop::GLY_HA3_INDEX, true);
  }

  for(uint i = 0; i <= XXX; ++i){
    num_sidechain_particles_[i] = sidechain_infos_[i].particles.size();
    num_frm_particles_[i] = num_sidechain_particles_[i];
    for(uint j = 0; j < frm_rules_[i].size(); ++j){
      num_frm_particles_[i] += (frm_rules_[i][j].prefactors.size() * 
                                frm_rules_[i][j].rotating_particles.size());
    }
  }
}


RotamerInfo RotamerLookup::GetTerminalBackboneInfo(RotamerID id, bool n_ter,
                                                   bool c_ter) {
  // get a copy to work on
  RotamerInfo info = backbone_infos_[id];

  if(n_ter && mode_ != HEAVY_ATOM_MODE) {
    // find and delete particle with name "H", add particles of 
    // name "H1", "H2", "H3", only the first two in case of Proline
    for(std::vector<ParticleInfo>::iterator it = info.particles.begin();
        it != info.particles.end(); ++it) {
      if(it->name == "H") {
        info.particles.erase(it);
        break;
      }
    }

    ost::conop::AminoAcid aa = RotIDToAA(id);

    ParticleInfo h1("H1", promod3::loop::AminoAcidLookup::GetInstance().GetH1Index(aa), true);
    info.particles.push_back(h1);

    ParticleInfo h2("H2", promod3::loop::AminoAcidLookup::GetInstance().GetH2Index(aa), true);
    info.particles.push_back(h2);

    if(aa != ost::conop::PRO) {
      ParticleInfo h3("H3", promod3::loop::AminoAcidLookup::GetInstance().GetH3Index(aa), true);
      info.particles.push_back(h3);
    }
  }

  if(c_ter) {
    // find particle with name "O" and mark it as terminal by setting its
    // atom_index to -1
    for(std::vector<ParticleInfo>::iterator it = info.particles.begin();
        it != info.particles.end(); ++it) {
      if(it->name == "O") {
        it->atom_idx = -1;
        break;
      }
    }
    // atom_index of -2 marks terminal oxt
    ParticleInfo oxt("OT", -2, false);
    info.particles.push_back(oxt);
  }

  return info;
}

}} // ns
