// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_SCWRL4_PARTICLE_SCORING_HH
#define PROMOD3_SCWRL4_PARTICLE_SCORING_HH

#include <ost/geom/mat4.hh>
#include <ost/geom/vecmat3_op.hh>
#include <promod3/sidechain/particle_scoring_base.hh>

namespace promod3{ namespace sidechain{

enum SCWRL4ParticleType {
  HParticle,
  CParticle,
  CH1Particle,
  CH2Particle,
  CH3Particle,
  NParticle,
  OParticle,
  OCParticle,
  SParticle,
};

const Real SCWRL4CollisionDistances[9] = {1.0300, 2.2400, 2.2400, 2.2400, 
                                          2.2400, 1.7067, 1.7200,
                                          1.7200, 2.2800};

struct SCWRL4Param : public PScoringParam {

  SCWRL4Param(SCWRL4ParticleType p, const geom::Vec3& pos, 
              Real charge): pos_(pos), 
                            collision_distance_(SCWRL4CollisionDistances[p]),
                            particle_type_(p), charge_(charge),
                            is_hbond_donor_(false), 
                            is_hbond_acceptor_(false) { }

  virtual ~SCWRL4Param() { }

  virtual const geom::Vec3& GetPos() const { return pos_; }

  virtual Real GetCollisionDistance() const { return collision_distance_; }

  virtual void ApplyTransform(const geom::Mat4& t);

  virtual SCWRL4Param* Copy() const;

  virtual bool EqualTo(PScoringParam* other) const;

  virtual PScoringFunction GetScoringFunction() const { return SCWRL4; }

  Real GetCharge() { return charge_; }

  SCWRL4ParticleType GetParticleType() { return particle_type_; }

  bool IsHBondDonor() const { return is_hbond_donor_; }

  bool IsHBondAcceptor() const { return is_hbond_acceptor_; }

  const std::vector<geom::Vec3>& GetLonePairs() const { return lone_pairs_; }

  const geom::Vec3& GetPolarDirection() const { return polar_direction_; }

  void AddLonePair(const geom::Vec3& lone_pair){
    is_hbond_acceptor_ = true;
    lone_pairs_.push_back(geom::Normalize(lone_pair));
  }

  void SetPolarDirection(const geom::Vec3& dir){
    is_hbond_donor_ = true;
    polar_direction_ = geom::Normalize(dir);
  }

  geom::Vec3 pos_;
  Real collision_distance_;
  SCWRL4ParticleType particle_type_;
  Real charge_;
  bool is_hbond_donor_;
  bool is_hbond_acceptor_; 
  std::vector<geom::Vec3> lone_pairs_;
  geom::Vec3 polar_direction_;
};


Real SCWRL4PairwiseScore(SCWRL4Param* p_one, SCWRL4Param* p_two);

}} //ns

#endif
