// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_SUBROTAMER_OPTIMIZER_HH
#define PROMOD3_SUBROTAMER_OPTIMIZER_HH

#include <vector>

#include <promod3/sidechain/rotamer_graph.hh>


namespace promod3{ namespace sidechain{

void SubrotamerOptimizer(std::vector<FRMRotamerPtr>& rotamers,
                         Real active_internal_energy = -2.0,
                         Real inactive_internal_energy = 0.0,
                         uint max_complexity = 100000000,
                         Real initial_epsilon = 0.02);
}} // ns

#endif
