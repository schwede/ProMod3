// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_ID_HH
#define PROMOD3_ROTAMER_ID_HH

#include <ost/base.hh>
#include <ost/conop/amino_acids.hh>
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3{ namespace sidechain{

enum RotamerID {
  ARG, ASN, ASP,
  GLN, GLU, LYS,
  SER, CYS, CYH,
  CYD, MET, TRP,
  TYR, THR, VAL,
  ILE, LEU, PRO,
  CPR, TPR, HSD,
  HSE, HIS, PHE,
  GLY, ALA, XXX
};

// portable serialization (with fixed-width base-types)
inline void Serialize(core::PortableBinaryDataSink& ds, RotamerID t) {
  core::ConvertBaseType<uint16_t>(ds, t);
}
inline void Serialize(core::PortableBinaryDataSource& ds, RotamerID& t) {
  core::ConvertBaseType<uint16_t>(ds, t);
}

RotamerID TLCToRotID(const String& tlc);

RotamerID AAToRotID(ost::conop::AminoAcid aa);

char RotIDToOLC(RotamerID id);

ost::conop::AminoAcid RotIDToAA(RotamerID id);

}}//ns

#endif
