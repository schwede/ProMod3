// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/disulfid.hh>
#include <promod3/sidechain/rotamer.hh>
#include <promod3/sidechain/particle.hh>
#include <promod3/core/graph.hh>
#include <promod3/core/enumerator.hh>
#include <promod3/scoring/scwrl3_energy_functions.hh>
#include <vector>

namespace{

template <typename T>
void ExtractCYSSG(T rot_one, T rot_two,
             std::vector<geom::Vec3>& pos_one,
             std::vector<geom::Vec3>& pos_two) {

  for(typename T::iterator i = rot_one.begin(); 
      i != rot_one.end(); ++i) {
    if(i->GetName() == "SG") {
      pos_one.push_back(i->GetPos());
    }
  }

  for(typename T::iterator i = rot_two.begin(); 
      i != rot_two.end(); ++i) {
    if(i->GetName() == "SG") {
      pos_two.push_back(i->GetPos());
    }
  }
}

void SingleResolve(const std::vector<Real>& scores, 
                   const promod3::core::Graph& graph, 
                   std::vector<int>& active_bonds) {

  int size = scores.size();
  std::vector<int> stupid_enumerator_input(size, 2);
  promod3::core::Enumerator<int> enumerator(stupid_enumerator_input);
  std::vector<int>& current_combination = enumerator.CurrentEnum();

  Real optimal_score = std::numeric_limits<Real>::max();

  do {

    Real current_score = 0.0;

    for(int i = 0; i < size; ++i) {
      current_score += current_combination[i] * scores[i];
      for(int j = 0; j < size; ++j) {
        if(i != j) current_score += graph.IsConnected(i, j) * 
                                    current_combination[i] * 
                                    current_combination[j] * Real(1000000.0);
      }
    } 

    if(current_score < optimal_score) {
      optimal_score = current_score;
      active_bonds = current_combination;
    }

  } while(enumerator.Next());
}

void Resolve(const std::vector<Real>& scores, 
             const promod3::core::Graph& graph,
             uint max_problem_size, 
             std::vector<int>& active_bonds) {

  uint size = scores.size();

  if(size <= max_problem_size) {
    SingleResolve(scores, graph, active_bonds);
    return;
  }

  // If we reach this point, the thing is not really solvable...
  // The idea is to always kill the disulfid bond with the worst energy
  // in the largest component.
  // This should result in smaller and smaller connected components that 
  // become solvable. 

  std::vector<std::vector<int> > connected_components(1);
  
  // at the beginning everything is in the same component
  for(int i = 0; i < graph.NumNodes(); ++i) {
    connected_components.back().push_back(i);
  }

  while(true) {

    uint largest_size = 0;
    uint largest_idx = 0;

    // search for the largest component
    for(uint i = 0; i < connected_components.size(); ++i) {
      uint current_size = connected_components[i].size();
      if(current_size > largest_size) {
        largest_size = current_size;
        largest_idx = i;
      }
    }

    // is it small enough?
    if(largest_size <= max_problem_size) break;

    const std::vector<int>& largest_component = 
    connected_components[largest_idx];

    // let's search for the item with the worst score
    Real worst_score = -std::numeric_limits<Real>::max();
    Real worst_score_idx = 0;

    for(uint i = 0; i < largest_component.size(); ++i) {
      Real actual_score = scores[largest_component[i]]; 
      if(actual_score > worst_score) {
        worst_score = actual_score;
        worst_score_idx = i;
      }
    }

    std::vector<int> indices_to_keep;
    for(uint i = 0; i < worst_score_idx; ++i) {
      indices_to_keep.push_back(largest_component[i]);
    }
    for(uint i = worst_score_idx + 1; i < largest_size; ++i) {
      indices_to_keep.push_back(largest_component[i]);
    }

    std::vector<std::vector<int> > new_components;
    promod3::core::Graph subgraph = graph.SubGraph(indices_to_keep);
    subgraph.ConnectedComponents(new_components);
    
    // reorganize connected components
    for(uint i = 0; i < new_components.size(); ++i) {
      connected_components.push_back(std::vector<int>());
      for(uint j = 0; j < new_components[i].size(); ++j) {
        int idx_to_add = indices_to_keep[new_components[i][j]];
        connected_components.back().push_back(idx_to_add);
      }
    }
    connected_components.erase(connected_components.begin() + largest_idx);
  }

  // all components are now solvable... let's do it!
  active_bonds.assign(scores.size(), 0);
  for(uint i = 0; i < connected_components.size(); ++i) {
    uint component_size = connected_components[i].size();
    if(component_size == 1) {
      active_bonds[connected_components[i][0]] = 1;
    }
    else{
      std::vector<Real> component_scores(component_size, 0.0);
      for(uint j = 0; j < component_size; ++j) {
        component_scores[j] = scores[connected_components[i][j]];
      }
      promod3::core::Graph component_graph = 
      graph.SubGraph(connected_components[i]);
      std::vector<int> component_active_bonds;
      SingleResolve(component_scores, component_graph, component_active_bonds);
      for(uint j = 0; j < component_size; ++j) {
        active_bonds[connected_components[i][j]] = component_active_bonds[j];
      }
    }
  }
}


template <typename T>
void CysteinResolve(const std::vector<T>& rot_groups,
                     const std::vector<geom::Vec3>& ca_pos,
                     const std::vector<geom::Vec3>& cb_pos,
                     Real disulfid_score_thresh,
                     std::vector<std::pair<uint, uint> >& disulfid_indices,
                     std::vector<std::pair<uint, uint> >& rotamer_indices) {

  // do the trivial cases...
  if(rot_groups.size() <= 1) {
    return;
  }

  // do some input checks
  if(ca_pos.size() != cb_pos.size()) {
    throw promod3::Error("Inconsistency in size of input lists observed!");
  }

  if(ca_pos.size() != rot_groups.size()) {
    throw promod3::Error("Inconsistency in size of input lists observed!");
  }

  // buildup disulfids, they might still be ambiguous!
  std::vector<Real> scores;
  std::vector<std::pair<uint, uint> > disulfids;
  std::vector<std::pair<uint, uint> > optimal_rotamers;
  uint num_rotamer_groups = rot_groups.size();

  for(uint i = 0; i < num_rotamer_groups; ++i) {
    for(uint j = i + 1; j < num_rotamer_groups; ++j) {
      if(geom::Distance(ca_pos[i], ca_pos[j]) < Real(8.0)) {
        // they are close and potentially build a disulfid bond...
        Real min_score = std::numeric_limits<Real>::max();
        uint min_k = 0;
        uint min_l = 0;
        Real score;
        for(uint k = 0; k < rot_groups[i]->size(); ++k) {
          for(uint l = 0; l < rot_groups[j]->size(); ++l) {
            score = promod3::sidechain::DisulfidScore((*rot_groups[i])[k], 
                                                      (*rot_groups[j])[l],
                                                      ca_pos[i], cb_pos[i], 
                                                      ca_pos[j], cb_pos[j]);
            if(score < min_score) {
              min_score = score;
              min_k = k;
              min_l = l;
            }
          }
        }

        if(min_score < disulfid_score_thresh) {
          // just found a potential disulfid bond!
          scores.push_back(min_score - disulfid_score_thresh);
          disulfids.push_back(std::make_pair(i, j));
          optimal_rotamers.push_back(std::make_pair(min_k, min_l));
        }
      }
    }
  }

 // in here we fill all indices of the active disulfid bonds...
  std::vector<int> final_disulfids;

  if(disulfids.size() > 1) {
    // resolve disambiguities
    promod3::core::Graph graph(disulfids.size());
    for(uint i = 0; i < disulfids.size(); ++i) {
      for(uint j = i + 1; j < disulfids.size(); ++j) {
        // check whether same rotamer group is involved in two disulfid bonds
        if(disulfids[j].first == disulfids[i].first ||
           disulfids[j].first == disulfids[i].second ||
           disulfids[j].second == disulfids[i].first ||
           disulfids[j].second == disulfids[i].second) {
          graph.Connect(i, j);
        }
      }
    }

    std::vector<std::vector<int> > connected_components;
    graph.ConnectedComponents(connected_components);

    for(uint component_idx = 0; component_idx < connected_components.size();
        ++component_idx) {

      const std::vector<int>& connected_component = 
      connected_components[component_idx];

      if(connected_component.size() > 1) {
        
        std::vector<Real> component_scores;
        for(uint i = 0; i < connected_component.size(); ++i) {
          component_scores.push_back(scores[connected_component[i]]);
        }
        promod3::core::Graph component_graph = 
        graph.SubGraph(connected_component);
        std::vector<int> active_bonds;
        Resolve(component_scores, component_graph, 15, active_bonds);
        for(uint i = 0; i < active_bonds.size(); ++i) {
          if(active_bonds[i] == 1) {
            final_disulfids.push_back(connected_component[i]);
          }
        }
      }
      else{
        final_disulfids.push_back(connected_component[0]);
      }
    }
  }
  else if(disulfids.size() == 1) {
    final_disulfids.push_back(0);
  }

  // fill output
  for(std::vector<int>::iterator i = final_disulfids.begin();
      i != final_disulfids.end(); ++i) {
    uint group_idx_one = disulfids[*i].first;
    uint group_idx_two = disulfids[*i].second;
    uint rot_idx_one = optimal_rotamers[*i].first;
    uint rot_idx_two = optimal_rotamers[*i].second;
    disulfid_indices.push_back(std::make_pair(group_idx_one, group_idx_two));
    rotamer_indices.push_back(std::make_pair(rot_idx_one, rot_idx_two));
  }
}

void SetOptimalSubrotamer(promod3::sidechain::FRMRotamerPtr rot_one, 
                          promod3::sidechain::FRMRotamerPtr rot_two,
                          const geom::Vec3& ca_pos_one, 
                          const geom::Vec3& cb_pos_one,
                          const geom::Vec3& ca_pos_two, 
                          const geom::Vec3& cb_pos_two) {

  std::vector<geom::Vec3> sg_pos_one, sg_pos_two;
  ExtractCYSSG(*rot_one, *rot_two, sg_pos_one, sg_pos_two);

  if(sg_pos_one.size() != rot_one->subrotamer_size() || 
     sg_pos_two.size() != rot_two->subrotamer_size() ) {
    throw promod3::Error("Expect cysteins to have exactly one gamma sulfur "
                         "per subrotamer!");
  }

  Real min_score = std::numeric_limits<Real>::max();
  uint min_i = 0;
  uint min_j = 0;

  for(uint i = 0; i < sg_pos_one.size(); ++i) {
    for(uint j = 0; j < sg_pos_two.size(); ++j) {

      Real score = promod3::scoring::SCWRL3DisulfidScore(ca_pos_one,
                                                         cb_pos_one,
                                                         sg_pos_one[i], 
                                                         ca_pos_two,
                                                         cb_pos_two,
                                                         sg_pos_two[j]);
      if(score < min_score) {
        min_score = score;
        min_i = i;
        min_j = j;
      }
    }
  }

  if(min_score < std::numeric_limits<Real>::max()) {
    rot_one->SetActiveSubrotamer(min_i);
    rot_two->SetActiveSubrotamer(min_j);
  }
}

}


namespace promod3{ namespace sidechain{


Real DisulfidScore(RRMRotamerPtr rot_one, RRMRotamerPtr rot_two, 
                   const geom::Vec3& ca_pos_one, const geom::Vec3& cb_pos_one, 
                   const geom::Vec3& ca_pos_two, const geom::Vec3& cb_pos_two) {

  std::vector<geom::Vec3> sg_pos_one, sg_pos_two;
  ExtractCYSSG(*rot_one, *rot_two, sg_pos_one, sg_pos_two);

  if(sg_pos_one.size() != 1 || sg_pos_two.size() != 1) {
    throw promod3::Error("Expect rigid rotamers to have exactly one gamma "
                         "sulfur particle!");
  }

  Real raw_score = promod3::scoring::SCWRL3DisulfidScore(ca_pos_one, 
                                                         cb_pos_one, 
                                                         sg_pos_one[0], 
                                                         ca_pos_two, 
                                                         cb_pos_two, 
                                                         sg_pos_two[0]); 

  Real self_energy = rot_one->GetSelfEnergy() + rot_two->GetSelfEnergy();
  self_energy *= Real(0.5);
  return raw_score + self_energy;
}

Real DisulfidScore(FRMRotamerPtr rot_one, FRMRotamerPtr rot_two, 
                   const geom::Vec3& ca_pos_one, const geom::Vec3& cb_pos_one, 
                   const geom::Vec3& ca_pos_two, const geom::Vec3& cb_pos_two) {

  std::vector<geom::Vec3> sg_pos_one, sg_pos_two;
  ExtractCYSSG(*rot_one, *rot_two, sg_pos_one, sg_pos_two);

  if(sg_pos_one.empty() || sg_pos_two.empty()) {
    throw promod3::Error("Expect flexible rotamers to have at least one gamma "
                         "sulfur particle!");
  }

  Real min_raw_score = std::numeric_limits<Real>::max();

  for(uint i = 0; i < sg_pos_one.size(); ++i) {
    for(uint j = 0; j < sg_pos_two.size(); ++j) {

      min_raw_score = std::min(promod3::scoring::SCWRL3DisulfidScore(ca_pos_one,
                                                                  cb_pos_one,
                                                                  sg_pos_one[i], 
                                                                  ca_pos_two,
                                                                  cb_pos_two,
                                                                  sg_pos_two[j])
                                                                ,min_raw_score);
    }
  }

  Real self_energy = rot_one->GetSelfEnergy() + rot_two->GetSelfEnergy();
  self_energy *= Real(0.5);
  return min_raw_score + self_energy;
}

// template functionality of following functions has been hidden in
// the source file to avoid bloating the header...
void ResolveCysteins(
      const std::vector<RRMRotamerGroupPtr>& rot_groups,
      const std::vector<geom::Vec3>& ca_pos,
      const std::vector<geom::Vec3>& cb_pos,
      Real disulfid_score_thresh, bool optimize_subrotamers,
      std::vector<std::pair<uint, uint> >& disulfid_indices,
      std::vector<std::pair<uint, uint> >& rotamer_indices) {

  CysteinResolve<RRMRotamerGroupPtr>(rot_groups, ca_pos, cb_pos,
                                      disulfid_score_thresh, disulfid_indices, 
                                      rotamer_indices);

}

void ResolveCysteins(const std::vector<FRMRotamerGroupPtr>& rot_groups,
                     const std::vector<geom::Vec3>& ca_pos,
                     const std::vector<geom::Vec3>& cb_pos,
                     Real disulfid_score_thresh, bool optimize_subrotamers,
                     std::vector<std::pair<uint, uint> >& disulfid_indices,
                     std::vector<std::pair<uint, uint> >& rotamer_indices) {

  CysteinResolve<FRMRotamerGroupPtr>(rot_groups, ca_pos, cb_pos,
                                     disulfid_score_thresh, disulfid_indices,
                                     rotamer_indices);

  if(optimize_subrotamers) {

    for(uint i = 0; i < disulfid_indices.size(); ++i) {

      const std::pair<uint, uint>& group_pair = disulfid_indices[i];
      const std::pair<uint, uint>& rotamer_pair = rotamer_indices[i];

      SetOptimalSubrotamer((*rot_groups[group_pair.first])[rotamer_pair.first],
                           (*rot_groups[group_pair.second])[rotamer_pair.second],
                           ca_pos[group_pair.first], 
                           cb_pos[group_pair.first],
                           ca_pos[group_pair.second], 
                           cb_pos[group_pair.second]);
    }
  }
}


}} // ns
