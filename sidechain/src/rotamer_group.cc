// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer_group.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/sidechain/particle_scoring.hh>

namespace{

Real LogSumExp(double* values, int n) {

  double min = std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  for(int i = 0; i < n; ++i) {
    min = std::min(min, values[i]);
    max = std::max(max, values[i]);
  }

  double range = max - min;

  if(range > double(100.0)) {
    // The log sum exp function (LSE) is bound by
    // max(x1, x2, ..., xn) <= LSE(x1, x2, ..., xn) <= 
    // max(x1, x2, ..., xn) + log(n)
    // the maximum value is therefore a rather good approximation
    // this gets returned if the range get's too large because
    // even the LSE trick implemented below gets numerical problems...
    return max;
  }
  else{
    double a = double(0.5) * (max + min);
    double sum = 0.0;
    for(int i = 0; i < n; ++i) sum += std::exp(values[i]-a);
    return std::log(sum) + a;
  }
}

}

namespace promod3 { namespace sidechain {


RRMRotamerGroup::RRMRotamerGroup(const std::vector<RRMRotamerPtr>& rotamers,
                                 uint residue_index)
                                 : rotamers_(rotamers)
                                 , residue_index_(residue_index) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                               "RRMRotamerGroup::RRMRotamerGroup", 2);

  if (rotamers.empty()) {
    throw promod3::Error("Cannot construct RRM rotamer group from empty "
                         "rotamer list!");
  }

  this->Init();
}


void RRMRotamerGroup::ApplyOnResidue(uint rotamer_index,
                                     ost::mol::ResidueHandle& res, 
                                     bool consider_hydrogens,
                                     const String& new_res_name) const {
  if (rotamer_index >= rotamers_.size()) {
    throw promod3::Error("Invalid rotamer index");
  }
  rotamers_[rotamer_index]->ApplyOnResidue(res, consider_hydrogens,
                                           new_res_name);
}


void RRMRotamerGroup::ApplyOnResidue(uint rotamer_index,
                                     loop::AllAtomPositions& all_atom,
                                     uint res_idx) const {
  if (rotamer_index >= rotamers_.size()) {
    throw promod3::Error("Invalid rotamer index");
  }
  rotamers_[rotamer_index]->ApplyOnResidue(all_atom, res_idx);
}


void RRMRotamerGroup::SetFrameEnergy(FramePtr frame) {

  for(iterator i = this->begin(); i != this->end(); ++i) {
    (*i)->SetFrameEnergy(0.0);
  }
  this->AddFrameEnergy(frame);
}


void RRMRotamerGroup::AddFrameEnergy(FramePtr frame) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "RRMRotamerGroup::AddFrameEnergy", 2);

  std::vector<std::pair<int, int> > overlapping_particles;
  collision_tree_.OverlappingPolytopes(frame->GetCollisionTree(), 
                                       overlapping_particles);

  if(overlapping_particles.empty()) return;

  const std::vector<uint>& frame_residue_indices = frame->GetResidueIndices();
  std::vector<Real> frame_energies(this->size(), 0.0);
  std::vector<Real> scores;
  scoring_parameters_.PairwiseScores(frame->GetScoringParameters(), 
                                     overlapping_particles, scores);

  for(uint i = 0; i < scores.size(); ++i) {
    const std::pair<int, int>& idx = overlapping_particles[i];
    if(frame_residue_indices[idx.second] != residue_index_) {
      frame_energies[rotamer_indices_[idx.first]] += scores[i];
    }
  }

  for(uint i = 0; i < this->size(); ++i) {
    rotamers_[i]->AddFrameEnergy(frame_energies[i]);
  }
}


bool RRMRotamerGroup::CalculatePairwiseEnergies(RRMRotamerGroupPtr other,
                                                Real threshold,
                                                promod3::core::EMatXX& emat) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                               "RRMRotamerGroup::CalculatePairwiseEnergies", 2);

  if(!collision_tree_.Overlap(other->GetCollisionTree())) return false;

  std::vector<std::pair<int,int> > overlapping_particles;
  overlapping_particles.reserve(4000);

  collision_tree_.OverlappingPolytopes(other->GetCollisionTree(), 
                                       overlapping_particles);

  if(overlapping_particles.empty()) return false;
  
  emat = promod3::core::EMatXX::Zero(this->size(), other->size());
  std::vector<Real> scores;
  scoring_parameters_.PairwiseScores(other->scoring_parameters_, 
                                     overlapping_particles, scores);

  for(uint i = 0; i < scores.size(); ++i) {
    if(scores[i] != 0.0) {
      const std::pair<int, int>& idx = overlapping_particles[i];
      emat(rotamer_indices_[idx.first], other->rotamer_indices_[idx.second]) += 
      scores[i];
    }
  }

  if(emat.cwiseAbs().maxCoeff() < threshold) {
    return false;
  }

  return true;
}


Real RRMRotamerGroup::GetMaxP() const {
  Real max_p = std::numeric_limits<Real>::min();
  for(const_iterator i = this->begin(); i != this->end(); ++i) {
    max_p = std::max(max_p,(*i)->GetProbability());
  }
  return max_p;
}


void RRMRotamerGroup::ApplySelfEnergyThresh(Real self_energy_thresh) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                               "RRMRotamerGroup::ApplySelfEnergyThresh", 2);

  if(rotamers_.size() <= 1) return;

  // find the lowest energy
  Real e_min = std::numeric_limits<Real>::max();
  for(std::vector<RRMRotamerPtr>::iterator i = rotamers_.begin();
      i != rotamers_.end(); ++i) {
    if( (*i)->GetSelfEnergy() < e_min) e_min = (*i)->GetSelfEnergy();
  }

  // define threshold relative to it
  Real thresh = e_min + self_energy_thresh;

  // kill all rotamers with self energies worse than thresh
  for(std::vector<RRMRotamerPtr>::iterator i = rotamers_.begin(); 
      i != rotamers_.end(); ++i) {
    if((*i)->GetSelfEnergy() > thresh) {
      i = rotamers_.erase(i);
      --i;
    }
  }

  this->Init();
}


void RRMRotamerGroup::Merge(const RRMRotamerGroupPtr other) {
  for(RRMRotamerGroup::const_iterator i = other->begin(); 
      i != other->end(); ++i) {
    rotamers_.push_back(*i);
  }
  this->Init();
}


void RRMRotamerGroup::Init() {

  int overall_size = 0;
  for(std::vector<RRMRotamerPtr>::iterator i = rotamers_.begin(); 
      i != rotamers_.end(); ++i) {
    overall_size += (*i)->size();
  }

  scoring_parameters_.clear();
  rotamer_indices_.resize(overall_size);

  std::vector<Real> x_particle_positions(overall_size);
  std::vector<Real> y_particle_positions(overall_size);
  std::vector<Real> z_particle_positions(overall_size);
  std::vector<Real> collision_distances(overall_size);

  int actual_position = 0;
  int actual_rotamer = 0;
  geom::Vec3 pos;

  for(std::vector<RRMRotamerPtr>::iterator i = rotamers_.begin(); 
      i != rotamers_.end(); ++i) {

    for(uint j = 0; j < (*i)->size(); ++j) {
      Particle& p = (**i)[j];
      scoring_parameters_.push_back(p.GetSParam());
      rotamer_indices_[actual_position] = actual_rotamer;
      pos = p.GetPos();
      x_particle_positions[actual_position] = pos[0];
      y_particle_positions[actual_position] = pos[1];
      z_particle_positions[actual_position] = pos[2];
      collision_distances[actual_position] = p.GetCollisionDistance();
      ++actual_position;
    }

    ++actual_rotamer;
  }
  collision_tree_.ResetTree(x_particle_positions,
                            y_particle_positions,
                            z_particle_positions,
                            collision_distances);
}


FRMRotamerGroup::FRMRotamerGroup(const std::vector<FRMRotamerPtr>& rotamers,
                                 uint residue_index)
                                 : rotamers_(rotamers)
                                 , residue_index_(residue_index) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                               "FRMRotamerGroup::FRMRotamerGroup", 2);

  if (rotamers.empty()) {
    throw promod3::Error("Cannot construct RRM rotamer group from empty "
                         "rotamer list!");
  }

  this->Init();
}


void FRMRotamerGroup::ApplyOnResidue(uint rotamer_index,
                                     ost::mol::ResidueHandle& res, 
                                     bool consider_hydrogens,
                                     const String& new_res_name) const {
  if (rotamer_index >= rotamers_.size()) {
    throw promod3::Error("Invalid rotamer index");
  }
  rotamers_[rotamer_index]->ApplyOnResidue(res, consider_hydrogens,
                                           new_res_name);
}


void FRMRotamerGroup::ApplyOnResidue(uint rotamer_index,
                                     loop::AllAtomPositions& all_atom,
                                     uint res_idx) const {
  if (rotamer_index >= rotamers_.size()) {
    throw promod3::Error("Invalid rotamer index");
  }
  rotamers_[rotamer_index]->ApplyOnResidue(all_atom, res_idx);
}


void FRMRotamerGroup::Merge(const FRMRotamerGroupPtr other) {
  for(FRMRotamerGroup::const_iterator i = other->begin(); 
      i != other->end(); ++i) {
    rotamers_.push_back(*i);
  }
  this->Init();
}


void FRMRotamerGroup::SetFrameEnergy(FramePtr frame) {

  for(iterator i = this->begin(); i != this->end(); ++i) {
    (*i)->SetFrameEnergy(0);
    for(uint j = 0; j < (*i)->subrotamer_size(); ++j) {
      (*i)->SetFrameEnergy(0,j);
    }
  }
  this->AddFrameEnergy(frame);
}


void FRMRotamerGroup::AddFrameEnergy(FramePtr frame) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "FRMRotamerGroup::AddFrameEnergy", 2);

  std::vector<std::pair<int, int> > overlapping_particles;
  collision_tree_.OverlappingPolytopes(frame->GetCollisionTree(),
                                       overlapping_particles);

  if(overlapping_particles.empty()) return;

  const std::vector<uint>& frame_residue_indices = frame->GetResidueIndices();

  std::vector<int> subrotamer_sizes(this->size());
  int max_subrotamer_size = 0;
  for(uint i = 0; i < this->size(); ++i) {
    subrotamer_sizes[i] = rotamers_[i]->subrotamer_size();
    max_subrotamer_size = std::max(max_subrotamer_size,
                                   subrotamer_sizes[i]);
  }

  promod3::core::EMatXX frame_energies = 
  promod3::core::EMatXX::Zero(this->size(), max_subrotamer_size);

  int counter = 0;
  for(iterator i = this->begin(); i != this->end(); ++i, ++counter) {
    // set the subrotamer frame energies to the already existing
    // energies
    for(int j = 0; j < subrotamer_sizes[counter]; ++j) {
      frame_energies(counter,j) = (*i)->GetFrameEnergy(j);
    }
  }

  std::vector<Real> scores;
  scoring_parameters_.PairwiseScores(frame->GetScoringParameters(), 
                                     overlapping_particles, scores);

  for(uint i = 0; i < scores.size(); ++i) {
    const std::pair<int, int>& idx = overlapping_particles[i];
    if(frame_residue_indices[idx.second] != residue_index_) {
      if(scores[i] != 0.0) {
        int row_idx = rotamer_indices_[idx.first];
        for(FRMRotamer::subrotamer_association_iterator j = 
            ass_ind_b_[idx.first]; j != ass_ind_e_[idx.first]; ++j) {
          frame_energies(row_idx, *j) += scores[i];
        }
      }
    }
  }

  std::vector<double> exp_buffer(max_subrotamer_size, 0.0);
  counter = 0;

  for(iterator i = this->begin(), e = this->end(); i != e; ++i, ++counter) {
    Real T = (*i)->GetTemperature();
    Real one_over_T = 1.0 / T;
    for(int j = 0; j < subrotamer_sizes[counter]; ++j) {
      Real energy = frame_energies(counter, j);
      (*i)->SetFrameEnergy(energy, j);
      exp_buffer[j] = -energy * one_over_T;
    }
    (*i)->SetFrameEnergy(-T*LogSumExp(&exp_buffer[0], 
                                      subrotamer_sizes[counter]));
  }
}


bool FRMRotamerGroup::CalculatePairwiseEnergies(FRMRotamerGroupPtr other,
                                                Real threshold,
                                                promod3::core::EMatXX& emat) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                               "FRMRotamerGroup::CalculatePairwiseEnergies", 2);

  if(!collision_tree_.Overlap(other->GetCollisionTree())) return false;

  std::vector<std::pair<int,int> > overlapping_particles;
  overlapping_particles.reserve(100000);

  collision_tree_.OverlappingPolytopes(other->GetCollisionTree(),
                                       overlapping_particles);

  if(overlapping_particles.empty()) return false;

  int this_size = this->size();
  int other_size = other->size();
  int rotamer_combinations = this_size * other_size;
  emat = promod3::core::EMatXX::Zero(this_size, other_size);

  std::vector<int> other_subrotamer_sizes(other_size);
  int total_other_subrotamers = 0;
  for(int i = 0; i < other_size; ++i) {
    other_subrotamer_sizes[i] = other->rotamers_[i]->subrotamer_size();   
    total_other_subrotamers += other_subrotamer_sizes[i];
  }

  // lets figure out what is the maximum number of subrotamer combinations
  // among all the rotamer combinations
  int max_subrotamers_i = 0;
  int max_subrotamers_j = 0;

  for(int i = 0; i < this_size; ++i) {
    max_subrotamers_i = std::max(max_subrotamers_i,
                        static_cast<int>(rotamers_[i]->subrotamer_size()));
  }

  for(int i = 0; i < other_size; ++i) {
    max_subrotamers_j = std::max(max_subrotamers_j,
                                 other_subrotamer_sizes[i]);
  }

  int max_subrotamer_combinations = max_subrotamers_i * max_subrotamers_j;

  // All pairwise subrotamer energies will be stored in the 
  // subrotamer_energies matrix.
  // In principle this would be a 4 dimensional problem.
  // This is how we store it:
  // Rows: one row per combination of rotamers...
  //       row for combination i,j is at (i * other_size + j) 
  // Cols: the actual subrotamer energies for rotamer pair i,j
  //       it follows the same row-major principle as above
  //       energy for subrotamer combination k, l of rotamers i, j 
  //       is stored at col: k * other_subrotamer_sizes[j] + l
  promod3::core::EMatXX subrotamer_energies = 
  promod3::core::EMatXX::Zero(rotamer_combinations, 
                              max_subrotamer_combinations);

  int r_index_j;
  int e_row_idx, e_col_idx;
  int other_subrotamer_size;
  FRMRotamer::subrotamer_association_iterator subrot_i_b, subrot_i_e,
                                              subrot_j_b, subrot_j_e, temp_it;

  std::vector<Real> scores;
  scoring_parameters_.PairwiseScores(other->scoring_parameters_, 
                                     overlapping_particles, scores);

  for(uint i = 0; i < scores.size(); ++i) {
    if(scores[i] != 0.0) {
      const std::pair<int, int>& idx = overlapping_particles[i];
      // we have to add this energy to all combinations of subrotamers where
      // this pair of particles is involved in...
      r_index_j = other->rotamer_indices_[idx.second];
      e_row_idx = rotamer_indices_[idx.first] * other_size + r_index_j;
      other_subrotamer_size = other_subrotamer_sizes[r_index_j];
      subrot_i_b = ass_ind_b_[idx.first];
      subrot_i_e = ass_ind_e_[idx.first];
      temp_it = other->ass_ind_b_[idx.second];
      subrot_j_e = other->ass_ind_e_[idx.second];

      for(;subrot_i_b != subrot_i_e; ++subrot_i_b) {
        subrot_j_b = temp_it;
        int temp = (*subrot_i_b) * other_subrotamer_size;
        for(;subrot_j_b != subrot_j_e; ++subrot_j_b) {
          subrotamer_energies(e_row_idx, temp + (*subrot_j_b)) += scores[i];
        }
      }
    }
  }

  if(subrotamer_energies.cwiseAbs().maxCoeff() < threshold) {
    return false;
  }

  std::vector<double> exp_buffer(max_subrotamer_combinations, 0.0);
  int a,b;
  Real T;
  Real one_over_T;
  Real T_i;

  Real i_frame_energy;
  Real k_subrotamer_frame_energy;
  std::vector<Real> j_frame_energies(other_size);
  std::vector<Real> j_temperatures(other_size);
  std::vector<Real> other_subrotamer_frame_energies(total_other_subrotamers);

  // do some caching...
  a = 0;
  b = 0;
  for(iterator i = other->begin(); i != other->end(); ++i,++a) {
    j_frame_energies[a] = (*i)->GetFrameEnergy();
    j_temperatures[a] = (*i)->GetTemperature();

    for(uint j = 0; j < (*i)->subrotamer_size(); ++j, ++b) {
      other_subrotamer_frame_energies[b] = (*i)->GetFrameEnergy(j);
    }
  }

  // Everything is in place, the show can start...
  for(int i = 0; i < this_size; ++i) {
    i_frame_energy = rotamers_[i]->GetFrameEnergy();
    a = rotamers_[i]->subrotamer_size();
    T_i = rotamers_[i]->GetTemperature();
    Real* other_subrotamer_frame_energy_ptr = 
    &other_subrotamer_frame_energies[0];
    for(int j = 0; j < other_size; ++j) {
      e_row_idx = i * other_size + j;
      e_col_idx = 0;
      T = 0.5 * (T_i + j_temperatures[j]);
      one_over_T = 1.0/T;
      b = other_subrotamer_sizes[j];
      for(int k = 0; k < a; ++k) {
        k_subrotamer_frame_energy = rotamers_[i]->GetFrameEnergy(k);
        for(int l = 0; l < b; ++l, ++e_col_idx) {
          exp_buffer[e_col_idx] = -(k_subrotamer_frame_energy + 
                                    other_subrotamer_frame_energy_ptr[l] + 
                                    subrotamer_energies(e_row_idx, e_col_idx)) *
                                    one_over_T;
        }
      }

      emat(i, j) = -T*LogSumExp(&exp_buffer[0], a*b) - 
                    i_frame_energy - j_frame_energies[j]; 

      other_subrotamer_frame_energy_ptr += b;
    }
  }

  return true;
}


Real FRMRotamerGroup::GetMaxP() const {
  Real max_p = std::numeric_limits<Real>::min();
  for(const_iterator i = this->begin(); i != this->end(); ++i) {
    max_p = std::max(max_p,(*i)->GetProbability());
  }
  return max_p;
}


void FRMRotamerGroup::ApplySelfEnergyThresh(Real self_energy_thresh) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                               "FRMRotamerGroup::ApplySelfEnergyThresh", 2);

  if(rotamers_.size() <= 1) return;

  bool something_happened = false;
  // find the lowest energy
  Real e_min = std::numeric_limits<Real>::max();
  for(std::vector<FRMRotamerPtr>::iterator i = rotamers_.begin();
      i != rotamers_.end(); ++i) {
    e_min = std::min(e_min,(*i)->GetSelfEnergy());
  }

  // define threshold relative to it
  Real thresh = e_min + self_energy_thresh;

  // kill all rotamers with self energies worse than thresh
  for(std::vector<FRMRotamerPtr>::iterator i = rotamers_.begin(); 
      i != rotamers_.end(); ++i) {
    if((*i)->GetSelfEnergy() > thresh) {
      i = rotamers_.erase(i);
      --i;
      something_happened = true;
    }
  }

  if(something_happened) {
    this->Init();
  }
}


void FRMRotamerGroup::Init() {

  int overall_size = 0;
  for(std::vector<FRMRotamerPtr>::iterator i = rotamers_.begin(); 
      i != rotamers_.end(); ++i) {
    overall_size += (*i)->size();
  }

  scoring_parameters_.clear();
  rotamer_indices_.resize(overall_size);

  ass_ind_b_.resize(overall_size);
  ass_ind_e_.resize(overall_size);

  std::vector<Real> x_particle_positions(overall_size);
  std::vector<Real> y_particle_positions(overall_size);
  std::vector<Real> z_particle_positions(overall_size);
  std::vector<Real> collision_distances(overall_size);

  int actual_position = 0;
  int actual_rotamer = 0;
  int actual_internal_index;
  geom::Vec3 pos;

  for(std::vector<FRMRotamerPtr>::iterator i = rotamers_.begin(); 
      i != rotamers_.end(); ++i) {

    actual_internal_index = 0;

    for(uint j = 0; j < (*i)->size(); ++j) {
      Particle& p = (**i)[j];
      scoring_parameters_.push_back(p.GetSParam());
      rotamer_indices_[actual_position] = actual_rotamer;
      ass_ind_b_[actual_position] = 
      (*i)->subrotamer_association_begin(actual_internal_index);
      ass_ind_e_[actual_position] = 
      (*i)->subrotamer_association_end(actual_internal_index);
      pos = p.GetPos();
      x_particle_positions[actual_position] = pos[0];
      y_particle_positions[actual_position] = pos[1];
      z_particle_positions[actual_position] = pos[2];
      collision_distances[actual_position] = p.GetCollisionDistance();
      ++actual_position;
      ++actual_internal_index;
    }
    ++actual_rotamer;
  }

  collision_tree_.ResetTree(x_particle_positions,
                            y_particle_positions,
                            z_particle_positions,
                            collision_distances);
}

}} // ns
