// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/sidechain_object_loader.hh>
#include <promod3/config.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3{ namespace sidechain{

BBDepRotamerLibPtr LoadBBDepLib(){
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "sidechain::LoadBBDepLib", 2);
  String path = GetProMod3BinaryPath("sidechain_data", "bb_dep_lib.dat");
  BBDepRotamerLibPtr p = BBDepRotamerLib::Load(path);
  return p;  
}

RotamerLibPtr LoadLib(){
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "sidechain::LoadLib", 2);
  String path = GetProMod3BinaryPath("sidechain_data", "lib.dat");
  RotamerLibPtr p = RotamerLib::Load(path);
  return p;  
}

}}//ns 
