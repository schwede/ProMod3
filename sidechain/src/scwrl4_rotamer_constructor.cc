// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/scwrl4_rotamer_constructor.hh>
#include <promod3/sidechain/scwrl4_particle_scoring.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/message.hh>
#include <promod3/loop/sidechain_atom_constructor.hh>
#include <promod3/core/runtime_profiling.hh>
#include <ost/conop/conop.hh>

namespace promod3 { namespace sidechain {

/// \brief Types of lone pair construction
enum SCWRL4LPRule {
  LONE_PAIR_CARBONYL, LONE_PAIR_COH, LONE_PAIR_CNC
};

/// \brief Info for lone pair construction (always involves 3 particles)
/// -> indices are in same order as particles. The lone pair gets finally added
/// to particle with p_idx
/// A, B and C are indices as they are stored in AllAtomPositions, p_idx is the 
/// index of the particle in the rotamer, where the lone pairs get added
struct SCWRL4LPInfo {
  SCWRL4LPInfo() { }
  SCWRL4LPInfo(int idx_A, int idx_B, int idx_C,
               bool a_ih, bool b_ih, bool c_ih, 
               SCWRL4LPRule r)
               : index_A(idx_A), index_B(idx_B),
                 index_C(idx_C), A_is_hydrogen(a_ih),
                 B_is_hydrogen(b_ih), C_is_hydrogen(c_ih),
                 rule(r) { }
  int index_A;
  int index_B;
  int index_C;
  bool A_is_hydrogen;
  bool B_is_hydrogen;
  bool C_is_hydrogen;
  SCWRL4LPRule rule;
};


// add lone pairs for carbonyl (X-C-O) to O particle p
void _AddLonePairsCarbonyl(const geom::Vec3& x_pos, const geom::Vec3& c_pos,
                           const geom::Vec3& o_pos, 
                           promod3::sidechain::SCWRL4Param* p) {
  geom::Vec3 lone_pair_center_one, lone_pair_center_two;
  promod3::core::ConstructAtomPos(x_pos, c_pos, o_pos, 1.00, 2.0944,
                                  M_PI, lone_pair_center_one);
  promod3::core::ConstructAtomPos(x_pos, c_pos, o_pos, 1.00, 2.0944,
                                  0.0, lone_pair_center_two);
  p->AddLonePair(lone_pair_center_one - o_pos);
  p->AddLonePair(lone_pair_center_two - o_pos);
}


// add lone pairs for C-O-H combos (SER / THR / TYR) to O particle p
void _AddLonePairsCOH(const geom::Vec3& c_pos, const geom::Vec3& o_pos,
                      const geom::Vec3& h_pos, 
                      promod3::sidechain::SCWRL4Param* p) {
  // create a center point between the two lone pair clouds with distance to O
  // equals 1
  const geom::Vec3 center_point
          = o_pos + geom::Normalize(  geom::Normalize(o_pos - h_pos)
                                    + geom::Normalize(o_pos - c_pos));
  // the two lone pair representatives can now be calculated. Note, that they
  // have tetrahedral conformation
  // => a bond length of 1.4150 does the trick... (tan(109.5/2))
  geom::Vec3 lone_pair_center_one, lone_pair_center_two;
  promod3::core::ConstructAtomPos(c_pos, o_pos, center_point, 1.4150, M_PI/2,
                                  M_PI/2, lone_pair_center_one);
  promod3::core::ConstructAtomPos(c_pos, o_pos, center_point, 1.4150, M_PI/2,
                                  -M_PI/2, lone_pair_center_two);
  p->AddLonePair(lone_pair_center_one - o_pos);
  p->AddLonePair(lone_pair_center_two - o_pos);
}

// add lone pairs for C-N-C combos (HIS) to N particle p
void _AddLonePairsCNC(const geom::Vec3& c1_pos, const geom::Vec3& n_pos,
                      const geom::Vec3& c2_pos, 
                      promod3::sidechain::SCWRL4Param* p) {
  p->AddLonePair(  geom::Normalize(n_pos - c1_pos)
                 + geom::Normalize(n_pos - c2_pos));
}

void EvalLonePairRule(promod3::sidechain::SCWRL4Param* p,
                      promod3::sidechain::SCWRL4LPRule rule,
                      const geom::Vec3& a,
                      const geom::Vec3& b,
                      const geom::Vec3& c) {
  switch(rule){
    case promod3::sidechain::LONE_PAIR_CARBONYL: {
      _AddLonePairsCarbonyl(a, b, c, p);
      break;
    }
    case promod3::sidechain::LONE_PAIR_COH: {
      _AddLonePairsCOH(a, b, c, p);
      break;
    }
    case promod3::sidechain::LONE_PAIR_CNC:{
      _AddLonePairsCNC(a, b, c, p);
      break;
    }
  }
}


inline bool _IsHydrogen(const String& element) {
  return element == "H" || element == "D";
}


// forward declaration
struct _AtomInfo;

// container of useful bond infos for each bond of an atom (extracted from compound)
struct _BondInfo {
  // link to other guy
  const _AtomInfo* other;
  // order of bond
  int order;
};

// container of useful atom infos for each atom (extracted from compound)
struct _AtomInfo {
  // link to atom spec
  const ost::conop::AtomSpec* p_atom_spec;
  // how many hydrogens attached to this
  int num_hydrogens;
  // list of non-hydrogen bonds
  std::vector<_BondInfo> non_hydrogen_bonds;
};

// get atom infos vector (same size and indexing as comp->GetAtomSpecs)
std::vector<_AtomInfo> _GetAtomInfos(ost::conop::CompoundPtr comp) {
  const ost::conop::AtomSpecList& atom_specs = comp->GetAtomSpecs(); // vector
  const ost::conop::BondSpecList& bond_specs = comp->GetBondSpecs(); // vector

  // extract info for all non-hydrogens
  std::vector<_AtomInfo> atom_infos(atom_specs.size());
  for (uint i_a = 0; i_a < atom_specs.size(); ++i_a) {
    atom_infos[i_a].p_atom_spec = &atom_specs[i_a];
    atom_infos[i_a].num_hydrogens = 0;
    // we only look at bonds for non-hydrogens
    if (!_IsHydrogen(atom_specs[i_a].element)) {
      // parse bonds
      for (uint i_b = 0; i_b < bond_specs.size(); ++i_b) {
        // are we involved in it? if yes, set other_idx
        int other_idx = -1;
        if (bond_specs[i_b].atom_one == int(i_a)) {
          other_idx = bond_specs[i_b].atom_two;
        } else if (bond_specs[i_b].atom_two == int(i_a)) {
          other_idx = bond_specs[i_b].atom_one;
        }
        if (other_idx >= 0) {
          // is the other one hydrogen?
          if (_IsHydrogen(atom_specs[other_idx].element)) {
            ++atom_infos[i_a].num_hydrogens;
          } else {
            // get out the bond
            _BondInfo bond;
            bond.other = &atom_infos[other_idx];
            bond.order = bond_specs[i_b].order;
            atom_infos[i_a].non_hydrogen_bonds.push_back(bond);
          }
        }
      }
    }
  }
  return atom_infos;
}

// is it a carboxyl? (assumes atom_info is an oxygen!)
// if yes, returned vector contains two atom specs of C and X
typedef std::vector<const ost::conop::AtomSpec*> _AtomSpecCPList;
_AtomSpecCPList _GetCarboxylAtoms(const _AtomInfo& atom_info) {
  _AtomSpecCPList carbonyl_atoms;
  // requirements: O connected (order 2) to C with 2 other connections (X, Y)
  const _AtomInfo* C_info = NULL;
  const _AtomInfo* X_info = NULL;
  if (atom_info.non_hydrogen_bonds.size() == 1
      and atom_info.non_hydrogen_bonds[0].order == 2
      and atom_info.non_hydrogen_bonds[0].other->p_atom_spec->element == "C") {
    C_info = atom_info.non_hydrogen_bonds[0].other;
    // C is ok, X chosen as non-hydrogen
    const std::vector<_BondInfo>& C_bonds = C_info->non_hydrogen_bonds;
    if (C_bonds.size() + C_info->num_hydrogens == 3) {
      for (uint i = 0; i < C_bonds.size(); ++i) {
        const _AtomInfo* other = C_bonds[i].other;
        if (other != &atom_info) {
          X_info = other;
          break;
        }
      }
    }
  }
  // fill carbonyl_atoms
  if (C_info != NULL and X_info != NULL) {
    carbonyl_atoms.push_back(C_info->p_atom_spec);
    carbonyl_atoms.push_back(X_info->p_atom_spec);
  }
  return carbonyl_atoms;
}


struct SCWRL4RotamerParam : public RotamerLookupParam {

  SCWRL4RotamerParam() {

    mode = POLAR_HYDROGEN_MODE;
    sample_his_protonation_states = true;

    // overwrite default FRM prefactors from base class
    ARG_CA_CB_prefactors[0] = -0.87;
    ARG_CB_CG_prefactors[0] = -1.62;
    ARG_CG_CD_prefactors[0] = -1.67;
    ARG_CD_NE_prefactors[0] = -0.73;
    ASN_CA_CB_prefactors[0] = -0.62;
    ASN_CB_CG_prefactors[0] = -1.93;
    ASP_CA_CB_prefactors[0] = -1.59;
    ASP_CB_CG_prefactors[0] = -0.63;
    GLN_CA_CB_prefactors[0] = -1.55;
    GLN_CB_CG_prefactors[0] = -0.53;
    GLN_CG_CD_prefactors[0] = -1.89;
    GLU_CA_CB_prefactors[0] = -0.82;
    GLU_CB_CG_prefactors[0] = -1.57;
    GLU_CG_CD_prefactors[0] = -0.76;
    LYS_CA_CB_prefactors[0] = -1.62;
    LYS_CB_CG_prefactors[0] = -0.99;
    LYS_CG_CD_prefactors[0] = -0.96;
    LYS_CD_CE_prefactors[0] = -1.49;
    SER_CA_CB_prefactors[0] = -0.65;
    SER_CB_OG_prefactors[0] = -2.98;
    CYS_CA_CB_prefactors[0] = -1.69;  
    MET_CA_CB_prefactors[0] = -0.97;
    MET_CB_CG_prefactors[0] = -1.54;
    MET_CG_SD_prefactors[0] = -1.21;
    TRP_CA_CB_prefactors[0] = -1.28;
    TRP_CB_CG_prefactors[0] = -1.48;
    TYR_CA_CB_prefactors[0] = -1.48;
    TYR_CB_CG_prefactors[0] = -0.73;
    TYR_CZ_OH_prefactors[0] = -0.96;
    THR_CA_CB_prefactors[0] = -0.88;
    THR_CB_OG1_prefactors[0] = -0.88;
    VAL_CA_CB_prefactors[0] = -2.09;
    ILE_CA_CB_prefactors[0] = -1.23;
    ILE_CB_CG1_prefactors[0] = -0.98;
    LEU_CA_CB_prefactors[0] = -1.15;
    LEU_CB_CG_prefactors[0] = -1.48;
    HIS_CA_CB_prefactors[0] = -1.84;
    HIS_CB_CG_prefactors[0] = -0.85;
    PHE_CA_CB_prefactors[0] = -1.45;
    PHE_CB_CG_prefactors[0] = -1.35;

    ARG_CA_CB_prefactors[1] = 0.87;
    ARG_CB_CG_prefactors[1] = 1.62;
    ARG_CG_CD_prefactors[1] = 1.67;
    ARG_CD_NE_prefactors[1] = 0.73;
    ASN_CA_CB_prefactors[1] = 0.62;
    ASN_CB_CG_prefactors[1] = 1.93;
    ASP_CA_CB_prefactors[1] = 1.59;
    ASP_CB_CG_prefactors[1] = 0.63;
    GLN_CA_CB_prefactors[1] = 1.55;
    GLN_CB_CG_prefactors[1] = 0.53;
    GLN_CG_CD_prefactors[1] = 1.89;
    GLU_CA_CB_prefactors[1] = 0.82;
    GLU_CB_CG_prefactors[1] = 1.57;
    GLU_CG_CD_prefactors[1] = 0.76;
    LYS_CA_CB_prefactors[1] = 1.62;
    LYS_CB_CG_prefactors[1] = 0.99;
    LYS_CG_CD_prefactors[1] = 0.96;
    LYS_CD_CE_prefactors[1] = 1.49;
    SER_CA_CB_prefactors[1] = 0.65;
    SER_CB_OG_prefactors[1] = 2.98;
    CYS_CA_CB_prefactors[1] = 1.69;  
    MET_CA_CB_prefactors[1] = 0.97;
    MET_CB_CG_prefactors[1] = 1.54;
    MET_CG_SD_prefactors[1] = 1.21;
    TRP_CA_CB_prefactors[1] = 1.28;
    TRP_CB_CG_prefactors[1] = 1.48;
    TYR_CA_CB_prefactors[1] = 1.48;
    TYR_CB_CG_prefactors[1] = 0.73;
    TYR_CZ_OH_prefactors[1] = 0.96;
    THR_CA_CB_prefactors[1] = 0.88;
    THR_CB_OG1_prefactors[1] = 0.88;
    VAL_CA_CB_prefactors[1] = 2.09;
    ILE_CA_CB_prefactors[1] = 1.23;
    ILE_CB_CG1_prefactors[1] = 0.98;
    LEU_CA_CB_prefactors[1] = 1.15;
    LEU_CB_CG_prefactors[1] = 1.48;
    HIS_CA_CB_prefactors[1] = 1.84;
    HIS_CB_CG_prefactors[1] = 0.85;
    PHE_CA_CB_prefactors[1] = 1.45;
    PHE_CB_CG_prefactors[1] = 1.35;

    // overwrite default frm_t from base class
    frm_t[ost::conop::ARG] = 1.23;
    frm_t[ost::conop::ASN] = 1.41;
    frm_t[ost::conop::ASP] = 1.48;
    frm_t[ost::conop::GLN] = 1.32;
    frm_t[ost::conop::GLU] = 0.94;
    frm_t[ost::conop::LYS] = 1.27;
    frm_t[ost::conop::SER] = 3.53;
    frm_t[ost::conop::CYS] = 1.69;
    frm_t[ost::conop::MET] = 1.77;
    frm_t[ost::conop::TRP] = 0.99;
    frm_t[ost::conop::TYR] = 1.96;
    frm_t[ost::conop::THR] = 1.11;
    frm_t[ost::conop::VAL] = 2.20;
    frm_t[ost::conop::ILE] = 2.03;
    frm_t[ost::conop::LEU] = 2.55;
    frm_t[ost::conop::PRO] = 2.62;
    frm_t[ost::conop::HIS] = 1.35;
    frm_t[ost::conop::PHE] = 1.07;

    // overwrite default internal_e_prefactor from base class
    internal_e_prefactor[ost::conop::ARG] = 2.27;
    internal_e_prefactor[ost::conop::ASN] = 1.80;
    internal_e_prefactor[ost::conop::ASP] = 2.44;
    internal_e_prefactor[ost::conop::GLN] = 1.61;
    internal_e_prefactor[ost::conop::GLU] = 1.85;
    internal_e_prefactor[ost::conop::LYS] = 2.13;
    internal_e_prefactor[ost::conop::SER] = 2.78;
    internal_e_prefactor[ost::conop::CYS] = 4.07;
    internal_e_prefactor[ost::conop::MET] = 1.95;
    internal_e_prefactor[ost::conop::TRP] = 3.24;
    internal_e_prefactor[ost::conop::TYR] = 2.00;
    internal_e_prefactor[ost::conop::THR] = 2.96;
    internal_e_prefactor[ost::conop::VAL] = 1.62;
    internal_e_prefactor[ost::conop::ILE] = 2.18;
    internal_e_prefactor[ost::conop::LEU] = 2.25;
    internal_e_prefactor[ost::conop::PRO] = 0.76;
    internal_e_prefactor[ost::conop::HIS] = 2.01;
    internal_e_prefactor[ost::conop::PHE] = 1.71;

    // set all parameters specific to SCWRL4RotamerParam to default values
    for(uint i = 0 ; i < promod3::loop::XXX_NUM_HYDROGENS; ++i) {
      hydrogen_charges_[i] = 0.0;
      polar_anchor_[i] = -1;
    }

    for(uint i = 0; i < promod3::loop::XXX_NUM_ATOMS; ++i) {
      heavy_atom_charges_[i] = 0.0;
      lone_pair_info_idx_[i] = -1;
    }

    // set anchor indices for all polar hydrogens
    polar_anchor_[promod3::loop::ALA_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ALA_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ALA_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ALA_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ARG_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ARG_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ARG_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ARG_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASN_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASN_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASN_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASN_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASP_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASP_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASP_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ASP_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLN_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLN_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLN_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLN_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLU_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLU_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLU_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLU_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LYS_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LYS_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LYS_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LYS_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::SER_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::SER_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::SER_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::SER_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::CYS_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::CYS_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::CYS_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::CYS_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::MET_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::MET_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::MET_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::MET_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TRP_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TRP_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TRP_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TRP_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TYR_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TYR_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TYR_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::TYR_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::THR_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::THR_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::THR_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::THR_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::VAL_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::VAL_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::VAL_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::VAL_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ILE_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ILE_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ILE_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::ILE_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LEU_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LEU_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LEU_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::LEU_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLY_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLY_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLY_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::GLY_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::PRO_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::PRO_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::HIS_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::HIS_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::HIS_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::HIS_H3] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::PHE_H] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::PHE_H1] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::PHE_H2] = promod3::loop::BB_N_INDEX;
    polar_anchor_[promod3::loop::PHE_H3] = promod3::loop::BB_N_INDEX;
    
    polar_anchor_[promod3::loop::ARG_HE] = promod3::loop::ARG_NE_INDEX;
    polar_anchor_[promod3::loop::ARG_HH11] = promod3::loop::ARG_NH1_INDEX; 
    polar_anchor_[promod3::loop::ARG_HH12] = promod3::loop::ARG_NH1_INDEX; 
    polar_anchor_[promod3::loop::ARG_HH21] = promod3::loop::ARG_NH2_INDEX; 
    polar_anchor_[promod3::loop::ARG_HH22] = promod3::loop::ARG_NH2_INDEX; 
    polar_anchor_[promod3::loop::ASN_HD21] = promod3::loop::ASN_ND2_INDEX;
    polar_anchor_[promod3::loop::ASN_HD22] = promod3::loop::ASN_ND2_INDEX;
    polar_anchor_[promod3::loop::GLN_HE21] = promod3::loop::GLN_NE2_INDEX;
    polar_anchor_[promod3::loop::GLN_HE22] = promod3::loop::GLN_NE2_INDEX;
    polar_anchor_[promod3::loop::LYS_HZ1] = promod3::loop::LYS_NZ_INDEX;
    polar_anchor_[promod3::loop::LYS_HZ2] = promod3::loop::LYS_NZ_INDEX;
    polar_anchor_[promod3::loop::LYS_HZ3] = promod3::loop::LYS_NZ_INDEX;
    polar_anchor_[promod3::loop::SER_HG] = promod3::loop::SER_OG_INDEX;
    polar_anchor_[promod3::loop::TRP_HE1] = promod3::loop::TRP_NE1_INDEX;
    polar_anchor_[promod3::loop::TYR_HH] = promod3::loop::TYR_OH_INDEX;
    polar_anchor_[promod3::loop::THR_HG1] = promod3::loop::THR_OG1_INDEX;
    polar_anchor_[promod3::loop::HIS_HD1] = promod3::loop::HIS_ND1_INDEX;
    polar_anchor_[promod3::loop::HIS_HE2] = promod3::loop::HIS_NE2_INDEX;

    // set lone pair infos 
    // Special care has to be taken for the terminal oxygen and
    // histidine
    int o_lp_idx = AddLP(promod3::loop::BB_CA_INDEX, promod3::loop::BB_C_INDEX, 
                         promod3::loop::BB_O_INDEX, false, false, false, 
                         LONE_PAIR_CARBONYL);
    lone_pair_info_idx_[promod3::loop::ALA_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::ARG_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::ASN_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::ASP_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::GLN_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::GLU_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::LYS_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::SER_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::CYS_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::MET_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::TRP_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::TYR_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::THR_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::VAL_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::ILE_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::LEU_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::GLY_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::PRO_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::HIS_O] = o_lp_idx;
    lone_pair_info_idx_[promod3::loop::PHE_O] = o_lp_idx;

    AddLP(ASN, promod3::loop::ASN_OD1_INDEX,
          promod3::loop::ASN_ND2_INDEX, promod3::loop::ASN_CG_INDEX, 
          promod3::loop::ASN_OD1_INDEX, false, false, false, LONE_PAIR_CARBONYL);

    AddLP(ASP, promod3::loop::ASP_OD1_INDEX,
          promod3::loop::ASP_OD2_INDEX, promod3::loop::ASP_CG_INDEX, 
          promod3::loop::ASP_OD1_INDEX, false, false, false, LONE_PAIR_CARBONYL);

    AddLP(ASP, promod3::loop::ASP_OD2_INDEX,
          promod3::loop::ASP_OD1_INDEX, promod3::loop::ASP_CG_INDEX, 
          promod3::loop::ASP_OD2_INDEX, false, false, false, LONE_PAIR_CARBONYL);

    AddLP(GLN, promod3::loop::GLN_OE1_INDEX,
          promod3::loop::GLN_NE2_INDEX, promod3::loop::GLN_CD_INDEX, 
          promod3::loop::GLN_OE1_INDEX, false, false, false, LONE_PAIR_CARBONYL);

    AddLP(GLU, promod3::loop::GLU_OE1_INDEX,
          promod3::loop::GLU_OE2_INDEX, promod3::loop::GLU_CD_INDEX, 
          promod3::loop::GLU_OE1_INDEX, false, false, false, LONE_PAIR_CARBONYL);

    AddLP(GLU, promod3::loop::GLU_OE2_INDEX,
          promod3::loop::GLU_OE1_INDEX, promod3::loop::GLU_CD_INDEX, 
          promod3::loop::GLU_OE2_INDEX, false, false, false, LONE_PAIR_CARBONYL);

    AddLP(SER, promod3::loop::SER_OG_INDEX,
          promod3::loop::BB_CB_INDEX, promod3::loop::SER_OG_INDEX, 
          promod3::loop::SER_HG_INDEX, false, false, true, LONE_PAIR_COH);

    AddLP(TYR, promod3::loop::TYR_OH_INDEX,
          promod3::loop::TYR_CZ_INDEX, promod3::loop::TYR_OH_INDEX, 
          promod3::loop::TYR_HH_INDEX, false, false, true, LONE_PAIR_COH);

    AddLP(THR, promod3::loop::THR_OG1_INDEX,
          promod3::loop::BB_CB_INDEX, promod3::loop::THR_OG1_INDEX, 
          promod3::loop::THR_HG1_INDEX, false, false, true, LONE_PAIR_COH);

    AddLP(HIS, promod3::loop::HIS_NE2_INDEX,
          promod3::loop::HIS_CD2_INDEX, promod3::loop::HIS_NE2_INDEX, 
          promod3::loop::HIS_CE1_INDEX, false, false, false, LONE_PAIR_CNC);

    AddLP(HIS, promod3::loop::HIS_ND1_INDEX,
          promod3::loop::HIS_CG_INDEX, promod3::loop::HIS_ND1_INDEX, 
          promod3::loop::HIS_CE1_INDEX, false, false, false, LONE_PAIR_CNC);

    // set charges for polar hydrogen
    hydrogen_charges_[promod3::loop::ALA_H] = 0.25;
    hydrogen_charges_[promod3::loop::ALA_H1] = 0.35;
    hydrogen_charges_[promod3::loop::ALA_H2] = 0.35;
    hydrogen_charges_[promod3::loop::ALA_H3] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_H] = 0.25;
    hydrogen_charges_[promod3::loop::ARG_H1] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_H2] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_H3] = 0.35;
    hydrogen_charges_[promod3::loop::ASN_H] = 0.25;
    hydrogen_charges_[promod3::loop::ASN_H1] = 0.35;
    hydrogen_charges_[promod3::loop::ASN_H2] = 0.35;
    hydrogen_charges_[promod3::loop::ASN_H3] = 0.35;
    hydrogen_charges_[promod3::loop::ASP_H] = 0.25;
    hydrogen_charges_[promod3::loop::ASP_H1] = 0.35;
    hydrogen_charges_[promod3::loop::ASP_H2] = 0.35;
    hydrogen_charges_[promod3::loop::ASP_H3] = 0.35;
    hydrogen_charges_[promod3::loop::GLN_H] = 0.25;
    hydrogen_charges_[promod3::loop::GLN_H1] = 0.35;
    hydrogen_charges_[promod3::loop::GLN_H2] = 0.35;
    hydrogen_charges_[promod3::loop::GLN_H3] = 0.35;
    hydrogen_charges_[promod3::loop::GLU_H] = 0.25;
    hydrogen_charges_[promod3::loop::GLU_H1] = 0.35;
    hydrogen_charges_[promod3::loop::GLU_H2] = 0.35;
    hydrogen_charges_[promod3::loop::GLU_H3] = 0.35;
    hydrogen_charges_[promod3::loop::LYS_H] = 0.25;
    hydrogen_charges_[promod3::loop::LYS_H1] = 0.35;
    hydrogen_charges_[promod3::loop::LYS_H2] = 0.35;
    hydrogen_charges_[promod3::loop::LYS_H3] = 0.35;
    hydrogen_charges_[promod3::loop::SER_H] = 0.25;
    hydrogen_charges_[promod3::loop::SER_H1] = 0.35;
    hydrogen_charges_[promod3::loop::SER_H2] = 0.35;
    hydrogen_charges_[promod3::loop::SER_H3] = 0.35;
    hydrogen_charges_[promod3::loop::CYS_H] = 0.25;
    hydrogen_charges_[promod3::loop::CYS_H1] = 0.35;
    hydrogen_charges_[promod3::loop::CYS_H2] = 0.35;
    hydrogen_charges_[promod3::loop::CYS_H3] = 0.35;
    hydrogen_charges_[promod3::loop::MET_H] = 0.25;
    hydrogen_charges_[promod3::loop::MET_H1] = 0.35;
    hydrogen_charges_[promod3::loop::MET_H2] = 0.35;
    hydrogen_charges_[promod3::loop::MET_H3] = 0.35;
    hydrogen_charges_[promod3::loop::TRP_H] = 0.25;
    hydrogen_charges_[promod3::loop::TRP_H1] = 0.35;
    hydrogen_charges_[promod3::loop::TRP_H2] = 0.35;
    hydrogen_charges_[promod3::loop::TRP_H3] = 0.35;
    hydrogen_charges_[promod3::loop::TYR_H] = 0.25;
    hydrogen_charges_[promod3::loop::TYR_H1] = 0.35;
    hydrogen_charges_[promod3::loop::TYR_H2] = 0.35;
    hydrogen_charges_[promod3::loop::TYR_H3] = 0.35;
    hydrogen_charges_[promod3::loop::THR_H] = 0.25;
    hydrogen_charges_[promod3::loop::THR_H1] = 0.35;
    hydrogen_charges_[promod3::loop::THR_H2] = 0.35;
    hydrogen_charges_[promod3::loop::THR_H3] = 0.35;
    hydrogen_charges_[promod3::loop::VAL_H] = 0.25;
    hydrogen_charges_[promod3::loop::VAL_H1] = 0.35;
    hydrogen_charges_[promod3::loop::VAL_H2] = 0.35;
    hydrogen_charges_[promod3::loop::VAL_H3] = 0.35;
    hydrogen_charges_[promod3::loop::ILE_H] = 0.25;
    hydrogen_charges_[promod3::loop::ILE_H1] = 0.35;
    hydrogen_charges_[promod3::loop::ILE_H2] = 0.35;
    hydrogen_charges_[promod3::loop::ILE_H3] = 0.35;
    hydrogen_charges_[promod3::loop::LEU_H] = 0.25;
    hydrogen_charges_[promod3::loop::LEU_H1] = 0.35;
    hydrogen_charges_[promod3::loop::LEU_H2] = 0.35;
    hydrogen_charges_[promod3::loop::LEU_H3] = 0.35;
    hydrogen_charges_[promod3::loop::GLY_H] = 0.25;
    hydrogen_charges_[promod3::loop::GLY_H1] = 0.35;
    hydrogen_charges_[promod3::loop::GLY_H2] = 0.35;
    hydrogen_charges_[promod3::loop::GLY_H3] = 0.35;
    hydrogen_charges_[promod3::loop::PRO_H1] = 0.35;
    hydrogen_charges_[promod3::loop::PRO_H2] = 0.35;
    hydrogen_charges_[promod3::loop::HIS_H] = 0.25;
    hydrogen_charges_[promod3::loop::HIS_H1] = 0.35;
    hydrogen_charges_[promod3::loop::HIS_H2] = 0.35;
    hydrogen_charges_[promod3::loop::HIS_H3] = 0.35;
    hydrogen_charges_[promod3::loop::PHE_H] = 0.25;
    hydrogen_charges_[promod3::loop::PHE_H1] = 0.35;
    hydrogen_charges_[promod3::loop::PHE_H2] = 0.35;
    hydrogen_charges_[promod3::loop::PHE_H3] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_HE] = 0.3;
    hydrogen_charges_[promod3::loop::ARG_HH11] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_HH12] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_HH21] = 0.35;
    hydrogen_charges_[promod3::loop::ARG_HH22] = 0.35;
    hydrogen_charges_[promod3::loop::ASN_HD21] = 0.3;
    hydrogen_charges_[promod3::loop::ASN_HD22] = 0.3;
    hydrogen_charges_[promod3::loop::GLN_HE21] = 0.3;
    hydrogen_charges_[promod3::loop::GLN_HE22] = 0.3;
    hydrogen_charges_[promod3::loop::LYS_HZ1] = 0.35;
    hydrogen_charges_[promod3::loop::LYS_HZ2] = 0.35;
    hydrogen_charges_[promod3::loop::LYS_HZ3] = 0.35;
    hydrogen_charges_[promod3::loop::SER_HG] = 0.40;
    hydrogen_charges_[promod3::loop::TRP_HE1] = 0.30;
    hydrogen_charges_[promod3::loop::TYR_HH] = 0.40;
    hydrogen_charges_[promod3::loop::THR_HG1] = 0.40;
    hydrogen_charges_[promod3::loop::HIS_HD1] = 0.30;
    hydrogen_charges_[promod3::loop::HIS_HE2] = 0.30;

    // set charges for hydrogen bond acceptors
    heavy_atom_charges_[promod3::loop::ALA_O] = -0.55;
    heavy_atom_charges_[promod3::loop::ARG_O] = -0.55;
    heavy_atom_charges_[promod3::loop::ASN_O] = -0.55;
    heavy_atom_charges_[promod3::loop::ASP_O] = -0.55;
    heavy_atom_charges_[promod3::loop::GLN_O] = -0.55;
    heavy_atom_charges_[promod3::loop::GLU_O] = -0.55;
    heavy_atom_charges_[promod3::loop::LYS_O] = -0.55;
    heavy_atom_charges_[promod3::loop::SER_O] = -0.55;
    heavy_atom_charges_[promod3::loop::CYS_O] = -0.55;
    heavy_atom_charges_[promod3::loop::MET_O] = -0.55;
    heavy_atom_charges_[promod3::loop::TRP_O] = -0.55;
    heavy_atom_charges_[promod3::loop::TYR_O] = -0.55;
    heavy_atom_charges_[promod3::loop::THR_O] = -0.55;
    heavy_atom_charges_[promod3::loop::VAL_O] = -0.55;
    heavy_atom_charges_[promod3::loop::ILE_O] = -0.55;
    heavy_atom_charges_[promod3::loop::LEU_O] = -0.55;
    heavy_atom_charges_[promod3::loop::GLY_O] = -0.55;
    heavy_atom_charges_[promod3::loop::PRO_O] = -0.55;
    heavy_atom_charges_[promod3::loop::HIS_O] = -0.55;
    heavy_atom_charges_[promod3::loop::PHE_O] = -0.55;
    heavy_atom_charges_[promod3::loop::ASN_OD1] = -0.55;
    heavy_atom_charges_[promod3::loop::ASP_OD1] = -0.60;
    heavy_atom_charges_[promod3::loop::ASP_OD2] = -0.60;
    heavy_atom_charges_[promod3::loop::GLN_OE1] = -0.55;
    heavy_atom_charges_[promod3::loop::GLU_OE1] = -0.60;
    heavy_atom_charges_[promod3::loop::GLU_OE2] = -0.60;
    heavy_atom_charges_[promod3::loop::SER_OG] = -0.65;
    heavy_atom_charges_[promod3::loop::TYR_OH] = -0.65;
    heavy_atom_charges_[promod3::loop::THR_OG1] = -0.65;
    heavy_atom_charges_[promod3::loop::HIS_NE2] = -0.40;
    heavy_atom_charges_[promod3::loop::HIS_ND1] = -0.40;

    // set most common atomtypes by element
    for(int i = 0; i < promod3::loop::XXX_NUM_ATOMS; ++i) {
      particle_types_[i] = CParticle;
      promod3::loop::AminoAcidAtom aaa = promod3::loop::AminoAcidAtom(i);
      const String& ele = 
      promod3::loop::AminoAcidLookup::GetInstance().GetElement(aaa);
      if(ele == "N") {
        particle_types_[i] = NParticle;
      }
      if(ele == "O") {
        particle_types_[i] = OParticle;
      }
      if(ele == "S") {
        particle_types_[i] = SParticle;
      }
    }

    // atoms of following types must be set manually:
    // CH1Particle, CH2Particle, CH3Particle, OCParticle
    particle_types_[promod3::loop::ARG_CG] = CH2Particle;
    particle_types_[promod3::loop::ARG_CD] = CH2Particle;
    particle_types_[promod3::loop::ARG_CB] = CH2Particle;
    particle_types_[promod3::loop::ARG_CA] = CH1Particle;
    particle_types_[promod3::loop::ASN_CB] = CH2Particle;
    particle_types_[promod3::loop::ASN_CA] = CH1Particle;
    particle_types_[promod3::loop::ASP_CB] = CH2Particle;
    particle_types_[promod3::loop::ASP_CA] = CH1Particle;
    particle_types_[promod3::loop::GLN_CG] = CH2Particle;
    particle_types_[promod3::loop::GLN_CB] = CH2Particle;
    particle_types_[promod3::loop::GLN_CA] = CH1Particle;
    particle_types_[promod3::loop::GLU_CG] = CH2Particle;
    particle_types_[promod3::loop::GLU_CB] = CH2Particle;
    particle_types_[promod3::loop::GLU_CA] = CH1Particle;
    particle_types_[promod3::loop::LYS_CG] = CH2Particle;
    particle_types_[promod3::loop::LYS_CD] = CH2Particle;
    particle_types_[promod3::loop::LYS_CE] = CH2Particle;
    particle_types_[promod3::loop::LYS_CB] = CH2Particle;
    particle_types_[promod3::loop::LYS_CA] = CH1Particle;
    particle_types_[promod3::loop::SER_CB] = CH2Particle;
    particle_types_[promod3::loop::SER_CA] = CH1Particle;
    particle_types_[promod3::loop::CYS_CB] = CH2Particle;
    particle_types_[promod3::loop::CYS_CA] = CH1Particle;
    particle_types_[promod3::loop::MET_CG] = CH2Particle;
    particle_types_[promod3::loop::MET_CE] = CH3Particle;
    particle_types_[promod3::loop::MET_CB] = CH2Particle;
    particle_types_[promod3::loop::MET_CA] = CH1Particle;
    particle_types_[promod3::loop::TRP_CB] = CH2Particle;
    particle_types_[promod3::loop::TRP_CA] = CH1Particle;
    particle_types_[promod3::loop::TYR_CB] = CH2Particle;
    particle_types_[promod3::loop::TYR_CA] = CH1Particle;
    particle_types_[promod3::loop::THR_CG2] = CH3Particle;
    particle_types_[promod3::loop::THR_CB] = CH2Particle;
    particle_types_[promod3::loop::THR_CA] = CH1Particle;
    particle_types_[promod3::loop::VAL_CG1] = CH3Particle;
    particle_types_[promod3::loop::VAL_CG2] = CH3Particle;
    particle_types_[promod3::loop::VAL_CB] = CH2Particle;
    particle_types_[promod3::loop::VAL_CA] = CH1Particle;
    particle_types_[promod3::loop::ILE_CG1] = CH2Particle;
    particle_types_[promod3::loop::ILE_CG2] = CH3Particle;
    particle_types_[promod3::loop::ILE_CD1] = CH3Particle;
    particle_types_[promod3::loop::ILE_CB] = CH2Particle;
    particle_types_[promod3::loop::ILE_CA] = CH1Particle;
    particle_types_[promod3::loop::LEU_CG] = CH1Particle;
    particle_types_[promod3::loop::LEU_CD1] = CH3Particle;
    particle_types_[promod3::loop::LEU_CD2] = CH3Particle;
    particle_types_[promod3::loop::LEU_CB] = CH2Particle;
    particle_types_[promod3::loop::LEU_CA] = CH1Particle;
    particle_types_[promod3::loop::PRO_CG] = CH2Particle;
    particle_types_[promod3::loop::PRO_CD] = CH2Particle;
    particle_types_[promod3::loop::PRO_CB] = CH2Particle;
    particle_types_[promod3::loop::PRO_CA] = CH1Particle;
    particle_types_[promod3::loop::HIS_CB] = CH2Particle;
    particle_types_[promod3::loop::HIS_CA] = CH1Particle;
    particle_types_[promod3::loop::PHE_CB] = CH2Particle;
    particle_types_[promod3::loop::PHE_CA] = CH1Particle;
    particle_types_[promod3::loop::ALA_CB] = CH3Particle;
    particle_types_[promod3::loop::ALA_CA] = CH1Particle;
    particle_types_[promod3::loop::GLY_CA] = CH2Particle;
  }  


  static const SCWRL4RotamerParam& Instance() {
    static SCWRL4RotamerParam scwrl4_param;
    return scwrl4_param;
  }


  Real GetHydrogenCharge(RotamerID rot_id, int hydrogen_idx) const{
    ost::conop::AminoAcid aa = RotIDToAA(rot_id);
    promod3::loop::AminoAcidHydrogen aah = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAH(aa, hydrogen_idx);
    return hydrogen_charges_[aah];
  }


  Real GetHeavyAtomCharge(RotamerID rot_id, int atom_idx) const{
    ost::conop::AminoAcid aa = RotIDToAA(rot_id);
    promod3::loop::AminoAcidAtom aaa = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAA(aa, atom_idx);
    return heavy_atom_charges_[aaa];
  }


  int GetPolarAnchor(RotamerID rot_id, int hydrogen_idx) const {
    ost::conop::AminoAcid aa = RotIDToAA(rot_id);
    promod3::loop::AminoAcidHydrogen aah = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAH(aa, hydrogen_idx);
    return polar_anchor_[aah];    
  }


  SCWRL4ParticleType GetParticleType(RotamerID rot_id, int atom_idx) const {
    ost::conop::AminoAcid aa = RotIDToAA(rot_id);
    promod3::loop::AminoAcidAtom aaa = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAA(aa, atom_idx);
    return particle_types_[aaa];
  }


  int GetLPInfoIdx(RotamerID rot_id, int atom_idx) const {

    // this is an ugly hack for histidine... 
    if((rot_id == HSD && atom_idx == promod3::loop::HIS_ND1_INDEX) ||
       (rot_id == HSE && atom_idx == promod3::loop::HIS_NE2_INDEX)) {
      return -1;
    }

    ost::conop::AminoAcid aa = RotIDToAA(rot_id);
    promod3::loop::AminoAcidAtom aaa = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAA(aa, atom_idx);
    return lone_pair_info_idx_[aaa];
  }


  const SCWRL4LPInfo& GetLP(int idx) const {
    return lone_pair_infos_[idx];
  }


  void AddLP(RotamerID id, int p_idx, int idx_a, int idx_b, int idx_c, 
             bool a_ih, bool b_ih, bool c_ih, SCWRL4LPRule r) {
    ost::conop::AminoAcid aa = RotIDToAA(id);
    promod3::loop::AminoAcidAtom aaa = 
    promod3::loop::AminoAcidLookup::GetInstance().GetAAA(aa, p_idx);
    lone_pair_info_idx_[aaa] = lone_pair_infos_.size();
    SCWRL4LPInfo lp(idx_a, idx_b, idx_c, a_ih, b_ih, c_ih, r);
    lone_pair_infos_.push_back(lp);
  }


  int AddLP(int idx_a, int idx_b, int idx_c, 
            bool a_ih, bool b_ih, bool c_ih, SCWRL4LPRule r) {
    SCWRL4LPInfo lp(idx_a, idx_b, idx_c, a_ih, b_ih, c_ih, r);
    lone_pair_infos_.push_back(lp);
    return lone_pair_infos_.size() - 1;
  }
 

  Real hydrogen_charges_[promod3::loop::XXX_NUM_HYDROGENS];
  Real heavy_atom_charges_[promod3::loop::XXX_NUM_ATOMS];
  int polar_anchor_[promod3::loop::XXX_NUM_HYDROGENS];
  int lone_pair_info_idx_[promod3::loop::XXX_NUM_ATOMS];
  std::vector<SCWRL4LPInfo> lone_pair_infos_;
  SCWRL4ParticleType particle_types_[promod3::loop::XXX_NUM_ATOMS];
};


SCWRL4RotamerConstructor::SCWRL4RotamerConstructor(bool cb_in_sidechain): 
                                RotamerConstructor(cb_in_sidechain, 
                                                   SCWRL4RotamerParam::Instance()) { }


FrameResiduePtr SCWRL4RotamerConstructor::ConstructFrameResidue(
          const ost::mol::ResidueHandle& res, uint residue_index) {

  // get atoms and count non-hydrogens (= num. particles to add)
  ost::mol::AtomHandleList atom_list = res.GetAtomList();
  std::vector<Particle> particles;
  for (uint i = 0; i < atom_list.size(); ++i) {
    const String& ele = atom_list[i].GetElement();
    if(_IsHydrogen(ele)) continue;
    Particle p(atom_list[i].GetName(), new SCWRL4Param(CParticle, 
                                                       atom_list[i].GetPos(), 
                                                       0.0));
    particles.push_back(p);
  }

  return boost::make_shared<FrameResidue>(particles, residue_index);
}


// NOTE: this only deals with few possible ligand cases and has not been
//       tested extensively!
FrameResiduePtr SCWRL4RotamerConstructor::ConstructFrameResidueHeuristic(
          const ost::mol::ResidueHandle& res, uint residue_index,
          ost::conop::CompoundLibPtr comp_lib) {

  // get compound and atom infos
  ost::conop::CompoundPtr comp = comp_lib->FindCompound(res.GetName(), 
                                              ost::conop::Compound::PDB);
  // fallback if unknown
  if (!comp) return ConstructFrameResidue(res, residue_index);
  std::vector<_AtomInfo> atom_infos = _GetAtomInfos(comp);

  // get atoms and count non-hydrogens (= num. particles to add)
  ost::mol::AtomHandleList atom_list = res.GetAtomList();
  uint size = 0;
  for (uint i = 0; i < atom_list.size(); ++i) {
    if (!_IsHydrogen(atom_list[i].GetElement())) ++size;
  }

  std::vector<Particle> particles;

  uint p_idx = 0;
  for (uint i = 0; i < atom_list.size(); ++i) {
    // aliases for readability
    const geom::Vec3& atom_pos = atom_list[i].GetPos();
    const String& atom_name = atom_list[i].GetName();
    const String& atom_elem = atom_list[i].GetElement();
    // skip hydrogens
    if (!_IsHydrogen(atom_elem)) {
      // look for it
      int idx = comp->GetAtomSpecIndex(atom_name);
      if (idx >= 0) {
        // create particle according to atom's element
        if (atom_elem == "C") {
          SCWRL4ParticleType p_id = CParticle;
          if      (atom_infos[idx].num_hydrogens == 1) p_id = CH1Particle;
          else if (atom_infos[idx].num_hydrogens == 2) p_id = CH2Particle;
          else if (atom_infos[idx].num_hydrogens == 3) p_id = CH3Particle;

          Particle p(atom_name, new SCWRL4Param(p_id, atom_pos, 0.0));
          particles.push_back(p);
        } else if (atom_elem == "N") {
          Particle p(atom_name, new SCWRL4Param(NParticle, atom_pos, 0.0));
          particles.push_back(p);
        } else if (atom_elem == "O") {
          // carbonyl?
          _AtomSpecCPList carbonyl_atoms = _GetCarboxylAtoms(atom_infos[idx]);
          if (carbonyl_atoms.size() == 2) {
            // charged particle needed for hydrogen bonds
            Particle p(atom_name, new SCWRL4Param(OParticle, atom_pos, -0.6));
            particles.push_back(p);
            // try to get lone pair directions (try name and alt. name)
            ost::mol::AtomHandle c = res.FindAtom(carbonyl_atoms[0]->name);
            if (!c.IsValid()) c = res.FindAtom(carbonyl_atoms[0]->alt_name);
            ost::mol::AtomHandle x = res.FindAtom(carbonyl_atoms[1]->name);
            if (!x.IsValid()) x = res.FindAtom(carbonyl_atoms[1]->alt_name);
            if (c.IsValid() && x.IsValid()) {
              _AddLonePairsCarbonyl(x.GetPos(), c.GetPos(), atom_pos,
                  reinterpret_cast<SCWRL4Param*>(particles[p_idx].GetSParam()));
            }
          } else {
            Particle p(atom_name, new SCWRL4Param(OParticle, atom_pos, 0.0));
            particles.push_back(p);
          }
        } else if (atom_elem == "S") {
          Particle p(atom_name, new SCWRL4Param(SParticle, atom_pos, 0.0));
          particles.push_back(p);
        } else {
          Particle p(atom_name, new SCWRL4Param(CParticle, atom_pos, 0.0));
          particles.push_back(p);
        }
      } else {
        // fallback -> treat as uncharged carbon
        Particle p(atom_name, new SCWRL4Param(CParticle, atom_pos, 0.0));
        particles.push_back(p);
      }
      // particle was created anyways...
      ++p_idx;
    }
  }

  return boost::make_shared<FrameResidue>(particles, residue_index);
}


void SCWRL4RotamerConstructor::AssignInternalEnergies(RRMRotamerGroupPtr group,
                                                      RotamerID id, 
                                                      uint residue_index,
                                                      Real phi, Real psi,
                                                      bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "SCWRL4RotamerConstructor::AssignInternalEnergies", 2);

  Real max_p = group->GetMaxP();
  for(uint i = 0; i < group->size(); ++i){
    RRMRotamerPtr r = (*group)[i];
    Real internal_e_prefactor = r->GetInternalEnergyPrefactor();
    Real probability = r->GetProbability();
    Real e = -internal_e_prefactor * std::log(probability/max_p);
    r->SetInternalEnergy(e);
  }
}


void SCWRL4RotamerConstructor::AssignInternalEnergies(FRMRotamerGroupPtr group,
                                                      RotamerID id, 
                                                      uint residue_index,
                                                      Real phi, Real psi,
                                                      bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "SCWRL4RotamerConstructor::AssignInternalEnergies", 2);

  Real max_p = group->GetMaxP();
  for(uint i = 0; i < group->size(); ++i){
    FRMRotamerPtr r = (*group)[i];
    Real internal_e_prefactor = r->GetInternalEnergyPrefactor();
    Real probability = r->GetProbability();
    Real e = -internal_e_prefactor * std::log(probability/max_p);
    r->SetInternalEnergy(e);
  }
}


void SCWRL4RotamerConstructor::ParametrizeParticle(int at_idx, 
                                                   bool is_hydrogen, 
                                                   Particle& particle) {

  if(is_hydrogen) {

    Real charge = SCWRL4RotamerParam::Instance().GetHydrogenCharge(id_, at_idx);
    SCWRL4Param* p = new SCWRL4Param(HParticle, 
                                     hydrogen_buffer_->GetPos(at_idx), 
                                     charge);
    int polar_anchor = SCWRL4RotamerParam::Instance().GetPolarAnchor(id_, at_idx);
    if(polar_anchor != -1) {
      geom::Vec3 polar_direction = hydrogen_buffer_->GetPos(at_idx) - 
                                   pos_buffer_->GetPos(id_, polar_anchor);
      p->SetPolarDirection(polar_direction);
    }
    particle.SetSParam(p);
  } else {

    // deal with terminal special cases
    if(at_idx == -1) {
      // manual setup
      SCWRL4Param* p = new SCWRL4Param(OParticle, terminal_o_pos_,  -0.55);
      _AddLonePairsCarbonyl(pos_buffer_->GetPos(id_, promod3::loop::BB_CA_INDEX), 
                            pos_buffer_->GetPos(id_, promod3::loop::BB_C_INDEX), 
                            terminal_o_pos_, p);
      particle.SetSParam(p);
    } else if(at_idx == -2) {
      // manual setup
      SCWRL4Param* p = new SCWRL4Param(OParticle, terminal_oxt_pos_,  -0.55);
      _AddLonePairsCarbonyl(pos_buffer_->GetPos(id_, promod3::loop::BB_CA_INDEX), 
                            pos_buffer_->GetPos(id_, promod3::loop::BB_C_INDEX), 
                            terminal_oxt_pos_, p);
      particle.SetSParam(p);
    } else {
      SCWRL4ParticleType particle_type = 
      SCWRL4RotamerParam::Instance().GetParticleType(id_, at_idx);
      geom::Vec3 pos = pos_buffer_->GetPos(id_, at_idx);      
      Real charge = 
      SCWRL4RotamerParam::Instance().GetHeavyAtomCharge(id_, at_idx);
      SCWRL4Param* p = new SCWRL4Param(particle_type, pos, charge);

      int lpi_idx = SCWRL4RotamerParam::Instance().GetLPInfoIdx(id_, at_idx);

      // check whether we have a valid lone pair idx, as well as whether we're
      // with a n_terminal nitrogen. The latter also has no lone pair
      //if(lpi_idx != -1 && !(n_ter_ && at_idx == promod3::loop::BB_N_INDEX)) {
      if(lpi_idx != -1) {
        const SCWRL4LPInfo& lp = SCWRL4RotamerParam::Instance().GetLP(lpi_idx);
        geom::Vec3 a, b, c;

        if(lp.A_is_hydrogen) a = hydrogen_buffer_->GetPos(lp.index_A);
        else a = pos_buffer_->GetPos(id_, lp.index_A);
    
        if(lp.B_is_hydrogen) b = hydrogen_buffer_->GetPos(lp.index_B);
        else  b = pos_buffer_->GetPos(id_, lp.index_B);
    
        if(lp.C_is_hydrogen) c = hydrogen_buffer_->GetPos(lp.index_C);
        else c = pos_buffer_->GetPos(id_, lp.index_C);
    
        EvalLonePairRule(p, lp.rule, a, b, c);
      } 
      particle.SetSParam(p);     
    }
  }
}

}} //ns
