// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <promod3/sidechain/rotamer_constructor.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/loop/sidechain_atom_constructor.hh>


#include <promod3/sidechain/scwrl4_particle_scoring.hh>

namespace promod3 { namespace sidechain{

RotamerConstructor::RotamerConstructor(bool cb_in_sidechain, 
                                       const RotamerLookupParam& param):
                                  rotamer_lookup_(cb_in_sidechain, param) {

  String s(XXX,'X');
  for(uint i = 0; i < XXX; ++i){
    s[i] = RotIDToOLC(RotamerID(i));
  }
  pos_buffer_ = boost::make_shared<promod3::loop::AllAtomPositions>(s);
  hydrogen_buffer_ = boost::make_shared<promod3::loop::HydrogenStorage>();
}

// PUBLICLY ACCESSIBLE QUERY FUNCTIONS

RRMRotamerGroupPtr RotamerConstructor::ConstructRRMRotamerGroup(
        const ost::mol::ResidueHandle& res, RotamerID id,
        uint residue_idx, RotamerLibPtr rot_lib, Real phi,
        Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct RRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id);
  this->SetPosBuffer(res);
  return this->ConstructRRMGroup(lib_entries, probability_cutoff);
}


RRMRotamerGroupPtr RotamerConstructor::ConstructRRMRotamerGroup(
        const promod3::loop::AllAtomPositions& all_atom, 
        uint aa_res_idx, RotamerID id, uint residue_idx,
        RotamerLibPtr rot_lib, Real phi, Real psi, bool n_ter, bool c_ter,
        Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct RRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id);
  this->SetPosBuffer(all_atom, aa_res_idx);
  return this->ConstructRRMGroup(lib_entries, probability_cutoff);
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMRotamerGroup(
        const ost::mol::ResidueHandle& res, RotamerID id,
        uint residue_idx, RotamerLibPtr rot_lib, Real phi, Real psi,
        bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct FRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id);
  this->SetPosBuffer(res);
  return this->ConstructFRMGroup(lib_entries, probability_cutoff);
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMRotamerGroup(
        const promod3::loop::AllAtomPositions& all_atom, 
        uint aa_res_idx, RotamerID id, uint residue_idx,
        RotamerLibPtr rot_lib, Real phi, Real psi, bool n_ter, bool c_ter,
        Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct FRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id);
  this->SetPosBuffer(all_atom, aa_res_idx);
  return this->ConstructFRMGroup(lib_entries, probability_cutoff);
}


RRMRotamerGroupPtr RotamerConstructor::ConstructRRMRotamerGroup(
        const ost::mol::ResidueHandle& res, RotamerID id,
        uint residue_idx, BBDepRotamerLibPtr rot_lib, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct RRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id, phi, psi);
  this->SetPosBuffer(res);
  return this->ConstructRRMGroup(lib_entries, probability_cutoff);
}


RRMRotamerGroupPtr RotamerConstructor::ConstructRRMRotamerGroup(
        const promod3::loop::AllAtomPositions& all_atom, 
        uint aa_res_idx, RotamerID id, uint residue_idx,
        BBDepRotamerLibPtr rot_lib, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct RRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id, phi, psi);
  this->SetPosBuffer(all_atom, aa_res_idx);
  return this->ConstructRRMGroup(lib_entries, probability_cutoff);
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMRotamerGroup(
        const ost::mol::ResidueHandle& res, RotamerID id,
        uint residue_idx, BBDepRotamerLibPtr rot_lib, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct FRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id, phi, psi);
  this->SetPosBuffer(res);
  return this->ConstructFRMGroup(lib_entries, probability_cutoff);
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMRotamerGroup(
        const promod3::loop::AllAtomPositions& all_atom, 
        uint aa_res_idx, RotamerID id, uint residue_idx,
        BBDepRotamerLibPtr rot_lib, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct FRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> lib_entries = rot_lib->QueryLib(id, phi, psi);
  this->SetPosBuffer(all_atom, aa_res_idx);
  return this->ConstructFRMGroup(lib_entries, probability_cutoff);
}


RRMRotamerGroupPtr RotamerConstructor::ConstructRRMRotamerGroup(
        const ost::mol::ResidueHandle& res, RotamerID id,
        uint residue_idx, 
        std::vector<RotamerLibEntry>& lib_entries, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct RRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> I_LIKE_CHEESE = 
  std::make_pair(&lib_entries[0], lib_entries.size());
  this->SetPosBuffer(res);
  return this->ConstructRRMGroup(I_LIKE_CHEESE, probability_cutoff);
}


RRMRotamerGroupPtr RotamerConstructor::ConstructRRMRotamerGroup(
        const promod3::loop::AllAtomPositions& all_atom, 
        uint aa_res_idx, RotamerID id, uint residue_idx,
        std::vector<RotamerLibEntry>& lib_entries, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct RRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> I_LIKE_CHEESE = 
  std::make_pair(&lib_entries[0], lib_entries.size());
  this->SetPosBuffer(all_atom, aa_res_idx);
  return this->ConstructRRMGroup(I_LIKE_CHEESE, probability_cutoff);
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMRotamerGroup(
        const ost::mol::ResidueHandle& res, RotamerID id,
        uint residue_idx, std::vector<RotamerLibEntry>& lib_entries, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct FRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> I_LIKE_CHEESE = 
  std::make_pair(&lib_entries[0], lib_entries.size());
  this->SetPosBuffer(res);
  return this->ConstructFRMGroup(I_LIKE_CHEESE, probability_cutoff);
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMRotamerGroup(
        const promod3::loop::AllAtomPositions& all_atom, 
        uint aa_res_idx, RotamerID id, uint residue_idx,
        std::vector<RotamerLibEntry>& lib_entries, 
        Real phi, Real psi, bool n_ter, bool c_ter, Real probability_cutoff) {

  if(id == XXX) {
    throw promod3::Error("Cannot construct FRMRotamerGroup for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;
  std::pair<RotamerLibEntry*,uint> I_LIKE_CHEESE = 
  std::make_pair(&lib_entries[0], lib_entries.size());
  this->SetPosBuffer(all_atom, aa_res_idx);
  return this->ConstructFRMGroup(I_LIKE_CHEESE, probability_cutoff);
}


FrameResiduePtr RotamerConstructor::ConstructBackboneFrameResidue(
          const ost::mol::ResidueHandle& res, RotamerID id, uint residue_idx,
          Real phi, Real psi, bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructBackboneFrameResidue", 2);

  if(id == XXX) {
    throw promod3::Error("Cannot construct FrameResidue for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;

  const RotamerInfo& info = rotamer_lookup_.GetBackboneInfo(id);

  for(uint i = 0; i < info.particles.size(); ++i){
    if(!info.particles[i].is_hydrogen){
      ost::mol::AtomHandle a = res.FindAtom(info.particles[i].name);
      if(!a.IsValid()){
        throw promod3::Error("Expected atom " + info.particles[i].name + 
                             " to be present in input residue!");
      }
      pos_buffer_->SetPos(id, info.particles[i].atom_idx, a.GetPos());
    }
  }

  return this->ConstructBackboneFrameResidue();
}


FrameResiduePtr RotamerConstructor::ConstructBackboneFrameResidue(
          const promod3::loop::AllAtomPositions& all_atom, uint aa_res_idx, 
          RotamerID id, uint residue_idx,
          Real phi, Real psi, bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructBackboneFrameResidue", 2);

  if(id == XXX) {
    throw promod3::Error("Cannot construct FrameResidue for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;

  const RotamerInfo& info = rotamer_lookup_.GetBackboneInfo(id);

  for(uint i = 0; i < info.particles.size(); ++i){
    if(!info.particles[i].is_hydrogen){

      if(!all_atom.IsSet(aa_res_idx, info.particles[i].atom_idx)){
        throw promod3::Error("Expected atom " + info.particles[i].name +
                             " to be set in all_atom");
      }

      pos_buffer_->SetPos(id, info.particles[i].atom_idx, 
                          all_atom.GetPos(aa_res_idx, info.particles[i].atom_idx));
    }
  }

  return this->ConstructBackboneFrameResidue();
}


FrameResiduePtr RotamerConstructor::ConstructSidechainFrameResidue(
        const ost::mol::ResidueHandle& res, RotamerID id, uint residue_idx,
        Real phi, Real psi, bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructSidechainFrameResidue", 2);

  if(id == XXX) {
    throw promod3::Error("Cannot construct FrameResidue for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;

  const RotamerInfo& info = rotamer_lookup_.GetSidechainInfo(id);

  for(uint i = 0; i < info.particles.size(); ++i){
    if(!info.particles[i].is_hydrogen){
      ost::mol::AtomHandle a = res.FindAtom(info.particles[i].name);
      if(!a.IsValid()){
        throw promod3::Error("Expected atom " + info.particles[i].name + 
                             " to be present in input residue!");
      }
      pos_buffer_->SetPos(id, info.particles[i].atom_idx, a.GetPos());
    }
  }

  //set bb hydrogen
  if(info.has_hydrogens){
    // to construct hydrogens, we might also need backbone atoms in the buffer
    const RotamerInfo& bb_info = rotamer_lookup_.GetBackboneInfo(id);

    for(uint i = 0; i < bb_info.particles.size(); ++i){
      if(!bb_info.particles[i].is_hydrogen){
        ost::mol::AtomHandle a = res.FindAtom(bb_info.particles[i].name);
        if(!a.IsValid()){
          throw promod3::Error("Expected atom " + bb_info.particles[i].name + 
                               " to be present in input residue!");
        }
        pos_buffer_->SetPos(id, bb_info.particles[i].atom_idx, a.GetPos());
      }
    }

    promod3::loop::ConstructHydrogens(*pos_buffer_, id, *hydrogen_buffer_, 
                                      rotamer_lookup_.GetMode() == POLAR_HYDROGEN_MODE, 
                                      promod3::loop::PROT_STATE_HISH);
  }

  int num_particles = info.particles.size();
  std::vector<Particle> particles(num_particles);

  for(int i = 0; i < num_particles; ++i) {
    const ParticleInfo& pi = info.particles[i];
    particles[i].SetName(pi.name);
    this->ParametrizeParticle(pi.atom_idx, pi.is_hydrogen, particles[i]);
  }

  FrameResiduePtr p = boost::make_shared<FrameResidue>(particles, 
                                                       residue_idx);

  return p;
}


FrameResiduePtr RotamerConstructor::ConstructSidechainFrameResidue(
          const promod3::loop::AllAtomPositions& all_atom, uint aa_res_idx, 
          RotamerID id, uint residue_idx, Real phi, Real psi, 
          bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructSidechainFrameResidue", 2);

  if(id == XXX) {
    throw promod3::Error("Cannot construct FrameResidue for unknown "
                         "rotamer id!");
  }

  id_ = id;
  residue_idx_ = residue_idx;
  phi_ = phi;
  psi_ = psi;
  n_ter_ = n_ter;
  c_ter_ = c_ter;

  const RotamerInfo& info = rotamer_lookup_.GetSidechainInfo(id);

  for(uint i = 0; i < info.particles.size(); ++i){
    if(!info.particles[i].is_hydrogen){
      if(!all_atom.IsSet(aa_res_idx, info.particles[i].atom_idx)){
        throw promod3::Error("Expected atom " + info.particles[i].name +
                             " to be set in all_atom");
      }
      pos_buffer_->SetPos(id, info.particles[i].atom_idx, 
                          all_atom.GetPos(aa_res_idx, info.particles[i].atom_idx));
    }
  }

  //set hydrogens
  if(info.has_hydrogens){
    // to construct hydrogens, we might also need backbone atoms in the buffer
    const RotamerInfo& bb_info = rotamer_lookup_.GetBackboneInfo(id);

    for(uint i = 0; i < bb_info.particles.size(); ++i){
      if(!bb_info.particles[i].is_hydrogen){
        if(!all_atom.IsSet(aa_res_idx, bb_info.particles[i].atom_idx)){
          throw promod3::Error("Expected atom " + bb_info.particles[i].name +
                               " to be set in all_atom");
        }
        pos_buffer_->SetPos(id, bb_info.particles[i].atom_idx, 
                            all_atom.GetPos(aa_res_idx, bb_info.particles[i].atom_idx));
      }
    }

    promod3::loop::ConstructHydrogens(*pos_buffer_, id, *hydrogen_buffer_, 
                                      rotamer_lookup_.GetMode() == POLAR_HYDROGEN_MODE, 
                                      promod3::loop::PROT_STATE_HISH);
  }

  int num_particles = info.particles.size();
  std::vector<Particle> particles(num_particles);

  for(int i = 0; i < num_particles; ++i) {
    const ParticleInfo& pi = info.particles[i];
    particles[i].SetName(pi.name);
    this->ParametrizeParticle(pi.atom_idx, pi.is_hydrogen, particles[i]);
  }

  FrameResiduePtr p = boost::make_shared<FrameResidue>(particles, 
                                                       residue_idx);

  return p; 
}


void RotamerConstructor::AssignInternalEnergies(RRMRotamerGroupPtr group, 
                                                RotamerID id,
                                                uint residue_idx,
                                                Real phi, Real psi,
                                                bool n_ter, bool c_ter) {
  for(uint i = 0; i < group->size(); ++i){
    (*group)[i]->SetInternalEnergy(0.0);
  }
}


void RotamerConstructor::AssignInternalEnergies(FRMRotamerGroupPtr group,
                                                RotamerID id,
                                                uint residue_idx,
                                                Real phi, Real psi,
                                                bool n_ter, bool c_ter) {
  for(uint i = 0; i < group->size(); ++i){
    (*group)[i]->SetInternalEnergy(0.0);
  }
}


RRMRotamerGroupPtr RotamerConstructor::ConstructRRMGroup(
          std::pair<RotamerLibEntry*,uint> lib_entries, 
          Real probability_cutoff) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructRRMGroup", 2);

  std::vector<RRMRotamerPtr> rotamers;
  Real summed_prob = 0.0;

  // in case of his, we have to move the backbone positions in the buffer
  // to the locations defining his in the different protonation states
  if(id_ == HIS && rotamer_lookup_.SampleHISProtonationStates()){
    MVBBPosBuffer(HIS, HSD);
    MVBBPosBuffer(HIS, HSE);
  }

  // in case of alanine and glycine, the rotamer libraries won't have any 
  // entries. For consistency we nevertheless construct rotamers.
  // We simply add a fake RotamerLibEntry in this case that has
  // no sidechain dihedrals set. In case of Alanine we might still have
  // some atoms in (depending on cb_in_sidechain flag)
  std::vector<RotamerLibEntry> fake_rotamers(1);
  if(lib_entries.second == 0 && (id_ == ALA || id_ == GLY)) {
    // we have to make sure, that the according probability really is one!
    fake_rotamers[0].probability = 1.0;
    lib_entries = std::make_pair(&fake_rotamers[0], 1);
  }
  else if(lib_entries.second == 0) {
    throw promod3::Error("Did not find any rotamers in Rotamer Library!");
  }

  for(uint i = 0; i < lib_entries.second; ++i){

    probability_ = lib_entries.first[i].probability;
    summed_prob += probability_;
    chi_angles_[0] = lib_entries.first[i].chi1;
    chi_angles_[1] = lib_entries.first[i].chi2;
    chi_angles_[2] = lib_entries.first[i].chi3;
    chi_angles_[3] = lib_entries.first[i].chi4;

    if(id_ == HIS && rotamer_lookup_.SampleHISProtonationStates()) {
      id_ = HSE;
      rotamers.push_back(ConstructRRM());
      id_ = HSD;
      rotamers.push_back(ConstructRRM());
      id_ = HIS;
    } else if(id_ == SER && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
      chi_angles_[1] = Real(M_PI);
      rotamers.push_back(ConstructRRM());
      chi_angles_[1] = Real(M_PI) / 3.0;
      rotamers.push_back(ConstructRRM());
      chi_angles_[1] = -Real(M_PI) / 3.0;
      rotamers.push_back(ConstructRRM());
    } else if(id_ == THR && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
      chi_angles_[1] = Real(M_PI);
      rotamers.push_back(ConstructRRM());
      chi_angles_[1] = Real(M_PI) / 3.0;
      rotamers.push_back(ConstructRRM());
      chi_angles_[1] = -Real(M_PI) / 3.0;
      rotamers.push_back(ConstructRRM()); 
    } else if(id_ == TYR && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
      chi_angles_[2] = Real(M_PI);
      rotamers.push_back(ConstructRRM());
      chi_angles_[2] = Real(0.0);
      rotamers.push_back(ConstructRRM());
    } else {
      rotamers.push_back(ConstructRRM());
    }

    if(summed_prob >= probability_cutoff && !rotamers.empty()) break;
  } 
  
  RRMRotamerGroupPtr group = boost::make_shared<RRMRotamerGroup>(rotamers,
                                                                 residue_idx_);
  this->AssignInternalEnergies(group, id_, residue_idx_, phi_, psi_, n_ter_, c_ter_);
  return group;
}


FRMRotamerGroupPtr RotamerConstructor::ConstructFRMGroup(
          std::pair<RotamerLibEntry*,uint> lib_entries, 
          Real probability_cutoff) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructFRMGroup", 2);

  std::vector<FRMRotamerPtr> rotamers;
  Real summed_prob = 0.0;

  // in case of his, we have to move the backbone positions in the buffer
  // to the locations defining his in the different protonation states
  if(id_ == HIS && rotamer_lookup_.SampleHISProtonationStates()){
    MVBBPosBuffer(HIS, HSD);
    MVBBPosBuffer(HIS, HSE);
  }

  // in case of alanine and glycine, the rotamer libraries won't have any 
  // entries. For consistency we nevertheless construct rotamers.
  // We simply add a fake RotamerLibEntry in this case that has
  // no sidechain dihedrals set. In case of Alanine we might still have
  // some atoms in (depending on cb_in_sidechain flag)
  std::vector<RotamerLibEntry> fake_rotamers(1);
  if(lib_entries.second == 0 && (id_ == ALA || id_ == GLY)) {
    // we have to make sure, that the according probability really is one!
    fake_rotamers[0].probability = 1.0;
    lib_entries = std::make_pair(&fake_rotamers[0], 1);
  }
  else if(lib_entries.second == 0) {
    throw promod3::Error("Did not find any rotamers in Rotamer Library!");
  }

  for(uint i = 0; i < lib_entries.second; ++i){
    probability_ = lib_entries.first[i].probability;
    summed_prob += probability_;
    chi_angles_[0] = lib_entries.first[i].chi1;
    chi_angles_[1] = lib_entries.first[i].chi2;
    chi_angles_[2] = lib_entries.first[i].chi3;
    chi_angles_[3] = lib_entries.first[i].chi4;
    chi_dev_[0] = lib_entries.first[i].sig1;
    chi_dev_[1] = lib_entries.first[i].sig2;
    chi_dev_[2] = lib_entries.first[i].sig3;
    chi_dev_[3] = lib_entries.first[i].sig4;

    if(id_ == HIS && rotamer_lookup_.SampleHISProtonationStates()) {
      id_ = HSE;
      rotamers.push_back(ConstructFRM());
      id_ = HSD;
      rotamers.push_back(ConstructFRM());
      id_ = HIS;
    } else if(id_ == SER && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
      chi_dev_[1] = Real(0.17453);
      chi_angles_[1] = Real(M_PI);
      rotamers.push_back(ConstructFRM());
      chi_angles_[1] = Real(M_PI) / 3.0;
      rotamers.push_back(ConstructFRM());
      chi_angles_[1] = -Real(M_PI) / 3.0;
      rotamers.push_back(ConstructFRM());
    } else if(id_ == THR && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
      chi_dev_[1] = Real(0.17453);
      chi_angles_[1] = Real(M_PI);
      rotamers.push_back(ConstructFRM());
      chi_angles_[1] = Real(M_PI) / 3.0;
      rotamers.push_back(ConstructFRM());
      chi_angles_[1] = -Real(M_PI) / 3.0;
      rotamers.push_back(ConstructFRM()); 
    } else if(id_ == TYR && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
      chi_dev_[2] = Real(0.17453);
      chi_angles_[2] = Real(M_PI);
      rotamers.push_back(ConstructFRM());
      chi_angles_[2] = Real(0.0);
      rotamers.push_back(ConstructFRM());
    } else {
      rotamers.push_back(ConstructFRM());
    }

    if(summed_prob >= probability_cutoff && !rotamers.empty()) break;
  } 
  
  FRMRotamerGroupPtr group = boost::make_shared<FRMRotamerGroup>(rotamers,
                                                                 residue_idx_);
  this->AssignInternalEnergies(group, id_, residue_idx_, phi_, psi_, n_ter_, c_ter_);

  return group;
}


RRMRotamerPtr RotamerConstructor::ConstructRRM() {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructRRM", 2);

  const RotamerInfo& info = rotamer_lookup_.GetSidechainInfo(id_);

  //set the positions
  promod3::loop::ConstructSidechainAtoms(*pos_buffer_, id_, chi_angles_[0], 
                                         chi_angles_[1], chi_angles_[2],
                                         chi_angles_[3]);

  if(info.has_hydrogens){

    promod3::loop::ConstructHydrogens(*pos_buffer_, id_, *hydrogen_buffer_, 
                                      rotamer_lookup_.GetMode() == POLAR_HYDROGEN_MODE, 
                                      promod3::loop::PROT_STATE_HISH);

    // If there are any custom rules, we apply them now and overrule the
    // default hydrogen construction
    if(!info.custom_hydrogens.empty()){
      for(uint i = 0; i < info.custom_hydrogens.size(); ++i){
        const CustomHydrogenInfo& h_info = info.custom_hydrogens[i];
        geom::Vec3 anchor_one, anchor_two, anchor_three, new_h_pos;
        anchor_one = pos_buffer_->GetPos(id_, h_info.anchor_idx_one);
        anchor_two = pos_buffer_->GetPos(id_, h_info.anchor_idx_two);
        anchor_three = pos_buffer_->GetPos(id_, h_info.anchor_idx_three);
        promod3::core::ConstructAtomPos(anchor_one, anchor_two, anchor_three,
                                        h_info.bond_length, h_info.angle, 
                                        chi_angles_[h_info.chi_idx], new_h_pos);
        hydrogen_buffer_->SetPos(h_info.atom_idx, new_h_pos);
      }
    }
  }

  int num_particles = info.particles.size();
  std::vector<Particle> particles(num_particles);
  for(int i = 0; i < num_particles; ++i) {
    const ParticleInfo& pi = info.particles[i];
    particles[i].SetName(pi.name);
    this->ParametrizeParticle(pi.atom_idx, pi.is_hydrogen, particles[i]);
  }

  RRMRotamerPtr rot = boost::make_shared<RRMRotamer>(particles, 
                                                     probability_, 
                                                     info.internal_e_prefactor);

  return rot; 
}


FRMRotamerPtr RotamerConstructor::ConstructFRM() {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::ConstructFRM", 2);

  const RotamerInfo& info = rotamer_lookup_.GetSidechainInfo(id_);

  //set the positions
  promod3::loop::ConstructSidechainAtoms(*pos_buffer_, id_, chi_angles_[0], 
                                         chi_angles_[1], chi_angles_[2],
                                         chi_angles_[3]);
  if(info.has_hydrogens){
    promod3::loop::ConstructHydrogens(*pos_buffer_, id_, *hydrogen_buffer_, 
                                      rotamer_lookup_.GetMode() == POLAR_HYDROGEN_MODE, 
                                      promod3::loop::PROT_STATE_HISH);
    // If there are any custom rules, we apply them now an overrule the
    // default hydrogen construction
    if(!info.custom_hydrogens.empty()){
      for(uint i = 0; i < info.custom_hydrogens.size(); ++i){
        const CustomHydrogenInfo& h_info = info.custom_hydrogens[i];
        geom::Vec3 anchor_one, anchor_two, anchor_three, new_h_pos;
        anchor_one = pos_buffer_->GetPos(id_, h_info.anchor_idx_one);
        anchor_two = pos_buffer_->GetPos(id_, h_info.anchor_idx_two);
        anchor_three = pos_buffer_->GetPos(id_, h_info.anchor_idx_three);
        promod3::core::ConstructAtomPos(anchor_one, anchor_two, anchor_three,
                                        h_info.bond_length, h_info.angle, 
                                        chi_angles_[h_info.chi_idx], new_h_pos);
        hydrogen_buffer_->SetPos(h_info.atom_idx, new_h_pos);
      }
    }
  }

  int num_rrm_particles = rotamer_lookup_.GetNumSidechainParticles(id_);
  int num_particles = rotamer_lookup_.GetNumFRMSidechainParticles(id_);
  std::vector<Particle> particles(num_particles);

  for(int i = 0; i < num_rrm_particles; ++i) {
    const ParticleInfo& pi = info.particles[i];
    particles[i].SetName(pi.name);
    this->ParametrizeParticle(pi.atom_idx, pi.is_hydrogen, particles[i]);
  }

  // Keep track of the subrotamers
  std::vector<std::vector<int> > subrotamer_definitions;
  std::vector<int> actual_definition;
  actual_definition.resize(num_rrm_particles);
  // Directly add the first particles as first subrotamer
  for(int i = 0; i < num_rrm_particles; ++i) actual_definition[i] = i;
  subrotamer_definitions.push_back(actual_definition);

  const std::vector<FRMRule>& frm_rules = rotamer_lookup_.GetFRMRules(id_);
  geom::Vec3 rot_axis, rot_anchor;
  geom::Vec3 orig_pos, rot_pos;
  geom::Mat4 transform;
  int base_idx = num_rrm_particles;

  for(uint frm_rule_idx = 0; frm_rule_idx < frm_rules.size(); ++frm_rule_idx){
    const FRMRule& frm_rule = frm_rules[frm_rule_idx];
    int num_fix_particles = frm_rule.fix_particles.size();
    int num_rotating_particles = frm_rule.rotating_particles.size();

    // Update the subrotamer definition... all particles that are fixed are 
    // taken from the first subrotamer.
    for(int i = 0; i < num_fix_particles; ++i) {
      actual_definition[i] = frm_rule.fix_particles[i];
    }

    // The data required for the rotation around the specified axis
    rot_anchor = pos_buffer_->GetPos(id_, frm_rule.anchor_idx_two);
    rot_axis = rot_anchor - pos_buffer_->GetPos(id_, frm_rule.anchor_idx_one); 

    // Every rotation results in a new subrotamer
    for(uint prefac_idx = 0; prefac_idx < frm_rule.prefactors.size(); 
        ++prefac_idx){

      // Get the rotation matrix
      Real rot_angle = frm_rule.prefactors[prefac_idx] * chi_dev_[frm_rule_idx];
      transform = promod3::core::RotationAroundLine(rot_axis, rot_anchor,
                                                    rot_angle);

      for(int i = 0; i < num_rotating_particles; ++i){
        int orig_particle_idx = frm_rule.rotating_particles[i];
        int new_particle_idx = base_idx + i;

        // replace the old with the new index...
        actual_definition[orig_particle_idx] = new_particle_idx;

        // construct the rotated particle
        particles[new_particle_idx] = particles[orig_particle_idx];
        particles[new_particle_idx].ApplyTransform(transform);
      }

      subrotamer_definitions.push_back(actual_definition);

      // start of next subrotamer
      base_idx += num_rotating_particles;
    }
  }

  FRMRotamerPtr r = boost::make_shared<FRMRotamer>(particles,
                                                   info.frm_t, probability_,
                                                   info.internal_e_prefactor);



  for(uint i = 0; i < subrotamer_definitions.size(); ++i) {
    r->AddSubrotamerDefinition(subrotamer_definitions[i]);
  }

  return r;
}


FrameResiduePtr RotamerConstructor::ConstructBackboneFrameResidue() {

  const RotamerInfo& info = rotamer_lookup_.GetBackboneInfo(id_);

  // set hydrogens (also enter if its an n_ter because proline would have no 
  // hydrogen)
  if((info.has_hydrogens || n_ter_) && rotamer_lookup_.GetMode() != HEAVY_ATOM_MODE) {
    if(rotamer_lookup_.GetMode() == FULL_ATOMIC_MODE) {
      promod3::loop::ConstructHydrogens(*pos_buffer_, id_, *hydrogen_buffer_,
                                        rotamer_lookup_.GetMode() == POLAR_HYDROGEN_MODE,
                                        promod3::loop::PROT_STATE_HISH);
    }
    if(n_ter_) {
      promod3::loop::ConstructHydrogenNTerminal(*pos_buffer_, id_, 
                                                *hydrogen_buffer_);
    } else {
      promod3::loop::ConstructHydrogenN(*pos_buffer_, id_, phi_, 
                                        *hydrogen_buffer_); 
    }
  }
  if(c_ter_) {
    promod3::core::ConstructCTerminalOxygens(
                            pos_buffer_->GetPos(id_, promod3::loop::BB_C_INDEX),
                            pos_buffer_->GetPos(id_, promod3::loop::BB_CA_INDEX),
                            pos_buffer_->GetPos(id_, promod3::loop::BB_N_INDEX),
                            terminal_o_pos_, terminal_oxt_pos_); 
  } 
  std::vector<Particle> particles;
  if(n_ter_ || c_ter_) {
    RotamerInfo terminal_info = 
    rotamer_lookup_.GetTerminalBackboneInfo(id_, n_ter_, c_ter_);
    int num_particles = terminal_info.particles.size();
    particles.resize(num_particles);
    for(int i = 0; i < num_particles; ++i) {
      ParticleInfo& pi = terminal_info.particles[i];
      particles[i].SetName(pi.name);
      this->ParametrizeParticle(pi.atom_idx, pi.is_hydrogen, particles[i]);
    }
  } else {
    int num_particles = info.particles.size();
    particles.resize(num_particles);
    for(int i = 0; i < num_particles; ++i) {
      const ParticleInfo& pi = info.particles[i];
      particles[i].SetName(pi.name);
      this->ParametrizeParticle(pi.atom_idx, pi.is_hydrogen, particles[i]);
    }    
  }

  FrameResiduePtr p = boost::make_shared<FrameResidue>(particles, residue_idx_);
  return p; 
}


void RotamerConstructor::SetPosBuffer(const ost::mol::ResidueHandle& res) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::SetPosBuffer_residue", 2);

  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle c = res.FindAtom("C");
  ost::mol::AtomHandle o = res.FindAtom("O");

  if(!(n.IsValid() && ca.IsValid() && c.IsValid() && o.IsValid())) {
    throw promod3::Error("All backbone atoms must be valid to construct "
                         "rotamer!");
  }

  pos_buffer_->SetPos(id_, promod3::loop::BB_N_INDEX, n.GetPos());
  pos_buffer_->SetPos(id_, promod3::loop::BB_CA_INDEX, ca.GetPos());
  pos_buffer_->SetPos(id_, promod3::loop::BB_C_INDEX, c.GetPos());
  pos_buffer_->SetPos(id_, promod3::loop::BB_O_INDEX, o.GetPos());

  if(id_ != GLY) {
    if(!cb.IsValid()) {
      throw promod3::Error("All backbone atoms must be valid to construct "
                           "rotamer!");
    }
    pos_buffer_->SetPos(id_, promod3::loop::BB_CB_INDEX, cb.GetPos());
  }
}


void RotamerConstructor::SetPosBuffer(const promod3::loop::AllAtomPositions& all_atom_pos,
                                      uint all_atom_idx) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "RotamerConstructor::SetPosBuffer_all_atom_pos", 2);


  if(!(all_atom_pos.IsSet(all_atom_idx, promod3::loop::BB_N_INDEX) &&
       all_atom_pos.IsSet(all_atom_idx, promod3::loop::BB_CA_INDEX) &&
       all_atom_pos.IsSet(all_atom_idx, promod3::loop::BB_C_INDEX) &&
       all_atom_pos.IsSet(all_atom_idx, promod3::loop::BB_O_INDEX))){
    throw promod3::Error("All backbone atoms must be set to construct "
                         "rotamer!");
  }

  pos_buffer_->SetPos(id_, promod3::loop::BB_N_INDEX, 
                      all_atom_pos.GetPos(all_atom_idx, 
                                          promod3::loop::BB_N_INDEX));
  pos_buffer_->SetPos(id_, promod3::loop::BB_CA_INDEX, 
                      all_atom_pos.GetPos(all_atom_idx, 
                                          promod3::loop::BB_CA_INDEX));
  pos_buffer_->SetPos(id_, promod3::loop::BB_C_INDEX, 
                      all_atom_pos.GetPos(all_atom_idx, 
                                          promod3::loop::BB_C_INDEX));
  pos_buffer_->SetPos(id_, promod3::loop::BB_O_INDEX, 
                      all_atom_pos.GetPos(all_atom_idx, 
                                          promod3::loop::BB_O_INDEX));  

  if(id_ != GLY) {
    if(!all_atom_pos.IsSet(all_atom_idx, promod3::loop::BB_CB_INDEX)) {
      throw promod3::Error("All backbone atoms must be valid to construct "
                           "rotamer!");
    }
    pos_buffer_->SetPos(id_, promod3::loop::BB_CB_INDEX, 
                        all_atom_pos.GetPos(all_atom_idx, 
                                            promod3::loop::BB_CB_INDEX));
  }
}


void RotamerConstructor::MVBBPosBuffer(RotamerID from, RotamerID to) {
  pos_buffer_->SetPos(to, promod3::loop::BB_N_INDEX, 
                      pos_buffer_->GetPos(from, promod3::loop::BB_N_INDEX));
  pos_buffer_->SetPos(to, promod3::loop::BB_CA_INDEX, 
                      pos_buffer_->GetPos(from, promod3::loop::BB_CA_INDEX));
  pos_buffer_->SetPos(to, promod3::loop::BB_C_INDEX, 
                      pos_buffer_->GetPos(from, promod3::loop::BB_C_INDEX));
  pos_buffer_->SetPos(to, promod3::loop::BB_O_INDEX, 
                      pos_buffer_->GetPos(from, promod3::loop::BB_O_INDEX));
  // Even though we officially count CB as a sidechain atom, we still move it
  // since its completely independent of any sidechain dihedral angle and
  // cannot be constructed anyway
  pos_buffer_->SetPos(to, promod3::loop::BB_CB_INDEX, 
                      pos_buffer_->GetPos(from, promod3::loop::BB_CB_INDEX));
}

}} // ns
