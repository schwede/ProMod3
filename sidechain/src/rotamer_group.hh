// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_GROUP_HH
#define PROMOD3_ROTAMER_GROUP_HH

#include <vector>

#include <boost/shared_ptr.hpp>

#include <promod3/core/tetrahedral_polytope.hh>
#include <promod3/core/eigen_types.hh>
#include <promod3/sidechain/rotamer.hh>
#include <promod3/sidechain/frame.hh>
#include <promod3/sidechain/particle_scoring.hh>

namespace promod3 { namespace sidechain {

class RRMRotamerGroup;
class FRMRotamerGroup;
typedef boost::shared_ptr<RRMRotamerGroup> RRMRotamerGroupPtr;
typedef boost::shared_ptr<FRMRotamerGroup> FRMRotamerGroupPtr;


class RRMRotamerGroup{

public:

  RRMRotamerGroup(const std::vector<RRMRotamerPtr>& rotamers, 
                  uint residue_index);

  void Merge(const RRMRotamerGroupPtr other);

  void ApplyOnResidue(uint rotamer_index, ost::mol::ResidueHandle& res, 
                      bool consider_hydrogens = false, 
                      const String& new_res_name = "") const;

  void ApplyOnResidue(uint rotamer_index, loop::AllAtomPositions& all_atom,
                      uint res_idx) const;

  void SetFrameEnergy(FramePtr p);

  void AddFrameEnergy(FramePtr p);

  bool CalculatePairwiseEnergies(RRMRotamerGroupPtr other, Real threshold,
                                 promod3::core::EMatXX& emat);

  Real GetMaxP() const;

  void ApplySelfEnergyThresh(Real self_energy_thresh = 30);

  uint GetResidueIndex() { return residue_index_; }

  const promod3::core::TetrahedralPolytopeTree& GetCollisionTree() const {
    return collision_tree_;
  }

  bool Overlap(RRMRotamerGroupPtr other) const {
    return collision_tree_.Overlap(other->GetCollisionTree());
  }
 
  // vector style access to rotamers

  typedef RRMRotamerPtr value_type;

  RRMRotamerPtr operator[](size_t index) { return rotamers_.at(index); }

  size_t size() const { return rotamers_.size(); }

  typedef std::vector<RRMRotamerPtr>::const_iterator const_iterator;
  typedef std::vector<RRMRotamerPtr>::iterator iterator;
  
  iterator begin() { return rotamers_.begin(); }
  iterator end() { return rotamers_.end(); }

  const_iterator begin() const { return rotamers_.begin(); }
  const_iterator end() const { return rotamers_.end(); }

  const std::vector<RRMRotamerPtr>& GetRotamers() { return rotamers_; }

private:

  void Init();

  std::vector<RRMRotamerPtr> rotamers_;
  PScoringParamContainer scoring_parameters_;
  std::vector<int> rotamer_indices_;
  promod3::core::TetrahedralPolytopeTree collision_tree_;
  uint residue_index_;

};

class FRMRotamerGroup{

public:

  FRMRotamerGroup(const std::vector<FRMRotamerPtr>& rotamers, 
                  uint residue_index);

  virtual ~FRMRotamerGroup() { rotamers_.clear(); }

  void Merge(const FRMRotamerGroupPtr other);

  void ApplyOnResidue(uint rotamer_index, ost::mol::ResidueHandle& res, 
                      bool consider_hydrogens = false, 
                      const String& new_res_name = "") const;

  void ApplyOnResidue(uint rotamer_index, loop::AllAtomPositions& all_atom,
                      uint res_idx) const;

  void SetFrameEnergy(FramePtr p);

  void AddFrameEnergy(FramePtr p);

  bool CalculatePairwiseEnergies(FRMRotamerGroupPtr other, Real threshold,
                                 promod3::core::EMatXX& emat);

  Real GetMaxP() const;

  void ApplySelfEnergyThresh(Real self_energy_thresh = 30);

  uint GetResidueIndex() const { return residue_index_; }

  const promod3::core::TetrahedralPolytopeTree& GetCollisionTree() const {
    return collision_tree_;
  }

  bool Overlap(FRMRotamerGroupPtr other) const {
    return collision_tree_.Overlap(other->GetCollisionTree());
  }

  // vector style access to rotamers
  
  typedef FRMRotamerPtr value_type;

  FRMRotamerPtr operator[](size_t index) { return rotamers_.at(index); }

  size_t size() const { return rotamers_.size(); }

  typedef std::vector<FRMRotamerPtr>::const_iterator const_iterator;
  typedef std::vector<FRMRotamerPtr>::iterator iterator;
  
  iterator begin() { return rotamers_.begin(); }
  iterator end() { return rotamers_.end(); }

  const_iterator begin() const { return rotamers_.begin(); }
  const_iterator end() const { return rotamers_.end(); }

  const std::vector<FRMRotamerPtr>& GetRotamers() { return rotamers_; }

private:

  void Init();
  
  std::vector<FRMRotamerPtr> rotamers_;
  PScoringParamContainer scoring_parameters_;
  std::vector<int> rotamer_indices_;
  std::vector<FRMRotamer::subrotamer_association_iterator> ass_ind_b_;
  std::vector<FRMRotamer::subrotamer_association_iterator> ass_ind_e_;
  promod3::core::TetrahedralPolytopeTree collision_tree_;
  uint residue_index_;
};



}} // ns

#endif
