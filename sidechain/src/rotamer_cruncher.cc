// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/sidechain/rotamer_cruncher.hh>

namespace{

  Real _bin_size = 0.0872664625997;
  Real _margin = 0.349065850399;
  int _num_sampling_points = 9;

  const float _1d_gauss[9] = {
    0.60653066, 0.7548396, 0.8824969, 0.96923323,  1.,
    0.96923323, 0.8824969, 0.7548396, 0.60653066
  };

  const float _2d_gauss[9][9] = {
    {0.36787944, 0.45783336, 0.53526143, 0.58786967, 0.60653066,
     0.58786967, 0.53526143, 0.45783336, 0.36787944},
    {0.45783336, 0.56978282, 0.66614361, 0.73161563, 0.7548396,
     0.73161563, 0.66614361, 0.56978282, 0.45783336},
    {0.53526143, 0.66614361, 0.77880078, 0.85534533, 0.8824969,
     0.85534533, 0.77880078, 0.66614361, 0.53526143},
    {0.58786967, 0.73161563, 0.85534533, 0.93941306, 0.96923323,
     0.93941306, 0.85534533, 0.73161563, 0.58786967},
    {0.60653066, 0.7548396 , 0.8824969 , 0.96923323, 1.,
     0.96923323, 0.8824969 , 0.7548396 , 0.60653066},
    {0.58786967, 0.73161563, 0.85534533, 0.93941306, 0.96923323,
     0.93941306, 0.85534533, 0.73161563, 0.58786967},
    {0.53526143, 0.66614361, 0.77880078, 0.85534533, 0.8824969,
     0.85534533, 0.77880078, 0.66614361, 0.53526143},
    {0.45783336, 0.56978282, 0.66614361, 0.73161563, 0.7548396 ,
     0.73161563, 0.66614361, 0.56978282, 0.45783336},
    {0.36787944, 0.45783336, 0.53526143, 0.58786967, 0.60653066,
     0.58786967, 0.53526143, 0.45783336, 0.36787944},
  };
}

namespace promod3{ namespace sidechain{

RotamerCruncher::RotamerCruncher():readonly_(false) { std::cout<<"construct rotamer cruncher "<<42<<std::endl;}

void RotamerCruncher::Initialize(const String& seqres, const std::vector<Real>& c_weights){
  if(readonly_){
    throw promod3::Error("Cannot reinitialize readonly RotamerCruncher!");
  }
  std::cout<<"start with initialization"<<std::endl;
  seqres_ = seqres;
  c_weights_ = c_weights;
  dimensionality_.clear();
  extracted_rotamers_.clear();
  extracted_rotamers_.resize(seqres_.size());
  for(uint i = 0; i < seqres_.size(); ++i){
    extracted_rotamers_[i].resize(c_weights_.size());
  }

  for(uint i = 0; i < seqres_.size(); ++i){
    switch(seqres_[i]){
      case 'S':{
        dimensionality_.push_back(0);
        break;
      }
      case 'V':{
        dimensionality_.push_back(0);
        break;
      }
      case 'T':{
        dimensionality_.push_back(0);
        break;
      }
      case 'C':{
        dimensionality_.push_back(0);
        break;
      }
      default:{
        dimensionality_.push_back(1);
        break;
      }
    }
  }
}

void RotamerCruncher::Save(const String& filename){

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  uint32_t seqres_size = seqres_.size();
  out_stream.write(reinterpret_cast<char*>(&seqres_size),sizeof(uint32_t));
  out_stream.write(reinterpret_cast<char*>(&seqres_[0]),seqres_size);
  for(uint i = 0; i < merged_rot_densities_.size(); ++i){ 
    uint32_t density_size = merged_rot_densities_[i]->Bins();
    out_stream.write(reinterpret_cast<char*>(&density_size),sizeof(uint32_t));
    uint32_t N = merged_rot_densities_[i]->GetNumRotamers();
    out_stream.write(reinterpret_cast<char*>(&N),sizeof(uint32_t));
    double* merged_density_data = merged_rot_densities_[i]->Data();
    out_stream.write(reinterpret_cast<char*>(merged_density_data),density_size*sizeof(double));
  }
}

RotamerCruncherPtr RotamerCruncher::Load(const String& filename){

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  uint32_t seqres_size;
  in_stream.read(reinterpret_cast<char*>(&seqres_size),sizeof(uint32_t));
  String seqres(seqres_size,'X');
  in_stream.read(reinterpret_cast<char*>(&seqres[0]),seqres_size);

  RotamerCruncherPtr loaded_cruncher(new RotamerCruncher);
  std::vector<Real> empty_vec;
  loaded_cruncher->Initialize(seqres,empty_vec);
  for(uint i = 0; i < seqres_size; ++i){
    uint32_t density_size;
    in_stream.read(reinterpret_cast<char*>(&density_size),sizeof(uint32_t));
    if(loaded_cruncher->dimensionality_[i] == 0){
      loaded_cruncher->merged_rot_densities_.push_back(RotamerDensityPtr(new RotamerDensity1D(density_size)));
    }
    else{
      loaded_cruncher->merged_rot_densities_.push_back(RotamerDensityPtr(new RotamerDensity2D(std::sqrt(density_size))));
    }
    uint32_t N;
    in_stream.read(reinterpret_cast<char*>(&N),sizeof(uint32_t));
    loaded_cruncher->merged_rot_densities_.back()->SetNumRotamers(N);
    double* merged_density_data = loaded_cruncher->merged_rot_densities_.back()->Data();
    in_stream.read(reinterpret_cast<char*>(merged_density_data),density_size*sizeof(double));   
  }
  loaded_cruncher->readonly_ = true;


  return loaded_cruncher;
}


void RotamerCruncher::AddStructure(const ost::mol::ChainHandle& chain, const ost::seq::AlignmentHandle& aln, uint c){
  if(readonly_){
    throw promod3::Error("Cannot add structure to readonly RotamerCruncher!");
  }

  if(c >= c_weights_.size()){
    throw promod3::Error("Invalid cluster index!");
  }
  int seqres_pos = 0;
  ost::seq::ConstSequenceHandle seqres_seq = aln.GetSequence(0);
  if(seqres_seq.GetGaplessString() != seqres_){
    throw promod3::Error("Seqres and target sequence from the alignment are not the same!");
  }
  ost::seq::ConstSequenceHandle templ_seq = aln.GetSequence(1);
  ost::mol::ResidueHandleList res_list = chain.GetResidueList();
  RotamerLibEntryPtr rotamer;
  
  // build naive cluster densities 
  for(int i = 0; i < aln.GetLength(); ++i){
    if (seqres_seq[i] == '-'){
      continue;
    }
    if (templ_seq[i] == '-'){
      ++seqres_pos;
      continue;
    }
    if (seqres_seq[i] == 'A' || seqres_seq[i] == 'G'){
      ++seqres_pos;
      continue;
    }
    if(seqres_seq[i] == templ_seq[i]){
      int residue_index = templ_seq.GetResidueIndex(i);
      
      ost::mol::ResidueHandle residue = res_list[residue_index];

      std::cerr<<residue.GetName()<<" "<<templ_seq[i]<<std::endl;



      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        std::cerr<<"just caught an error"<<std::endl;
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    // include similar sidechains
    else if(seqres_seq[i] == 'N' && templ_seq[i] == 'D'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
      // not sure if this works ...
      rotamer->chi2 -= Real(M_PI);
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'D' && templ_seq[i] == 'N'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
      rotamer->chi2 -= Real(M_PI);
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'R' && templ_seq[i] == 'K'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'K' && templ_seq[i] == 'R'){
      
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'S' && templ_seq[i] == 'T'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'T' && templ_seq[i] == 'S'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'Y' && templ_seq[i] == 'F'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'F' && templ_seq[i] == 'Y'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'E' && templ_seq[i] == 'Q'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }
    else if(seqres_seq[i] == 'Q' && templ_seq[i] == 'E'){
      int residue_index = templ_seq.GetResidueIndex(i);
      ost::mol::ResidueHandle residue = res_list[residue_index];
      try{
        rotamer = RotamerLibEntry::FromResidue(residue);
      } catch(promod3::Error& e){
        ++seqres_pos;
        continue;
      }
      extracted_rotamers_[seqres_pos][c].push_back(*rotamer);
    }


    ++seqres_pos;
  }
}
void RotamerCruncher::MergeDensities(){
  // normalize and apply weight to cluster densities
  if(readonly_){
    throw promod3::Error("Cannot merge densities of readonly RotamerCruncher!");
  }
  merged_rot_densities_.clear();

  for(uint i = 0; i < extracted_rotamers_.size(); ++i){
    if(dimensionality_[i] == 0){
      merged_rot_densities_.push_back(RotamerDensityPtr(new RotamerDensity1D(72)));
    }
    else if(dimensionality_[i] == 1){
      merged_rot_densities_.push_back(RotamerDensityPtr(new RotamerDensity2D(72)));
    }    
    else{
      throw promod3::Error("Invalid dimensionality observed when merging rotamer densities!");
    }
  }

  std::cerr<<"merging probs"<<std::endl;
  RotamerLibEntry actual_rotamer;
  for(uint i = 0; i < extracted_rotamers_.size(); ++i){
    for(uint j = 0; j < extracted_rotamers_[i].size(); ++j){
      if(extracted_rotamers_[i][j].empty()) continue;
      Real p = c_weights_[j] / extracted_rotamers_[i][j].size(); 
      for(uint k = 0; k < extracted_rotamers_[i][j].size(); ++k){
        actual_rotamer = extracted_rotamers_[i][j][k];
        actual_rotamer.probability = p;
        merged_rot_densities_[i]->AddRotamer(actual_rotamer);
        if (seqres_[i]=='Y'||seqres_[i]=='F'){
          actual_rotamer.chi2 -= Real(M_PI);
          merged_rot_densities_[i]->AddRotamer(actual_rotamer);
        }
      }
    }
  }

  for(uint i = 0; i < extracted_rotamers_.size(); ++i){
    merged_rot_densities_[i]->Normalize();
  }
}

Real RotamerCruncher::GetExtractedProbability(uint seqres_pos, Real chi1, Real chi2){

  
  if(seqres_pos >= seqres_.size()){
    throw promod3::Error("Invalid index observed!");
  }

  Real chi_angles[2];
  chi_angles[0] = chi1;

  if(dimensionality_[seqres_pos] == 0){
    return merged_rot_densities_[seqres_pos]->GetP(&chi_angles[0]);
  }
  
  if(chi2 != chi2){
    throw promod3::Error("Chi2 angle must be given!");
  }

  chi_angles[1] = chi2;

  return merged_rot_densities_[seqres_pos]->GetP(&chi_angles[0]);
}

void RotamerCruncher::UpdateRotamerProbabilities(std::vector<RotamerLibEntryPtr>& rotamers, uint seqres_pos, Real sigma){

  std::cerr<<"start to update probs"<<std::endl;

  String seqres = seqres_;
  if(seqres_pos >= seqres.size()){
    std::cerr<<"seq_respos: "<<seqres_pos<<" seqres_size: "<<seqres.size()<<std::endl;
    throw Error("Seqres_pos out of range!");
  }

  uint32_t N = merged_rot_densities_[seqres_pos]->GetNumRotamers();
  Real sippl_prefac_one = (1.0/(N*0.05+1));
  Real sippl_prefac_two = (N*0.05/(N*0.05+1));
  Real chi_angles[2];
  std::vector<Real> density_probs;
  std::vector<Real> original_probs;
  
  //in case of only chi1
  if(dimensionality_[seqres_pos]==0){

    for(std::vector<RotamerLibEntryPtr>::iterator i = rotamers.begin();
        i != rotamers.end(); ++i){

      Real chi1 = (*i)->chi1;
      Real summed_prob = 0.0;

      chi_angles[0] = chi1-_margin;

      for(int j = 0; j < _num_sampling_points; ++j){
        summed_prob += merged_rot_densities_[seqres_pos]->GetP(&chi_angles[0]);
        chi_angles[0] += _bin_size;
      }

      density_probs.push_back(summed_prob*_bin_size);
      original_probs.push_back(((*i)->probability)*_num_sampling_points*_bin_size);
    }
  }

  else{
    for(std::vector<RotamerLibEntryPtr>::iterator i = rotamers.begin();
        i != rotamers.end(); ++i){
  
      Real chi1 = (*i)->chi1;
      Real chi2 = (*i)->chi2;
      Real summed_prob = 0.0;
  
      chi_angles[0] = chi1-_margin;
  
  
  
      for(int j = 0; j<_num_sampling_points; ++j){
        chi_angles[1] = chi2-_margin;
        for(int k = 0; k<_num_sampling_points; ++k){
          summed_prob += merged_rot_densities_[seqres_pos]->GetP(&chi_angles[0]);
          chi_angles[1] += _bin_size;
        }
        chi_angles[0] += _bin_size;
      }
  
      density_probs.push_back(summed_prob*_bin_size*_bin_size);
      original_probs.push_back(((*i)->probability)*_num_sampling_points*_num_sampling_points*_bin_size*_bin_size);
    }
  }

  Real sum = 0.0;

  for(std::vector<Real>::iterator i = density_probs.begin(); i != density_probs.end(); ++i){
    sum += (*i);
  }

  if(sum == 0.0) return;

  for(uint i = 0; i < density_probs.size(); ++i){
    density_probs[i] = density_probs[i]/sum;
  }

  std::vector<Real> sippl_probs;

  for(uint i = 0; i < density_probs.size(); ++i){
    sippl_probs.push_back(sippl_prefac_one*original_probs[i]+sippl_prefac_two*density_probs[i]);
  }

  sum = 0.0;
  for(std::vector<Real>::iterator i = sippl_probs.begin(); i != sippl_probs.end(); ++i){
    sum += (*i);
  }

  for(uint i = 0; i < sippl_probs.size(); ++i){
    //std::cerr<<"2D probability before updating "<<rotamers[i]->probability<<std::endl;
    rotamers[i]->probability = sippl_probs[i]/sum;
    //std::cerr<<"2D probability after updating "<<rotamers[i]->probability<<std::endl;
  }
  /*
  //WITH GAUSS
  //1D
  if(dimensionality_[seqres_pos] == 0){
    Real updated_probabilities_sum = 0.0;
    std::vector<Real> updated_probabilities;
    for(std::vector<RotamerLibEntryPtr>::iterator i = rotamers.begin(); i!=rotamers.end(); ++i){
      Real chi1 = (*i)->chi1;
      Real probability = (*i)->probability;
      chi_angles[0] = chi1-_margin;
      Real sample_sum = 0.0;
      for(int j = 0; j < _num_sampling_points; ++j){
        Real MergedP = merged_rot_densities_[seqres_pos]->GetP(&chi_angles[0]);
        Real CombinedP = sippl_prefac_one*_1d_gauss[j]*probability+sippl_prefac_two*MergedP;
        sample_sum += CombinedP;
        chi_angles[0] += _bin_size;
      }
      Real updated_probability = sample_sum*_bin_size;
      updated_probabilities.push_back(updated_probability);
    }
    if(updated_probabilities_sum == 0.0) return;
    Real Test_sum_of_probabilities = 0.0;
    for(uint i = 0; i < rotamers.size(); ++i){
      rotamers[i]->probability = updated_probabilities[i]/updated_probabilities_sum;
      Test_sum_of_probabilities += updated_probabilities[i]/updated_probabilities_sum;
    }
    if(Test_sum_of_probabilities < 0.999 || Test_sum_of_probabilities > 1.001){
      throw Error("Test_sum_of_probabilities !=1");
    }
  }
  //2D
  else{
    Real updated_probabilities_sum = 0.0;
    std::vector<Real> updated_probabilities;
    for(std::vector<RotamerLibEntryPtr>::iterator i = rotamers.begin(); i != rotamers.end(); ++i){
      Real chi1 = (*i)->chi1;
      Real chi2 = (*i)->chi2;
      chi_angles[0] = chi1-_margin;
      chi_angles[1] = chi2-_margin;
      Real probability = (*i)->probability;
      Real sample_sum = 0.0;
      for(int j = 0; j < _num_sampling_points; ++j){
        chi_angles[1] = chi2-_margin;
        for(int k = 0; k < _num_sampling_points; ++k){
          Real MergedP = merged_rot_densities_[seqres_pos]->GetP(&chi_angles[0]);
          Real CombinedP = sippl_prefac_one*_2d_gauss[j][k]*probability+sippl_prefac_two*MergedP;
          sample_sum += CombinedP;
          chi_angles[1] += _bin_size;
        }
        chi_angles[0] += _bin_size;
      }
      Real updated_probability = sample_sum*_bin_size*_bin_size;
      updated_probabilities_sum += updated_probability;
      updated_probabilities.push_back(updated_probability);
    }
    if(updated_probabilities_sum == 0.0) return;
    Real Test_sum_of_probabilities = 0.0;
    for(uint i = 0; i < rotamers.size(); ++i){
      rotamers[i]->probability = updated_probabilities[i]/updated_probabilities_sum;
      Test_sum_of_probabilities += updated_probabilities[i]/updated_probabilities_sum;
    }
    if(Test_sum_of_probabilities < 0.999 || Test_sum_of_probabilities > 1.001){
      throw Error("Test_sum_of_probabilities != 1");
    }
  }
  */
}

}}//ns
