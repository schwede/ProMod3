// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_PARTICLE_SCORING_BASE_HH
#define PROMOD3_PARTICLE_SCORING_BASE_HH

#include <ost/geom/vec3.hh>
#include <ost/geom/mat4.hh>


namespace promod3{ namespace sidechain{

// the available particle scoring functions
enum PScoringFunction{
SCWRL4,
SCWRL3,
VINA,
INVALID_PSCORING_FUNCTION
};


// abstract base struct to be implemented by all particle scoring function 
// specific parameter classes
struct PScoringParam {

  virtual ~PScoringParam() { }

  virtual const geom::Vec3& GetPos() const = 0;

  virtual Real GetCollisionDistance() const = 0;

  virtual void ApplyTransform(const geom::Mat4& transform) = 0;

  virtual PScoringParam* Copy() const = 0;

  virtual bool EqualTo(PScoringParam* other) const = 0;
  
  virtual PScoringFunction GetScoringFunction() const =  0;
};

}} //ns

#endif
