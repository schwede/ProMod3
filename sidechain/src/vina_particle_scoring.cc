// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/vina_particle_scoring.hh>
#include <promod3/core/message.hh>


namespace promod3 { namespace sidechain {

Real VINAParam::gaussian1_weight_ = -0.035579;
Real VINAParam::gaussian2_weight_ = -0.005156;
Real VINAParam::repulsion_weight_ = 0.840245;
Real VINAParam::hydrophobic_weight_ = -0.035069;
Real VINAParam::hbond_weight_ = -0.587439;

void SetVINAWeightGaussian1(float w) { VINAParam::gaussian1_weight_ = w; }
void SetVINAWeightGaussian2(float w) { VINAParam::gaussian2_weight_ = w; }
void SetVINAWeightRepulsion(float w) { VINAParam::repulsion_weight_ = w; }
void SetVINAWeightHydrophobic(float w) { VINAParam::hydrophobic_weight_ = w; }
void SetVINAWeightHBond(float w) { VINAParam::hbond_weight_ = w; }

Real GetVINAWeightGaussian1() { return VINAParam::gaussian1_weight_; }
Real GetVINAWeightGaussian2() { return VINAParam::gaussian2_weight_; }
Real GetVINAWeightRepulsion() { return VINAParam::repulsion_weight_; }
Real GetVINAWeightHydrophobic() { return VINAParam::hydrophobic_weight_; }
Real GetVINAWeightHBond() { return VINAParam::hbond_weight_; }


VINAParam::VINAParam(VINAParticleType t, 
                     const geom::Vec3& pos): pos_(pos),
                                             radius_(VINAVDWRadii[t]),
                                             collision_distance_(4.0) {
                                              
  if(t==INVALID_VINAParticle) {
    throw promod3::Error("Cannot initialize VINAParam with particle_type "
                         "INVALID_VINAParticle!");
  }

  hydrophobic_ = (t == C_VINAParticle || t == F_VINAParticle ||
                  t == Cl_VINAParticle || t == Br_VINAParticle ||
                  t == I_VINAParticle);

  hbond_acceptor_ = (t == O_A_VINAParticle || t == N_A_VINAParticle ||
                     t == O_AD_VINAParticle || t == N_AD_VINAParticle);

  hbond_donor_ = (t == O_D_VINAParticle || t == N_D_VINAParticle ||
                  t == O_AD_VINAParticle || t == N_AD_VINAParticle);
}


void VINAParam::ApplyTransform(const geom::Mat4& t) {

  // get transformed pos
  Real a = t(0,0)*pos_[0] + t(0,1)*pos_[1] + t(0,2)*pos_[2] + t(0,3);
  Real b = t(1,0)*pos_[0] + t(1,1)*pos_[1] + t(1,2)*pos_[2] + t(1,3);
  Real c = t(2,0)*pos_[0] + t(2,1)*pos_[1] + t(2,2)*pos_[2] + t(2,3);
  pos_ = geom::Vec3(a, b, c);
}


VINAParam* VINAParam::Copy() const {

  VINAParam* return_ptr = new VINAParam(pos_, radius_,
                                        collision_distance_,
                                        hydrophobic_, hbond_acceptor_,
                                        hbond_donor_);
  return return_ptr;
}


bool VINAParam::EqualTo(PScoringParam* other) const {

  if(other == NULL) return false;
  if(other->GetScoringFunction() != VINA) return false;

  // as the other scoring function is also VINA, we can assume that
  // the following dynamic cast is fine...
  VINAParam* p = dynamic_cast<VINAParam*>(other);

  return pos_ == p->pos_ &&
         radius_ == p->radius_ &&
         collision_distance_ == p->collision_distance_ &&
         hydrophobic_ == p->hydrophobic_ &&
         hbond_acceptor_ == p->hbond_acceptor_ &&
         hbond_donor_ == p->hbond_donor_;
}


Real VINAPairwiseScore(VINAParam* p_one, VINAParam* p_two) {

  Real r = geom::Distance(p_one->pos_, p_two->pos_);
  Real d = r - (p_one->radius_ + p_two->radius_);

  if(r > 8.0) {
    return 0.0;
  }

  // do gaussian1
  Real exp_one = d*Real(2.0);
  Real e_gaussian1 = std::exp(-exp_one*exp_one);

  // do gaussian2
  Real exp_two = (d-Real(3.0))*Real(0.5);
  Real e_gaussian2 = std::exp(-exp_two*exp_two);

  // do repulsion
  Real e_repulsion = (d < Real(0.0) ? d*d : 0.0);

  // do hydrophobic
  Real e_hydrophobic = 0.0;
  if(p_one->hydrophobic_ && p_two->hydrophobic_ && d < Real(1.5)) {
    e_hydrophobic = (d < Real(0.5) ? 1.0 : (Real(1.5) - d));
  }

  // do hbond
  Real e_hbond = 0.0;
  if(((p_one->hbond_acceptor_ && p_two->hbond_donor_) ||
      (p_one->hbond_donor_ && p_two->hbond_acceptor_)) && d < Real(0.0)) {
    e_hbond = (d < Real(-0.7) ? 1.0 : (d/Real(-0.7)));
  }

  Real e = VINAParam::gaussian1_weight_ * e_gaussian1 +
           VINAParam::gaussian2_weight_ * e_gaussian2 +
           VINAParam::repulsion_weight_ * e_repulsion +
           VINAParam::hydrophobic_weight_ * e_hydrophobic +
           VINAParam::hbond_weight_ * e_hbond;

  return e;
}

}}//ns
