// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_GRAPH_COMPONENTS_HH
#define PROMOD3_ROTAMER_GRAPH_COMPONENTS_HH

#include <vector>
#include <limits.h>

#include <boost/shared_ptr.hpp>

#include <promod3/sidechain/rotamer_group.hh>
#include <promod3/core/graph_minimizer.hh>


namespace promod3 { namespace sidechain {

class RotamerGraph;
typedef boost::shared_ptr<RotamerGraph> RotamerGraphPtr;


class RotamerGraph : public promod3::core::GraphMinimizer {

public:

  virtual ~RotamerGraph() { }

  template<typename RotamerGroupPtr>
  static RotamerGraphPtr CreateFromList(const std::vector<RotamerGroupPtr>& 
                                        rotamer_groups) {

    core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                  "RotamerGraph::CreateFromList", 2);

    RotamerGraphPtr graph(new RotamerGraph);

    // let's create the nodes
    std::vector<Real> self_energies;
    for(uint i = 0; i < rotamer_groups.size(); ++i) {
      RotamerGroupPtr rot_group = rotamer_groups[i];
      self_energies.resize(rot_group->size());
      for(uint j = 0; j < rot_group->size(); ++j){
        self_energies[j] = (*rot_group)[j]->GetInternalEnergy() + 
                           (*rot_group)[j]->GetFrameEnergy();
      }

      graph->AddNode(self_energies);
    }

    // let's create the edges
    for(uint i = 0; i < rotamer_groups.size(); ++i){
      for(uint j = i+1; j < rotamer_groups.size(); ++j){
        promod3::core::EMatXX emat;
        if(rotamer_groups[i]->CalculatePairwiseEnergies(rotamer_groups[j],
                                                        0.01, emat)) {
          graph->AddEdge(i, j, emat);
        }
      }
    }

    return graph;
  }

private:



};

}}//ns

#endif
