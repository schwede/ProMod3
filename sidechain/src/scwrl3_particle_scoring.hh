// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_SCWRL3_PARTICLE_SCORING_HH
#define PROMOD3_SCWRL3_PARTICLE_SCORING_HH

#include <ost/geom/mat4.hh>
#include <ost/geom/vecmat3_op.hh>
#include <promod3/sidechain/particle_scoring_base.hh>

namespace promod3{ namespace sidechain{

struct SCWRL3Param : public PScoringParam {

  SCWRL3Param(const geom::Vec3& pos, Real r): pos_(pos), radius_(r) { }

  virtual ~SCWRL3Param() { }

  virtual const geom::Vec3& GetPos() const { return pos_; }

  virtual Real GetCollisionDistance() const { return radius_; }

  virtual void ApplyTransform(const geom::Mat4& t);

  virtual SCWRL3Param* Copy() const;

  virtual bool EqualTo(PScoringParam* other) const;

  virtual PScoringFunction GetScoringFunction() const { return SCWRL3; }

  geom::Vec3 pos_;
  Real radius_;
};


Real SCWRL3PairwiseScore(SCWRL3Param* p_one, SCWRL3Param* p_two);

}} //ns

#endif
