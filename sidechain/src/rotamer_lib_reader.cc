// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer_lib_reader.hh>

namespace promod3{ namespace sidechain{

BBDepRotamerLibPtr ReadDunbrackFile(const String& filename){
  
  std::ifstream file;
  file.open((filename.c_str()));

  if(!file){
    std::stringstream ss;
    ss<<"Could not read file. File '";
    ss<<filename<<"' does not exist!";
    throw promod3::Error(ss.str());
  }

  BBDepRotamerLibPtr lib(new BBDepRotamerLib(36,36));

  lib->SetInterpolate(true);

  String line;
  ost::StringRef stringref_line;
  std::vector<ost::StringRef> split_line;


  String name;
  RotamerID id;
  Real chi1,chi2,chi3,chi4,sig1,sig2,sig3,sig4,probability;
  uint r1,r2,r3,r4;
  //angles are in degree in the dunbrack library
  int phi,psi,phi_bin,psi_bin;

  while(file.good()){
    std::getline(file,line);
    stringref_line = ost::StringRef(line.c_str(),line.length());
    split_line = stringref_line.split();
    if(split_line.empty()) continue;
    if(split_line[0][0] == '#') continue;
    if(split_line.size() != 17){
      std::stringstream ss;
      ss<<"Failed in parsing: "<<line<<"\n\n";
      ss<<"Expected exactly 17 columns.";
      throw promod3::Error(ss.str());
    }

    name = split_line[0].str();
    id = TLCToRotID(name);
    if(id == XXX) continue;
    phi = boost::lexical_cast<int>(split_line[1].str());
    psi = boost::lexical_cast<int>(split_line[2].str());

    //the dunbrack 2010 library is kind of circular...
    if(phi == 180 || psi == 180) continue;

    //bring angles in a range [0,360[
    phi+=180;
    psi+=180;

    //get the final angle bins (10 degree steps)
    phi_bin = phi/10;
    psi_bin = psi/10;

    r1  = boost::lexical_cast<uint>(split_line[4].str());
    r2  = boost::lexical_cast<uint>(split_line[5].str());
    r3  = boost::lexical_cast<uint>(split_line[6].str());
    r4  = boost::lexical_cast<uint>(split_line[7].str());
    probability = boost::lexical_cast<Real>(split_line[8].str());

    if(r1 == 0){
      chi1 = std::numeric_limits<Real>::quiet_NaN();
      sig1 = std::numeric_limits<Real>::quiet_NaN();
    }
    else{
      chi1 = boost::lexical_cast<Real>(split_line[9].str())/180.0*M_PI;
      sig1 = boost::lexical_cast<Real>(split_line[13].str())/180.0*M_PI;
    }

    if(r2 == 0){
      chi2 = std::numeric_limits<Real>::quiet_NaN();
      sig2 = std::numeric_limits<Real>::quiet_NaN();
    }
    else{
      chi2 = boost::lexical_cast<Real>(split_line[10].str())/180.0*M_PI;
      sig2 = boost::lexical_cast<Real>(split_line[14].str())/180.0*M_PI;
    }

    if(r3 == 0){
      chi3 = std::numeric_limits<Real>::quiet_NaN();
      sig3 = std::numeric_limits<Real>::quiet_NaN();
    }
    else{
      chi3 = boost::lexical_cast<Real>(split_line[11].str())/180.0*M_PI;
      sig3 = boost::lexical_cast<Real>(split_line[15].str())/180.0*M_PI;
    }
   
    if(r4 == 0){
      chi4 = std::numeric_limits<Real>::quiet_NaN();
      sig4 = std::numeric_limits<Real>::quiet_NaN();
    }
    else{
      chi4 = boost::lexical_cast<Real>(split_line[12].str())/180.0*M_PI;
      sig4 = boost::lexical_cast<Real>(split_line[16].str())/180.0*M_PI;
    }   

    RotamerLibEntry new_rotamer(probability,chi1,chi2,chi3,chi4,
                                sig1,sig2,sig3,sig4);

    lib->AddRotamer(id, r1, r2, r3, r4, phi_bin, psi_bin, new_rotamer);
  }

  lib->MakeStatic();

  return lib;
}


BBDepRotamerLibPtr ReadDaggetFile(const String& filename){
  
  std::ifstream file;
  file.open((filename.c_str()));

  if(!file){
    std::stringstream ss;
    ss<<"Could not read file. File '";
    ss<<filename<<"' does not exist!";
    throw promod3::Error(ss.str());
  }

  BBDepRotamerLibPtr lib(new BBDepRotamerLib(36,36));

  lib->SetInterpolate(false);

  std::map<String,int> conf_mapper;
  conf_mapper["-"] = 0;    
  conf_mapper["g-"] = 1;
  conf_mapper["g+"] = 2;
  conf_mapper["t"] = 3;
  conf_mapper["Cg-"] = 4;
  conf_mapper["Cg+"] = 5;
  conf_mapper["Ct"] = 6;
  conf_mapper["Ng-"] = 7;
  conf_mapper["Ng+"] = 8;
  conf_mapper["Nt"] = 9;
  conf_mapper["Og-"] = 10;
  conf_mapper["Og+"] = 11;
  conf_mapper["Ot"] = 12;
  conf_mapper["g"] = 13;

  std::map<String,int>::iterator conf_it;

  String line;
  ost::StringRef stringref_line;
  std::vector<ost::StringRef> split_line;


  String name;
  RotamerID id;
  Real chi1,chi2,chi3,chi4,sig1,sig2,sig3,sig4,probability;
  uint r1,r2,r3,r4;
  String conf_1, conf_2, conf_3, conf_4;
  String s_chi1, s_chi2, s_chi3, s_chi4;

  //angles are in degree in the dagget library
  int phi,psi,phi_bin,psi_bin;

  sig1 = std::numeric_limits<Real>::quiet_NaN();
  sig2 = std::numeric_limits<Real>::quiet_NaN();
  sig3 = std::numeric_limits<Real>::quiet_NaN();
  sig4 = std::numeric_limits<Real>::quiet_NaN();
  r1 = 0;
  r2 = 0;
  r3 = 0;
  r4 = 0;

  while(file.good()){
    std::getline(file,line);
    stringref_line = ost::StringRef(line.c_str(),line.length());
    split_line = stringref_line.split(',');
    if(split_line.empty()) continue;
    if(split_line[0].size() != 3) continue; //expect three letter code at the beginning
    if(split_line.size() != 14){
      std::stringstream ss;
      ss<<"Failed in parsing: "<<line<<"\n\n";
      ss<<"Expected exactly 14 columns.";
      throw promod3::Error(ss.str());
    }

    name = split_line[0].str();
    if(name == "HID") name = "HSD";//HIS naming issues
    if(name == "HIE") name = "HSE";//HIS naming issues
    if(name == "CYS") name = "CYD";//In Dagget Lib, CYS is the oxidiced cysteine
                                   //(disulfid bonded)
                                   //and CYH the "free" cysteine, there is no
                                   //general cysteine rotamer
    id = TLCToRotID(name);
    if(id == XXX){
      std::stringstream ss;
      ss << "Could not parse residue name \""<<name;
      ss << "\" when reading dagget lib...";
      throw promod3::Error(ss.str());
    }

    phi = boost::lexical_cast<int>(split_line[1].trim().str());
    psi = boost::lexical_cast<int>(split_line[3].trim().str());

    //bring angles in a range [0,360[
    phi+=180;
    psi+=180;

    //get the final angle bins (10 degree steps)
    phi_bin = phi/10;
    psi_bin = psi/10;

    chi1 = std::numeric_limits<Real>::quiet_NaN();
    chi2 = std::numeric_limits<Real>::quiet_NaN();
    chi3 = std::numeric_limits<Real>::quiet_NaN();
    chi4 = std::numeric_limits<Real>::quiet_NaN();

    conf_1 = split_line[5].trim().str();
    conf_2 = split_line[6].trim().str();
    conf_3 = split_line[7].trim().str();
    conf_4 = split_line[8].trim().str();

    conf_it = conf_mapper.find(conf_1);
    if(conf_it == conf_mapper.end()){
      throw promod3::Error("Invalid rot configuration observed!");
    }
    else{
      r1 = conf_it->second;
    }

    conf_it = conf_mapper.find(conf_2);
    if(conf_it == conf_mapper.end()){
      throw promod3::Error("Invalid rot configuration observed!");
    }
    else{
      r2 = conf_it->second;
    }

    conf_it = conf_mapper.find(conf_3);
    if(conf_it == conf_mapper.end()){
      throw promod3::Error("Invalid rot configuration observed!");
    }
    else{
      r3 = conf_it->second;
    }

    conf_it = conf_mapper.find(conf_4);
    if(conf_it == conf_mapper.end()){
      throw promod3::Error("Invalid rot configuration observed!");
    }
    else{
      r4 = conf_it->second;
    }

    probability = boost::lexical_cast<Real>(split_line[13].trim().str())/100.0;

    s_chi1 = split_line[9].trim().str(); 
    s_chi2 = split_line[10].trim().str(); 
    s_chi3 = split_line[11].trim().str(); 
    s_chi4 = split_line[12].trim().str(); 

    if(r1 != 0 && s_chi1 != "-"){
      chi1 = boost::lexical_cast<Real>(s_chi1)/180.0*M_PI;
      if(chi1 > M_PI) chi1 -= (2*M_PI);
    }

    if(r2 != 0 && s_chi2 != "-"){
      chi2 = boost::lexical_cast<Real>(s_chi2)/180.0*M_PI;
      if(chi2 > M_PI) chi2 -= (2*M_PI);
    }

    if(r3 != 0 && s_chi3 != "-"){
      chi3 = boost::lexical_cast<Real>(s_chi3)/180.0*M_PI;
      if(chi3 > M_PI) chi3 -= (2*M_PI);
    }
   
    if(r4 != 0 && s_chi4 != "-"){
      chi4 = boost::lexical_cast<Real>(s_chi4)/180.0*M_PI;
      if(chi4 > M_PI) chi4 -= (2*M_PI);
    }   

    RotamerLibEntry new_rotamer(probability,chi1,chi2,chi3,chi4,
                                sig1,sig2,sig3,sig4);


    lib->AddRotamer(id, r1, r2, r3, r4, phi_bin, psi_bin, new_rotamer);
  }
  lib->MakeStatic();

  return lib;
}

}}
