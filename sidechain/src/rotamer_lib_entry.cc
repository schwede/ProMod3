// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer_lib_entry.hh>

namespace{

Real AngleDiff(Real a, Real b) {

  // enforce proper ranges
  while(a < -Real(M_PI)) a += 2*Real(M_PI);
  while(a > Real(M_PI))  a -= 2*Real(M_PI);
  while(b < -Real(M_PI)) b += 2*Real(M_PI);
  while(b > Real(M_PI))  b -= 2*Real(M_PI);

  Real diff = a - b;  

  if (diff > 0) {
    if (diff > Real(M_PI)) diff = diff - 2*Real(M_PI);  
  } else {
    if (diff < -Real(M_PI)) diff = diff + 2*Real(M_PI);
  }
  return std::fabs(diff);
}

promod3::sidechain::RotamerLibEntryPtr FromARG(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle ne = res.FindAtom("NE");
  ost::mol::AtomHandle cz = res.FindAtom("CZ");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       cd.IsValid() && ne.IsValid() && cz.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd.GetPos());
  p->chi3 = geom::DihedralAngle(cb.GetPos(),cg.GetPos(),cd.GetPos(),ne.GetPos());
  p->chi4 = geom::DihedralAngle(cg.GetPos(),cd.GetPos(),ne.GetPos(),cz.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  p->sig3 = 0.0;
  p->sig4 = 0.0;
  
  return p;
}

promod3::sidechain::RotamerLibEntryPtr FromASN(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle od1 = res.FindAtom("OD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       od1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),od1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromASP(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle od1 = res.FindAtom("OD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       od1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),od1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromGLN(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle oe1 = res.FindAtom("OE1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       cd.IsValid() && oe1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd.GetPos());
  p->chi3 = geom::DihedralAngle(cb.GetPos(),cg.GetPos(),cd.GetPos(),oe1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  p->sig3 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromGLU(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle oe1 = res.FindAtom("OE1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       cd.IsValid() && oe1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd.GetPos());
  p->chi3 = geom::DihedralAngle(cb.GetPos(),cg.GetPos(),cd.GetPos(),oe1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  p->sig3 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromLYS(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");
  ost::mol::AtomHandle ce = res.FindAtom("CE");
  ost::mol::AtomHandle nz = res.FindAtom("NZ");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       cd.IsValid() && ce.IsValid() && nz.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd.GetPos());
  p->chi3 = geom::DihedralAngle(cb.GetPos(),cg.GetPos(),cd.GetPos(),ce.GetPos());
  p->chi4 = geom::DihedralAngle(cg.GetPos(),cd.GetPos(), ce.GetPos(),nz.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  p->sig3 = 0.0;
  p->sig4 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromSER(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle og = res.FindAtom("OG");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && og.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),og.GetPos());
  p->sig1 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromCYS(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle sg = res.FindAtom("SG");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && sg.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),sg.GetPos());
  p->sig1 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromMET(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle sd = res.FindAtom("SD");
  ost::mol::AtomHandle ce = res.FindAtom("CE");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       sd.IsValid() && ce.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),sd.GetPos());
  p->chi3 = geom::DihedralAngle(cb.GetPos(),cg.GetPos(),sd.GetPos(),ce.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  p->sig3 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromTRP(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       cd1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromTYR(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() && 
       cd1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromTHR(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle og1 = res.FindAtom("OG1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && og1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),og1.GetPos());
  p->sig1 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromVAL(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg1 = res.FindAtom("CG1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg1.GetPos());
  p->sig1 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromILE(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg1 = res.FindAtom("CG1");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg1.IsValid() &&
       cd1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg1.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg1.GetPos(),cd1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromLEU(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() &&
       cd1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromPRO(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd = res.FindAtom("CD");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() &&
       cd.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromHIS(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle nd1 = res.FindAtom("ND1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() &&
       nd1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),nd1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

promod3::sidechain::RotamerLibEntryPtr FromPHE(const ost::mol::ResidueHandle& res){
  ost::mol::AtomHandle n = res.FindAtom("N");
  ost::mol::AtomHandle ca = res.FindAtom("CA");
  ost::mol::AtomHandle cb = res.FindAtom("CB");
  ost::mol::AtomHandle cg = res.FindAtom("CG");
  ost::mol::AtomHandle cd1 = res.FindAtom("CD1");

  if(!(n.IsValid() && ca.IsValid() && cb.IsValid() && cg.IsValid() &&
       cd1.IsValid())){
    std::stringstream ss;
    ss << "Could not find all atoms required to get ";
    ss << "all rotamer dihedral angles!";
    throw promod3::Error(ss.str());
  }

  promod3::sidechain::RotamerLibEntryPtr p(new promod3::sidechain::RotamerLibEntry);
  p->chi1 = geom::DihedralAngle(n.GetPos(),ca.GetPos(),cb.GetPos(),cg.GetPos());
  p->chi2 = geom::DihedralAngle(ca.GetPos(),cb.GetPos(),cg.GetPos(),cd1.GetPos());
  p->sig1 = 0.0;
  p->sig2 = 0.0;
  
  return p; 
}

}

namespace promod3 {namespace sidechain{

RotamerLibEntryPtr RotamerLibEntry::FromResidue(const ost::mol::ResidueHandle& res,
                                                RotamerID id){

  if(!res.IsValid()) throw promod3::Error("Cannot build rotamer lib entry from invalid view!");

  switch(id){
    case ARG: return FromARG(res);
    case ASN: return FromASN(res);
    case ASP: return FromASP(res);
    case GLN: return FromGLN(res);
    case GLU: return FromGLU(res);
    case LYS: return FromLYS(res);
    case SER: return FromSER(res);
    case CYS: return FromCYS(res);
    case CYH: return FromCYS(res);
    case CYD: return FromCYS(res);
    case MET: return FromMET(res);
    case TRP: return FromTRP(res);
    case TYR: return FromTYR(res);
    case THR: return FromTHR(res);
    case VAL: return FromVAL(res);
    case ILE: return FromILE(res);
    case LEU: return FromLEU(res);
    case PRO: return FromPRO(res);
    case CPR: return FromPRO(res);
    case TPR: return FromPRO(res);
    case HSD: return FromHIS(res);
    case HSE: return FromHIS(res);
    case HIS: return FromHIS(res);
    case PHE: return FromPHE(res);
    case GLY: {
      RotamerLibEntryPtr p(new RotamerLibEntry);
      return p;
    }
    case ALA:{
      RotamerLibEntryPtr p(new RotamerLibEntry);
      return p;
    }
    case XXX:{
      std::stringstream ss;
      ss << "could not find valid rotamer id for residue of name ";
      ss << res.GetName()<<std::endl; 
      throw promod3::Error(ss.str());    
    }
  }

  RotamerLibEntryPtr p(new RotamerLibEntry);
  return p; //won't be reached anyway, just to make the compiler happy
            //(non void warning)
}

RotamerLibEntryPtr RotamerLibEntry::FromResidue(const ost::mol::ResidueView& res,
                                                RotamerID id){
  if(!res.IsValid()) throw promod3::Error("Cannot build rotamer lib entry from invalid view!");
  return FromResidue(res.GetHandle(),id);
}

RotamerLibEntryPtr RotamerLibEntry::FromResidue(const ost::mol::ResidueHandle& res){

  if(!res.IsValid()){
    throw promod3::Error("Cannot build rotamer lib entry from invalid handle!");
  }
  RotamerID id = TLCToRotID(res.GetName());
  return FromResidue(res,id);
}

RotamerLibEntryPtr RotamerLibEntry::FromResidue(const ost::mol::ResidueView& res){
  if(!res.IsValid()) throw promod3::Error("Cannot build rotamer lib entry from invalid view!");
  return FromResidue(res.GetHandle());
}

bool RotamerLibEntry::IsSimilar(RotamerLibEntryPtr other, Real thresh){

  bool chi_set = chi1 == chi1;
  bool other_chi_set = other->chi1 == other->chi1;

  if(chi_set != other_chi_set) return false; //either both or none are set
  if(!chi_set) return true; //its all fulfilled if the angle is not to be compared
  if(AngleDiff(chi1,other->chi1) > thresh) return false; 

  chi_set = chi2 == chi2;
  other_chi_set = other->chi2 == other->chi2;

  if(chi_set != other_chi_set) return false; //either both or none are set
  if(!chi_set) return true; //its all fulfilled if the angle is not to be compared
  if(AngleDiff(chi2,other->chi2) > thresh) return false; 

  chi_set = chi3 == chi3;
  other_chi_set = other->chi3 == other->chi3;

  if(chi_set != other_chi_set) return false; //either both or none are set
  if(!chi_set) return true; //its all fulfilled if the angle is not to be compared
  if(AngleDiff(chi3,other->chi3) > thresh) return false; 

  chi_set = chi4 == chi4;
  other_chi_set = other->chi4 == other->chi4;

  if(chi_set != other_chi_set) return false; //either both or none are set
  if(!chi_set) return true; //its all fulfilled if the angle is not to be compared
  if(AngleDiff(chi4,other->chi4) > thresh) return false; 

  return true;
}

bool RotamerLibEntry::IsSimilar(RotamerLibEntryPtr other, Real thresh,
                                RotamerID id){
  //check for cases, where chi angle depends on chemically indistinguishable atoms
  switch(id){
    case XXX:{
      throw promod3::Error("Cannot compare rotamer lib entries of unknown residue type!");
    }
    case TYR:{
      Real transformed_chi = chi2+Real(M_PI);
      RotamerLibEntryPtr transformed_entry(new RotamerLibEntry);
      transformed_entry->chi1 = chi1;
      transformed_entry->chi2 = transformed_chi;
      return this->IsSimilar(other,thresh) || 
             transformed_entry->IsSimilar(other,thresh);
    }
    case PHE:{
      Real transformed_chi = chi2+Real(M_PI);
      RotamerLibEntryPtr transformed_entry(new RotamerLibEntry);
      transformed_entry->chi1 = chi1;
      transformed_entry->chi2 = transformed_chi;
      return this->IsSimilar(other,thresh) || 
             transformed_entry->IsSimilar(other,thresh);
    }
    case ASP:{
      Real transformed_chi = chi2+Real(M_PI);
      RotamerLibEntryPtr transformed_entry(new RotamerLibEntry);
      transformed_entry->chi1 = chi1;
      transformed_entry->chi2 = transformed_chi;
      return this->IsSimilar(other,thresh) || 
             transformed_entry->IsSimilar(other,thresh);
    }
    case GLU:{
      Real transformed_chi = chi3+Real(M_PI);
      RotamerLibEntryPtr transformed_entry(new RotamerLibEntry);
      transformed_entry->chi1 = chi1;
      transformed_entry->chi2 = chi2;
      transformed_entry->chi3 = transformed_chi;
      return this->IsSimilar(other,thresh) || 
             transformed_entry->IsSimilar(other,thresh);
    }
    default: return this->IsSimilar(other,thresh);
  }
}

bool RotamerLibEntry::IsSimilar(RotamerLibEntryPtr other, Real thresh,
                                const String& res_name){
  RotamerID id = TLCToRotID(res_name);
  return this->IsSimilar(other,thresh,id);
}

bool RotamerLibEntry::SimilarDihedral(RotamerLibEntryPtr other, uint dihedral_idx, 
                                      Real thresh){
  switch(dihedral_idx){
    case 0:{
      if(chi1!=chi1 || other->chi1!=other->chi1){
        return false;
      }
      return AngleDiff(chi1,other->chi1) < thresh;
    }
    case 1:{
      if(chi2!=chi2 || other->chi2!=other->chi2){
        return false;
      }
      return AngleDiff(chi2,other->chi2) < thresh;
    }
    case 2:{
      if(chi3!=chi3 || other->chi3!=other->chi3){
        return false;
      }
      return AngleDiff(chi3,other->chi3) < thresh;
    }
    case 3:{
      if(chi4!=chi4 || other->chi4!=other->chi4){
        return false;
      }
      return AngleDiff(chi4,other->chi4) < thresh;
    }
    default:{
      throw promod3::Error("Dihedral index must be in [0,3]!");
    }
  }
}

bool RotamerLibEntry::SimilarDihedral(RotamerLibEntryPtr other, uint dihedral_idx, 
                                      Real thresh, RotamerID id){

  switch(dihedral_idx){
    case 0:{
      if(chi1!=chi1 || other->chi1!=other->chi1){
        return false;
      }
      return AngleDiff(chi1,other->chi1) < thresh;
    }
    case 1:{
      if(chi2!=chi2 || other->chi2!=other->chi2){
        return false;
      }
      if(id == TYR || id == ASP || id == PHE){
        Real transformed_chi = chi2 + Real(M_PI);
        return AngleDiff(chi2,other->chi2) < thresh ||
               AngleDiff(transformed_chi,other->chi2) < thresh;
      }
      return AngleDiff(chi2,other->chi2) < thresh;
    }
    case 2:{
      if(chi3!=chi3 || other->chi3!=other->chi3){
        return false;
      }
      if(id == GLU){
        Real transformed_chi = chi3 + Real(M_PI);
        return AngleDiff(chi3,other->chi3) < thresh ||
               AngleDiff(transformed_chi,other->chi3) < thresh;
      }
      return AngleDiff(chi3,other->chi3) < thresh;
    }
    case 3:{
      if(chi4!=chi4 || other->chi4!=other->chi4){
        return false;
      }
      return AngleDiff(chi4,other->chi4) < thresh;
    }
    default:{
      throw promod3::Error("Dihedral index must be in [0,3]!");
    }
  }
}

bool RotamerLibEntry::SimilarDihedral(RotamerLibEntryPtr other, uint dihedral_idx,
                                      Real thresh, const String& res_name){
  RotamerID id = TLCToRotID(res_name);
  return this->SimilarDihedral(other,dihedral_idx,thresh,id);
}

bool HasNonRotameric(RotamerID id) {

  return id == ASN || id == ASP || id == GLN || id == GLU || id == PHE ||
         id == TYR || id == HIS || id == TRP;
}

bool IsRotameric(RotamerID id, int dihedral_idx) {

  if(dihedral_idx == 0) {
    return id!=GLY && id!=ALA && id!=XXX; // all others are rotameric
  }

  if(dihedral_idx == 1) {
    return id == ARG || id == GLN || id == GLU || id == ILE || id == LEU || 
           id == LYS || id == MET;
  }

  if(dihedral_idx == 2) {
    return id == ARG || id == LYS || id == MET;
  }

  if(dihedral_idx == 3) {
    return id == ARG || id == LYS;
  }

  return false;
}

DihedralConfiguration GetRotamericConfiguration(Real angle) {

  if(angle != angle) {
    return INVALID;
  }

  if(angle > Real(0.0) && angle < Real(2.09439510239)) {
    return GAUCHE_PLUS;
  }  

  if(angle < Real(0.0) && angle > Real(-2.09439510239)) {
    return GAUCHE_MINUS;
  }

  return TRANS;  
}

DihedralConfiguration GetDihedralConfiguration(const RotamerLibEntry& entry,
                                               RotamerID id,
                                               uint dihedral_idx) {

  if(dihedral_idx > 3) {
    return INVALID;
  }

  switch(dihedral_idx) {

    case 0: {
      if(entry.chi1 != entry.chi1) {
        return INVALID;
      }
      if(id == GLY || id == ALA || id == XXX) {
        return INVALID;
      }
      return GetRotamericConfiguration(entry.chi1);
    }

    case 1: {
      if(entry.chi2 != entry.chi2) {
        return INVALID;
      }
      if(id == ASN || id == ASP || id == PHE || id == TYR || id == HIS || 
         id == TRP) {
        return NON_ROTAMERIC;
      }
      if(id == ARG || id == GLN || id == GLU || id == ILE || id == LEU || 
         id == LYS || id == MET) {
        return GetRotamericConfiguration(entry.chi2);
      }
      return INVALID;
    }

    case 2: {
      if(entry.chi3 != entry.chi3) {
        return INVALID;
      }
      if(id == ARG || id == LYS || id == MET) {
        return GetRotamericConfiguration(entry.chi3);
      }
      if(id == GLN || id == GLU) {
        return NON_ROTAMERIC;
      }
      return INVALID;
    }

    case 3: {
      if(entry.chi4 != entry.chi4) {
        return INVALID;
      }
      if(id == ARG || id == LYS) {
        return GetRotamericConfiguration(entry.chi4);
      }
      return INVALID;
    }

    default: {
      throw promod3::Error("Invalid dihedral idx!");
    }
  }
}

}} //ns
