// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/subrotamer_optimizer.hh>

namespace promod3{ namespace sidechain{

void SubrotamerOptimizer(std::vector<FRMRotamerPtr>& rotamers,
                         Real active_internal_energy,
                         Real inactive_internal_energy,
                         uint max_complexity,
                         Real initial_epsilon) {

  // buildup a list of RRMRotamer groups
  std::vector<RRMRotamerGroupPtr> rrm_rotamer_groups;

  for(uint rot_idx = 0; rot_idx < rotamers.size(); ++rot_idx) {
    
    // extract all subrotamers of current FRMRotamer as RRMRotamers
    FRMRotamerPtr frm_rotamer = rotamers[rot_idx];
    std::vector<RRMRotamerPtr> rrm_rotamers;
    uint active_idx = frm_rotamer->GetActiveSubrotamer();

    for(uint subrot_idx = 0; subrot_idx < frm_rotamer->subrotamer_size(); 
        ++subrot_idx) {
      
      frm_rotamer->SetActiveSubrotamer(subrot_idx);
      RRMRotamerPtr new_rrm_rotamer = frm_rotamer->ToRRMRotamer();

      if(subrot_idx == active_idx) {
        new_rrm_rotamer->SetInternalEnergy(active_internal_energy);
      }   
      else {
        new_rrm_rotamer->SetInternalEnergy(inactive_internal_energy);
      }
      rrm_rotamers.push_back(new_rrm_rotamer);
    }
    
    RRMRotamerGroupPtr rrm_rotamer_group(new RRMRotamerGroup(rrm_rotamers, 0));
    rrm_rotamer_groups.push_back(rrm_rotamer_group);
  }

  // generate a graph from the RRMRotamerGroups to find the optimal subrotamers
  RotamerGraphPtr graph = RotamerGraph::CreateFromList(rrm_rotamer_groups);

  std::pair<std::vector<int>, Real> full_solution = 
  graph->TreeSolve(max_complexity, initial_epsilon);

  const std::vector<int>& solution = full_solution.first;

  // activate the appropriate subrotamers
  for(uint rot_idx = 0; rot_idx < solution.size(); ++rot_idx) {
     rotamers[rot_idx]->SetActiveSubrotamer(solution[rot_idx]); 
  }
}

}} // ns
