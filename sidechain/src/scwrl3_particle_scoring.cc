// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/scwrl3_particle_scoring.hh>


namespace promod3 { namespace sidechain {

void SCWRL3Param::ApplyTransform(const geom::Mat4& t) {

  // get transformed pos
  Real a = t(0,0)*pos_[0] + t(0,1)*pos_[1] + t(0,2)*pos_[2] + t(0,3);
  Real b = t(1,0)*pos_[0] + t(1,1)*pos_[1] + t(1,2)*pos_[2] + t(1,3);
  Real c = t(2,0)*pos_[0] + t(2,1)*pos_[1] + t(2,2)*pos_[2] + t(2,3);
  pos_ = geom::Vec3(a, b, c);
}


SCWRL3Param* SCWRL3Param::Copy() const {
  SCWRL3Param* return_ptr = new SCWRL3Param(pos_, radius_);
  return return_ptr;
}


bool SCWRL3Param::EqualTo(PScoringParam* other) const {

  if(other == NULL) return false;
  if(other->GetScoringFunction() != SCWRL3) return false;

  // as the other scoring function is also SCWRL3, we can assume that
  // the following dynamic cast is fine...
  SCWRL3Param* p = dynamic_cast<SCWRL3Param*>(other);

  return pos_ == p->pos_ &&
         radius_ == p->radius_;
}


Real SCWRL3PairwiseScore(SCWRL3Param* p_one, SCWRL3Param* p_two) {

  Real d = geom::Distance(p_one->pos_, p_two->pos_);
  Real R = p_one->radius_ + p_two->radius_;
  Real e = 0.0;
  if(d < Real(0.8254) * R) {
    e = 10.0;
  } else if(d <= R) {
    e = 57.273 * (1 - d/R);
  }
  return e;
}

}}//ns
