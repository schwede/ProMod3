// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_ROTAMER_CONSTRUCTOR_HH
#define PROMOD3_ROTAMER_CONSTRUCTOR_HH

#include <vector>
#include <ost/base.hh>
#include <ost/conop/compound_lib.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/loop/all_atom_positions.hh>
#include <promod3/loop/hydrogen_constructor.hh>
#include <promod3/sidechain/rotamer_id.hh>
#include <promod3/sidechain/rotamer_group.hh>
#include <promod3/sidechain/frame.hh>
#include <promod3/sidechain/rotamer_lib.hh>
#include <promod3/sidechain/bb_dep_rotamer_lib.hh>
#include <promod3/sidechain/rotamer_lookup.hh>


namespace promod3 { namespace sidechain {


class RotamerConstructor;
typedef boost::shared_ptr<RotamerConstructor> RotamerConstructorPtr;


class RotamerConstructor {

public:

  RotamerConstructor(bool cb_in_sidechain,
                     const RotamerLookupParam& param = RotamerLookupParam());

  virtual ~RotamerConstructor() { }

  // Construct rotamer groups from non backbone dependent library
  RRMRotamerGroupPtr ConstructRRMRotamerGroup(
                  const ost::mol::ResidueHandle& res, RotamerID id,
                  uint residue_index, RotamerLibPtr rot_lib, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  RRMRotamerGroupPtr ConstructRRMRotamerGroup(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  RotamerLibPtr rot_lib, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);
  
  FRMRotamerGroupPtr ConstructFRMRotamerGroup(
                  const ost::mol::ResidueHandle& res, RotamerID id,
                  uint residue_index, RotamerLibPtr rot_lib,
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false, 
                  Real probability_cutoff = 0.98);

  FRMRotamerGroupPtr ConstructFRMRotamerGroup(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  RotamerLibPtr rot_lib,
                  Real phi = -1.0472, 
                  Real psi =  -0.7854, 
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  // Construct rotamer groups from backbone dependent library
  RRMRotamerGroupPtr ConstructRRMRotamerGroup(
                  const ost::mol::ResidueHandle& res, RotamerID id,
                  uint residue_index, BBDepRotamerLibPtr rot_lib, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  RRMRotamerGroupPtr ConstructRRMRotamerGroup(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  BBDepRotamerLibPtr rot_lib, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);
  
  FRMRotamerGroupPtr ConstructFRMRotamerGroup(
                  const ost::mol::ResidueHandle& res, RotamerID id,
                  uint residue_index, BBDepRotamerLibPtr rot_lib, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  FRMRotamerGroupPtr ConstructFRMRotamerGroup(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  BBDepRotamerLibPtr rot_lib, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  // Construct rotamer groups directly from rotamerlib entries
  RRMRotamerGroupPtr ConstructRRMRotamerGroup(
                  const ost::mol::ResidueHandle& res, RotamerID id,
                  uint residue_index, 
                  std::vector<RotamerLibEntry>& lib_entries, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  RRMRotamerGroupPtr ConstructRRMRotamerGroup(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  std::vector<RotamerLibEntry>& lib_entries, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);
  
  FRMRotamerGroupPtr ConstructFRMRotamerGroup(
                  const ost::mol::ResidueHandle& res, RotamerID id,
                  uint residue_index,
                  std::vector<RotamerLibEntry>& lib_entries, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  FRMRotamerGroupPtr ConstructFRMRotamerGroup(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  std::vector<RotamerLibEntry>& lib_entries, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false,
                  Real probability_cutoff = 0.98);

  // Construct frame residues
  FrameResiduePtr ConstructBackboneFrameResidue(
                  const ost::mol::ResidueHandle& res, RotamerID id, 
                  uint residue_index, 
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false);

  FrameResiduePtr ConstructBackboneFrameResidue(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false);
  
  FrameResiduePtr ConstructSidechainFrameResidue(
                  const ost::mol::ResidueHandle& res, RotamerID id, 
                  uint residue_index,
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false);
  
  FrameResiduePtr ConstructSidechainFrameResidue(
                  const promod3::loop::AllAtomPositions& all_atom, 
                  uint aa_res_idx, RotamerID id, uint residue_index,
                  Real phi = -1.0472, 
                  Real psi =  -0.7854,
                  bool n_ter = false,
                  bool c_ter = false);

  // Assign internal energies to rotamer groups
  // This implementation just sets the internal energy of every rotamer to 0.0
  // Implement this function in the child class
  virtual void AssignInternalEnergies(RRMRotamerGroupPtr group,
                                      RotamerID id,
                                      uint residue_index,
                                      Real phi = -1.0472, 
                                      Real psi =  -0.7854,
                                      bool n_ter = false,
                                      bool c_ter = false);

  virtual void AssignInternalEnergies(FRMRotamerGroupPtr group,
                                      RotamerID id,
                                      uint residue_index,
                                      Real phi = -1.0472, 
                                      Real psi =  -0.7854,
                                      bool n_ter = false,
                                      bool c_ter = false);

protected:

  // Function that must be implemented by child class
  // there are two special cases that have to be treatet separately:
  // the terminal oxygens.
  // atom_idx for terminal O is set to -1
  // atom_idx for terminal OXT is set to -2
  virtual void ParametrizeParticle(int atom_idx, bool is_hydrogen, 
                                   Particle& p) = 0;

  // information that is available to the child class in order to parametrize
  // the given particle
  promod3::loop::AllAtomPositionsPtr pos_buffer_;
  promod3::loop::HydrogenStoragePtr hydrogen_buffer_;
  geom::Vec3 terminal_o_pos_;
  geom::Vec3 terminal_oxt_pos_;
  RotamerID id_;
  uint residue_idx_;
  Real phi_;
  Real psi_;
  bool n_ter_;
  bool c_ter_;

private:

  // Construct the rotamer groups after all backbone position information has 
  // been set into the internal position buffer objects
  virtual RRMRotamerGroupPtr ConstructRRMGroup( 
              std::pair<RotamerLibEntry*,uint> lib_entries,
              Real probability_cutoff);

  virtual FRMRotamerGroupPtr ConstructFRMGroup(
              std::pair<RotamerLibEntry*,uint> lib_entries,
              Real probability_cutoff);

  // assumes positions / probabilities, chi angles and chi deviations 
  // to be set in buffer
  RRMRotamerPtr ConstructRRM();
  FRMRotamerPtr ConstructFRM();
  FrameResiduePtr ConstructBackboneFrameResidue();

  // helpers
  void SetPosBuffer(const ost::mol::ResidueHandle& res);

  void SetPosBuffer(const promod3::loop::AllAtomPositions&,
                    uint aa_res_index);

  void MVBBPosBuffer(RotamerID from, RotamerID to);

  RotamerLookup rotamer_lookup_;
  Real chi_angles_[4];
  Real chi_dev_[4];
  Real probability_;
};

}} // ns

#endif
