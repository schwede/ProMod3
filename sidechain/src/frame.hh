// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_FRAME_HH
#define PROMOD3_FRAME_HH

#include <vector>

#include <ost/mol/residue_handle.hh>
#include <boost/shared_ptr.hpp>

#include <promod3/core/tetrahedral_polytope.hh>
#include <promod3/sidechain/particle.hh>
#include <promod3/core/message.hh>


namespace promod3{ namespace sidechain{


class Frame;
class FrameResidue;
typedef boost::shared_ptr<Frame> FramePtr;
typedef boost::shared_ptr<FrameResidue> FrameResiduePtr;



class FrameResidue{
public:

  FrameResidue(std::vector<Particle> particles, 
               uint residue_index):particles_(particles),
                                   n_particles_(particles.size()),
                                   residue_index_(residue_index) { }

  Particle& operator[](size_t index) { return particles_[index]; }

  const Particle& operator[](size_t index) const { return particles_[index]; }

  size_t size() const { return n_particles_; }

  typedef std::vector<Particle>::iterator iterator;
  typedef std::vector<Particle>::const_iterator const_iterator;
  
  iterator begin() { return particles_.begin(); }
  iterator end() { return particles_.end(); }
  const_iterator begin() const { return particles_.begin(); }
  const_iterator end() const { return particles_.end(); }

  uint GetResidueIndex() const { return residue_index_; }
  void ApplyOnResidue(ost::mol::ResidueHandle& res, bool consider_hydrogens) const;

private:
  std::vector<Particle> particles_;
  size_t n_particles_;
  uint residue_index_;
};


class Frame {

public:

  Frame(const std::vector<FrameResiduePtr>& frame_residues);

  const std::vector<uint>& GetResidueIndices() const {
    return residue_indices_;
  }

  const promod3::core::TetrahedralPolytopeTree& GetCollisionTree() const { 
    return collision_tree_; 
  }

  const PScoringParamContainer& GetScoringParameters() const {
    return scoring_parameters_;
  }

private:

  PScoringParamContainer scoring_parameters_;
  std::vector<FrameResiduePtr> frame_residues_;
  std::vector<uint> residue_indices_;
  promod3::core::TetrahedralPolytopeTree collision_tree_;
};


}}//ns

#endif
