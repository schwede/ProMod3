// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_LIB_READER_HH
#define PROMOD3_ROTAMER_LIB_READER_HH

#include <limits>
#include <map>

#include <promod3/sidechain/bb_dep_rotamer_lib.hh>

#include <ost/string_ref.hh>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>


namespace promod3{ namespace sidechain{

  BBDepRotamerLibPtr ReadDunbrackFile(const String& filename);

  BBDepRotamerLibPtr ReadDaggetFile(const String& filename);

}}

#endif
