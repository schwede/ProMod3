// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_ROTAMER_LOOKUP_HH
#define PROMOD3_ROTAMER_LOOKUP_HH

#include <vector>
#include <ost/base.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/sidechain/rotamer_id.hh>

namespace promod3 { namespace sidechain {

// Controls the particles that will be Constructed from the RotamerConstructor
enum RotamerLookupMode{
  HEAVY_ATOM_MODE,
  POLAR_HYDROGEN_MODE,
  FULL_ATOMIC_MODE
};

// Controls the parametrization of the rotamers
// Must be filled by energy function specific RotamerConstructor and
// passed to base class RotamerConstructor when calling constructor
// of RotamerConstructor base class
struct RotamerLookupParam {

  RotamerLookupParam(bool default_parametrization = true) {

    if(default_parametrization) {

      mode = HEAVY_ATOM_MODE;
      sample_his_protonation_states = false;

      for(int i = 0; i < ost::conop::XXX + 1; ++i) {
        frm_t[i] = 1.0;
        internal_e_prefactor[i] = 1.0;
      }
      std::vector<Real> default_prefactors;
      default_prefactors.push_back(-1.0);
      default_prefactors.push_back(1.0);
      ARG_CA_CB_prefactors = default_prefactors;
      ARG_CB_CG_prefactors = default_prefactors;
      ARG_CG_CD_prefactors = default_prefactors;
      ARG_CD_NE_prefactors = default_prefactors;
      ASN_CA_CB_prefactors = default_prefactors;
      ASN_CB_CG_prefactors = default_prefactors;
      ASP_CA_CB_prefactors = default_prefactors;
      ASP_CB_CG_prefactors = default_prefactors;
      GLN_CA_CB_prefactors = default_prefactors;
      GLN_CB_CG_prefactors = default_prefactors;
      GLN_CG_CD_prefactors = default_prefactors;
      GLU_CA_CB_prefactors = default_prefactors;
      GLU_CB_CG_prefactors = default_prefactors;
      GLU_CG_CD_prefactors = default_prefactors;
      LYS_CA_CB_prefactors = default_prefactors;
      LYS_CB_CG_prefactors = default_prefactors;
      LYS_CG_CD_prefactors = default_prefactors;
      LYS_CD_CE_prefactors = default_prefactors;
      SER_CA_CB_prefactors = default_prefactors;
      SER_CB_OG_prefactors = default_prefactors;                                    
      CYS_CA_CB_prefactors = default_prefactors;  
      MET_CA_CB_prefactors = default_prefactors;
      MET_CB_CG_prefactors = default_prefactors;
      MET_CG_SD_prefactors = default_prefactors;
      TRP_CA_CB_prefactors = default_prefactors;
      TRP_CB_CG_prefactors = default_prefactors;
      TYR_CA_CB_prefactors = default_prefactors;
      TYR_CB_CG_prefactors = default_prefactors;
      TYR_CZ_OH_prefactors = default_prefactors;                                     
      THR_CA_CB_prefactors = default_prefactors;
      THR_CB_OG1_prefactors = default_prefactors; 
      VAL_CA_CB_prefactors = default_prefactors;
      ILE_CA_CB_prefactors = default_prefactors;
      ILE_CB_CG1_prefactors = default_prefactors;
      LEU_CA_CB_prefactors = default_prefactors;
      LEU_CB_CG_prefactors = default_prefactors;
      HIS_CA_CB_prefactors = default_prefactors;
      HIS_CB_CG_prefactors = default_prefactors;
      PHE_CA_CB_prefactors = default_prefactors;
      PHE_CB_CG_prefactors = default_prefactors;
    }
  }

  // controls what atoms are defined in the rotamer lookup
  RotamerLookupMode mode;

  // if set to true, RotamerGroups will have two rotamers for each HIS rotamer.
  // One with RotamerID HSE and one with RotamerID HSD. If your 
  // RotamerConstructor does not consider those different protonation states, 
  // you can set that to false...
  bool sample_his_protonation_states; 

  // the internal energy prefactor of the constructed rotamers
  Real internal_e_prefactor[ost::conop::XXX + 1];

  // frm temperature parameters
  Real frm_t[ost::conop::XXX + 1];

  // rotation bond specific frm prefactors
  std::vector<Real> ARG_CA_CB_prefactors;
  std::vector<Real> ARG_CB_CG_prefactors;
  std::vector<Real> ARG_CG_CD_prefactors;
  std::vector<Real> ARG_CD_NE_prefactors;

  std::vector<Real> ASN_CA_CB_prefactors;
  std::vector<Real> ASN_CB_CG_prefactors;

  std::vector<Real> ASP_CA_CB_prefactors;
  std::vector<Real> ASP_CB_CG_prefactors;

  std::vector<Real> GLN_CA_CB_prefactors;
  std::vector<Real> GLN_CB_CG_prefactors;
  std::vector<Real> GLN_CG_CD_prefactors;

  std::vector<Real> GLU_CA_CB_prefactors;
  std::vector<Real> GLU_CB_CG_prefactors;
  std::vector<Real> GLU_CG_CD_prefactors;

  std::vector<Real> LYS_CA_CB_prefactors;
  std::vector<Real> LYS_CB_CG_prefactors;
  std::vector<Real> LYS_CG_CD_prefactors;
  std::vector<Real> LYS_CD_CE_prefactors;

  std::vector<Real> SER_CA_CB_prefactors;
  std::vector<Real> SER_CB_OG_prefactors; // only relevant when hydrogen is 
                                          // constructed

  std::vector<Real> CYS_CA_CB_prefactors;  

  std::vector<Real> MET_CA_CB_prefactors;
  std::vector<Real> MET_CB_CG_prefactors;
  std::vector<Real> MET_CG_SD_prefactors;

  std::vector<Real> TRP_CA_CB_prefactors;
  std::vector<Real> TRP_CB_CG_prefactors;

  std::vector<Real> TYR_CA_CB_prefactors;
  std::vector<Real> TYR_CB_CG_prefactors;
  std::vector<Real> TYR_CZ_OH_prefactors; // only relevant when hydrogen is 
                                          // constructed

  std::vector<Real> THR_CA_CB_prefactors;
  std::vector<Real> THR_CB_OG1_prefactors; // only relevant when hydrogen is 
                                           // constructed

  std::vector<Real> VAL_CA_CB_prefactors;

  std::vector<Real> ILE_CA_CB_prefactors;
  std::vector<Real> ILE_CB_CG1_prefactors;

  std::vector<Real> LEU_CA_CB_prefactors;
  std::vector<Real> LEU_CB_CG_prefactors;

  std::vector<Real> HIS_CA_CB_prefactors;
  std::vector<Real> HIS_CB_CG_prefactors;

  std::vector<Real> PHE_CA_CB_prefactors;
  std::vector<Real> PHE_CB_CG_prefactors;
};


struct ParticleInfo {

  ParticleInfo() { }

  ParticleInfo(const String& n, int idx, bool ih): name(n), atom_idx(idx),
                                                   is_hydrogen(ih) { }

  String name; 
  int atom_idx;  // for AllAtomPositions 
                 // (idx in HydrogenStorage if is_hydrogen is true)
  bool is_hydrogen;
};


struct CustomHydrogenInfo {

  CustomHydrogenInfo() { }
  
  CustomHydrogenInfo(int idx, int a_idx_one, int a_idx_two, int a_idx_three, 
                          Real bl, Real a, int c_idx): atom_idx(idx),
                                                       anchor_idx_one(a_idx_one), 
                                                       anchor_idx_two(a_idx_two), 
                                                       anchor_idx_three(a_idx_three), 
                                                       bond_length(bl), angle(a),
                                                       chi_idx(c_idx){ }
  int atom_idx; //the idx in the hydrogen constructor
  int anchor_idx_one; // idx in AllAtomPositions
  int anchor_idx_two; 
  int anchor_idx_three;
  Real bond_length;
  Real angle;
  int chi_idx;
};


struct FRMRule {

  FRMRule() { }

  FRMRule(int idx_one, int idx_two): anchor_idx_one(idx_one), 
                                     anchor_idx_two(idx_two) { }

  FRMRule(int idx_one, int idx_two,
          const std::vector<Real>& prefactors): anchor_idx_one(idx_one), 
                                                anchor_idx_two(idx_two),
                                                prefactors(prefactors) { }
  
  int anchor_idx_one; // idx of heavy atom in RotamerConstructor::pos_buffer_
  int anchor_idx_two; // idx of heavy atom in RotamerConstructor::pos_buffer_
  std::vector<Real> prefactors;
  // The following indices are NOT relative to any position buffer but rather
  // to the ordering as the particles are defined in RotamerInfo
  std::vector<int> fix_particles;
  std::vector<int> rotating_particles;
};


struct RotamerInfo {

  RotamerInfo() { }
  
  std::vector<ParticleInfo> particles;
  std::vector<CustomHydrogenInfo> custom_hydrogens;
  Real internal_e_prefactor;
  Real frm_t;
  bool has_hydrogens;
};


class RotamerLookup{

public:
  
  RotamerLookup(bool cb_in_sidechain, 
                const RotamerLookupParam& param = RotamerLookupParam());

  // Data access
  const RotamerInfo& GetSidechainInfo(RotamerID id) const { 
    return sidechain_infos_[id]; 
  }

  const RotamerInfo& GetBackboneInfo(RotamerID id) const { 
    return backbone_infos_[id]; 
  }

  RotamerInfo GetTerminalBackboneInfo(RotamerID id, bool n_ter,
                                      bool c_ter);

  const std::vector<FRMRule>& GetFRMRules(RotamerID id) const { 
    return frm_rules_[id]; 
  }

  int GetNumSidechainParticles(RotamerID id) const { 
    return num_sidechain_particles_[id]; 
  }

  int GetNumFRMSidechainParticles(RotamerID id) const { 
    return num_frm_particles_[id]; 
  }

  RotamerLookupMode GetMode() const {
    return mode_;
  }

  bool SampleHISProtonationStates() const {
    return sample_his_protonation_states_;
  }

private:

  void AddInfo(RotamerID id, const String& name, int idx, bool is_h) {
    ParticleInfo p(name, idx, is_h);
    sidechain_infos_[id].particles.push_back(p);
  }

  void AddCustomHydrogenInfo(RotamerID id, int idx, 
                             int a_idx_one, int a_idx_two, int a_idx_three, 
                             Real bl, Real a, int c_idx) {
    CustomHydrogenInfo i(idx, a_idx_one, a_idx_two, a_idx_three, bl, a, c_idx);
    sidechain_infos_[id].custom_hydrogens.push_back(i);
  }

  void AddBBInfo(RotamerID id, const String& name, int idx, bool is_h) {
    ParticleInfo p(name, idx, is_h);
    backbone_infos_[id].particles.push_back(p);
  }

  void AddFRMRule(RotamerID id, int idx_one, int idx_two,
                  const std::vector<Real>& prefactors){
    frm_rules_[id].push_back(FRMRule(idx_one, idx_two, prefactors));
  }

  void AddFRMFixParticle(RotamerID id, int rule_idx, int p_idx){
    frm_rules_[id][rule_idx].fix_particles.push_back(p_idx);
  }

  void AddFRMRotatingParticle(RotamerID id, int rule_idx, int p_idx){
    frm_rules_[id][rule_idx].rotating_particles.push_back(p_idx);
  }

  // To construct classical rotamers or as lookup for sidechain frame residues
  RotamerInfo sidechain_infos_[XXX + 1];

  // To construct backbone frame residues
  RotamerInfo backbone_infos_[XXX + 1];

  // Additional info required for generating frm rotamers
  std::vector<FRMRule> frm_rules_[XXX + 1];

  int num_sidechain_particles_[XXX + 1];
  int num_frm_particles_[XXX + 1];

  RotamerLookupMode mode_;
  bool sample_his_protonation_states_;
};


}} // ns

#endif
