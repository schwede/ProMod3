// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_SIDECHAIN_DISULFID_HH
#define PROMOD3_SIDECHAIN_DISULFID_HH

#include <limits>

#include <promod3/sidechain/rotamer_group.hh>
#include <promod3/core/message.hh>


namespace promod3{ namespace sidechain{


Real DisulfidScore(RRMRotamerPtr rot_one, RRMRotamerPtr rot_two, 
                   const geom::Vec3& ca_pos_one, const geom::Vec3& cb_pos_one, 
                   const geom::Vec3& ca_pos_two, const geom::Vec3& cb_pos_two);

Real DisulfidScore(FRMRotamerPtr rot_one, FRMRotamerPtr rot_two, 
                   const geom::Vec3& ca_pos_one, const geom::Vec3& cb_pos_one, 
                   const geom::Vec3& ca_pos_two, const geom::Vec3& cb_pos_two);

 void ResolveCysteins(const std::vector<FRMRotamerGroupPtr>& rot_groups,
                      const std::vector<geom::Vec3>& ca_pos,
                      const std::vector<geom::Vec3>& cb_pos,
                      Real disulfid_score_thresh, bool optimize_subrotamers,
                      std::vector<std::pair<uint, uint> >& disulfid_indices,
                      std::vector<std::pair<uint, uint> >& rotamer_indices);

 void ResolveCysteins(const std::vector<RRMRotamerGroupPtr>& rot_groups,
                      const std::vector<geom::Vec3>& ca_pos,
                      const std::vector<geom::Vec3>& cb_pos,
                      Real disulfid_score_thresh, bool optimize_subrotamers,
                      std::vector<std::pair<uint, uint> >& disulfid_indices,
                      std::vector<std::pair<uint, uint> >& rotamer_indices);

}} // ns

#endif
