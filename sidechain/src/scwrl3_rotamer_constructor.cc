// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/scwrl3_rotamer_constructor.hh>
#include <promod3/sidechain/scwrl3_particle_scoring.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>
#include <ost/conop/conop.hh>

namespace promod3 { namespace sidechain {

struct SCWRL3RotamerParam : public RotamerLookupParam {

  SCWRL3RotamerParam() {

    mode = HEAVY_ATOM_MODE;
    sample_his_protonation_states = false;

    // overwrite default internal_e_prefactor from base class
    internal_e_prefactor[ost::conop::ARG] = 3.0;
    internal_e_prefactor[ost::conop::ASN] = 3.0;
    internal_e_prefactor[ost::conop::ASP] = 3.0;
    internal_e_prefactor[ost::conop::GLN] = 3.0;
    internal_e_prefactor[ost::conop::GLU] = 3.0;
    internal_e_prefactor[ost::conop::LYS] = 3.0;
    internal_e_prefactor[ost::conop::SER] = 3.0;
    internal_e_prefactor[ost::conop::CYS] = 3.0;
    internal_e_prefactor[ost::conop::MET] = 3.0;
    internal_e_prefactor[ost::conop::TRP] = 3.0;
    internal_e_prefactor[ost::conop::TYR] = 3.0;
    internal_e_prefactor[ost::conop::THR] = 3.0;
    internal_e_prefactor[ost::conop::VAL] = 3.0;
    internal_e_prefactor[ost::conop::ILE] = 3.0;
    internal_e_prefactor[ost::conop::LEU] = 3.0;
    internal_e_prefactor[ost::conop::PRO] = 3.0;
    internal_e_prefactor[ost::conop::HIS] = 3.0;
    internal_e_prefactor[ost::conop::PHE] = 3.0;
  }  

  static const SCWRL3RotamerParam& Instance() {
    static SCWRL3RotamerParam scwrl3_param;
    return scwrl3_param;
  }
};


SCWRL3RotamerConstructor::SCWRL3RotamerConstructor(bool cb_in_sidechain): 
                          RotamerConstructor(cb_in_sidechain,
                                             SCWRL3RotamerParam::Instance()) { }

void SCWRL3RotamerConstructor::AssignInternalEnergies(RRMRotamerGroupPtr group,
                                                      RotamerID id, 
                                                      uint residue_index,
                                                      Real phi, Real psi,
                                                      bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "SCWRL3RotamerConstructor::AssignInternalEnergies", 2);

  Real max_p = group->GetMaxP();
  for(uint i = 0; i < group->size(); ++i){
    RRMRotamerPtr r = (*group)[i];
    Real internal_e_prefactor = r->GetInternalEnergyPrefactor();
    Real probability = r->GetProbability();
    Real e = -internal_e_prefactor * std::log(probability/max_p);
    r->SetInternalEnergy(e);
  }
}


void SCWRL3RotamerConstructor::AssignInternalEnergies(FRMRotamerGroupPtr group,
                                                      RotamerID id, 
                                                      uint residue_index,
                                                      Real phi, Real psi,
                                                      bool n_ter, bool c_ter) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
          "SCWRL3RotamerConstructor::AssignInternalEnergies", 2);

  Real max_p = group->GetMaxP();
  for(uint i = 0; i < group->size(); ++i){
    FRMRotamerPtr r = (*group)[i];
    Real internal_e_prefactor = r->GetInternalEnergyPrefactor();
    Real probability = r->GetProbability();
    Real e = -internal_e_prefactor * std::log(probability/max_p);
    r->SetInternalEnergy(e);
  }
}


void SCWRL3RotamerConstructor::ParametrizeParticle(int at_idx, 
                                                   bool is_hydrogen, 
                                                   Particle& particle) {

  if(is_hydrogen) {
    throw promod3::Error("Expect no hydrogen in SCWRL3RotamerConstructor!");
  }

  geom::Vec3 pos;
  Real radius = 0.0;

  if(at_idx == -2) {
    pos = terminal_o_pos_;
    radius = 1.3;
  } else if (at_idx == -1){
    pos = terminal_oxt_pos_;
    radius = 1.3;
  } else {
    pos = pos_buffer_->GetPos(id_, at_idx);
    ost::conop::AminoAcid aa = RotIDToAA(id_);
    promod3::loop::AminoAcidAtom aaa =
    promod3::loop::AminoAcidLookup::GetInstance().GetAAA(aa, at_idx);
    String ele = promod3::loop::AminoAcidLookup::GetInstance().GetElement(aaa);
    if(ele == "C") {
      radius = 1.6;
    } else if(ele == "N") {
      radius = 1.3;
    } else if(ele == "O") {
      radius = 1.3;
    } else if (ele == "S") {
      radius = 1.7;
    }
  }

  SCWRL3Param* p = new SCWRL3Param(pos, radius);
  particle.SetSParam(p);
}

}} //ns
