// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_SCWRL3_ROTAMER_CONSTRUCTOR_HH
#define PROMOD3_SCWRL3_ROTAMER_CONSTRUCTOR_HH

#include <promod3/sidechain/rotamer_constructor.hh>

namespace promod3 { namespace sidechain {

class SCWRL3RotamerConstructor;
typedef boost::shared_ptr<SCWRL3RotamerConstructor> SCWRL3RotamerConstructorPtr;

class SCWRL3RotamerConstructor : public RotamerConstructor{

public:

  SCWRL3RotamerConstructor(bool cb_in_sidechain = false);

  virtual ~SCWRL3RotamerConstructor() { }

  // Assign internal energies to rotamer groups
  virtual void AssignInternalEnergies(RRMRotamerGroupPtr group,
                                      RotamerID id,
                                      uint residue_index,
                                      Real phi = -1.0472, 
                                      Real psi =  -0.7854,
                                      bool n_ter = false,
                                      bool c_ter = false);

  virtual void AssignInternalEnergies(FRMRotamerGroupPtr group,
                                      RotamerID id,
                                      uint residue_index,
                                      Real phi = -1.0472, 
                                      Real psi =  -0.7854,
                                      bool n_ter = false,
                                      bool c_ter = false);

private:

  virtual void ParametrizeParticle(int atom_idx, bool is_hydrogen, 
                                   Particle& p);
};

}} // ns

#endif
