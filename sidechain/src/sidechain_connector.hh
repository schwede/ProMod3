// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_SIDECHAIN_CONNECTOR_HH
#define PROMOD3_SIDECHAIN_CONNECTOR_HH

#include <ost/mol/bond_handle.hh>
#include <ost/mol/atom_handle.hh>
#include <ost/mol/residue_handle.hh>
#include <ost/mol/xcs_editor.hh>
#include <promod3/sidechain/rotamer_id.hh>


namespace promod3{ namespace sidechain{

  void ConnectSidechain(ost::mol::ResidueHandle& res, RotamerID rot_id);

}}//ns

#endif
