// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_BB_DEP_ROTAMER_LIB_HH
#define PROMOD3_BB_DEP_ROTAMER_LIB_HH

#include <ost/base.hh>

#include <boost/shared_ptr.hpp>

#include <stdint.h>
#include <map>
#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <promod3/core/message.hh>
#include <promod3/sidechain/rotamer_id.hh>
#include <promod3/sidechain/rotamer_lib_entry.hh>
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3{ namespace sidechain{

class BBDepRotamerLib;
typedef boost::shared_ptr<BBDepRotamerLib> BBDepRotamerLibPtr;

class BBDepRotamerLib{

public:

  BBDepRotamerLib(uint phi_bins, uint psi_bins): phi_bins_(phi_bins),
                                                 psi_bins_(psi_bins),
                                                 phi_bin_size_(2*M_PI/phi_bins),
                                                 psi_bin_size_(2*M_PI/psi_bins),
                                                 readonly_(false),
                                                 interpolate_(false) { }

  ~BBDepRotamerLib();

  void Save(const String& filename);

  static BBDepRotamerLibPtr Load(const String& filename);

  void SavePortable(const String& filename);

  static BBDepRotamerLibPtr LoadPortable(const String& filename);

  std::pair<RotamerLibEntry*,uint> QueryLib(RotamerID id, Real phi, Real psi) const;

  void MakeStatic();

  void AddRotamer(RotamerID id, uint r1, uint r2, uint r3, uint r4,
                  uint phi_bin, uint psi_bin, const RotamerLibEntry& rot);

  void SetInterpolate(bool do_it) { interpolate_ = do_it; }

private:

  uint GetBackboneBin(Real phi, Real psi) const;

  void GetBackboneBin(Real phi, Real psi, uint& bin00, uint& bin10, 
                      uint& bin01, uint& bin11, Real& x, Real& y) const;

  // serialize map (base-types in there!)
  void SerializeStaticDataHelper(core::PortableBinaryDataSink& ds) {
    core::ConvertBaseType<uint32_t>(ds, static_data_helper_.size());
    typedef std::map<RotamerID,std::pair<uint64_t,uint> > MyMap;
    for (MyMap::iterator i = static_data_helper_.begin();
         i != static_data_helper_.end(); ++i) {
      ds & i->first;
      ds & i->second.first;
      core::ConvertBaseType<uint32_t>(ds, i->second.second);
    }
  }
  void SerializeStaticDataHelper(core::PortableBinaryDataSource& ds) {
    static_data_helper_.clear();
    uint size;
    core::ConvertBaseType<uint32_t>(ds, size);
    for (uint i = 0; i < size; ++i) {
      RotamerID id;
      uint64_t current_pos;
      uint num_rotamers;
      ds & id;
      ds & current_pos;
      core::ConvertBaseType<uint32_t>(ds, num_rotamers);
      static_data_helper_[id] = std::make_pair(current_pos, num_rotamers);
    }
  }

  // portable serialization (only stores static data relevant for Load/Save!)
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void SerializeLoadSave(DS& ds) {
    // enforce static
    if (!readonly_ && !ds.IsSource()) MakeStatic();
    readonly_ = true;
    // store data
    ds & interpolate_;
    SerializeStaticDataHelper(ds); // static_data_helper_
    ds & total_num_rotamers_;
    if (ds.IsSource()) static_data_ = new RotamerLibEntry[total_num_rotamers_];
    for (uint i = 0; i < total_num_rotamers_; ++i) {
      ds & static_data_[i];
    }
  }

  // general data
  uint phi_bins_;
  uint psi_bins_;
  Real phi_bin_size_;
  Real psi_bin_size_;
  bool readonly_;
  bool interpolate_;

  mutable std::vector<RotamerLibEntry> result_buffer_;

  // dynamic data
  std::map<RotamerID, std::vector<std::vector<std::pair<std::vector<uint>,RotamerLibEntry> > > > dynamic_data_;
  
  // static data
  std::map<RotamerID,std::pair<uint64_t,uint> > static_data_helper_;
  uint64_t total_num_rotamers_;
  RotamerLibEntry* static_data_;
};


}}//ns

#endif
