// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_ROTAMER_DENSITY_HH
#define PROMOD3_ROTAMER_DENSITY_HH

#include <promod3/sidechain/rotamer_lib_entry.hh>
#include <promod3/core/message.hh>

#include <limits>

namespace promod3{ namespace sidechain{

class RotamerDensity1D;
class RotamerDensity2D;
class RotamerDensity;

typedef boost::shared_ptr<RotamerDensity1D> RotamerDensity1DPtr;
typedef boost::shared_ptr<RotamerDensity2D> RotamerDensity2DPtr;
typedef boost::shared_ptr<RotamerDensity> RotamerDensityPtr;


class RotamerDensity{
public:

  RotamerDensity() { }

  virtual ~RotamerDensity() { };

  virtual void Normalize() = 0;

  virtual void AddRotamer(const RotamerLibEntry& rot) = 0;

  virtual uint GetNumRotamers() = 0;

  virtual uint BinsPerDimension() = 0;

  virtual uint Bins() = 0;

  virtual void ApplyFactor(Real factor) = 0;

  virtual Real GetP(Real* chi_angles) = 0;

  virtual void MergeProbabilities(RotamerDensityPtr other) = 0;

  virtual void Clear() = 0;

  virtual void SetNumRotamers(uint num) = 0;

  virtual double* Data() = 0;

};


class RotamerDensity1D : public RotamerDensity{
public:

  RotamerDensity1D(uint num_bins);

  RotamerDensity1D(const std::vector<RotamerDensity1DPtr>& densities,
                   const std::vector<Real>& weights);

  virtual ~RotamerDensity1D(){ delete [] data_; }

  virtual void Normalize();

  virtual void AddRotamer(const RotamerLibEntry& rot);

  virtual uint GetNumRotamers() { return num_rotamers_;}

  virtual uint BinsPerDimension() { return num_bins_; }

  virtual uint Bins() { return num_bins_;}

  virtual void ApplyFactor(Real factor);

  virtual Real GetP(Real* chi_angles);

  virtual void MergeProbabilities(RotamerDensityPtr other);

  virtual void Clear();

  virtual void SetNumRotamers(uint num) { num_rotamers_ = num; }

  virtual double* Data() { return data_; }



private:
  double* data_;
  uint num_rotamers_;
  Real bin_size_;
  uint num_bins_;
};


class RotamerDensity2D : public RotamerDensity{
public:

  RotamerDensity2D(uint num_bins);

  RotamerDensity2D(const std::vector<RotamerDensity2DPtr>& densities,
                   const std::vector<Real>& weights);

  virtual ~RotamerDensity2D(){ delete [] data_; }

  virtual void Normalize();

  virtual void AddRotamer(const RotamerLibEntry& rot);

  virtual uint GetNumRotamers() { return num_rotamers_;}

  virtual uint BinsPerDimension() { return num_bins_; }

  virtual uint Bins() { return num_bins_*num_bins_;}

  virtual void ApplyFactor(Real factor);

  virtual Real GetP(Real* chi_angles);

  virtual void MergeProbabilities(RotamerDensityPtr other);

  virtual void Clear();

  virtual void SetNumRotamers(uint num) { num_rotamers_ = num; }

  virtual double* Data() { return data_; }


private:
  double* data_;
  uint num_rotamers_;
  Real bin_size_;
  uint num_bins_;
};

}} //ns

#endif

