// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD3_PARTICLE_SCORING_HH
#define PROMOD3_PARTICLE_SCORING_HH

#include <vector>
#include <promod3/sidechain/particle_scoring_base.hh>

namespace promod3{ namespace sidechain{

// The generic pairwise energy function that calls the right pairwise energy 
// function. Be aware of the virtual function calls to check what scoring 
// function should be used. If you already know what you're dealing with
// you should directly call the specific scoring function. reinterpret_cast the 
// ptrs if necessary but only do that if you know what you're doing...
Real PairwiseParticleScore(PScoringParam* p_one, PScoringParam* p_two);

// this class is for internal use only and serves the RotamerGroup objects to
// avoid too many virtual function calls. Its a container for pointeres to
// PScoringParam objects. It guarantees that they're the same type. Virtual 
// function calls to check for the underlying scoring function are therefore
// not necessary anymore.
class PScoringParamContainer{

public:

  PScoringParamContainer(): scoring_function_(INVALID_PSCORING_FUNCTION) { }

  void push_back(PScoringParam* p);

  void clear();

  PScoringFunction GetScoringFunction() const { return scoring_function_; }

  void PairwiseScores(const PScoringParamContainer& other,
                      const std::vector<std::pair<int, int> >& idx, 
                      std::vector<Real>& scores) const;

private:

  PScoringFunction scoring_function_;
  std::vector<PScoringParam*> params_;
};

}} //ns

#endif
