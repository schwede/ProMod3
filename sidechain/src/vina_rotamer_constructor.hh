// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_VINA_ROTAMER_CONSTRUCTOR_HH
#define PROMOD3_VINA_ROTAMER_CONSTRUCTOR_HH

#include <promod3/sidechain/rotamer_constructor.hh>

namespace promod3 { namespace sidechain {

class VINARotamerConstructor;
typedef boost::shared_ptr<VINARotamerConstructor> VINARotamerConstructorPtr;

class VINARotamerConstructor : public RotamerConstructor{

public:

  VINARotamerConstructor(bool cb_in_sidechain = false);

  virtual ~VINARotamerConstructor() { }

  // Assign internal energies to rotamer groups
  virtual void AssignInternalEnergies(RRMRotamerGroupPtr group,
                                      RotamerID id,
                                      uint residue_index,
                                      Real phi = -1.0472, 
                                      Real psi =  -0.7854,
                                      bool n_ter = false,
                                      bool c_ter = false);

  virtual void AssignInternalEnergies(FRMRotamerGroupPtr group,
                                      RotamerID id,
                                      uint residue_index,
                                      Real phi = -1.0472, 
                                      Real psi =  -0.7854,
                                      bool n_ter = false,
                                      bool c_ter = false);

  FrameResiduePtr ConstructFrameResidueHeuristic(
          const ost::mol::ResidueHandle& res, uint residue_index);

  RRMRotamerPtr ConstructRRMRotamerHeuristic(const ost::mol::ResidueHandle& res);

  FRMRotamerPtr ConstructFRMRotamerHeuristic(const ost::mol::ResidueHandle& res);

private:

  virtual void ParametrizeParticle(int atom_idx, bool is_hydrogen, 
                                   Particle& p);

  void GenerateParticleVec(const ost::mol::ResidueHandle& res, 
                           std::vector<Particle>& particle_vec) const;
};

}} // ns

#endif
