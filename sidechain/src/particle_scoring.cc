// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <promod3/core/message.hh>
#include <promod3/sidechain/particle_scoring.hh>
#include <promod3/sidechain/scwrl4_particle_scoring.hh>
#include <promod3/sidechain/scwrl3_particle_scoring.hh>
#include <promod3/sidechain/vina_particle_scoring.hh>

namespace promod3{ namespace sidechain{

Real PairwiseParticleScore(PScoringParam* p_one, PScoringParam* p_two) {

  if(p_one->GetScoringFunction() != p_two->GetScoringFunction()) {
    throw promod3::Error("Try to calculate pairwise particle energy with "
                         "parameters of different type!");
  }

  switch(p_one->GetScoringFunction()) {
    case SCWRL4: {
      return SCWRL4PairwiseScore(reinterpret_cast<SCWRL4Param*>(p_one),
                                 reinterpret_cast<SCWRL4Param*>(p_two));
    }
    case SCWRL3: {
      return SCWRL3PairwiseScore(reinterpret_cast<SCWRL3Param*>(p_one),
                                 reinterpret_cast<SCWRL3Param*>(p_two));
    }
    case VINA: {
      return VINAPairwiseScore(reinterpret_cast<VINAParam*>(p_one),
                               reinterpret_cast<VINAParam*>(p_two));
    }
    default: {
      throw promod3::Error("Don't know what scoring function to call between "
                           "particles... maybe a lazy developer forgot to "
                           "update the PairwiseParticleEnergy function after "
                           "implementing a new score");
    }
  }
}


void PScoringParamContainer::push_back(PScoringParam* p) {
  if(params_.empty()) {
    scoring_function_ = p->GetScoringFunction();
  } else{
    if(scoring_function_ != p->GetScoringFunction()) {
      throw promod3::Error("Tried to setup PScoringParamContainer with "
                            "inconsistent scoring functions");
    }
  }
  params_.push_back(p);
}


void PScoringParamContainer::clear() {
  scoring_function_ = INVALID_PSCORING_FUNCTION;
  params_.clear();
}


void PScoringParamContainer::PairwiseScores(const PScoringParamContainer& other,
                                   const std::vector<std::pair<int, int> >& idx, 
                                   std::vector<Real>& scores) const {

  if(idx.empty()) {
    scores.clear();
    return;
  }

  if(scoring_function_ != other.GetScoringFunction()) {
    throw promod3::Error("Tried to get pairwise scores from two "
                         "PScoringParamContainers with inconsistent energy "
                         "functions");
  }

  switch(scoring_function_) {
    case SCWRL4: {
      scores.resize(idx.size());
      for(uint i = 0; i < idx.size(); ++i) {
        scores[i] = 
        SCWRL4PairwiseScore(reinterpret_cast<SCWRL4Param*>(params_[idx[i].first]),
                            reinterpret_cast<SCWRL4Param*>(other.params_[idx[i].second]));
      }
      break;
    }
    case SCWRL3: {
      scores.resize(idx.size());
      for(uint i = 0; i < idx.size(); ++i) {
        scores[i] = 
        SCWRL3PairwiseScore(reinterpret_cast<SCWRL3Param*>(params_[idx[i].first]),
                            reinterpret_cast<SCWRL3Param*>(other.params_[idx[i].second]));
      }
      break;
    }
    case VINA: {
      scores.resize(idx.size());
      for(uint i = 0; i < idx.size(); ++i) {
        scores[i] = 
        VINAPairwiseScore(reinterpret_cast<VINAParam*>(params_[idx[i].first]),
                          reinterpret_cast<VINAParam*>(other.params_[idx[i].second]));
      }
      break;
    }
    default: {
      throw promod3::Error("Don't know what scoring function to call between "
                           "particles... maybe a lazy developer forgot to "
                           "update the PairwiseParticleEnergy function after "
                           "implementing a new score");
    }
  }
}

}} //ns
