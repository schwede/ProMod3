// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/sidechain/rotamer.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3{ namespace sidechain{


RRMRotamer::RRMRotamer(const std::vector<Particle>& particles, 
                       Real probability, Real internal_e_prefactor):
                       particles_(particles), n_particles_(particles.size()), 
                       internal_energy_(0.0), frame_energy_(0.0), 
                       probability_(probability),
                       internal_e_prefactor_(internal_e_prefactor) { }


void RRMRotamer::ApplyOnResidue(ost::mol::ResidueHandle& res,
                                bool consider_hydrogens,
                                const String& new_res_name) const {
  
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "RRMRotamer::ApplyOnResidue", 2);

  ost::mol::XCSEditor ed = res.GetEntity().EditXCS(ost::mol::BUFFERED_EDIT);
  for(uint i = 0; i < n_particles_; ++i) {
    const Particle& p=particles_[i];
    const String& particle_name =p.GetName();
    if(particle_name[0] == 'H' && !consider_hydrogens) continue;
    ost::mol::AtomHandle a=res.FindAtom(particle_name);
    if (a.IsValid()){
      ed.DeleteAtom(a);
      ed.InsertAtom(res, particle_name, p.GetPos(), particle_name.substr(0,1));
    }
    else {
      ed.InsertAtom(res,p.GetName(),p.GetPos(),particle_name.substr(0,1));
    }
  }

  if (new_res_name != "") {
    ed.RenameResidue(res, new_res_name);
    res.SetOneLetterCode(ost::conop::ResidueNameToOneLetterCode(new_res_name));
  }
  ed.UpdateICS();
}


void RRMRotamer::ApplyOnResidue(loop::AllAtomPositions& all_atom,
                                uint res_idx) const {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "RRMRotamer::ApplyOnResidueAA", 2);

  // check res_idx
  all_atom.CheckResidueIndex(res_idx);
  // reset all heavy atoms
  for (const_iterator p = this->begin(); p != this->end(); ++p) {
    const String& particle_name = p->GetName();
    if (particle_name[0] != 'H') {
      all_atom.SetPos(all_atom.GetIndex(res_idx, particle_name), p->GetPos());
    }
  }
}


FrameResiduePtr RRMRotamer::ToFrameResidue(uint res_idx) const {

  FrameResiduePtr frame_res(new FrameResidue(particles_, res_idx));
  return frame_res;
}


FRMRotamer::FRMRotamer(const std::vector<Particle>& particles, Real T, 
                       Real probability, Real internal_e_prefactor):
                       particles_(particles), n_particles_(particles.size()),
                       active_subrotamer_(0), internal_energy_(0.0),
                       frame_energy_(0.0), T_(T), probability_(probability),
                       internal_e_prefactor_(internal_e_prefactor) {
  subrotamer_associations_.resize(n_particles_,std::vector<int>());
}


void FRMRotamer::AddSubrotamerDefinition(const std::vector<int>& definition) {
  uint subrotamer_index = subrotamer_definitions_.size();
  subrotamer_definitions_.push_back(definition);
  for(std::vector<int>::const_iterator i = definition.begin(); 
      i != definition.end();++i) {
    subrotamer_associations_[*i].push_back(subrotamer_index);
  }
  frame_energies_.push_back(0.0);
}


void FRMRotamer::SetActiveSubrotamer(uint idx) {
  if(idx >= subrotamer_definitions_.size()) {
    throw promod3::Error("Invalid subrotamer idx provided!");
  }
  active_subrotamer_ = idx;
}


void FRMRotamer::ApplyOnResidue(ost::mol::ResidueHandle& res,
                                bool consider_hydrogens,
                                const String& new_res_name) const {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "FRMRotamer::ApplyOnResidue", 2);

  if(subrotamer_definitions_.empty()) {
    throw promod3::Error("At least one subrotamer must be set in FRM rotamer!");
  }

  if(active_subrotamer_ >= subrotamer_definitions_.size()) {
    throw promod3::Error("Need to add subrotamer definitions to "
                         "apply FRMRotamer to residue!");
  }

  ost::mol::XCSEditor ed = res.GetEntity().EditXCS(ost::mol::BUFFERED_EDIT);

  const std::vector<int>& sub_def = subrotamer_definitions_[active_subrotamer_];
  ost::mol::AtomHandle a;
  for (uint i = 0; i < sub_def.size(); ++i) {
    const Particle& p=particles_[sub_def[i]];
    const String& particle_name = p.GetName();
    if(particle_name[0] == 'H' && !consider_hydrogens) continue;
    a = res.FindAtom(particle_name);
    if (a.IsValid()) {
      // weirdly enough, the commented out command is significantly slower 
      // (at least on el schdaudoputer) There is no obvious reason for that...
      ed.DeleteAtom(a);
      ed.InsertAtom(res, particle_name, p.GetPos(), particle_name.substr(0,1));
      //ed.SetAtomPos(a, p.GetPos());
    }
    else {
      ed.InsertAtom(res, p.GetName(), p.GetPos(), particle_name.substr(0,1));
    }
  }

  if(new_res_name != "") {
    ed.RenameResidue(res,new_res_name);
    res.SetOneLetterCode(ost::conop::ResidueNameToOneLetterCode(new_res_name));
  }
  ed.UpdateICS();
}


void FRMRotamer::ApplyOnResidue(loop::AllAtomPositions& all_atom,
                                uint res_idx) const {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "FRMRotamer::ApplyOnResidueAA", 2);

  if(active_subrotamer_ >= subrotamer_definitions_.size()) {
    throw promod3::Error("Need to add subrotamer definitions to "
                         "apply FRMRotamer to residue!");
  }

  // check res_idx
  all_atom.CheckResidueIndex(res_idx);
  // reset all heavy atoms
  const std::vector<int>& sub_def_ = 
  subrotamer_definitions_[active_subrotamer_];
  for (uint i = 0; i < sub_def_.size(); ++i) {
    const Particle& p = particles_[sub_def_[i]];
    const String& particle_name = p.GetName();
    if (particle_name[0] != 'H') {
      all_atom.SetPos(all_atom.GetIndex(res_idx, particle_name), p.GetPos());
    }
  }
}


FrameResiduePtr FRMRotamer::ToFrameResidue(uint res_idx) const {

  if(active_subrotamer_ >= subrotamer_definitions_.size()) {
    throw promod3::Error("Need to add subrotamer definitions to "
                         "generate FrameResidue!");
  }

  const std::vector<int>& sub_def = subrotamer_definitions_[active_subrotamer_];
  std::vector<Particle> particles(sub_def.size());
  for(uint i = 0; i < sub_def.size(); ++i) {
    particles[i] = particles_[sub_def[i]];
  }
  FrameResiduePtr frame_res(new FrameResidue(particles, res_idx));
  return frame_res;
}


RRMRotamerPtr FRMRotamer::ToRRMRotamer() const {

  if(active_subrotamer_ >= subrotamer_definitions_.size()) {
    throw promod3::Error("Need to add subrotamer definitions to "
                         "to generate RRMRotamer!");
  }

  const std::vector<int>& sub_def = subrotamer_definitions_[active_subrotamer_];
  std::vector<Particle> particles(sub_def.size());
  for(uint i = 0; i < sub_def.size(); ++i) {
    particles[i] = particles_[sub_def[i]];
  }
  RRMRotamerPtr rot(new RRMRotamer(particles, probability_, 
                                   internal_e_prefactor_));
  rot->SetFrameEnergy(frame_energies_[active_subrotamer_]);
  rot->SetInternalEnergy(internal_energy_);
  return rot;
}

}} // ns
