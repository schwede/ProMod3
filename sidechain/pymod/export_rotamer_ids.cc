// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/sidechain/rotamer_id.hh>


using namespace boost::python;
using namespace promod3::sidechain;

void export_RotamerIDs()
{
  enum_<RotamerID>("RotamerID")
    .value("ALA", ALA)
    .value("ARG", ARG)
    .value("ASN", ASN)
    .value("ASP", ASP)
    .value("GLN", GLN) 
    .value("GLU", GLU)
    .value("LYS", LYS)
    .value("SER", SER)
    .value("CYS", CYS)
    .value("CYH", CYH)
    .value("CYD", CYD)
    .value("TYR", TYR)
    .value("THR", THR)
    .value("VAL", VAL)
    .value("ILE", ILE)
    .value("LEU", LEU)
    .value("GLY", GLY)
    .value("PRO", PRO)
    .value("CPR", CPR)
    .value("TPR", TPR)
    .value("MET", MET)
    .value("HIS", HIS)
    .value("HSD", HSD)
    .value("HSE", HSE)
    .value("PHE", PHE)
    .value("TRP", TRP)
    .value("XXX", XXX)
    .export_values()
  ;

  def("TLCToRotID",&TLCToRotID,(arg("tlc")));

  def("AAToRotID",&AAToRotID,(arg("aa")));

  def("RotIDToAA",&RotIDToAA,(arg("rot_id")));
}
