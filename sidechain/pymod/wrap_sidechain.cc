// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>

void export_Connector();
void export_Disulfid();
void export_Frame();
void export_RotamerGraph();
void export_Particle();
void export_Rotamer();
void export_RotamerCruncher();
void export_RotamerDensity();
void export_RotamerIDs();
void export_RotamerLib();
void export_SidechainObjectLoader();
void export_RotamerConstructor();
void export_SubrotamerOptimizer();

using namespace boost::python;

BOOST_PYTHON_MODULE(_sidechain)
{
  export_Connector();
  export_Disulfid();
  export_Frame();
  export_RotamerGraph();
  export_Particle();
  export_Rotamer();
  export_RotamerCruncher();
  export_RotamerDensity();
  export_RotamerIDs();
  export_RotamerLib(); 
  export_SidechainObjectLoader();
  export_RotamerConstructor();
  export_SubrotamerOptimizer();
}
