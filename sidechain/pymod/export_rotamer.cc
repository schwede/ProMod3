// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/sidechain/rotamer.hh>
#include <promod3/sidechain/rotamer_group.hh>

using namespace boost::python;
using namespace promod3::sidechain;
using namespace promod3;

namespace {

void (FRMRotamer::*WrapFRMApplyOnRes)(ost::mol::ResidueHandle&,
                                      bool, const String&) const
                                     = &FRMRotamer::ApplyOnResidue;
void (FRMRotamer::*WrapFRMApplyOnResAA)(loop::AllAtomPositions&,
                                        uint) const
                                       = &FRMRotamer::ApplyOnResidue;

void (RRMRotamer::*WrapRRMApplyOnRes)(ost::mol::ResidueHandle&,
                                      bool, const String&) const
                                     = &RRMRotamer::ApplyOnResidue;
void (RRMRotamer::*WrapRRMApplyOnResAA)(loop::AllAtomPositions&,
                                        uint) const
                                       = &RRMRotamer::ApplyOnResidue;

void (FRMRotamerGroup::*WrapFRMGApplyOnRes)(uint, ost::mol::ResidueHandle&,
                                            bool, const String&) const
                                           = &FRMRotamerGroup::ApplyOnResidue;
void (FRMRotamerGroup::*WrapFRMGApplyOnResAA)(uint, loop::AllAtomPositions&,
                                              uint) const
                                             = &FRMRotamerGroup::ApplyOnResidue;

void (RRMRotamerGroup::*WrapRRMGApplyOnRes)(uint, ost::mol::ResidueHandle&,
                                            bool, const String&) const
                                           = &RRMRotamerGroup::ApplyOnResidue;
void (RRMRotamerGroup::*WrapRRMGApplyOnResAA)(uint, loop::AllAtomPositions&,
                                              uint) const
                                             = &RRMRotamerGroup::ApplyOnResidue;

RRMRotamerGroupPtr WrapRRMRotamerGroupInit(const boost::python::list& rotamers,
                                           uint residue_index) {
  std::vector<RRMRotamerPtr> v_rotamers;
  core::ConvertListToVector(rotamers, v_rotamers);
  return RRMRotamerGroupPtr(new RRMRotamerGroup(v_rotamers,residue_index));
}

FRMRotamerGroupPtr WrapFRMRotamerGroupInit(const boost::python::list& rotamers,
                                           uint residue_index) {
  std::vector<FRMRotamerPtr> v_rotamers;
  core::ConvertListToVector(rotamers, v_rotamers);
  return FRMRotamerGroupPtr(new FRMRotamerGroup(v_rotamers,residue_index));
}

RRMRotamerPtr WrapRRMRotamerInit(const boost::python::list& particles,
                                 Real probability,
                                 Real internal_e_prefactor){
  std::vector<Particle> v_particles;
  core::ConvertListToVector(particles, v_particles);
  RRMRotamerPtr p(new RRMRotamer(v_particles, probability, 
                                 internal_e_prefactor));
  return p;
}

FRMRotamerPtr WrapFRMRotamerInit(const boost::python::list& particles,
                                 Real T,
                                 Real probability,
                                 Real internal_e_prefactor){
  std::vector<Particle> v_particles;
  core::ConvertListToVector(particles, v_particles);
  FRMRotamerPtr p(new FRMRotamer(v_particles, T, probability, 
                                 internal_e_prefactor));
  return p;
}

Particle WrapFRMGetItem(FRMRotamerPtr p, uint index) {
  if (index >= p->size()) {
    throw promod3::Error("Invalid particle index!");
  }
  Particle ret_particle = (*p)[index];
  return ret_particle;
}

Particle WrapRRMGetItem(RRMRotamerPtr p, uint index) {
  if (index >= p->size()) {
    throw promod3::Error("Invalid particle index!");
  }
  Particle ret_particle = (*p)[index];
  return ret_particle;
}

boost::python::list WrapGetSubrotamerDefinition(FRMRotamerPtr p,
                                                uint subrotamer_index) {
  if (subrotamer_index >= p->subrotamer_size()) {
    throw promod3::Error("Invalid subrotamer index observed!");
  }

  boost::python::list return_list;
  FRMRotamer::subrotamer_iterator i = p->subrotamers_begin() + subrotamer_index;
  core::AppendToList(i->begin(), i->end(), return_list);
  return return_list;
}


void WrapAddSubrotamerDefinition(FRMRotamerPtr p,
                                 const boost::python::list& subrot_definition) {

  std::vector<int> v_subrot_definition;
  core::ConvertListToVector(subrot_definition, v_subrot_definition);
  p->AddSubrotamerDefinition(v_subrot_definition);
}

Real GetFRMFrameEnergyOne(FRMRotamerPtr p) {
  return p->GetFrameEnergy();
}

Real GetFRMFrameEnergyTwo(FRMRotamerPtr p, uint index) {
  if (index >= p->subrotamer_size()) {
    throw promod3::Error("Invalid subrotamer index");
  }
  return p->GetFrameEnergy(index);
}

void SetFRMFrameEnergyOne(FRMRotamerPtr p, Real e) {
  p->SetFrameEnergy(e);
}

void SetFRMFrameEnergyTwo(FRMRotamerPtr p, Real e, uint index) {
  if (index >= p->subrotamer_size()) {
    throw promod3::Error("Invalid subrotamer index");
  }
  p->SetFrameEnergy(e,index);
}

void AddFRMFrameEnergyOne(FRMRotamerPtr p, Real e) {
  p->AddFrameEnergy(e);
}

void AddFRMFrameEnergyTwo(FRMRotamerPtr p, Real e, uint index) {
  if (index >= p->subrotamer_size()) {
    throw promod3::Error("Invalid subrotamer index");
  }
  p->AddFrameEnergy(e,index);
}

}

void export_Rotamer()
{  

  class_<RRMRotamer>("RRMRotamer", no_init)
    .def("__init__",boost::python::make_constructor(&WrapRRMRotamerInit))
    .def("__len__",&RRMRotamer::size)
    .def("__getitem__",&WrapRRMGetItem,arg("index"))
    .def("ApplyOnResidue", WrapRRMApplyOnRes,
         (arg("res"), arg("consider_hydrogens")=false, arg("new_res_name")=""))
    .def("ApplyOnResidue", WrapRRMApplyOnResAA,
         (arg("all_atom"), arg("res_idx")))
    .def("ToFrameResidue", &RRMRotamer::ToFrameResidue, (arg("res_idx")))
    .def("GetInternalEnergyPrefactor",&RRMRotamer::GetInternalEnergyPrefactor)
    .def("GetInternalEnergy",&RRMRotamer::GetInternalEnergy)
    .def("GetFrameEnergy",&RRMRotamer::GetFrameEnergy)
    .def("GetSelfEnergy",&RRMRotamer::GetSelfEnergy)
    .def("GetProbability",&RRMRotamer::GetProbability)
    .def("SetInternalEnergyPrefactor",&RRMRotamer::SetInternalEnergyPrefactor,
         (arg("prefactor")))
    .def("SetInternalEnergy",&RRMRotamer::SetInternalEnergy,
         (arg("internal_energy")))
    .def("SetFrameEnergy",&RRMRotamer::SetFrameEnergy,(arg("frame_energy")))
    .def("AddFrameEnergy",&RRMRotamer::AddFrameEnergy,(arg("frame_energy")))
    .def("SetProbability",&RRMRotamer::SetProbability,(arg("probability")))
  ; 

  register_ptr_to_python<RRMRotamerPtr>();

  class_<FRMRotamer>("FRMRotamer", no_init)
    .def("__init__",boost::python::make_constructor(&WrapFRMRotamerInit))
    .def("__len__",&FRMRotamer::size)
    .def("__getitem__",&WrapFRMGetItem,arg("index"))
    .def("GetNumSubrotamers",&FRMRotamer::subrotamer_size)
    .def("GetSubrotamerDefinition",&WrapGetSubrotamerDefinition, (arg("index")))
    .def("SetActiveSubrotamer", &FRMRotamer::SetActiveSubrotamer, (arg("idx")))
    .def("GetActiveSubrotamer", &FRMRotamer::GetActiveSubrotamer)
    .def("ApplyOnResidue", WrapFRMApplyOnRes,
         (arg("res"), arg("consider_hydrogens")=false, arg("new_res_name")=""))
    .def("ApplyOnResidue", WrapFRMApplyOnResAA,
         (arg("all_atom"), arg("res_idx")))
    .def("ToFrameResidue", &FRMRotamer::ToFrameResidue, (arg("res_idx")))
    .def("ToRRMRotamer", &FRMRotamer::ToRRMRotamer)
    .def("GetInternalEnergyPrefactor",&FRMRotamer::GetInternalEnergyPrefactor)
    .def("GetInternalEnergy",&FRMRotamer::GetInternalEnergy)
    .def("GetFrameEnergy",&GetFRMFrameEnergyOne)
    .def("GetFrameEnergy",&GetFRMFrameEnergyTwo)
    .def("GetSelfEnergy",&FRMRotamer::GetSelfEnergy)
    .def("GetTemperature",&FRMRotamer::GetTemperature)
    .def("GetProbability",&FRMRotamer::GetProbability)
    .def("SetInternalEnergyPrefactor",&FRMRotamer::SetInternalEnergyPrefactor,(arg("prefactor")))
    .def("SetInternalEnergy",&FRMRotamer::SetInternalEnergy,
         (arg("internal_energy")))
    .def("SetFrameEnergy",&SetFRMFrameEnergyOne,(arg("frame_energy")))
    .def("SetFrameEnergy",&SetFRMFrameEnergyTwo,
         (arg("frame_energy"),arg("subrotamer_index")))
    .def("AddFrameEnergy",&AddFRMFrameEnergyOne,(arg("frame_energy")))
    .def("AddFrameEnergy",&AddFRMFrameEnergyTwo,(arg("frame_energy"),arg("subrotamer_index")))
    .def("AddSubrotamerDefinition", &WrapAddSubrotamerDefinition, (arg("subrotamer_definition")))
    .def("SetTemperature",&FRMRotamer::SetTemperature,(arg("temperature")))
    .def("SetProbability",&FRMRotamer::SetProbability,(arg("probability")))
  ; 

  register_ptr_to_python<FRMRotamerPtr>();


  class_<RRMRotamerGroup, boost::noncopyable>("RRMRotamerGroup",no_init)
    .def("__init__",boost::python::make_constructor(&WrapRRMRotamerGroupInit))
    .def("__len__",&RRMRotamerGroup::size)
    .def("__getitem__", &RRMRotamerGroup::operator[], (arg( "index" )))
    .def("ApplyOnResidue", WrapRRMGApplyOnRes,
         (arg("index"), arg("res"), arg("consider_hydrogens")=false,
          arg("new_res_name")=""))
    .def("ApplyOnResidue", WrapRRMGApplyOnResAA,
         (arg("index"), arg("all_atom"), arg("res_idx")))
    .def("Merge",&RRMRotamerGroup::Merge,(arg("other")))
    .def("SetFrameEnergy",&RRMRotamerGroup::SetFrameEnergy,(arg("frame")))
    .def("AddFrameEnergy",&RRMRotamerGroup::AddFrameEnergy,(arg("frame")))
    .def("ApplySelfEnergyThresh", &RRMRotamerGroup::ApplySelfEnergyThresh,
         (arg("thresh")=30))
    .def("GetResidueIndex", &RRMRotamerGroup::GetResidueIndex)
  ;

  register_ptr_to_python<RRMRotamerGroupPtr>();


  class_<FRMRotamerGroup, boost::noncopyable>("FRMRotamerGroup",no_init)
    .def("__init__",boost::python::make_constructor(&WrapFRMRotamerGroupInit))
    .def("__len__",&FRMRotamerGroup::size)
    .def("__getitem__", &FRMRotamerGroup::operator[], (arg("index")))
    .def("ApplyOnResidue", WrapFRMGApplyOnRes,
         (arg("index"), arg("res"), arg("consider_hydrogens")=false,
          arg("new_res_name")=""))
    .def("ApplyOnResidue", WrapFRMGApplyOnResAA,
         (arg("index"), arg("all_atom"), arg("res_idx")))
    .def("Merge",&FRMRotamerGroup::Merge,(arg("other")))
    .def("SetFrameEnergy",&FRMRotamerGroup::SetFrameEnergy,(arg("frame")))
    .def("AddFrameEnergy",&FRMRotamerGroup::AddFrameEnergy,(arg("frame")))
    .def("ApplySelfEnergyThresh", &FRMRotamerGroup::ApplySelfEnergyThresh,
         (arg("thresh")=30))
    .def("GetResidueIndex", &FRMRotamerGroup::GetResidueIndex)
  ;

  register_ptr_to_python<FRMRotamerGroupPtr>();

}
