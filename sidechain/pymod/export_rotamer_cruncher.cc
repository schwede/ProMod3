// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/sidechain/rotamer_cruncher.hh>
#include <promod3/core/export_helper.hh>

using namespace boost::python;
using namespace promod3;
using namespace promod3::sidechain;

namespace {

void wrap_Initialize(RotamerCruncherPtr cruncher, const String& seqres, 
                     const boost::python::list& weights) {
  std::vector<Real> v_weights;
  core::ConvertListToVector(weights, v_weights);
  cruncher->Initialize(seqres, v_weights);
}
void wrap_UpdateRotamerProbabilities(RotamerCruncherPtr cruncher,
                                     boost::python::list rotamers,
                                     uint seqres_pos, Real sigma) {
  std::vector<RotamerLibEntryPtr> v_rotamers;
  core::ConvertListToVector(rotamers, v_rotamers);
  cruncher->UpdateRotamerProbabilities(v_rotamers, seqres_pos, sigma);
}

}

void export_RotamerCruncher()
{
  class_<RotamerCruncher>("RotamerCruncher", init<>())
    .def("Initialize", &wrap_Initialize)
    .def("Save", &RotamerCruncher::Save,(arg("filename")))
    .def("Load", &RotamerCruncher::Load,(arg("filename"))).staticmethod("Load")
    .def("AddStructure", &RotamerCruncher::AddStructure)
    .def("MergeDensities", &RotamerCruncher::MergeDensities)
    .def("GetExtractedProbability", &RotamerCruncher::GetExtractedProbability,
         (arg("seqres_pos"), arg("chi1"),
          arg("chi2")=std::numeric_limits<Real>::quiet_NaN()))
    .def("UpdateRotamerProbabilities", &wrap_UpdateRotamerProbabilities)
  ;  

  register_ptr_to_python<RotamerCruncherPtr>();
}
