// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/graph_minimizer.hh>
#include <promod3/sidechain/rotamer_graph.hh>
#include <promod3/core/export_helper.hh>

using namespace boost::python;
using namespace promod3::sidechain;
using namespace promod3;

namespace {

RotamerGraphPtr WrapRRMList(boost::python::list& rotamer_groups) {
  std::vector<RRMRotamerGroupPtr> v_rotamer_groups;
  core::ConvertListToVector(rotamer_groups, v_rotamer_groups);
  return RotamerGraph::CreateFromList(v_rotamer_groups);
}

RotamerGraphPtr WrapFRMList(boost::python::list& rotamer_groups) {
  std::vector<FRMRotamerGroupPtr> v_rotamer_groups;
  core::ConvertListToVector(rotamer_groups, v_rotamer_groups);
  return RotamerGraph::CreateFromList(v_rotamer_groups);
}

}


void export_RotamerGraph() {


  class_<RotamerGraph,  
         bases<promod3::core::GraphMinimizer> >("RotamerGraph", init<>())

    .def("CreateFromRRMList", &WrapRRMList, (arg("rotamer_groups"))).staticmethod("CreateFromRRMList")
    .def("CreateFromFRMList", &WrapFRMList, (arg("rotamer_groups"))).staticmethod("CreateFromFRMList")
  ;  

  register_ptr_to_python<RotamerGraphPtr>();
}
