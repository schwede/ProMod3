// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>
#include <promod3/sidechain/rotamer_density.hh>

using namespace boost::python;
using namespace promod3;
using namespace promod3::sidechain;

namespace {

RotamerDensity1DPtr WrapRotamerDensity1DListInit(boost::python::list& densities,
                                                 boost::python::list& weights) {
  std::vector<RotamerDensity1DPtr> v_densities;
  std::vector<Real> v_weights;
  core::ConvertListToVector(densities, v_densities);
  core::ConvertListToVector(weights, v_weights);
  return RotamerDensity1DPtr(new RotamerDensity1D(v_densities, v_weights));
}

RotamerDensity1DPtr WrapRotamerDensity1DInit(uint num_bins){
  return RotamerDensity1DPtr(new RotamerDensity1D(num_bins));
}

RotamerDensity2DPtr WrapRotamerDensity2DListInit(boost::python::list& densities,
                                                 boost::python::list& weights) {
  std::vector<RotamerDensity2DPtr> v_densities;
  std::vector<Real> v_weights;
  core::ConvertListToVector(densities, v_densities);
  core::ConvertListToVector(weights, v_weights);
  return RotamerDensity2DPtr(new RotamerDensity2D(v_densities, v_weights));
}

RotamerDensity2DPtr WrapRotamerDensity2DInit(uint num_bins){
  return RotamerDensity2DPtr(new RotamerDensity2D(num_bins));
}

Real wrap_GetPOne(RotamerDensity1DPtr p, Real chi1){
  Real chi_angles[] = {chi1};
  return p->GetP(chi_angles);
}

Real wrap_GetPTwo(RotamerDensity2DPtr p, Real chi1, Real chi2){
  Real chi_angles[] = {chi1, chi2};
  return p->GetP(chi_angles);
}

}


void export_RotamerDensity()
{  

  class_<RotamerDensity, boost::noncopyable>("RotamerDensity", no_init);
  register_ptr_to_python<RotamerDensityPtr>();

  class_<RotamerDensity1D,bases<RotamerDensity> >("RotamerDensity1D", no_init)
    .def("__init__",make_constructor(&WrapRotamerDensity1DInit))
    .def("__init__",make_constructor(&WrapRotamerDensity1DListInit))
    .def("Normalize",&RotamerDensity1D::Normalize)
    .def("AddRotamer",&RotamerDensity1D::AddRotamer,(arg("rot")))
    .def("GetNumRotamers",&RotamerDensity1D::GetNumRotamers)
    .def("BinsPerDimension",&RotamerDensity1D::BinsPerDimension)
    .def("Bins",&RotamerDensity1D::Bins)
    .def("ApplyFactor",&RotamerDensity1D::ApplyFactor)
    .def("GetP",&wrap_GetPOne,(arg("chi1")))
    .def("MergeProbabilities",&RotamerDensity1D::MergeProbabilities)
    .def("Clear",&RotamerDensity1D::Clear)
    //.def("Data",&RotamerDensity1D::Data)
  ;

  register_ptr_to_python<RotamerDensity1DPtr>();

  class_<RotamerDensity2D,bases<RotamerDensity> >("RotamerDensity2D", no_init)
    .def("__init__",make_constructor(&WrapRotamerDensity2DInit))
    .def("__init__",make_constructor(&WrapRotamerDensity2DListInit))
    .def("Normalize",&RotamerDensity2D::Normalize)
    .def("AddRotamer",&RotamerDensity2D::AddRotamer,(arg("rot")))
    .def("GetNumRotamers",&RotamerDensity2D::GetNumRotamers)
    .def("BinsPerDimension",&RotamerDensity2D::BinsPerDimension)
    .def("Bins",&RotamerDensity2D::Bins)
    .def("ApplyFactor",&RotamerDensity2D::ApplyFactor)
    .def("GetP",&wrap_GetPTwo,(arg("chi1"),arg("chi2")))
    .def("MergeProbabilities",&RotamerDensity2D::MergeProbabilities)
    .def("Clear",&RotamerDensity2D::Clear)
    //.def("Data",&RotamerDensity2D::Data)
  ;

  register_ptr_to_python<RotamerDensity2DPtr>();
}
