// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/sidechain/rotamer.hh>
#include <promod3/sidechain/rotamer_group.hh>
#include <promod3/sidechain/scwrl4_rotamer_constructor.hh>
#include <promod3/sidechain/scwrl3_rotamer_constructor.hh>
#include <promod3/sidechain/vina_rotamer_constructor.hh>
#include <promod3/sidechain/vina_particle_scoring.hh>

using namespace boost::python;
using namespace promod3::sidechain;
using namespace promod3;


namespace{

SCWRL4RotamerConstructorPtr WrapSCWRL4RotamerConstructorInit(bool cb_in_sidechain) {
  SCWRL4RotamerConstructorPtr p(new SCWRL4RotamerConstructor(cb_in_sidechain));
  return p;
}

SCWRL3RotamerConstructorPtr WrapSCWRL3RotamerConstructorInit(bool cb_in_sidechain) {
  SCWRL3RotamerConstructorPtr p(new SCWRL3RotamerConstructor(cb_in_sidechain));
  return p;
}

VINARotamerConstructorPtr WrapVINARotamerConstructorInit(bool cb_in_sidechain) {
  VINARotamerConstructorPtr p(new VINARotamerConstructor(cb_in_sidechain));
  return p;
}

RRMRotamerGroupPtr WrapRRMGroup_res(RotamerConstructorPtr constructor,
                                    const ost::mol::ResidueHandle& res,
                                    RotamerID id,
                                    uint residue_idx,
                                    RotamerLibPtr rot_lib,
                                    Real phi, Real psi, bool n_ter, bool c_ter,
                                    Real probability_cutoff){
  return constructor->ConstructRRMRotamerGroup(res, id, residue_idx, 
                                               rot_lib, phi, psi, n_ter, c_ter,
                                               probability_cutoff);
}

RRMRotamerGroupPtr WrapRRMGroup_aa(RotamerConstructorPtr constructor,
                                   const promod3::loop::AllAtomPositions& all_atom_pos,
                                   uint all_atom_pos_idx,
                                   RotamerID rotamer_id,
                                   uint residue_idx,
                                   RotamerLibPtr rot_lib,
                                   Real phi, Real psi,
                                   bool n_ter, bool c_ter,
                                   Real probability_cutoff){
  return constructor->ConstructRRMRotamerGroup(all_atom_pos, all_atom_pos_idx,
                                               rotamer_id, residue_idx,
                                               rot_lib, phi, psi, n_ter, c_ter, 
                                               probability_cutoff);
}

FRMRotamerGroupPtr WrapFRMGroup_res(RotamerConstructorPtr constructor,
                                    const ost::mol::ResidueHandle& res,
                                    RotamerID id,
                                    uint residue_idx,
                                    RotamerLibPtr rot_lib, 
                                    Real phi, Real psi,
                                    bool n_ter, bool c_ter,
                                    Real probability_cutoff){
  return constructor->ConstructFRMRotamerGroup(res, id, residue_idx, 
                                               rot_lib, phi, psi,
                                               n_ter, c_ter, 
                                               probability_cutoff);
}

FRMRotamerGroupPtr WrapFRMGroup_aa(RotamerConstructorPtr constructor,
                                   const promod3::loop::AllAtomPositions& all_atom_pos,
                                   uint all_atom_pos_idx,
                                   RotamerID rotamer_id,
                                   uint residue_idx,
                                   RotamerLibPtr rot_lib,
                                   Real phi, Real psi, bool n_ter, bool c_ter,
                                   Real probability_cutoff){
  return constructor->ConstructFRMRotamerGroup(all_atom_pos, all_atom_pos_idx,
                                               rotamer_id, residue_idx,
                                               rot_lib, phi, psi, n_ter, c_ter,
                                               probability_cutoff);
}

RRMRotamerGroupPtr WrapRRMGroup_bbdep_res(RotamerConstructorPtr constructor,
                                          const ost::mol::ResidueHandle& res,
                                          RotamerID id,
                                          uint residue_idx,
                                          BBDepRotamerLibPtr rot_lib,
                                          Real phi, Real  psi,
                                          bool n_ter, bool c_ter,
                                          Real probability_cutoff){
  return constructor->ConstructRRMRotamerGroup(res, id, residue_idx, 
                                               rot_lib, phi, psi, n_ter, c_ter, 
                                               probability_cutoff);
}

RRMRotamerGroupPtr WrapRRMGroup_bbdep_aa(RotamerConstructorPtr constructor,
                                         const promod3::loop::AllAtomPositions& all_atom_pos,
                                         uint all_atom_pos_idx,
                                         RotamerID rotamer_id,
                                         uint residue_idx,
                                         BBDepRotamerLibPtr rot_lib,
                                         Real phi, Real psi,
                                         bool n_ter, bool c_ter,
                                         Real probability_cutoff){
  return constructor->ConstructRRMRotamerGroup(all_atom_pos, all_atom_pos_idx,
                                               rotamer_id, residue_idx,
                                               rot_lib, phi, psi, 
                                               probability_cutoff);
}

FRMRotamerGroupPtr WrapFRMGroup_bbdep_res(RotamerConstructorPtr constructor,
                                          const ost::mol::ResidueHandle& res,
                                          RotamerID id,
                                          uint residue_idx,
                                          BBDepRotamerLibPtr rot_lib,
                                          Real phi, Real  psi,
                                          bool n_ter, bool c_ter,
                                          Real probability_cutoff){
  return constructor->ConstructFRMRotamerGroup(res, id, residue_idx, 
                                              rot_lib, phi, psi, n_ter, c_ter,
                                              probability_cutoff);
}

FRMRotamerGroupPtr WrapFRMGroup_bbdep_aa(RotamerConstructorPtr constructor,
                                         const promod3::loop::AllAtomPositions& all_atom_pos,
                                         uint all_atom_pos_idx,
                                         RotamerID rotamer_id,
                                         uint residue_idx,
                                         BBDepRotamerLibPtr rot_lib,
                                         Real phi, Real  psi,
                                         bool n_ter, bool c_ter,
                                         Real probability_cutoff){
  return constructor->ConstructFRMRotamerGroup(all_atom_pos, all_atom_pos_idx,
                                               rotamer_id, residue_idx,
                                               rot_lib, phi, psi, n_ter, c_ter, 
                                               probability_cutoff);
}

RRMRotamerGroupPtr WrapRRMGroup_entries_res(RotamerConstructorPtr constructor,
                                            const ost::mol::ResidueHandle& res,
                                            RotamerID id,
                                            uint residue_idx,
                                            const boost::python::list& entries,
                                            Real phi, Real psi, 
                                            bool n_ter, bool c_ter,
                                            Real probability_cutoff){
  std::vector<RotamerLibEntry> v_entries;
  core::ConvertListToVector(entries, v_entries);
  return constructor->ConstructRRMRotamerGroup(res, id, residue_idx, 
                                               v_entries, phi, psi, 
                                               n_ter, c_ter,
                                               probability_cutoff);
}

RRMRotamerGroupPtr WrapRRMGroup_entries_aa(RotamerConstructorPtr constructor,
                                           const promod3::loop::AllAtomPositions& all_atom_pos,
                                           uint all_atom_pos_idx,
                                           RotamerID rotamer_id,
                                           uint residue_idx,
                                           const boost::python::list& entries,
                                           Real phi, Real psi, 
                                           bool n_ter, bool c_ter,
                                           Real probability_cutoff){
  std::vector<RotamerLibEntry> v_entries;
  core::ConvertListToVector(entries, v_entries);
  return constructor->ConstructRRMRotamerGroup(all_atom_pos, all_atom_pos_idx,
                                               rotamer_id, residue_idx,
                                               v_entries, phi, psi, n_ter, c_ter,
                                               probability_cutoff);
}

FRMRotamerGroupPtr WrapFRMGroup_entries_res(RotamerConstructorPtr constructor,
                                            const ost::mol::ResidueHandle& res,
                                            RotamerID id,
                                            uint residue_idx,
                                            const boost::python::list& entries,
                                            Real phi, Real psi, 
                                            bool n_ter, bool c_ter,
                                            Real probability_cutoff){
  std::vector<RotamerLibEntry> v_entries;
  core::ConvertListToVector(entries, v_entries);
  return constructor->ConstructFRMRotamerGroup(res, id, residue_idx, 
                                               v_entries, 
                                               phi, psi, n_ter, c_ter,
                                               probability_cutoff);
}

FRMRotamerGroupPtr WrapFRMGroup_entries_aa(RotamerConstructorPtr constructor,
                                           const promod3::loop::AllAtomPositions& all_atom_pos,
                                           uint all_atom_pos_idx,
                                           RotamerID rotamer_id,
                                           uint residue_idx,
                                           const boost::python::list& entries,
                                           Real phi, Real psi, bool n_ter, 
                                           bool c_ter,
                                           Real probability_cutoff){
  std::vector<RotamerLibEntry> v_entries;
  core::ConvertListToVector(entries, v_entries);
  return constructor->ConstructFRMRotamerGroup(all_atom_pos, all_atom_pos_idx,
                                               rotamer_id, residue_idx,
                                               v_entries, phi, psi, n_ter, c_ter,
                                               probability_cutoff);
}

FrameResiduePtr WrapBBFrame_res(RotamerConstructorPtr constructor,
                                const ost::mol::ResidueHandle& residue,
                                RotamerID id, uint residue_index,
                                Real phi, Real psi, bool n_ter, bool c_ter){
    return constructor->ConstructBackboneFrameResidue(residue, id, residue_index,
                                                      phi, psi, n_ter, c_ter);
}

FrameResiduePtr WrapBBFrame_aa(RotamerConstructorPtr constructor,
                               const promod3::loop::AllAtomPositions& all_atom_pos,
                               uint all_atom_pos_idx,
                               RotamerID id, uint residue_index,
                               Real phi, Real psi, bool n_ter, bool c_ter){
    return constructor->ConstructBackboneFrameResidue(all_atom_pos, all_atom_pos_idx, 
                                                      id, residue_index,
                                                      phi, psi, n_ter, c_ter);
}


FrameResiduePtr WrapSCFrame_res(RotamerConstructorPtr constructor,
                                const ost::mol::ResidueHandle& residue,
                                RotamerID id, uint residue_index,
                                Real phi, Real psi, bool n_ter, bool c_ter){
    return constructor->ConstructSidechainFrameResidue(residue, id, residue_index,
                                                       phi, psi, n_ter, c_ter);
}

FrameResiduePtr WrapSCFrame_aa(RotamerConstructorPtr constructor,
                               const promod3::loop::AllAtomPositions& all_atom_pos,
                               uint all_atom_pos_idx,
                               RotamerID id, uint residue_index,
                               Real phi, Real psi, bool n_ter, bool c_ter){
    return constructor->ConstructSidechainFrameResidue(all_atom_pos, all_atom_pos_idx, 
                                                      id, residue_index, phi, psi,
                                                      n_ter, c_ter);
}

void AssignInternalEnergiesRRM(RotamerConstructorPtr constructor,
                               RRMRotamerGroupPtr group, RotamerID id,
                               uint residue_index,
                               Real phi, Real psi, bool n_ter, bool c_ter){
  constructor->AssignInternalEnergies(group, id, residue_index, phi, psi,
                                      n_ter, c_ter);
}

void AssignInternalEnergiesFRM(RotamerConstructorPtr constructor,
                               FRMRotamerGroupPtr group, RotamerID id,
                               uint residue_index,
                               Real phi, Real psi, bool n_ter, bool c_ter){
  constructor->AssignInternalEnergies(group, id, residue_index, phi, psi, 
                                      n_ter, c_ter);
}

} // anon ns



void export_RotamerConstructor(){

  class_<RotamerConstructor, RotamerConstructorPtr, boost::noncopyable>("RotamerConstructor", no_init)
    .def("ConstructRRMRotamerGroup", &WrapRRMGroup_res,(arg("residue"),
                                                        arg("rotamer_id"),
                                                        arg("residue_idx"),
                                                        arg("rot_lib"),
                                                        arg("phi") = -1.0472,
                                                        arg("psi") = -0.7854,
                                                        arg("n_ter") = false,
                                                        arg("c_ter") = false,
                                                        arg("probability_cutoff") = 0.98))
    .def("ConstructRRMRotamerGroup", &WrapRRMGroup_aa,(arg("all_atom_pos"),
                                                       arg("all_atom_pos_idx"),
                                                       arg("rotamer_id"),
                                                       arg("residue_idx"),
                                                       arg("rot_lib"),
                                                       arg("phi") = -1.0472,
                                                       arg("psi") = -0.7854,
                                                       arg("n_ter") = false,
                                                       arg("c_ter") = false,                                                       
                                                       arg("probability_cutoff") = 0.98))
    .def("ConstructFRMRotamerGroup", &WrapFRMGroup_res,(arg("residue"),
                                                        arg("rotamer_id"),
                                                        arg("residue_idx"),
                                                        arg("rot_lib"),
                                                        arg("phi") = -1.0472,
                                                        arg("psi") = -0.7854,
                                                        arg("n_ter") = false,
                                                        arg("c_ter") = false,
                                                        arg("probability_cutoff") = 0.98))
    .def("ConstructFRMRotamerGroup", &WrapFRMGroup_aa,(arg("all_atom_pos"),
                                                       arg("all_atom_pos_idx"),
                                                       arg("rotamer_id"),
                                                       arg("residue_idx"),
                                                       arg("rot_lib"),
                                                       arg("phi") = -1.0472,
                                                       arg("psi") = -0.7854,
                                                       arg("n_ter") = false,
                                                       arg("c_ter") = false,
                                                       arg("probability_cutoff") = 0.98))
    .def("ConstructRRMRotamerGroup", &WrapRRMGroup_bbdep_res,(arg("residue"),
                                                              arg("rotamer_id"),
                                                              arg("residue_idx"),
                                                              arg("rot_lib"),
                                                              arg("phi") = -1.0472,
                                                              arg("psi") = -0.7854,
                                                              arg("n_ter") = false,
                                                              arg("c_ter") = false,
                                                              arg("probability_cutoff") = 0.98))
    .def("ConstructRRMRotamerGroup", &WrapRRMGroup_bbdep_aa,(arg("all_atom_pos"),
                                                             arg("all_atom_pos_idx"),
                                                             arg("rotamer_id"),
                                                             arg("residue_idx"),
                                                             arg("rot_lib"),
                                                             arg("phi") = -1.0472,
                                                             arg("psi") = -0.7854,
                                                             arg("n_ter") = false,
                                                             arg("c_ter") = false,
                                                             arg("probability_cutoff") = 0.98))
    .def("ConstructFRMRotamerGroup", &WrapFRMGroup_bbdep_res,(arg("residue"),
                                                              arg("rotamer_id"),
                                                              arg("residue_idx"),
                                                              arg("rot_lib"),
                                                              arg("phi") = -1.0472,
                                                              arg("psi") = -0.7854,
                                                              arg("n_ter") = false,
                                                              arg("c_ter") = false,
                                                              arg("probability_cutoff") = 0.98))
    .def("ConstructFRMRotamerGroup", &WrapFRMGroup_bbdep_aa,(arg("all_atom_pos"),
                                                             arg("all_atom_pos_idx"),
                                                             arg("rotamer_id"),
                                                             arg("residue_idx"),
                                                             arg("rot_lib"),
                                                             arg("phi") = -1.0472,
                                                             arg("psi") = -0.7854,
                                                             arg("phi") = false,
                                                             arg("psi") = false,
                                                             arg("probability_cutoff") = 0.98))
    .def("ConstructRRMRotamerGroup", &WrapRRMGroup_entries_res,(arg("residue"),
                                                                arg("rotamer_id"),
                                                                arg("residue_idx"),
                                                                arg("entries"),
                                                                arg("phi") = -1.0472,
                                                                arg("psi") = -0.7854,
                                                                arg("n_ter") = false,
                                                                arg("c_ter") = false,
                                                                arg("probability_cutoff") = 0.98))
    .def("ConstructRRMRotamerGroup", &WrapRRMGroup_entries_aa,(arg("all_atom_pos"),
                                                               arg("all_atom_pos_idx"),
                                                               arg("rotamer_id"),
                                                               arg("residue_idx"),
                                                               arg("entries"),
                                                               arg("phi") = -1.0472,
                                                               arg("psi") = -0.7854,
                                                               arg("n_ter") = false,
                                                               arg("c_ter") = false,
                                                               arg("probability_cutoff") = 0.98))
    .def("ConstructFRMRotamerGroup", &WrapFRMGroup_entries_res,(arg("residue"),
                                                                arg("rotamer_id"),
                                                                arg("residue_idx"),
                                                                arg("entries"),
                                                                arg("phi") = -1.0472,
                                                                arg("psi") = -0.7854,
                                                                arg("n_ter") = false,
                                                                arg("c_ter") = false,
                                                                arg("probability_cutoff") = 0.98))
    .def("ConstructFRMRotamerGroup", &WrapFRMGroup_entries_aa,(arg("all_atom_pos"),
                                                               arg("all_atom_pos_idx"),
                                                               arg("rotamer_id"),
                                                               arg("residue_idx"),
                                                               arg("entries"),
                                                               arg("phi") = -1.0472,
                                                               arg("psi") = -0.7854,
                                                               arg("n_ter") = false,
                                                               arg("c_ter") = false,
                                                               arg("probability_cutoff") = 0.98))
    .def("ConstructBackboneFrameResidue", &WrapBBFrame_res,(arg("residue"),
                                                            arg("rotamer_id"),
                                                            arg("residue_index"),
                                                            arg("phi") = -1.0472,
                                                            arg("psi") = -0.7854,
                                                            arg("n_ter")=false,
                                                            arg("c_ter")=false))
    .def("ConstructBackboneFrameResidue", &WrapBBFrame_aa,(arg("all_atom_pos"),
                                                           arg("all_atom_pos_idx"),
                                                           arg("rotamer_id"),
                                                           arg("residue_index"),
                                                           arg("phi") = -1.0472,
                                                           arg("psi") = -0.7854,
                                                           arg("n_ter")=false,
                                                           arg("c_ter")=false))
    .def("ConstructSidechainFrameResidue", &WrapSCFrame_res,(arg("residue"),
                                                             arg("rotamer_id"),
                                                             arg("residue_index"),
                                                             arg("phi") = -1.0472,
                                                             arg("psi") = -0.7854,
                                                             arg("n_ter")=false,
                                                             arg("c_ter")=false))
    .def("ConstructSidechainFrameResidue", &WrapSCFrame_aa,(arg("all_atom_pos"),
                                                            arg("all_atom_pos_idx"),
                                                            arg("rotamer_id"),
                                                            arg("residue_index"),
                                                            arg("phi") = -1.0472,
                                                            arg("psi") = -0.7854,
                                                            arg("n_ter")=false,
                                                            arg("c_ter")=false))
    .def("AssignInternalEnergies", &AssignInternalEnergiesRRM, (arg("rot_group"),
                                                                arg("rotamer_id"),
                                                                arg("residue_index"),
                                                                arg("phi")=-1.0472,
                                                                arg("psi")=-0.7854,
                                                                arg("n_ter")=false,
                                                                arg("c_ter")=false))
    .def("AssignInternalEnergies", &AssignInternalEnergiesFRM, (arg("rot_group"),
                                                                arg("rotamer_id"),
                                                                arg("residue_index"),
                                                                arg("phi")=-1.0472,
                                                                arg("psi")=-0.7854,
                                                                arg("n_ter")=false,
                                                                arg("c_ter")=false))
  ;


  class_<SCWRL4RotamerConstructor, SCWRL4RotamerConstructorPtr, 
         bases<RotamerConstructor> >("SCWRL4RotamerConstructor", no_init)
    .def("__init__", boost::python::make_constructor(&WrapSCWRL4RotamerConstructorInit))
    .def("ConstructFrameResidue",
         &SCWRL4RotamerConstructor::ConstructFrameResidue, (arg("res"),
                                                           arg("residue_index")))
    .def("ConstructFrameResidueHeuristic",
         &SCWRL4RotamerConstructor::ConstructFrameResidueHeuristic, (arg("res"),
                                                                    arg("residue_index"),
                                                                    arg("comp_lib")))
  ;


  class_<SCWRL3RotamerConstructor, SCWRL3RotamerConstructorPtr, 
         bases<RotamerConstructor> >("SCWRL3RotamerConstructor", no_init)
    .def("__init__", boost::python::make_constructor(&WrapSCWRL3RotamerConstructorInit))
  ;


  class_<VINARotamerConstructor, VINARotamerConstructorPtr, 
         bases<RotamerConstructor> >("VINARotamerConstructor", no_init)
    .def("__init__", boost::python::make_constructor(&WrapVINARotamerConstructorInit))
    .def("ConstructFrameResidueHeuristic", 
         &VINARotamerConstructor::ConstructFrameResidueHeuristic, (arg("res"), 
                                                                   arg("residue_index")))
    .def("ConstructRRMRotamerHeuristic", 
         &VINARotamerConstructor::ConstructRRMRotamerHeuristic, (arg("res")))
    .def("ConstructFRMRotamerHeuristic", 
         &VINARotamerConstructor::ConstructFRMRotamerHeuristic, (arg("res")))
  ;

  def("SetVINAWeightGaussian1", &SetVINAWeightGaussian1, (arg("weight")));
  def("SetVINAWeightGaussian2", &SetVINAWeightGaussian2, (arg("weight")));
  def("SetVINAWeightRepulsion", &SetVINAWeightRepulsion, (arg("weight")));
  def("SetVINAWeightHydrophobic", &SetVINAWeightHydrophobic, (arg("weight")));
  def("SetVINAWeightHBond", &SetVINAWeightHBond, (arg("weight")));

  def("GetVINAWeightGaussian1", &GetVINAWeightGaussian1);
  def("GetVINAWeightGaussian2", &GetVINAWeightGaussian2);
  def("GetVINAWeightRepulsion", &GetVINAWeightRepulsion);
  def("GetVINAWeightHydrophobic", &GetVINAWeightHydrophobic);
  def("GetVINAWeightHBond", &GetVINAWeightHBond);
}
