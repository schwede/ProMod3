// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <exception>

#include <promod3/core/export_helper.hh>
#include <promod3/sidechain/disulfid.hh>

using namespace promod3;
using namespace promod3::sidechain;
using namespace boost::python;

namespace{

Real WrapDisulfidOne(RRMRotamerPtr rot_one, RRMRotamerPtr rot_two,
                     const geom::Vec3& ca_pos_one, const geom::Vec3& cb_pos_one, 
                     const geom::Vec3& ca_pos_two, const geom::Vec3& cb_pos_two){

  return DisulfidScore(rot_one, rot_two,ca_pos_one, cb_pos_one, 
                       ca_pos_two, cb_pos_two);
}

Real WrapDisulfidTwo(FRMRotamerPtr rot_one, FRMRotamerPtr rot_two,
                     const geom::Vec3& ca_pos_one, const geom::Vec3& cb_pos_one, 
                     const geom::Vec3& ca_pos_two, const geom::Vec3& cb_pos_two){

  return DisulfidScore(rot_one, rot_two,ca_pos_one, cb_pos_one, 
                       ca_pos_two, cb_pos_two);
}

boost::python::tuple WrapResolveCysteins(const boost::python::list& rot_groups,
                                         const boost::python::list& ca_pos,
                                         const boost::python::list& cb_pos,
                                         Real score_threshold, 
                                         bool optimize_subrotamers){

  boost::python::list disulfid_return_list;
  boost::python::list rotamer_return_list;
  if(boost::python::len(rot_groups) == 0){
    return boost::python::make_tuple(disulfid_return_list, 
                                     rotamer_return_list);
  }

  std::vector<geom::Vec3> v_ca_pos;
  std::vector<geom::Vec3> v_cb_pos;
  promod3::core::ConvertListToVector(ca_pos, v_ca_pos);
  promod3::core::ConvertListToVector(cb_pos, v_cb_pos);
  std::vector<std::pair<uint, uint> > disulfid_indices; 
  std::vector<std::pair<uint, uint> > rotamer_indices;
  // find out what we're dealing with by only looking at the first element
  // since we're only dealing with smart users, this will be sufficient...
  boost::python::extract<FRMRotamerGroupPtr> extractor(rot_groups[0]);

  if(extractor.check()){
    std::vector<promod3::sidechain::FRMRotamerGroupPtr> v_rot_groups;
    promod3::core::ConvertListToVector(rot_groups, v_rot_groups);
    ResolveCysteins(v_rot_groups, v_ca_pos, v_cb_pos, score_threshold,
                    optimize_subrotamers, 
                    disulfid_indices, rotamer_indices);
  }
  else{
    std::vector<promod3::sidechain::RRMRotamerGroupPtr> v_rot_groups;
    promod3::core::ConvertListToVector(rot_groups, v_rot_groups);
    ResolveCysteins(v_rot_groups, v_ca_pos, v_cb_pos, score_threshold,
                    optimize_subrotamers,
                    disulfid_indices, rotamer_indices);
  }

  for(uint i = 0; i < disulfid_indices.size(); ++i){
    disulfid_return_list.append(boost::python::make_tuple(disulfid_indices[i].first,
                                                          disulfid_indices[i].second));

    rotamer_return_list.append(boost::python::make_tuple(rotamer_indices[i].first,
                                                         rotamer_indices[i].second));
  }

    return boost::python::make_tuple(disulfid_return_list, rotamer_return_list);
}

}


void export_Disulfid(){

  def("DisulfidScore",&WrapDisulfidOne,(arg("rotamer_one"), arg("rotamer_two"),
                                        arg("ca_pos_one"), arg("cb_pos_one"),
                                        arg("ca_pos_two"), arg("cb_pos_two")));
  def("DisulfidScore",&WrapDisulfidTwo,(arg("rotamer_one"), arg("rotamer_two"),
                                        arg("ca_pos_one"), arg("cb_pos_one"),
                                        arg("ca_pos_two"), arg("cb_pos_two")));

  def("ResolveCysteins", &WrapResolveCysteins, (arg("rotamer_groups"),
                                                arg("ca_positions"),
                                                arg("cb_positions"),
                                                arg("score_threshold") = 45.0,
                                                arg("optimize_subrotamers") = false));
}