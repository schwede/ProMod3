// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/sidechain/frame.hh>
#include <promod3/core/export_helper.hh>

using namespace boost::python;
using namespace promod3::sidechain;
using namespace promod3;

namespace {

FramePtr WrapFrameInit(boost::python::list& frame_residues){
  std::vector<FrameResiduePtr> v_residues;
  core::ConvertListToVector(frame_residues, v_residues);
  return FramePtr(new Frame(v_residues));
}

FrameResiduePtr WrapFrameResidueInit(boost::python::list& particles,
                                     uint residue_index) {

  std::vector<Particle> v_particles;
  core::ConvertListToVector(particles, v_particles);
  return FrameResiduePtr(new FrameResidue(v_particles, residue_index));
}

Particle WrapGetItem(FrameResiduePtr p, uint index){
  if(index >= p->size()){
    throw promod3::Error("Invalid Index");
  }
  Particle ret_particle = (*p)[index];
  return ret_particle;
}

}

void export_Frame()
{
  class_<Frame, boost::noncopyable>("Frame", no_init)
    .def("__init__",make_constructor(&WrapFrameInit))
  ;  

  register_ptr_to_python<FramePtr>();

  class_<FrameResidue>("FrameResidue", no_init)
    .def("__init__",make_constructor(&WrapFrameResidueInit))
    .def("__len__", &FrameResidue::size)
    .def("__getitem__", &WrapGetItem, (arg( "index" )))
    .def("ApplyOnResidue", &FrameResidue::ApplyOnResidue)
    .def("GetResidueIndex", &FrameResidue::GetResidueIndex)
  ;

  register_ptr_to_python<FrameResiduePtr>();
}
