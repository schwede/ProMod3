// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/sidechain/particle.hh>
#include <promod3/sidechain/scwrl4_particle_scoring.hh>
#include <promod3/sidechain/scwrl3_particle_scoring.hh>
#include <promod3/sidechain/vina_particle_scoring.hh>

using namespace boost::python;
using namespace promod3::sidechain;

namespace{

ParticlePtr WrapCreateSCWRL4Particle(const String& name, 
                                     SCWRL4ParticleType particle_type,
                                     const geom::Vec3& pos,
                                     Real charge,
                                     const geom::Vec3List& lone_pairs,
                                     const geom::Vec3& polar_direction) {
  SCWRL4Param* p = new SCWRL4Param(particle_type, pos, charge);
  if(geom::Length2(polar_direction) > Real(0.000001)) {
    p->SetPolarDirection(polar_direction);
  }
  for(uint i = 0; i < lone_pairs.size(); ++i) {
    p->AddLonePair(lone_pairs[i]);
  }
  ParticlePtr return_ptr(new Particle(name, p));
  return return_ptr;
}

ParticlePtr WrapCreateSCWRL3Particle(const String& name,
                                     Real radius,
                                     const geom::Vec3& pos) {
  SCWRL3Param* p = new SCWRL3Param(pos, radius);
  ParticlePtr return_ptr(new Particle(name, p));
  return return_ptr;
}

ParticlePtr WrapCreateVINAParticle(const String& name, 
                                   VINAParticleType particle_type,
                                   const geom::Vec3& pos) {
  VINAParam* p = new VINAParam(particle_type, pos);
  ParticlePtr return_ptr(new Particle(name, p));
  return return_ptr;
}

}


void export_Particle() {
  
  enum_<PScoringFunction>("PScoringFunction")
    .value("SCWRL4", SCWRL4)
    .value("SCWRL3", SCWRL3)
    .value("VINA", VINA)
  ;

  class_<Particle, ParticlePtr>("Particle", no_init)
    .def("PairwiseScore", &Particle::PairwiseScore, (arg("other_particle")))
    .def("GetName", &Particle::GetName, 
         return_value_policy<copy_const_reference>())
    .def("GetPos", &Particle::GetPos, 
         return_value_policy<copy_const_reference>())
    .def("GetCollisionDistance", &Particle::GetCollisionDistance)
    .def("GetScoringFunction", &Particle::GetScoringFunction)
  ;  


  // SCWRL4 specific stuff
  enum_<SCWRL4ParticleType>("SCWRL4ParticleType")
    .value("HParticle", HParticle)
    .value("CParticle", CParticle)
    .value("CH1Particle", CH1Particle)
    .value("CH2Particle", CH2Particle)
    .value("CH3Particle", CH3Particle)
    .value("NParticle", NParticle)
    .value("OParticle", OParticle)
    .value("OCParticle", OCParticle)
    .value("SParticle", SParticle)
  ;

  def("CreateSCWRL4Particle", 
      &WrapCreateSCWRL4Particle, (arg("name"), 
                                  arg("particle_type"),
                                  arg("pos"),  
                                  arg("charge") = 0.0, 
                                  arg("lone_pairs") = geom::Vec3List(),
                                  arg("polar_direction") = geom::Vec3()));


  // SCWRL3 specific stuff
  def("CreateSCWRL3Particle",
      &WrapCreateSCWRL3Particle, (arg("name"),
                                  arg("radius"),
                                  arg("pos")));

  // VINA specific stuff
  enum_<VINAParticleType>("VINAParticleType")
    .value("O_D_VINAParticle",O_D_VINAParticle) 
    .value("N_D_VINAParticle",N_D_VINAParticle)  
    .value("O_A_VINAParticle",O_A_VINAParticle) 
    .value("N_A_VINAParticle",N_A_VINAParticle)  
    .value("O_AD_VINAParticle",O_AD_VINAParticle) 
    .value("N_AD_VINAParticle",N_AD_VINAParticle) 
    .value("O_VINAParticle",O_VINAParticle)    
    .value("N_VINAParticle",N_VINAParticle)    
    .value("S_VINAParticle",S_VINAParticle)    
    .value("P_VINAParticle",P_VINAParticle)    
    .value("C_P_VINAParticle", C_P_VINAParticle)  
    .value("C_VINAParticle",C_VINAParticle)    
    .value("F_VINAParticle",F_VINAParticle)    
    .value("Cl_VINAParticle",Cl_VINAParticle)   
    .value("Br_VINAParticle",Br_VINAParticle)   
    .value("I_VINAParticle",I_VINAParticle)    
    .value("M_VINAParticle",M_VINAParticle)    
    .value("INVALID_VINAParticle",INVALID_VINAParticle) 
  ;

  def("CreateVINAParticle",
      &WrapCreateVINAParticle, (arg("name"),
                                arg("particle_type"),
                                arg("pos")));
}
