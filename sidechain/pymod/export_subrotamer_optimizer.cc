// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/register_ptr_to_python.hpp>

#include <promod3/core/export_helper.hh>
#include <promod3/sidechain/subrotamer_optimizer.hh>


using namespace promod3;
using namespace promod3::sidechain;
using namespace boost::python;


namespace{

void WrapOptimizer(boost::python::list& rotamers,
                   Real active_internal_energy,
                   Real inactive_internal_energy,
                   uint max_complexity,
                   Real initial_epsilon) {

  std::vector<FRMRotamerPtr> v_rotamers;
  promod3::core::ConvertListToVector(rotamers, v_rotamers);

  promod3::sidechain::SubrotamerOptimizer(v_rotamers, 
                                          active_internal_energy,
                                          inactive_internal_energy, 
                                          max_complexity,
                                          initial_epsilon);
}

}

void export_SubrotamerOptimizer() {

  def("SubrotamerOptimizer", &WrapOptimizer,(arg("rotamers"),
                                             arg("active_internal_energy")=-2.0,
                                             arg("inactive_internal_energy")=0.0,
                                             arg("max_complexity")=100000000,
                                             arg("initial_epsilon")=0.02));
}






