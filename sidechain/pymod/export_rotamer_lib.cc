// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/register_ptr_to_python.hpp>

#include <promod3/sidechain/bb_dep_rotamer_lib.hh>
#include <promod3/sidechain/rotamer_lib.hh>
#include <promod3/sidechain/rotamer_lib_reader.hh>

using namespace promod3;
using namespace promod3::sidechain;
using namespace boost::python;

namespace {

boost::python::list query_bbdep_wrapper(sidechain::BBDepRotamerLibPtr lib, 
                                        RotamerID id, Real phi, Real psi) {

  boost::python::list return_list;
  std::pair<RotamerLibEntry*,uint> lib_entries = lib->QueryLib(id, phi, psi);
  for (uint i = 0; i < lib_entries.second; ++i) {
    return_list.append(*lib_entries.first);
    ++lib_entries.first;
  }
  return return_list;
}

boost::python::list query_wrapper(sidechain::RotamerLibPtr lib,
                                  RotamerID id) {

  boost::python::list return_list;
  std::pair<RotamerLibEntry*,uint> lib_entries = lib->QueryLib(id);
  for (uint i = 0; i < lib_entries.second; ++i) {
    return_list.append(*lib_entries.first);
    ++lib_entries.first;
  }
  return return_list;
}

RotamerLibEntryPtr FromResidue_one(const ost::mol::ResidueHandle& res,
                                   RotamerID id){
  return RotamerLibEntry::FromResidue(res,id);
}

RotamerLibEntryPtr FromResidue_two(const ost::mol::ResidueView& res,
                                   RotamerID id){
  return RotamerLibEntry::FromResidue(res,id);
}

RotamerLibEntryPtr FromResidue_three(const ost::mol::ResidueHandle& res){
  return RotamerLibEntry::FromResidue(res);
}

RotamerLibEntryPtr FromResidue_four(const ost::mol::ResidueView& res){
  return RotamerLibEntry::FromResidue(res);
}

bool IsSimilar_one(RotamerLibEntryPtr p, RotamerLibEntryPtr other, 
                   Real thresh){
  return p->IsSimilar(other,thresh);
}

bool IsSimilar_two(RotamerLibEntryPtr p, RotamerLibEntryPtr other, 
                   Real thresh, RotamerID id){
  return p->IsSimilar(other,thresh,id);
}

bool IsSimilar_three(RotamerLibEntryPtr p, RotamerLibEntryPtr other, 
                     Real thresh, const String& res_name){
  return p->IsSimilar(other,thresh,res_name);
}

bool SimilarDihedral_one(RotamerLibEntryPtr p, RotamerLibEntryPtr other,
                         uint dihedral_idx, Real thresh){
  return p->SimilarDihedral(other,dihedral_idx,thresh);
}

bool SimilarDihedral_two(RotamerLibEntryPtr p, RotamerLibEntryPtr other,
                         uint dihedral_idx, Real thresh, RotamerID id){
  return p->SimilarDihedral(other,dihedral_idx,thresh,id);
}

bool SimilarDihedral_three(RotamerLibEntryPtr p, RotamerLibEntryPtr other,
                           uint dihedral_idx, Real thresh, const String& res_name){
  return p->SimilarDihedral(other,dihedral_idx,thresh,res_name);
}


}

void export_RotamerLib(){

  class_<RotamerLibEntry>("RotamerLibEntry",init<Real,Real,Real,Real,Real,Real,Real,Real,Real>())

    .def("FromResidue",&FromResidue_one)
    .def("FromResidue",&FromResidue_two)
    .def("FromResidue",&FromResidue_three)
    .def("FromResidue",&FromResidue_four).staticmethod("FromResidue")
    .def("IsSimilar",&IsSimilar_one)
    .def("IsSimilar",&IsSimilar_two)
    .def("IsSimilar",&IsSimilar_three)
    .def("SimilarDihedral",&SimilarDihedral_one)
    .def("SimilarDihedral",&SimilarDihedral_two)
    .def("SimilarDihedral",&SimilarDihedral_three)
    .def_readwrite("probability",&RotamerLibEntry::probability)
    .def_readwrite("chi1",&RotamerLibEntry::chi1)
    .def_readwrite("chi2",&RotamerLibEntry::chi2)
    .def_readwrite("chi3",&RotamerLibEntry::chi3)
    .def_readwrite("chi4",&RotamerLibEntry::chi4)
    .def_readwrite("sig1",&RotamerLibEntry::sig1)
    .def_readwrite("sig2",&RotamerLibEntry::sig2)
    .def_readwrite("sig3",&RotamerLibEntry::sig3)
    .def_readwrite("sig4",&RotamerLibEntry::sig4)
  ;

  register_ptr_to_python<RotamerLibEntryPtr>();

  class_<RotamerLib>("RotamerLib",init<>())
    .def("AddRotamer",&RotamerLib::AddRotamer,(arg("id"),arg("rotamer")))
    .def("Save",&RotamerLib::Save,(arg("filename")))
    .def("Load",&RotamerLib::Load,(arg("filename"))).staticmethod("Load")
    .def("SavePortable",&RotamerLib::SavePortable,(arg("filename")))
    .def("LoadPortable",&RotamerLib::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("QueryLib",&query_wrapper,(arg("id")))
    .def("MakeStatic",&RotamerLib::MakeStatic)
  ;

  class_<BBDepRotamerLib>("BBDepRotamerLib",init<uint,uint>())
    .def("AddRotamer",&BBDepRotamerLib::AddRotamer,(arg("id"),arg("r1"),arg("r2"),arg("r3"),arg("r4"),arg("phi_bin"),arg("psi_bin"),arg("rotamer")))
    .def("Save",&BBDepRotamerLib::Save,(arg("filename")))
    .def("Load",&BBDepRotamerLib::Load,(arg("filename"))).staticmethod("Load")
    .def("SavePortable",&BBDepRotamerLib::SavePortable,(arg("filename")))
    .def("LoadPortable",&BBDepRotamerLib::LoadPortable,(arg("filename"))).staticmethod("LoadPortable")
    .def("QueryLib",&query_bbdep_wrapper,(arg("id"),arg("phi"),arg("psi")))
    .def("SetInterpolate",&BBDepRotamerLib::SetInterpolate,(arg("do_it")))
    .def("MakeStatic",&BBDepRotamerLib::MakeStatic)
  ;

  register_ptr_to_python<BBDepRotamerLibPtr>();
  register_ptr_to_python<RotamerLibPtr>();


  enum_<DihedralConfiguration>("DihedralConfiguration")
    .value("INVALID", INVALID)
    .value("TRANS", TRANS)
    .value("GAUCHE_MINUS", GAUCHE_MINUS)
    .value("GAUCHE_PLUS", GAUCHE_PLUS)
    .value("NON_ROTAMERIC", NON_ROTAMERIC)
    .export_values()
  ;

  def("GetRotamericConfiguration", &GetRotamericConfiguration, (arg("angle")));
  def("GetDihedralConfiguration", &GetDihedralConfiguration, (arg("entry"),
                                                              arg("id"),
                                                              arg("dihedral_idx")));

  def("ReadDunbrackFile",&ReadDunbrackFile,(arg("filename")));
  def("ReadDaggetFile",&ReadDaggetFile,arg("filename"));

}
