..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Disulfid Bond Evaluation
================================================================================

.. currentmodule:: promod3.sidechain

When calculating the pairwise interaction energy between two rotamers building
a disulfid bond, one would get an unfavourable energy due to "clashes" between
the sulfur atoms. It is possible to improve performance in sidechain
reconstruction regarding cysteins when finding and separately handle
disulfid bonds. The scoring module implements an empirically derived disulfid
score (:func:`promod3.scoring.SCWRL3DisulfidScore`) as defined in 
[canutescu2003b]_. The paper proposes two rotamers to be in a disulfid
bonded state, if the resulting disulfid score plus the self energies of the 
involved rotamers is below 45. If there are several cysteines close together,
this problem gets another layer of complexity. One has to assure, that
every cysteine only participates in one disulfid bond but the network
of disulfid bonds is geometrically optimal. SCWRL4 proposes an approach,
that has also been implemented here.


.. method:: DisulfidScore(rotamer_one, rotamer_two, ca_pos_one, cb_pos_one, \
                          ca_pos_two, cb_pos_two)

  Directly extracts the positions of the gamma sulfurs from the rotamers by
  searching for particles with the name "SG".
  The found positions are then passed to 
  :func:`promod3.scoring.SCWRL3DisulfidScore`. 
  In case of :class:`RRMRotamer` it expects exactly one gamma sulfur per 
  rotamer. In case of :class:`FRMRotamer` there can be an arbitrary number 
  of gamma sulfurs per rotamer (at least one), it then evaluates the score 
  for all possible combinations of gamma sulfurs and takes the minimum score.
  To get a final DisulfidScore, it finally adds the self energies of the
  rotamers to the result of the geometric expression.

  :param rotamer_one:   First rotamer
  :param rotamer_two:   Second rotamer
  :param ca_pos_one:    CA position of first rotamer
  :param cb_pos_one:    CB position of first rotamer
  :param ca_pos_two:    CA position of second rotamer
  :param cb_pos_two:    CB position of second rotamer

  :type rotamer_one:    :class:`RRMRotamer` , :class:`FRMRotamer`
  :type rotamer_two:    :class:`RRMRotamer` , :class:`FRMRotamer`
  :type ca_pos_one:     :class:`ost.geom.Vec3`
  :type cb_pos_one:     :class:`ost.geom.Vec3`
  :type ca_pos_two:     :class:`ost.geom.Vec3`
  :type cb_pos_two:     :class:`ost.geom.Vec3`

  :raises:  :exc:`~exceptions.RuntimeError` if given rotamers do not contain
            exactly (in case of :class:`RRMRotamer`) or at least (in case of
            :class:`FRMRotamer`) one particle representing the gamma sulfur.

  :returns:             The result of the raw score plus the average 
                        self energies of the input rotamers


.. method:: ResolveCysteins(rotamer_groups, ca_positions, cb_positions,\
                            [score_threshold=45.0, optimize_subrotamers=False])

  Tries to optimize disulfid bond network. In a first step, all disulfid bonds
  get detected using :func:`DisulfidScore`. If the value between two rotamers is
  below **score_threshold**, they're assumed to be bonded. The function then 
  tries to detect the largest possible set of disulfide bonds, with no
  cysteine being part of more than one bond. If several largest sets are 
  possible, the one with the optimal sum of scores gets estimated.

  :param rotamer_groups: Every group represents a cysteine
  :param ca_positions:   The CA positions of the according rotamers 
  :param cb_positions:   The CB positions of the according rotamers
  :param score_threshold: The score two rotamers must have to be considered
                          as a disulfid bond
  :param optimize_subrotamers: If set to true and the input consists of flexible
                               rotamer groups, the active subrotamers get 
                               optimized. For every pair of rotamers
                               participating in a disulfid bond, the subrotamers
                               with best :func:`DisulfidScore` get activated in
                               the input **rotamer_groups**. This has an effect
                               when the rotamers get applied on residues.

  :type rotamer_groups:  :class:`list` of 
                         :class:`FRMRotamerGroup`/:class:`RRMRotamerGroup`
  :type ca_positions: :class:`list` of :class:`ost.geom.Vec3`
  :type cb_positions: :class:`list` of :class:`ost.geom.Vec3`
  :type score_threshold: :class:`float`
  :type optimize_subrotamers: :class:`bool`

  :returns: A :class:`tuple` containing two :class:`list` objects with equal
            length. Both lists contain :class:`tuple` objects with two elements.
            The tuples in the first list describe the indices of cysteins 
            participating in disulfid bonds. The tuples in the second list 
            describe the optimal rotamers in the according rotamer groups.
