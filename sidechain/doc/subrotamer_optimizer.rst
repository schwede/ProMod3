..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Subrotamer Optimization
================================================================================

.. currentmodule:: promod3.sidechain

The idea of the flexible rotamer model is to have several subrotamers
representing one single rotamer. 
One subrotamer is the representative (the active subrotamer).
Thats the one that gets inserted in the structure once the reconstruction has
finished. This is not necessarily the optimal one. The SubrotamerOptimizer
takes a list of flexible rotamers, converts every single flexible rotamer 
in a rotamer group of rigid rotamers, solves the sidechain reconstruction
problem and assigns the optimal subrotamers as active in the original 
flexible rotamers. In terms of energies, the SubrotamerOptimizer expects
the energy of the flexible rotamers towards the rigid frame already to be set.
The internal energies can be controlled as parameters and a constant value is
set for rotamers that are currently active or not. The reason for that is
that you might want to prefer the currently active subrotamers if no
pairwise energies to other rotamers are observed. 

.. method:: SubrotamerOptimizer(rotamers, [active_internal_energy=-2.0, \
                                inactive_internal_energy=0.0, \
                                max_complexity=100000000, initial_epsilon=0.02)

  Takes the **rotamers** of type :class:`FRMRotamer`, converts all their 
  subrotamers into rigid rotamers, solves the sidechain reconstruction problem
  and assigns the ideal subrotamers as active in the input **rotamers**.

  :param rotamers:      The rotamers to be optimized               
  :type rotamers:       :class:`list` of :class:`FRMRotamer`

  :param active_internal_energy: Internal energy that gets assigned to all
                                 currently active subrotamers
  :type active_internal_energy:  :class:`float`

  :param inactive_internal_energy: Internal energy that gets assigned to all
                                   currently inactive subrotamers
  :type inactive_internal_energy: :class:`float`

  :param max_complexity: Max complexity of the internal :class:`RotamerGraph`
  :type max_complexity: :class:`int`

  :param initial_epsilon: Epsilon value controlling the pruning of 
                          internal :class:`RotamerGraph`
  :type initial_epsilon: :class:`float`
