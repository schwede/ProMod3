..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Frame - The Rigid Part
================================================================================

.. currentmodule:: promod3.sidechain

In contrast to the rotamers, the frame is a rigid object. It either
represents the protein backbone or sidechains kept rigid during the 
sidechain modelling process. Regions, that should not be occupied by
any sidechain atoms can also be blocked by adding particles to the frame.
The frame is built using single frame residues, all of them associated to
a residues index. If a rotamer associated to the same residue index
enters the frame energy calculation, all interaction with particles 
belonging to the frame residue with the same residue index are neglected.


The Frame Objects
--------------------------------------------------------------------------------

.. class:: FrameResidue(particles, residue_index)

  :param particles:     particles building frame residue
  :param residue_index: Interaction energies between the constructed frame
                        residue and any rotamer associated to the same 
                        residue index are neglected
                        
  :type particles:      :class:`list` of :class:`Particle`
  :type residue_index:  :class:`int`

  .. method:: __len__()   

    :returns:           Number of particles in :class:`FrameResidue`


  .. method:: __getitem__(index)

    :param index:       :class:`Particle` index
    :type index:        :class:`int`

    :returns:           :class:`Particle` at position **index**

    :raises:   :exc:`~exceptions.RuntimeError` if index is invalid


 
.. class:: Frame(frame_residues)

  The :class:`Frame` object is used as a container for rigid particles, that
  can be passed to rotamer groups for calculating frame energies.

  :param frame_residues:  residues building the frame.
  :type frame_residues:   :class:`list` of :class:`FrameResidue`

