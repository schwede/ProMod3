..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Rotamer Graph
================================================================================

.. currentmodule:: promod3.sidechain

Once having a frame representing the rigid parts, the internal energies in
rotamer groups can be calculated. To come to a final solution of the sidechain
modelling problem, the pairwise energies also have to be evaluated and an
overall solution has to be found. |project| implements a
:class:`promod3.core.GraphMinimizer` that allows to find solutions using
tree decomposition, A* and Monte Carlo algorithms.

.. class:: RotamerGraph

  The :class:`RotamerGraph` objects inherits from 
  :class:`promod3.core.GraphMinimizer` and extends the minimizer by static
  initialization functions.

  .. staticmethod:: CreateFromRRMList(rotamer_groups)

  .. staticmethod:: CreateFromFRMList(rotamer_groups)

    :param rotamer_groups: :class:`RRMRotamerGroup` or :class:`FRMRotamerGroup` 
                           objects representing the possible sidechain 
                           conformations for every amino acid position.

    :type rotamer_groups:  :class:`list`


