..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:mod:`~promod3.sidechain` - Sidechain Modelling
================================================================================

.. module:: promod3.sidechain
  :synopsis: Sidechain Modelling

.. currentmodule:: promod3.sidechain

Tools and algorithms to model sidechains given backbone coordinates. The full
module is heavily based on SCWRL4 [krivov2009]_ . The according paper describes
the modelling of sidechains using two different rotamer models. A rigid model,
as well as a flexible model. Both models are implemented in |project| and can be
applied in flexible ways.

The following code fragment shows an example of a basic sidechain reconstruction
algorithm using the functionality in the module. Note, that this code will crash
as soon as you have structures containing all the weirdness the PDB throws at
us. In this case, you should use the full fletched sidechain reconstruction
pipelines available in the modelling module. 


.. literalinclude:: ../../../tests/doc/scripts/sidechain_steps.py

Contents:

.. toctree::
   :maxdepth: 2

   rotamer
   frame
   rotamer_constructor
   rotamer_lib
   graph
   disulfid
   loading
   subrotamer_optimizer


