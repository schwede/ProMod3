..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Representing Sidechains - Rotamers & Co. 
================================================================================

.. currentmodule:: promod3.sidechain

A rotamer represents an amino acid sidechain identified by a :class:`RotamerID` 
and is a set of :class:`Particle` objects. 
Two types of rotamers exist. The :class:`RRMRotamer` and :class:`FRMRotamer`. 
To gather all possible rotamers for one location,
|project| offers the :class:`RRMRotamerGroup` and :class:`FRMRotamerGroup`.
All parts of the structure that are kept rigid can be represented by
a :class:`Frame` object.


RotamerID
--------------------------------------------------------------------------------

The sidechain module has its own definition of amino acids to satisfy custom 
requirements for the implemented sidechain construction algorithms. 
As an example there are histidine in two possible protonation states, 
that affect the hbond term or different versions of proline/cysteine. 

.. class:: RotamerID

  Enumerates the amino acids. Possible values:

  .. hlist::
    :columns: 2
  
    * ARG - Arginine
    * ASN - Asparagine
    * ASP - Aspartate
    * GLN - Glutamine
    * GLU - Glutamate
    * LYS - Lysine
    * SER - Serine
    * CYS - Cystein
    * CYH - "free" Cystein
    * CYD - disulfid bonded Cystein
    * MET - Methionine
    * TRP - Tryptophane
    * TYR - Tyrosine
    * THR - Threonine
    * VAL - Valine
    * ILE - Isoleucine
    * LEU - Leucine
    * PRO - Proline
    * CPR - cis-Proline
    * TPR - trans-Proline
    * HIS - Histidine
    * HSD - d-protonated Histidine
    * HSE - e-protonated Histidine
    * PHE - Phenylalanine
    * GLY - Glycine
    * ALA - Alanine
    * XXX - Invalid
  
  The RotamerID enum can be accessed either directly as ``promod3.sidechain.ARG``
  or as ``promod3.sidechain.RotamerID.ARG``.

  
How can I get an ID?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The RotamerID enum can directly be accessed from Python. Two convenient
functions exist to get RotamerIDs from the :class:`ost.conop.AminoAcid` enum
or from amino acid three letter codes.

.. method:: TLCToRotID(tlc)

  Directly translates the three letter code into a RotamerID. Following
  exactly the naming convention defined above.  

  :param tlc:           Three letter code of amino acid
  :type tlc:            :class:`str`

  :returns:             :class:`RotamerID`, XXX if **tlc** cannot be recoginzed.


.. method:: AAToRotID(aa)

  Directly translates **aa** into a RotamerID. Note, that it is not possible
  to generate special IDs this way 
  (e.g. HSD, HSE or the special prolines/cysteins) since they're simply not
  defined in :class:`ost.conop.AminoAcid` 

  :param aa:            AA enum of amino acid
  :type  aa:            :class:`ost.conop.AminoAcid`

  :returns:             :class:`RotamerID`, XXX if **aa** is invalid.


The Smallest Building Block - The Particle
--------------------------------------------------------------------------------

Particles give raise to more complex objects such as rotamers and frame 
residues. They contain all data required to calculate pairwise energies.
For every energy function available in |project|, there's a particle creation
function.

.. class:: PScoringFunction

  The available scoring functions between :class:`Particle` objects

  * SCWRL4 - :ref:`scwrl4-scoring-function`
  * SCWRL3 - :ref:`scwrl3-scoring-function`
  * VINA - :ref:`vina-scoring-function`

.. class:: Particle

  The particle class. There's no constructor. You can either use the
  :class:`RotamerConstructor` to create full :class:`RotamerGroup` objects
  with all underlying particles or the energy function specific creation 
  functions.

  .. method:: PairwiseScore(other_particle)

    Calculates score between the two particles

    :param other_particle:  The interacting particle
    :type other_particle:  :class:`Particle`

    :returns:           The score
    :rtype:             :class:`float`

    :raises:  :exc:`~exceptions.RuntimeError` if the scoring function 
              parametrization of the two particles is inconsistent         

  .. method:: GetName()

    :returns:           The name of the particle, which corresponds to the
                        atom name
    :rtype:             :class:`str`

  .. method:: GetCollisionDistance()

    :returns:           The "collision distance" all pairs of particles with 
                        their distance below the sum of their collision 
                        distances are considered as interacting and thus
                        evaluated by the underlying scoring function.

    :rtype:             :class:`float`

  .. method:: GetPos()

    :returns:           The position of the particle
    :rtype:             :class:`ost.geom.Vec3`

  .. method:: GetScoringFunction()

    :returns:           The underlying scoring function
    :rtype:             :class:`PScoringFunction`



.. _scwrl4-scoring-function:

The SCWRL4 scoring function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The SCWRL4 scoring function combines a Lennard-Jones style term with 
a hydrogen bond term. Details can be found in the relevant publication
[krivov2009]_.

.. class:: SCWRL4ParticleType

  The SCWRL4 energy function differentiates between following particle types
  that define the behaviour of the Lennard-Jones style term:

  * HParticle   - represents hydrogen
  * CParticle   - default representation of a carbon
  * CH1Particle - represents carbon bound to 1 hydrogen
  * CH2Particle - represents carbon bound to 2 hydrogen
  * CH3Particle - represents carbon bound to 3 hydrogen
  * NParticle   - represents nitrogen
  * OParticle   - default representation of oxygen
  * OCParticle  - represents carbonyl-oxygen for ASP/GLU
  * SParticle   - represents sulfur

.. method:: CreateSCWRL4Particle(name, particle_type, pos, [charge, \
                                 lone_pairs=None, polar_direction=None])

  Creates and returns a :class:`Particle` that can evaluate the SCWRL4 scoring 
  function

  :param name:          The name of the particle
  :param particle_type: The type of the particle
  :param pos:           The position of the particle
  :param charge:        The charge of the particle, relevant for the hydrogen 
                        bond term
  :param lone_pairs:    Direction of all possible lone pairs of the particle,
                        relevant for the hydrogen bond term. If set, the 
                        particle is a potential hydrogen bond acceptor.
                        An example would be the Serine OG atom, where you can 
                        represent the two lone pairs with vectors pointing
                        from the OG position towards the lone pair centers.
  :param polar_direction: The polar direction of the particle,
                          relevant for the hydrogen bond term. If set, the 
                          particle is a potential hydrogen bond donor. An
                          example would be the Serine HG hydrogen. The 
                          *polar_direction* would be a vector 
                          estimated as follows: hg_pos-og_pos.

  :type name:           :class:`str`
  :type particle_type:  :class:`SCWRL4ParticleType`
  :type pos:            :class:`ost.geom.Vec3`
  :type charge:         :class:`float`
  :type lone_pairs:     :class:`ost.geom.Vec3List`
  :type polar_direction: :class:`ost.geom.Vec3`


.. _scwrl3-scoring-function:

The SCWRL3 scoring function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The SCWRL3 scoring function implements a simple repulsion term that depends on
the hard-sphere radius of the involved particles. 
Details can be found in the relevant publication [canutescu2003]_.

.. method:: CreateSCWRL3Particle(name, radius, pos)

  Creates and returns a :class:`Particle` that can evaluate the SCWRL3 scoring
  function

  :param name:          The name of the particle
  :param radius:        The hard-sphere radius of the particle, relevant for the
                        repulsion term.
  :param pos:           The position of the particle

  :type name:           :class:`str`
  :type radius:         :class:`float`
  :type pos:            :class:`ost.geom.Vec3`


.. _vina-scoring-function:

The VINA scoring function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The VINA scoring function is a combination of functional forms that are named 
gaussian1, gaussian2, repulsion, hydrophobic and hbond in the Autodock Vina
software [trott2010]_. VINA only evaluates heavy atoms. Gaussian1, gaussian2
and repulsion are evaluated for all pairs of particles. Hydrophobic is only
evaluated between C_VINAParticle :class:`VINAParticleType` and hbond is
evaluated between hydrogen bond donor/acceptor pairs. While SCWRL3 and SCWRL4 
are intended to evaluate sidechain-sidechain interactions in proteins, 
VINA is mainly targeted at interactions between sidechains and ligands.

The functional forms are linearly combined with the default weights from
the Autodock Vina Software. They're set as global variables and can be
extracted with:

.. method:: GetVINAWeightGaussian1


.. method:: GetVINAWeightGaussian2


.. method:: GetVINAWeightRepulsion


.. method:: GetVINAWeightHydrophobic


.. method:: GetVINAWeightHBond

You can set custom weights. A call to the following functions overwrites 
according weights globally which affects any subsequent score evaluation:

.. method:: SetVINAWeightGaussian1(w)


.. method:: SetVINAWeightGaussian2(w)


.. method:: SetVINAWeightRepulsion(w)


.. method:: SetVINAWeightHydrophobic(w)


.. method:: SetVINAWeightHBond(w)



The VINA scoring function differentiates between the following particle types:

.. class:: VINAParticleType

    * O_D_VINAParticle - Oxygen that can be a hydrogen bond donor 
    * N_D_VINAParticle - Nitrogen that can be a hydrogen bond donor
    * O_A_VINAParticle - Oxygen that can be a hydrogen bond acceptor
    * N_A_VINAParticle - Nitrogen that can be a hydrogen bond acceptor
    * O_AD_VINAParticle - Oxygen that can be a hydrogen bond donor and acceptor
    * N_AD_VINAParticle - Nitrogen that can be a hydrogen bond donor and acceptor
    * O_VINAParticle - Oxygen
    * N_VINAParticle - Nitrogen    
    * S_VINAParticle - Sulfur    
    * P_VINAParticle - Phosphorus    
    * C_P_VINAParticle - Polar carbon that is covalently bound to a charged atom  
    * C_VINAParticle - Hydrophobic carbon that is only bound to other carbons or hydrogens
    * F_VINAParticle - Fluorine
    * Cl_VINAParticle - Chlorine   
    * Br_VINAParticle - Bromine
    * I_VINAParticle - Iodine    
    * M_VINAParticle - Metals    
    * INVALID_VINAParticle - Invalid particle... 


.. method:: CreateVINAParticle(name, particle_type, pos)

  Creates and returns a :class:`Particle` that can evaluate the VINA scoring
  function

  :param name:          The name of the particle
  :param radius:        The type of the particle
  :param pos:           The position of the particle

  :type name:           :class:`str`
  :type radius:         :class:`VINAParticleType`
  :type pos:            :class:`ost.geom.Vec3`


Rotamers
--------------------------------------------------------------------------------


.. class:: RRMRotamer(particles, probability, internal_e_prefactor)

  The RRMRotamer represents a rotamer of the so called rigid rotamer model.
 
  :param particles:     List of :class:`Particle` objects
  :param probability:   Probability of rotamers. In case of the SCWRL4
                        energy calculation, this directly controls the 
                        internal energy of that rotamer. 
  :param internal_e_prefactor: Factor applied to the internal energy calculated 
                               as -log(**probability**/max_probability),
                               where max_probability is the maximum
                               rotamer probability of any rotamer in a
                               particular :class:`RRMRotamerGroup`.

  :type particles:      :class:`list`
  :type probability:    :class:`float`
  :type internal_e_prefactor: :class:`float`


  .. method:: __getitem__(index)

    Access particle at specified index

    :param index:       Index of particle of interest

    :type index:        :class:`int`

    :returns:           :class:`Particle` at specified index

    :raises:  :exc:`~exceptions.RuntimeError` if index is invalid


  .. method:: __len__()

    :returns:           Number of particles the rotamer contains

  .. method:: ApplyOnResidue(res, consider_hydrogens=False, new_res_name="")

    Iterates over every particle and searches for the according atom in
    **res**. If it's present, the position gets reset to the particle position.
    If not, a new atom gets added to **res**. No atoms are removed from **res**
    in this process. 

    :param res:         Residue to be reconstructed
    :param consider_hydrogens: Flag, whether polar hydrogens should be added to 
                               **res**
    :param new_res_name: New name of **res**. Nothing happens in case of the 
                         default value ("")

    :type res:          :class:`ost.mol.ResidueHandle`
    :type consider_hydrogens: :class:`bool`
    :type new_res_name: :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if not all required backbone atoms
             are present in *res*

  .. method:: ApplyOnResidue(all_atom, res_idx)

    Set all sidechain atom positions for given residue to the positions of the
    particles in the rotamer.
    
    :param all_atom:    Container to which to apply rotamer
    :param res_idx:     Residue index into *all_atom*

    :type all_atom:     :class:`~promod3.loop.AllAtomPositions`
    :type res_idx:      :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_idx* is invalid

  .. method:: ToFrameResidue(res_idx)

    Generates and returns a :class:`FrameResidue` based on the internal
    particles.

    :param res_idx:     Idx passed over to :class:`FrameResidue` constructor
    :type res_idx:      :class:`int`

    :returns:           The constructed :class:`FrameResidue` 

  .. method:: GetInternalEnergyPrefactor()

    :returns:           Prefactor used in internal energy calculation


  .. method:: GetInternalEnergy()

    :returns:           Internal Energy if calculated, 0.0 otherwise 


  .. method:: GetFrameEnergy()

    Returns frame energy. This energy can either be manually set or calculated
    using a :class:`Frame` and the :class:`RRMRotamerGroup` this rotamer 
    belongs to.

    :returns: Frame energy if calculated, 0.0 otherwise 


  .. method:: GetSelfEnergy()

    :returns:           Self energy consisting of internal plus frame energy


  .. method:: GetProbability()

    :returns:           probability of this rotamer


  .. method:: SetInternalEnergyPrefactor(prefactor)

    :param energy:      Internal energy prefactor to be set

    :type energy:       :class:`float`


  .. method:: SetInternalEnergy(energy)

    :param energy:      Internal energy to be set

    :type energy:       :class:`float`


  .. method:: SetFrameEnergy(energy)

    :param energy:      Frame energy to be set

    :type energy:       :class:`float`


  .. method:: AddFrameEnergy(energy)

    :param energy:      Frame energy to be added

    :type energy:       :class:`float`


  .. method:: SetProbability(probability)

    :param energy:      Internal probability to be set

    :type energy:       :class:`float`



.. class:: FRMRotamer(particles, T, probability, internal_e_prefactor)

  The FRMRotamer represents a rotamer of the so called flexible rotamer model,
  where one rotamer gets represented by several subrotamers.
  The idea is that all particles of all subrotamers are given at
  initialization. Subrotamers are then defined by providing lists of indices.
  One particle can be part of several subrotamers.

  :param particles:     List of :class:`Particle` objects
  :param probability:   Probability of rotamers. In case of the SCWRL4
                        energy calculation, this directly controls the 
                        internal energy of that rotamer. 
  :param T:             Temperature factor, that is used to generate a final
                        energy given the subrotamers according to the formalism
                        described in the SCWRL4 paper.
  :param internal_e_prefactor: Factor applied to the internal energy calculated 
                               as -log(**probability**/max_probability),
                               where max_probability is the maximum
                               rotamer probability of any rotamer in a
                               particular :class:`FRMRotamerGroup`.

  :type particles:      :class:`list`
  :type probability:    :class:`float`
  :type T:              :class:`float`
  :type internal_e_prefactor: :class:`float`


  .. method:: __getitem__(index)

    Access particle at specified index

    :param index:       Index of particle of interest

    :type index:        :class:`int`

    :returns:           :class:`Particle` at specified index

    :raises:  :exc:`~exceptions.RuntimeError` if index is invalid


  .. method:: __len__()

    :returns:           Number of particles the rotamer contains


  .. method:: GetNumSubrotamers()

    :returns:           Number of subrotamers

  .. method:: ApplyOnResidue(res, consider_hydrogens=False, new_res_name="")

    Iterates over every particle of the active subrotamer and searches for the 
    according atom in **res**. If it's present, the position gets reset to the 
    particle position. If not, a new atom gets added to **res**. 
    No atoms are removed from **res** in this process. 

    :param res:         Residue to be reconstructed
    :param consider_hydrogens: Flag, whether polar hydrogens should be added to 
                               the sidechain
    :param new_res_name: New name of residue. Nothing happens in case of the 
                         default value ("")

    :type res:          :class:`ost.mol.ResidueHandle`
    :type consider_hydrogens: :class:`bool`
    :type new_res_name: :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if not all required backbone atoms
             are present in *res*

  .. method:: ApplyOnResidue(all_atom, res_idx)

    Set all sidechain atom positions for given residue to the positions of the
    particles in the active surotamer.
    
    :param all_atom:    Container to which to apply rotamer
    :param res_idx:     Residue index into *all_atom*

    :type all_atom:     :class:`~promod3.loop.AllAtomPositions`
    :type res_idx:      :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_idx* is invalid

  .. method:: ToFrameResidue(res_idx)

    Generates and returns a :class:`FrameResidue` based on the internal
    particles of the active subrotamer.

    :param res_idx:     Idx passed over to :class:`FrameResidue` constructor
    :type res_idx:      :class:`int`

    :returns:           The constructed :class:`FrameResidue`

  .. method:: ToRRMRotamer()

    Generates and returns a :class:`RRMRotamer` based on the internal
    particles of the active subrotamer. Following parameters of the 
    returned rotamer get set: probability (probability of the whole FRMRotamer),
    internal_e_prefactor (prefactor of the whole FRMRotamer),
    frame_energy (The frame energy of that particular subrotamer if already 
    calculated), internal_energy (the internal energy of the whole FRMRotamer)

  .. method:: GetSubrotamerDefinition(index)

    :param index:       Index of subrotamer

    :type index:        :class:`int`

    :returns:           :class:`list` of particle indices belonging to this
                        particular subrotamer

    :raises:  :exc:`~exceptions.RuntimeError` if index is invalid


  .. method:: GetInternalEnergyPrefactor()

    :returns:           Prefactor used in internal energy calculation

  .. method:: GetInternalEnergy()

    :returns:           Internal Energy if calculated, 0.0 otherwise 

  .. method:: GetFrameEnergy()

    Returns frame energy. This energy can either be manually set or calculated
    using a :class:`Frame` and the :class:`FRMRotamerGroup` this rotamer 
    belongs to.

    :returns: Frame energy if calculated, 0.0 otherwise 

  .. method:: GetFrameEnergy(index)

    Returns frame energy of specified **index**.

    :param index:       Index of subrotamer you want the frame energy from

    :type index:        :class:`int`

    :returns: Frame energy if calculated, 0.0 otherwise 

    :raises:  :exc:`~exceptions.RuntimeError` if index is invalid


  .. method:: GetSelfEnergy()

    :returns:           Self energy consisting of internal plus frame energy


  .. method:: GetTemperature()

    :returns:           The temperature factor for this rotamer


  .. method:: GetProbability()

    :returns:           Probability of this rotamer


  .. method:: SetInternalEnergyPrefactor(prefactor)

    :param energy:      Internal energy prefactor to be set

    :type energy:       :class:`float`


  .. method:: SetInternalEnergy(energy)

    :param energy:      Internal energy to be set

    :type energy:       :class:`float`


  .. method:: SetFrameEnergy(energy)

    :param energy:      Frame energy for full rotamer to be set

    :type energy:       :class:`float`


  .. method:: SetFrameEnergy(energy, index)

    :param energy:      Frame energy for single  subrotamer to be set
    :param index:       Index of subrotamer

    :type energy:       :class:`float`
    :type index:        :class:

  .. method:: AddFrameEnergy(energy)

    :param energy:      Frame energy for full rotamer to be added

    :type energy:       :class:`float`


  .. method:: AddFrameEnergy(energy, index)

    :param energy:      Frame energy for single  subrotamer to be added
    :param index:       Index of subrotamer

    :type energy:       :class:`float`
    :type index:        :class:

  .. method:: AddSubrotamerDefinition(indices)

    :param indices:     List of indices defining a subrotamer

    :type indices:      :class:`list`

  .. method:: SetActiveSubrotamer(idx)

    The provided **idx** relates to the subrotamer definitions added at the
    rotamer buildup. This idx controls which subrotamer is used when
    :func:`ApplyOnResidue`, :func:`ToFrameResidue` or :func:`ToRRMRotamer` 
    gets called. By default, the value is 0 => first added subrotamer 
    definition gets used.

    :param idx:         Index of subrotamer definition applied on residues

    :type idx:          :class:`int`

  .. method:: GetActiveSubrotamer()

    Get the index of the active subrotamer

  .. method:: SetTemperature(temperature)

    :param temperature: Temperature factor for this rotamer

    :type temperature:  :class:`float`

  .. method:: SetProbability(probability)

    :param energy:      Internal probability to be set

    :type energy:       :class:`float`



Rotamer Groups
--------------------------------------------------------------------------------


.. class:: RRMRotamerGroup(rotamers, residue_index)

  The RRMRotamerGroup groups several :class:`RRMRotamer` objects for the same
  residue position. 

  :param rotamers: A list of :class:`RRMRotamer` objects
  :param residue_index: Location of residue this :class:`FRMRotamerGroup`
                        represents. This index is important when calculating
                        frame energies to neglect the interactions to frame 
                        particles of the same residue.

  :type rotamers:       :class:`list`
  :type residue_index:  :class:`int`

  :raises:  :exc:`~exceptions.RuntimeError` if provided *rotamers* is empty


  .. method:: __len__()

    :returns:           Number of rotamers in group


  .. method:: __getitem__(index)

    :returns:           :class:`RRMRotamer` at given *index*

    :raises:  :exc:`~exceptions.RuntimeError` if *index* is invalid


  .. method:: ApplyOnResidue(index, res, consider_hydrogens=False,\
                             new_res_name="")
              ApplyOnResidue(index, all_atom, res_idx)

    Calls ApplyOnResidue function on rotamer at position *index*

    :param index:       Rotamer index
    :param res:         Residue to be reconstructed
    :param consider_hydrogens: Flag, whether polar hydrogens should be added to 
                               the sidechain
    :param new_res_name: New name of residue. Nothing happens in case of the 
                         default value ("")
    :param all_atom:    Container to which to apply rotamer
    :param res_idx:     Residue index into *all_atom*

    :type index:        :class:`int`
    :type res:          :class:`ost.mol.ResidueHandle`
    :type consider_hydrogens: :class:`bool`
    :type new_res_name: :class:`str`
    :type all_atom:     :class:`~promod3.loop.AllAtomPositions`
    :type res_idx:      :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if not all required backbone atoms
             are present in *res* or when *index* or *res_idx* are invalid


  .. method:: Merge(other)

    Adds all rotamers in *other* to current :class:`RRMRotamerGroup`

    :param other:       RotamerGroup to be merged in

    :type other:        :class:`RRMRotamerGroup`

  .. method:: SetFrameEnergy(frame)

    Calculates sets the energy of all rotamers in the group towards the 
    given **frame**.

    :param frame:       Frame containing rigid particles
    :type frame:        :class:`Frame`

  .. method:: AddFrameEnergy(frame)

    Calculates adds the energy of all rotamers in the group towards the 
    given **frame**.

    :param frame:       Frame containing rigid particles
    :type frame:        :class:`Frame`

  .. method:: ApplySelfEnergyThresh(thresh=30)

    Searches rotamer with lowest self energy *l_e* and deletes all
    rotamers with *self_energy* > *l_e* + *thresh*


.. class:: FRMRotamerGroup(rotamers, residue_index)

  The FRMRotamerGroup groups several :class:`FRMRotamer` objects for the same
  residue position. 

  :param rotamers: A list of :class:`FRMRotamer` objects
  :param residue_index: Location of residue this :class:`FRMRotamerGroup`
                        represents. This index is important when calculating
                        frame energies to neglect the interactions to frame 
                        particles of the same residue.

  :type rotamers:       :class:`list`
  :type residue_index:  :class:`int`

  :raises:  :exc:`~exceptions.RuntimeError` if provided *rotamers* is empty


  .. method:: __len__()

    :returns:           Number of rotamers in group


  .. method:: __getitem__(index)

    :returns:           :class:`FRMRotamer` at given *index*

    :raises:  :exc:`~exceptions.RuntimeError` if *index* is invalid


  .. method:: ApplyOnResidue(index, res, consider_hydrogens=False,\
                             new_res_name="")
              ApplyOnResidue(index, all_atom, res_idx)

    Calls ApplyOnResidue function on rotamer at position *index*

    :param index:       Rotamer index
    :param res:         Residue to be reconstructed
    :param consider_hydrogens: Flag, whether polar hydrogens should be added to 
                               the sidechain
    :param new_res_name: New name of residue. Nothing happens in case of the 
                         default value ("")
    :param all_atom:    Container to which to apply rotamer
    :param res_idx:     Residue index into *all_atom*

    :type index:        :class:`int`
    :type res:          :class:`ost.mol.ResidueHandle`
    :type consider_hydrogens: :class:`bool`
    :type new_res_name: :class:`str`
    :type all_atom:     :class:`~promod3.loop.AllAtomPositions`
    :type res_idx:      :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if not all required backbone atoms
             are present in *res* or when *index* or *res_idx* are invalid


  .. method:: Merge(other)

    Adds all rotamers in *other* to current :class:`FRMRotamerGroup`

    :param other:       RotamerGroup to be merged in

    :type other:        :class:`FRMRotamerGroup`

  .. method:: SetFrameEnergy(frame)

    Calculates sets the energy of all rotamers in the group towards the 
    given **frame**.

    :param frame:       Frame containing rigid particles
    :type frame:        :class:`Frame`

  .. method:: AddFrameEnergy(frame)

    Calculates adds the energy of all rotamers in the group towards the 
    given **frame**.

    :param frame:       Frame containing rigid particles
    :type frame:        :class:`Frame`

  .. method:: ApplySelfEnergyThresh(thresh=30)

    Searches rotamer with lowest self energy *l_e* and deletes all
    rotamers with *self_energy* > *l_e* + *thresh*

