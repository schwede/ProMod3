..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Rotamer Library
================================================================================

.. currentmodule:: promod3.sidechain


With angles and bonds being rather rigid, the conformation of an amino acid
sidechain can completely be described in terms of dihedral angles. Preferred
combinations of such dihedral angles are a result of steric properties and
can be gathered in rotamer libraries. Different libraries exist in the field
and their main difference is, whether the provided sidechain conformations 
are dependent on their backbone or not. |project| provides you with a 
:class:`BBDepRotamerLib` organizing rotamers for the different aminoacids 
in equidistant phi/psi bins, as well as a simple :class:`RotamerLib`.
Both libraries are containers for :class:`RotamerLibEntry` and are optimized
for fast writing/loading from/to disk. Once initialized, the library is in a
dynamic mode where rotamers can be added. 
No rotamers can be read at this stage. As soon as
all required rotamers are added, the library can be made static. This is the
mode you can get the rotamers out of it or dump it to disk.


The Non Backbone Dependent Rotamer Library
--------------------------------------------------------------------------------


.. class:: RotamerLib()
  
  Non backbone dependent rotamer library

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: A rotamer library
    :rtype:   :class:`RotamerLib`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

  .. method:: AddRotamer(id, rotamer)

    :param id:          Identity of rotamer to be added
    :param rotamer:     the rotamer to be added

    :type id:           :class:`RotamerID`
    :type rotamer:      :class:`RotamerLibEntry`

    :raises:  :exc:`~exceptions.RuntimeError` if the library is already static

  .. method:: QueryLib(id)

    The query function follows following strategies in case of 
    special *id* requests.

    * if id = HSD (d-protonated HIS) try to find HSD, fallback to HIS in case of failure

    * if id = HSE (e-protonated HIS) try to find HSE, fallback to HIS in case of failure

    * if id = CYH ("free" CYS) try to find CYH, fallback to CYS in case of failure

    * if id = CYD ("disulfid" CYS) try to find CYD, fallback to CYS in case of failure

    * if id = CPR (cis-PRO) try to find CPR, fallback to PRO in case of failure

    * if id = TPR (trans-PRO) try to find TPR, fallback to PRO in case of failure

    :param id:          Identity of rotamer of interest

    :type id:           :class:`RotamerID`

    :returns:           :class:`list` of :class:`RotamerLibEntry` of nonzero
                        probability sorted by their probability

    :raises:  :exc:`~exceptions.RuntimeError` if no entries for *id* can be 
              found

  .. method:: MakeStatic()
    
    Once all rotamers are added, the library can be made static to become readable
    and ready for io. 


The Backbone Dependent Rotamer Library
--------------------------------------------------------------------------------


.. class:: BBDepRotamerLib(phi_bins,psi_bins)
  
  Backbone dependent rotamer library

  :param phi_bins:      number of bins for phi backbone dihedral
  :param psi_bins:      number of bins for psi backbone dihedral

  :type phi_bins:       :class:`int`
  :type psi_bins:       :class:`int`

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: A rotamer library
    :rtype:   :class:`BBDepRotamerLib`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: AddRotamer(id, r1, r2, r3, r4, phi_bin, psi_bin, rotamer)

    :param id:          Identity of rotamer to be added
    :param r1:          Configuration of chi1
    :param r2:          Configuration of chi2
    :param r3:          Configuration of chi3
    :param r4:          Configuration of chi4
    :param phi_bin:     Phi backbone dihedral description
    :param psi_bin:     Psi backbone dihedral description
    :param rotamer:     the rotamer to be added

    :type id:           :class:`RotamerID`
    :type r1:           :class:`int`
    :type r2:           :class:`int`
    :type r3:           :class:`int`
    :type r4:           :class:`int`
    :type phi_bin:      :class:`int`
    :type psi_bin:      :class:`int`
    :type rotamer:      :class:`RotamerLibEntry`

    :raises:  :exc:`~exceptions.RuntimeError` if an invalid backbone angle bin
              is given or when the library is already static.

  .. method:: QueryLib(id, phi, psi)

    The returned rotamers are either directly returned in the form as they are 
    added to the library or can be interpolated.
    In the first option, *phi* and *psi* simply get transformed to the 
    according bin using following formalism: bin = round((angle + pi)/bin_size).
    In case of interpolation, the chi angles of rotameric dihedral angles and the 
    according standard deviations of the rotamers get bilinearly interpolated 
    using the corresponding rotamers with same configuration from the 
    neighbouring bins. No interplation is applied to non-rotameric dihedral
    angles (chi2 in ASP, ASN, HIS, PHE, TRP, TYR; chi3 in GLU, GLN).
    This behaviour can be controlled with :meth:`SetInterpolate`.
    The query function follows following strategies in case of 
    special *id* requests.

    * if id = HSD (d-protonated HIS) try to find HSD, fallback to HIS in case of failure

    * if id = HSE (e-protonated HIS) try to find HSE, fallback to HIS in case of failure

    * if id = CYH ("free" CYS) try to find CYH, fallback to CYS in case of failure

    * if id = CYD ("disulfid" CYS) try to find CYD, fallback to CYS in case of failure

    * if id = CPR (cis-PRO) try to find CPR, fallback to PRO in case of failure

    * if id = TPR (trans-PRO) try to find TPR, fallback to PRO in case of failure

    :param id:          Identity of rotamer of interest
    :param phi:         Phi backbone dihedral angle in range [-pi,pi[
    :param psi:         Psi backbone dihedral angle in range [-pi,pi[

    :type id:           :class:`RotamerID`
    :type phi:          :class:`float`
    :type psi:          :class:`float`

    :returns:           :class:`list` of :class:`RotamerLibEntry` of nonzero
                        probability for given phi/psi pair sorted by their probability

    :raises:  :exc:`~exceptions.RuntimeError` if no entries for *id* can be
              found

  .. method:: MakeStatic()
    
    Once all rotamers are added, the library can be made static to become readable
    and ready for io. Several things get checked during this process

    * For every phi/psi bin combination of a particular :class:`RotamerID`, 
      the same number of rotamers must have been added

    * All configuration combinations of the added rotamers in one phi/psi bin
      of a particular :class:`RotamerID` must be unique

    * The configuration combinations of a particular :class:`RotamerID` must
      be consistent across all phi/psi bins

    :raises:  :exc:`~exceptions.RuntimeError` if one of the points above is
              not fulfilled

  .. method:: SetInterpolate(interpolate)

    :param interpolate: Controls behaviour when :meth:`QueryLib` 
                        gets called

    :type interpolate:  :class:`bool`    



The Library Entry Type   
--------------------------------------------------------------------------------

The entries for the rotamer libraries provide a basic container for rotamer    
dihedral angles and their corresponding standard deviations. They can be
constructed by directly providing the values, but also by some static 
convenience functions. For analytical purposes they provide some comparison 
functionalities.

.. class:: RotamerLibEntry()

.. class:: RotamerLibEntry(p, chi1, chi2, chi3, chi4, sig1, sig2, sig3, sig4)   

  :param p:             Probability of rotamer
  :param chi1:          Chi1 dihedral in range [-pi,pi[
  :param chi2:          Chi2 dihedral in range [-pi,pi[
  :param chi3:          Chi3 dihedral in range [-pi,pi[
  :param chi4:          Chi4 dihedral in range [-pi,pi[
  :param sig1:          Standard deviation for Chi1 dihedral
  :param sig2:          Standard deviation for Chi2 dihedral
  :param sig3:          Standard deviation for Chi3 dihedral
  :param sig4:          Standard deviation for Chi4 dihedral

  :type p:              :class:`float`
  :type chi1:           :class:`float`
  :type chi2:           :class:`float`
  :type chi3:           :class:`float`
  :type chi4:           :class:`float`
  :type sig1:           :class:`float`
  :type sig2:           :class:`float`
  :type sig3:           :class:`float`
  :type sig4:           :class:`float`

  **Attributes:**

  .. attribute:: probability

    probability of rotamer

  .. attribute:: chi1

    Chi1 dihedral in range [-pi,pi[, NaN if not defined

  .. attribute:: chi2

    Chi2 dihedral in range [-pi,pi[, NaN if not defined

  .. attribute:: chi3

    Chi3 dihedral in range [-pi,pi[, NaN if not defined

  .. attribute:: chi4

    Chi4 dihedral in range [-pi,pi[, NaN if not defined

  .. attribute:: sig1

    Standard deviation of Chi1 Dihedral, NaN if not defined

  .. attribute:: sig2

    Standard deviation of Chi2 Dihedral, NaN if not defined

  .. attribute:: sig3

    Standard deviation of Chi3 Dihedral, NaN if not defined

  .. attribute:: sig4

    Standard deviation of Chi4 Dihedral, NaN if not defined


  .. staticmethod:: FromResidue(res)

    Creates a :class:`RotamerLibEntry` from the given *res*.
    The function tries to automatically identify the :class:`RotamerID` based
    on the residue name. The probability and standard deviations are set to 0.0, 
    all not required chi angles with their corresponding  standard deviations to 
    NaN.

    :param res:         Source of dihedral angles

    :type res:          :class:`ost.mol.ResidueHandle`

    :returns:           :class:`RotamerLibEntry`

    :raises:  :exc:`~exceptions.RuntimeError` if residue name cannot be
              translated to :class:`RotamerID` or when not all  required atoms
              are present in *res*.

  .. staticmethod:: FromResidue(res, id)

    Creates a :class:`RotamerLibEntry` from the given *res*.
    The probability gets set to zero and the standard deviations to 0.  
    All not required chi angles with their corresponding standard deviations 
    are NaN.

    :param res:         Source of dihedral angles
    :param id:          The identity of the returned :class:`RotamerLibEntry`

    :type res:          :class:`ost.mol.ResidueHandle`
    :type id:           :class:`RotamerID`

    :returns:           :class:`RotamerLibEntry`

    :raises:  :exc:`~exceptions.RuntimeError` if not all required atoms are
              present in *res*.

  .. method:: IsSimilar(other, thresh)

    Compares two RotamerLibEntries for their similarity in dihedral angles.
    The function goes from chi1 to chi4 and checks one after the other
    for similarity. The function doesn't know the identity of the entries 
    and can therefore not specifically treat symmetric rotamers 
    (TYR,PHE,ASP,GLU) or rotamers with ambiguous naming in general.
    
    :param other:       The Entry you want to compare with
    :param thresh:      The max difference between two dihedrals to be 
                        considered similar

    :type other:        :class:`RotamerLibEntry`
    :type thresh:       :class:`float`

    :returns:           :class:`bool` Whether the two entries have the same
                        amount of defined chi angles (not NaN) and whether
                        they are within the specified threshold.


  .. method:: IsSimilar(other, thresh, id)

    Compares two RotamerLibEntries for their similarity in dihedral angles.
    The function goes from chi1 to chi4 and checks one after the other
    for similarity. Sidechains with ambigous naming that are symmetric 
    (TYR,PHE,ASP,GLU) get treated specifically. E.g. in case of aspartate, 
    the chi2 is checked for its actual value, but also for its flipped state.

    :param other:       The Entry you want to compare with
    :param thresh:      The max difference between two dihedrals to be 
                        considered similar
    :param id:          Identity of the entries to be compared

    :type other:        :class:`RotamerLibEntry`
    :type thresh:       :class:`float`
    :type id:           :class:`RotamerID`

    :returns:           :class:`bool` Whether the two entries have the same
                        amount of defined chi angles (not NaN) and whether
                        they are within the specified threshold.

  .. method:: SimilarDihedral(other, dihedral_idx, thresh)

    Compares a specific dihedral angle. 
    The function doesn't know the identity of the entries and can therefore 
    not specifically treat symmetric rotamers 
    (TYR,PHE,ASP,GLU) or rotamers with ambiguous naming in general.

    :param other:       The Entry you want to compare with
    :param dihedral_idx: Index of the dihedral to be checked
                         (0 for chi1, 3 for chi4)
    :param thresh:      The max difference between two dihedrals to be 
                        considered similar

    :type other:        :class:`RotamerLibEntry`
    :type dihedral_idx: :class:`int`
    :type thresh:       :class:`float`

    :returns:           :class:`bool` Whether both dihedrals are defined 
                        (not NaN) and within the specified threshold


  .. method:: SimilarDihedral(other, dihedral_idx, thresh, id)

    Compares a specific dihedral angle.
    Symmetric sidechains with ambigous naming (TYR,PHE,ASP,GLU) 
    get treated specifically. E.g. in case of aspartate, the chi2 is checked
    for its actual value, but also for its flipped state.

    :param other:       The Entry you want to compare with
    :param dihedral_idx: Index of the dihedral to be checked 
                         (0 for chi1, 3 for chi4)
    :param thresh:      The max difference between two dihedrals to be 
                        considered similar
    :param id:          Identity of the entries to be compared

    :type other:        :class:`RotamerLibEntry`
    :type dihedral_idx: :class:`int`
    :type thresh:       :class:`float`
    :type id:           :class:`RotamerID`

    :returns:           :class:`bool` Whether both dihedrals are defined 
                        (not NaN) and within the specified threshold

Rotamer Configurations  
--------------------------------------------------------------------------------

In rotamers, one distinguishes between rotameric and non-rotameric sidechain
dihedral angles. The rotameric ones are around SP3-SP3 hybridized bonds and
typically have three distinct configurations (trans, gauche-, gauche+).
The non-rotameric ones behave differently. |project| offers some functionality
to estimate those configurations.

.. class:: DihedralConfiguration

  Enumerates the possible sidechain dihedral configurations

  .. hlist::
    :columns: 1

    * TRANS - Trans configuration (120 < angle < -120)
    * GAUCHE_PLUS - Gauche+ configuration (0 < angle < 120)
    * GAUCHE_MINUS - Gauce- configuration (-120 < angle < 0)
    * NON_ROTAMERIC - Dihedral without SP3-SP3 bond
    * INVALID - Invalid configuration, e.g. chi3 of ALA (doesnt exist...)

.. method:: GetRotamericConfiguration(angle)

  Evaluates the *angle* according to the ranges specified for 
  :class:`DihedralConfiguration`. 

  :param angle:         Angle to be evaluated
  :type angle:          :class:`float`

  :returns:             TRANS, GAUCHE_PLUS or GAUCHE_MINUS.
                        INVALID if *angle* is NaN.

.. method:: GetDihedralConfiguration(entry, id, dihedral_idx) 

  Estimates configuration of a sidechain dihedral angle in a specific 
  :class:`RotamerLibEntry` with the knowledge of its identity. This allows
  to also return NON_ROTAMERIC (e.g. chi2 for ASN).

  :param entry:         Sidechain dihedral angle comes from here
  :param id:            Identity of rotamer
  :param dihedral_idx:  Specifies angle (0 => chi1, ..., 3 => chi4)

  :type entry:          :class:`RotamerLibEntry`
  :type id:             :class:`RotamerID`
  :type dihedral_idx:   :class:`int`

  :returns:             Result of :meth:`GetRotamericConfiguration` if specified
                        angle is Rotameric, NON_ROTAMERIC if specified angle is 
                        valid and non rotameric, INVALID otherwise.
                        

