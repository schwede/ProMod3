..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Geometry functions
================================================================================

.. currentmodule:: promod3.core

.. function:: EvaluateGromacsPosRule(rule, number, anchors)

  Constructs *number* positions with the given Gromacs rule and anchor positions
  (see Gromacs manual for details).

  :param rule: Gromacs rule
  :type rule:  :class:`int`
  :param number: Desired number of positions (max. 3)
  :type number:  :class:`int`
  :param anchors: Anchor positions (max. 4)
  :type anchors:  :class:`list` of :class:`~ost.geom.Vec3`
  :return: Constructed *number* positions.
  :rtype:  :class:`list` of :class:`~ost.geom.Vec3`

.. function:: ConstructCTerminalOxygens(c_pos, ca_pos, n_pos)

  Constructs positions of O and OXT atoms for C terminal.

  :param c_pos: Position of C atom
  :type c_pos:  :class:`~ost.geom.Vec3`
  :param n_pos: Position of nitrogen atom
  :type n_pos:  :class:`~ost.geom.Vec3`
  :param ca_pos: Position of C-alpha atom
  :type ca_pos:  :class:`~ost.geom.Vec3`
  :return: Positions of O and OXT atoms.
  :rtype:  :class:`tuple` of :class:`~ost.geom.Vec3`

.. function:: ConstructAtomPos(A, B, C, bond_length, angle, dihedral)

  Constructs position of atom "D" based on bond length (C-D), angle (B-C-D) and
  dihedral (A-B-C-D).

  :param A: Position of atom A
  :type A:  :class:`~ost.geom.Vec3`
  :param B: Position of atom B
  :type B:  :class:`~ost.geom.Vec3`
  :param C: Position of atom C
  :type C:  :class:`~ost.geom.Vec3`
  :param bond_length: Bond length (C-D)
  :type bond_length:  :class:`float`
  :param angle: Angle (B-C-D)
  :type angle:  :class:`float`
  :param dihedral: Dihedral (A-B-C-D)
  :type dihedral:  :class:`float`
  :return: Position of atom D
  :rtype:  :class:`~ost.geom.Vec3`

.. function:: ConstructCBetaPos(n_pos, ca_pos, c_pos)

  Constructs position of C-beta atom given the positions of the backbone nitrogen,
  C-alpha and C atoms.

  :param n_pos: Position of nitrogen atom
  :type n_pos:  :class:`~ost.geom.Vec3`
  :param ca_pos: Position of C-alpha atom
  :type ca_pos:  :class:`~ost.geom.Vec3`
  :param c_pos: Position of C atom
  :type c_pos:  :class:`~ost.geom.Vec3`
  :return: Position of C-beta atom
  :rtype:  :class:`~ost.geom.Vec3`

.. function:: RotationAroundLine(axis, anchor, angle)

  Creates a geometric transform leading to a rotation with specified `angle`
  around a line defined by `axis` and `anchor`.

  :param axis: Axis of rotation
  :type axis:  :class:`~ost.geom.Vec3`
  :param anchor: Anchor for rotation
  :type anchor:  :class:`~ost.geom.Vec3`
  :param angle: Angle (in radians in range [-pi,pi]) of rotation
  :type angle:  :class:`float`
  :return: Transformation matrix
  :rtype:  :class:`~ost.geom.Mat4`

.. function:: RotationAroundLine(axis, angle)

  Creates a 3x3 matrix for a rotation by a specified `angle` around an `axis`
  going through the origin.

  :param axis: Axis of rotation
  :type axis:  :class:`~ost.geom.Vec3`
  :param angle: Angle (in radians in range [-pi,pi]) of rotation
  :type angle:  :class:`float`
  :return: Rotation matrix
  :rtype:  :class:`~ost.geom.Mat3`

.. class:: StemCoords()
           StemCoords(res)

  Simple container class to store N, CA and C coordinates.

  :param res: Residue handle from which to extract N, CA and C coordinates.
  :type res:  :class:`ost.mol.ResidueHandle`

  :raises: :exc:`~exceptions.RuntimeError` if *res* does not contain N, CA and C
           atoms.

  .. attribute:: n_coord
                 ca_coord
                 c_coord

    Position of nitrogen / alpha carbon / carbon atom.
    
    :type:  :class:`~ost.geom.Vec3`

.. class:: StemPairOrientation()
           StemPairOrientation(n_stem, c_stem)

  Relative orientation of a pair of stems. Can be used to define gaps with four
  angles and one distance and is used in the fragment database for fast lookups.

  :param n_stem: Coordinates of stem A.
  :type n_stem:  :class:`StemCoords`
  :param c_stem: Coordinates of stem B.
  :type c_stem:  :class:`StemCoords`

  .. attribute:: distance

    Distance of C atom of stem A to N atom of stem B.

    :type: :class:`float`

  .. attribute:: angle_one
                 angle_two

    Angles defining relative orientation of stem B with respect to stem A.

    :type: :class:`float`

  .. attribute:: angle_three
                 angle_four

    Angles defining relative orientation of stem A with respect to stem B.

    :type: :class:`float`
