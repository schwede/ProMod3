..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:mod:`~promod3.core.pm3argparse` - Parsing Command Lines
================================================================================

.. module:: promod3.core.pm3argparse
  :synopsis: Argument Parsing
  
.. currentmodule:: promod3.core.pm3argparse

Introduction
--------------------------------------------------------------------------------

A lot of the actions in |project| have a bunch of command line parameters/
arguments in common. For example we need an input alignment quite often and
usually for an alignment we need information on what is the target sequence,
what identifies a template sequence and eventually a hint on the format. That
means we need the same functionality on the command line in several actions.
There :class:`~promod3.core.pm3argparse.PM3ArgumentParser` serves as a
simplification. It provides a set of standard arguments you just need to
activate for your action plus it comes with some verification functionality for
input.

.. literalinclude:: ../../../tests/doc/scripts/core_pm3argparse.py

When the example above is run with ``pm`` and no additional arguments, the
script exits with a code 2. If it is run with an additional argument ``-h`` or
``--help``, it exits with a code 0 and displays the help message given as a
docstring in your script.

Argument Parser
--------------------------------------------------------------------------------

.. autoclass:: PM3ArgumentParser
  :members:

  .. automethod:: __init__

.. |descattr| replace:: :attr:`description`
.. |argpinit| replace:: :meth:`argparse.ArgumentParser.__init__`
.. |progattr| replace:: :attr:`prog`
.. |sysargv| replace:: :attr:`sys.argv`

..  LocalWords:  currentmodule argparse ArgumentParser autoclass automethod
..  LocalWords:  init descattr attr argpinit meth progattr prog
