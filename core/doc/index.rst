..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:mod:`~promod3.core` - |project| Core Functionality
================================================================================

.. module:: promod3.core
  :synopsis: Basic functionality, supporting standard tasks in your code.
  
This module gathers functions and classes which are not devoted to homology
modeling per se but cover standard programming issues.

Contents:

.. toctree:: 
  :maxdepth: 2
  
  pm3argparse
  helper
  geometry
  runtime_profiling
  graph_minimizer
