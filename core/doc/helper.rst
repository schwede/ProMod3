..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:mod:`~promod3.core.helper` - Shared Functionality For the Everything
================================================================================

.. module:: promod3.core.helper
  :synopsis: Helper functions

.. currentmodule:: promod3.core.helper

Introduction
--------------------------------------------------------------------------------

We collect functions here, which should be useful in many places but would make
rather empty modules left alone.


Messages
--------------------------------------------------------------------------------

.. literalinclude:: ../../../tests/doc/scripts/core_msg_error.py

.. autofunction:: MsgErrorAndExit

File Tests
--------------------------------------------------------------------------------

The following example parses an argument (call as ``pm SCRIPTNAME FILENAME``) as
a file name and checks whether it is a ``pdb`` or ``mmcif`` file.

.. literalinclude:: ../../../tests/doc/scripts/core_file_checks.py

.. autofunction:: FileExists

.. autofunction:: FileExtension

.. autofunction:: FileGzip
