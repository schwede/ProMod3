..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:func:`~promod3.SetCompoundsChemlib`
================================================================================

.. currentmodule:: promod3

This is the one function defined on the highest level of |project|'s module
hierarchy. It is used to load an |ost_s| compound library to avoid running
|python| code via the ancient |ost_s| wrapper. Applying this function at
top-level, we can set a decent default by |cmake| plus the library is
immediately available just by invoking **any** |project| module. You do not
need to call this function, only if you want to load a different chemical
components dictionary.

.. autofunction:: SetCompoundsChemlib

..  LocalWords:  func promod SetCompoundsChemlib currentmodule ost cmake
..  LocalWords:  autofunction
