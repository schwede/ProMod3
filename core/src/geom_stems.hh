// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_GEOM_STEMS_HH
#define PROMOD3_GEOM_STEMS_HH

#include <ost/base.hh>
#include <ost/geom/vecmat3_op.hh>
#include <ost/geom/mat4.hh>
#include <ost/mol/residue_handle.hh>

namespace promod3 { namespace core {

// simple container for N/CA/C positions
struct StemCoords {
  // DATA
  geom::Vec3 n_coord;
  geom::Vec3 ca_coord;
  geom::Vec3 c_coord;

  // CONSTRUCT
  StemCoords() { }
  StemCoords(const ost::mol::ResidueHandle& res);
};

// relative orientation of a pair of stems
// -> can be used to define gaps with 4 angles and 1 distance...
struct StemPairOrientation {
  // DATA
  Real distance;                // distance of C of stem A to N of stem B
  Real angle_one, angle_two;    // angles defining rel. orientation of
                                // stem B w.r.t. stem A
  Real angle_three, angle_four; // angles defining rel. orientation of
                                // stem A w.r.t. stem B

  // CONSTRUCT
  StemPairOrientation() { }
  StemPairOrientation(const StemCoords& n_stem, const StemCoords& c_stem);
  StemPairOrientation(const geom::Vec3& n_stem_n, const geom::Vec3& n_stem_ca,
                      const geom::Vec3& n_stem_c, const geom::Vec3& c_stem_n,
                      const geom::Vec3& c_stem_ca, const geom::Vec3& c_stem_c);

private:
  void Init(const geom::Vec3& n_stem_n, const geom::Vec3& n_stem_ca,
            const geom::Vec3& n_stem_c, const geom::Vec3& c_stem_n,
            const geom::Vec3& c_stem_ca, const geom::Vec3& c_stem_c);
};

}} // ns

#endif
