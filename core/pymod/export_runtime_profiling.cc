// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/shared_ptr.hpp>

#include <promod3/core/runtime_profiling.hh>

using namespace promod3::core;
using namespace boost::python;

namespace {
bool IsEnabled() { return (PM3_RUNTIME_PROFILING_LEVEL > 0); }
}

void export_runtime_profiling() {
  
  class_<ScopedTimer>("ScopedTimer", no_init);
  register_ptr_to_python<ScopedTimerPtr>();

  class_<StaticRuntimeProfiler>("StaticRuntimeProfiler", no_init)
    .def("Start", &StaticRuntimeProfiler::Start, (arg("id"), arg("level")=1))
    .staticmethod("Start")
    .def("StartScoped", &StaticRuntimeProfiler::StartScoped,
         (arg("id"), arg("level")=1))
    .staticmethod("StartScoped")
    .def("Stop", &StaticRuntimeProfiler::Stop, (arg("level")=1))
    .staticmethod("Stop")
    .def("PrintSummary", &StaticRuntimeProfiler::PrintSummary,
         (arg("max_to_show")=-1))
    .staticmethod("PrintSummary")
    .def("IsEnabled", &IsEnabled)
    .staticmethod("IsEnabled")
    .def("Clear", &StaticRuntimeProfiler::Clear)
    .staticmethod("Clear")
  ;

}
