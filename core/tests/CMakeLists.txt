set(CORE_UNIT_TESTS
  test_setcompoundschemlib.py
  test_helper.py
  test_pm3argparse.py
  test_check_io.cc
  test_portable_binary.cc
  test_graph_minimizer.cc
  tests.cc
)

set(CORE_TEST_DATA
  test_helper.py
  data/broken_on_purpose.chemlib
  data/aln_tpl/1crn.fasta
  data/aln_tpl/1crn_cut.pdb
  data/aln_tpl/2aoh-1_cut.pdb
  data/aln_tpl/2aoh-1_cut_A.fasta
  data/aln_tpl/2aoh-1_cut_B.fasta
  data/aln_tpl/2jlp-1.aln
  data/aln_tpl/2jlp-1.fasta
  data/aln_tpl/2jlp-1.json
  data/aln_tpl/2jlp-1.pdb
  data/aln_tpl/5d52-1_cut.pdb
  data/aln_tpl/5d52-1_cut_A.fasta
  data/aln_tpl/5d52-1_cut_B.fasta
  data/aln_tpl/1crn.hhm
  data/aln_tpl/1crn_corrupted.hhm
  data/aln_tpl/5ua4_B.fasta
  data/aln_tpl/5ua4_B.hhm
  data/fasta/alignment.fas
  data/fasta/1ake.fas
  data/fasta/1ake_1.fas
  data/fasta/1ake_2trg.fas
  data/fasta/1ake_3.fas
  data/fasta/1ake.fas.gz
  data/fasta/1ake_nel.fas
  data/fasta/1ake_sw.fas

)

promod3_unittest(MODULE core
                 SOURCES "${CORE_UNIT_TESTS}"
                 DATA "${CORE_TEST_DATA}")
