# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


'''Convert files from portable to raw.'''
import sys
import os
from promod3 import loop

if len(sys.argv) != 4:
	print("usage: python convert_binaries.py PORTABLE_FILE_IN RAW_FILE_OUT CLASS")
	sys.exit(1)

portable_file_in = sys.argv[1]
raw_file_out = sys.argv[2]
class_name = sys.argv[3]

if class_name == "TorsionSampler":
	my_data = loop.TorsionSampler.LoadPortable(portable_file_in, 0)
elif class_name == "FragDB":
	my_data = loop.FragDB.LoadPortable(portable_file_in)
elif class_name == "StructureDB":
	my_data = loop.StructureDB.LoadPortable(portable_file_in)
elif class_name == "ForcefieldLookup":
  my_data = loop.ForcefieldLookup.LoadPortable(portable_file_in)
else:
	raise RuntimeError("Cannot convert class " + class_name)

my_data.Save(raw_file_out)
