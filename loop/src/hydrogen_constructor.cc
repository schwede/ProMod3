// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/hydrogen_constructor.hh>

namespace promod3 { namespace loop {

namespace {

void AddHydrogensToRes(ost::mol::ResidueHandle& res,
                       ost::mol::XCSEditor& edi,
                       const HydrogenStorage& hydrogens,
                       const ost::conop::AminoAcid aa) {
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  // check for all set hydrogens
  for (uint i = 0; i < aa_lookup.GetNumHydrogens(aa); ++i) {
    if (hydrogens.IsSet(i)) {
      const AminoAcidHydrogen aah = aa_lookup.GetAAH(aa, i);
      const String& aname = aa_lookup.GetAtomName(aah);
      // check if already there, otherwise add
      ost::mol::AtomHandle atom = res.FindAtom(aname);
      if (!atom.IsValid()) {
        // add new atom
        atom = edi.InsertAtom(res, aname, hydrogens.GetPos(i), "H");
      } else {
        // update and wipe connectivity (just to be safe)
        edi.SetAtomPos(atom, hydrogens.GetPos(i));
        edi.DeleteBonds(atom.GetBondList());
      }
      // reconnect to anchor (must exist if hydrogen was constructed)
      const uint anchor_idx = aa_lookup.GetAnchorAtomIndex(aah);
      const AminoAcidAtom anchor_aaa = aa_lookup.GetAAA(aa, anchor_idx);
      edi.Connect(atom, res.FindAtom(aa_lookup.GetAtomName(anchor_aaa)), 1);
    }
  }
}

} // anon ns

ost::mol::EntityHandle
ConstructProtonatedEntity(const ost::mol::EntityHandle& ent, bool only_polar,
                          HisProtonationState state) {
  // we work on a copy of the entity
  ost::mol::EntityHandle result = ent.Copy();
  ost::mol::XCSEditor edi = result.EditXCS(ost::mol::BUFFERED_EDIT);
  // we get a dummy AllAtomPositions with all 20 AA to work on
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  String sequence;
  for (uint aa_idx = 0; aa_idx < ost::conop::XXX; ++aa_idx) {
    sequence += aa_lookup.GetOLC(ost::conop::AminoAcid(aa_idx));
  }
  AllAtomPositions buffer(sequence);  // res_idx == AA-enum
  // get storage for hydrogen positions
  HydrogenStorage hydrogens;

  // go through all residues
  ost::mol::ResidueHandleList res_list = result.GetResidueList();
  for (ost::mol::ResidueHandleList::iterator i_res = res_list.begin();
       i_res != res_list.end(); ++i_res) {
    // check amino acid
    const char olc = i_res->GetOneLetterCode();
    const ost::conop::AminoAcid aa = ost::conop::OneLetterCodeToAminoAcid(olc);
    if (aa == ost::conop::XXX) {
      std::stringstream ss;
      ss << "Invalid one letter code '" << olc << "' encountered for "
         << "residue " << i_res->GetQualifiedName() << "! Can only handle "
         << "standard amino acids!";
      throw promod3::Error(ss.str());
    }

    // extract positions, get hydrogens and add into entity
    buffer.SetResidue(uint(aa), *i_res);
    ConstructHydrogens(buffer, uint(aa), hydrogens, only_polar, state);
    
    // add H attached to N (it's polar anyways)
    // -> need also to deal with termini here
    ost::mol::ResidueHandle prev_res = i_res->GetPrev();
    if (prev_res) {
      ost::mol::AtomHandle prev_c = prev_res.FindAtom("C");
      if (prev_c && ost::mol::BondExists(prev_c, i_res->FindAtom("N"))) {
        ConstructHydrogenN(buffer, uint(aa), prev_c.GetPos(), hydrogens);
      } else {
        // consider it a terminal residue
        ConstructHydrogenNTerminal(buffer, uint(aa), hydrogens);
      }
    } else {
      // consider it a terminal residue
      ConstructHydrogenNTerminal(buffer, uint(aa), hydrogens);
    }

    // update res
    AddHydrogensToRes(*i_res, edi, hydrogens, aa);

    // NOTE: C-terminal OXT not done here
    // -> if needed use core::ConstructCTerminalOxygens
  }

  return result;
}

ost::mol::EntityHandle
ConstructProtonatedEntity(const AllAtomPositions& all_pos, bool only_polar,
                          HisProtonationState state) {
  // get non-protonated entity to start with
  ost::mol::EntityHandle result = all_pos.ToEntity();
  ost::mol::XCSEditor edi = result.EditXCS(ost::mol::BUFFERED_EDIT);
  // get storage for hydrogen positions
  HydrogenStorage hydrogens;

  // go through all residues
  ost::mol::ResidueHandleList res_list = result.GetResidueList();
  for (uint res_idx = 0; res_idx < all_pos.GetNumResidues(); ++res_idx) {
    // extract positions, get hydrogens and add into entity
    ConstructHydrogens(all_pos, res_idx, hydrogens, only_polar, state);
    
    // add H attached to N (it's polar anyways)
    // -> need also to deal with termini here
    if (res_idx > 0) {
      // we only consider first idx terminal here, gaps get helical HN
      if (all_pos.IsSet(res_idx-1, BB_C_INDEX)) {
        const geom::Vec3& prev_c_pos = all_pos.GetPos(res_idx-1, BB_C_INDEX);
        ConstructHydrogenN(all_pos, res_idx, prev_c_pos, hydrogens);
      } else {
        ConstructHydrogenN(all_pos, res_idx, -1.0472, hydrogens);
      }
    } else {
      // consider it a terminal residue
      ConstructHydrogenNTerminal(all_pos, res_idx, hydrogens);
    }

    // update res
    AddHydrogensToRes(res_list[res_idx], edi, hydrogens, all_pos.GetAA(res_idx));

    // NOTE: C-terminal OXT not done here
    // -> if needed use core::ConstructCTerminalOxygens
  }

  return result;
}

uint ConstructHydrogens(const AllAtomPositions& all_pos, uint res_idx,
                        HydrogenStorage& hydrogens, bool only_polar,
                        HisProtonationState state) {
  // setup
  uint num_added = 0;
  hydrogens.Clear();
  // need max 4 anchors, get max. 3 pos.
  geom::Vec3 anchors[4];
  geom::Vec3 positions[3];
  // get rules
  const ost::conop::AminoAcid aa = all_pos.GetAA(res_idx);
  const HydrogenRuleLookup& hy_lookup = HydrogenRuleLookup::GetInstance();
  const std::vector<HydrogenRule>& h_rules = hy_lookup.GetRules(aa);
  for (uint i = 0; i < h_rules.size(); ++i) {
    const HydrogenRule& h_rule = h_rules[i];
    // some rules are to be skipped
    if (!h_rule.IsToSkip(only_polar, aa, state)) {
      // check/get anchors -> all_set = false if any anchor unset
      bool all_set = true;
      for (uint j = 0; j < h_rule.num_anchors; ++j) {
        if (all_pos.IsSet(res_idx, h_rule.anchor_idx[j])) {
          anchors[j] = all_pos.GetPos(res_idx, h_rule.anchor_idx[j]);
        } else {
          all_set = false;
          break;
        }
      }
      // get hydrogens if we can
      if (all_set) {
        core::EvaluateGromacsPosRule(h_rule.rule, h_rule.num_hydrogens,
                                     anchors, positions);
        for (uint j = 0; j < h_rule.num_hydrogens; ++j) {
          hydrogens.SetPos(h_rule.hydro_idx[j], positions[j]);
        }
        num_added += h_rule.num_hydrogens;
      }
    }
  }
  return num_added;
}

uint ConstructHydrogenN(const AllAtomPositions& all_pos, uint res_idx,
                        const geom::Vec3& prev_c_pos,
                        HydrogenStorage& hydrogens) {
  // check PRO
  const ost::conop::AminoAcid aa = all_pos.GetAA(res_idx);
  if (aa == ost::conop::PRO) return 0;
  // check if positions set
  if (   !all_pos.IsSet(res_idx, BB_N_INDEX)
      || !all_pos.IsSet(res_idx, BB_CA_INDEX)) {
    return 0;
  }
  // get H pos
  geom::Vec3 anchors[] = {all_pos.GetPos(res_idx, BB_N_INDEX),
                          prev_c_pos,
                          all_pos.GetPos(res_idx, BB_CA_INDEX)};
  geom::Vec3 h_pos;
  core::EvaluateGromacsPosRule(1, 1, anchors, &h_pos);
  // fill hydrogens
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  hydrogens.SetPos(aa_lookup.GetHNIndex(aa), h_pos);
  return 1;
}

uint ConstructHydrogenN(const AllAtomPositions& all_pos, uint res_idx,
                        Real phi, HydrogenStorage& hydrogens) {
  // check PRO
  const ost::conop::AminoAcid aa = all_pos.GetAA(res_idx);
  if (aa == ost::conop::PRO) return 0;
  // check if positions set
  if (   !all_pos.IsSet(res_idx, BB_C_INDEX)
      || !all_pos.IsSet(res_idx, BB_CA_INDEX)
      || !all_pos.IsSet(res_idx, BB_N_INDEX)) {
    return 0;
  }
  // get H pos
  geom::Vec3 h_pos;
  core::ConstructAtomPos(all_pos.GetPos(res_idx, BB_C_INDEX),
                         all_pos.GetPos(res_idx, BB_CA_INDEX),
                         all_pos.GetPos(res_idx, BB_N_INDEX),
                         0.9970, 2.0420, phi+M_PI, h_pos);
  // fill hydrogens
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  hydrogens.SetPos(aa_lookup.GetHNIndex(aa), h_pos);
  return 1;
}

uint ConstructHydrogenNTerminal(const AllAtomPositions& all_pos, uint res_idx,
                                HydrogenStorage& hydrogens) {
  // check if positions set
  if (   !all_pos.IsSet(res_idx, BB_C_INDEX)
      || !all_pos.IsSet(res_idx, BB_CA_INDEX)
      || !all_pos.IsSet(res_idx, BB_N_INDEX)) {
    return 0;
  }
  // setup
  const ost::conop::AminoAcid aa = all_pos.GetAA(res_idx);
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  // get H pos and fill hydrogens
  geom::Vec3 anchors[] = {all_pos.GetPos(res_idx, BB_N_INDEX),
                          all_pos.GetPos(res_idx, BB_CA_INDEX),
                          all_pos.GetPos(res_idx, BB_C_INDEX)};
  geom::Vec3 positions[3];
  // need to distinguish PRO
  const uint num = (aa == ost::conop::PRO) ? 2 : 3;
  core::EvaluateGromacsPosRule(4, num, anchors, positions);
  hydrogens.SetPos(aa_lookup.GetH1Index(aa), positions[0]);
  hydrogens.SetPos(aa_lookup.GetH2Index(aa), positions[1]);
  if (num == 3) hydrogens.SetPos(aa_lookup.GetH3Index(aa), positions[2]);
  return num;
}

}} //ns
