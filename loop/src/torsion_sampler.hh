// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_TORSION_SAMPLER_HH
#define PROMOD_LOOP_TORSION_SAMPLER_HH

#include <vector>
#include <map>
#include <fstream>

#include <boost/random/discrete_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/shared_ptr.hpp>

#include <ost/mol/mol.hh>
#include <ost/conop/amino_acids.hh>
#include <ost/string_ref.hh>

#include <promod3/core/message.hh>


namespace promod3 { namespace loop {

class TorsionSampler;
typedef boost::shared_ptr<TorsionSampler> TorsionSamplerPtr;
typedef std::vector<TorsionSamplerPtr> TorsionSamplerList;

class TorsionSampler {

public:
  TorsionSampler(const std::vector<String>& group_definitions, 
                 int bins_per_dimension, uint seed);

  static TorsionSamplerPtr Load(const String& filename, uint seed);

  void Save(const String& filename);

  static TorsionSamplerPtr LoadPortable(const String& filename, uint seed);

  void SavePortable(const String& filename);

  bool operator==(const TorsionSampler& rhs) const;
  bool operator!=(const TorsionSampler& rhs) const {
    return !this->operator==(rhs);
  }

  void ExtractStatistics(ost::mol::EntityView& view);

  std::pair<Real, Real> Draw(ost::conop::AminoAcid before, 
                             ost::conop::AminoAcid central, 
                             ost::conop::AminoAcid after);

  std::pair<Real, Real> Draw(uint histogram_index);

  Real DrawPhiGivenPsi(ost::conop::AminoAcid before, 
                       ost::conop::AminoAcid central, 
                       ost::conop::AminoAcid after, Real psi);

  Real DrawPhiGivenPsi(uint histogram_index, Real psi);

  Real DrawPsiGivenPhi(ost::conop::AminoAcid before, 
                       ost::conop::AminoAcid central, 
                       ost::conop::AminoAcid after, Real phi);

  Real DrawPsiGivenPhi(uint histogram_index, Real phi);

  Real GetProbability(ost::conop::AminoAcid before, 
                      ost::conop::AminoAcid central, 
                      ost::conop::AminoAcid after, 
                      const std::pair<Real,Real>& dihedrals);

  Real GetProbability(uint histogram_index, const std::pair<Real,Real>& dihedrals);

  //following function is not intended to be exported to python because the
  //input parameters are not checked
  inline Real GetProbability(uint histogram_index, int phi_bin, int psi_bin){
    return probabilities_[histogram_index][bins_per_dimension_ * psi_bin + phi_bin];
  }

  inline int GetAngleBin(Real angle) {
    while (angle >= Real(M_PI)) angle -= 2*Real(M_PI);
    while (angle < -Real(M_PI)) angle += 2*Real(M_PI);

    return static_cast<int>(std::floor((angle + Real(M_PI)) / bin_size_));
  }

  Real GetPhiProbabilityGivenPsi(ost::conop::AminoAcid before, 
                                 ost::conop::AminoAcid central, 
                                 ost::conop::AminoAcid after, 
                                 Real phi, Real psi);

  Real GetPhiProbabilityGivenPsi(uint histogram_index, Real phi, Real psi);

  Real GetPsiProbabilityGivenPhi(ost::conop::AminoAcid before, 
                                 ost::conop::AminoAcid central, 
                                 ost::conop::AminoAcid after, 
                                 Real psi, Real phi);

  Real GetPsiProbabilityGivenPhi(uint histogram_index, Real psi, Real phi);

  uint GetHistogramIndex(ost::conop::AminoAcid before, 
                         ost::conop::AminoAcid central, 
                         ost::conop::AminoAcid after) const;

  std::vector<uint> GetHistogramIndices(const String& sequence);

  int GetBinsPerDimension() { return bins_per_dimension_; }

  Real GetBinSize() { return bin_size_; }

  void UpdateDistributions();

private:

  TorsionSampler(int bins_per_dimension_, uint seed);

  void UpdateConditionalDistributions();

  boost::mt19937 gen_;
  int bins_per_dimension_;
  Real bin_size_;
  bool conditional_probabilities_;
  uint index_mapper_[8000];
  std::vector<std::vector<int> > histogram_;
  std::vector<boost::random::discrete_distribution<int,Real> > distributions_;
  std::vector<std::vector<Real> > probabilities_;
  //to save some memory, the conditional probabilities are not stored.
  //we store the integrals over psi/phi instead. The final probability
  //can then efficiently be calculated by the full probability and integral as normalization
  std::vector<std::vector<Real> > summed_phi_probabilities_;
  std::vector<std::vector<Real> > summed_psi_probabilities_;
  std::vector<std::vector<boost::random::discrete_distribution<int,Real> > > distributions_cond_phi_;
  std::vector<std::vector<boost::random::discrete_distribution<int,Real> > > distributions_cond_psi_;
};

}}


#endif

