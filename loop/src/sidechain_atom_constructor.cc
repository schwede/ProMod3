// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/sidechain_atom_constructor.hh>
#include <promod3/core/geom_base.hh>

namespace{

Real EvalPRORingClosingEnergy(const geom::Vec3& n_pos,
                              const geom::Vec3& ca_pos,
                              const geom::Vec3& cg_pos,
                              const geom::Vec3& cd_pos) {
  // parameters are extracted from the CHARMM27 forcefield available through 
  //  GROMACS
  Real e = 0.0;
  // eval bond between CD and N
  e += Real(267776.0) * 
  std::pow(geom::Distance(n_pos,cd_pos) * 0.1 - Real(0.14550), 2);
  // eval angle between cd n ca
  e += Real(836.8) * 
  std::pow(geom::Angle(cd_pos-n_pos, ca_pos-n_pos)-Real(1.9932), 2);
  // eval angle between cg cd n
  e += Real(585.8) * 
  std::pow(geom::Angle(cg_pos-cd_pos,n_pos-cd_pos)-Real(1.9286), 2);
  return e;
}


Real ProlineChi2Fallback(const promod3::loop::AllAtomPositions& all_pos, 
                         uint res_idx,
                         const promod3::loop::SidechainAtomRule& rule){

  Real min_e = std::numeric_limits<Real>::max();
  Real optimal_chi2 = 0.0;
  Real actual_chi2 = -M_PI;
  Real step_size = Real(M_PI)/200;

  geom::Vec3 n_pos = all_pos.GetPos(res_idx, promod3::loop::BB_N_INDEX);
  geom::Vec3 ca_pos = all_pos.GetPos(res_idx, rule.anchor_idx[0]);
  geom::Vec3 cb_pos = all_pos.GetPos(res_idx, rule.anchor_idx[1]);
  geom::Vec3 cg_pos = all_pos.GetPos(res_idx, rule.anchor_idx[2]);
  geom::Vec3 cd_pos;

  for(uint i = 0; i < 400; ++i){
    promod3::core::ConstructAtomPos(ca_pos, cb_pos, cg_pos,
                                    rule.bond_length, rule.angle, 
                                    actual_chi2, cd_pos);
    Real e = EvalPRORingClosingEnergy(n_pos,ca_pos,cg_pos,cd_pos);
    if(e < min_e){
      min_e = e;
      optimal_chi2 = actual_chi2;
    }
    actual_chi2 += step_size;
  }
  return optimal_chi2;
}

} // anon ns




namespace promod3{ namespace loop{

void ConstructSidechainAtoms(AllAtomPositions& all_pos, uint res_idx,
                             Real chi1, Real chi2, Real chi3, Real chi4){

  //do initial checks
  if(res_idx >= all_pos.GetNumResidues()){
    throw promod3::Error("Invalid res_idx in ConstructSidechainAtoms!");
  }

  ost::conop::AminoAcid aa = all_pos.GetAA(res_idx);

  // We make sure that ALL backbone positions are present of ALL heavy atoms
  // This way we can guarantee that all heavy atoms of the FULL residue are
  // set when this function ran through.
  if(!(all_pos.IsSet(res_idx, BB_N_INDEX) &&
       all_pos.IsSet(res_idx, BB_CA_INDEX) &&
       all_pos.IsSet(res_idx, BB_C_INDEX) &&
       all_pos.IsSet(res_idx, BB_O_INDEX)) ||
     (aa != ost::conop::GLY && !all_pos.IsSet(res_idx, BB_CB_INDEX))) {
    throw promod3::Error("All backbone heavy atoms must be set in "
                         "ConstructSidechainAtoms!");
  }

  const std::vector<SidechainAtomRule>& rules = 
  SidechainAtomRuleLookup::GetInstance().GetRules(aa);

  if(rules.empty()) {
    // no sidechain atoms to construct! (e.g. ALA or GLY)
    return;
  }

  Real chi_angles[5];
  chi_angles[0] = chi1;
  chi_angles[1] = chi2;
  chi_angles[2] = chi3;
  chi_angles[3] = chi4;
  chi_angles[4] = 0.0;

  for(uint rule_idx = 0; rule_idx < rules.size(); ++rule_idx){

    const SidechainAtomRule& rule = rules[rule_idx];

    Real dihedral_angle = chi_angles[rule.dihedral_idx];

    if(dihedral_angle != dihedral_angle){
      if(aa == ost::conop::PRO && rule.dihedral_idx == 1){
        dihedral_angle = ProlineChi2Fallback(all_pos, res_idx, rule);
      }
      else{
        throw promod3::Error("Not all required dihedrals provided in "
                             "ConstructSidechainAtoms!");
      }
    }

    dihedral_angle += rule.base_dihedral;

    geom::Vec3 constructed_pos;
    promod3::core::ConstructAtomPos(all_pos.GetPos(res_idx, rule.anchor_idx[0]), 
                                    all_pos.GetPos(res_idx, rule.anchor_idx[1]), 
                                    all_pos.GetPos(res_idx, rule.anchor_idx[2]), 
                                    rule.bond_length, rule.angle, 
                                    dihedral_angle, constructed_pos);
    all_pos.SetPos(res_idx, rule.sidechain_atom_idx, constructed_pos);
  }
}


Real GetChiAngle(const AllAtomPositions& all_pos, uint res_idx, uint chi_idx){

  if(res_idx >= all_pos.GetNumResidues()){
    throw promod3::Error("Invalid res_idx in GetChiAngles!");
  }

  ChiDefinition chi_definition =
  SidechainAtomRuleLookup::GetInstance().GetChiDefinition(all_pos.GetAA(res_idx), chi_idx);

  if(!(all_pos.IsSet(res_idx, chi_definition.idx_one) &&
       all_pos.IsSet(res_idx, chi_definition.idx_two) &&
       all_pos.IsSet(res_idx, chi_definition.idx_three) &&
       all_pos.IsSet(res_idx, chi_definition.idx_four)))


  {
    throw promod3::Error("Request to get sidechain dihedral angle but not all" 
                         "required atoms are set in AllAtomPosition object!");
  }

  return geom::DihedralAngle(all_pos.GetPos(res_idx, chi_definition.idx_one),
                             all_pos.GetPos(res_idx, chi_definition.idx_two),
                             all_pos.GetPos(res_idx, chi_definition.idx_three),
                             all_pos.GetPos(res_idx, chi_definition.idx_four));
}

Real GetChi1Angle(const AllAtomPositions& all_pos, uint res_idx){
  return GetChiAngle(all_pos, res_idx, 0);
}

Real GetChi2Angle(const AllAtomPositions& all_pos, uint res_idx){
  return GetChiAngle(all_pos, res_idx, 1);
}

Real GetChi3Angle(const AllAtomPositions& all_pos, uint res_idx){
  return GetChiAngle(all_pos, res_idx, 2);
}

Real GetChi4Angle(const AllAtomPositions& all_pos, uint res_idx){
  return GetChiAngle(all_pos, res_idx, 3);
}

}} // ns
