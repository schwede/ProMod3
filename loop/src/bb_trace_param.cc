// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/bb_trace_param.hh>
#include <promod3/core/message.hh>

namespace promod3{ namespace loop{

void BBTraceParam(char olc, Real& n_ca_bond, Real& ca_c_bond,
                  Real& c_n_bond, Real& c_n_ca_angle,
                  Real& n_ca_c_angle, Real& ca_c_n_angle){
  switch(olc){
    case 'A':{
      n_ca_bond = 1.4618;
      ca_c_bond = 1.5255;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9374;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'R':{
      n_ca_bond = 1.4614;
      ca_c_bond = 1.5253;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9359;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'N':{
      n_ca_bond = 1.4614;
      ca_c_bond = 1.5256;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9434;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'D':{
      n_ca_bond = 1.4624;
      ca_c_bond = 1.5268;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9364;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'Q':{
      n_ca_bond = 1.4614;
      ca_c_bond = 1.5254;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9375;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'E':{
      n_ca_bond = 1.4615;
      ca_c_bond = 1.5258;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9389;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'K':{
      n_ca_bond = 1.4616;
      ca_c_bond = 1.5255;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9374;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'S':{
      n_ca_bond = 1.4610;
      ca_c_bond = 1.5251;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9401;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'C':{
      n_ca_bond = 1.4605;
      ca_c_bond = 1.5242;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9338;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'M':{
      n_ca_bond = 1.4618;
      ca_c_bond = 1.5249;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9350;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'W':{
      n_ca_bond = 1.4611;
      ca_c_bond = 1.5243;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9348;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'Y':{
      n_ca_bond = 1.4609;
      ca_c_bond = 1.5242;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9341;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'T':{
      n_ca_bond = 1.4606;
      ca_c_bond = 1.5254;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9314;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'V':{
      n_ca_bond = 1.4614;
      ca_c_bond = 1.5258;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9140;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'I':{
      n_ca_bond = 1.4616;
      ca_c_bond = 1.5259;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9139;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'L':{
      n_ca_bond = 1.4612;
      ca_c_bond = 1.5248;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9339;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'G':{
      n_ca_bond = 1.4556;
      ca_c_bond = 1.5164;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9747;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'P':{
      n_ca_bond = 1.4667;
      ca_c_bond = 1.5255;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9665;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'H':{
      n_ca_bond = 1.4612;
      ca_c_bond = 1.5242;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9374;
      ca_c_n_angle = 2.0380;
      break;
    }
    case 'F':{
      n_ca_bond = 1.4608;
      ca_c_bond = 1.5243;
      c_n_bond = 1.3310;
      c_n_ca_angle = 2.1200;
      n_ca_c_angle = 1.9323;
      ca_c_n_angle = 2.0380;
      break;
    }
    default:{
      throw promod3::Error("Invalid OneLetterCode observed!");
    }
  }
}

}}
