// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/loop_object_loader.hh>
#include <promod3/config.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3{ namespace loop{


TorsionSamplerPtr LoadTorsionSampler(uint seed) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "loop::LoadTorsionSampler", 2);
  String path = GetProMod3BinaryPath("loop_data", "torsion_sampler.dat");
  TorsionSamplerPtr p = TorsionSampler::Load(path,seed);
  return p;
}

TorsionSamplerPtr LoadTorsionSamplerHelical(uint seed) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "loop::LoadTorsionSamplerHelical", 2);
  String path = GetProMod3BinaryPath("loop_data", "torsion_sampler_helical.dat");
  TorsionSamplerPtr p = TorsionSampler::Load(path,seed);
  return p;
}

TorsionSamplerPtr LoadTorsionSamplerExtended(uint seed) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "loop::LoadTorsionSamplerExtended", 2);
  String path = GetProMod3BinaryPath("loop_data", "torsion_sampler_extended.dat");
  TorsionSamplerPtr p = TorsionSampler::Load(path,seed);
  return p;
}

TorsionSamplerPtr LoadTorsionSamplerCoil(uint seed) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "loop::LoadTorsionSamplerCoil", 2);
  String path = GetProMod3BinaryPath("loop_data", "torsion_sampler_coil.dat");
  TorsionSamplerPtr p = TorsionSampler::Load(path,seed);
  return p;
}

FragDBPtr LoadFragDB() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "loop::LoadFragDB", 2);
  String path = GetProMod3BinaryPath("loop_data", "frag_db.dat");
  FragDBPtr p = FragDB::Load(path);
  return p; 
}

StructureDBPtr LoadStructureDB() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "loop::LoadStructureDB", 2);
  String path = GetProMod3BinaryPath("loop_data", "structure_db.dat");
  StructureDBPtr p = StructureDB::Load(path);
  return p; 
}

}} //ns

