// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/hydrogen_rule_lookup.hh>

namespace promod3 { namespace loop {

void HydrogenRuleLookup::AddRule_(ost::conop::AminoAcid aa, uint r, bool p,
                                  HisProtonationState s) {
  HydrogenRule rule;
  rule.rule = r;
  rule.is_polar = p;
  rule.his_prot_state = s;
  rules_[aa].push_back(rule);
}

void HydrogenRuleLookup::SetAnchors_(ost::conop::AminoAcid aa, uint anch1,
                                     uint anch2, uint anch3) {
  rules_[aa].back().anchor_idx[0] = anch1;
  rules_[aa].back().anchor_idx[1] = anch2;
  rules_[aa].back().anchor_idx[2] = anch3;
  rules_[aa].back().num_anchors = 3;
}

void HydrogenRuleLookup::SetAnchors_(ost::conop::AminoAcid aa, uint anch1,
                                     uint anch2, uint anch3, uint anch4) {
  rules_[aa].back().anchor_idx[0] = anch1;
  rules_[aa].back().anchor_idx[1] = anch2;
  rules_[aa].back().anchor_idx[2] = anch3;
  rules_[aa].back().anchor_idx[3] = anch4;
  rules_[aa].back().num_anchors = 4;
}

void HydrogenRuleLookup::SetHydrogens_(ost::conop::AminoAcid aa, uint idx) {
  rules_[aa].back().hydro_idx[0] = idx;
  rules_[aa].back().num_hydrogens = 1;
}
void HydrogenRuleLookup::SetHydrogens_(ost::conop::AminoAcid aa, uint idx1,
                                       uint idx2) {
  rules_[aa].back().hydro_idx[0] = idx1;
  rules_[aa].back().hydro_idx[1] = idx2;
  rules_[aa].back().num_hydrogens = 2;
}
void HydrogenRuleLookup::SetHydrogens_(ost::conop::AminoAcid aa, uint idx1,
                                       uint idx2, uint idx3) {
  rules_[aa].back().hydro_idx[0] = idx1;
  rules_[aa].back().hydro_idx[1] = idx2;
  rules_[aa].back().hydro_idx[2] = idx3;
  rules_[aa].back().num_hydrogens = 3;
}

HydrogenRuleLookup::HydrogenRuleLookup() {
  using namespace ost::conop;

  // ALA
  AddRule_(ALA, 5, false);
  SetAnchors_(ALA, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(ALA, ALA_HA_INDEX);
  AddRule_(ALA, 4, false);
  SetAnchors_(ALA, BB_CB_INDEX, BB_CA_INDEX, BB_N_INDEX);
  SetHydrogens_(ALA, ALA_HB1_INDEX, ALA_HB2_INDEX, ALA_HB3_INDEX);

  // ARG
  AddRule_(ARG, 5, false);
  SetAnchors_(ARG, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(ARG, ARG_HA_INDEX);
  AddRule_(ARG, 6, false);
  SetAnchors_(ARG, BB_CB_INDEX, ARG_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(ARG, ARG_HB2_INDEX, ARG_HB3_INDEX);
  AddRule_(ARG, 6, false);
  SetAnchors_(ARG, ARG_CG_INDEX, ARG_CD_INDEX, BB_CB_INDEX);
  SetHydrogens_(ARG, ARG_HG2_INDEX, ARG_HG3_INDEX);
  AddRule_(ARG, 6, false);
  SetAnchors_(ARG, ARG_CD_INDEX, ARG_NE_INDEX, ARG_CG_INDEX);
  SetHydrogens_(ARG, ARG_HD2_INDEX, ARG_HD3_INDEX);
  AddRule_(ARG, 1, true);
  SetAnchors_(ARG, ARG_NE_INDEX, ARG_CD_INDEX, ARG_CZ_INDEX);
  SetHydrogens_(ARG, ARG_HE_INDEX);
  AddRule_(ARG, 3, true);
  SetAnchors_(ARG, ARG_NH1_INDEX, ARG_CZ_INDEX, ARG_NE_INDEX);
  SetHydrogens_(ARG, ARG_HH11_INDEX, ARG_HH12_INDEX);
  AddRule_(ARG, 3, true);
  SetAnchors_(ARG, ARG_NH2_INDEX, ARG_CZ_INDEX, ARG_NE_INDEX);
  SetHydrogens_(ARG, ARG_HH21_INDEX, ARG_HH22_INDEX);

  // ASN
  AddRule_(ASN, 5, false);
  SetAnchors_(ASN, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(ASN, ASN_HA_INDEX);
  AddRule_(ASN, 6, false);
  SetAnchors_(ASN, BB_CB_INDEX, ASN_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(ASN, ASN_HB2_INDEX, ASN_HB3_INDEX);
  AddRule_(ASN, 3, true);
  SetAnchors_(ASN, ASN_ND2_INDEX, ASN_CG_INDEX, BB_CB_INDEX);
  SetHydrogens_(ASN, ASN_HD21_INDEX, ASN_HD22_INDEX);

  // ASP
  AddRule_(ASP, 5, false);
  SetAnchors_(ASP, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(ASP, ASP_HA_INDEX);
  AddRule_(ASP, 6, false);
  SetAnchors_(ASP, BB_CB_INDEX, ASP_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(ASP, ASP_HB2_INDEX, ASP_HB3_INDEX);

  // GLN
  AddRule_(GLN, 5, false);
  SetAnchors_(GLN, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(GLN, GLN_HA_INDEX);
  AddRule_(GLN, 6, false);
  SetAnchors_(GLN, BB_CB_INDEX, GLN_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(GLN, GLN_HB2_INDEX, GLN_HB3_INDEX);
  AddRule_(GLN, 6, false);
  SetAnchors_(GLN, GLN_CG_INDEX, GLN_CD_INDEX, BB_CB_INDEX);
  SetHydrogens_(GLN, GLN_HG2_INDEX, GLN_HG3_INDEX);
  AddRule_(GLN, 3, true);
  SetAnchors_(GLN, GLN_NE2_INDEX, GLN_CD_INDEX, GLN_CG_INDEX);
  SetHydrogens_(GLN, GLN_HE21_INDEX, GLN_HE22_INDEX);

  // GLU
  AddRule_(GLU, 5, false);
  SetAnchors_(GLU, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(GLU, GLU_HA_INDEX);
  AddRule_(GLU, 6, false);
  SetAnchors_(GLU, BB_CB_INDEX, GLU_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(GLU, GLU_HB2_INDEX, GLU_HB3_INDEX);
  AddRule_(GLU, 6, false);
  SetAnchors_(GLU, GLU_CG_INDEX, GLU_CD_INDEX, BB_CB_INDEX);
  SetHydrogens_(GLU, GLU_HG2_INDEX, GLU_HG3_INDEX);

  // LYS
  AddRule_(LYS, 5, false);
  SetAnchors_(LYS, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(LYS, LYS_HA_INDEX);
  AddRule_(LYS, 6, false);
  SetAnchors_(LYS, BB_CB_INDEX, LYS_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(LYS, LYS_HB2_INDEX, LYS_HB3_INDEX);
  AddRule_(LYS, 6, false);
  SetAnchors_(LYS, LYS_CG_INDEX, LYS_CD_INDEX, BB_CB_INDEX);
  SetHydrogens_(LYS, LYS_HG2_INDEX, LYS_HG3_INDEX);
  AddRule_(LYS, 6, false);
  SetAnchors_(LYS, LYS_CD_INDEX, LYS_CE_INDEX, LYS_CG_INDEX);
  SetHydrogens_(LYS, LYS_HD2_INDEX, LYS_HD3_INDEX);
  AddRule_(LYS, 6, false);
  SetAnchors_(LYS, LYS_CE_INDEX, LYS_NZ_INDEX, LYS_CD_INDEX);
  SetHydrogens_(LYS, LYS_HE2_INDEX, LYS_HE3_INDEX);
  AddRule_(LYS, 4, true);
  SetAnchors_(LYS, LYS_NZ_INDEX, LYS_CE_INDEX, LYS_CD_INDEX);
  SetHydrogens_(LYS, LYS_HZ1_INDEX, LYS_HZ2_INDEX, LYS_HZ3_INDEX);

  // SER
  AddRule_(SER, 5, false);
  SetAnchors_(SER, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(SER, SER_HA_INDEX);
  AddRule_(SER, 6, false);
  SetAnchors_(SER, BB_CB_INDEX, SER_OG_INDEX, BB_CA_INDEX);
  SetHydrogens_(SER, SER_HB2_INDEX, SER_HB3_INDEX);
  AddRule_(SER, 2, true);
  SetAnchors_(SER, SER_OG_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(SER, SER_HG_INDEX);

  // CYS
  AddRule_(CYS, 5, false);
  SetAnchors_(CYS, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(CYS, CYS_HA_INDEX);
  AddRule_(CYS, 6, false);
  SetAnchors_(CYS, BB_CB_INDEX, CYS_SG_INDEX, BB_CA_INDEX);
  SetHydrogens_(CYS, CYS_HB2_INDEX, CYS_HB3_INDEX);
  AddRule_(CYS, 2, false);
  SetAnchors_(CYS, CYS_SG_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(CYS, CYS_HG_INDEX);

  // MET
  AddRule_(MET, 5, false);
  SetAnchors_(MET, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(MET, MET_HA_INDEX);
  AddRule_(MET, 6, false);
  SetAnchors_(MET, BB_CB_INDEX, MET_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(MET, MET_HB2_INDEX, MET_HB3_INDEX);
  AddRule_(MET, 6, false);
  SetAnchors_(MET, MET_CG_INDEX, MET_SD_INDEX, BB_CB_INDEX);
  SetHydrogens_(MET, MET_HG2_INDEX, MET_HG3_INDEX);
  AddRule_(MET, 4, false);
  SetAnchors_(MET, MET_CE_INDEX, MET_SD_INDEX, MET_CG_INDEX);
  SetHydrogens_(MET, MET_HE1_INDEX, MET_HE2_INDEX, MET_HE3_INDEX);

  // TRP
  AddRule_(TRP, 5, false);
  SetAnchors_(TRP, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(TRP, TRP_HA_INDEX);
  AddRule_(TRP, 6, false);
  SetAnchors_(TRP, BB_CB_INDEX, TRP_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(TRP, TRP_HB2_INDEX, TRP_HB3_INDEX);
  AddRule_(TRP, 1, false);
  SetAnchors_(TRP, TRP_CD1_INDEX, TRP_NE1_INDEX, TRP_CG_INDEX);
  SetHydrogens_(TRP, TRP_HD1_INDEX);
  AddRule_(TRP, 1, true);
  SetAnchors_(TRP, TRP_NE1_INDEX, TRP_CD1_INDEX, TRP_CE2_INDEX);
  SetHydrogens_(TRP, TRP_HE1_INDEX);
  AddRule_(TRP, 1, false);
  SetAnchors_(TRP, TRP_CE3_INDEX, TRP_CD2_INDEX, TRP_CZ3_INDEX);
  SetHydrogens_(TRP, TRP_HE3_INDEX);
  AddRule_(TRP, 1, false);
  SetAnchors_(TRP, TRP_CZ3_INDEX, TRP_CE3_INDEX, TRP_CH2_INDEX);
  SetHydrogens_(TRP, TRP_HZ3_INDEX);
  AddRule_(TRP, 1, false);
  SetAnchors_(TRP, TRP_CH2_INDEX, TRP_CZ3_INDEX, TRP_CZ2_INDEX);
  SetHydrogens_(TRP, TRP_HH2_INDEX);
  AddRule_(TRP, 1, false);
  SetAnchors_(TRP, TRP_CZ2_INDEX, TRP_CE2_INDEX, TRP_CH2_INDEX);
  SetHydrogens_(TRP, TRP_HZ2_INDEX);

  // TYR
  AddRule_(TYR, 5, false);
  SetAnchors_(TYR, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(TYR, TYR_HA_INDEX);
  AddRule_(TYR, 6, false);
  SetAnchors_(TYR, BB_CB_INDEX, TYR_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(TYR, TYR_HB2_INDEX, TYR_HB3_INDEX);
  AddRule_(TYR, 1, false);
  SetAnchors_(TYR, TYR_CD1_INDEX, TYR_CG_INDEX, TYR_CE1_INDEX);
  SetHydrogens_(TYR, TYR_HD1_INDEX);
  AddRule_(TYR, 1, false);
  SetAnchors_(TYR, TYR_CD2_INDEX, TYR_CG_INDEX, TYR_CE2_INDEX);
  SetHydrogens_(TYR, TYR_HD2_INDEX);
  AddRule_(TYR, 1, false);
  SetAnchors_(TYR, TYR_CE1_INDEX, TYR_CD1_INDEX, TYR_CZ_INDEX);
  SetHydrogens_(TYR, TYR_HE1_INDEX);
  AddRule_(TYR, 1, false);
  SetAnchors_(TYR, TYR_CE2_INDEX, TYR_CD2_INDEX, TYR_CZ_INDEX);
  SetHydrogens_(TYR, TYR_HE2_INDEX);
  AddRule_(TYR, 2, true);
  SetAnchors_(TYR, TYR_OH_INDEX, TYR_CZ_INDEX, TYR_CE1_INDEX);
  SetHydrogens_(TYR, TYR_HH_INDEX);

  // THR
  AddRule_(THR, 5, false);
  SetAnchors_(THR, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(THR, THR_HA_INDEX);
  AddRule_(THR, 5, false);
  SetAnchors_(THR, BB_CB_INDEX, BB_CA_INDEX, THR_OG1_INDEX, THR_CG2_INDEX);
  SetHydrogens_(THR, THR_HB_INDEX);
  AddRule_(THR, 2, true);
  SetAnchors_(THR, THR_OG1_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(THR, THR_HG1_INDEX);
  AddRule_(THR, 4, false);
  SetAnchors_(THR, THR_CG2_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(THR, THR_HG21_INDEX, THR_HG22_INDEX, THR_HG23_INDEX);

  // VAL
  AddRule_(VAL, 5, false);
  SetAnchors_(VAL, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(VAL, VAL_HA_INDEX);
  AddRule_(VAL, 5, false);
  SetAnchors_(VAL, BB_CB_INDEX, BB_CA_INDEX, VAL_CG1_INDEX, VAL_CG2_INDEX);
  SetHydrogens_(VAL, VAL_HB_INDEX);
  AddRule_(VAL, 4, false);
  SetAnchors_(VAL, VAL_CG1_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(VAL, VAL_HG11_INDEX, VAL_HG12_INDEX, VAL_HG13_INDEX);
  AddRule_(VAL, 4, false);
  SetAnchors_(VAL, VAL_CG2_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(VAL, VAL_HG21_INDEX, VAL_HG22_INDEX, VAL_HG23_INDEX);

  // ILE
  AddRule_(ILE, 5, false);
  SetAnchors_(ILE, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(ILE, ILE_HA_INDEX);
  AddRule_(ILE, 5, false);
  SetAnchors_(ILE, BB_CB_INDEX, BB_CA_INDEX, ILE_CG1_INDEX, ILE_CG2_INDEX);
  SetHydrogens_(ILE, ILE_HB_INDEX);
  AddRule_(ILE, 6, false);
  SetAnchors_(ILE, ILE_CG1_INDEX, ILE_CD1_INDEX, BB_CB_INDEX);
  SetHydrogens_(ILE, ILE_HG12_INDEX, ILE_HG13_INDEX);
  AddRule_(ILE, 4, false);
  SetAnchors_(ILE, ILE_CG2_INDEX, BB_CB_INDEX, BB_CA_INDEX);
  SetHydrogens_(ILE, ILE_HG21_INDEX, ILE_HG22_INDEX, ILE_HG23_INDEX);
  AddRule_(ILE, 4, false);
  SetAnchors_(ILE, ILE_CD1_INDEX, ILE_CG1_INDEX, BB_CB_INDEX);
  SetHydrogens_(ILE, ILE_HD11_INDEX, ILE_HD12_INDEX, ILE_HD13_INDEX);

  // LEU
  AddRule_(LEU, 5, false);
  SetAnchors_(LEU, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(LEU, LEU_HA_INDEX);
  AddRule_(LEU, 6, false);
  SetAnchors_(LEU, BB_CB_INDEX, LEU_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(LEU, LEU_HB2_INDEX, LEU_HB3_INDEX);
  AddRule_(LEU, 5, false);
  SetAnchors_(LEU, LEU_CG_INDEX, BB_CB_INDEX, LEU_CD1_INDEX, LEU_CD2_INDEX);
  SetHydrogens_(LEU, LEU_HG_INDEX);
  AddRule_(LEU, 4, false);
  SetAnchors_(LEU, LEU_CD1_INDEX, LEU_CG_INDEX, BB_CB_INDEX);
  SetHydrogens_(LEU, LEU_HD11_INDEX, LEU_HD12_INDEX, LEU_HD13_INDEX);
  AddRule_(LEU, 4, false);
  SetAnchors_(LEU, LEU_CD2_INDEX, LEU_CG_INDEX, BB_CB_INDEX);
  SetHydrogens_(LEU, LEU_HD21_INDEX, LEU_HD22_INDEX, LEU_HD23_INDEX);

  // GLY
  AddRule_(GLY, 6, false);
  SetAnchors_(GLY, BB_CA_INDEX, BB_C_INDEX, BB_N_INDEX);
  SetHydrogens_(GLY, GLY_HA2_INDEX, GLY_HA3_INDEX);

  // PRO
  AddRule_(PRO, 5, false);
  SetAnchors_(PRO, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(PRO, PRO_HA_INDEX);
  AddRule_(PRO, 6, false);
  SetAnchors_(PRO, BB_CB_INDEX, PRO_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(PRO, PRO_HB2_INDEX, PRO_HB3_INDEX);
  AddRule_(PRO, 6, false);
  SetAnchors_(PRO, PRO_CG_INDEX, PRO_CD_INDEX, BB_CB_INDEX);
  SetHydrogens_(PRO, PRO_HG2_INDEX, PRO_HG3_INDEX);
  AddRule_(PRO, 6, false);
  SetAnchors_(PRO, PRO_CD_INDEX, BB_N_INDEX, PRO_CG_INDEX);
  SetHydrogens_(PRO, PRO_HD2_INDEX, PRO_HD3_INDEX);

  // HIS
  AddRule_(HIS, 5, false);
  SetAnchors_(HIS, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(HIS, HIS_HA_INDEX);
  AddRule_(HIS, 6, false);
  SetAnchors_(HIS, BB_CB_INDEX, HIS_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(HIS, HIS_HB2_INDEX, HIS_HB3_INDEX);
  AddRule_(HIS, 1, false);
  SetAnchors_(HIS, HIS_CD2_INDEX, HIS_CG_INDEX, HIS_NE2_INDEX);
  SetHydrogens_(HIS, HIS_HD2_INDEX);
  AddRule_(HIS, 1, false);
  SetAnchors_(HIS, HIS_CE1_INDEX, HIS_ND1_INDEX, HIS_NE2_INDEX);
  SetHydrogens_(HIS, HIS_HE1_INDEX);
  AddRule_(HIS, 1, true, PROT_STATE_HISD);
  SetAnchors_(HIS, HIS_ND1_INDEX, HIS_CG_INDEX, HIS_CE1_INDEX);
  SetHydrogens_(HIS, HIS_HD1_INDEX);
  AddRule_(HIS, 1, true, PROT_STATE_HISE);
  SetAnchors_(HIS, HIS_NE2_INDEX, HIS_CE1_INDEX, HIS_CD2_INDEX);
  SetHydrogens_(HIS, HIS_HE2_INDEX);

  // PHE
  AddRule_(PHE, 5, false);
  SetAnchors_(PHE, BB_CA_INDEX, BB_N_INDEX, BB_C_INDEX, BB_CB_INDEX);
  SetHydrogens_(PHE, PHE_HA_INDEX);
  AddRule_(PHE, 6, false);
  SetAnchors_(PHE, BB_CB_INDEX, PHE_CG_INDEX, BB_CA_INDEX);
  SetHydrogens_(PHE, PHE_HB2_INDEX, PHE_HB3_INDEX);
  AddRule_(PHE, 1, false);
  SetAnchors_(PHE, PHE_CD1_INDEX, PHE_CG_INDEX, PHE_CE1_INDEX);
  SetHydrogens_(PHE, PHE_HD1_INDEX);
  AddRule_(PHE, 1, false);
  SetAnchors_(PHE, PHE_CD2_INDEX, PHE_CG_INDEX, PHE_CE2_INDEX);
  SetHydrogens_(PHE, PHE_HD2_INDEX);
  AddRule_(PHE, 1, false);
  SetAnchors_(PHE, PHE_CE1_INDEX, PHE_CD1_INDEX, PHE_CZ_INDEX);
  SetHydrogens_(PHE, PHE_HE1_INDEX);
  AddRule_(PHE, 1, false);
  SetAnchors_(PHE, PHE_CE2_INDEX, PHE_CD2_INDEX, PHE_CZ_INDEX);
  SetHydrogens_(PHE, PHE_HE2_INDEX);
  AddRule_(PHE, 1, false);
  SetAnchors_(PHE, PHE_CZ_INDEX, PHE_CE1_INDEX, PHE_CE2_INDEX);
  SetHydrogens_(PHE, PHE_HZ_INDEX);

  // extract single rules for each hydrogen -> NULL if no rule
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  std::fill_n(single_rule_, XXX_NUM_HYDROGENS+1, (HydrogenRule*)NULL);
  for (uint aa_idx = 0; aa_idx < XXX; ++aa_idx) {
    const AminoAcid aa = AminoAcid(aa_idx);
    for (uint rule_idx = 0; rule_idx < rules_[aa_idx].size(); ++rule_idx) {
      HydrogenRule& rule = rules_[aa_idx][rule_idx];
      for (uint h_idx = 0; h_idx < rule.num_hydrogens; ++h_idx) {
        const uint atom_idx = rule.hydro_idx[h_idx];
        const AminoAcidHydrogen aah = aa_lookup.GetAAH(aa, atom_idx);
        single_rule_[aah] = &rule;
      }
    }
  }
}

}} //ns
