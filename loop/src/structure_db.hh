// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_STRUCTURE_DB_HH
#define PROMOD_LOOP_STRUCTURE_DB_HH

#include <stdint.h>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <limits>
#include <list>

#include <ost/base.hh>
#include <ost/paged_array.hh>
#include <ost/fixed_string.hh>
#include <ost/mol/entity_view.hh>
#include <ost/mol/mol.hh>
#include <ost/seq/alg/subst_weight_matrix.hh>
#include <ost/seq/alignment_handle.hh>
#include <ost/seq/aligned_column_iterator.hh>
#include <ost/seq/alg/semiglobal_align.hh>
#include <ost/seq/profile_handle.hh>
#include <ost/mol/surface_handle.hh>

#include <promod3/loop/backbone.hh>
#include <promod3/core/message.hh>
#include <promod3/core/superpose.hh>
#include <promod3/loop/structure_db_custom_types.hh>

#include <promod3/core/portable_binary_serializer.hh>

namespace promod3 { namespace loop {

class StructureDB;
typedef boost::shared_ptr<StructureDB> StructureDBPtr;


struct CoordInfo {
  CoordInfo() { }
  CoordInfo(const String& id, const String& chain_name,
            uint off, uint s, uint sr, const geom::Vec3 sh): 
                                                id(id), chain_name(chain_name), 
                                                offset(off), size(s), 
                                                start_resnum(sr), shift(sh) { }

  /// pdb id plus chain all lowercase letters
  String GetFullID() const { 
    return id + "_" + chain_name;
  }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertBaseType<String> (ds, id);
    core::ConvertBaseType<String> (ds, chain_name);    
    core::ConvertBaseType<uint32_t>(ds, offset);
    core::ConvertBaseType<uint16_t>(ds, size);
    core::ConvertBaseType<uint16_t>(ds, start_resnum);
    core::ConvertBaseType<float>(ds, shift[0]);
    core::ConvertBaseType<float>(ds, shift[1]);
    core::ConvertBaseType<float>(ds, shift[2]);
  }

  bool operator==(const CoordInfo& rhs) const {
    return id == rhs.id
        && chain_name == rhs.chain_name
        && offset == rhs.offset
        && size == rhs.size
        && start_resnum == rhs.start_resnum;
  }
  bool operator!=(const CoordInfo& rhs) const {
    return !this->operator==(rhs);
  }

  String               id;
  String               chain_name;
  uint                 offset;
  unsigned short       size;
  unsigned short       start_resnum;
  geom::Vec3           shift;
};


struct FragmentInfo {
  FragmentInfo() { }
  FragmentInfo(unsigned int c_i,
               unsigned short o,
               unsigned short l): chain_index(c_i),
                                  offset(o),
                                  length(l){ }
  
  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, chain_index);
    core::ConvertBaseType<uint16_t>(ds, offset);
    core::ConvertBaseType<uint16_t>(ds, length);
  }

  bool operator==(const FragmentInfo& rhs) const {
    return chain_index == rhs.chain_index
        && offset == rhs.offset
        && length == rhs.length;
  }
  bool operator!=(const FragmentInfo& rhs) const {
    return !this->operator==(rhs);
  }

  unsigned int chain_index;
  unsigned short offset;
  unsigned short length;
};


class StructureDB {
public:

  enum DBDataType {All = -1, Minimal = 0, Dihedrals = 1, 
                   SolventAccessibilities = 2, ResidueDepths = 4, DSSP = 8, 
                   AAFrequencies = 16, AAFrequenciesStruct = 32};

  StructureDB(int32_t stored_data = -1): stored_data_(stored_data) { }

  static StructureDBPtr Load(const String& file_name);

  void Save(const String& filename);

  static StructureDBPtr LoadPortable(const String& file_name);

  void SavePortable(const String& filename);

  bool HasData(DBDataType data_type) const {
    return (data_type & stored_data_) == data_type;
  }

  bool operator==(const StructureDB& rhs) const;
  bool operator!=(const StructureDB& rhs) const {
    return !this->operator==(rhs);
  }

  std::vector<int> AddCoordinates(const String& id, const String& chain_name,
                                  ost::mol::EntityView& ent,
                                  ost::seq::SequenceHandle& seqres,
                                  ost::seq::ProfileHandlePtr prof = 
                                  ost::seq::ProfileHandlePtr(),
                                  bool only_longest_stretch = true);

  std::vector<int> AddCoordinates(const String& id, const String& chain_name,
                                  ost::mol::EntityHandle& ent,
                                  ost::seq::SequenceHandle& seqres,
                                  ost::seq::ProfileHandlePtr prof = 
                                  ost::seq::ProfileHandlePtr(),
                                  bool only_longest_stretch = true);

  void RemoveCoordinates(uint coord_idx);

  std::vector<int> GetCoordIdx(const String& id, 
                               const String& chain_name) const;

  const CoordInfo& GetCoordInfo(int index) const;

  size_t GetNumCoords() const { return coord_toc_.size(); }

  Real CARMSD(const FragmentInfo& info_one,
              const FragmentInfo& info_two,
              bool superpose_stems = true) const;

  void FillBackbone(BackboneList& bb_list, const FragmentInfo& info,
                    const String& sequence = "") const;

  void FillBackbone(BackboneList& bb_list, const geom::Mat4& tf,
                    const FragmentInfo& info, const String& sequence = "") const;

  void FillBackbone(BackboneList& bb_list,
                    const ost::mol::ResidueHandle& stem_one,
                    const ost::mol::ResidueHandle& stem_two,
                    const FragmentInfo& info, const String& sequence = "") const;

  void FillBackbone(BackboneList& bb_list, uint coord_idx,
                    const String& sequence = "") const;

  void FillBackbone(BackboneList& bb_list, const geom::Mat4& tf,
                    uint coord_idx, const String& sequence = "") const;

  void FillBackbone(BackboneList& bb_list,
                    const ost::mol::ResidueHandle& stem_one,
                    const ost::mol::ResidueHandle& stem_two,
                    uint coord_idx, const String& sequence = "") const;

  int GetStartResnum(const FragmentInfo& info) const;

  StructureDBPtr GetSubDB(const std::vector<uint>& indices) const;

  void PrintStatistics() const;

  ost::seq::ProfileHandlePtr GenerateStructureProfile(
                                const BackboneList& other_db,
                                const std::vector<Real>& residue_depths) const;

  void SetStructureProfile(uint chain_idx, ost::seq::ProfileHandlePtr prof);

  // getters for FragmentInfo
  String GetSequence(const FragmentInfo& info) const;

  String GetDSSPStates(const FragmentInfo& info) const;

  std::vector<std::pair<Real,Real> > GetDihedralAngles(const FragmentInfo& info) const;

  std::vector<Real> GetResidueDepths(const FragmentInfo& info) const;

  std::vector<int> GetSolventAccessibilities(const FragmentInfo& info) const;

  ost::seq::ProfileHandlePtr GetSequenceProfile(const FragmentInfo& info) const;

  ost::seq::ProfileHandlePtr GetStructureProfile(const FragmentInfo& info) const;

  // getters for full coords
  String GetSequence(uint coord_idx) const;

  String GetDSSPStates(uint coord_idx) const;

  std::vector<std::pair<Real,Real> > GetDihedralAngles(uint coord_idx) const;

  std::vector<Real> GetResidueDepths(uint coord_idx) const;

  std::vector<int> GetSolventAccessibilities(uint coord_idx) const;

  ost::seq::ProfileHandlePtr GetSequenceProfile(uint coord_idx) const;

  ost::seq::ProfileHandlePtr GetStructureProfile(uint coord_idx) const;

  // functions to gain direct access to db internals, not intended for python 
  // export

  const std::vector<CoordInfo>& GetCoordToc() const { return coord_toc_; }

  const ost::PagedArray<char, 65536>& GetSeq() const { 
    return seq_; 
  } 

  const ost::PagedArray<PeptideCoords, 65536>& GetCoords() const { 
    return coords_; 
  }

  const ost::PagedArray<DihedralInfo, 65536>& GetDihedrals() const {

    if(!this->HasData(Dihedrals)) {
      throw promod3::Error("StructureDB does not contain dihedrals!");
    }

    return dihedrals_; 
  }

  const ost::PagedArray<unsigned short, 65536>& GetSolvAcc() const {

    if(!this->HasData(SolventAccessibilities)) {
      throw promod3::Error("StructureDB does not contain "
                           "solvent accessibilities!");
    }

    return solv_acc_;
  }

  const ost::PagedArray<unsigned short, 65536>& GetResDepth() const {

    if(!this->HasData(ResidueDepths)) {
      throw promod3::Error("StructureDB does not contain residue depths!");
    }

    return res_depths_;
  }

  const ost::PagedArray<char, 65536>& GetDSSP() const {

    if(!this->HasData(DSSP)) {
      throw promod3::Error("StructureDB does not contain dssp assignments!");
    }

    return dssp_;
  } 

  const ost::PagedArray<AAFreq, 65536>& GetAAFrequencies() const { 

    if(!this->HasData(AAFrequencies)) {
      throw promod3::Error("StructureDB does not contain amino acid "
                           "frequencies!");
    }

    return aa_frequencies_; 
  }

  const ost::PagedArray<AAFreq, 65536>& GetAAStructFrequencies() const {

    if(!this->HasData(AAFrequenciesStruct)) {
      throw promod3::Error("StructureDB does not contain amino acid " 
                           "frequencies derived from structural information!");
    }

    return aa_frequencies_struct_; 
  }

private:

  // just don't allow it...
  StructureDB(const StructureDB& other) { };

  // get start index into paged arrays
  int GetStartIndex(const FragmentInfo& info) const {
    return coord_toc_[info.chain_index].offset + info.offset;
  }
  void CheckFragmentInfo(const FragmentInfo& info) const;

  ///////////////////
  // INTERNAL DATA //
  ///////////////////

  // bitfield that defines what kind of data is stored in the database
  int32_t stored_data_;

  // stores information about full stretches
  std::vector<CoordInfo>            coord_toc_;

  // one letter codes
  ost::PagedArray<char, 65536>           seq_;

  // backbone coordinates
  ost::PagedArray<PeptideCoords, 65536>  coords_;

  // phi and psi dihedrals
  ost::PagedArray<DihedralInfo, 65536>   dihedrals_;    

  // solvent accessibility in A^2 as calculated by Lee & Richards implementation 
  // in ost
  ost::PagedArray<uint16_t, 65536> solv_acc_;      

  // distance towards closest surface residue in A multiplied by 10 to allow
  // accuracy of 0.1
  ost::PagedArray<uint16_t, 65536> res_depths_;   
                    
  // dssp state, which is a member of {H,B,E,G,I,T,S}  
  ost::PagedArray<char, 65536>           dssp_;

  // Amino acid frequencies as given as input profile
  ost::PagedArray<AAFreq, 65536>         aa_frequencies_;

  // Amino acid frequencies as derived from structural information
  ost::PagedArray<AAFreq, 65536>         aa_frequencies_struct_;
};

}}

#endif
