// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <fstream>
#include <limits>
#include <ost/mol/mol.hh>

#include <promod3/loop/frag_db.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace loop {

FragDB::FragDB(Real dist_bin_size, int ang_bin_s, bool load_from_disk):
  angular_bin_size_(ang_bin_s), dist_bin_size_(dist_bin_size){
  if(!load_from_disk) fragments_.SetEmptyKey(StemPairGeom());
}

FragDBPtr FragDB::Load(const String& file_name) {
  
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "FragDB::Load", 2);

  // open file
  std::ifstream in_stream(file_name.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << file_name << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version != 2) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << file_name;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  typedef FragmentData::BagInfo BI;
  typedef std::pair<StemPairGeom,BI> MyPair;
  core::CheckTypeSize<int>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<char>(in_stream);
  core::CheckTypeSize<unsigned int>(in_stream);
  core::CheckTypeSize<unsigned short>(in_stream);
  core::CheckTypeSize<MyPair>(in_stream);
  core::CheckTypeSize<StemPairGeom>(in_stream);
  core::CheckTypeSize<BI>(in_stream);
  core::CheckTypeSize<FragmentInfo>(in_stream);

  core::CheckBaseType<int>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<char>(in_stream);
  core::CheckBaseType<unsigned int>(in_stream);
  core::CheckBaseType<unsigned short>(in_stream);

  // raw data
  Real dist_bin_size;
  int angular_bin_size;
  in_stream.read(reinterpret_cast<char*>(&dist_bin_size), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&angular_bin_size), sizeof(int));
  FragDBPtr frag_db(new FragDB(dist_bin_size,angular_bin_size,true));
  uint frag_lengths_size;
  in_stream.read(reinterpret_cast<char*>(&frag_lengths_size), sizeof(uint));
  for (uint i = 0; i < frag_lengths_size; ++i) {
    uint cur_length;
    in_stream.read(reinterpret_cast<char*>(&cur_length), sizeof(uint));
    frag_db->frag_lengths_.insert(cur_length);
  }
  frag_db->fragments_.Read(in_stream);
  return frag_db;
}

void FragDB::Save(const String& filename) {
  // open file
  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 2);
  // required base types: int, Real, uint, char, unsigned int, unsigned short
  // required structs: std::pair<StemPairGeom, BagInfo>, FragmentInfo
  typedef FragmentData::BagInfo BI;
  typedef std::pair<StemPairGeom,BI> MyPair;
  core::WriteTypeSize<int>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<char>(out_stream);
  core::WriteTypeSize<unsigned int>(out_stream);
  core::WriteTypeSize<unsigned short>(out_stream);
  core::WriteTypeSize<MyPair>(out_stream);
  core::WriteTypeSize<StemPairGeom>(out_stream);
  core::WriteTypeSize<BI>(out_stream);
  core::WriteTypeSize<FragmentInfo>(out_stream);

  core::WriteBaseType<int>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<char>(out_stream);
  core::WriteBaseType<unsigned int>(out_stream);
  core::WriteBaseType<unsigned short>(out_stream);

  // raw data
  out_stream.write(reinterpret_cast<char*>(&dist_bin_size_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&angular_bin_size_), sizeof(int));
  uint frag_lengths_size = frag_lengths_.size();
  out_stream.write(reinterpret_cast<char*>(&frag_lengths_size), sizeof(uint));
  for (std::set<uint>::iterator i = frag_lengths_.begin();
       i != frag_lengths_.end(); ++i) {
    out_stream.write(reinterpret_cast<const char*>(&*i), sizeof(uint));
  }
  fragments_.SetReadOnly(true);
  fragments_.Write(out_stream);
  out_stream.close();
}

FragDBPtr FragDB::LoadPortable(const String& file_name) {
  // open file
  std::ifstream in_stream_(file_name.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << file_name << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version != 2) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << file_name;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed
  core::CheckTypeSize<short>(in_stream, true);
  core::CheckTypeSize<int>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);

  core::CheckBaseType<uint16_t>(in_stream);
  core::CheckBaseType<int32_t>(in_stream);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<float>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // load data
  Real dist_bin_size;
  int angular_bin_size;
  core::ConvertBaseType<float>(in_stream, dist_bin_size);
  core::ConvertBaseType<int32_t>(in_stream, angular_bin_size);
  FragDBPtr frag_db(new FragDB(dist_bin_size, angular_bin_size, true));
  in_stream & frag_db->fragments_;
  frag_db->fragments_.SetReadOnly(true);
  // set frag_lengths_
  frag_db->SetFragLengths();

  in_stream_.close();
  return frag_db;
}

void FragDB::SavePortable(const String& filename) {
  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 2);
  // here: only base-type-checks needed (we store Real as float!)
  core::WriteTypeSize<int16_t>(out_stream);
  core::WriteTypeSize<int32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);

  core::WriteBaseType<uint16_t>(out_stream);
  core::WriteBaseType<int32_t>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<float>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // dump data
  core::ConvertBaseType<float>(out_stream, dist_bin_size_);
  core::ConvertBaseType<int32_t>(out_stream, angular_bin_size_);
  out_stream & fragments_;

  out_stream_.close();
}

bool FragDB::operator==(const FragDB& rhs) const {
  return dist_bin_size_ == rhs.dist_bin_size_
      && angular_bin_size_ == rhs.angular_bin_size_
      && fragments_ == rhs.fragments_;
}

void FragDB::AddFragments(uint fragment_length,
                          Real rmsd_cutoff,
                          StructureDBPtr structure_db){

  if(rmsd_cutoff < 0.0){
    throw promod3::Error("Have you ever seen an RMSD below zero?");
  }

  if(fragment_length > static_cast<uint>(std::numeric_limits<char>::max())){
    std::stringstream ss;
    ss << "Fragment size is limited to "<<std::numeric_limits<char>::max();
    throw promod3::Error(ss.str());
  }

  uint num_pdb_chains = structure_db->GetNumCoords();
  CoordInfo info;
  uint size,offset;
  bool already_there;
  bool added_sthg = false;
  std::vector<StemPairGeom> stem_pair_geoms;
  IteratorPair it_pair;
  std::vector<StemCoordPair> stem_coords;
  FragmentInfo current_fragment;

  current_fragment.length = static_cast<char>(fragment_length);

  for(uint i = 0; i < num_pdb_chains; ++i){
    
    info = structure_db->GetCoordInfo(i);
    size = info.size;
    offset = info.offset;
    current_fragment.chain_index = i;

    if(size < fragment_length) continue;

    //extract all stem information of this chain to create the
    //stem geoms later on
    const ost::PagedArray<PeptideCoords, 65536>& db_coords = structure_db->GetCoords();
    stem_coords.clear();
    for(uint j = 0; j < size; ++j){
      const PeptideCoords& coords = db_coords[offset + j];
      stem_coords.push_back(StemCoordPairFromPeptideCoords(coords, coords));
    }

    //create a sliding window and go over all possible fragments
    //of this particular pdb chain

    for(uint j = 0; j < size - fragment_length; ++j){
      already_there = false;
      current_fragment.offset = j;
      //go over all fragments in the database with the same stem pair
      //geom and check, whether there is one with low RMSD
      stem_pair_geoms = MakeStemPairGeom(stem_coords[j].first, 
                                         stem_coords[j+fragment_length-1].second,
                                         fragment_length);
      it_pair = fragments_.Get(stem_pair_geoms[0]);
      for (;it_pair.first!=it_pair.second; ++it_pair.first){
        if(structure_db->CARMSD(current_fragment,*(it_pair.first)) < rmsd_cutoff){
          already_there = true;
          break;
        }
      }
      if (already_there) continue;
      SetReadOnly(false);
      fragments_.Append(stem_pair_geoms[0], &current_fragment, &current_fragment+1); 
      added_sthg = true;
    }
  }
  if (added_sthg) frag_lengths_.insert(fragment_length);
}

void FragDB::SearchDB(const ost::mol::ResidueHandle& n_stem,
                      const ost::mol::ResidueHandle& c_stem,
                      uint frag_size, std::vector<FragmentInfo>& fragments,
                      uint extra_bins) {

  StemCoordPair stems = StemCoordPairFromResidues(n_stem, c_stem);
  SearchDB(stems, frag_size, fragments, extra_bins);
}

void FragDB::SearchDB(const StemCoordPair& stems,
                      uint frag_size,
                      std::vector<FragmentInfo>& fragments,
                      uint extra_bins) {

  std::vector<StemPairGeom> stem_pairs;
  try {
    stem_pairs = MakeStemPairGeom(stems.first, stems.second, frag_size,
                                  extra_bins);
  } catch (const promod3::Error&) {
    // MakeStemPairGeom can fail if distance bin is out of bounds
    // -> this is ok here as it simply means that we have no fitting fragments
    //    in this FragDB
    // => silently ignore and keep fragments vector unchanged
    return;
  }

  for(std::vector<StemPairGeom>::iterator it = stem_pairs.begin();
      it != stem_pairs.end(); ++it){
    IteratorPair matches = fragments_.Get(*it);
    for (;matches.first!=matches.second; ++matches.first) {
      fragments.push_back(*matches.first);
    }
  }  
}

void FragDB::PrintStatistics() const{
  std::cout << "number of stem groups     :" << GetNumStemPairs() << std::endl;
  std::cout << "total fragments           :" << GetNumFragments() << std::endl;
  uint smax=std::numeric_limits<uint>::min();
  uint smin=std::numeric_limits<uint>::max();
  if (fragments_.ItemCount()>0) {
    for (FragmentData::Iterator i=fragments_.Begin(), e=fragments_.End();
         i!=e; ++i) {
      std::pair<StemPairGeom, FragmentData::IteratorPair> p=*i;
      uint size=p.second.second-p.second.first;
      smax=std::max(size, smax);
      smin=std::min(size, smin);
    }
  } else {
    smax=smin=0;
  }
  std::cout << "max=" << smax << ", min=" << smin << std::endl;
}

int FragDB::GetNumStemPairs(int length) const {
  int count = 0;
  for(FragmentData::Iterator i=fragments_.Begin(), e=fragments_.End();
       i!=e; ++i) {
    if(length == (*i).first.length) ++count;
  } 
  return count; 
}

int FragDB::GetNumFragments(int length) const{
  int count = 0;
  for(FragmentData::Iterator i=fragments_.Begin(), e=fragments_.End();
       i!=e; ++i) {
    if(length == (*i).first.length){
      count += (*i).second.second - (*i).second.first;
    }
  } 
  return count; 
}

FragDB::StemCoordPair FragDB::StemCoordPairFromResidues(
                                const ost::mol::ResidueHandle& n_res,
                                const ost::mol::ResidueHandle& c_res) {

  core::StemCoords n_stem(n_res);
  core::StemCoords c_stem(c_res);
  return std::make_pair(n_stem, c_stem);
}

FragDB::StemCoordPair FragDB::StemCoordPairFromPeptideCoords(
                                           const PeptideCoords& n_coords,
                                           const PeptideCoords& c_coords) {

  core::StemCoords n_stem;
  core::StemCoords c_stem;

  n_stem.n_coord = n_coords.n.ToVec3();
  n_stem.ca_coord = n_coords.ca.ToVec3();
  n_stem.c_coord = n_coords.c.ToVec3();

  c_stem.n_coord = c_coords.n.ToVec3();
  c_stem.ca_coord = c_coords.ca.ToVec3();
  c_stem.c_coord = c_coords.c.ToVec3();

  return std::make_pair(n_stem,c_stem);
}


std::vector<StemPairGeom>
FragDB::MakeStemPairGeom(const core::StemCoords& n_stem, 
                         const core::StemCoords& c_stem, int length,
                         uint extra_bins) const {

  // get orientation
  core::StemPairOrientation orient(n_stem, c_stem);

  // normalize distance
  Real f_d = orient.distance / dist_bin_size_; 
  int d = int(f_d);
  // check limits
  const int max_d = std::numeric_limits<char>::max();
  if (d >= max_d) {
    std::stringstream ss;
    ss << "Due to technical reasons, a fragment with distance bin size ";
    ss << dist_bin_size_ << " can not cover distances larger than ";
    ss << static_cast<Real>(max_d)*dist_bin_size_;
    throw promod3::Error(ss.str());
  }
  
  // normalize angles
  int num_angle_bins = 360 / angular_bin_size_;
  Real f = Real(180) / (angular_bin_size_ * Real(M_PI));
  Real f_angle_one = (orient.angle_one + Real(M_PI)) * f;
  Real f_angle_two = (orient.angle_two + Real(M_PI)) * f;
  Real f_angle_three = (orient.angle_three + Real(M_PI)) * f;
  Real f_angle_four = (orient.angle_four + Real(M_PI)) * f;
  // note: angles are in [-pi,pi] -> modulo needed if exactly pi
  int i_angle_one = int(f_angle_one) % num_angle_bins;
  int i_angle_two = int(f_angle_two) % num_angle_bins;
  int i_angle_three = int(f_angle_three) % num_angle_bins;
  int i_angle_four = int(f_angle_four) % num_angle_bins;

  std::vector<StemPairGeom> return_vec;
  
  if (extra_bins == 0) {
    // special fast case
    return_vec.push_back(StemPairGeom(i_angle_one, i_angle_two, i_angle_three,
                                      i_angle_four, d, length));
  } else {
    // limit extra bins
    extra_bins = std::min(extra_bins, uint(num_angle_bins));
    // we wish to have extra_bins additional bins
    // -> odd extra_bins: we use closer one, else symmetric
    int bins_per_side = extra_bins / 2;
    int d_a1_min = -bins_per_side;
    int d_a1_max = bins_per_side;
    int d_a2_min = -bins_per_side;
    int d_a2_max = bins_per_side;
    int d_a3_min = -bins_per_side;
    int d_a3_max = bins_per_side;
    int d_a4_min = -bins_per_side;
    int d_a4_max = bins_per_side;
    int d_d_min = -bins_per_side;
    int d_d_max = bins_per_side;
    // fix for odd extra bins
    if (extra_bins % 2 == 1) {
      if (round(f_angle_one) == i_angle_one) --d_a1_min;
      else                                   ++d_a1_max;
      if (round(f_angle_two) == i_angle_two) --d_a2_min;
      else                                   ++d_a2_max;
      if (round(f_angle_three) == i_angle_three) --d_a3_min;
      else                                       ++d_a3_max;
      if (round(f_angle_four) == i_angle_four) --d_a4_min;
      else                                     ++d_a4_max;
      if (round(f_d) == d) --d_d_min;
      else                 ++d_d_max;
    }
    // check bounds for distances (enforce d + d_d to be in [0,max_d])
    d_d_min = std::max(d_d_min, -d);
    d_d_max = std::min(d_d_max, max_d - d);
    // loop over all combinations
    for (int d_a1 = d_a1_min; d_a1 <= d_a1_max; ++d_a1) {
      int cur_a1 = (i_angle_one + num_angle_bins + d_a1) % num_angle_bins;
      for (int d_a2 = d_a2_min; d_a2 <= d_a2_max; ++d_a2) {
        int cur_a2 = (i_angle_two + num_angle_bins + d_a2) % num_angle_bins;
        for (int d_a3 = d_a3_min; d_a3 <= d_a3_max; ++d_a3) {
          int cur_a3 = (i_angle_three + num_angle_bins + d_a3) % num_angle_bins;
          for (int d_a4 = d_a4_min; d_a4 <= d_a4_max; ++d_a4) {
            int cur_a4 = (i_angle_four + num_angle_bins + d_a4) % num_angle_bins;
            for (int d_d = d_d_min; d_d <= d_d_max; ++d_d) {
              int cur_d = d + d_d; // bounds enforced by d_d_min, d_d_max
              return_vec.push_back(StemPairGeom(cur_a1, cur_a2, cur_a3, cur_a4,
                                                cur_d, length));
            }
          }
        }
      }
    }
  }

  return return_vec;
}

void FragDB::SetFragLengths() {
  frag_lengths_.clear();
  if (fragments_.ItemCount() > 0) {
    for (FragmentData::Iterator i = fragments_.Begin(), e = fragments_.End();
         i != e; ++i) {
      const StemPairGeom& s = (*i).first;
      frag_lengths_.insert(static_cast<uint>(s.length));
    }
  }
}

}}
