// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_BACKBONE_HH
#define PM3_BACKBONE_HH


#include <vector>
#include <ost/base.hh>
#include <ost/geom/vecmat3_op.hh>
#include <ost/conop/amino_acids.hh>
#include <ost/geom/transform.hh>
#include <ost/mol/entity_handle.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/mol/bond_handle.hh>
#include <ost/img/map.hh>

#include <promod3/core/message.hh>
#include <promod3/core/geom_base.hh>
#include <promod3/core/superpose.hh>
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3 { namespace loop {

class BackboneList;
typedef boost::shared_ptr<BackboneList> BackboneListPtr;

// this struct is for internal use only!
struct Backbone {
  // DATA: do NOT change order/types without checking ApplyTransform function!!
  geom::Vec3 n_coord;
  geom::Vec3 ca_coord;
  geom::Vec3 cb_coord;
  geom::Vec3 c_coord;
  geom::Vec3 o_coord;
  char one_letter_code;
  ost::conop::AminoAcid aa;

  // CONSTRUCT
  Backbone() { }
  Backbone(const geom::Vec3& n, const geom::Vec3& ca, const geom::Vec3& cb,
           const geom::Vec3& c, const geom::Vec3& o, char olc);
  Backbone(const geom::Vec3& n, const geom::Vec3& ca,
           const geom::Vec3& c, const geom::Vec3& o, char olc);
  Backbone(const ost::mol::ResidueHandle& res, char olc = '\0');

  // SAFE AA SETTER (disallows XXX)
  void SetAA(ost::conop::AminoAcid new_aa) {
    if (new_aa == ost::conop::XXX) {
      throw promod3::Error("Invalid amino acid observed in Backbone!");
    } else {
      aa = new_aa;
    }
  }

  // FUNCTIONALITY
  void ApplyTransform(const geom::Mat4& t) {
    geom::Vec3 * pos_ptr = &n_coord;
    Real a,b,c;
    for (int i = 0; i < 5; ++i) {
      // benmchmarks show, that this is much faster than writing something like
      // (*pos_ptr) = geom::Vec3(t*(*pos_ptr));
      a = t(0,0)*(*pos_ptr)[0]+t(0,1)*(*pos_ptr)[1]+t(0,2)*(*pos_ptr)[2]+t(0,3);
      b = t(1,0)*(*pos_ptr)[0]+t(1,1)*(*pos_ptr)[1]+t(1,2)*(*pos_ptr)[2]+t(1,3);
      c = t(2,0)*(*pos_ptr)[0]+t(2,1)*(*pos_ptr)[1]+t(2,2)*(*pos_ptr)[2]+t(2,3);
      (*pos_ptr)[0] = a;
      (*pos_ptr)[1] = b;
      (*pos_ptr)[2] = c;
      ++pos_ptr;
    }
  }

  void ApplyTransform(uint i, const geom::Mat4& t) {
    // no checking, it gets assumed the user knows what he does!
    // => dangerous for potential segfaults when the index is out of bounds!
    geom::Vec3 * pos_ptr = &n_coord;
    pos_ptr += i;
    Real a,b,c;
    a = t(0,0)*(*pos_ptr)[0]+t(0,1)*(*pos_ptr)[1]+t(0,2)*(*pos_ptr)[2]+t(0,3);
    b = t(1,0)*(*pos_ptr)[0]+t(1,1)*(*pos_ptr)[1]+t(1,2)*(*pos_ptr)[2]+t(1,3);
    c = t(2,0)*(*pos_ptr)[0]+t(2,1)*(*pos_ptr)[1]+t(2,2)*(*pos_ptr)[2]+t(2,3);
    (*pos_ptr)[0] = a;
    (*pos_ptr)[1] = b;
    (*pos_ptr)[2] = c;
    // NOTE: equivalent (but faster) to: *pos_ptr = geom::Vec3(t * (*pos_ptr))
  }

  geom::Mat4 GetTransform(const Backbone& other) const {
    core::EMatX3 pos_one = core::EMatX3::Zero(3, 3);
    core::EMatX3 pos_two = core::EMatX3::Zero(3, 3);

    core::EMatFillRow(pos_one, 0, n_coord);
    core::EMatFillRow(pos_one, 1, ca_coord);
    core::EMatFillRow(pos_one, 2, c_coord);

    core::EMatFillRow(pos_two, 0, other.n_coord);
    core::EMatFillRow(pos_two, 1, other.ca_coord);
    core::EMatFillRow(pos_two, 2, other.c_coord);

    return core::MinRMSDSuperposition(pos_one, pos_two);
  }

  geom::Mat4 GetTransform(const ost::mol::ResidueHandle& res) const {
    core::EMatX3 pos_one = core::EMatX3::Zero(3, 3);
    core::EMatX3 pos_two = core::EMatX3::Zero(3, 3);

    core::EMatFillRow(pos_one, 0, n_coord);
    core::EMatFillRow(pos_one, 1, ca_coord);
    core::EMatFillRow(pos_one, 2, c_coord);

    ost::mol::AtomHandle n = res.FindAtom("N");
    ost::mol::AtomHandle ca = res.FindAtom("CA");
    ost::mol::AtomHandle c = res.FindAtom("C");

    if (!(n.IsValid() && ca.IsValid() && c.IsValid())) {
      throw promod3::Error("Residue must have all backbone atoms present to "
                           "use it as a superposition target!");
    }

    core::EMatFillRow(pos_two, 0, n.GetPos());
    core::EMatFillRow(pos_two, 1, ca.GetPos());
    core::EMatFillRow(pos_two, 2, c.GetPos());

    return core::MinRMSDSuperposition(pos_one, pos_two);
  }

  bool operator==(const Backbone& rhs) const {
   return n_coord==rhs.n_coord && ca_coord==rhs.ca_coord &&
          c_coord==rhs.c_coord && o_coord==rhs.o_coord && 
          one_letter_code==rhs.one_letter_code && aa == rhs.aa;
  }  
  bool operator!=(const Backbone& rhs) const {
    return !this->operator==(rhs);
  }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertFixedSizedVector<float>(ds, n_coord, 3);
    core::ConvertFixedSizedVector<float>(ds, ca_coord, 3);
    core::ConvertFixedSizedVector<float>(ds, cb_coord, 3);
    core::ConvertFixedSizedVector<float>(ds, c_coord, 3);
    core::ConvertFixedSizedVector<float>(ds, o_coord, 3);
    ds & one_letter_code;
    if (ds.IsSource()) {
      aa = ost::conop::OneLetterCodeToAminoAcid(one_letter_code);
    }
  }
};

///////////////////////////////////////////////////////////////////////////////
class BackboneList {
public:
  // constructors
  BackboneList() { }
  BackboneList(const String& sequence);
  BackboneList(const String& sequence, const std::vector<Real>& phi_angles, 
               const std::vector<Real>& psi_angles);
  BackboneList(const String& sequence, const std::vector<Real>& phi_angles, 
               const std::vector<Real>& psi_angles,
               const std::vector<Real>& omega_angles);
  BackboneList(const ost::mol::ResidueHandleList& res_list);
  BackboneList(const String& sequence,
               const ost::mol::ResidueHandleList& res_list);
  // NOTE: default copy and assignment used!

  // convert to / insert into OST objects
  ost::img::MapHandle ToDensity(Real padding = 10.0,
                                geom::Vec3 sampling = geom::Vec3(1.0,1.0,1.0),
                                Real resolution = 3.0,
                                bool high_resolution = false) const;
  ost::mol::EntityHandle ToEntity() const;
  void InsertInto(ost::mol::ChainHandle& chain,
                  const ost::mol::ResNum& start_resnum) const;
  void InsertInto(ost::mol::ChainHandle& chain, uint start_resnum) const {
    InsertInto(chain, ost::mol::ResNum(start_resnum));
  }
  void InsertInto(ost::img::MapHandle& map,
                  Real resolution = 3.0,
                  bool high_resolution = false) const;
  geom::AlignedCuboid GetBounds(bool all_atom = true) const;

  // get whole sequence
  String GetSequence() const;
  void SetSequence(const String& seq);

  // extract subset from..to-1 of backbone
  BackboneList Extract(uint from, uint to) const;
  // replace part of backbone
  void ReplaceFragment(const BackboneList& frag, uint index,
                       bool superpose_stems);

  // backbone getters
  const geom::Vec3& GetN(uint idx) const { return bb_list_[idx].n_coord; }
  const geom::Vec3& GetCA(uint idx) const { return bb_list_[idx].ca_coord; }
  const geom::Vec3& GetCB(uint idx) const { return bb_list_[idx].cb_coord; }
  const geom::Vec3& GetC(uint idx) const { return bb_list_[idx].c_coord; }
  const geom::Vec3& GetO(uint idx) const { return bb_list_[idx].o_coord; }
  char GetOLC(uint idx) const { return bb_list_[idx].one_letter_code; }
  ost::conop::AminoAcid GetAA(uint idx) const { return bb_list_[idx].aa; }
  // backbone setters
  void SetN(uint idx, const geom::Vec3& pos) { bb_list_[idx].n_coord = pos; }
  void SetCA(uint idx, const geom::Vec3& pos) { bb_list_[idx].ca_coord = pos; }
  void SetCB(uint idx, const geom::Vec3& pos) { bb_list_[idx].cb_coord = pos; }
  void SetC(uint idx, const geom::Vec3& pos) { bb_list_[idx].c_coord = pos; }
  void SetO(uint idx, const geom::Vec3& pos) { bb_list_[idx].o_coord = pos; }
  void SetOLC(uint idx, char olc) { bb_list_[idx].one_letter_code = olc; }
  void SetAA(uint idx, ost::conop::AminoAcid aa) { bb_list_[idx].SetAA(aa); }
  // set full backbone (as push_back below)
  void Set(uint idx, const geom::Vec3& n, const geom::Vec3& ca,
           const geom::Vec3& cb, const geom::Vec3& c, const geom::Vec3& o,
           char olc) {
    bb_list_[idx] = Backbone(n, ca, cb, c, o, olc);
  }
  void Set(uint idx, const geom::Vec3& n, const geom::Vec3& ca,
           const geom::Vec3& c, const geom::Vec3& o, char olc) {
    bb_list_[idx] = Backbone(n, ca, c, o, olc);
  }
  void Set(uint idx, const ost::mol::ResidueHandle& res, char olc = '\0') {
    bb_list_[idx] = Backbone(res, olc);
  }

  // some functions to make it behave vector-ish
  size_t size() const { return bb_list_.size(); }
  void resize(size_t new_size) { bb_list_.resize(new_size); }
  bool empty() const { return bb_list_.empty(); }
  void push_back(const geom::Vec3& n, const geom::Vec3& ca, const geom::Vec3& cb,
                 const geom::Vec3& c, const geom::Vec3& o, char olc) {
    bb_list_.push_back(Backbone(n, ca, cb, c, o, olc));
  }
  // this will reconstruct CB pos
  void push_back(const geom::Vec3& n, const geom::Vec3& ca,
                 const geom::Vec3& c, const geom::Vec3& o, char olc) {
    bb_list_.push_back(Backbone(n, ca, c, o, olc));
  }
  // this will reconstruct CB pos if not existing
  void push_back(const ost::mol::ResidueHandle& res, char olc = '\0') {
    bb_list_.push_back(Backbone(res, olc));
  }
  void clear() { bb_list_.clear(); }
  void swap(BackboneList& other) { bb_list_.swap(other.bb_list_); }
  bool operator==(const BackboneList& other) const {
    return bb_list_ == other.bb_list_;
  }
  bool operator!=(const BackboneList& other) const {
    return !(other == (*this));
  }

  // reconstruct
  void ReconstructCBetaPositions();
  void ReconstructOxygenPositions(Real last_psi = -0.78540);
  void ReconstructCStemOxygen(const ost::mol::ResidueHandle& after_c_stem);

  // transform
  void ApplyTransform(uint index, const geom::Mat4& t) {
    bb_list_[index].ApplyTransform(t);
  }
  void ApplyTransform(uint from, uint to, const geom::Mat4& t) {
    for (uint i = from; i < to; ++i) ApplyTransform(i, t);
  }
  void ApplyTransform(const geom::Mat4& t) {
    ApplyTransform(0, size(), t);
  }
  void ApplyTransform(const geom::Transform& t) {
    ApplyTransform(0, size(), t.GetMatrix());
  }
  geom::Mat4 GetTransform(uint index, const BackboneList& other,
                          uint other_index) const {
    return bb_list_[index].GetTransform(other.bb_list_[other_index]);
  }
  geom::Mat4 GetTransform(uint index, const ost::mol::ResidueHandle& res) const {
    return bb_list_[index].GetTransform(res);
  }
  geom::Mat4 GetTransform(const BackboneList& other) const;
  void SuperposeOnto(const BackboneList& other) {
    ApplyTransform(GetTransform(other));
  }
  geom::Mat4 GetTransformIterative(const BackboneList& other, uint max_iterations,
                                   Real dist_thresh) const;
  void SuperposeOntoIterative(const BackboneList& other, uint max_iterations,
                              Real dist_thresh) {
    ApplyTransform(GetTransformIterative(other, max_iterations, dist_thresh));
  }

  // rotate torsions
  void RotateAroundPhiTorsion(uint index, Real angle, bool sequential=false);
  void RotateAroundPsiTorsion(uint index, Real angle, bool sequential=false);
  void RotateAroundOmegaTorsion(uint index, Real angle, bool sequential=false);
  void RotateAroundPhiPsiTorsion(uint index, Real phi, Real psi,
                                 bool sequential=false);

  // get torsions
  Real GetPhiTorsion(uint index) const {
    if (index < 1 || index >= size()) {
      throw promod3::Error("Cannot get phi torsion from this position.");
    }
    return geom::DihedralAngle(GetC(index-1), GetN(index),
                               GetCA(index), GetC(index));
  }
  Real GetPsiTorsion(uint index) const {
    if (index >= size()-1) {
      throw promod3::Error("Cannot get psi torsion from this position.");
    }
    return geom::DihedralAngle(GetN(index), GetCA(index),
                               GetC(index), GetN(index+1));
  }
  Real GetOmegaTorsion(uint index) const {
    if (index >= size()-1) {
      throw promod3::Error("Cannot get omega torsion from this position.");
    }
    return geom::DihedralAngle(GetCA(index), GetC(index), 
                               GetN(index+1), GetCA(index+1));
  }

  // set torsions
  void SetPhiTorsion(uint index, Real angle, bool sequential=false);
  void SetPsiTorsion(uint index, Real angle, bool sequential=false);
  void SetOmegaTorsion(uint index, Real angle, bool sequential=false);
  void SetPhiPsiTorsion(uint index, Real phi, Real psi, bool sequential=false);

  // check torsions
  bool TransOmegaTorsions(Real thresh = 20.0/180*M_PI,
                          bool allow_prepro_cis = true) const;

  //backrub motions
  void SetBackrub(uint index, Real primary_rot_angle, 
                  Real flanking_rot_angle_one, Real flanking_rot_angle_two);

  void SetBackrub(uint index, Real primary_rot, Real scaling = 1.0);

  // distance metrics
  Real MinCADistance(const BackboneList& other) const;
  Real CARMSD(const BackboneList& other, bool superposed_rmsd = false) const;
  Real RMSD(const BackboneList& other, bool superposed_rmsd = false) const;

  // helpers to reduce code redundancy
  static void ExtractEigenCAPos(const BackboneList& bb_list, 
                                core::EMatX3& pos);
  static void ExtractEigenNCACPos(const BackboneList& bb_list, 
                                  core::EMatX3& pos);
  static void ExtractEigenPos(const BackboneList& bb_list, 
                              core::EMatX3& pos);
                              
  // portable serialization
  template <typename DS>
  void Serialize(DS& ds) {
    ds & bb_list_;
  }

private:
  std::vector<Backbone> bb_list_;

  // helper to construct bb_list_ from angles
  void ConstructBackboneList_(const String& sequence, 
                              const std::vector<Real>& phi_angles,
                              const std::vector<Real>& psi_angles,
                              const std::vector<Real>& omega_angles);

  // helper to generate ideal angle for flanking rotation in Backrub
  Real ConstructFlankingAngle(const geom::Vec3& actual_o,
                              const geom::Vec3& target_o,
                              const geom::Vec3& anchor_ca,
                              const geom::Vec3& rot_axis) const;
};

}}

#endif

