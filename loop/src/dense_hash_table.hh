// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_DENSE_HASH_TABLE_HH
#define PROMOD_LOOP_DENSE_HASH_TABLE_HH

#include <memory> 
#include <fstream>
#include <algorithm>

#include <ost/dllexport.hh>
#include <boost/type_traits/is_fundamental.hpp>
#include <promod3/core/message.hh>
#include <promod3/core/portable_binary_serializer.hh>

/*
  Author: Marco Biasini
 */
namespace promod3 {

namespace detail {

template <typename T, typename C>
class  TEMPLATE_EXPORT DenseHashTableIterator {
public:
  typedef T ValueType;
  typedef DenseHashTableIterator<T, C> ClassType;
public:
  DenseHashTableIterator(C* tab, ValueType* start, ValueType* end, 
                         bool step_empty):
    tab_(tab), current_(start), end_(end)
  {
    if (step_empty)
      this->StepOverEmptyBuckets();
  }
  
  ValueType& operator*()
  {
    return *current_;
  }
  
  ValueType* operator->()
  {
    return &(this->operator*());
  }
  
  ClassType& operator++() {
    ++current_;
    this->StepOverEmptyBuckets();
    return *this;
  }
  bool operator==(const ClassType& rhs) const 
  { 
    return current_==rhs.current_; 
  }
  
  bool operator!=(const ClassType& rhs) const 
  { 
    return !(*this==rhs); 
  }  
private:
  void StepOverEmptyBuckets() {
    while (current_!=end_ && tab_->IsEmptyValue(*current_))
      ++current_;
  }
  C*         tab_;
  ValueType* current_;
  ValueType* end_;
};

}

/// \brief dense hash table implementation
/// 
/// The main reason for having our own implementation for a dense hash table is
/// to be able to serialize it efficiently. This is required, for example, when
/// reading table of contents from a binary file.
/// 
/// The implementation is based on google dense_hashtable but does not implement
/// any of the delete logic
/// 
/// The hash table uses an open adressing scheme with quadratic probing to 
/// resolve collisions
///
/// NOTE: this class is only fully functional after SetEmptyValue() is called.
/// -> otherwise only Read() or Serialize() can be called
template <typename T, typename K, typename H, typename G, typename E>  
class TEMPLATE_EXPORT DenseHashTable {
public:
  typedef T ValueType;
  typedef T& Reference;
  typedef const T& ConstReference;
  typedef DenseHashTable<T,K,H,G,E> ClassType;  
  typedef detail::DenseHashTableIterator<T, ClassType> Iterator;
  typedef detail::DenseHashTableIterator<const T, const ClassType> ConstIterator;     
  typedef H HashFunc;
  typedef G KeyExtractor;
  typedef E KeyComp;
  typedef K KeyType;
  friend class detail::DenseHashTableIterator<T, ClassType>;
  friend class detail::DenseHashTableIterator<const T, const ClassType>;
public:
  const static uint NOT_FOUND=uint(-1);  
  const static uint DEFAULT_NUM_OF_BUCKETS=32;
  const static uint MIN_NUM_OF_BUCKETS=4;  
  explicit DenseHashTable(uint expected_num_of_items=0, 
                          const HashFunc& hash_func=HashFunc(),
                          const KeyExtractor& key_extract=KeyExtractor(),
                          const KeyComp& key_eq=KeyComp()):
    hash_(hash_func), key_(key_extract), keys_equal_(key_eq),
    enlarge_resize_ratio_(0.8), table_(NULL),
    num_buckets_(this->MinSize(expected_num_of_items, 0)),
    num_items_(0)
  {
    this->ResetThresholds();
  }
  
  DenseHashTable(const ClassType& ht, uint wanted_size=0):
    hash_(ht.hash_), key_(ht.key_), keys_equal_(ht.keys_equal_), 
    enlarge_resize_ratio_(ht.enlarge_resize_ratio_), table_(NULL),
    num_buckets_(wanted_size>0 ? wanted_size : ht.num_buckets_),
    num_items_(0)
  {
    this->SetEmptyValue(ht.empty_value_);
    if(num_buckets_!=ht.num_buckets_) {
      for (typename ClassType::ConstIterator i=ht.Begin(),e=ht.End(); 
           i!=e; ++i) {
        this->DoInsert(*i);
      }
    } else {
      // cannot use memcpy since ValueType may be non-trivial-copyable
      // (e.g. std::pair is not!)
      std::copy(ht.table_, ht.table_ + num_buckets_, table_);
    }
    this->ResetThresholds();
  }
  
  std::pair<Iterator, bool> Insert(const ValueType& value)
  {
    this->EnsureSpace(1);
    return this->DoInsert(value);
  }
  Iterator Begin()
  {
    return Iterator(this, table_, table_+num_buckets_, true);
  }
  
  Iterator End()
  {
    return Iterator(this, table_+num_buckets_, table_+num_buckets_, false);
  }
  
  ConstIterator Begin()  const
  {
    return ConstIterator(this, table_, table_+num_buckets_, true);
  }
  
  ConstIterator End() const
  {
    return ConstIterator(this, table_+num_buckets_, table_+num_buckets_, false);
  }  
  uint Size() const 
  {
    return num_items_;
  }

  
  Iterator Find(const KeyType& key)
  {
    std::pair<uint, uint> pos=this->FindPosition(key);
    if (pos.first==NOT_FOUND) {
      return this->End();
    } 
    return Iterator(this, table_+pos.first, table_+num_buckets_, false);
  }
  
  ConstIterator Find(const KeyType& key) const
  {
    std::pair<uint, uint> pos=this->FindPosition(key);
    if (pos.first==NOT_FOUND) {
      return this->End();
    } 
    return ConstIterator(this, table_+pos.first, table_+num_buckets_, false);
  }  
  void SetEmptyValue(const ValueType& value)
  {
    empty_value_=value;
    empty_key_=key_(value);
    assert(table_==NULL);
    table_=(ValueType*)malloc(num_buckets_*sizeof(ValueType));
    this->FillRangeWithEmpty(table_, table_+num_buckets_);
  }
  ~DenseHashTable() 
  {
    if (table_) {
      free(table_);
      table_=NULL;
    }
  }
  void Swap(ClassType& ht) 
  {
    std::swap(hash_, ht.hash_);
    std::swap(keys_equal_, ht.keys_equal_);
    std::swap(key_, ht.key_);
    std::swap(enlarge_resize_ratio_, ht.enlarge_resize_ratio_);
    std::swap(empty_value_, ht.empty_value_);
    std::swap(empty_key_, ht.empty_key_);
    std::swap(table_, ht.table_);
    std::swap(num_buckets_, ht.num_buckets_);
    std::swap(num_items_, ht.num_items_);
    this->ResetThresholds();
    ht.ResetThresholds();
  }
  
  void Read(std::ifstream& in_stream)
  {
    in_stream.read(reinterpret_cast<char*>(&num_buckets_), sizeof(uint));
    in_stream.read(reinterpret_cast<char*>(&num_items_), sizeof(uint));    
    ValueType empty_val;    
    in_stream.read(reinterpret_cast<char*>(&empty_val), sizeof(ValueType));
    this->SetEmptyValue(empty_val);
    in_stream.read(reinterpret_cast<char*>(table_), 
                   sizeof(ValueType)*num_buckets_);
    this->ResetThresholds();
  }
  
  void Write(std::ofstream& out_stream)
  {
    out_stream.write(reinterpret_cast<char*>(&num_buckets_), sizeof(uint));
    out_stream.write(reinterpret_cast<char*>(&num_items_), sizeof(uint));    
    out_stream.write(reinterpret_cast<char*>(&empty_value_), sizeof(ValueType));
    out_stream.write(reinterpret_cast<char*>(table_), 
                      sizeof(ValueType)*num_buckets_);
  }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, num_buckets_);
    core::ConvertBaseType<uint32_t>(ds, num_items_);
    // special handling of empty value (ONLY SAFE IF non-base-type ValueType)
    if (boost::is_fundamental<ValueType>::value) {
      throw promod3::Error("DenseHashTable cannot serialize "
                           "fundamental ValueType.");
    }
    // NOTE: Serialize function must exist to read/write ValueType!
    if (ds.IsSource()) {
      // load
      ValueType empty_val; 
      ds & empty_val;
      this->SetEmptyValue(empty_val);
    } else {
      // save
      ds & empty_value_;
    }

    // value-by-value
    for (uint i = 0; i < num_buckets_; ++i) {
      ds & table_[i];
    }
    // finalize
    if (ds.IsSource()) this->ResetThresholds();
  }

  bool operator==(const DenseHashTable& rhs) const {
    bool equal = num_buckets_ == rhs.num_buckets_
              && num_items_ == rhs.num_items_
              && empty_value_ == rhs.empty_value_
              && enlarge_resize_ratio_ == rhs.enlarge_resize_ratio_
              && enlarge_threshold_ == rhs.enlarge_threshold_;
    if (!equal) return false;
    for (uint i = 0; i < num_buckets_; ++i) {
      equal = equal && table_[i] == rhs.table_[i];
    }
    return equal;
  }
  bool operator!=(const DenseHashTable& rhs) const {
    return !this->operator==(rhs);
  }

private:
  uint MinSize(uint num_of_items, uint min_num_of_buckets)
  {
    num_of_items=num_of_items==0 ? DEFAULT_NUM_OF_BUCKETS : num_of_items;
    uint buckets=MIN_NUM_OF_BUCKETS;
    while (buckets<min_num_of_buckets || 
           num_of_items>=buckets*enlarge_resize_ratio_) {
      buckets*=2;
    }
    return buckets;
  }
  void FillRangeWithEmpty(ValueType* start, ValueType* end)
  {
    std::uninitialized_fill(start, end, empty_value_);
  }
  void ResetThresholds()
  {
    enlarge_threshold_=static_cast<uint>(num_buckets_*enlarge_resize_ratio_);
  }
  
  void EnsureSpace(int delta)
  {
    if (delta+num_items_>=enlarge_threshold_) {
      ClassType resized(*this, num_buckets_*2);
      resized.Swap(*this);
    }
  }
      
  std::pair<uint, uint> FindPosition(const KeyType& key) const
  {
     // the number of buckets are required to be 2^n. By substracting one
     // from the number of buckets we get a bitfield that has set all bits
     // below to 1 and we can use that value for masking.
     uint buckets_minus_one=num_buckets_-1;
     uint bucket_index=hash_(key) & buckets_minus_one;
     uint num_probes=0;
     while (1) {
       if (keys_equal_(key_(table_[bucket_index]), empty_key_)) {
         return std::make_pair(NOT_FOUND, bucket_index);
       } else if (keys_equal_(key_(table_[bucket_index]), key)) {
         return std::make_pair(bucket_index, NOT_FOUND);
       }
       num_probes++;       
       uint jump=(num_probes*num_probes+num_probes) >> 1;
       bucket_index=(bucket_index+jump) & buckets_minus_one;
     }
  }
  std::pair<Iterator, bool> DoInsert(const T& value)
  {
    assert(table_);
    std::pair<uint, uint> pos=this->FindPosition(key_(value));
    if (pos.first!=NOT_FOUND) {
      return std::make_pair(Iterator(this, table_+pos.first, 
                                     table_+pos.first+num_buckets_, false), 
                            false);
    } else {
      ++num_items_;
      table_[pos.second]=value;
      return std::make_pair(Iterator(this, table_+pos.second, 
                                     table_+pos.second+num_buckets_, false), 
                            true);
    }
  }
protected:
  bool IsEmptyValue(const ValueType& value) const
  {
    return keys_equal_(key_(value), empty_key_);
  }
private:
  HashFunc        hash_;
  KeyExtractor    key_;
  KeyComp         keys_equal_;
  ValueType       empty_value_;
  KeyType         empty_key_;
  /// table that holds the hashed values
  float           enlarge_resize_ratio_;
  uint            enlarge_threshold_;
  T*              table_;  
  /// number of buckets, i.e. the length of the table array
  uint            num_buckets_;
  uint            num_items_;
};

}

#endif

