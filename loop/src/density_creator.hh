// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_LOOP_DENSITY_CREATOR_HH
#define PM3_LOOP_DENSITY_CREATOR_HH

#include <ost/img/map.hh>


namespace promod3 { namespace loop {

void FillDensityGaussianSpheres(const std::vector<geom::Vec3>& positions,
	                              const std::vector<Real>& molecular_weights,
	                              ost::img::MapHandle& map,
	                              Real resolution = 4.0,
	                              bool clear_map = false,
	                              bool high_resolution = false);



}} //namespace 

#endif
