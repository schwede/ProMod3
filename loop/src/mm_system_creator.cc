// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/mm_system_creator.hh>

#include <promod3/loop/hydrogen_constructor.hh>
#include <promod3/loop/idx_handler.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>

#include <set>
#include <cassert>

namespace promod3 { namespace loop {

namespace {

// last 2 args only relevant if non_contiguous = true
template<bool non_contiguous>
uint GetIndex_(uint idx, uint start_idx, uint start_idx2, uint split_idx) {
  if (non_contiguous) {
    if (idx < split_idx) return idx + start_idx;
    else                 return (idx - split_idx) + start_idx2;
  } else {
    return idx + start_idx;
  }
}

// last 2 args only relevant if non_contiguous = true
template<bool non_contiguous>
void AddConnectivity_(ost::mol::mm::TopologyPtr top, uint start_idx,
                      const ForcefieldConnectivity& connectivity,
                      const std::vector<Real>& atom_masses,
                      bool inaccurate_pot_energy,
                      uint start_idx2 = 0, uint split_idx = 0) {
  // need to keep track of exclusions while adding bonds / angles
  typedef ost::mol::mm::Index<2> ExclusionIndex;
  typedef std::set<ExclusionIndex> ExclusionSet;
  ExclusionSet exclusions;
  // add harmonic bonds
  for (uint i = 0; i < connectivity.harmonic_bonds.size(); ++i) {
    const ForcefieldBondInfo& info = connectivity.harmonic_bonds[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    const bool fixed_all = atom_masses[idx1] == 0 && atom_masses[idx2] == 0;
    if (!fixed_all && info.force_constant != 0.0) {
      top->AddHarmonicBond(idx1, idx2, info.bond_length, info.force_constant);
    }
    if (!fixed_all || !inaccurate_pot_energy) {
      exclusions.insert(ExclusionIndex(idx1, idx2));
    }
  }
  // add harmonic angles
  for(uint i = 0; i < connectivity.harmonic_angles.size(); ++i) {
    const ForcefieldHarmonicAngleInfo& info = connectivity.harmonic_angles[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    const uint idx3 = GetIndex_<non_contiguous>(info.index_three, start_idx,
                                                start_idx2, split_idx);
    const bool fixed_13 = atom_masses[idx1] == 0 && atom_masses[idx3] == 0;
    const bool fixed_all = fixed_13 && atom_masses[idx2] == 0;
    if (!fixed_all && info.force_constant != 0.0) {
      top->AddHarmonicAngle(idx1, idx2, idx3, info.angle, info.force_constant);
    }
    if (!fixed_13 || !inaccurate_pot_energy) {
      exclusions.insert(ExclusionIndex(idx1, idx3));
    }
  }
  // add urey bradley angles
  for(uint i = 0; i < connectivity.urey_bradley_angles.size(); ++i) {
    const ForcefieldUreyBradleyAngleInfo&
    info = connectivity.urey_bradley_angles[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    const uint idx3 = GetIndex_<non_contiguous>(info.index_three, start_idx,
                                                start_idx2, split_idx);
    const bool fixed_13 = atom_masses[idx1] == 0 && atom_masses[idx3] == 0;
    const bool fixed_all = fixed_13 && atom_masses[idx2] == 0;
    if (!fixed_all && (info.angle_force_constant != 0.0 ||
                       info.bond_force_constant != 0.0)) {
      top->AddUreyBradleyAngle(idx1, idx2, idx3,
                               info.angle, info.angle_force_constant,
                               info.bond_length, info.bond_force_constant);
    }
    if (!fixed_13 || !inaccurate_pot_energy) {
      exclusions.insert(ExclusionIndex(idx1, idx3));
    }
  }
  // add periodic dihedrals
  for(uint i = 0; i < connectivity.periodic_dihedrals.size(); ++i) {
    const ForcefieldPeriodicDihedralInfo& 
    info = connectivity.periodic_dihedrals[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    const uint idx3 = GetIndex_<non_contiguous>(info.index_three, start_idx,
                                                start_idx2, split_idx);
    const uint idx4 = GetIndex_<non_contiguous>(info.index_four, start_idx,
                                                start_idx2, split_idx);
    const bool fixed_all = atom_masses[idx1] == 0 && atom_masses[idx2] == 0
                        && atom_masses[idx3] == 0 && atom_masses[idx4] == 0;
    if (!fixed_all && info.force_constant != 0.0) {
      top->AddPeriodicDihedral(idx1, idx2, idx3, idx4, info.multiplicity,
                               info.phase, info.force_constant);
    }
  }
  // add periodic impropers
  for(uint i = 0; i < connectivity.periodic_impropers.size(); ++i) {
    const ForcefieldPeriodicDihedralInfo& 
    info = connectivity.periodic_impropers[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    const uint idx3 = GetIndex_<non_contiguous>(info.index_three, start_idx,
                                                start_idx2, split_idx);
    const uint idx4 = GetIndex_<non_contiguous>(info.index_four, start_idx,
                                                start_idx2, split_idx);
    const bool fixed_all = atom_masses[idx1] == 0 && atom_masses[idx2] == 0
                        && atom_masses[idx3] == 0 && atom_masses[idx4] == 0;
    if (!fixed_all && info.force_constant != 0.0) {
      top->AddPeriodicImproper(idx1, idx2, idx3, idx4, info.multiplicity,
                               info.phase, info.force_constant);
    }
  }
  // add harmonic impropers
  for(uint i = 0; i < connectivity.harmonic_impropers.size(); ++i) {
    const ForcefieldHarmonicImproperInfo& 
    info = connectivity.harmonic_impropers[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    const uint idx3 = GetIndex_<non_contiguous>(info.index_three, start_idx,
                                                start_idx2, split_idx);
    const uint idx4 = GetIndex_<non_contiguous>(info.index_four, start_idx,
                                                start_idx2, split_idx);
    const bool fixed_all = atom_masses[idx1] == 0 && atom_masses[idx2] == 0
                        && atom_masses[idx3] == 0 && atom_masses[idx4] == 0;
    if (!fixed_all && info.force_constant != 0.0) {
      top->AddHarmonicImproper(idx1, idx2, idx3, idx4,
                               info.angle, info.force_constant);
    }
  }
  // add lj pairs
  for(uint i = 0; i < connectivity.lj_pairs.size(); ++i) {
    const ForcefieldLJPairInfo& info = connectivity.lj_pairs[i];
    const uint idx1 = GetIndex_<non_contiguous>(info.index_one, start_idx,
                                                start_idx2, split_idx);
    const uint idx2 = GetIndex_<non_contiguous>(info.index_two, start_idx,
                                                start_idx2, split_idx);
    if (!inaccurate_pot_energy || atom_masses[idx1] != 0
                               || atom_masses[idx2] != 0) {
      top->AddLJPair(idx1, idx2, info.sigma, info.epsilon);
    }
  }
  // add exclusions
  for (ExclusionSet::iterator i = exclusions.begin(); i != exclusions.end();
       ++i) {
    top->AddExclusion((*i)[0], (*i)[1]);
  }
}

// Adds connectivity to topology
// -> usually it's assumed that atom index = start_idx + index in connectivity
// -> two start_idx can be provided if connectivity binds two residues which are
//    not contiguous in memory. Then indices are split: if < split_idx, it's as
//    before, else, we use start_idx2.
// -> we only put connectivity info as needed: fixed atoms (mass = 0) only need
//    exclusions and LJ to get a reasonable pot. energy and the latter can be
//    removed by setting inaccurate_pot_energy to true
void AddConnectivity(ost::mol::mm::TopologyPtr top, uint start_idx,
                     const ForcefieldConnectivity& connectivity,
                     const std::vector<Real>& atom_masses,
                     bool inaccurate_pot_energy) {
  AddConnectivity_<false>(top, start_idx, connectivity, atom_masses,
                          inaccurate_pot_energy);
}
void AddConnectivity(ost::mol::mm::TopologyPtr top, uint start_idx,
                     uint start_idx2, uint split_idx,
                     const ForcefieldConnectivity& connectivity,
                     const std::vector<Real>& atom_masses,
                     bool inaccurate_pot_energy) {
  AddConnectivity_<true>(top, start_idx, connectivity, atom_masses,
                         inaccurate_pot_energy, start_idx2, split_idx);
}

} // anon ns

/////////////////////////////////////////////////////////////////////

MmSystemCreator::MmSystemCreator(ForcefieldLookupPtr ff_lookup,
                                 bool fix_surrounding_hydrogens,
                                 bool kill_electrostatics,
                                 Real nonbonded_cutoff,
                                 bool inaccurate_pot_energy)
                                 : num_loop_residues_(0)
                                 , aa_lookup_(AminoAcidLookup::GetInstance()) {
  // store settings and check for CPU support
  ff_lookup_ = ff_lookup;
  fix_surrounding_hydrogens_ = fix_surrounding_hydrogens;
  kill_electrostatics_ = kill_electrostatics;
  nonbonded_cutoff_ = nonbonded_cutoff;
  inaccurate_pot_energy_ = inaccurate_pot_energy;
  SetCpuPlatformSupport(true);
}

MmSystemCreator::DisulfidBridgeVector
MmSystemCreator::GetDisulfidBridges(const AllAtomPositions& all_pos,
                                    const std::vector<uint>& res_indices) const
{
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "MmSystemCreator::GetDisulfidBridges", 2);
  // setup
  DisulfidBridgeVector disulfid_bridges;
  const uint num_residues = res_indices.size();
  // keep track of CYS and CYS2
  std::vector<uint> cys_indices;
  std::vector<bool> cys_bound;
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    const uint res_idx = res_indices[i_res];
    if (all_pos.GetAA(res_idx) == ost::conop::CYS) {
      bool is_bound = false;
      // check other CYS-SG within 2.5A (as in Modeller::GenerateDisulfidBonds)
      const geom::Vec3& pos = all_pos.GetPos(res_idx, CYS_SG_INDEX);
      for (uint i_cys = 0; i_cys < cys_indices.size(); ++i_cys) {
        if (!cys_bound[i_cys]) {
          const uint i_res2 = cys_indices[i_cys];
          const uint res_idx2 = res_indices[i_res2];
          const geom::Vec3& pos2 = all_pos.GetPos(res_idx2, CYS_SG_INDEX);
          if (geom::Distance(pos, pos2) < 2.5) {
            disulfid_bridges.push_back(std::make_pair(i_res, i_res2));
            cys_bound[i_cys] = true;
            is_bound = true;
            break;
          }
        }
      }
      // if nothing found, keep track of it
      if (!is_bound) {
        cys_indices.push_back(i_res);
        cys_bound.push_back(false);
      }
    }
  }
  return disulfid_bridges;
}

void MmSystemCreator::SetupSystem(const AllAtomPositions& all_pos,
                                  const std::vector<uint>& res_indices,
                                  uint loop_length,
                                  const std::vector<bool>& is_n_ter,
                                  const std::vector<bool>& is_c_ter,
                                  const DisulfidBridgeVector& disulfid_bridges)
{
  // call other one
  std::vector<uint> loop_start_indices(1, 0);
  std::vector<uint> loop_lengths(1, loop_length);
  SetupSystem(all_pos, res_indices, loop_start_indices, loop_lengths,
              is_n_ter, is_c_ter, disulfid_bridges);
}

void MmSystemCreator::SetupSystem(const AllAtomPositions& all_pos,
                                  const std::vector<uint>& res_indices,
                                  const std::vector<uint>& loop_start_indices,
                                  const std::vector<uint>& loop_lengths,
                                  const std::vector<bool>& is_n_ter,
                                  const std::vector<bool>& is_c_ter,
                                  const DisulfidBridgeVector& disulfid_bridges)
{
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "MmSystemCreator::SetupSystem", 2);

  // check data consistency
  const uint num_loops = loop_start_indices.size();
  if (num_loops != loop_lengths.size()) {
    throw promod3::Error("Sizes of loop_start_indices and loop_lengths must "
                         "match in MmSystemCreator::SetupSystem!");
  }
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    // loop from start_idx to start_idx + loop_lengths[i_loop] - 1
    const uint start_idx = loop_start_indices[i_loop];
    if (start_idx + loop_lengths[i_loop] > res_indices.size()) {
      throw promod3::Error("Loop indices out of bounds compared to res_indices "
                           "in MmSystemCreator::SetupSystem!");
    }
  }
  if (is_n_ter.size() != res_indices.size()) {
    throw promod3::Error("Sizes of res_indices and is_n_ter must match in "
                         "MmSystemCreator::SetupSystem!");
  }
  if (is_c_ter.size() != res_indices.size()) {
    throw promod3::Error("Sizes of res_indices and is_c_ter must match in "
                         "MmSystemCreator::SetupSystem!");
  }
  for (uint i_res = 0; i_res < res_indices.size(); ++i_res) {
    const uint res_idx = res_indices[i_res];
    all_pos.CheckResidueIndex(res_idx);
    if (!all_pos.IsAllSet(res_idx)) {
      throw promod3::Error("All heavy atoms must be defined to generate "
                           "MM system in MmSystemCreator::SetupSystem!");
    }
  }
  for (uint i = 0; i < disulfid_bridges.size(); ++i) {
    if (disulfid_bridges[i].first >= res_indices.size()) {
      throw promod3::Error("Invalid disulfid bridge residue index observed in "
                           "MmSystemCreator::SetupSystem!");
    }
    if (disulfid_bridges[i].second >= res_indices.size()) {
      throw promod3::Error("Invalid disulfid bridge residue index observed in "
                           "MmSystemCreator::SetupSystem!");
    }
  }

  // setup internal data (loop_..., is_X_ter_, ff_aa_, first_idx_)
  loop_start_indices_ = loop_start_indices;
  loop_lengths_ = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_, loop_lengths_, true);
  // note: ok to not have any loops here (should not bother anyone really...)
  num_loop_residues_ = 0;
  for (uint i_loop = 0; i_loop < loop_lengths_.size(); ++i_loop) {
    num_loop_residues_ += loop_lengths_[i_loop];
  }
  is_n_ter_ = is_n_ter;
  is_c_ter_ = is_c_ter;
  SetupNextIRes_(res_indices);
  SetupFfAa_(all_pos, res_indices, disulfid_bridges);
  SetupFirstIdx_();
  // setup topology
  SetupTopology_(disulfid_bridges);
  // setup positions
  SetupPositions_(all_pos, res_indices);
  // setup simulation object
  SetupSimulation_();
}

void MmSystemCreator::UpdatePositions(const AllAtomPositions& all_pos,
                                      const std::vector<uint>& res_indices) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "MmSystemCreator::UpdatePositions", 2);

  // check data consistency
  if (ff_aa_.size() != res_indices.size()) {
    throw promod3::Error("Inconsistent size of res_indices in "
                         "MmSystemCreator::SetupSystem!");
  }
  for (uint i_res = 0; i_res < res_indices.size(); ++i_res) {
    const uint res_idx = res_indices[i_res];
    all_pos.CheckResidueIndex(res_idx);
    if (all_pos.GetAA(res_idx) != ff_lookup_->GetAA(ff_aa_[i_res])) {
      throw promod3::Error("Inconsistent amino acid types observed in "
                           "MmSystemCreator::UpdatePositions!");
    }
    if (!all_pos.IsAllSet(res_idx)) {
      throw promod3::Error("All heavy atoms must be defined to update "
                           "positions in MmSystemCreator::UpdatePositions!");
    }
  }

  // overwrite positions and reset simulation
  SetupPositions_(all_pos, res_indices);
  simulation_->SetPositions(positions_);
}

void MmSystemCreator::ExtractLoopPositions(AllAtomPositions& loop_pos) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "MmSystemCreator::ExtractLoopPositions", 2);

  // check data consistency
  if (loop_pos.GetNumResidues() < num_loop_residues_) {
    throw promod3::Error("Output storage too small in "
                         "MmSystemCreator::ExtractLoopPositions!");
  }
  // check AA for all loops
  const uint num_loops = loop_start_indices_.size();
  uint loop_pos_idx = 0;
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    // loop from start_idx to start_idx + loop_lengths_[i_loop] - 1
    const uint start_idx = loop_start_indices_[i_loop];
    for (uint loop_idx = 0; loop_idx < loop_lengths_[i_loop]; ++loop_idx) {
      const uint i_res = start_idx + loop_idx;
      if (loop_pos.GetAA(loop_pos_idx) != ff_lookup_->GetAA(ff_aa_[i_res])) {
        throw promod3::Error("Inconsistent amino acid types observed in "
                             "MmSystemCreator::ExtractLoopPositions!");
      }
      ++loop_pos_idx;
    }
  }

  // get from simulation
  positions_ = simulation_->GetPositions();

  // fill heavy atoms from each loop
  loop_pos_idx = 0;
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    const uint start_idx = loop_start_indices_[i_loop];
    for (uint loop_idx = 0; loop_idx < loop_lengths_[i_loop]; ++loop_idx) {
      const uint i_res = start_idx + loop_idx;
      const ForcefieldAminoAcid ff_aa = ff_aa_[i_res];
      const uint first_idx = first_idx_[i_res];
      const ost::conop::AminoAcid aa = ff_lookup_->GetAA(ff_aa);
      // get heavy atoms
      for (uint i = 0; i < aa_lookup_.GetNumAtoms(aa); ++i) {
        const uint idx = ff_lookup_->GetHeavyIndex(ff_aa, i);
        loop_pos.SetPos(loop_pos_idx, i, positions_[first_idx + idx]);
      }
      ++loop_pos_idx;
    }
  }
}

void MmSystemCreator::ExtractLoopPositions(AllAtomPositions& out_pos,
                                           const std::vector<uint>& res_indices)
{
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "MmSystemCreator::ExtractLoopPositions", 2);

  // check data consistency
  const uint num_loops = loop_start_indices_.size();
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    // loop from start_idx to start_idx + loop_lengths_[i_loop] - 1
    const uint start_idx = loop_start_indices_[i_loop];
    // check res_indices
    if (start_idx + loop_lengths_[i_loop] > res_indices.size()) {
        throw promod3::Error("Too few residue indices passed in "
                             "MmSystemCreator::ExtractLoopPositions!");
    }
    // check AA
    for (uint loop_idx = 0; loop_idx < loop_lengths_[i_loop]; ++loop_idx) {
      const uint i_res = start_idx + loop_idx;
      const uint res_idx = res_indices[i_res];
      out_pos.CheckResidueIndex(res_idx);
      if (out_pos.GetAA(res_idx) != ff_lookup_->GetAA(ff_aa_[i_res])) {
        throw promod3::Error("Inconsistent amino acid types observed in "
                             "MmSystemCreator::ExtractLoopPositions!");
      }
    }
  }

  // get from simulation
  positions_ = simulation_->GetPositions();

  // fill heavy atoms from each loop
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    const uint start_idx = loop_start_indices_[i_loop];
    for (uint loop_idx = 0; loop_idx < loop_lengths_[i_loop]; ++loop_idx) {
      const uint i_res = start_idx + loop_idx;
      const uint res_idx = res_indices[i_res];
      const ForcefieldAminoAcid ff_aa = ff_aa_[i_res];
      const uint first_idx = first_idx_[i_res];
      const ost::conop::AminoAcid aa = ff_lookup_->GetAA(ff_aa);
      // get heavy atoms
      for (uint i = 0; i < aa_lookup_.GetNumAtoms(aa); ++i) {
        const uint idx = ff_lookup_->GetHeavyIndex(ff_aa, i);
        out_pos.SetPos(res_idx, i, positions_[first_idx + idx]);
      }
    }
  }
}

void MmSystemCreator::SetCpuPlatformSupport(bool cpu_platform_support) {
  if (cpu_platform_support) {
    // check if it's possible
    using namespace ost::mol;
    mm::SettingsPtr settings(new mm::Settings);
    settings->platform = mm::CPU;
    cpu_platform_support_ = mm::Simulation::IsPlatformAvailable(settings);
  } else {
    cpu_platform_support_ = false;
  }
}

void MmSystemCreator::SetupNextIRes_(const std::vector<uint>& res_indices) {

  // map res. indices to internal indexing
  const uint num_residues = res_indices.size();
  std::map<uint, uint> idx_map;
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    idx_map[res_indices[i_res]] = i_res;
  }
  // setup neighbors
  next_i_res_.resize(num_residues);
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    // check only relevant if we are not C terminal
    if (is_c_ter_[i_res]) {
      next_i_res_[i_res] = -1;
    } else {
      // look for next one
      const uint next_res_idx = res_indices[i_res] + 1;
      std::map<uint, uint>::const_iterator found = idx_map.find(next_res_idx);
      if (found == idx_map.end()) next_i_res_[i_res] = -1;
      else                        next_i_res_[i_res] = found->second;
    }
  }
}

void MmSystemCreator::SetupFfAa_(const AllAtomPositions& all_pos,
                                 const std::vector<uint>& res_indices,
                                 const DisulfidBridgeVector& disulfid_bridges) {
  // setup
  const uint num_residues = res_indices.size();
  // get base ff_aa_
  ff_aa_.resize(num_residues);
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    const uint res_idx = res_indices[i_res];
    ff_aa_[i_res] = ForcefieldAminoAcid(all_pos.GetAA(res_idx));
  }
  // fix disulfid bridges
  for (uint i = 0; i < disulfid_bridges.size(); ++i) {
    ff_aa_[disulfid_bridges[i].first] = FF_CYS2;
    ff_aa_[disulfid_bridges[i].second] = FF_CYS2;
  }
}

void MmSystemCreator::SetupFirstIdx_() {
  // setup
  const uint num_residues = ff_aa_.size();
  // fill first indices
  first_idx_.resize(num_residues+1);
  uint num_atoms = 0;
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    first_idx_[i_res] = num_atoms;
    num_atoms += ff_lookup_->GetNumAtoms(ff_aa_[i_res], is_n_ter_[i_res],
                                         is_c_ter_[i_res]);
  }
  first_idx_[num_residues] = num_atoms;
}

void
MmSystemCreator::SetupTopology_(const DisulfidBridgeVector& disulfid_bridges) {
  // setup
  const uint num_residues = ff_aa_.size();
  const uint num_atoms = first_idx_.back();
  assert(is_n_ter_.size() == num_residues);
  assert(is_c_ter_.size() == num_residues);
  assert(first_idx_.size() == num_residues+1);

  // get per atom data
  std::vector<Real> atom_masses(num_atoms);
  std::vector<Real> atom_charges(num_atoms, 0);
  std::vector<Real> atom_sigmas(num_atoms);
  std::vector<Real> atom_epsilons(num_atoms);
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    const ForcefieldAminoAcid ff_aa = ff_aa_[i_res];
    const bool is_nter = is_n_ter_[i_res];
    const bool is_cter = is_c_ter_[i_res];
    const uint first_idx = first_idx_[i_res];
    // masses
    const std::vector<Real>&
    masses = ff_lookup_->GetMasses(ff_aa, is_nter, is_cter);
    std::copy(masses.begin(), masses.end(), &atom_masses[first_idx]);
    // loop check
    const uint i_loc = GetIndexLocation_(i_res);
    if ((i_loc & ON_N_STEM) && !is_nter) {
      // fix N-stem (only fix N, CA, CB)
      const uint idx_N = ff_lookup_->GetHeavyIndex(ff_aa, BB_N_INDEX);
      atom_masses[first_idx + idx_N] = 0;
      const uint idx_CA = ff_lookup_->GetHeavyIndex(ff_aa, BB_CA_INDEX);
      atom_masses[first_idx + idx_CA] = 0;
      if (ff_aa != FF_GLY) {
        const uint idx_CB = ff_lookup_->GetHeavyIndex(ff_aa, BB_CB_INDEX);
        atom_masses[first_idx + idx_CB] = 0;
      }
    }
    if ((i_loc & ON_C_STEM) && !is_cter) {
      // fix C-stem (only fix CA, CB, C, O)
      const uint idx_CA = ff_lookup_->GetHeavyIndex(ff_aa, BB_CA_INDEX);
      atom_masses[first_idx + idx_CA] = 0;
      const uint idx_C = ff_lookup_->GetHeavyIndex(ff_aa, BB_C_INDEX);
      atom_masses[first_idx + idx_C] = 0;
      const uint idx_O = ff_lookup_->GetHeavyIndex(ff_aa, BB_O_INDEX);
      atom_masses[first_idx + idx_O] = 0;
      if (ff_aa != FF_GLY) {
        const uint idx_CB = ff_lookup_->GetHeavyIndex(ff_aa, BB_CB_INDEX);
        atom_masses[first_idx + idx_CB] = 0;
      }
    }
    if (i_loc == OUT_OF_LOOP) {
      // fix surrounding (either all or only heavy atoms)
      if (fix_surrounding_hydrogens_) {
        std::fill_n(&atom_masses[first_idx], masses.size(), Real(0));
      } else {
        const ost::conop::AminoAcid aa = ff_lookup_->GetAA(ff_aa);
        for (uint i = 0; i < aa_lookup_.GetNumAtoms(aa); ++i) {
          const uint idx = ff_lookup_->GetHeavyIndex(ff_aa, i);
          atom_masses[first_idx + idx] = 0;
        }
      }
    }
    // charges (if desired)
    if (!kill_electrostatics_) {
      const std::vector<Real>&
      charges = ff_lookup_->GetCharges(ff_aa, is_nter, is_cter);
      std::copy(charges.begin(), charges.end(), &atom_charges[first_idx]);
    }
    // LJ params
    const std::vector<Real>&
    sigmas = ff_lookup_->GetSigmas(ff_aa, is_nter, is_cter);
    std::copy(sigmas.begin(), sigmas.end(), &atom_sigmas[first_idx]);
    const std::vector<Real>&
    epsilons = ff_lookup_->GetEpsilons(ff_aa, is_nter, is_cter);
    std::copy(epsilons.begin(), epsilons.end(), &atom_epsilons[first_idx]);
  }

  // setup topology
  top_.reset(new ost::mol::mm::Topology(atom_masses));
  top_->SetCharges(atom_charges);
  top_->SetSigmas(atom_sigmas);
  top_->SetEpsilons(atom_epsilons);
  top_->SetFudgeQQ(ff_lookup_->GetFudgeQQ());
  top_->SetFudgeLJ(ff_lookup_->GetFudgeLJ());

  // add internal and peptide connectivity
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    // get data
    const ForcefieldAminoAcid ff_aa = ff_aa_[i_res];
    const bool is_nter = is_n_ter_[i_res];
    const bool is_cter = is_c_ter_[i_res];
    const uint first_idx = first_idx_[i_res];
    const ForcefieldConnectivity& connectivity
     = ff_lookup_->GetInternalConnectivity(ff_aa, is_nter, is_cter);
    AddConnectivity(top_, first_idx, connectivity, atom_masses,
                    inaccurate_pot_energy_);
    // peptide bond only if we have neighbor
    const int i_res2i = next_i_res_[i_res];
    if (i_res2i >= 0) {
      const uint i_res2 = i_res2i;
      const bool next_is_cter = is_c_ter_[i_res2];
      const ForcefieldAminoAcid ff_aa2 = ff_aa_[i_res2];
      const ForcefieldConnectivity& connectivity
       = ff_lookup_->GetPeptideBoundConnectivity(ff_aa, ff_aa2, is_nter,
                                                 next_is_cter);
      // can do simpler indexing if the residues are contiguous
      if (i_res2 == i_res+1) {
        AddConnectivity(top_, first_idx, connectivity, atom_masses,
                        inaccurate_pot_energy_);
      } else {
        const uint split_idx = ff_lookup_->GetNumAtoms(ff_aa, is_nter, is_cter);
        const uint first_idx2 = first_idx_[i_res2];
        AddConnectivity(top_, first_idx, first_idx2, split_idx,
                        connectivity, atom_masses, inaccurate_pot_energy_);
      }
    }
  }

  // add disulfid bridges
  for (uint i = 0; i < disulfid_bridges.size(); ++i) {
    const uint split_idx = ff_lookup_->GetNumAtoms(FF_CYS2, false, false);
    const uint i_res1 = disulfid_bridges[i].first;
    const uint i_res2 = disulfid_bridges[i].second;
    const uint first_idx1 = first_idx_[i_res1];
    const uint first_idx2 = first_idx_[i_res2];
    AddConnectivity(top_, first_idx1, first_idx2, split_idx,
                    ff_lookup_->GetDisulfidConnectivity(),
                    atom_masses, inaccurate_pot_energy_);
  }
}

void MmSystemCreator::SetupPositions_(const AllAtomPositions& all_pos,
                                      const std::vector<uint>& res_indices) {
  // setup
  const uint num_residues = res_indices.size();
  const uint num_atoms = first_idx_.back();
  assert(ff_aa_.size() == num_residues);
  assert(is_n_ter_.size() == num_residues);
  assert(is_c_ter_.size() == num_residues);
  assert(first_idx_.size() == num_residues+1);

  // fill data
  HydrogenStorage hydrogens;
  positions_.resize(num_atoms);
  for (uint i_res = 0; i_res < num_residues; ++i_res) {
    const uint res_idx = res_indices[i_res];
    const ForcefieldAminoAcid ff_aa = ff_aa_[i_res];
    const bool is_nter = is_n_ter_[i_res];
    const bool is_cter = is_c_ter_[i_res];
    const uint first_idx = first_idx_[i_res];
    const ost::conop::AminoAcid aa = ff_lookup_->GetAA(ff_aa);
    // get heavy atoms
    for (uint i = 0; i < aa_lookup_.GetNumAtoms(aa); ++i) {
      const uint idx = ff_lookup_->GetHeavyIndex(ff_aa, i);
      positions_[first_idx + idx] = all_pos.GetPos(res_idx, i);
    }
    // check prot. state
    HisProtonationState his_prot_state = PROT_STATE_HISE;
    if (ff_aa == FF_HISD) his_prot_state = PROT_STATE_HISD;
    // get hydrogens
    ConstructHydrogens(all_pos, res_idx, hydrogens, false, his_prot_state);
    // either N-terminal, or use prev. C (if there) or else helical HN
    if (is_nter) {
      ConstructHydrogenNTerminal(all_pos, res_idx, hydrogens);
    } else if (res_idx > 0 && all_pos.IsSet(res_idx-1, BB_C_INDEX)) {
      const geom::Vec3& prev_c_pos = all_pos.GetPos(res_idx-1, BB_C_INDEX);
      ConstructHydrogenN(all_pos, res_idx, prev_c_pos, hydrogens);
    } else {
      ConstructHydrogenN(all_pos, res_idx, -1.0472, hydrogens);
    }
    // CYS2 -> remove HG -> copy rest into positions
    if (ff_aa == FF_CYS2) hydrogens.ClearPos(CYS_HG_INDEX);
    for (uint i = 0; i < aa_lookup_.GetNumHydrogens(aa); ++i) {
      if (hydrogens.IsSet(i)) {
        const uint idx = ff_lookup_->GetHydrogenIndex(ff_aa, i);
        positions_[first_idx + idx] = hydrogens.GetPos(i);
      }
    }
    // add OXT
    if (is_cter) {
      const uint o_idx = ff_lookup_->GetHeavyIndex(ff_aa, BB_O_INDEX);
      const uint oxt_idx = ff_lookup_->GetOXTIndex(ff_aa, is_nter);
      core::ConstructCTerminalOxygens(all_pos.GetPos(res_idx, BB_C_INDEX),
                                      all_pos.GetPos(res_idx, BB_CA_INDEX),
                                      all_pos.GetPos(res_idx, BB_N_INDEX),
                                      positions_[first_idx + o_idx],
                                      positions_[first_idx + oxt_idx]);
    }
  }
}

void MmSystemCreator::SetupSimulation_() {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "MmSystemCreator::SetupSimulation_", 2);

  // setup dummy entity for simulation
  ost::mol::EntityHandle dummy_ent = ost::mol::CreateEntity();
  ost::mol::XCSEditor ed = dummy_ent.EditXCS(ost::mol::BUFFERED_EDIT);
  ost::mol::ChainHandle dummy_chain = ed.InsertChain("A"); 
  ost::mol::ResidueHandle dummy_res = ed.AppendResidue(dummy_chain, "RES");
  for (uint i = 0; i < positions_.size(); ++i) {
    ed.InsertAtom(dummy_res, "A", positions_[i]);
  }
  ed.UpdateICS();

  // setup simulation
  ost::mol::mm::SettingsPtr settings(new ost::mol::mm::Settings);
  settings->kill_electrostatics = kill_electrostatics_;
  if (nonbonded_cutoff_ < 0) {
    settings->nonbonded_method = ost::mol::mm::NoCutoff;
  } else {
    settings->nonbonded_method = ost::mol::mm::CutoffNonPeriodic;
    settings->nonbonded_cutoff = nonbonded_cutoff_;
  }
  // set platform (checked in constructor)
  if (cpu_platform_support_) {
    // setup CPU
    settings->platform = ost::mol::mm::CPU;
    char* num_cpu_threads = getenv("PM3_OPENMM_CPU_THREADS");
    if (num_cpu_threads) {
      settings->cpu_properties["CpuThreads"] = num_cpu_threads;
    } else {
      settings->cpu_properties["CpuThreads"] = "1";
    }
  } else {
    // fallback to reference
    settings->platform = ost::mol::mm::Reference;
  }
  simulation_.reset(new ost::mol::mm::Simulation(top_, dummy_ent, settings));
}

}} // ns
