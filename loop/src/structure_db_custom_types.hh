// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_STRUCTURE_DB_CUSTOM_TYPES
#define PROMOD_LOOP_STRUCTURE_DB_CUSTOM_TYPES

#include <promod3/loop/ushort_vec.hh>
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3 { namespace loop {

struct PeptideCoords {
  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    ds & n;
    ds & ca;
    ds & c;
    ds & o;
  }

  bool operator==(const PeptideCoords& rhs) const {
    return n == rhs.n
        && ca == rhs.ca
        && c == rhs.c
        && o == rhs.o;
  }
  bool operator!=(const PeptideCoords& rhs) const {
    return !this->operator==(rhs);
  }

  UShortVec n;
  UShortVec ca;
  UShortVec c;
  UShortVec o;
};


struct DihedralInfo {
  DihedralInfo() { }
  DihedralInfo(Real a, Real b){
    phi = a * 1000;
    psi = b * 1000;
  }

  inline Real GetPhi() const { return static_cast<Real>(phi) * Real(0.001); }

  inline Real GetPsi() const { return static_cast<Real>(psi) * Real(0.001); }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertBaseType<int16_t>(ds, phi);
    core::ConvertBaseType<int16_t>(ds, psi);
  }

  bool operator==(const DihedralInfo& rhs) const {
    return phi == rhs.phi
        && psi == rhs.psi;
  }
  bool operator!=(const DihedralInfo& rhs) const {
    return !this->operator==(rhs);
  }

  short phi;
  short psi;
};


struct AAFreq {

  AAFreq() {
    memset(freqs, 0, sizeof(short)*20);
  }

  void AssignProfileColumn(const ost::seq::ProfileColumn& col) {
    const Real* col_freq_ptr = col.freqs_begin();
    short* freq_ptr = &freqs[0];
    for(uint i = 0; i < 20; ++i){
      *freq_ptr = (*col_freq_ptr)*10000;
      ++freq_ptr;
      ++col_freq_ptr;
    } 
  }

  const short* data() const { return freqs; }

  short* data() { return freqs; }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    for (int i = 0; i < 20; ++i) {
      core::ConvertBaseType<int16_t>(ds, freqs[i]);
    }
  }

  bool operator==(const AAFreq& rhs) const {
    return !memcmp(freqs, rhs.freqs, sizeof(short)*20);
  }
  bool operator!=(const AAFreq& rhs) const {
    return !this->operator==(rhs);
  }
  
  short freqs[20];
};

}} // ns

#endif
