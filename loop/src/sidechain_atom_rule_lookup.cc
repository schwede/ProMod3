// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/sidechain_atom_rule_lookup.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <ost/conop/amino_acids.hh>
#include <promod3/core/message.hh>

namespace promod3 { namespace loop{


ChiDefinition SidechainAtomRuleLookup::GetChiDefinition(ost::conop::AminoAcid aa, 
                                                     uint chi_idx) const{
  if(chi_idx >= chi_definitions_[aa].size()){
    String err = "Specified sidechain dihedral does not exist for ";
    err += (ost::conop::AminoAcidToResidueName(aa) + "!");
    throw promod3::Error(err);
  }
  return chi_definitions_[aa][chi_idx];
}

void SidechainAtomRuleLookup::AddRule(ost::conop::AminoAcid aa, 
                                      uint sidechain_atom_idx,
                                      uint anch_1, uint anch_2, uint anch_3, 
                                      Real bond_length, Real angle, 
                                      uint dihedral_idx, 
                                      Real base_dihedral){
  SidechainAtomRule rule;
  rule.sidechain_atom_idx = sidechain_atom_idx;
  rule.anchor_idx[0] = anch_1;
  rule.anchor_idx[1] = anch_2;
  rule.anchor_idx[2] = anch_3;
  rule.bond_length = bond_length;
  rule.angle = angle;
  rule.dihedral_idx = dihedral_idx;
  rule.base_dihedral = base_dihedral;
  rules_[aa].push_back(rule);
}

void SidechainAtomRuleLookup::AddChiDefinition(ost::conop::AminoAcid aa,
                                           uint idx_one, uint idx_two, 
                                           uint idx_three, uint idx_four){
  ChiDefinition chi_definition;
  chi_definition.idx_one = idx_one;
  chi_definition.idx_two = idx_two;
  chi_definition.idx_three = idx_three;
  chi_definition.idx_four = idx_four;
  chi_definitions_[aa].push_back(chi_definition);
}

SidechainAtomRuleLookup::SidechainAtomRuleLookup(){
    
  // ARG
  AddRule(ost::conop::ARG, 5, 0, 1, 4,
          Real(1.5209), Real(1.9872), 0, Real(0.0)); //CG
  AddRule(ost::conop::ARG, 6, 1, 4, 5,
          Real(1.5220), Real(1.9507), 1, Real(0.0)); // CD
  AddRule(ost::conop::ARG, 7, 4, 5, 6,
          Real(1.4604), Real(1.9486), 2, Real(0.0)); // NE
  AddRule(ost::conop::ARG, 8, 5, 6, 7,
          Real(1.3304), Real(2.1771), 3, Real(0.0)); // CZ
  AddRule(ost::conop::ARG, 9, 6, 7, 8,
          Real(1.3287), Real(2.1053), 4, Real(0.0)); // NH1
  AddRule(ost::conop::ARG, 10, 6, 7, 8,
          Real(1.3272), Real(2.0893), 4, Real(3.14159265359)); // NH2

  // ASN
  AddRule(ost::conop::ASN, 5, 0, 1, 4,
          Real(1.5154), Real(1.9661), 0, Real(0.0)); // CG
  AddRule(ost::conop::ASN, 6, 1, 4, 5,
          Real(1.2329), Real(2.1098), 1, Real(0.0)); // OD1
  AddRule(ost::conop::ASN, 7, 1, 4, 5,
          Real(1.3272), Real(2.0328), 1, Real(3.14159265359)); // ND2

  // ASP
  AddRule(ost::conop::ASP, 5, 0, 1, 4,
          Real(1.5192), Real(1.9737), 0, Real(0.0)); // CG
  AddRule(ost::conop::ASP, 6, 1, 4, 5,
          Real(1.2505), Real(2.0798), 1, Real(0.0)); // OD1
  AddRule(ost::conop::ASP, 7, 1, 4, 5,
          Real(1.2508), Real(2.0593), 1, Real(3.14159265359)); // OD2

  // GLN
  AddRule(ost::conop::GLN, 5, 0, 1, 4,
          Real(1.5220), Real(1.9861), 0, Real(0.0)); // CG
  AddRule(ost::conop::GLN, 6, 1, 4, 5,
          Real(1.5169), Real(1.9674), 1, Real(0.0)); // CD
  AddRule(ost::conop::GLN, 7, 4, 5, 6,
          Real(1.2341), Real(2.1112), 2, Real(0.0)); // OE1
  AddRule(ost::conop::GLN, 8, 4, 5, 6,
          Real(1.3280), Real(2.0319), 2, Real(3.14159265359)); // NE2

  // GLU
  AddRule(ost::conop::GLU, 5, 0, 1, 4,
          Real(1.5215), Real(1.9869), 0, Real(0.0)); // CG
  AddRule(ost::conop::GLU, 6, 1, 4, 5,
          Real(1.5212), Real(1.9786), 1, Real(0.0)); // CD
  AddRule(ost::conop::GLU, 7, 4, 5, 6,
          Real(1.2522), Real(2.0762), 2, Real(0.0)); // OE1
  AddRule(ost::conop::GLU, 8, 4, 5, 6,
          Real(1.2516), Real(2.0610), 2, Real(3.14159265359)); // OE2

  // LYS
  AddRule(ost::conop::LYS, 5, 0, 1, 4,
          Real(1.5217), Real(1.9903), 0, Real(0.0)); // CG
  AddRule(ost::conop::LYS, 6, 1, 4, 5,
          Real(1.5230), Real(1.9488), 1, Real(0.0)); // CD
  AddRule(ost::conop::LYS, 7, 4, 5, 6,
          Real(1.5215), Real(1.9493), 2, Real(0.0)); // CE
  AddRule(ost::conop::LYS, 8, 5, 6, 7,
          Real(1.4922), Real(1.9498), 3, Real(0.0)); // NZ

  // SER
  AddRule(ost::conop::SER, 5, 0, 1, 4,
          Real(1.4171), Real(1.9335), 0, Real(0.0)); // OG

  // CYS
  AddRule(ost::conop::CYS, 5, 0, 1, 4,
          Real(1.8072), Real(1.9860), 0, Real(0.0)); // SG

  // MET
  AddRule(ost::conop::MET, 5, 0, 1, 4,
          Real(1.5199), Real(1.9856), 0, Real(0.0)); // CG
  AddRule(ost::conop::MET, 6, 1, 4, 5,
          Real(1.8063), Real(1.9668), 1, Real(0.0)); // SD
  AddRule(ost::conop::MET, 7, 4, 5, 6,
          Real(1.7868), Real(1.7561), 2, Real(0.0)); // CE

  // TRP
  AddRule(ost::conop::TRP, 5, 0, 1, 4,
          Real(1.4986), Real(1.9896), 0, Real(0.0)); // CG
  AddRule(ost::conop::TRP, 6, 1, 4, 5,
          Real(1.3674), Real(2.2179), 1, Real(0.0)); // CD1
  AddRule(ost::conop::TRP, 7, 1, 4, 5,
          Real(1.4330), Real(2.2097), 1, Real(3.14159265359)); // CD2
  AddRule(ost::conop::TRP, 8, 6, 5, 7,
          Real(1.4127), Real(1.8710), 4, Real(0.0)); // CE2
  AddRule(ost::conop::TRP, 9, 4, 5, 6,
          Real(1.3749), Real(1.9219), 4, Real(3.14159265359)); // NE1
  AddRule(ost::conop::TRP, 10, 6, 5, 7,
          Real(1.4001), Real(2.3370), 4, Real(3.14159265359)); // CE3
  AddRule(ost::conop::TRP, 11, 8, 7, 10,
          Real(1.3882), Real(2.0715), 4, Real(0.0)); // CZ3
  AddRule(ost::conop::TRP, 12, 7, 10, 11,
          Real(1.4025), Real(2.1130), 4, Real(0.0)); // CH2
  AddRule(ost::conop::TRP, 13, 10, 11, 12,
          Real(1.3714), Real(2.1213), 4, Real(0.0)); // CZ2

  // TYR
  AddRule(ost::conop::TYR, 5, 0, 1, 4,
          Real(1.5104), Real(1.9853), 0, Real(0.0)); // CG
  AddRule(ost::conop::TYR, 6, 1, 4, 5,
          Real(1.3910), Real(2.1111), 1, Real(0.0)); // CD1
  AddRule(ost::conop::TYR, 7, 1, 4, 5,
          Real(1.3903), Real(2.1093), 1, Real(3.14159265359)); // CD2
  AddRule(ost::conop::TYR, 8, 7, 5, 6,
          Real(1.3888), Real(2.1144), 4, Real(0.0)); // CE1
  AddRule(ost::conop::TYR, 9, 6, 5, 7,
          Real(1.3885), Real(2.1147), 4, Real(0.0)); // CE2
  AddRule(ost::conop::TYR, 10, 5, 6, 8,
          Real(1.3814), Real(2.0866), 4, Real(0.0)); // CZ
  AddRule(ost::conop::TYR, 11, 7, 9, 10,
          Real(1.3771), Real(2.0909), 4, Real(3.14159265359)); // OH

  // THR
  AddRule(ost::conop::THR, 5, 0, 1, 4,
          Real(1.4323), Real(1.9059), 0, Real(0.0)); // OG1
  AddRule(ost::conop::THR, 6, 5, 1, 4,
          Real(1.5239), Real(1.9412), 4, Real(-2.1068)); // CG2

  // VAL
  AddRule(ost::conop::VAL, 5, 0, 1, 4,
          Real(1.5262), Real(1.9338), 0, Real(0.0)); // CG1
  AddRule(ost::conop::VAL, 6, 5, 1, 4,
          Real(1.5257), Real(1.9295), 4, Real(2.1478)); // CG2

  // ILE
  AddRule(ost::conop::ILE, 5, 0, 1, 4,
          Real(1.5331), Real(1.9286), 0, Real(0.0)); // CG1
  AddRule(ost::conop::ILE, 6, 5, 1, 4,
          Real(1.5300), Real(1.9320), 4, Real(-2.1534)); // CG2
  AddRule(ost::conop::ILE, 7, 1, 4, 5,
          Real(1.5194), Real(1.9887), 1, Real(0.0)); // CD1

  // LEU
  AddRule(ost::conop::LEU, 5, 0, 1, 4,
          Real(1.5298), Real(2.0276), 0, Real(0.0)); // CG
  AddRule(ost::conop::LEU, 6, 1, 4, 5,
          Real(1.5236), Real(1.9276), 1, Real(0)); // CD1
  AddRule(ost::conop::LEU, 7, 6, 4, 5,
          Real(1.5242), Real(1.9316), 4, Real(2.1459)); // CD2

  // PRO
  AddRule(ost::conop::PRO, 5, 0, 1, 4,
          Real(1.4955), Real(1.8224), 0, Real(0.0)); // CG
  AddRule(ost::conop::PRO, 6, 1, 4, 5,
          Real(1.5063), Real(1.8391), 1, Real(0.0)); // CD

  // HIS
  AddRule(ost::conop::HIS, 5, 0, 1, 4,
          Real(1.4953), Real(1.9832), 0, Real(0.0)); // CG
  AddRule(ost::conop::HIS, 6, 1, 4, 5,
          Real(1.3783), Real(2.1399), 1, Real(0.0)); // ND1
  AddRule(ost::conop::HIS, 7, 1, 4, 5,
          Real(1.3551), Real(2.2869), 1, Real(3.14159265359)); // CD2
  AddRule(ost::conop::HIS, 8, 7, 5, 6,
          Real(1.3234), Real(1.9046), 4, Real(0.0)); // CE1
  AddRule(ost::conop::HIS, 9, 6, 5, 7,
          Real(1.3734), Real(1.8712), 4, Real(0.0)); // NE2

  // PHE
  AddRule(ost::conop::PHE, 5, 0, 1, 4,
          Real(1.5043), Real(1.9866), 0, Real(0.0)); // CG
  AddRule(ost::conop::PHE, 6, 1, 4, 5,
          Real(1.3883), Real(2.1071), 1, Real(0.0)); // CD1
  AddRule(ost::conop::PHE, 7, 1, 4, 5,
          Real(1.3879), Real(2.1039), 1, Real(3.14159265359)); // CD2
  AddRule(ost::conop::PHE, 8, 7, 5, 6,
          Real(1.3906), Real(2.1079), 4, Real(0.0)); // CE1
  AddRule(ost::conop::PHE, 9, 6, 5, 7,
          Real(1.3902), Real(2.1082), 4, Real(0.0)); // CE2
  AddRule(ost::conop::PHE, 10, 5, 6, 8,
          Real(1.3832), Real(2.0928), 4, Real(0.0)); // CZ

  // ARG
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::ARG, 0, 1, 4, 5);
  // CA-CB-CG-CD
  AddChiDefinition(ost::conop::ARG, 1, 4, 5, 6);
  // CB-CG-CD-NE
  AddChiDefinition(ost::conop::ARG, 4, 5, 6, 7);
  // CG-CD-NE-CZ
  AddChiDefinition(ost::conop::ARG, 5, 6, 7, 8);

  // ASN
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::ASN, 0, 1, 4, 5);
  // CA-CB-CG-OD1
  AddChiDefinition(ost::conop::ASN, 1, 4, 5, 6);

  // ASP
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::ASP, 0, 1, 4, 5);
  // CA-CB-CG-OD1
  AddChiDefinition(ost::conop::ASP, 1, 4, 5, 6);

  // GLN
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::GLN, 0, 1, 4, 5);
  // CA-CB-CG-CD
  AddChiDefinition(ost::conop::GLN, 1, 4, 5, 6);
  // CB-CG-CD-OE1
  AddChiDefinition(ost::conop::GLN, 4, 5, 6, 7);

  // GLU
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::GLU, 0, 1, 4, 5);
  // CA-CB-CG-CD
  AddChiDefinition(ost::conop::GLU, 1, 4, 5, 6);
  // CB-CG-CD-OE1
  AddChiDefinition(ost::conop::GLU, 4, 5, 6, 7);

  // LYS
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::LYS, 0, 1, 4, 5);
  // CA-CB-CG-CD
  AddChiDefinition(ost::conop::LYS, 1, 4, 5, 6);
  // CB-CG-CD-CE
  AddChiDefinition(ost::conop::LYS, 4, 5, 6, 7);
  // CG-CD-CE-NZ
  AddChiDefinition(ost::conop::LYS, 5, 6, 7, 8);

  // SER
  // N-CA-CB-OG
  AddChiDefinition(ost::conop::SER, 0, 1, 4, 5);

  // CYS
  // N-CA-CB-SG
  AddChiDefinition(ost::conop::CYS, 0, 1, 4, 5);

  // MET
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::MET, 0, 1, 4, 5);
  // CA-CB-CG-SD
  AddChiDefinition(ost::conop::MET, 1, 4, 5, 6);
  // CB-CG-SD-CE
  AddChiDefinition(ost::conop::MET, 4, 5, 6, 7);

  // TRP
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::TRP, 0, 1, 4, 5);
  // CA-CB-CG-CD1
  AddChiDefinition(ost::conop::TRP, 1, 4, 5, 6);

  // TYR
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::TYR, 0, 1, 4, 5);
  // CA-CB-CG-CD1
  AddChiDefinition(ost::conop::TYR, 1, 4, 5, 6);

  // THR
  // N-CA-CB-OG1
  AddChiDefinition(ost::conop::THR, 0, 1, 4, 5);

  // VAL
  // N-CA-CB-CG1
  AddChiDefinition(ost::conop::VAL, 0, 1, 4, 5);

  // ILE
  // N-CA-CB-CG1
  AddChiDefinition(ost::conop::ILE, 0, 1, 4, 5);
  // CA-CB-CG1-CD1
  AddChiDefinition(ost::conop::ILE, 1, 4, 5, 7);

  // LEU
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::LEU, 0, 1, 4, 5);
  // CA-CB-CG-CD1
  AddChiDefinition(ost::conop::LEU, 1, 4, 5, 6);

  // PRO
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::PRO, 0, 1, 4, 5);
  // CA-CB-CG-CD
  AddChiDefinition(ost::conop::PRO, 1, 4, 5, 6);

  // HIS
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::HIS, 0, 1, 4, 5);
  // CA-CB-CG-ND1
  AddChiDefinition(ost::conop::HIS, 1, 4, 5, 6);

  // PHE
  // N-CA-CB-CG
  AddChiDefinition(ost::conop::PHE, 0, 1, 4, 5);
  // CA-CB-CG-CD1
  AddChiDefinition(ost::conop::PHE, 1, 4, 5, 6);
}

}}
