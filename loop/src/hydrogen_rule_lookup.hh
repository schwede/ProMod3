// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


/// \brief Defines rules to construct hydrogens for amino acids.

#ifndef PROMOD3_HYDROGEN_RULE_LOOKUP_HH
#define PROMOD3_HYDROGEN_RULE_LOOKUP_HH

#include <promod3/loop/amino_acid_atoms.hh>

namespace promod3 { namespace loop {

// Protonation state for HIS (default = HISE)
enum HisProtonationState {
  PROT_STATE_COMMON, // only hydrogens shared by all prot. states
  PROT_STATE_HISE,   // + HE2
  PROT_STATE_HISD,   // + HD1
  PROT_STATE_HISH    // + both
};

// Hydrogen construction rules
// -> anchor_idx are atom indices for heavy atoms (..AtomIndex)
// -> hydro_idx are atom indices for hydrogen atoms (..HydrogenIndex)
struct HydrogenRule {
  // DATA
  uint rule;
  uint num_hydrogens;
  uint num_anchors;
  uint anchor_idx[4]; // max. 4 anchors
  uint hydro_idx[3];  // max. 3 hydrogens
  bool is_polar;      // either all polar or none...
  HisProtonationState his_prot_state; // only for HIS
  // TO SKIP?
  bool IsToSkip(bool only_polar, ost::conop::AminoAcid aa,
                HisProtonationState state) const {
    return (only_polar && !is_polar)
           || (aa == ost::conop::HIS && his_prot_state != PROT_STATE_COMMON
               && state != PROT_STATE_HISH && state != his_prot_state);
  }
};

// Defines lookups to build hydrogens.
class HydrogenRuleLookup {
public:
  // Singleton access to one constant instance (see AminoAcidLookup for details)
  static const HydrogenRuleLookup& GetInstance() {
    static HydrogenRuleLookup instance;
    return instance;
  }
  // Data access
  const std::vector<HydrogenRule>& GetRules(ost::conop::AminoAcid aa) const {
    return rules_[aa];
  }
  // returns NULL if there is no rule (prominently for H-N)
  const HydrogenRule* GetRule(AminoAcidHydrogen aah) const {
    return single_rule_[aah];
  }
private:
  // construction helpers
  void AddRule_(ost::conop::AminoAcid aa, uint r, bool p = false,
                HisProtonationState s = PROT_STATE_COMMON);
  void SetAnchors_(ost::conop::AminoAcid aa, uint anch1, uint anch2,
                   uint anch3);
  void SetAnchors_(ost::conop::AminoAcid aa, uint anch1, uint anch2,
                   uint anch3, uint anch4);
  void SetHydrogens_(ost::conop::AminoAcid aa, uint idx);
  void SetHydrogens_(ost::conop::AminoAcid aa, uint idx1, uint idx2);
  void SetHydrogens_(ost::conop::AminoAcid aa, uint idx1, uint idx2, uint idx3);
  
  // construction only inside here
  HydrogenRuleLookup();

  // lookup tables
  std::vector<HydrogenRule> rules_[ost::conop::XXX+1];
  HydrogenRule* single_rule_[XXX_NUM_HYDROGENS+1];
};

}} // ns

#endif
