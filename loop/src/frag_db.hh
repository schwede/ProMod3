// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_FRAG_DB_HH
#define PROMOD_LOOP_FRAG_DB_HH

#include <stdint.h>
#include <vector>
#include <set>
#include <boost/shared_ptr.hpp>
#include <limits>

#include <ost/base.hh>
#include <ost/fixed_string.hh>
#include <ost/mol/entity_view.hh>
#include <ost/mol/mol.hh>

#include <promod3/loop/data_bag.hh>
#include <promod3/loop/ushort_vec.hh>
#include <promod3/loop/structure_db.hh>
#include <promod3/loop/stem_geom.hh>
#include <promod3/core/message.hh>
#include <promod3/core/geom_stems.hh>

namespace promod3 { namespace loop {

class FragDB;
typedef boost::shared_ptr<FragDB> FragDBPtr;

struct Hasher {
  uint operator()(const StemPairGeom& g) const
  { //given not more than 45 angle bins and a length below 20, this hash function
    //produces unique values for all stem pair geometries not including their
    //distance bin
    uint key = static_cast<uint>(g.angle_one) * 1822500; //1822500 = 45*45*45*20
    key += static_cast<uint>(g.angle_two) * 40500; // 40500 = 45*45*20
    key += static_cast<uint>(g.angle_three) * 900; // 900 = 45*20
    key += static_cast<uint>(g.angle_four) * 20;  
    key += static_cast<uint>(g.length);

    return key;
  }
};

class FragDB {
public:
  typedef DataBag<StemPairGeom, FragmentInfo, Hasher> FragmentData;
  typedef FragmentData::IteratorPair IteratorPair;

  FragDB(Real dist_bin_s, int ang_bin_s, bool load_from_disk=false);

  static FragDBPtr Load(const String& file_name);

  void Save(const String& filename);

  static FragDBPtr LoadPortable(const String& file_name);

  void SavePortable(const String& filename);

  bool operator==(const FragDB& rhs) const;
  bool operator!=(const FragDB& rhs) const {
    return !this->operator==(rhs);
  }

  void AddFragments(uint fragment_length, Real rmsd_cutoff,
                    StructureDBPtr structure_db);

  void SearchDB(const ost::mol::ResidueHandle& n_stem,
                const ost::mol::ResidueHandle& c_stem,
                uint frag_size, std::vector<FragmentInfo>& fragments, 
                uint extra_bins = 0);

  uint GetAngularBinSize() const { return angular_bin_size_; }

  Real GetDistBinSize() const { return dist_bin_size_; }

  bool IsReadOnly() const { return fragments_.IsReadOnly(); }
  
  void SetReadOnly(bool ro) { fragments_.SetReadOnly(ro); }
  
  void PrintStatistics() const;

  int GetNumStemPairs() const { return fragments_.ItemCount(); };

  int GetNumStemPairs(int length) const;

  int GetNumFragments() const { return fragments_.Size(); };

  int GetNumFragments(int length) const;

  bool HasFragLength(uint length) const {
    return (frag_lengths_.find(length) != frag_lengths_.end());
  }

  uint MaxFragLength() const {
    if (frag_lengths_.empty()) {
      return 0;
    } else {
      return *frag_lengths_.rbegin();
    }
  }

private:

  typedef std::pair<core::StemCoords,core::StemCoords> StemCoordPair;

  void SearchDB(const StemCoordPair& stems, uint frag_size, 
                std::vector<FragmentInfo>& fragments,
                uint extra_bins = 0);

  StemCoordPair StemCoordPairFromResidues(const ost::mol::ResidueHandle& n_res,
                                          const ost::mol::ResidueHandle& c_res);

  StemCoordPair StemCoordPairFromPeptideCoords(const PeptideCoords& n_coords,
                                               const PeptideCoords& c_coords);

  std::vector<StemPairGeom> MakeStemPairGeom(const core::StemCoords& n_stem,
                                             const core::StemCoords& c_stem,
                                             int length,
                                             uint extra_bins = 0) const;

  void SetFragLengths();

  FragmentData fragments_;
  uint angular_bin_size_;
  Real dist_bin_size_;
  std::set<uint> frag_lengths_;
};

}}

#endif
