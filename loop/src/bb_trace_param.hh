// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_BB_TRACE_PARAM_HH
#define PM3_BB_TRACE_PARAM_HH

#include <ost/base.hh>

namespace promod3{ namespace loop{

// Hardcode bond lengths and angles. Using these value, you can construct a 
// Backbone trace (N, CA, C) if you additionally know the phi/psi/omega
// dihedrals.
// The values are extracted from high resolution xray structures
// (see promod3/extras/code_generation/parameter_extraction.py)
// You can extract following bonds: N-CA, CA-C, C-(N+1)
// and following angles: (C-1)-N-CA, N-CA-C, CA-C-(N+1)
void BBTraceParam(char olc, Real& n_ca_bond, Real& ca_c_bond, Real& c_n_bond, 
                  Real& c_n_ca_angle, Real& n_ca_c_angle, Real& ca_c_n_angle);

}}
#endif
