// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/backbone.hh>
#include <promod3/loop/density_creator.hh>
#include <ost/conop/processor.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/loop/bb_trace_param.hh>

namespace promod3 { namespace loop {

namespace {

// default chem. type and class for amino acids
const ost::mol::ChemClass aa_chem_class(ost::mol::ChemClass::L_PEPTIDE_LINKING);
const ost::mol::ChemType aa_chem_type(ost::mol::ChemType::AMINOACIDS);

// set residue props
void SetResidueProps(ost::mol::ResidueHandle& res, char olc) {
  res.SetOneLetterCode(olc);
  res.SetChemClass(aa_chem_class);
  res.SetChemType(aa_chem_type);
  res.SetIsProtein(true);
}

} // anon ns

///////////////////////////////////////////////////////////////////////////////
// Backbone public
Backbone::Backbone(const geom::Vec3& n, const geom::Vec3& ca, 
                   const geom::Vec3& cb, const geom::Vec3& c, 
                   const geom::Vec3& o, char olc) {
  n_coord = n;
  ca_coord = ca;
  cb_coord = cb;
  c_coord = c;
  o_coord = o;
  one_letter_code = olc;
  SetAA(ost::conop::OneLetterCodeToAminoAcid(olc));
}

Backbone::Backbone(const geom::Vec3& n, const geom::Vec3& ca, 
                   const geom::Vec3& c, const geom::Vec3& o, 
                   char olc) {
  n_coord = n;
  ca_coord = ca;
  core::ConstructCBetaPos(n,ca,c,cb_coord);
  c_coord = c;
  o_coord = o;
  one_letter_code = olc;
  SetAA(ost::conop::OneLetterCodeToAminoAcid(olc));
}

Backbone::Backbone(const ost::mol::ResidueHandle& res, char olc) {

  ost::mol::AtomHandle n, ca, cb, c, o;

  n = res.FindAtom("N");
  ca = res.FindAtom("CA");
  cb = res.FindAtom("CB");
  c = res.FindAtom("C");
  o = res.FindAtom("O");

  if (!(n.IsValid() && ca.IsValid() && c.IsValid() && o.IsValid())) {
    throw promod3::Error("Could not find all required backbone atoms in given "
                         "residue!");
  }

  n_coord = n.GetPos();
  ca_coord = ca.GetPos();
  c_coord = c.GetPos();
  o_coord = o.GetPos();

  if (cb.IsValid()) {
    cb_coord = cb.GetPos();
  } else {
    core::ConstructCBetaPos(n_coord, ca_coord, c_coord, cb_coord);
  }

  if (olc == '\0') one_letter_code = res.GetOneLetterCode();
  else one_letter_code = olc;

  SetAA(ost::conop::OneLetterCodeToAminoAcid(one_letter_code));
}

///////////////////////////////////////////////////////////////////////////////
// BackboneList public
BackboneList::BackboneList(const String& sequence,
                           const std::vector<Real>& phi_angles,
                           const std::vector<Real>& psi_angles) {
  if (sequence.empty()) return;
  if (phi_angles.size() != sequence.size()) {
    throw promod3::Error("Must have same number of phi angles as letters in "
                         "sequence when initializing BackboneList!");
  }
  if (psi_angles.size() != sequence.size()) {
    throw promod3::Error("Must have same number of psi angles as letters in "
                         "sequence when initializing BackboneList!");
  }
  // typical omega value
  std::vector<Real> omega_angles(sequence.size(), Real(M_PI));
  ConstructBackboneList_(sequence, phi_angles, psi_angles, omega_angles);
}

BackboneList::BackboneList(const String& sequence,
                           const std::vector<Real>& phi_angles,
                           const std::vector<Real>& psi_angles,
                           const std::vector<Real>& omega_angles) {
  if (sequence.empty()) return;
  if (phi_angles.size() != sequence.size()) {
    throw promod3::Error("Must have same number of phi angles as letters in "
                         "sequence when initializing BackboneList!");
  }
  if (psi_angles.size() != sequence.size()) {
    throw promod3::Error("Must have same number of psi angles as letters in "
                         "sequence when initializing BackboneList!");
  }
  if (omega_angles.size() != sequence.size()) {
    throw promod3::Error("Must have same number of omega angles as letters in "
                         "sequence when initializing BackboneList!");
  }
  ConstructBackboneList_(sequence, phi_angles, psi_angles, omega_angles);
}

BackboneList::BackboneList(const String& sequence){
  if (sequence.empty()) return;
  // typical backbone torsion angles for a helix
  std::vector<Real> phi_angles(sequence.size(), -1.0472);
  std::vector<Real> psi_angles(sequence.size(), -0.78540);
  std::vector<Real> omega_angles(sequence.size(), Real(M_PI));
  ConstructBackboneList_(sequence, phi_angles, psi_angles, omega_angles);
}

BackboneList::BackboneList(const ost::mol::ResidueHandleList& res_list) {

  bb_list_.reserve(res_list.size());
  for (uint i = 0; i < res_list.size(); ++i) {
    bb_list_.push_back(Backbone(res_list[i]));
  }
}

BackboneList::BackboneList(const String& sequence,
                           const ost::mol::ResidueHandleList& res_list) {

  if (sequence.empty()) return;
  if (res_list.size() != sequence.size()) {
    throw promod3::Error("Must have same number of residues as aa letters "
                         "when initializing BackboneList!");
  } 

  bb_list_.reserve(sequence.size());
  for (uint i = 0; i < res_list.size(); ++i) {
    bb_list_.push_back(Backbone(res_list[i], sequence[i]));
  }
}

ost::mol::EntityHandle BackboneList::ToEntity() const {
  // make entity
  ost::mol::EntityHandle ent = ost::mol::CreateEntity();
  ost::mol::XCSEditor edi = ent.EditXCS(ost::mol::BUFFERED_EDIT);
  ost::mol::ChainHandle chain = edi.InsertChain("A");
  ost::mol::AtomHandle n,ca,c,o,cb;
  ost::mol::ResidueHandle last;
  for (uint i = 0; i < size(); ++i) {
    String rname=ost::conop::OneLetterCodeToResidueName(GetOLC(i));
    ost::mol::ResidueHandle res=edi.AppendResidue(chain, rname);
    n = edi.InsertAtom(res, "N", GetN(i), "N");
    ca = edi.InsertAtom(res, "CA", GetCA(i), "C");
    c = edi.InsertAtom(res, "C", GetC(i), "C");
    o = edi.InsertAtom(res, "O", GetO(i), "O");
    edi.Connect(n, ca);
    edi.Connect(ca, c);
    edi.Connect(c, o);
    if (GetOLC(i) != 'G') {
      // insert Cbeta for all residues but glycine
      cb = edi.InsertAtom(res, "CB", GetCB(i), "C");
      edi.Connect(ca, cb);
    }
    SetResidueProps(res, GetOLC(i));
    if (last.IsValid()) {
      edi.Connect(last.FindAtom("C"), n);
    }
    last = res;
  }
  // set torsions
  ost::conop::AssignBackboneTorsions(chain);
  return ent;
}

ost::img::MapHandle BackboneList::ToDensity(Real padding,
                                            geom::Vec3 sampling,
                                            Real resolution,
                                            bool high_resolution) const {

  std::vector<geom::Vec3> positions;
  std::vector<Real> atom_weights;
  geom::AlignedCuboid bounds;

  if (high_resolution) {
    bounds = this->GetBounds();
    positions.reserve(5 * this->size());
    atom_weights.reserve(5 * this->size());
    for (uint i = 0; i < this->size(); ++i) {
      positions.push_back(bb_list_[i].n_coord);
      atom_weights.push_back(14.007);

      positions.push_back(bb_list_[i].ca_coord);
      atom_weights.push_back(12.011);

      if (bb_list_[i].aa != ost::conop::GLY) {
        positions.push_back(bb_list_[i].cb_coord);
        atom_weights.push_back(12.011);
      }

      positions.push_back(bb_list_[i].c_coord);
      atom_weights.push_back(12.011);

      positions.push_back(bb_list_[i].o_coord);
      atom_weights.push_back(15.999);
    }
  } else {
    bounds = this->GetBounds(false);
    positions.reserve(this->size());
    atom_weights.reserve(this->size());
    for (uint i = 0; i < this->size(); ++i) {
      positions.push_back(bb_list_[i].ca_coord);
      atom_weights.push_back(12.001);
    }
  }

  geom::Vec3 min = bounds.GetMin();
  geom::Vec3 max = bounds.GetMax();

  min -= padding;
  max += padding;

  ost::img::Size img_size(std::ceil((max[0] - min[0]) / sampling[0]),
                          std::ceil((max[1] - min[1]) / sampling[1]),
                          std::ceil((max[2] - min[2]) / sampling[2]));
  ost::img::MapHandle map = ost::img::CreateMap(img_size);
  map.SetPixelSampling(sampling);
  map.SetAbsoluteOrigin(min);

  FillDensityGaussianSpheres(positions, atom_weights, map, 
                             resolution, false, high_resolution);

  return map;
}

void BackboneList::InsertInto(ost::mol::ChainHandle& chain,
                              const ost::mol::ResNum& start_resnum) const {

  if (!chain.IsValid()) {
    std::stringstream ss;
    ss << "Provided chain you want the BackboneList inserted into is invalid!";
    throw promod3::Error(ss.str());
  }

  ost::mol::XCSEditor edi=chain.GetEntity().EditXCS(ost::mol::BUFFERED_EDIT);  

  ost::mol::ResNum n_num = start_resnum;
  ost::mol::ResNum c_num = start_resnum + this->size() -1;

  // handle n_stem
  ost::mol::ResidueHandle before = chain.FindResidue(n_num);
  if (before.IsValid()) {

    ost::mol::AtomHandle n = before.FindAtom("N");
    ost::mol::AtomHandle ca = before.FindAtom("CA");
    ost::mol::AtomHandle c = before.FindAtom("C");
    ost::mol::AtomHandle o = before.FindAtom("O");
    ost::mol::AtomHandle cb = before.FindAtom("CB");

    if (!n.IsValid() && ca.IsValid() && c.IsValid() && o.IsValid()) {
      throw promod3::Error("Expect stem residues to contain all backbone atoms!");
    }

    if (!cb.IsValid() && GetAA(0) != ost::conop::GLY) {
      geom::Vec3 cb_pos;
      core::ConstructCBetaPos(n.GetPos(), ca.GetPos(), c.GetPos(), cb_pos);
      cb = edi.InsertAtom(before, "CB", cb_pos, "C");
      edi.Connect(cb, ca);      
    }
    if (cb.IsValid() && GetAA(0) == ost::conop::GLY) {
      edi.DeleteAtom(cb);
    }

    edi.SetAtomPos(n, GetN(0));
    edi.SetAtomPos(ca, GetCA(0));
    edi.SetAtomPos(c, GetC(0));
    edi.SetAtomPos(o, GetO(0));
    if(cb.IsValid()) edi.SetAtomPos(cb, GetCB(0));

    ost::mol::AtomHandleList atoms=before.GetAtomList();
    for (ost::mol::AtomHandleList::iterator 
         i=atoms.begin(), e=atoms.end(); i!=e; ++i) {
      String aname = i->GetName();
      if (aname!="N" && aname!="CA" && aname!="C" && aname!="O" && aname!="CB") {
        edi.DeleteAtom(*i);
      }
    }

    edi.RenameResidue(before, ost::conop::OneLetterCodeToResidueName(GetOLC(0)));
    before.SetOneLetterCode(GetOLC(0));

  } else { // !before.IsValid()

    before = edi.AppendResidue(chain,
                               ost::conop::OneLetterCodeToResidueName(GetOLC(0)),
                               n_num);
    SetResidueProps(before, GetOLC(0));
    ost::mol::AtomHandle n = edi.InsertAtom(before, "N", GetN(0), "N");
    ost::mol::AtomHandle ca = edi.InsertAtom(before, "CA", GetCA(0), "C");
    ost::mol::AtomHandle c = edi.InsertAtom(before, "C", GetC(0), "C");
    ost::mol::AtomHandle o = edi.InsertAtom(before, "O", GetO(0), "O");

    edi.Connect(n, ca);
    edi.Connect(ca, c);
    edi.Connect(c, o);
    
    if (GetOLC(0) != 'G') {
      ost::mol::AtomHandle cb = edi.InsertAtom(before, "CB", GetCB(0), "C");
      edi.Connect(ca, cb);
    }

    ost::mol::ResidueHandle before_before = chain.FindResidue(n_num-1);
    if (before_before.IsValid()) {
      ost::mol::AtomHandle before_before_c = before_before.FindAtom("C");
      if (before_before_c.IsValid()) {
        edi.Connect(before_before_c, n);
      }
    }
  }

  // handle c_stem
  ost::mol::ResidueHandle after = chain.FindResidue(c_num);
  const uint last_idx = size() - 1;
  if (n_num != c_num && after.IsValid()) {

    ost::mol::AtomHandle n = after.FindAtom("N");
    ost::mol::AtomHandle ca = after.FindAtom("CA");
    ost::mol::AtomHandle c = after.FindAtom("C");
    ost::mol::AtomHandle o = after.FindAtom("O");
    ost::mol::AtomHandle cb = after.FindAtom("CB");

    if (!n.IsValid() && ca.IsValid() && c.IsValid() && o.IsValid()) {
      throw promod3::Error("Expect stem residues to contain all backbone atoms!");
    }

    if (!cb.IsValid() && GetAA(last_idx) != ost::conop::GLY) {
      geom::Vec3 cb_pos;
      core::ConstructCBetaPos(n.GetPos(), ca.GetPos(), c.GetPos(), cb_pos);
      cb = edi.InsertAtom(after,"CB",cb_pos,"C");
      edi.Connect(cb, ca);      
    }
    if (cb.IsValid() && GetAA(last_idx) == ost::conop::GLY) {
      edi.DeleteAtom(cb);
    }

    edi.SetAtomPos(n, GetN(last_idx));
    edi.SetAtomPos(ca, GetCA(last_idx));
    edi.SetAtomPos(c, GetC(last_idx));
    edi.SetAtomPos(o, GetO(last_idx));
    
    if(cb.IsValid()) edi.SetAtomPos(cb, GetCB(last_idx));    

    ost::mol::AtomHandleList atoms=after.GetAtomList();
    for (ost::mol::AtomHandleList::iterator 
         i=atoms.begin(), e=atoms.end(); i!=e; ++i) {
      String aname=i->GetName();
      if (aname!="N" && aname!="CA" && aname!="C" && aname!="O" && aname!="CB") {
        edi.DeleteAtom(*i);
      }
    }

    edi.RenameResidue(after, ost::conop::OneLetterCodeToResidueName(GetOLC(last_idx)));
    after.SetOneLetterCode(GetOLC(last_idx));

  } else if(n_num != c_num) { // !after.IsValid()

    after = edi.AppendResidue(chain, ost::conop::OneLetterCodeToResidueName(GetOLC(last_idx)), c_num);
    SetResidueProps(after, GetOLC(last_idx));
    ost::mol::AtomHandle n = edi.InsertAtom(after, "N", GetN(last_idx), "N");
    ost::mol::AtomHandle ca = edi.InsertAtom(after, "CA", GetCA(last_idx), "C");
    ost::mol::AtomHandle c = edi.InsertAtom(after, "C", GetC(last_idx), "C");
    ost::mol::AtomHandle o = edi.InsertAtom(after, "O", GetO(last_idx), "O");

    edi.Connect(n, ca);
    edi.Connect(ca, c);
    edi.Connect(c, o);
    
    if (GetOLC(last_idx) != 'G') {
      ost::mol::AtomHandle cb = edi.InsertAtom(after, "CB", GetCB(last_idx), "C");
      edi.Connect(ca, cb);
    }

    // check peptide bond to next one
    ost::mol::ResidueHandle after_after = chain.FindResidue(c_num+1);
    if (after_after.IsValid()) {
      ost::mol::AtomHandle after_after_n = after_after.FindAtom("N");
      if (after_after_n.IsValid()) {
        edi.Connect(c, after_after_n);
      }
    }
  }

  // lets kill all residues between n_stem and c_stem
  for (ost::mol::ResNum i = n_num + 1; i < c_num; ++i) {
    ost::mol::ResidueHandle res = chain.FindResidue(i);
    if (res.IsValid()) {
      edi.DeleteResidue(res);
    }
  }

  //and refill them
  ost::mol::ResidueHandle last = before;
  for (uint i = 1; i < size()-1; ++i) {
    String rname = ost::conop::OneLetterCodeToResidueName(GetOLC(i));
    ost::mol::ResidueHandle res = edi.AppendResidue(chain, rname,
                                                    last.GetNumber() + 1);
    SetResidueProps(res, GetOLC(i));
    ost::mol::AtomHandle n = edi.InsertAtom(res, "N", GetN(i), "N");
    ost::mol::AtomHandle ca = edi.InsertAtom(res, "CA", GetCA(i), "C");
    ost::mol::AtomHandle c = edi.InsertAtom(res, "C", GetC(i), "C");
    ost::mol::AtomHandle o = edi.InsertAtom(res, "O", GetO(i), "O");
    edi.Connect(n, ca);
    edi.Connect(ca, c);
    edi.Connect(c, o);
    if (GetOLC(i) != 'G') {
      // insert Cbeta for all residues but glycine
      ost::mol::AtomHandle cb = edi.InsertAtom(res, "CB", GetCB(i), "C");
      edi.Connect(ca, cb);
    }
    edi.Connect(last.FindAtom("C"), n);
    last = res;
  }

  edi.Connect(last.FindAtom("C"), after.FindAtom("N"));
  edi.ReorderResidues(chain);

  // add backbone torsions
  ost::mol::ResidueHandle r1;
  ost::mol::ResidueHandle r2 = chain.FindResidue(n_num - 1);
  ost::mol::ResidueHandle r3 = chain.FindResidue(n_num);
  for (ost::mol::ResNum r_num = n_num; r_num <= c_num; ++r_num) {
    // add for r_num
    r1 = r2;
    r2 = r3;
    r3 = chain.FindResidue(r_num + 1);
    ost::conop::AssignBackboneTorsions(r1, r2, r3);
  }
}

void BackboneList::InsertInto(ost::img::MapHandle& map,
                              Real resolution,
                              bool high_resolution) const {

  std::vector<geom::Vec3> positions;
  std::vector<Real> atom_weights;

  if (high_resolution) {
    positions.reserve(5 * this->size());
    atom_weights.reserve(5 * this->size());
    for (uint i = 0; i < this->size(); ++i) {
      positions.push_back(GetN(i));
      atom_weights.push_back(14.007);

      positions.push_back(GetCA(i));
      atom_weights.push_back(12.011);

      if (GetAA(i) != ost::conop::GLY) {
        positions.push_back(GetCB(i));
        atom_weights.push_back(12.011);
      }

      positions.push_back(GetC(i));
      atom_weights.push_back(12.011);

      positions.push_back(GetO(i));
      atom_weights.push_back(15.999);
    }
  } else {
    positions.reserve(this->size());
    atom_weights.reserve(this->size());
    for(uint i = 0; i < this->size(); ++i){
      positions.push_back(GetCA(i));
      atom_weights.push_back(12.001);
    }
  }

  FillDensityGaussianSpheres(positions, atom_weights, map, 
                             resolution, false, high_resolution);
}

geom::AlignedCuboid BackboneList::GetBounds(bool all_atom) const {

  Real min_x = std::numeric_limits<Real>::max(); 
  Real max_x = -std::numeric_limits<Real>::max();
  Real min_y = std::numeric_limits<Real>::max();
  Real max_y = -std::numeric_limits<Real>::max();
  Real min_z = std::numeric_limits<Real>::max(); 
  Real max_z = -std::numeric_limits<Real>::max();

  geom::Vec3 pos;
  if (all_atom) {
    for (uint i = 0; i < this->size(); ++i) {
      pos = GetN(i);
      min_x = std::min(pos[0], min_x);
      max_x = std::max(pos[0], max_x);
      min_y = std::min(pos[1], min_y);
      max_y = std::max(pos[1], max_y);
      min_z = std::min(pos[2], min_z);
      max_z = std::max(pos[2], max_z);

      pos = GetCA(i);
      min_x = std::min(pos[0], min_x);
      max_x = std::max(pos[0], max_x);
      min_y = std::min(pos[1], min_y);
      max_y = std::max(pos[1], max_y);
      min_z = std::min(pos[2], min_z);
      max_z = std::max(pos[2], max_z);

      if (GetAA(i) != ost::conop::GLY) {
        pos = GetCB(i);
        min_x = std::min(pos[0], min_x);
        max_x = std::max(pos[0], max_x);
        min_y = std::min(pos[1], min_y);
        max_y = std::max(pos[1], max_y);
        min_z = std::min(pos[2], min_z);
        max_z = std::max(pos[2], max_z);
      }

      pos = GetC(i);
      min_x = std::min(pos[0], min_x);
      max_x = std::max(pos[0], max_x);
      min_y = std::min(pos[1], min_y);
      max_y = std::max(pos[1], max_y);
      min_z = std::min(pos[2], min_z);
      max_z = std::max(pos[2], max_z);

      pos = GetO(i);
      min_x = std::min(pos[0], min_x);
      max_x = std::max(pos[0], max_x);
      min_y = std::min(pos[1], min_y);
      max_y = std::max(pos[1], max_y);
      min_z = std::min(pos[2], min_z);
      max_z = std::max(pos[2], max_z);
    }
  } else {
    for(uint i = 0; i < this->size(); ++i){
      pos = GetCA(i);
      min_x = std::min(pos[0], min_x);
      max_x = std::max(pos[0], max_x);
      min_y = std::min(pos[1], min_y);
      max_y = std::max(pos[1], max_y);
      min_z = std::min(pos[2], min_z);
      max_z = std::max(pos[2], max_z);
    }
  }

  return geom::AlignedCuboid(geom::Vec3(min_x,min_y,min_z),
                             geom::Vec3(max_x,max_y,max_z));
}

void BackboneList::SetSequence(const String& seq) {
  // check size (AA checked in SetAA)
  if (seq.size() != this->size()) {
    throw promod3::Error("Sequence and bb_list must have consistent size!");
  }
  // set
  for (uint i = 0; i < seq.size(); ++i) {
    SetAA(i, ost::conop::OneLetterCodeToAminoAcid(seq[i]));
    SetOLC(i, seq[i]);
  }
}

String BackboneList::GetSequence() const{
  String return_string(this->size(),'X');
  for(uint i = 0; i < this->size(); ++i){
    return_string[i] = GetOLC(i);
  }
  return return_string;
}

BackboneList BackboneList::Extract(uint from, uint to) const{
  if (from >= to) {
    throw promod3::Error("From must be smaller to!");
  }
  if (from >= this->size() || to > this->size()) {
    throw promod3::Error("Invalid position idx!");
  }

  BackboneList bb_list;
  for (uint i = from; i < to; ++i) {
    bb_list.bb_list_.push_back(bb_list_[i]);
  }
  return bb_list;
}

void BackboneList::ReplaceFragment(const BackboneList& frag, uint index, 
                                   bool superpose_stems) {
  // check the size
  uint frag_size = frag.size();
  if (frag_size < 2) {
    throw promod3::Error("Subfragment must be at least of length 2!");
  }
  if (index + frag_size > this->size()) {
    throw promod3::Error("Provided subfragment must fit into the BackboneList "
                         "at given position!");
  }

  if (!superpose_stems) {
    for (uint i = 0; i < frag.size(); ++i) {
      bb_list_[index+i] = frag.bb_list_[i];
    }
  } else {
    uint stem_one = index;
    uint stem_two = index + frag_size - 1;
    // insert transformed fragment aligned on stem one
    geom::Mat4 t = frag.GetTransform(0, *this, stem_one);
    for(uint i = 0; i < frag_size-1; ++i){
      bb_list_[stem_one+i] = frag.bb_list_[i];
      ApplyTransform(stem_one+i, t);
    }
    // transform rest based on stem two
    // (note: we keep original stem_two for oxygen pos.!)
    Backbone stem_two_backbone = frag.bb_list_.back();
    stem_two_backbone.ApplyTransform(t);
    t = bb_list_[stem_two].GetTransform(stem_two_backbone);
    ApplyTransform(stem_two, size(), t);
  }
}

Real BackboneList::MinCADistance(const BackboneList& other) const{
  Real min_distance = std::numeric_limits<Real>::max();
  for(uint i = 0; i < this->size(); ++i){
    for(uint j = 0; j < other.size(); ++j){
      min_distance = std::min(min_distance,
                              geom::Length2(GetCA(i) - other.GetCA(j)));
    }
  }
  return std::sqrt(min_distance);
}

void BackboneList::ReconstructCBetaPositions() {
  for (uint i = 0; i < size(); ++i) {
    geom::Vec3 cb_pos;
    core::ConstructCBetaPos(GetN(i), GetCA(i), GetC(i), cb_pos);
    SetCB(i, cb_pos);
  }
}

void BackboneList::ReconstructOxygenPositions(Real last_psi) {
  if (empty()) return;
  // parametrization from CHARMM27 forcefield
  const Real d_c_o = 1.230;
  // set until last-1
  const uint last_idx = size() - 1;
  for (uint i = 0; i < last_idx; ++i) {
    const geom::Vec3 c_pos = GetC(i);
    const geom::Vec3 v_one = geom::Normalize(c_pos - GetCA(i));
    const geom::Vec3 v_two = geom::Normalize(c_pos - GetN(i+1));
    SetO(i, c_pos + geom::Normalize(v_one + v_two) * d_c_o);
  }

  // the last oxygen has still to be constructed...
  // we assume, that ca, c, o and n of the next residue are in one plane
  // if the dihedral n-ca-c-n has angle last_psi, the dihedral n-ca-c-o 
  // has therefore angle psi + pi
  geom::Vec3 o_pos;
  core::ConstructAtomPos(GetN(last_idx), GetCA(last_idx), GetC(last_idx),
                         d_c_o, 2*Real(M_PI)/3, last_psi + Real(M_PI), o_pos);
  SetO(last_idx, o_pos);
}

void BackboneList::ReconstructCStemOxygen(const ost::mol::ResidueHandle&
                                           after_c_stem) {
  // same logic as in ReconstructOxygenPositions
  if (after_c_stem.IsValid()) {
    const uint last_idx = size() - 1;
    const Real d_c_o = 1.230;
    ost::mol::AtomHandle after_c_stem_n = after_c_stem.FindAtom("N");
    if (after_c_stem_n.IsValid()) {
      const geom::Vec3 c_pos = GetC(last_idx);
      const geom::Vec3 v_one = geom::Normalize(c_pos - GetCA(last_idx));
      const geom::Vec3 v_two = geom::Normalize(c_pos - after_c_stem_n.GetPos());
      SetO(last_idx, c_pos + geom::Normalize(v_one + v_two) * d_c_o);
    }
  }
}

void BackboneList::SetPhiTorsion(uint index, Real angle, bool sequential) {
  if(index<1 || index >= this->size()){
    throw promod3::Error("Cannot apply phi torsion at this position.");
  }

  while (angle >= Real(M_PI)) angle -= 2*Real(M_PI);
  while (angle < -Real(M_PI)) angle += 2*Real(M_PI);

  Real actual_angle = geom::DihedralAngle(GetC(index-1), GetN(index),
                                          GetCA(index), GetC(index));
  RotateAroundPhiTorsion(index,angle-actual_angle,sequential);
}

void BackboneList::SetPsiTorsion(uint index, Real angle, bool sequential){
  if(index >= this->size()-1 ){
    throw promod3::Error("Cannot apply psi torsion at this position.");
  }

  while (angle >= Real(M_PI)) angle -= 2*Real(M_PI);
  while (angle < -Real(M_PI)) angle += 2*Real(M_PI);

  Real actual_angle = geom::DihedralAngle(GetN(index), GetCA(index),
                                          GetC(index), GetN(index+1));
  RotateAroundPsiTorsion(index,angle-actual_angle, sequential);
}

void BackboneList::SetOmegaTorsion(uint index, Real angle, bool sequential){
  if(index >= this->size()-1 ){
    throw promod3::Error("Cannot apply psi torsion at this position.");
  }

  while (angle >= Real(M_PI)) angle -= 2*Real(M_PI);
  while (angle < -Real(M_PI)) angle += 2*Real(M_PI);

  Real actual_angle = geom::DihedralAngle(GetCA(index), GetC(index), 
                                          GetN(index+1), GetCA(index+1));
  RotateAroundOmegaTorsion(index,angle-actual_angle, sequential);
}

void BackboneList::SetPhiPsiTorsion(uint index, Real phi, Real psi, bool sequential){

  if(index == 0 || index >= this->size()-1 ){
    throw promod3::Error("Cannot set phi psi torsions at this position.");
  }

  while (phi >= Real(M_PI)) phi -= 2*Real(M_PI);
  while (phi < -Real(M_PI)) phi += 2*Real(M_PI);

  while (psi >= Real(M_PI)) psi -= 2*Real(M_PI);
  while (psi < -Real(M_PI)) psi += 2*Real(M_PI);

  Real actual_phi = geom::DihedralAngle(GetC(index-1), GetN(index),
                                        GetCA(index), GetC(index));

  Real actual_psi = geom::DihedralAngle(GetN(index), GetCA(index), 
                                        GetC(index), GetN(index+1));

  RotateAroundPhiPsiTorsion(index, phi-actual_phi, psi-actual_psi, sequential);
}

void BackboneList::RotateAroundPhiTorsion(uint index, Real angle,
                                          bool sequential) {
  if (index<1 || index >= this->size()) {
    throw promod3::Error("Cannot apply phi torsion at this position.");
  }
  if (!sequential && index < this->size()/2) {
    const geom::Vec3 rot_anchor = GetN(index);
    const geom::Vec3 rot_axis = rot_anchor - GetCA(index);
    geom::Mat4 transformation = core::RotationAroundLine(rot_axis, rot_anchor,
                                                         angle);
    ApplyTransform(0, index, transformation);
  } else {
    const geom::Vec3 rot_anchor = GetCA(index);
    const geom::Vec3 rot_axis = rot_anchor - GetN(index);
    geom::Mat4 transformation = core::RotationAroundLine(rot_axis, rot_anchor,
                                                         angle);

    // rotate the atoms from current residue
    bb_list_[index].ApplyTransform(2, transformation); // CB
    bb_list_[index].ApplyTransform(3, transformation); // C
    bb_list_[index].ApplyTransform(4, transformation); // O

    // rotate all remaining positions
    ApplyTransform(index+1, size(), transformation);
  }
}

void BackboneList::RotateAroundPsiTorsion(uint index, Real angle,
                                          bool sequential) {
  if (index >= this->size()-1) {
    throw promod3::Error("Cannot apply psi torsion at this position.");
  }
  if (!sequential && index < this->size()/2) {
    const geom::Vec3 rot_anchor = GetCA(index);
    const geom::Vec3 rot_axis = rot_anchor - GetC(index);
    geom::Mat4 transformation = core::RotationAroundLine(rot_axis, rot_anchor,
                                                         angle);
    // rotate the atoms from current residue
    bb_list_[index].ApplyTransform(0, transformation); //N
    bb_list_[index].ApplyTransform(2, transformation); //CB

    // rotate all the remaining positions
    ApplyTransform(0, index, transformation);
  } else {
    const geom::Vec3 rot_anchor = GetC(index);
    const geom::Vec3 rot_axis = rot_anchor - GetCA(index);
    geom::Mat4 transformation = core::RotationAroundLine(rot_axis, rot_anchor,
                                                         angle);
    // rotate the atoms from current residue
    bb_list_[index].ApplyTransform(4, transformation); // O 

    // rotate all remaining positions
    ApplyTransform(index+1, size(), transformation);
  }
}

void BackboneList::RotateAroundOmegaTorsion(uint index, Real angle,
                                            bool sequential) {
  if (index >= this->size()-1) {
    throw promod3::Error("Cannot apply omega torsion at this position.");
  }
  if (!sequential && index < this->size()/2) {
    const geom::Vec3 rot_anchor = GetC(index);
    const geom::Vec3 rot_axis = rot_anchor - GetN(index+1);
    geom::Mat4 transformation = core::RotationAroundLine(rot_axis, rot_anchor,
                                                         angle);
    // rotate the atoms from current residue
    bb_list_[index].ApplyTransform(0, transformation); //N
    bb_list_[index].ApplyTransform(1, transformation); //CA
    bb_list_[index].ApplyTransform(2, transformation); //CB
    bb_list_[index].ApplyTransform(4, transformation); //O

    // rotate all the remaining positions
    ApplyTransform(0, index, transformation);
  } else {
    const geom::Vec3 rot_anchor = GetN(index+1);
    const geom::Vec3 rot_axis = rot_anchor - GetC(index);
    geom::Mat4 transformation = core::RotationAroundLine(rot_axis, rot_anchor,
                                                         angle);
    // rotate all remaining positions
    ApplyTransform(index+1, size(), transformation);
  }
}

void BackboneList::RotateAroundPhiPsiTorsion(uint index, Real phi, Real psi,
                                             bool sequential) {
  if (index == 0 || index >= this->size()-1) {
    throw promod3::Error("Cannot apply phi and psi torsion at this position.");
  }
  if (!sequential && index < this->size()/2) {
    // get phi transform 
    geom::Vec3 rot_anchor = GetN(index);
    geom::Vec3 rot_axis = rot_anchor - GetCA(index);
    geom::Mat4 phi_t = core::RotationAroundLine(rot_axis, rot_anchor, phi);
    // get psi transform
    rot_anchor = GetCA(index);
    rot_axis = rot_anchor - GetC(index);
    geom::Mat4 psi_t = core::RotationAroundLine(rot_axis, rot_anchor, psi);
    // rotate the atoms from current residue
    bb_list_[index].ApplyTransform(0, psi_t); //N
    bb_list_[index].ApplyTransform(2, psi_t); //CB  

    // let's finally get the overall transform
    // we first apply the phi transform, then the psi transform
    geom::Mat4 t = psi_t * phi_t;
    ApplyTransform(0, index, t);
  } else {
    // get phi transform
    geom::Vec3 rot_anchor = GetCA(index);
    geom::Vec3 rot_axis = rot_anchor - GetN(index);
    geom::Mat4 phi_t = core::RotationAroundLine(rot_axis, rot_anchor, phi);
    // get psi transform
    rot_anchor = GetC(index);
    rot_axis = rot_anchor - GetCA(index);
    geom::Mat4 psi_t = core::RotationAroundLine(rot_axis, rot_anchor, psi);

    // rotate the atoms from current residue
    bb_list_[index].ApplyTransform(2, phi_t); // CB
    bb_list_[index].ApplyTransform(3, phi_t); // C

    // let's finally get the overall transform
    // we first apply the psi transform, then the phi transform
    geom::Mat4 t = phi_t * psi_t;

    // oxygen needs both transformations to be applied
    bb_list_[index].ApplyTransform(4, t); // O

    // rotate all remaining positions
    ApplyTransform(index+1, size(), t);
  }
}

bool BackboneList::TransOmegaTorsions(Real thresh, bool allow_prepro_cis) const
{
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneList::TransOmegaTorsions", 2);

  int size = this->size();

  if (size < 3) {
    throw promod3::Error("Size of Backbone list must be at least 3!");
  }

  Real angle;
  for (int i = 0; i < size-1; ++i) {
    angle = this->GetOmegaTorsion(i);
    if (std::abs(angle) < Real(M_PI)-thresh) {
      if (allow_prepro_cis && this->GetAA(i+1) == ost::conop::PRO) {
        if (angle < thresh) continue;
      }
      return false;
    }
  }
  return true;
}

void BackboneList::SetBackrub(uint index, Real primary_rot_angle, 
                            Real flanking_rot_angle_one, 
                            Real flanking_rot_angle_two){

  if(index < 1 || index > this->size()-2){
    throw promod3::Error("Index for Backrub must be larger 1 or smaller size "
                         "of BackboneList - 2!");
  }

  geom::Vec3 ca_after = this->GetCA(index + 1);
  geom::Vec3 ca_before = this->GetCA(index - 1);
  geom::Vec3 ca_central = this->GetCA(index);
  geom::Vec3 primary_axis = ca_after - ca_before;
  geom::Mat4 primary_rotation = core::RotationAroundLine(primary_axis, 
                                                         ca_before, 
                                                         primary_rot_angle);

  //Only the CA and CB positions of the central residue get transformed only by
  //the primary rotation
  bb_list_[index].ApplyTransform(1, primary_rotation); //CA
  bb_list_[index].ApplyTransform(2, primary_rotation); //CB

  //let's get the transformed CA to construct the flanking rotation axis
  ca_central = this->GetCA(index);
  geom::Vec3 flanking_axis_one = ca_central - ca_before;
  geom::Vec3 flanking_axis_two = ca_after - ca_central;

  geom::Mat4 flanking_rotation_one = 
  core::RotationAroundLine(flanking_axis_one, ca_before, 
                           flanking_rot_angle_one);

  geom::Mat4 flanking_rotation_two = 
  core::RotationAroundLine(flanking_axis_two, ca_central, 
                           flanking_rot_angle_two);

  //we directly combine the transformations...
  flanking_rotation_one = flanking_rotation_one * primary_rotation;
  flanking_rotation_two = flanking_rotation_two * primary_rotation;

  //from residue with at position index - 1 we have to transform C and O
  bb_list_[index-1].ApplyTransform(3, flanking_rotation_one); //C
  bb_list_[index-1].ApplyTransform(4, flanking_rotation_one); //O 

  //from residue at position index we have to transform N with the first
  //flanking rotation and C and O with the second flanking rotation
  bb_list_[index].ApplyTransform(0, flanking_rotation_one); //N 
  bb_list_[index].ApplyTransform(3, flanking_rotation_two); //C
  bb_list_[index].ApplyTransform(4, flanking_rotation_two); //O

  //from the residue at position index + 1 we have to transform N
  bb_list_[index+1].ApplyTransform(0,flanking_rotation_two);
}

void BackboneList::SetBackrub(uint index, Real primary_rot_angle, Real scaling){

  if(index < 1 || index > this->size()-2){
    throw promod3::Error("Index for Backrub must be larger 1 or smaller size "
                         "of BackboneList - 2!");
  }

  geom::Vec3 ca_after = this->GetCA(index + 1);
  geom::Vec3 ca_before = this->GetCA(index - 1);
  geom::Vec3 ca_central = this->GetCA(index);
  geom::Vec3 primary_axis = ca_after - ca_before;
  geom::Mat4 primary_rotation = core::RotationAroundLine(primary_axis, 
                                                         ca_before, 
                                                         primary_rot_angle);

  //before applying any transformation we want to extract the 
  //flanking oxygen positions
  geom::Vec3 old_o_one = this->GetO(index-1);
  geom::Vec3 old_o_two = this->GetO(index);

  //Let's apply the initial transform
  bb_list_[index-1].ApplyTransform(3, primary_rotation); //C
  bb_list_[index-1].ApplyTransform(4, primary_rotation); //O

  bb_list_[index].ApplyTransform(0, primary_rotation); //N
  bb_list_[index].ApplyTransform(1, primary_rotation); //CA
  bb_list_[index].ApplyTransform(2, primary_rotation); //CB
  bb_list_[index].ApplyTransform(3, primary_rotation); //C
  bb_list_[index].ApplyTransform(4, primary_rotation); //O

  bb_list_[index+1].ApplyTransform(0, primary_rotation); //N

  //let's get the transformed CA to construct the flanking rotation axis
  ca_central = this->GetCA(index);
  geom::Vec3 flanking_axis_one = ca_central - ca_before;
  geom::Vec3 flanking_axis_two = ca_after - ca_central;

  //extract the flanking oxygen positions with the primary
  //transform applied
  geom::Vec3 new_o_one = this->GetO(index-1);
  geom::Vec3 new_o_two = this->GetO(index);

  //we have now to find the flanking rotation angles, that minimize the 
  //distance between old_o and new_o...
  Real flanking_rot_angle_one = this->ConstructFlankingAngle(new_o_one,
                                                             old_o_one,
                                                             ca_before,
                                                             flanking_axis_one);

  Real flanking_rot_angle_two = this->ConstructFlankingAngle(new_o_two,
                                                             old_o_two,
                                                             ca_central,
                                                             flanking_axis_two);

  //apply the scaling to the rot angle
  flanking_rot_angle_one *= scaling;
  flanking_rot_angle_two *= scaling;

  geom::Mat4 flanking_rotation_one = 
  core::RotationAroundLine(flanking_axis_one, ca_before, 
                           flanking_rot_angle_one);

  geom::Mat4 flanking_rotation_two = 
  core::RotationAroundLine(flanking_axis_two, ca_central, 
                           flanking_rot_angle_two);

  //from residue with at position index - 1 we have to transform C and O
  bb_list_[index-1].ApplyTransform(3, flanking_rotation_one); //C
  bb_list_[index-1].ApplyTransform(4, flanking_rotation_one); //O 

  //from residue at position index we have to transform N with the first
  //flanking rotation and C and O with the second flanking rotation
  bb_list_[index].ApplyTransform(0, flanking_rotation_one); //N 
  bb_list_[index].ApplyTransform(3, flanking_rotation_two); //C
  bb_list_[index].ApplyTransform(4, flanking_rotation_two); //O

  //from the residue at position index + 1 we have to transform N
  bb_list_[index+1].ApplyTransform(0,flanking_rotation_two);
}

Real BackboneList::CARMSD(const BackboneList& other,
                          bool superposed_rmsd) const {

  if (size() != other.size()) {
    throw promod3::Error("Inconsistent sizes when calculating ca rmsd between "
                         "BackboneLists");
  }

  if(size() < 3) {
    throw promod3::Error("Length of BackboneList must be at least 3 to "
                         "estimate a meaningful CARMSD!");
  }

  if (superposed_rmsd) {
    // superpose CA positions
    core::EMatX3 pos_one, pos_two;
    BackboneList::ExtractEigenCAPos(*this, pos_one);
    BackboneList::ExtractEigenCAPos(other, pos_two);
    return core::SuperposedRMSD(pos_one, pos_two);

  } else {

    Real rmsd = 0.0;
    for(uint i = 0; i < size(); ++i){
      rmsd += geom::Length2(GetCA(i) - other.GetCA(i));
    }
    return std::sqrt(rmsd/size());
  }
}

Real BackboneList::RMSD(const BackboneList& other, bool superposed_rmsd) const {

  if (size() != other.size()) {
    throw promod3::Error("Inconsistent sizes when calculating rmsd between "
                         "BackboneLists");
  }

  if (empty()) return 0.0;

  if (superposed_rmsd) {
    core::EMatX3 pos_one, pos_two;
    BackboneList::ExtractEigenPos(*this, pos_one);
    BackboneList::ExtractEigenPos(other, pos_two);
    return core::SuperposedRMSD(pos_one, pos_two);
  } else {

    Real rmsd = 0.0;
    for(uint i = 0; i < size(); ++i){
      rmsd += geom::Length2(GetN(i) - other.GetN(i));    
      rmsd += geom::Length2(GetCA(i) - other.GetCA(i));
      rmsd += geom::Length2(GetCB(i) - other.GetCB(i));
      rmsd += geom::Length2(GetC(i) - other.GetC(i));
      rmsd += geom::Length2(GetO(i) - other.GetO(i));
    }
    return std::sqrt(rmsd/(size()*5));
  }
}

geom::Mat4 BackboneList::GetTransform(const BackboneList& other) const {

  if(empty()) {
    throw promod3::Error("Cannot get transform for empty BackboneList!");
  }

  if (size() != other.size()) {
    throw promod3::Error("Sizes of backbone lists must match in order to "
                         "calculate the transform between them!");
  }

  core::EMatX3 pos_one, pos_two;

  if(size() >= 3) {
    // extract CA positions
    BackboneList::ExtractEigenCAPos(*this, pos_one);
    BackboneList::ExtractEigenCAPos(other, pos_two);
    
  } else {
    // superpose N, CA, C positions
    BackboneList::ExtractEigenNCACPos(*this, pos_one);
    BackboneList::ExtractEigenNCACPos(other, pos_two);   
  }

  return core::MinRMSDSuperposition(pos_one, pos_two);
}

geom::Mat4 BackboneList::GetTransformIterative(const BackboneList& other,
                                               uint max_iterations,
                                               Real distance_thresh) const {

  if(empty()) {
    throw promod3::Error("Cannot get transform for empty BackboneList!");
  }

  if (size() != other.size()) {
    throw promod3::Error("Sizes of backbone lists must match in order to "
                         "calculate the transform between them!");
  }

  core::EMatX3 pos_one, pos_two;

  if(size() >= 3) {
    // superpose CA positions
    BackboneList::ExtractEigenCAPos(*this, pos_one);
    BackboneList::ExtractEigenCAPos(other, pos_two);
  } else {
    // superpose N, CA, C positions
    BackboneList::ExtractEigenNCACPos(*this, pos_one);
    BackboneList::ExtractEigenNCACPos(other, pos_two);
  }

  std::vector<uint> index_vec;
  geom::Mat4 t = core::MinRMSDSuperposition(pos_one, pos_two, max_iterations,
                                            distance_thresh, index_vec, false);
  if(index_vec.size() >= 3) {
    return t;
  } else {
    // iterative superposition did not converge, let's use normal superposition
    // as fallback
    return core::MinRMSDSuperposition(pos_one, pos_two);
  }
}
///////////////////////////////////////////////////////////////////////////////
// BackboneList private
void BackboneList::ConstructBackboneList_(const String& sequence, 
                                          const std::vector<Real>& phi_angles,
                                          const std::vector<Real>& psi_angles,
                                          const std::vector<Real>& omega_angles)
{
  // Parameters required to build a backbone trace with N, CA and C atoms.
  // The amino acid specific parameters get extracted with BBTraceParam(),
  // that contains a lookup table of idealized parameters extracted 
  // from the CHARMM36 forcefield.
  Real d_n_ca = 0.0;
  Real d_ca_c = 0.0;
  Real d_c_n = 0.0;
  Real a_c_n_ca = 0.0;
  Real a_n_ca_c = 0.0;
  Real a_ca_c_n = 0.0;

  // constuct minimal backbone containing n, ca and c atoms
  uint seq_size = sequence.size();
  geom::Vec3List backbone_positions;
  backbone_positions.resize(seq_size*3);

  // start with first residue
  BBTraceParam(sequence[0], d_n_ca, d_ca_c, d_c_n, 
               a_c_n_ca, a_n_ca_c, a_ca_c_n);

  backbone_positions[0] = geom::Vec3(0.0, 0.0, 0.0);
  backbone_positions[1] = geom::Vec3(d_n_ca, 0.0, 0.0);
  backbone_positions[2] = geom::Vec3(
    d_n_ca + d_ca_c * std::cos(Real(M_PI) - a_n_ca_c),
    d_ca_c * std::sin(Real(M_PI) - a_n_ca_c), 0.0);

  if(seq_size > 1){
     core::ConstructAtomPos(backbone_positions[0],
                            backbone_positions[1],
                            backbone_positions[2],
                            d_c_n, a_ca_c_n, psi_angles[0],
                            backbone_positions[3]);   
  }
  

  for(uint i = 1; i < seq_size; ++i){

    // start with first residue
    BBTraceParam(sequence[i], d_n_ca, d_ca_c, d_c_n, 
                 a_c_n_ca, a_n_ca_c, a_ca_c_n);

    // construct CA
    core::ConstructAtomPos(backbone_positions[(i-1)*3+1],
                           backbone_positions[(i-1)*3+2],
                           backbone_positions[i*3],
                           d_n_ca, a_c_n_ca, omega_angles[i-1],
                           backbone_positions[i*3+1]);
    // construct C
    core::ConstructAtomPos(backbone_positions[(i-1)*3+2],
                           backbone_positions[i*3],
                           backbone_positions[i*3+1],
                           d_ca_c, a_n_ca_c, phi_angles[i],
                           backbone_positions[i*3+2]);

    if(i < seq_size - 1){
      // construct N
      core::ConstructAtomPos(backbone_positions[i*3],
                             backbone_positions[i*3+1],
                             backbone_positions[i*3+2],
                             d_c_n, a_ca_c_n, psi_angles[i],
                             backbone_positions[i*3+3]);
    }
  }

  // let's create the backbone... we use the simple cb reconstruction implemented
  // in PROMOD3 at this point... the oxygens will be positioned in a last step

  resize(seq_size);
  for (uint i = 0; i < seq_size; ++i) {
    geom::Vec3 cb_pos;
    core::ConstructCBetaPos(backbone_positions[i*3],
                            backbone_positions[i*3+1],
                            backbone_positions[i*3+2], cb_pos);
    SetN(i, backbone_positions[i*3]);
    SetCA(i, backbone_positions[i*3+1]);
    SetC(i, backbone_positions[i*3+2]);
    SetCB(i, cb_pos);
    SetOLC(i, sequence[i]);
    SetAA(i, ost::conop::OneLetterCodeToAminoAcid(sequence[i]));
  }

  // oxygens get constructed in a last step
  ReconstructOxygenPositions(psi_angles.back());
}

Real BackboneList::ConstructFlankingAngle(const geom::Vec3& actual_o,
                                          const geom::Vec3& target_o,
                                          const geom::Vec3& anchor_ca,
                                          const geom::Vec3& rot_axis) const{

  geom::Vec3 n_axis = geom::Normalize(rot_axis);
  geom::Vec3 o = geom::Dot(actual_o - anchor_ca, n_axis) * n_axis + anchor_ca;
  geom::Vec3 f = target_o - o;
  geom::Vec3 r = geom::Normalize(actual_o - o);
  geom::Vec3 s = geom::Normalize(geom::Cross(n_axis, r));
  return std::atan2(geom::Dot(f,s), geom::Dot(f,r));
}

void BackboneList::ExtractEigenCAPos(const BackboneList& bb_list, 
                                     core::EMatX3& pos) {
  pos = core::EMatX3::Zero(bb_list.size(), 3);
  for (uint i = 0; i < bb_list.size(); ++i) {
    core::EMatFillRow(pos, i, bb_list.GetCA(i));
  }
}

void BackboneList::ExtractEigenNCACPos(const BackboneList& bb_list, 
                                       core::EMatX3& pos) {
  pos = core::EMatX3::Zero(bb_list.size() * 3, 3);
  
  for (uint i = 0; i < bb_list.size(); ++i) {
    core::EMatFillRow(pos, i*3, bb_list.GetN(i));
    core::EMatFillRow(pos, i*3+1, bb_list.GetCA(i));
    core::EMatFillRow(pos, i*3+2, bb_list.GetC(i));
  }
}

void BackboneList::ExtractEigenPos(const BackboneList& bb_list, 
                                   core::EMatX3& pos) {
  pos = core::EMatX3::Zero(bb_list.size() * 5, 3);

  for (uint i = 0; i < bb_list.size(); ++i) {
    core::EMatFillRow(pos, i*5, bb_list.GetN(i));
    core::EMatFillRow(pos, i*5+1, bb_list.GetCA(i));
    core::EMatFillRow(pos, i*5+2, bb_list.GetC(i));
    core::EMatFillRow(pos, i*5+3, bb_list.GetCB(i));
    core::EMatFillRow(pos, i*5+4, bb_list.GetO(i));
  }
}

}} // PM3-ns
