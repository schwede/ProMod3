// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


/// \brief Defines functionality to construct sidechain heavy atoms based on 
///        dihedral angles for amino acids.

#ifndef PROMOD3_SIDECHAIN_ATOM_CONSTRUCTOR_HH
#define PROMOD3_SIDECHAIN_ATOM_CONSTRUCTOR_HH

#include <promod3/loop/all_atom_positions.hh>
#include <promod3/loop/sidechain_atom_rule_lookup.hh>
#include <limits>

namespace promod3{ namespace loop{

void ConstructSidechainAtoms(AllAtomPositions& all_pos, uint res_idx,
                             Real chi1 = std::numeric_limits<Real>::quiet_NaN(),
                             Real chi2 = std::numeric_limits<Real>::quiet_NaN(),
                             Real chi3 = std::numeric_limits<Real>::quiet_NaN(),
                             Real chi4 = std::numeric_limits<Real>::quiet_NaN());

Real GetChiAngle(const AllAtomPositions& all_pos, uint res_idx, uint chi_idx);

Real GetChi1Angle(const AllAtomPositions& all_pos, uint res_idx);

Real GetChi2Angle(const AllAtomPositions& all_pos, uint res_idx);

Real GetChi3Angle(const AllAtomPositions& all_pos, uint res_idx);

Real GetChi4Angle(const AllAtomPositions& all_pos, uint res_idx);


}}

#endif
