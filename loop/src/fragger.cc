// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/fragger.hh>

// for raw serialization
#include <ost/io/binary_data_source.hh>
#include <ost/io/binary_data_sink.hh>
#include <ost/io/container_serialization.hh>
#include <fstream>

namespace{

const Real ss_agreements[8][3][10] = 
                        {{{0.0743092447519,0.108941435814,0.195960968733,
                           0.337140411139, 0.425812125206,0.543472349644,
                           0.661104083061,0.789981544018, 0.912394762039,
                           1.07248711586},
                          {-1.64370155334,-1.79595589638,-1.94891047478,
                           -1.96786797047,-2.36278915405,-2.55591487885,
                           -2.87808513641,-3.47547626495,-3.98427605629,
                           -5.64885807037},
                          {-0.544677555561,-0.779035568237,-0.77005892992,
                           -1.03206443787,-1.26424705982,-1.48530709743,
                           -1.83679986,-2.36390447617,
                           -2.97444701195,-4.1738986969}},
                         {{-1.11703908443,-1.07918512821,-1.25778782368,
                           -1.50218391418,-1.75994050503,-2.17622733116,
                           -2.32324171066,-2.97105693817,-3.37147068977,
                           -5.31579208374},
                          {0.750329315662,0.849426627159,0.912194311619,
                           0.970754146576,1.05472362041,1.11723971367,
                           1.19878709316,1.29728412628,1.38588225842,
                           1.47694754601},
                          {-0.0678861141205,0.0303211919963,-0.120359390974,
                           -0.226397946477,-0.350316345692,-0.538187503815,
                           -0.757252454758,-1.02170014381,-1.48863530159,
                           -2.44456338882}},
                         {{-0.119507029653,-0.172990471125,-0.352041393518,
                           -0.434395909309,-0.55849725008,-0.806809127331,
                           -1.04099702835,-1.38133561611,-1.93705511093,
                           -3.50974106789},
                          {0.3128657341,0.270476609468,0.206361323595,
                           0.146568775177,0.0670324862003,-0.0262309163809,
                           -0.212483048439,-0.463072091341,-1.00368797779,
                           -2.30808115005},
                          {0.180694058537,0.310992240906,0.370765715837,
                           0.436445981264,0.513785243034,0.590631008148,
                           0.671290814877,0.748263478279,0.795238792896,
                           0.821499228477}},
                         {{1.02313637733,1.08071899414,1.14563918114,
                           1.07327151299,1.15058779716,1.05704510212,
                           1.01562321186,0.825398027897,0.532406628132,
                           -0.905648589134},
                          {-0.301836967468,-0.377451807261,-0.315694093704,
                           -0.56239336729,-0.801452219486,-1.03933620453,
                           -1.37189328671,-1.94911396503,-2.74501252174,
                           -3.99086308479},
                          {0.837272584438,0.648295938969,0.605300545692,
                           0.650563180447,0.629740417004,0.516098201275,
                           0.342855125666,0.093064814806,-0.433632612228,
                           -1.16891598701}},
                         {{0.116117276251,0.201020091772,-0.0972542092204,
                           -0.0303701441735,-0.254964113235,-0.505028486252,
                           -0.937196671963,-1.56948208809,-2.15329885483,
                           -4.40864515305},
                          {0.59810769558,0.474071443081,0.50852137804,
                           0.482196033001,0.507676005363,0.382976412773,
                           0.600304543972,0.215302869678,-0.0416598133743,
                           -1.59244310856},
                          {0.834200918674,0.800064682961,0.761974215508,
                           0.833927571774,0.766715407372,0.74843031168,
                           0.733701407909,0.562983155251,0.315246403217,
                           -0.286427915096}},
                         {{-0.0280654467642,-0.0565700419247,-0.171910837293,
                           -0.307654708624,-0.497547507286,-0.743263840675,
                           -0.943607032299,-1.35142934322,-1.84449696541,
                           -3.61821937561},
                          {-0.0244269836694,-0.140174359083,-0.25533798337,
                           -0.274367451668,-0.440044075251,-0.605534315109,
                           -0.824404835701,-1.29827606678,-1.77194535732,
                           -3.58129119873},
                          {0.11961632967,0.201561167836,0.294322878122,
                           0.411768943071,0.492409586906,0.596683323383,
                           0.719297587872,0.832410573959,0.956784129143,
                           0.857286930084}},
                         {{0.499598443508,0.435712516308,0.493463039398,
                           0.388838768005,0.309403896332,0.307138592005,
                           0.0605980306864,-0.162343963981,-0.624721944332,
                           -2.12761187553},
                          {-0.606510996819,-0.880988955498,-0.976592063904,
                           -1.24563848972,-1.39992249012,-1.49151813984,
                           -1.93427443504,-2.49963355064,-3.1217815876,
                           -4.99877405167},
                          {0.259083896875,0.222127184272,0.278366446495,
                           0.378224879503,0.453232705593,0.519194841385,
                           0.563889801502,0.61486685276,0.70871335268,
                           0.98008453846}},
                         {{0.0743092447519,0.108941435814,0.195960968733,
                           0.337140411139,0.425812125206,0.543472349644,
                           0.661104083061,0.789981544018,0.912394762039,
                           1.07248711586},
                          {-1.64370155334,-1.79595589638,-1.94891047478,
                           -1.96786797047,-2.36278915405,-2.55591487885,
                           -2.87808513641,-3.47547626495,-3.98427605629,
                           -5.64885807037},
                          {-0.544677555561,-0.779035568237,-0.77005892992,
                           -1.03206443787,-1.26424705982,-1.48530709743,
                           -1.83679986,-2.36390447617,-2.97444701195,
                           -4.1738986969}}};

inline int GetDSSPIdx(char dssp_state){
  switch(dssp_state){
    case 'H': return 0;
    case 'E': return 1;
    case 'C': return 2;
    case 'G': return 3;
    case 'B': return 4;
    case 'S': return 5;
    case 'T': return 6;
    case 'I': return 7;
    default: throw promod3::Error("Invalid dssp state observed!");
  }
}

inline int GetPsipredIdx(char psipred_state){
  switch(psipred_state){
    case 'H': return 0;
    case 'E': return 1;
    case 'C': return 2;
    default: throw promod3::Error("Invalid psipred state observed!");
  }
}

}



namespace promod3 { namespace loop {

Fragger::Fragger(const String& sequence){ 
  if(sequence.size() < 3){
    throw promod3::Error("Sequence size must at least be 3 to set up Fragger!");
  }
  frag_sequence_ = sequence;
}

Fragger::~Fragger() {
  // clean up profiles if needed
  for (uint i = 0; i < profiles_.size(); ++i) {
    if (profiles_[i] != NULL) {
      for (uint j = 0; j < frag_sequence_.size(); ++j) {
        delete profiles_[i][j];
      }
      delete profiles_[i];
    }
  }
}

void Fragger::Fill(StructureDBPtr db, Real max_rmsd, uint num_fragments){

  //check, whether there are any score parameters set
  if(score_types_.empty()){
    throw promod3::Error("Must set the parametrization of at least one score "
                         "type to fill Fragger!");
  }

  this->LinearFill(db, max_rmsd, num_fragments);
}

void Fragger::AddSeqIDParameters(Real w){
  score_types_.push_back(SeqID);
  weights_.push_back(w);
  subst_matrix_.push_back(ost::seq::alg::SubstWeightMatrix());
  psipred_prediction_.push_back(std::vector<int>());
  psipred_confidence_.push_back(std::vector<int>());
  t_sampler_.push_back(TorsionSamplerList());
  t_sampler_indices_.push_back(std::vector<int>());
  profiles_.push_back(NULL);
}

void Fragger::AddSeqSimParameters(Real w, 
                                ost::seq::alg::SubstWeightMatrix& subst_matrix){
  score_types_.push_back(SeqSim);
  weights_.push_back(w);
  subst_matrix_.push_back(subst_matrix);
  psipred_prediction_.push_back(std::vector<int>());
  psipred_confidence_.push_back(std::vector<int>());
  t_sampler_.push_back(TorsionSamplerList());
  t_sampler_indices_.push_back(std::vector<int>());
  profiles_.push_back(NULL);
}

void Fragger::AddSSAgreeParameters(Real w, const PsipredPrediction& pp){

  if(pp.size() != frag_sequence_.size()){
    throw promod3::Error("Size of psipred prediction must be consistent with "
                         "fragment sequence!");
  }

  std::vector<int> p_p;
  std::vector<int> p_c;

  for(uint i = 0; i < pp.size(); ++i){
    p_p.push_back(GetPsipredIdx(pp.GetPrediction(i)));
    p_c.push_back(pp.GetConfidence(i));
  }

  score_types_.push_back(SSAgree);
  weights_.push_back(w);
  subst_matrix_.push_back(ost::seq::alg::SubstWeightMatrix());
  psipred_prediction_.push_back(p_p);
  psipred_confidence_.push_back(p_c);
  t_sampler_.push_back(TorsionSamplerList());
  t_sampler_indices_.push_back(std::vector<int>());
  profiles_.push_back(NULL);
}

void Fragger::AddTorsionProbabilityParameters(Real w,
                                              const TorsionSamplerPtr t_s,
                                              const String& before,
                                              const String& after) {

  if(ost::conop::ResidueNameToOneLetterCode(before) == 'X'){
    throw promod3::Error("Provided amino acid before seems to be invalid!");
  }
  if(ost::conop::ResidueNameToOneLetterCode(after) == 'X'){
    throw promod3::Error("Provided amino acid after seems to be invalid!");
  }

  TorsionSamplerList v_t_s;
  v_t_s.assign(frag_sequence_.size(),t_s);

  std::vector<int> indices;
  this->FillTorsionSamplerIndices(before,after,v_t_s,indices);

  score_types_.push_back(TorsionProb);
  weights_.push_back(w);
  subst_matrix_.push_back(ost::seq::alg::SubstWeightMatrix());
  psipred_prediction_.push_back(std::vector<int>());
  psipred_confidence_.push_back(std::vector<int>());
  t_sampler_.push_back(v_t_s);
  t_sampler_indices_.push_back(indices);
  profiles_.push_back(NULL);
} 

void Fragger::AddTorsionProbabilityParameters(Real w,
                                              const TorsionSamplerList t_s, 
                                              const String& before,
                                              const String& after) {
  if(t_s.size() != frag_sequence_.size()){
    throw promod3::Error("Number of torsion samplers must be consistent with "
                         "fragment sequence!");
  }
  if(ost::conop::ResidueNameToOneLetterCode(before) == 'X'){
    throw promod3::Error("Provided amino acid before seems to be invalid!");
  }
  if(ost::conop::ResidueNameToOneLetterCode(after) == 'X'){
    throw promod3::Error("Provided amino acid after seems to be invalid!");
  }

  int bins_per_dimension = t_s[0]->GetBinsPerDimension();
  for (TorsionSamplerList::const_iterator i = t_s.begin(); i != t_s.end(); ++i)
  {
    if ((*i)->GetBinsPerDimension() != bins_per_dimension) {
      throw promod3::Error("All torsion samplers must have equivalent "
                           "binning when adding them to the Fragger!");
    }
  }

  std::vector<int> indices;
  this->FillTorsionSamplerIndices(before,after,t_s,indices);

  score_types_.push_back(TorsionProb);
  weights_.push_back(w);
  subst_matrix_.push_back(ost::seq::alg::SubstWeightMatrix());
  psipred_prediction_.push_back(std::vector<int>());
  psipred_confidence_.push_back(std::vector<int>());
  t_sampler_.push_back(t_s);
  t_sampler_indices_.push_back(indices);
  profiles_.push_back(NULL);
} 

void Fragger::AddSequenceProfileParameters(Real w,
                                           ost::seq::ProfileHandlePtr prof) {
  if(prof->size() != frag_sequence_.size()){
    throw promod3::Error("Size of provided profile must be consistent with "
                         "fragment sequence!");
  }
  score_types_.push_back(SequenceProfile);
  weights_.push_back(w);
  subst_matrix_.push_back(ost::seq::alg::SubstWeightMatrix());
  psipred_prediction_.push_back(std::vector<int>());
  psipred_confidence_.push_back(std::vector<int>());
  t_sampler_.push_back(TorsionSamplerList());
  t_sampler_indices_.push_back(std::vector<int>());

  short** profile = new short*[frag_sequence_.size()];
  for(uint i = 0; i < frag_sequence_.size(); ++i){
    profile[i] = new short[20];
  }

  for(uint i = 0; i < frag_sequence_.size(); ++i){
    Real* freq_ptr = (*prof)[i].freqs_begin();
    for(uint j = 0; j < 20; ++j){
      profile[i][j] = static_cast<short>(freq_ptr[j] * 10000);
    }
  }

  profiles_.push_back(profile);
}

void Fragger::AddStructureProfileParameters(Real w,
                                            ost::seq::ProfileHandlePtr prof) {
  if(prof->size() != frag_sequence_.size()){
    throw promod3::Error("Size of provided profile must be consistent with "
                         "fragment sequence!");
  }
  score_types_.push_back(StructureProfile);
  weights_.push_back(w);
  subst_matrix_.push_back(ost::seq::alg::SubstWeightMatrix());
  psipred_prediction_.push_back(std::vector<int>());
  psipred_confidence_.push_back(std::vector<int>());
  t_sampler_.push_back(TorsionSamplerList());
  t_sampler_indices_.push_back(std::vector<int>());

  short** profile = new short*[frag_sequence_.size()];
  for(uint i = 0; i < frag_sequence_.size(); ++i){
    profile[i] = new short[20];
  }

  for(uint i = 0; i < frag_sequence_.size(); ++i){
    Real* freq_ptr = (*prof)[i].freqs_begin();
    for(uint j = 0; j < 20; ++j){
      profile[i][j] = static_cast<short>(freq_ptr[j] * 10000);
    }
  }

  profiles_.push_back(profile);
}

void Fragger::LinearFill(StructureDBPtr db, Real max_rmsd, uint num_fragments){

  uint num_parameters = score_types_.size();
  uint frag_size = this->fragment_size();
  uint num_chains = db->GetNumCoords();
  std::vector<std::vector<Real> > profiles(num_parameters);
  std::vector<Real> scores;
  Real worst_score = -std::numeric_limits<Real>::max();
  Real actual_weight;
  // pair(combined_score, pair(single_scores, fragment_info))
  typedef std::pair<Real, std::pair<std::vector<Real>, FragmentInfo> >
          fragment_list_value_t;
  typedef std::list<fragment_list_value_t> fragment_list_t;
  fragment_list_t fragment_list;
  fragment_list.assign(num_fragments, 
                       std::make_pair(worst_score, 
                                      std::make_pair(std::vector<Real>(), 
                                                     FragmentInfo(0, 0, 
                                                                  frag_size))));

  for(uint i = 0; i < num_chains; ++i){

    this->FillProfiles(db,profiles,i);

    //calculate the final scores
    uint profile_size = profiles[0].size();
    scores.assign(profile_size,0.0);
    for(uint j = 0; j < num_parameters; ++j){
      const std::vector<Real>& actual_profile = profiles[j];
      actual_weight = weights_[j];
      for(uint k = 0; k < profile_size; ++k){
        scores[k] += actual_weight*actual_profile[k];
      }
    }

    FragmentInfo frag_info(i,0,frag_size);
    Real ca_rmsd;
    fragment_list_t::iterator frag_it;
    bool already_present;

    for(uint j = 0; j < profile_size; ++j){

      //items in fragment_list are sorted, if current candidate has
      //lower score, it won't get added anyway
      if(scores[j] < worst_score) continue;

      frag_info.offset = j;

      //check, whether low RMSD fragment with better score is already present
      frag_it = fragment_list.begin();
      already_present = false;

      if(max_rmsd > Real(0.0)){
        for( ; frag_it != fragment_list.end(); ++frag_it){

          //if we reach a fragment in the list with lower score,
          //we know that there won't be a better scoring fragment
          //with low rmsd
          if(frag_it->first < scores[j]) break;

          ca_rmsd = db->CARMSD(frag_info,frag_it->second.second,false);
          if(ca_rmsd < max_rmsd){
            already_present = true;
            break;
          }
        }
        if(already_present) continue;
      }
      else{
        for( ; frag_it != fragment_list.end(); ++frag_it){
          //find first fragment in fragger with lower score
          if(frag_it->first < scores[j]) break;
        }
      }

      std::vector<Real> single_scores(num_parameters,0.0);
      for(uint k = 0; k < num_parameters; ++k){
        single_scores[k] = profiles[k][j];
      }

      fragment_list.insert(frag_it, std::make_pair(scores[j], 
                                                   std::make_pair(single_scores, 
                                                                  frag_info)));

      if(max_rmsd > Real(0.0)){
        //let's remove all lower scoring fragments with rmsd below max_rmsd
        while(frag_it != fragment_list.end()){
          ca_rmsd = db->CARMSD(frag_info,frag_it->second.second,false);
          if(ca_rmsd < max_rmsd){
            frag_it = fragment_list.erase(frag_it);
            continue;
          }
          ++frag_it;
        }
      }
      //reduce the size if necessary
      while(fragment_list.size() > num_fragments){
        fragment_list.pop_back();
      }

      worst_score = fragment_list.back().first;
    }
  }

  fragments_.clear();
  fragment_infos_.clear();
  scores_.clear();
  BackboneList bb_list;
  // let's fill the stuff into the internal data structures
  for (fragment_list_t::iterator frag_it = fragment_list.begin();
       frag_it != fragment_list.end(); ++frag_it) {
    db->FillBackbone(bb_list, frag_it->second.second, frag_sequence_);
    fragments_.push_back(bb_list);
    fragment_infos_.push_back(frag_it->second.second);
    single_scores_.push_back(frag_it->second.first);
    scores_.push_back(frag_it->first);
  }
}

void Fragger::FillProfiles(StructureDBPtr db,
                           std::vector<std::vector<Real> >& profiles,
                           uint chain_idx) const{

  for(uint i = 0; i < score_types_.size(); ++i){
    switch(score_types_[i]){
      case SeqSim:{
        this->GenerateSeqSimProfile(db, profiles[i], chain_idx, 
                                    subst_matrix_[i]);
        break;
      }
      case SeqID:{
        this->GenerateSeqIDProfile(db, profiles[i], chain_idx);
        break;
      }
      case SSAgree:{
        this->GenerateSSAgreementProfile(db, profiles[i], chain_idx, 
                                         psipred_prediction_[i],
                                         psipred_confidence_[i]);
        break;
      }
      case TorsionProb:{
        this->GenerateTorsionProbabilityProfile(db, profiles[i], chain_idx, 
                                                t_sampler_[i],
                                                t_sampler_indices_[i]);

        break;
      }
      case SequenceProfile:{
        const ost::PagedArray<AAFreq, 65536>& frequencies = 
        db->GetAAFrequencies();
        this->GenerateProfileProfile(db, profiles[i], chain_idx, frequencies, 
                                     profiles_[i]);
        break;
      }
      case StructureProfile:{
        const ost::PagedArray<AAFreq, 65536>& frequencies = 
        db->GetAAStructFrequencies();        
        this->GenerateProfileProfile(db, profiles[i], chain_idx, frequencies, 
                                     profiles_[i]);
        break;
      }
      default: break;
    }
  }
}

void Fragger::FillTorsionSamplerIndices(const String& before,
                                        const String& after,
                                        const TorsionSamplerList& sampler,
                                        std::vector<int>& indices){

  uint seq_size = frag_sequence_.size();
  std::vector<ost::conop::AminoAcid> aa;
  for (uint i = 0; i < seq_size; ++i) {
    aa.push_back(ost::conop::OneLetterCodeToAminoAcid(frag_sequence_[i]));
  }

  //extract the torsion group indices
  indices.clear();
  int idx;
  idx = sampler[0]->GetHistogramIndex(ost::conop::ResidueNameToAminoAcid(before),
                                      aa[0], aa[1]);
  indices.push_back(idx);

  for (uint i = 1; i < seq_size-1; ++i) {
    idx = sampler[i]->GetHistogramIndex(aa[i-1], aa[i], aa[i+1]);
    indices.push_back(idx);
  }
  
  idx = 
  sampler[seq_size-1]->GetHistogramIndex(aa[seq_size-2],aa[seq_size-1],
                                    ost::conop::ResidueNameToAminoAcid(after));
  indices.push_back(idx);
}

void Fragger::GenerateSeqSimProfile(StructureDBPtr db, 
                          std::vector<Real>& profile,
                          uint index, 
                          const ost::seq::alg::SubstWeightMatrix& subst) const{

  const std::vector<CoordInfo>& coord_toc = db->GetCoordToc();
  const ost::PagedArray<char, 65536>& full_db_seq = db->GetSeq();

  profile.clear();

  uint seq_size = frag_sequence_.size();
  uint chain_size = coord_toc[index].size;
  if(seq_size > chain_size) return;

  String db_seq(chain_size,'X');
  uint coord_index = coord_toc[index].offset;
  for(uint i = 0; i < chain_size; ++i, ++coord_index){
    db_seq[i] = full_db_seq[coord_index];
  }

  Real score = 0.0;
  Real factor = 1.0 / seq_size;
  for(uint i = 0; i <= chain_size - seq_size; ++i){
    score = 0.0;
    for(uint j = 0; j < seq_size; ++j){
      score += subst.GetWeight(frag_sequence_[j],db_seq[i+j]);
    }
    profile.push_back(score*factor);
  }

  //normalize to a range [0,1]
  Real min_weight = subst.GetMinWeight();
  Real one_over_range = 1.0/(subst.GetMaxWeight()-min_weight);
  
  for(uint i = 0; i < profile.size(); ++i){
    profile[i] = (profile[i]-min_weight) * one_over_range;
  }
}

void Fragger::GenerateSeqIDProfile(StructureDBPtr db, 
                                   std::vector<Real>& profile,
                                   uint index) const{

  const std::vector<CoordInfo>& coord_toc = db->GetCoordToc();
  const ost::PagedArray<char, 65536>& full_db_seq = db->GetSeq();

  profile.clear();

  uint seq_size = frag_sequence_.size();
  uint chain_size = coord_toc[index].size;
  if(seq_size > chain_size) return;

  String db_seq(chain_size,'X');
  uint coord_index = coord_toc[index].offset;
  for(uint i = 0; i < chain_size; ++i, ++coord_index){
    db_seq[i] = full_db_seq[coord_index];
  }

  int matches = 0;
  Real factor = 1.0 / seq_size;
  for(uint i = 0; i <= chain_size - seq_size; ++i){
    matches = 0;
    for(uint j = 0; j < seq_size; ++j){
      if(db_seq[i+j] == frag_sequence_[j]) ++matches;
    }
    profile.push_back(matches*factor);
  }
}

void Fragger::GenerateSSAgreementProfile(StructureDBPtr db,
                              std::vector<Real>& profile, uint index,
                              const std::vector<int>& predicted_ss, 
                              const std::vector<int>& confidence) const {

  const std::vector<CoordInfo>& coord_toc = db->GetCoordToc();
  const ost::PagedArray<char, 65536>& full_db_dssp = db->GetDSSP();

  profile.clear();

  uint chain_size = coord_toc[index].size;
  uint seq_size = frag_sequence_.size();
  if(seq_size > chain_size) return;

  //let's precalculate as much as we can
  std::vector<int> chain_dssp_states(chain_size);
  
  uint coord_index = coord_toc[index].offset;
  for(uint i = 0; i < chain_size; ++i){
    chain_dssp_states[i] = GetDSSPIdx(full_db_dssp[coord_index]);
    ++coord_index;
  }

  Real score = 0.0;
  Real factor = 1.0 / seq_size;

  for(uint i = 0; i <= chain_size - seq_size; ++i){
    score = 0.0;
    for(uint j = 0; j < seq_size; ++j){
      score += 
      ss_agreements[chain_dssp_states[i+j]][predicted_ss[j]][confidence[j]];
    }
    profile.push_back(score*factor);
  }    
}

void Fragger::GenerateTorsionProbabilityProfile(StructureDBPtr db,
                              std::vector<Real>& profile, uint index,
                              const TorsionSamplerList& t_sampler,
                              const std::vector<int>& t_sampler_indices) const {

  const std::vector<CoordInfo>& coord_toc = db->GetCoordToc();
  const ost::PagedArray<DihedralInfo, 65536>& full_db_dihedrals = db->GetDihedrals();

  profile.clear();

  uint seq_size = t_sampler.size();
  uint chain_size = coord_toc[index].size;
  if(seq_size > chain_size) return;

  std::vector<int> phi_bins(chain_size);
  std::vector<int> psi_bins(chain_size);

  uint coord_idx = coord_toc[index].offset;
  for(uint i = 0; i < chain_size; ++i){
    const DihedralInfo& db_dihedrals = full_db_dihedrals[coord_idx];
    phi_bins[i] = t_sampler[0]->GetAngleBin(db_dihedrals.GetPhi());
    psi_bins[i] = t_sampler[0]->GetAngleBin(db_dihedrals.GetPsi());
    ++coord_idx;
  }

  Real score;
  Real factor = 1.0/seq_size;
  for(uint i = 0; i <= chain_size - seq_size; ++i){
    score = 0.0;
    for(uint j = 0; j < seq_size; ++j){
      score += t_sampler[j]->GetProbability(t_sampler_indices[j],
                                            phi_bins[i+j],
                                            psi_bins[i+j]);
    }
    profile.push_back(score*factor);
  }
}

void Fragger::GenerateProfileProfile(StructureDBPtr db,
                                     std::vector<Real>& profile,
                                     uint index,
                                     const ost::PagedArray<AAFreq,65536>& frequencies,
                                     short** prof) const {

  const std::vector<CoordInfo>& coord_toc = db->GetCoordToc();

  profile.clear();

  uint chain_size = coord_toc[index].size;
  uint profile_size = frag_sequence_.size();
  if(profile_size > chain_size) return;

  std::vector<const short*> freq_ptrs;
  freq_ptrs.reserve(chain_size);
  uint coord_idx = coord_toc[index].offset;
  for(uint i = 0; i < chain_size; ++i){
    freq_ptrs.push_back(frequencies[coord_idx].data());
    ++coord_idx;
  }

  Real summed_score;
  int summed_col_score;
  Real factor = 1.0 / profile_size;
  for(uint i = 0; i <= chain_size - profile_size; ++i){
    summed_score = 0.0;
    for(uint j = 0; j < profile_size; ++j){
      const short* freq_ptr = freq_ptrs[i+j];
      const short* frag_freq_ptr = prof[j];
      summed_col_score = 0; 
      for(uint k = 0; k < 20; ++k){
        summed_col_score += std::abs(freq_ptr[k] - frag_freq_ptr[k]);
      }
      summed_score += static_cast<Real>(Real(0.0001) * summed_col_score);
    }
    profile.push_back(summed_score * factor);
  }  
}

FraggerMapPtr FraggerMap::Load(const String& file_name, StructureDBPtr db) {
  return Load_(file_name, db);
}
FraggerMapPtr FraggerMap::LoadBB(const String& file_name) {
  return Load_(file_name, StructureDBPtr());
}
void FraggerMap::Save(const String& file_name) {
  Save_(file_name, false);
}
void FraggerMap::SaveBB(const String& file_name) {
  Save_(file_name, true);
}

FraggerMapPtr FraggerMap::Load_(const String& file_name, StructureDBPtr db) {
  // open file
  std::ifstream in_stream_(file_name.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << file_name << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  ost::io::BinaryDataSource ds(in_stream_);

  // fill data
  FraggerMapPtr fmp(new FraggerMap);
  FraggerMap& fragger_map = *fmp;
  size_t map_size;
  ds & map_size;
  for (size_t i = 0; i < map_size; ++i) {
    int id;
    String sequence;
    ds & id;
    ds & sequence;
    FraggerPtr fragger(new Fragger(sequence));
    ds & fragger->fragment_infos_;
    ds & fragger->scores_;
    ds & fragger->single_scores_;
    ds & fragger->weights_;
    ds & fragger->score_types_;

    // get from db or file?
    if (db) {
      BackboneList bb_list;
      fragger->fragments_.clear();
      for (size_t j = 0; j < fragger->fragment_infos_.size(); ++j) {
        db->FillBackbone(bb_list, fragger->fragment_infos_[j], sequence);
        fragger->fragments_.push_back(bb_list);
      }
    } else {
      ds & fragger->fragments_;
    }
    fragger_map[id] = fragger;
  }

  return fmp;
}

void FraggerMap::Save_(const String& file_name, bool with_bb) {
  // open file
  std::ofstream out_stream_(file_name.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << file_name << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  ost::io::BinaryDataSink ds(out_stream_);

  // save map
  ds & map_.size();
  for (std::map<int, FraggerPtr>::iterator i = map_.begin();
       i != map_.end(); ++i) {
    ds & i->first;
    ds & i->second->frag_sequence_;
    ds & i->second->fragment_infos_;
    ds & i->second->scores_;
    ds & i->second->single_scores_;
    ds & i->second->weights_;
    ds & i->second->score_types_;
    if (with_bb) ds & i->second->fragments_;
  }
}

}} //ns
