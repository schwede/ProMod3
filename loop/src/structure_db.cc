// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/structure_db.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>
#include <promod3/core/runtime_profiling.hh>
#include <ost/mol/alg/accessibility.hh>
#include <ost/mol/alg/sec_struct.hh>

namespace {

void FloodLevel(char* data, int x_start, int y_start, 
                int x_extent, int y_extent, 
                int orig_value, int dest_value) {

  //http://lodev.org/cgtutor/floodfill.html
  if(orig_value != data[x_start*y_extent + y_start]) {
    return;
  }

  std::vector<std::pair<int,int> > queue;
  queue.push_back(std::make_pair(x_start, y_start));

  int y1,y,x; 
  bool spanLeft, spanRight;
  std::pair<int,int> actual_position;
    
  while(!queue.empty()){    

    actual_position = queue.back();
    queue.pop_back();
    x = actual_position.first;
    y = actual_position.second;

    y1 = y;

    while(y1 >= 0 && data[x*y_extent + y1] == orig_value) {
      --y1;
    }

    y1++;
    spanLeft = spanRight = 0;

    while(y1 < y_extent && 
          data[x*y_extent + y1] == orig_value ) {

      data[x*y_extent + y1] = dest_value;

      if(!spanLeft && x > 0 && 
         data[(x-1)*y_extent + y1] == orig_value) {
        queue.push_back(std::make_pair(x-1,y1));
        spanLeft = 1;
      }
      else if(spanLeft && x > 0 && 
              data[(x-1)*y_extent + y1] != orig_value) {
        spanLeft = 0;
      }

      if(!spanRight && x < x_extent - 1 && 
         data[(x+1)*y_extent + y1] == orig_value) {
        queue.push_back(std::make_pair(x+1,y1));
        spanRight = 1;
      }
      else if(spanRight && x < x_extent - 1 && 
              data[(x+1)*y_extent + y1] != orig_value) {
        spanRight = 0;
      } 
      ++y1;
    }
  }
}


void AssignResidueDepths(const ost::mol::EntityView& ent,
                         const ost::mol::ChainView& chain) {


  // sum of approx. vdw radius of the present heavy atoms (1.8) 
  // plus 1.4 for water.
  Real radius = 3.2;
  Real one_over_radius = Real(1.0) / radius;

  // lets setup a grid in which we place the atoms
  Real min_x = std::numeric_limits<Real>::max();
  Real max_x = -min_x;

  Real min_y = std::numeric_limits<Real>::max();
  Real max_y = -min_y;

  Real min_z = std::numeric_limits<Real>::max();
  Real max_z = -min_z;

  std::vector<geom::Vec3> atom_positions;
  ost::mol::AtomViewList atom_list = ent.GetAtomList();
  
  for(ost::mol::AtomViewList::iterator it = atom_list.begin();
      it != atom_list.end(); ++it) {
    geom::Vec3 pos = it->GetPos();
    min_x = std::min(min_x, pos[0]);
    max_x = std::max(max_x, pos[0]);
    min_y = std::min(min_y, pos[1]);
    max_y = std::max(max_y, pos[1]);
    min_z = std::min(min_z, pos[2]);
    max_z = std::max(max_z, pos[2]);
    atom_positions.push_back(pos);
  }

  // we guarantee that the thing is properly solvated in the x-y plane and add 
  // some space around this is also necessary to avoid overflow checks in 
  // different places
  min_x -= Real(2.1) * radius;
  min_y -= Real(2.1) * radius;
  min_z -= Real(2.1) * radius;
  max_x += Real(2.1) * radius;
  max_y += Real(2.1) * radius;
  max_z += Real(2.1) * radius;

  int num_xbins = std::ceil((max_x - min_x) * one_over_radius);
  int num_ybins = std::ceil((max_y - min_y) * one_over_radius);
  int num_zbins = std::ceil((max_z - min_z) * one_over_radius);
  int num_bins = num_xbins * num_ybins * num_zbins;
  char* grid = new char[num_bins];
  memset(grid, 0, num_bins);

  for(uint i = 0; i < atom_positions.size(); ++i) {

    const geom::Vec3& pos = atom_positions[i];
    int x_bin = (pos[0] - min_x) * one_over_radius;
    int y_bin = (pos[1] - min_y) * one_over_radius;
    int z_bin = (pos[2] - min_z) * one_over_radius;

    // we're really crude here and simply set all 27 cubes with central
    // cube defined by x_bin, y_bin and z_bin to one
    for(int z = z_bin - 1; z <= z_bin + 1; ++z) {
      for(int x = x_bin - 1; x <= x_bin + 1; ++x) {
        for(int y = y_bin - 1; y <= y_bin + 1; ++y) {
          grid[z*num_xbins*num_ybins + x*num_ybins + y] = 1;
        }
      }
    }
  }

  // lets call flood fill for every layer along the z-axis from every 
  // corner in the x-y plane. 
  for(int z = 0; z < num_zbins; ++z) {
    char* level = &grid[z*num_xbins*num_ybins];
    FloodLevel(level, 0,             0,             num_xbins, num_ybins, 0, 2);
    FloodLevel(level, 0,             num_ybins - 1, num_xbins, num_ybins, 0, 2);
    FloodLevel(level, num_xbins - 1, 0,             num_xbins, num_ybins, 0, 2);
    FloodLevel(level, num_xbins - 1, num_ybins - 1, num_xbins, num_ybins, 0, 2);
  }

  // we filled the xy-planes but we also need to consider stuff along the z-axis
  bool something_happened = true;
  while(something_happened) {

    something_happened = false;

    char* level; 
    char* upper_level; 
    char* lower_level;
    int idx;

    // do first layer
    level = &grid[0];
    upper_level = &grid[num_xbins*num_ybins];
    idx = 0;
    for(int x = 0; x < num_xbins; ++x) {
      for(int y = 0; y < num_ybins; ++y, ++idx) {
        if(level[idx] == 2 && upper_level[idx] == 0) {
          FloodLevel(upper_level, x, y, num_xbins, num_ybins, 0, 2);
          something_happened = true;
        }
      }
    }

    // do all layers in between
    for(int z = 1; z < num_zbins - 1; ++z) {
      level = &grid[z*num_xbins*num_ybins];
      upper_level = &grid[(z+1)*num_xbins*num_ybins];
      lower_level = &grid[(z-1)*num_xbins*num_ybins];
      idx = 0;
      for(int x = 0; x < num_xbins; ++x) {
        for(int y = 0; y < num_ybins; ++y, ++idx) {
          if(level[idx] == 2 && upper_level[idx] == 0) {
            FloodLevel(upper_level, x, y, num_xbins, num_ybins, 0, 2);
            something_happened = true;
          }
          if(level[idx] == 2 && lower_level[idx] == 0) {
            FloodLevel(lower_level, x, y, num_xbins, num_ybins, 0, 2);
            something_happened = true;
          }
        }
      }
    }

    // do last layer
    level = &grid[(num_zbins - 1) * num_xbins*num_ybins];
    lower_level = &grid[(num_zbins - 2) * num_xbins*num_ybins];
    idx = 0;
    for(int x = 0; x < num_xbins; ++x) {
      for(int y = 0; y < num_ybins; ++y, ++idx) {
        if(level[idx] == 2 && lower_level[idx] == 0) {
          FloodLevel(lower_level, x, y, num_xbins, num_ybins, 0, 2);
          something_happened = true;
        }
      }
    }
  }
  
  // every cube in every layer that has currently value 1 that has a city-block
  // distance below 3 to any cube with value 2 is considered to be in contact
  // with the outer surface... lets set them to a value of three
  for(int z = 0; z < num_zbins; ++z) {
    for(int x = 0; x < num_xbins; ++x) {
      for(int y = 0; y < num_ybins; ++y) {
        if(grid[z*num_xbins*num_ybins + x*num_ybins + y] == 1) {
          int x_from = std::max(0, x - 3);
          int x_to = std::min(num_xbins-1, x + 3);
          int y_from = std::max(0, y - 3);
          int y_to = std::min(num_ybins-1, y + 3);
          int z_from = std::max(0, z - 3);
          int z_to = std::min(num_zbins-1, z + 3);
          bool exposed = false;
          for(int i = x_from; i <= x_to && !exposed; ++i) {
            for(int j = y_from; j <= y_to; ++j) {
              for(int k = z_from; k < z_to; ++k) {
                if(grid[k*num_xbins*num_ybins + i*num_ybins + j] == 2) {
                  grid[z*num_xbins*num_ybins + x*num_ybins + y] = 3;
                  exposed = true;
                  break;
                }
              }
            }
          }
        }
      }
    }
  }

  // All residues that have at least one atom in a cube with value 3 AND have a 
  // relative solvent accessibility > 0.25 are considered to be exposed.
  // To finally calculate a distance, we use the CB position.
  ost::mol::ResidueViewList all_residues = ent.GetResidueList();
  std::vector<geom::Vec3> exposed_positions;

  for(ost::mol::ResidueViewList::iterator res_it = all_residues.begin();
      res_it != all_residues.end(); ++res_it) {

    if(res_it->HasProp("asaRel") && res_it->GetFloatProp("asaRel") > Real(25)) {

      bool has_exposed_atom = false;
      ost::mol::AtomViewList at_list = res_it->GetAtomList();
      for(ost::mol::AtomViewList::iterator at_it = at_list.begin();
          at_it != at_list.end(); ++at_it) {
        const geom::Vec3& pos = at_it->GetPos();
        int x_bin = (pos[0] - min_x) * one_over_radius;
        int y_bin = (pos[1] - min_y) * one_over_radius;
        int z_bin = (pos[2] - min_z) * one_over_radius;
        if(grid[z_bin*num_xbins*num_ybins + x_bin*num_ybins + y_bin] == 3) {
          has_exposed_atom = true;
          break;
        }
      }

      if(has_exposed_atom) {
        ost::mol::AtomView cb = res_it->FindAtom("CB");
        if(cb.IsValid()) {
          exposed_positions.push_back(cb.GetPos());
        } else {
          ost::mol::AtomView n = res_it->FindAtom("N");
          ost::mol::AtomView ca = res_it->FindAtom("CA");
          ost::mol::AtomView c = res_it->FindAtom("C");
          if(n.IsValid() && ca.IsValid() && c.IsValid()) {
            geom::Vec3 cb_pos;
            promod3::core::ConstructCBetaPos(n.GetPos(), ca.GetPos(),
                                             c.GetPos(), cb_pos);
            exposed_positions.push_back(cb_pos);
          }
        }
      }
    }
  }

  ost::mol::ResidueViewList chain_residues = chain.GetResidueList();
  for(ost::mol::ResidueViewList::iterator res_it = chain_residues.begin();
      res_it != chain_residues.end(); ++res_it) {

    Real min_dist_2 = std::numeric_limits<Real>::max();

    bool valid_pos = false;
    geom::Vec3 pos;

    ost::mol::AtomView cb = res_it->FindAtom("CB"); 
    if(cb.IsValid()) {
      pos = cb.GetPos();  
      valid_pos = true;
    } else {
      ost::mol::AtomView n = res_it->FindAtom("N");
      ost::mol::AtomView ca = res_it->FindAtom("CA");
      ost::mol::AtomView c = res_it->FindAtom("C");
      if(n.IsValid() && ca.IsValid() && c.IsValid()) {
        promod3::core::ConstructCBetaPos(n.GetPos(), ca.GetPos(),
                                         c.GetPos(), pos);
        valid_pos = true;
      }
    }
    if(valid_pos) {
      for(uint i = 0; i < exposed_positions.size(); ++i) {
        min_dist_2 = std::min(min_dist_2, 
                              geom::Length2(pos - exposed_positions[i]));
      }
    } else {
      // dont do anything, we just leave it at the max value...
    }

    res_it->SetFloatProp("residue_depth", std::sqrt(min_dist_2));
  }

  // cleanup
  delete [] grid;
}

}

namespace promod3 { namespace loop {

StructureDBPtr StructureDB::Load(const String& file_name) {
  
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "StructureDB::Load", 2);
  
  std::ifstream in_stream_(file_name.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << file_name << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version != 2) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << file_name;
    throw promod3::Error(ss.str());
  }

  // see Save for required types...
  typedef ost::FixedString<5> MyString;
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckTypeSize<unsigned short>(in_stream);
  core::CheckTypeSize<uint64_t>(in_stream);
  core::CheckTypeSize<short>(in_stream);
  core::CheckTypeSize<MyString>(in_stream);
  core::CheckTypeSize<CoordInfo>(in_stream);
  core::CheckTypeSize<AAFreq>(in_stream);
  core::CheckTypeSize<PeptideCoords>(in_stream);
  core::CheckTypeSize<UShortVec>(in_stream);
  core::CheckTypeSize<DihedralInfo>(in_stream);

  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<char>(in_stream);
  core::CheckBaseType<unsigned short>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<short>(in_stream);
  // raw data
  StructureDBPtr db_p(new StructureDB);

  in_stream & db_p->stored_data_;
  in_stream & db_p->coord_toc_;  
  db_p->seq_.Read(in_stream.Stream());
  db_p->coords_.Read(in_stream.Stream());

  if(db_p->HasData(Dihedrals)) {
    db_p->dihedrals_.Read(in_stream.Stream());
  }

  if(db_p->HasData(SolventAccessibilities)) {
    db_p->solv_acc_.Read(in_stream.Stream());
  }

  if(db_p->HasData(ResidueDepths)) {
    db_p->res_depths_.Read(in_stream.Stream());
  }

  if(db_p->HasData(DSSP)) {
    db_p->dssp_.Read(in_stream.Stream());
  }

  if(db_p->HasData(AAFrequencies)) {
    db_p->aa_frequencies_.Read(in_stream.Stream());
  }

  if(db_p->HasData(AAFrequenciesStruct)) {
    db_p->aa_frequencies_struct_.Read(in_stream.Stream());
  }

  in_stream_.close();
  
  return db_p;
}

void StructureDB::Save(const String& filename) {
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 2);
  // required base types: uint, char, unsigned short, uint64_t, short
  // required structs: ost::FixedString<5>, CoordInfo, AAFreq
  //                   PeptideCoords, UShortVec, DihedralInfo
  typedef ost::FixedString<5> MyString;
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteTypeSize<unsigned short>(out_stream);
  core::WriteTypeSize<uint64_t>(out_stream);
  core::WriteTypeSize<short>(out_stream);
  core::WriteTypeSize<MyString>(out_stream);
  core::WriteTypeSize<CoordInfo>(out_stream);
  core::WriteTypeSize<AAFreq>(out_stream);
  core::WriteTypeSize<PeptideCoords>(out_stream);
  core::WriteTypeSize<UShortVec>(out_stream);
  core::WriteTypeSize<DihedralInfo>(out_stream);

  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<char>(out_stream);
  core::WriteBaseType<unsigned short>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<short>(out_stream);
  // raw data

  out_stream & stored_data_;
  out_stream & coord_toc_;
  seq_.Write(out_stream.Stream());
  coords_.Write(out_stream.Stream());

  if(this->HasData(Dihedrals)) {
    dihedrals_.Write(out_stream.Stream());
  }

  if(this->HasData(SolventAccessibilities)) {
    solv_acc_.Write(out_stream.Stream());
  }

  if(this->HasData(ResidueDepths)) {
    res_depths_.Write(out_stream.Stream());
  }

  if(this->HasData(DSSP)) {
    dssp_.Write(out_stream.Stream());
  }

  if(this->HasData(AAFrequencies)) {
    aa_frequencies_.Write(out_stream.Stream());
  }

  if(this->HasData(AAFrequenciesStruct)) {
    aa_frequencies_struct_.Write(out_stream.Stream());
  }

  out_stream_.close();
}

StructureDBPtr StructureDB::LoadPortable(const String& file_name) {
  // open file
  std::ifstream in_stream_(file_name.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << file_name << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version != 2) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << file_name;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed
  core::CheckTypeSize<short>(in_stream, true);
  core::CheckTypeSize<int>(in_stream, true);

  core::CheckBaseType<uint16_t>(in_stream);
  core::CheckBaseType<int16_t>(in_stream);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // data
  StructureDBPtr db_p(new StructureDB);
  in_stream & db_p->stored_data_;
  in_stream & db_p->coord_toc_;
  // seq_ has fundamental type
  db_p->seq_.Read(in_stream.Stream());
  in_stream & db_p->coords_;
  
  if(db_p->HasData(Dihedrals)) {
    in_stream & db_p->dihedrals_;
  }

  if(db_p->HasData(SolventAccessibilities)) {
    // solv_acc_ has fundamental type
    db_p->solv_acc_.Read(in_stream.Stream());
  }

  if(db_p->HasData(ResidueDepths)) {
    // res_depths_ has fundamental type
    db_p->res_depths_.Read(in_stream.Stream());
  }

  if(db_p->HasData(DSSP)) {
    // dssp_ has fundamental type
    db_p->dssp_.Read(in_stream.Stream());
  }

  if(db_p->HasData(AAFrequencies)) {
    in_stream & db_p->aa_frequencies_;
  }

  if(db_p->HasData(AAFrequenciesStruct)) {
    in_stream & db_p->aa_frequencies_struct_;
  }
  
  in_stream_.close();
  return db_p;
}

void StructureDB::SavePortable(const String& filename) {
  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 2);
  // here: only base-type-checks needed
  core::WriteTypeSize<int16_t>(out_stream);
  core::WriteTypeSize<int32_t>(out_stream);

  core::WriteBaseType<uint16_t>(out_stream);
  core::WriteBaseType<int16_t>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // data
  out_stream & stored_data_;
  out_stream & coord_toc_;
  // seq_ has fundamental type
  seq_.Write(out_stream.Stream());
  out_stream & coords_;  

  if(this->HasData(Dihedrals)) {
    out_stream & dihedrals_;
  }

  if(this->HasData(SolventAccessibilities)) {
    // solv_acc_ has fundamental type
    solv_acc_.Write(out_stream.Stream());
  }

  if(this->HasData(ResidueDepths)) {
    // res_depths_ has fundamental type
    res_depths_.Write(out_stream.Stream());
  }

  if(this->HasData(DSSP)) {
    // dssp_ has fundamental type
    dssp_.Write(out_stream.Stream());
  }

  if(this->HasData(AAFrequencies)) {
    out_stream & aa_frequencies_;
  }

  if(this->HasData(AAFrequenciesStruct)) {
    out_stream & aa_frequencies_struct_;
  }

  out_stream_.close();
}

bool StructureDB::operator==(const StructureDB& rhs) const {
  return coord_toc_ == rhs.coord_toc_
      && coords_ == rhs.coords_
      && aa_frequencies_ == rhs.aa_frequencies_
      && aa_frequencies_struct_ == rhs.aa_frequencies_struct_;
}

std::vector<int> StructureDB::AddCoordinates(const String& id,
                                             const String& chain_name,
                                             ost::mol::EntityView& ent,
                                             ost::seq::SequenceHandle& seqres,
                                             ost::seq::ProfileHandlePtr prof,
                                             bool only_longest_stretch) {

  ost::mol::ChainView chain = ent.FindChain(chain_name);

  if(!chain.IsValid()) {
    throw promod3::Error("Could not find specified chain in input entity!");
  }

  // if we want the amino acid frequencies, we must ensure a valid profile
  // that has matching sequence with seqres
  if(this->HasData(AAFrequencies)) {

    if(!prof) {
      throw promod3::Error("You must provide a valid sequence profile when you"
                           " add coordinates to this StructureDB!"); 
    }

    if(prof->GetSequence() != seqres.GetString()) {
      std::stringstream ss;
      ss << "Profile sequence does not match with seqres: ";
      ss << prof->GetSequence() << " vs " << seqres.GetString();
      throw promod3::Error(ss.str());
    }
  }

  std::vector<int> return_vec;
  ost::mol::ResidueViewList residues = chain.GetResidueList();

  if(residues.empty()) {
    return return_vec;
  }

  // check for consistency of the residue numbers with seqres
  int seqres_size = seqres.GetLength();
  std::vector<int> resnums;
  ost::mol::AtomView c_prev; 
  Real optimal_peptide_bond_length = 1.348435;
  std::vector<bool> connected_to_prev;

  for(ost::mol::ResidueViewList::iterator it = residues.begin();
      it != residues.end(); ++it) {
    int rnum = it->GetNumber().GetNum();
    char olc = it->GetOneLetterCode();
    if(rnum < 1 || rnum > seqres_size || olc != seqres[rnum-1]) {
      std::stringstream ss;
      ss << "Residue " << *it << " doesnt match with seqres: ";
      ss << seqres;
      throw promod3::Error(ss.str());
    }

    // only consider residue if all required backbone atoms are present
    ost::mol::AtomView n = it->FindAtom("N");
    ost::mol::AtomView ca = it->FindAtom("CA");
    ost::mol::AtomView c = it->FindAtom("C");
    ost::mol::AtomView o = it->FindAtom("O");

    if(!(n.IsValid() && ca.IsValid() && c.IsValid() && o.IsValid())) {
      continue;
    }

    resnums.push_back(rnum);

    Real d = 0.0;
    if(c_prev.IsValid()) {
      d = geom::Distance(c_prev.GetPos(), n.GetPos());
    }
    if(std::abs(d - optimal_peptide_bond_length) > 0.5) {
      connected_to_prev.push_back(false);
    } else {
      connected_to_prev.push_back(true);
    }
    c_prev = c;
  }

  // find connected stretches of residue numbers
  std::vector<std::vector<int> > connected_stretches;
  std::vector<int> current_resnums;
  current_resnums.push_back(resnums[0]);
  for(uint i = 1; i < resnums.size(); ++i) {
    if(resnums[i] == current_resnums.back() + 1 && connected_to_prev[i]) {
      current_resnums.push_back(resnums[i]);
    } else {
      connected_stretches.push_back(current_resnums);
      current_resnums = std::vector<int>();
      current_resnums.push_back(resnums[i]);
    }
  }
  if(!current_resnums.empty()) {
    connected_stretches.push_back(current_resnums);
  }

  // depending on the only_longest_stretch flag, we only add one stretch 
  // or all of them
  std::vector<int> stretches_to_add;
  if(only_longest_stretch) {
    uint max_size = 0;
    int max_idx = 0;
    for(uint i = 0; i < connected_stretches.size(); ++i) {
      if(connected_stretches[i].size() > max_size) {
        max_size = connected_stretches[i].size();
        max_idx = i;
      }
    }
    stretches_to_add.push_back(max_idx);
  } else {
    for(uint i = 0; i < connected_stretches.size(); ++i) {
      stretches_to_add.push_back(i);
    }
  }

  // If required, we assign secondary structures, solvent accessibilities and 
  // residue depths given the full complex. Since they relate
  // to the same underlying data, the stuff will also be accessible from the 
  // residues in the ChainView.
  if(this->HasData(DSSP)) {
    ost::mol::alg::AssignSecStruct(ent);
  }

  if(this->HasData(SolventAccessibilities) || 
     this->HasData(ResidueDepths)) {
    ost::mol::alg::Accessibility(ent);
  }

  if(this->HasData(ResidueDepths)) {
    AssignResidueDepths(ent, chain);
  }

  /////////////////
  // Lets do it! //
  /////////////////

  for(std::vector<int>::iterator stretch_idx_it = stretches_to_add.begin();
      stretch_idx_it != stretches_to_add.end(); ++stretch_idx_it) {

    const std::vector<int>& stretch = connected_stretches[*stretch_idx_it];

    // chain must not exceed length...
    if(stretch.size() >= std::numeric_limits<unsigned short>::max()) {
      std::stringstream ss;
      ss << "Due to technical reasons, the maximum length of added chains is ";
      ss << std::numeric_limits<unsigned short>::max() << ". skip...";
      LOG_ERROR(ss.str());
      continue;
    }

    // lets build a BackboneList of the stretch to simplify feature extraction
    ost::mol::ResidueHandleList stretch_residues;
    String stretch_sequence(stretch.size(), 'X');

    for(uint rnum_idx = 0; rnum_idx < stretch.size(); ++rnum_idx) {
      ost::mol::ResNum rnum(stretch[rnum_idx]);
      ost::mol::ResidueView res = chain.FindResidue(rnum);
      stretch_residues.push_back(res.GetHandle());
      stretch_sequence[rnum_idx] = res.GetOneLetterCode();
    }

    promod3::loop::BackboneList bb_list(stretch_sequence, stretch_residues);

    // get the bounds and check whether they're small enough for our custom
    // position data type
    geom::AlignedCuboid bounds = bb_list.GetBounds();
    geom::Vec3 cuboid_size = bounds.GetSize();
    Real max_extent = std::max(cuboid_size[0],
                               std::max(cuboid_size[1],
                                        cuboid_size[2]));
    Real max_v = std::floor(std::numeric_limits<unsigned short>::max() * 0.01); 
    if(max_extent >= max_v) {
      std::stringstream ss;
      ss << "Max extent of stretch to be added must not exceed ";
      ss << std::floor(std::numeric_limits<unsigned short>::max() * 0.01);
      ss <<" Angstrom! skip...";
      LOG_ERROR(ss.str());
      continue;
    }

    // we're ready to go, from now on we should have no errors anymore
    uint offset = coords_.size();
    geom::Vec3 shift = -bounds.GetMin();
    coord_toc_.push_back(CoordInfo(id, chain_name, offset, 
                         bb_list.size(), stretch[0], shift));

    for(uint rnum_idx = 0; rnum_idx < bb_list.size(); ++rnum_idx) {
      PeptideCoords coords;
      coords.n = UShortVec(bb_list.GetN(rnum_idx) + shift);
      coords.ca = UShortVec(bb_list.GetCA(rnum_idx) + shift);
      coords.c = UShortVec(bb_list.GetC(rnum_idx) + shift);
      coords.o = UShortVec(bb_list.GetO(rnum_idx) + shift);
      seq_.push_back(bb_list.GetOLC(rnum_idx));
      coords_.push_back(coords);
    }

    if(this->HasData(Dihedrals)) {

      dihedrals_.push_back(DihedralInfo(-1.0472,
                                        bb_list.GetPsiTorsion(0)));
      for(uint i = 1; i < bb_list.size() - 1; ++i) {
        dihedrals_.push_back(DihedralInfo(bb_list.GetPhiTorsion(i),
                                          bb_list.GetPsiTorsion(i)));
      }
      dihedrals_.push_back(DihedralInfo(bb_list.GetPhiTorsion(bb_list.size()-1),
                                        -0.78540));
    }

    if(this->HasData(SolventAccessibilities)) {
      for(ost::mol::ResidueHandleList::iterator it = stretch_residues.begin();
        it != stretch_residues.end(); ++it) {
        if(it->HasProp("asaAbs")) {
          Real acc = static_cast<unsigned short>(it->GetFloatProp("asaAbs"));
          solv_acc_.push_back(acc);
        } else {
          std::stringstream ss;
          ss << "Residue " << *it << " has no valid solvent accessibility ";
          ss << "assigned. Use 0.0..."<<std::endl;
          LOG_ERROR(ss.str());

          solv_acc_.push_back(0);
        }
      }
    }
    
    if(this->HasData(ResidueDepths)) {
      for(ost::mol::ResidueHandleList::iterator it = stretch_residues.begin();
          it != stretch_residues.end(); ++it) {
        Real residue_depth = it->GetFloatProp("residue_depth");
        Real transformed_r_depth = residue_depth * 10;
        unsigned short final_depth;
        if(transformed_r_depth >= std::numeric_limits<unsigned short>::max()) {
          final_depth = std::numeric_limits<unsigned short>::max();
        } else {
          final_depth = static_cast<unsigned short>(transformed_r_depth);
        }
        res_depths_.push_back(final_depth);
      }       
    }

    if(this->HasData(DSSP)) {
      for(ost::mol::ResidueHandleList::const_iterator it = 
          stretch_residues.begin(); it != stretch_residues.end(); ++it) {
        char dssp_state = char(it->GetSecStructure());
        dssp_.push_back(dssp_state);
      }
    }

    if(this->HasData(AAFrequencies)) {
      for(uint rnum_idx = 0; rnum_idx < bb_list.size(); ++rnum_idx) {
        AAFreq aa_freq;
        const ost::seq::ProfileColumn& col = (*prof)[stretch[rnum_idx] - 1];
        aa_freq.AssignProfileColumn(col);
        aa_frequencies_.push_back(aa_freq);
      }
    }

    if(this->HasData(AAFrequenciesStruct)) {
      for(uint rnum_idx = 0; rnum_idx < bb_list.size(); ++rnum_idx) {
        AAFreq aa_freq_struct;
        aa_frequencies_struct_.push_back(aa_freq_struct);
      }
    }

    return_vec.push_back(coord_toc_.size() - 1);
  }

  return return_vec;
}


std::vector<int> StructureDB::AddCoordinates(const String& id,
                                             const String& chain_name,
                                             ost::mol::EntityHandle& ent,
                                             ost::seq::SequenceHandle& seqres,
                                             ost::seq::ProfileHandlePtr prof,
                                             bool only_longest_stretch) {

  ost::mol::EntityView view = ent.CreateFullView();
  return this->AddCoordinates(id, chain_name, view, seqres,
                              prof, only_longest_stretch);
}


void StructureDB::RemoveCoordinates(uint coord_idx) {

  if(coord_idx >= coord_toc_.size()) {
    throw promod3::Error("Invalid input idx!");
  }

  // lets first remove everything from the paged arrays
  uint64_t from = coord_toc_[coord_idx].offset;
  uint64_t to = from + coord_toc_[coord_idx].size;

  seq_.ClearRange(from, to);
  coords_.ClearRange(from, to);

  if(this->HasData(Dihedrals)) {
    dihedrals_.ClearRange(from, to);
  }

  if(this->HasData(SolventAccessibilities)) {
    solv_acc_.ClearRange(from, to); 
  }

  if(this->HasData(ResidueDepths)) {
    res_depths_.ClearRange(from, to);
  }

  if(this->HasData(DSSP)) {
    dssp_.ClearRange(from, to);
  }

  if(this->HasData(AAFrequencies)) {
    aa_frequencies_.ClearRange(from, to);
  }

  if(this->HasData(AAFrequenciesStruct)) {
    aa_frequencies_struct_.ClearRange(from, to);
  }

  // correct all offset values of the coords that are stored afterwards
  for(uint i = coord_idx + 1; i < coord_toc_.size(); ++i) {
    coord_toc_[i].offset -= coord_toc_[coord_idx].size;
  }

  // and finally kill the CoordInfo itself
  coord_toc_.erase(coord_toc_.begin() + coord_idx);
}


std::vector<int> StructureDB::GetCoordIdx(const String& id, 
                                          const String& chain_name) const{

  std::vector<int> return_vec;

  for(uint i = 0; i < coord_toc_.size(); ++i){
    if(coord_toc_[i].id == id) {
      if(coord_toc_[i].chain_name == chain_name) {
        return_vec.push_back(i);
      }
    }
  }
  return return_vec;
}

const CoordInfo& StructureDB::GetCoordInfo(int index) const{
  if(static_cast<uint>(index) >= coord_toc_.size()){
    throw promod3::Error("pdb index exceeds number of coord tocs in database!");
  }
  return coord_toc_[index];
}

Real StructureDB::CARMSD(const FragmentInfo& info_one,
                         const FragmentInfo& info_two,
                         bool superpose_stems) const{

  this->CheckFragmentInfo(info_one);
  this->CheckFragmentInfo(info_two);

  if(info_one.length != info_two.length){
    throw promod3::Error("Fragments must have equal size to calculate CARMSD");
  }

  if(static_cast<uint>(info_one.length) < 3){
    throw promod3::Error("Coordinates need at least length of three for "
                         "meaningful CARMSD!");
  }

  uint start_one = GetStartIndex(info_one);
  uint start_two = GetStartIndex(info_two);

  if(superpose_stems){

    core::EMatX3 pos_one = core::EMatX3::Zero(6,3);
    core::EMatX3 pos_two = core::EMatX3::Zero(6,3);

    PeptideCoords start_coords = coords_[start_one];
    PeptideCoords end_coords = coords_[start_one + 
                                       static_cast<uint>(info_one.length) - 1];

    core::EMatFillRow(pos_one, 0, start_coords.n.ToVec3());
    core::EMatFillRow(pos_one, 1, start_coords.ca.ToVec3());
    core::EMatFillRow(pos_one, 2, start_coords.c.ToVec3());
    core::EMatFillRow(pos_one, 3, end_coords.n.ToVec3());
    core::EMatFillRow(pos_one, 4, end_coords.ca.ToVec3());
    core::EMatFillRow(pos_one, 5, end_coords.c.ToVec3());

    start_coords = coords_[start_two];
    end_coords = coords_[start_two + static_cast<uint>(info_two.length) - 1];

    core::EMatFillRow(pos_two, 0, start_coords.n.ToVec3());
    core::EMatFillRow(pos_two, 1, start_coords.ca.ToVec3());
    core::EMatFillRow(pos_two, 2, start_coords.c.ToVec3());
    core::EMatFillRow(pos_two, 3, end_coords.n.ToVec3());
    core::EMatFillRow(pos_two, 4, end_coords.ca.ToVec3());
    core::EMatFillRow(pos_two, 5, end_coords.c.ToVec3());

    geom::Mat4 t = core::MinRMSDSuperposition(pos_one, pos_two);

    Real msd = 0.0;
    Real temp;
    geom::Vec3 ca_pos_one;
    geom::Vec3 ca_pos_two;

    for(int i = 0; i < info_one.length; ++i){
      ca_pos_one = coords_[start_one+i].ca.ToVec3();
      ca_pos_two = coords_[start_two+i].ca.ToVec3();

      temp = t(0,0) * ca_pos_one[0] + 
             t(0,1) * ca_pos_one[1] + 
             t(0,2) * ca_pos_one[2] + 
             t(0,3);
      temp -= ca_pos_two[0];
      msd += temp * temp;

      temp = t(1,0) * ca_pos_one[0] + 
             t(1,1) * ca_pos_one[1] + 
             t(1,2) * ca_pos_one[2] + 
             t(1,3);
      temp -= ca_pos_two[1];
      msd += temp * temp;

      temp = t(2,0) * ca_pos_one[0] + 
             t(2,1) * ca_pos_one[1] +
             t(2,2) * ca_pos_one[2] + 
             t(2,3);
      temp -= ca_pos_two[2];
      msd += temp * temp;
    }
    return std::sqrt(msd / info_one.length);
  }

  core::EMatX3 pos_one = core::EMatX3::Zero(info_one.length,3);
  core::EMatX3 pos_two = core::EMatX3::Zero(info_one.length,3);  
  
  for(int i = 0; i < info_one.length; ++i){
    core::EMatFillRow(pos_one, i, coords_[start_one + i].ca.ToVec3());
  }

  for(int i = 0; i < info_two.length; ++i){
    core::EMatFillRow(pos_two, i, coords_[start_two + i].ca.ToVec3());
  }

  return core::SuperposedRMSD(pos_one,pos_two);
}

void StructureDB::FillBackbone(BackboneList& bb_list, const FragmentInfo& info,
                               const String& sequence) const {

  this->CheckFragmentInfo(info);
  if (!sequence.empty() && info.length != sequence.size()) {
    throw promod3::Error("Inconsistent sequence size in FillBackbone!");
  }

  String bb_list_seq = sequence;
  if(bb_list_seq.empty()) {
    bb_list_seq = this->GetSequence(info);
  }

  bb_list.clear();

  uint coord_start = GetStartIndex(info);
  for (uint i = 0; i < info.length; ++i) {
    // NOTE: CB pos. and AA (must be non-XXX) are set in push_back
    bb_list.push_back(coords_[coord_start+i].n.ToVec3(),
                      coords_[coord_start+i].ca.ToVec3(),
                      coords_[coord_start+i].c.ToVec3(),
                      coords_[coord_start+i].o.ToVec3(),
                      bb_list_seq[i]);
  }
}

void StructureDB::FillBackbone(BackboneList& bb_list, const geom::Mat4& tf,
                               const FragmentInfo& info,
                               const String& sequence) const {
  // fragment info will be checked in here
  this->FillBackbone(bb_list, info, sequence);
  bb_list.ApplyTransform(tf);
}

void StructureDB::FillBackbone(BackboneList& bb_list,
                               const ost::mol::ResidueHandle& stem_one,
                               const ost::mol::ResidueHandle& stem_two,
                               const FragmentInfo& info,
                               const String& sequence) const {

  this->CheckFragmentInfo(info);

  core::EMatX3 pos_one = core::EMatX3::Zero(6,3);
  core::EMatX3 pos_two = core::EMatX3::Zero(6,3);

  ost::mol::AtomHandle n = stem_one.FindAtom("N");
  ost::mol::AtomHandle ca = stem_one.FindAtom("CA");
  ost::mol::AtomHandle c = stem_one.FindAtom("C");

  if (!(n.IsValid() && ca.IsValid() && c.IsValid())) {
    throw promod3::Error("Provided stem residue does not contain all required "
                         "backbone atoms!");
  }

  core::EMatFillRow(pos_one, 0, n.GetPos());
  core::EMatFillRow(pos_one, 1, ca.GetPos());
  core::EMatFillRow(pos_one, 2, c.GetPos());

  n = stem_two.FindAtom("N");  
  ca = stem_two.FindAtom("CA");  
  c = stem_two.FindAtom("C");  

  if (!(n.IsValid() && ca.IsValid() && c.IsValid())) {
    throw promod3::Error("Provided stem residue does not contain all required "
                         "backbone atoms!");
  }

  core::EMatFillRow(pos_one, 3, n.GetPos());
  core::EMatFillRow(pos_one, 4, ca.GetPos());
  core::EMatFillRow(pos_one, 5, c.GetPos());

  int start = GetStartIndex(info);
  const PeptideCoords& start_coords = coords_[start];
  const PeptideCoords& end_coords = coords_[start + info.length - 1];

  core::EMatFillRow(pos_two, 0, start_coords.n.ToVec3());
  core::EMatFillRow(pos_two, 1, start_coords.ca.ToVec3());
  core::EMatFillRow(pos_two, 2, start_coords.c.ToVec3());
  core::EMatFillRow(pos_two, 3, end_coords.n.ToVec3());
  core::EMatFillRow(pos_two, 4, end_coords.ca.ToVec3());
  core::EMatFillRow(pos_two, 5, end_coords.c.ToVec3());

  geom::Mat4 t = core::MinRMSDSuperposition(pos_two,pos_one);

  this->FillBackbone(bb_list, t, info, sequence);
}

void StructureDB::FillBackbone(BackboneList& bb_list, uint coord_idx,
                               const String& sequence) const {

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  this->FillBackbone(bb_list, f_info, sequence);
}

void StructureDB::FillBackbone(BackboneList& bb_list, const geom::Mat4& tf,
                               uint coord_idx,
                               const String& sequence) const {

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  this->FillBackbone(bb_list, tf, f_info, sequence);
}

void StructureDB::FillBackbone(BackboneList& bb_list,
                               const ost::mol::ResidueHandle& stem_one,
                               const ost::mol::ResidueHandle& stem_two,
                               uint coord_idx,
                               const String& sequence) const {

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  this->FillBackbone(bb_list, stem_one, stem_two, f_info, sequence);
}

int StructureDB::GetStartResnum(const FragmentInfo& info) const {
  
  this->CheckFragmentInfo(info);

  return coord_toc_[info.chain_index].start_resnum + info.offset;
}

StructureDBPtr StructureDB::GetSubDB(const std::vector<uint>& indices) const{

  StructureDBPtr sub_db(new StructureDB(stored_data_));

  for(std::vector<uint>::const_iterator i = indices.begin();
      i != indices.end(); ++i){

    if(*i >= this->GetNumCoords()){
      throw promod3::Error("Invalid idx observed when creating SubDB!");
    }

    sub_db->coord_toc_.push_back(coord_toc_[*i]);
    sub_db->coord_toc_.back().offset = sub_db->coords_.size();
    
    uint start = coord_toc_[*i].offset;
    uint end = start + coord_toc_[*i].size;

    for(uint j = start; j < end; ++j) {
      sub_db->seq_.push_back(seq_[j]);
      sub_db->coords_.push_back(coords_[j]);
    }

    if(this->HasData(Dihedrals)) {
      for(uint j = start; j < end; ++j) {
        sub_db->dihedrals_.push_back(dihedrals_[j]);
      }
    }

    if(this->HasData(SolventAccessibilities)) {
      for(uint j = start; j < end; ++j) {
        sub_db->solv_acc_.push_back(solv_acc_[j]);
      }
    }

    if(this->HasData(ResidueDepths)) {
      for(uint j = start; j < end; ++j) {
        sub_db->res_depths_.push_back(res_depths_[j]);
      }
    }

    if(this->HasData(DSSP)) {
      for(uint j = start; j < end; ++j) {
        sub_db->dssp_.push_back(dssp_[j]);
      }
    }

    if(this->HasData(AAFrequencies)) {
      for(uint j = start; j < end; ++j) {
        sub_db->aa_frequencies_.push_back(aa_frequencies_[j]);
      }
    }

    if(this->HasData(AAFrequenciesStruct)) {
      for(uint j = start; j < end; ++j) {
        sub_db->aa_frequencies_struct_.push_back(aa_frequencies_struct_[j]);
      }
    }
  }

  return sub_db;
}

void StructureDB::PrintStatistics() const{

  std::cout << "db summary:" << std::endl << std::endl;
  std::cout << "number of chains : " << coord_toc_.size() << std::endl;
  std::cout << "number of backbone coords : "  << coords_.size() << std::endl;
  std::cout << std::endl;

  if(stored_data_ == 0) {
    return;
  }

  std::cout << "db contains following optional data: " << std::endl << std::endl;

  if(this->HasData(Dihedrals)) {
    std::cout << "- backbone dihedral angles" << std::endl;
  }

  if(this->HasData(SolventAccessibilities)) {
    std::cout << "- solvent accessibilities" << std::endl;
  }

  if(this->HasData(ResidueDepths)) {
    std::cout << "- residue depths" << std::endl;
  }

  if(this->HasData(DSSP)) {
    std::cout << "- dssp secondary structure assignments" << std::endl;
  }

  if(this->HasData(AAFrequencies)) {
    std::cout << "- sequence profiles" << std::endl;
  }

  if(this->HasData(AAFrequenciesStruct)) {
    std::cout << "- structure profiles" << std::endl;
  }
}

ost::seq::ProfileHandlePtr StructureDB::GenerateStructureProfile(
                              const BackboneList& bb_list,
                              const std::vector<Real>& residue_depths) const {

  if(!this->HasData(ResidueDepths)) {
    throw promod3::Error("StructureDB does not contain residue depths!");
  }

  if(bb_list.size() != residue_depths.size()) {
    throw promod3::Error("BackboneList and residue depths must be of same "
                         "size!");
  }

  uint bb_list_size = bb_list.size();
  uint frag_length = std::min(9, static_cast<int>(bb_list.size()));
  FragmentInfo inf(0, 0, frag_length);

  std::vector<std::vector<char> > close_sequences;
  close_sequences.assign(bb_list_size, std::vector<char>());
  std::list<std::pair<Real,FragmentInfo> > similar_frags;

  // let's create a buffer for the exponential depth terms in the db
  // as well as the extracted positions
  uint longest_chain = 0;
  for(uint i = 0; i < coord_toc_.size(); ++i){
    longest_chain = std::max(longest_chain,static_cast<uint>(coord_toc_[i].size));
  }
  std::vector<Real> db_depths_full_chain(longest_chain);
  std::vector<geom::Vec3> ca_pos_full_chain(longest_chain);

  // the exponential depth values can be precalculated
  std::vector<Real> depths(bb_list_size);
  for(uint i = 0; i < bb_list_size; ++i){
    depths[i] = std::exp(-residue_depths[i] * 0.35714);
  }

  // positions for rmsd calculations will be stored in here
  core::EMatX3 pos = core::EMatX3::Zero(frag_length, 3);
  core::EMatX3 db_pos = core::EMatX3::Zero(frag_length, 3);
  
  for(uint i = 0; i <= bb_list_size - frag_length; ++i){

    similar_frags.clear();
    Real worst_score = std::numeric_limits<Real>::max();

    // set the positions for query fragments
    for (uint j = 0; j < frag_length; ++j) {
      core::EMatFillRow(pos, j, bb_list.GetCA(i+j));
    }

    inf.chain_index = 0;

    // let's search through all chains of the db
    for(uint j = 0; j < coord_toc_.size(); ++j, ++inf.chain_index){

      if(coord_toc_[j].size < frag_length) continue;

      inf.offset = 0;
      // the depth exp value of the full chain can be precalculated
      // at the same time we're extracting the positions here
      uint db_chain_offset = coord_toc_[j].offset;
      for(uint k = 0; k < coord_toc_[j].size; ++k){
        db_depths_full_chain[k] = std::exp(-res_depths_[db_chain_offset + k] * 0.035714);
        ca_pos_full_chain[k] = coords_[db_chain_offset + k].ca.ToVec3();
      }

      // let's finally iterate over all possible fragments in this chain
      for(uint k = 0; k <= coord_toc_[j].size - frag_length; ++k, ++inf.offset){

        for(uint l = 0; l < frag_length; ++l){
          core::EMatFillRow(db_pos, l, ca_pos_full_chain[k+l]);
        }

        Real rmsd = core::SuperposedRMSD(pos, db_pos);
        Real summed_squared_depth_diff = 0.0;
        for(uint l = 0; l < frag_length; ++l){
          Real temp = depths[i+l] - db_depths_full_chain[k+l];
          summed_squared_depth_diff += temp*temp;
        }

        Real score = rmsd * rmsd + 10 * summed_squared_depth_diff;

        if(score > worst_score && similar_frags.size() >= 25) continue;

        // insert the fragment at the right place to ensure a sorted list
        std::list<std::pair<Real,FragmentInfo> >::iterator frag_it = similar_frags.begin();
        for( ; frag_it != similar_frags.end(); ++frag_it){
          // find first fragment in fragger with larger score
          if(frag_it->first > score) break;
        }
        similar_frags.insert(frag_it,std::make_pair(score,inf));

        // reduce size if necessary
        while(similar_frags.size() > 25){
          similar_frags.pop_back();
        }
        worst_score = similar_frags.back().first; 
      }
    }

    for(std::list<std::pair<Real,FragmentInfo> >::iterator frag_it = similar_frags.begin();
        frag_it != similar_frags.end(); ++frag_it){
      String seq = this->GetSequence(frag_it->second);
      for(uint j = 0; j < frag_length; ++j){
        close_sequences[i+j].push_back(seq[j]);
      }
    }
  }

  // set profile
  ost::seq::ProfileHandlePtr return_prof(new ost::seq::ProfileHandle);
  return_prof->SetNullModel(ost::seq::ProfileColumn::HHblitsNullModel());
  String reference_sequence = bb_list.GetSequence();
  Real frequencies[20];
  for(uint i = 0; i < bb_list_size; ++i) {
    // sum up a value of one for every position
    memset(frequencies,0,sizeof(Real)*20);
    for(uint j = 0; j < close_sequences[i].size(); ++j) {
      int idx = ost::seq::ProfileColumn::GetIndex(close_sequences[i][j]);
      if(idx != -1) {
        frequencies[idx] += 1.0;
      }
    }
    // normalize it
    Real sum = 0.0;
    for(uint j = 0; j < 20; ++j) {
      sum += frequencies[j];
    }

    if(sum != 0) {
      Real factor = 1.0/sum;
      for(uint j = 0; j < 20; ++j) {
        frequencies[j] *= factor;
      }
    }
    // and add it to the profile
    ost::seq::ProfileColumn actual_col;
    Real* prof_col_data = actual_col.freqs_begin();
    for(uint j = 0; j < 20; ++j) {
      prof_col_data[j] = frequencies[j];
    }
    return_prof->AddColumn(actual_col, reference_sequence[i]);
  }

  return return_prof;
}

void StructureDB::SetStructureProfile(uint chain_idx,
                                      ost::seq::ProfileHandlePtr prof) {

  if(!this->HasData(AAFrequenciesStruct)) {
    throw promod3::Error("StructureDB does not contain amino acid " 
                         "frequencies derived from structural information!");
  }

  if(chain_idx >= coord_toc_.size()){
    throw promod3::Error("Invalid chain idx provided when setting structure profile!");
  }

  if(prof->size() != coord_toc_[chain_idx].size){
    std::stringstream ss;
    ss << "Expect provided profile to have exactly the length of the chain with ";
    ss << "given idx!";
    throw promod3::Error(ss.str());
  }

  uint offset = coord_toc_[chain_idx].offset;
  for(uint i = 0; i < prof->size(); ++i){
    short* db_data = aa_frequencies_struct_[offset+i].data();
    Real* prof_data = (*prof)[i].freqs_begin();
    for(uint j = 0; j < 20; ++j) db_data[j] = static_cast<short>(prof_data[j]*10000);
  }
}

String StructureDB::GetSequence(const FragmentInfo& info) const{

  this->CheckFragmentInfo(info);

  std::string return_sequence(static_cast<uint>(info.length),'?');
  int start = GetStartIndex(info);
  int end = start+info.length;
  int pos = 0;
  for (; start < end; ++start, ++pos) {
    return_sequence[pos] = seq_[start];
  }
  return return_sequence;
}

String StructureDB::GetDSSPStates(const FragmentInfo& info) const{

  if(!this->HasData(DSSP)) {
    throw promod3::Error("StructureDB does not contain dssp assignments!");
  }

  this->CheckFragmentInfo(info);

  String return_states(static_cast<uint>(info.length),'C');
  int start = GetStartIndex(info);
  int end = start+info.length;
  int pos = 0;
  for (; start < end; ++start, ++pos) {
    return_states[pos] = dssp_[start];
  }
  return return_states;
}

std::vector<std::pair<Real,Real> > 
StructureDB::GetDihedralAngles(const FragmentInfo& info) const{

  if(!this->HasData(Dihedrals)) {
    throw promod3::Error("StructureDB does not contain dihedrals!");
  }

  this->CheckFragmentInfo(info);

  std::vector<std::pair<Real,Real> > return_vec(info.length);
  int start = GetStartIndex(info);
  int end = start+info.length;
  int pos = 0;
  for (; start < end; ++start, ++pos) {
    const DihedralInfo& dihedrals = dihedrals_[start];
    return_vec[pos] = std::make_pair(dihedrals.GetPhi(),
                                     dihedrals.GetPsi());
  }
  return return_vec;
}

std::vector<Real> StructureDB::GetResidueDepths(const FragmentInfo& info) const{

  if(!this->HasData(ResidueDepths)) {
    throw promod3::Error("StructureDB does not contain residue depths!");
  }

  this->CheckFragmentInfo(info);

  std::vector<Real> return_depths;
  return_depths.reserve(info.length);
  int start = GetStartIndex(info);
  int end = start+info.length;
  for (; start < end; ++start) {
    return_depths.push_back(0.1 * res_depths_[start]);
  }
  return return_depths;
}

std::vector<int> StructureDB::GetSolventAccessibilities(const FragmentInfo& info) const{

  if(!this->HasData(SolventAccessibilities)) {
    throw promod3::Error("StructureDB does not contain "
                         "solvent accessibilities!");
  }

  this->CheckFragmentInfo(info);

  std::vector<int> return_accessibilities;
  return_accessibilities.reserve(info.length);
  int start = GetStartIndex(info);
  int end = start+info.length;
  for (; start < end; ++start) {
    return_accessibilities.push_back(solv_acc_[start]);
  }
  return return_accessibilities;
}

ost::seq::ProfileHandlePtr
StructureDB::GetSequenceProfile(const FragmentInfo& info) const {

  if(!this->HasData(AAFrequencies)) {
    throw promod3::Error("StructureDB does not contain amino acid "
                         "frequencies!");
  }

  this->CheckFragmentInfo(info);

  ost::seq::ProfileHandlePtr prof(new ost::seq::ProfileHandle);
  prof->SetNullModel(ost::seq::ProfileColumn::HHblitsNullModel());
  int start = GetStartIndex(info);
  int end = start+info.length;
  for (; start < end; ++start) {
    const short* freqs = aa_frequencies_[start].data();
    ost::seq::ProfileColumn col;
    Real* col_data = col.freqs_begin();
    for (uint i = 0; i < 20; ++i) {
      col_data[i] = static_cast<Real>(0.0001 * freqs[i]);
    }
    prof->AddColumn(col, seq_[start]);
  }
  return prof;
}

ost::seq::ProfileHandlePtr
StructureDB::GetStructureProfile(const FragmentInfo& info) const {

  if(!this->HasData(AAFrequenciesStruct)) {
    throw promod3::Error("StructureDB does not contain amino acid " 
                         "frequencies derived from structural information!");
  }

  this->CheckFragmentInfo(info);

  ost::seq::ProfileHandlePtr prof(new ost::seq::ProfileHandle);
  prof->SetNullModel(ost::seq::ProfileColumn::HHblitsNullModel());
  int start = GetStartIndex(info);
  int end = start+info.length;
  for (; start < end; ++start) {
    const short* freqs = aa_frequencies_struct_[start].data();
    ost::seq::ProfileColumn col;
    Real* col_data = col.freqs_begin();
    for (uint i = 0; i < 20; ++i) {
      col_data[i] = static_cast<Real>(0.0001 * freqs[i]);
    }
    prof->AddColumn(col, seq_[start]);
  }
  return prof;
}

String StructureDB::GetSequence(uint coord_idx) const{

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetSequence(f_info);
}

String StructureDB::GetDSSPStates(uint coord_idx) const{

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetDSSPStates(f_info);
}

std::vector<std::pair<Real,Real> > 
StructureDB::GetDihedralAngles(uint coord_idx) const{

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetDihedralAngles(f_info);
}

std::vector<Real> StructureDB::GetResidueDepths(uint coord_idx) const{

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetResidueDepths(f_info);
}

std::vector<int> StructureDB::GetSolventAccessibilities(uint coord_idx) const{

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetSolventAccessibilities(f_info);
}

ost::seq::ProfileHandlePtr
StructureDB::GetSequenceProfile(uint coord_idx) const {

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetSequenceProfile(f_info);
}

ost::seq::ProfileHandlePtr
StructureDB::GetStructureProfile(uint coord_idx) const {

  if(coord_idx >= this->GetNumCoords()) {
    throw promod3::Error("Provided invalid coord_idx!");
  }

  CoordInfo c_info = this->GetCoordInfo(coord_idx);
  FragmentInfo f_info(coord_idx, 0, c_info.size);
  return this->GetStructureProfile(f_info);
}

void StructureDB::CheckFragmentInfo(const FragmentInfo& info) const {
  if (info.chain_index >= coord_toc_.size()) {
    throw promod3::Error("Provided Fragment info tries to access non existent "
                         "structural data!");
  }
  if (info.offset + info.length > coord_toc_[info.chain_index].size) {
    throw promod3::Error("Provided Fragment info tries to access non existent "
                         "structural data!");
  }
}
  
}}
