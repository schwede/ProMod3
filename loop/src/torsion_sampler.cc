// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/torsion_sampler.hh>
#include <promod3/core/check_io.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace loop {

TorsionSampler::TorsionSampler(const std::vector<String>& group_definitions, 
                               int bins_per_dimension, uint seed)
                               : gen_(seed)
                               , conditional_probabilities_(false)
{
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionSampler::TorsionSampler", 2);

  if(bins_per_dimension < 1){
    throw promod3::Error("Bins per dimension must be at least one!");
  }

  memset(index_mapper_, 0, 8000*sizeof(uint));

  //Let's check, whether all possible combinations of the 20 standard amino
  //acids are covered
  std::vector<String> single_ids(3);
  for(int i = 0; i < ost::conop::XXX; ++i){
    for(int j = 0; j < ost::conop::XXX; ++j){
      for(int k = 0; k < ost::conop::XXX; ++k){
        single_ids[0] = ost::conop::AminoAcidToResidueName(ost::conop::AminoAcid(i));
        single_ids[1] = ost::conop::AminoAcidToResidueName(ost::conop::AminoAcid(j));
        single_ids[2] = ost::conop::AminoAcidToResidueName(ost::conop::AminoAcid(k));
        bool match;
        bool found_id(false);

        for(uint l = 0; l < group_definitions.size(); ++l){

          match=true;
          ost::StringRef gid(group_definitions[l].c_str(), group_definitions[l].length());
          std::vector<ost::StringRef> split_definition=gid.split('-'); //split ids
          if(split_definition.size() != 3){
            throw promod3::Error("Invalid torsion group definition encountered!");
          }
          //iterate over all three positions
          for(int m=0;m<3;++m){
            //"all" matches every residue
            if(split_definition[m].str().find("all")!=std::string::npos){
              continue;
            }
            //check, whether currrent residue matches current id position
            if(split_definition[m].str().find(single_ids[m])==std::string::npos){
              match=false;
              break;
            }
          }
          if(match){
            index_mapper_[i*400 + j*20 + k] = l;
            found_id = true;
            break;
          }
        }
        if(!found_id){
          std::stringstream ss;
          ss << "Could not match residues " << single_ids[0] << ", " << single_ids[1];
          ss << " and " << single_ids[2] << " to any of the group_identifiers";
          throw promod3::Error(ss.str());
        }
      }
    }
  }

  bins_per_dimension_ = bins_per_dimension;
  bin_size_ = 2 * Real(M_PI) / bins_per_dimension_;

  //initialize the histogram
  histogram_.resize(group_definitions.size());
  for(uint i = 0; i < group_definitions.size(); ++i){
    histogram_[i].assign(bins_per_dimension_ * bins_per_dimension_, 0);
  }

  //Let's update the distribution... This mainly burns CPU since all probabilities
  //will be set to zero. We do not really care at this point and prefer it over 
  //risking segfaults when to probabilities are not set...
  this->UpdateDistributions();
}

TorsionSampler::TorsionSampler(int bins_per_dimension, uint seed)
                              : gen_(seed)
                              , bins_per_dimension_(bins_per_dimension)
                              , bin_size_(2 * Real(M_PI) / bins_per_dimension)
                              , conditional_probabilities_(false) { }

TorsionSamplerPtr TorsionSampler::Load(const String& filename, uint seed) {
  
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionSampler::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // see Save for required types...
  core::CheckTypeSize<int>(in_stream);
  core::CheckTypeSize<uint>(in_stream);
  core::CheckBaseType<int>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<char>(in_stream);
  // raw data
  uint bins_per_dimension = 0;
  in_stream.read(reinterpret_cast<char*>(&bins_per_dimension), sizeof(int));
  TorsionSamplerPtr sampler(new TorsionSampler(bins_per_dimension, seed));
  uint num_histograms = 0;
  in_stream.read(reinterpret_cast<char*>(&num_histograms), sizeof(uint));
  in_stream.read(reinterpret_cast<char*>(sampler->index_mapper_),
                 8000 * sizeof(uint));
  uint total_bins = bins_per_dimension * bins_per_dimension;
  sampler->histogram_.resize(num_histograms);
  for (uint i = 0; i < num_histograms; ++i) {
    sampler->histogram_[i].resize(total_bins);
    in_stream.read(reinterpret_cast<char*>(&sampler->histogram_[i][0]),
                                           total_bins * sizeof(int));
  }

  sampler->UpdateDistributions();

  in_stream.close();
  return sampler;
}

void TorsionSampler::Save(const String& filename) {
  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: int, uint, char
  core::WriteTypeSize<int>(out_stream);
  core::WriteTypeSize<uint>(out_stream);
  core::WriteBaseType<int>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<char>(out_stream);
  // raw data
  out_stream.write(reinterpret_cast<char*>(&bins_per_dimension_), sizeof(int));
  uint num_histograms = histogram_.size();
  out_stream.write(reinterpret_cast<char*>(&num_histograms), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(index_mapper_), 8000 * sizeof(uint));
  int bytes_per_histogram = bins_per_dimension_ * bins_per_dimension_ * sizeof(int);
  for (uint i = 0; i < num_histograms; ++i) {
    out_stream.write(reinterpret_cast<char*>(&histogram_[i][0]),
                                             bytes_per_histogram);
  }
  out_stream.close();
}

TorsionSamplerPtr TorsionSampler::LoadPortable(const String& filename,
                                               uint seed) {
  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed
  core::CheckTypeSize<int>(in_stream, true);
  core::CheckBaseType<int32_t>(in_stream);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<uint64_t>(in_stream);
  core::CheckBaseType<char>(in_stream);

  // get necessary data to construct TorsionSampler
  int bins_per_dimension;
  core::ConvertBaseType<int32_t>(in_stream, bins_per_dimension);

  TorsionSamplerPtr sampler(new TorsionSampler(bins_per_dimension, seed));

  for(int i = 0; i < 8000; ++i){
    core::ConvertBaseType<uint32_t>(in_stream, sampler->index_mapper_[i]);
  }

  uint num_histograms;
  core::ConvertBaseType<uint32_t>(in_stream, num_histograms);
  sampler->histogram_.resize(num_histograms);
  for(uint i = 0; i < num_histograms; ++i){
    sampler->histogram_[i].resize(bins_per_dimension * bins_per_dimension);
    for(int j = 0; j < bins_per_dimension * bins_per_dimension; ++j){
      core::ConvertBaseType<int32_t>(in_stream, sampler->histogram_[i][j]);
    }
  }

  sampler->UpdateDistributions();
  in_stream_.close();
  return sampler;
}

void TorsionSampler::SavePortable(const String& filename) {
  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // here: only base-type-checks needed
  core::WriteTypeSize<int32_t>(out_stream);
  core::WriteBaseType<int32_t>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<uint64_t>(out_stream);
  core::WriteBaseType<char>(out_stream);

  // dump me
  core::ConvertBaseType<int32_t>(out_stream, bins_per_dimension_);
  for(int i = 0; i < 8000; ++i){
    core::ConvertBaseType<uint32_t>(out_stream, index_mapper_[i]);
  }

  // note: cannot use "out_stream & histogram_" due to int<->int32_t conversion
  uint32_t num_histograms = histogram_.size();
  out_stream & num_histograms;
  for (uint i = 0; i < num_histograms; ++i) {
    for (int j = 0; j < bins_per_dimension_ * bins_per_dimension_; ++j) {
      core::ConvertBaseType<int32_t>(out_stream, histogram_[i][j]);
    }
  }
  out_stream_.close();
}

bool TorsionSampler::operator==(const TorsionSampler& rhs) const {
  return gen_ == rhs.gen_
      && bins_per_dimension_ == rhs.bins_per_dimension_
      && bin_size_ == rhs.bin_size_
      && (memcmp(index_mapper_, rhs.index_mapper_, 8000*sizeof(uint)) == 0)
      && histogram_ == rhs.histogram_
      && probabilities_ == rhs.probabilities_
      && summed_phi_probabilities_ == rhs.summed_phi_probabilities_
      && summed_psi_probabilities_ == rhs.summed_psi_probabilities_
      && distributions_ == rhs.distributions_
      && distributions_cond_phi_ == rhs.distributions_cond_phi_
      && distributions_cond_psi_ == rhs.distributions_cond_psi_;
}

void TorsionSampler::ExtractStatistics(ost::mol::EntityView& view){
  ost::mol::ResidueViewList res_list = view.GetResidueList();
  ost::mol::ResidueHandle before, central, after;
  String key;

  for(ost::mol::ResidueViewList::iterator i = res_list.begin(); i != res_list.end(); ++i){
    ost::mol::ResidueHandle central = i->GetHandle();
    if(!central.IsValid()) continue;
    ost::mol::TorsionHandle phi_handle = central.GetPhiTorsion();
    ost::mol::TorsionHandle psi_handle = central.GetPsiTorsion();
    if(!phi_handle.IsValid() || !psi_handle.IsValid()) continue;
    before = central.GetPrev();
    after = central.GetNext();
    if(!before.IsValid() || !after.IsValid()) continue;
    try{
      uint histogram_idx = this->GetHistogramIndex(ost::conop::ResidueToAminoAcid(before),
                                                   ost::conop::ResidueToAminoAcid(central),
                                                   ost::conop::ResidueToAminoAcid(after));
      int phi_bin = this->GetAngleBin(phi_handle.GetAngle());
      int psi_bin = this->GetAngleBin(psi_handle.GetAngle());
      ++histogram_[histogram_idx][bins_per_dimension_ * psi_bin + phi_bin];
    }catch(promod3::Error& e){
      //non default amino acids...
      continue;
    }
  }
}

std::pair<Real,Real> TorsionSampler::Draw(ost::conop::AminoAcid before, 
                                          ost::conop::AminoAcid central, 
                                          ost::conop::AminoAcid after) {
  uint histogram_index = this->GetHistogramIndex(before, central, after);
  return this->Draw(histogram_index);
}

std::pair<Real,Real> TorsionSampler::Draw(uint histogram_index){
  if(histogram_index>=distributions_.size()){
    throw promod3::Error("Your provided index exceeds distributions present in torsion sampler!");
  }
  int random_int = distributions_[histogram_index](gen_);
  int int_phi = random_int % bins_per_dimension_;
  int int_psi = (random_int - int_phi)/bins_per_dimension_;
  Real phi = bin_size_*int_phi-Real(M_PI) + bin_size_/2;
  Real psi = bin_size_*int_psi-Real(M_PI) + bin_size_/2;
  return std::make_pair(phi,psi);
}

Real TorsionSampler::DrawPhiGivenPsi(ost::conop::AminoAcid before, 
                                     ost::conop::AminoAcid central, 
                                     ost::conop::AminoAcid after, 
                                     Real psi) {
  uint histogram_index = this->GetHistogramIndex(before, central, after);
  return this->DrawPhiGivenPsi(histogram_index,psi);  
}

Real TorsionSampler::DrawPhiGivenPsi(uint histogram_index, Real psi){
  if (!conditional_probabilities_) this->UpdateConditionalDistributions();

  if (histogram_index>=distributions_cond_psi_.size()) {
    throw promod3::Error("Your provided index exceeds distributions present in torsion sampler!");
  }

  int psi_bin = this->GetAngleBin(psi);
  int random_int = distributions_cond_psi_[histogram_index][psi_bin](gen_);
  Real phi = bin_size_*random_int-Real(M_PI) + bin_size_/2;
  return phi;
}

Real TorsionSampler::DrawPsiGivenPhi(ost::conop::AminoAcid before, 
                                     ost::conop::AminoAcid central, 
                                     ost::conop::AminoAcid after, Real phi){
  uint histogram_index = this->GetHistogramIndex(before,central,after);
  return this->DrawPsiGivenPhi(histogram_index,phi);  
}

Real TorsionSampler::DrawPsiGivenPhi(uint histogram_index, Real phi){
  if (!conditional_probabilities_) this->UpdateConditionalDistributions();

  if (histogram_index>=distributions_cond_phi_.size()) {
    throw promod3::Error("Your provided index exceeds distributions present in torsion sampler!");
  }

  int phi_bin = this->GetAngleBin(phi);
  int random_int = distributions_cond_phi_[histogram_index][phi_bin](gen_);
  Real psi = bin_size_*random_int-Real(M_PI) + bin_size_/2;
  return psi;
}

Real TorsionSampler::GetProbability(ost::conop::AminoAcid before, 
                                    ost::conop::AminoAcid central, 
                                    ost::conop::AminoAcid after, 
                                    const std::pair<Real,Real>& dihedrals){
  uint histogram_index = this->GetHistogramIndex(before,central,after);
  return this->GetProbability(histogram_index,dihedrals);

}

Real TorsionSampler::GetProbability(uint histogram_index, const std::pair<Real,Real>& dihedrals){
  if(histogram_index>=distributions_.size()){
    throw promod3::Error("Your provided index exceeds distributions present in torsion sampler!");
  }

  int phi_bin = this->GetAngleBin(dihedrals.first);
  int psi_bin = this->GetAngleBin(dihedrals.second);
  return probabilities_[histogram_index][bins_per_dimension_ * psi_bin + phi_bin];
}

Real TorsionSampler::GetPhiProbabilityGivenPsi(ost::conop::AminoAcid before, 
                                               ost::conop::AminoAcid central, 
                                               ost::conop::AminoAcid after, 
                                               Real phi, Real psi){
  uint histogram_index = this->GetHistogramIndex(before,central,after);
  return this->GetPhiProbabilityGivenPsi(histogram_index,phi,psi);
}

Real TorsionSampler::GetPhiProbabilityGivenPsi(uint histogram_index, Real phi, Real psi){
  if (!conditional_probabilities_) this->UpdateConditionalDistributions();

  if (histogram_index>=distributions_.size()) {
    throw promod3::Error("Your provided index exceeds distributions present in torsion sampler!");
  }

  int phi_bin = this->GetAngleBin(phi);
  int psi_bin = this->GetAngleBin(psi);
  Real p = probabilities_[histogram_index][bins_per_dimension_ * psi_bin + phi_bin];
  //if p > 0.0, summed_probabilities are nonzero by definition
  if(p > 0.0) return p / summed_psi_probabilities_[histogram_index][psi_bin];
  return 0.0;
}

Real TorsionSampler::GetPsiProbabilityGivenPhi(ost::conop::AminoAcid before, 
                                               ost::conop::AminoAcid central, 
                                               ost::conop::AminoAcid after, 
                                               Real psi, Real phi){
  uint histogram_index = this->GetHistogramIndex(before,central,after);
  return this->GetPhiProbabilityGivenPsi(histogram_index,psi,phi);
}

Real TorsionSampler::GetPsiProbabilityGivenPhi(uint histogram_index, Real psi, Real phi){
  if (!conditional_probabilities_) this->UpdateConditionalDistributions();

  if (histogram_index>=distributions_.size()) {
    throw promod3::Error("Your provided index exceeds distributions present in torsion sampler!");
  }

  int phi_bin = this->GetAngleBin(phi);
  int psi_bin = this->GetAngleBin(psi);
  Real p = probabilities_[histogram_index][bins_per_dimension_ * psi_bin + phi_bin];
  //if p > 0.0, summed_probabilities are nonzero by definition
  if(p > 0.0) return p / summed_phi_probabilities_[histogram_index][phi_bin];
  return 0.0;
}

uint TorsionSampler::GetHistogramIndex(ost::conop::AminoAcid before, 
                                       ost::conop::AminoAcid central, 
                                       ost::conop::AminoAcid after) const{

  if(before == ost::conop::XXX || central == ost::conop::XXX || 
     after == ost::conop::XXX){
    throw promod3::Error("Can only deal with default amino acids!");
  }
  return index_mapper_[before*400 + central*20 + after];
}

std::vector<uint> TorsionSampler::GetHistogramIndices(const String& sequence){

  if(sequence.size() < 3){
    throw promod3::Error("Sequence must be at least of length 3 to extract histogram indices!");
  }

  std::vector<ost::conop::AminoAcid> aa;
  for(uint i = 0; i < sequence.size(); ++i){
    aa.push_back(ost::conop::OneLetterCodeToAminoAcid(sequence[i]));
    if(aa.back() == ost::conop::XXX){
      throw promod3::Error("Only standard amino acids are allowed for getting histogram indices!");
    }
  }

  std::vector<uint> ret_value;

  for(uint i = 1; i < sequence.size()-1; ++i){
    ret_value.push_back(this->GetHistogramIndex(aa[i-1], aa[i], aa[i+1]));
  }

  return ret_value;
}

void TorsionSampler::UpdateDistributions() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "TorsionSampler::UpdateDistributions", 2);

  uint num_histograms = histogram_.size();
  //let's resize all distributions and to current histogram 
  distributions_.resize(num_histograms);
  probabilities_.resize(num_histograms);

  //fill phi/psi stuff
  for(uint i = 0; i < num_histograms; ++i){
    distributions_[i] = boost::random::discrete_distribution<int,Real>(histogram_[i].begin(),histogram_[i].end());
    probabilities_[i] = distributions_[i].probabilities();
  }

  conditional_probabilities_ = false; 
}

void TorsionSampler::UpdateConditionalDistributions() {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                          "TorsionSampler::UpdateConditionalDistributions", 2);

  uint num_histograms = histogram_.size();
  //let's resize all distributions, probabilities and summed probabilities
  //according to current histogram 
  summed_phi_probabilities_.resize(num_histograms);
  summed_psi_probabilities_.resize(num_histograms);
  distributions_cond_phi_.resize(num_histograms);
  distributions_cond_psi_.resize(num_histograms);

  //fill summed probabilities
  Real phi_sum, psi_sum;
  for(uint i = 0; i < num_histograms; ++i){
    summed_phi_probabilities_[i].resize(bins_per_dimension_);
    summed_psi_probabilities_[i].resize(bins_per_dimension_);
    for(int j = 0; j < bins_per_dimension_; ++j){
      phi_sum = 0.0;
      psi_sum = 0.0;
      for(int k = 0; k < bins_per_dimension_; ++k){
        phi_sum += probabilities_[i][j + k * bins_per_dimension_];
        psi_sum += probabilities_[i][j * bins_per_dimension_ + k];
      }
      summed_phi_probabilities_[i][j] = phi_sum;
      summed_psi_probabilities_[i][j] = psi_sum;
    }
  }

  //fill phi given psi stuff
  for(uint i = 0; i < num_histograms; ++i){
    distributions_cond_psi_[i].resize(bins_per_dimension_);
    for(int j = 0; j < bins_per_dimension_; ++j){
      std::vector<int>::iterator b = histogram_[i].begin() + j * bins_per_dimension_;
      std::vector<int>::iterator e = histogram_[i].begin() + (j + 1) * bins_per_dimension_;
      distributions_cond_psi_[i][j] = boost::random::discrete_distribution<int,Real>(b,e);
    }
  }

  //fill psi given phi stuff
  std::vector<int> temp(bins_per_dimension_,0);
  for(uint i = 0; i < num_histograms; ++i){
    distributions_cond_phi_[i].resize(bins_per_dimension_);
    for(int j = 0; j < bins_per_dimension_; ++j){
      for(int k = 0; k < bins_per_dimension_; ++k){
        temp[k] = histogram_[i][j + k * bins_per_dimension_];
      }
      distributions_cond_phi_[i][j] = boost::random::discrete_distribution<int,Real>(temp.begin(),temp.end());
    }
  } 

  conditional_probabilities_ = true; 
}

}} //ns
