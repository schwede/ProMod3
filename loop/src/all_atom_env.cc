// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/all_atom_env.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>
#include <cassert>

namespace promod3 { namespace loop {

AllAtomEnv::AllAtomEnv(const ost::seq::SequenceList& seqres): seqres_(seqres) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::AllAtomEnv", 2);

  // setup IdxHandler
  std::vector<size_t> chain_sizes;
  for (ost::seq::SequenceList::Iterator i = seqres_.begin(); 
       i != seqres_.end(); ++i) {
    chain_sizes.push_back(i->GetLength());
  }
  idx_handler_.reset(new IdxHandler(chain_sizes));

  // setup AllAtomPositions
  String full_sequence = "";
  for (ost::seq::SequenceList::Iterator i = seqres_.begin(); 
       i != seqres_.end(); ++i) {
    uint seq_length = i->GetLength();
    for (uint j = 0; j < seq_length; ++j) {
      full_sequence += (*i)[j];
    }
  }
  all_pos_.reset(new AllAtomPositions(full_sequence));

  assert(all_pos_->GetNumResidues() == idx_handler_->GetNumResidues());
}

void AllAtomEnv::SetInitialEnvironment(const ost::mol::EntityHandle& env) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::SetInitialEnvironment", 2);
  
  // check for consistency in seqres and env by simply checking the consistency
  // of the the one letter codes at the according positions
  ost::mol::ChainHandleList chain_list = env.GetChainList();
  ost::mol::ResidueHandleList res_list;

  if (chain_list.size() < idx_handler_->GetNumChains()) {
    throw promod3::Error("Less chains than seqres sequences when setting the "
                         "AllAtomEnv environment!");
  }

  for (uint i = 0; i < idx_handler_->GetNumChains(); ++i) {

    res_list = chain_list[i].GetResidueList();

    for (ost::mol::ResidueHandleList::iterator j = res_list.begin(),
         e = res_list.end(); j != e; ++j) {

      const uint num = j->GetNumber().GetNum();

      if (num == 0) {
        String err = "Observed residue number of 0 in residue ";
        err += j->GetQualifiedName();
        err += ". Per definition, the residue numbers must start from 1! ";
        err += "don't do anything...";
        throw promod3::Error(err);
      }

      if (num > idx_handler_->GetChainSize(i)) {
        String err = "ResNum of res " + j->GetQualifiedName();
        err += " exceeds size of seqres, the AllAtomEnv has been initialized ";
        err += "with! Don't do anything...";
        throw promod3::Error(err);
      }

      const uint res_idx = idx_handler_->ToIdx(num, i);
      const char olc = j->GetOneLetterCode();
      const ost::conop::AminoAcid myaa = all_pos_->GetAA(res_idx);

      if (myaa != ost::conop::OneLetterCodeToAminoAcid(olc)) {
        String err = "Sequence mismatch when setting loop scorers env! ";
        err += j->GetQualifiedName() + " should be a ";
        err += ost::conop::AminoAcidToResidueName(myaa);
        throw promod3::Error(err);
      }
    }
  }

  // it could be, that there has already been another structure set...
  // -> let's clear the existing environment
  for (uint i = 0; i < all_pos_->GetNumAtoms(); ++i) all_pos_->ClearPos(i);

  // let's set the new data
  for (uint i = 0; i < idx_handler_->GetNumChains(); ++i) {
    res_list = chain_list[i].GetResidueList();
    for (ost::mol::ResidueHandleList::iterator j = res_list.begin(),
         e = res_list.end(); j != e; ++j) {
      const uint res_idx = idx_handler_->ToIdx(j->GetNumber().GetNum(), i);
      all_pos_->SetResidue(res_idx, *j);
    }
  }
  // notify the listeners about the newly set stuff
  for (uint i = 0; i < listener_.size(); ++i) {
    listener_[i]->ResetEnvironment(*this);
  }
}

void AllAtomEnv::SetEnvironment(const AllAtomPositions& new_pos,
                                uint start_resnum, uint chain_idx) {
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::SetEnvironmentRange", 2);
  SetEnvironment(new_pos, idx_handler_->ToIdxVector(start_resnum,
                                                    new_pos.GetNumResidues(),
                                                    chain_idx));
}

void AllAtomEnv::SetEnvironment(const AllAtomPositions& new_pos,
                                const std::vector<uint>& res_indices) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::SetEnvironment", 2);
  // consistency check
  const uint num_residues = res_indices.size();
  if (num_residues != new_pos.GetNumResidues()) {
    throw promod3::Error("Must have same number of residues as indices "
                         "when setting env. of AllAtomEnv!");
  }

  // update data
  for (uint i = 0; i < num_residues; ++i) {
    // note: SetResidue checks AA consistency
    all_pos_->SetResidue(res_indices[i], new_pos, i);
  }

  // notify listeners
  for (uint i = 0; i < listener_.size(); ++i) {
    listener_[i]->UpdateEnvironment(*this, res_indices);
  }
}

void AllAtomEnv::SetEnvironment(const AllAtomEnvPositions& new_env_pos) {
  AllAtomPositionsPtr new_pos = new_env_pos.all_pos;
  if (!new_pos) throw promod3::Error("Unitialized positions passed!");
  SetEnvironment(*new_pos, new_env_pos.res_indices);
}

void AllAtomEnv::SetEnvironment(const BackboneList& bb_list,
                                uint start_resnum, uint chain_idx) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::SetEnvironmentBB", 2);

  // get indices (needed to notify listeners anyways)
  std::vector<uint> res_indices
    = idx_handler_->ToIdxVector(start_resnum, bb_list.size(), chain_idx);
  // update data
  for (uint i = 0; i < res_indices.size(); ++i) {
    // check AA
    const uint res_idx = res_indices[i];
    if (bb_list.GetAA(i) != all_pos_->GetAA(res_idx)) {
      throw promod3::Error("Sequence mismatch when setting environment in "
                           "AllAtomEnv!");
    }
    // set BB
    const uint first_idx = all_pos_->GetFirstIndex(res_idx);
    all_pos_->SetPos(first_idx + BB_N_INDEX, bb_list.GetN(i));
    all_pos_->SetPos(first_idx + BB_CA_INDEX, bb_list.GetCA(i));
    all_pos_->SetPos(first_idx + BB_C_INDEX, bb_list.GetC(i));
    all_pos_->SetPos(first_idx + BB_O_INDEX, bb_list.GetO(i));
    if (all_pos_->GetAA(res_idx) != ost::conop::GLY) {
      all_pos_->SetPos(first_idx + BB_CB_INDEX, bb_list.GetCB(i));
    }
    // clear SC
    const uint last_idx = all_pos_->GetLastIndex(res_idx);
    for (uint idx = first_idx + BB_CB_INDEX + 1; idx <= last_idx; ++idx) {
      all_pos_->ClearPos(idx);
    }
  }

  // notify listeners
  for (uint i = 0; i < listener_.size(); ++i) {
    listener_[i]->UpdateEnvironment(*this, res_indices);
  }
}

void AllAtomEnv::ClearEnvironment(uint start_resnum, uint num_residues, 
                                  uint chain_idx) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::ClearEnvironment", 2);

  // clear residue positions and collect cleared ones
  std::vector<uint> res_indices = idx_handler_->ToIdxVector(start_resnum,
                                                            num_residues,
                                                            chain_idx);
  for (uint i = 0; i < num_residues; ++i) all_pos_->ClearResidue(res_indices[i]);

  // notify all listeners
  for (uint i = 0; i < listener_.size(); ++i) {
    listener_[i]->UpdateEnvironment(*this, res_indices);
  }
}

AllAtomEnvPositionsPtr AllAtomEnv::GetEnvironment(uint start_resnum,
                                                  uint num_residues,
                                                  uint chain_idx) {
  // just wrap to other function...
  return GetEnvironment(idx_handler_->ToIdxVector(start_resnum,
                                                  num_residues,
                                                  chain_idx));
}

AllAtomEnvPositionsPtr
AllAtomEnv::GetEnvironment(const std::vector<uint>& res_indices) {
  // setup copy and return
  AllAtomEnvPositionsPtr res(new AllAtomEnvPositions);
  res->all_pos = all_pos_->Extract(res_indices);
  res->res_indices = res_indices;
  return res;
}

AllAtomEnvListenerPtr AllAtomEnv::GetListener(const String& id) const {
  for (uint i = 0; i < listener_.size(); ++i) {
    if (listener_[i]->WhoAmI() == id) return listener_[i];
  }
  return AllAtomEnvListenerPtr();
}

void AllAtomEnv::AttachListener(AllAtomEnvListenerPtr listener) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomEnv::AttachListener", 2);

  listener->Init(*this);
  listener->ResetEnvironment(*this);
  listener_.push_back(listener);
}

}} //ns
