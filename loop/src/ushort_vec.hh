// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_USHORT_VEC_HH
#define PROMOD_USHORT_VEC_HH

#include <ost/geom/vec3.hh>
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3 {

struct UShortVec
{
  UShortVec():
    x(0), y(0), z(0)
  { }
  UShortVec(unsigned short ax, unsigned short ay, 
            unsigned short az):
    x(ax), y(ay), z(az)
  { }

  UShortVec(const geom::Vec3& v):
    x(static_cast<int>(round(v[0]*100))), y(static_cast<int>(round(v[1]*100))),
    z(static_cast<int>(round(v[2]*100)))
  { }

  geom::Vec3 ToVec3() const
  {
    return geom::Vec3(x*0.01, y*0.01, z*0.01);  
  }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    core::ConvertBaseType<uint16_t>(ds, x);
    core::ConvertBaseType<uint16_t>(ds, y);
    core::ConvertBaseType<uint16_t>(ds, z);
  }

  bool operator==(const UShortVec& rhs) const {
    return x == rhs.x
        && y == rhs.y
        && z == rhs.z;
  }
  bool operator!=(const UShortVec& rhs) const {
    return !this->operator==(rhs);
  }

  unsigned short x;
  unsigned short y;
  unsigned short z;
};

}

#endif

