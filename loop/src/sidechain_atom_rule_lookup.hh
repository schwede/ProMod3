// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


/// \brief Defines rules to construct sidechain heavy atoms from dihedral angles.

#ifndef PROMOD3_SIDECHAIN_ATOM_RULE_LOOKUP_HH
#define PROMOD3_SIDECHAIN_ATOM_RULE_LOOKUP_HH

#include <promod3/loop/amino_acid_atoms.hh>

namespace promod3 { namespace loop {

struct SidechainAtomRule {
  uint sidechain_atom_idx;
  uint anchor_idx[3];
  Real bond_length;
  Real angle;
  // 0: chi1, 1: chi2, 2: chi3, 3: chi4, 4: 0.0
  uint dihedral_idx;
  // the value of the dihedral above will be added to base_dihedral to get
  // the final diheral angle. If you want to have the effect of chi3 + M_PI
  // you define dihedral_idx as 2 and base_dihedral = M_PI.
  Real base_dihedral;
};

struct ChiDefinition{
  uint idx_one;
  uint idx_two;
  uint idx_three;
  uint idx_four;
};

// Defines lookups to build sidechain atoms based on dihedrals.
class SidechainAtomRuleLookup {
public:
  // Singleton access to one constant instance (see AminoAcidLookup for details)
  static const SidechainAtomRuleLookup& GetInstance() {
    static SidechainAtomRuleLookup instance;
    return instance;
  }

  // Data access
  const std::vector<SidechainAtomRule>& GetRules(ost::conop::AminoAcid aa) const {
    return rules_[aa];
  }

  const std::vector<ChiDefinition>& GetChiDefinitions(ost::conop::AminoAcid aa) const{
    return chi_definitions_[aa];
  }

  ChiDefinition GetChiDefinition(ost::conop::AminoAcid aa, uint chi_idx) const;

  ChiDefinition GetChi1Definition(ost::conop::AminoAcid aa) const{
    return this->GetChiDefinition(aa, 0);
  }

  ChiDefinition GetChi2Definition(ost::conop::AminoAcid aa) const{
    return this->GetChiDefinition(aa, 1);
  }

  ChiDefinition GetChi3Definition(ost::conop::AminoAcid aa) const{
    return this->GetChiDefinition(aa, 2);
  }

  ChiDefinition GetChi4Definition(ost::conop::AminoAcid aa) const{
    return this->GetChiDefinition(aa, 3);
  }

  uint GetNumSidechainDihedrals(ost::conop::AminoAcid aa) const {
    return chi_definitions_[aa].size();
  }

private:
  
  // helper
  void AddRule(ost::conop::AminoAcid aa, uint sidechain_atom_idx,
               uint anch_1, uint anch_2, uint anch_3, 
               Real bond_length, Real angle, uint dihedral_idx, 
               Real base_dihedral);

  void AddChiDefinition(ost::conop::AminoAcid aa,
                        uint idx_one, uint idx_two, 
                        uint idx_three, uint idx_four);


  // construction only inside here
  SidechainAtomRuleLookup();

  // lookup tables
  std::vector<SidechainAtomRule> rules_[ost::conop::XXX + 1];
  std::vector<ChiDefinition> chi_definitions_[ost::conop::XXX + 1];
};

}} // ns

#endif
