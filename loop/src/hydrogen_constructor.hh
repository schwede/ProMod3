// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


/// \brief Defines functionality to construct hydrogens for amino acids.

#ifndef PROMOD3_HYDROGEN_CONSTRUCTOR_HH
#define PROMOD3_HYDROGEN_CONSTRUCTOR_HH

#include <promod3/loop/all_atom_positions.hh>
#include <promod3/loop/hydrogen_rule_lookup.hh>
#include <ost/mol/entity_handle.hh>
#include <ost/mol/residue_handle.hh>

namespace promod3 { namespace loop {

class HydrogenStorage;
typedef boost::shared_ptr<HydrogenStorage> HydrogenStoragePtr;

// Defines storage for hydrogen positions of a single amino acid
// -> access with ..HydrogenIndex (amino_acid_atoms.hh) enums as index
// -> each position can be set or unset
class HydrogenStorage {
public:
  HydrogenStorage() {
    const uint max_h = AminoAcidLookup::GetInstance().GetMaxNumHydrogens();
    pos_.resize(max_h);
    is_set_.assign(max_h, false);
  }
  // pos itself only relevant if set
  const geom::Vec3& GetPos(uint idx) const { return pos_[idx]; }
  void SetPos(uint idx, const geom::Vec3& pos) {
    pos_[idx] = pos;
    is_set_[idx] = true;
  }
  bool IsSet(uint idx) const { return is_set_[idx]; }
  // clear = all pos "unset"
  void Clear() {
    is_set_.assign(is_set_.size(), false);
  }
  void ClearPos(uint idx) { is_set_[idx] = false; }
private:
  std::vector<geom::Vec3> pos_;
  std::vector<bool> is_set_;
};

// Construct entity with hydrogens from OST entity / AAP (single chain)
ost::mol::EntityHandle
ConstructProtonatedEntity(const ost::mol::EntityHandle& ent,
                          bool only_polar = false,
                          HisProtonationState state = PROT_STATE_HISE);
ost::mol::EntityHandle
ConstructProtonatedEntity(const AllAtomPositions& all_pos,
                          bool only_polar = false,
                          HisProtonationState state = PROT_STATE_HISE);

// Construct hydrogen positions for given AAP residue
// -> output written into given HydrogenStorage (cleared first!)
// -> each hydrogen is only constructed if all needed pos. are set in AAP
// -> state only needed to choose prot. state of HIS
// -> returns number of actually constructed hydrogens
uint ConstructHydrogens(const AllAtomPositions& all_pos, uint res_idx,
                        HydrogenStorage& hydrogens, bool only_polar = false,
                        HisProtonationState state = PROT_STATE_HISE);

// Construct N-hydrogen using N, CA from given AAP res. and C from prev. one
// -> output added into given HydrogenStorage
// -> hydrogen is only constructed if all needed pos. are set in AAP
// -> returns number of actually constructed hydrogens
uint ConstructHydrogenN(const AllAtomPositions& all_pos, uint res_idx,
                        const geom::Vec3& prev_c_pos,
                        HydrogenStorage& hydrogens);

// Construct N-hydrogen using N, C, CA and phi dihedral from given AAP res.
// -> can make sense if we can guess/estimate phi w/o knowing prev. res.
// -> see above for details
uint ConstructHydrogenN(const AllAtomPositions& all_pos, uint res_idx,
                        Real phi, HydrogenStorage& hydrogens);

// Construct terminal N-hydrogens using N, C, CA from given AAP res.
// -> output added into given HydrogenStorage
// -> hydrogens are only constructed if all needed pos. are set in AAP
// -> returns number of actually constructed hydrogens
uint ConstructHydrogenNTerminal(const AllAtomPositions& all_pos, uint res_idx,
                                HydrogenStorage& hydrogens);

}} // ns

#endif
