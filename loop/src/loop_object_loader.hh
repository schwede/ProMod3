// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_LOOP_OBJECT_LOADER_HH
#define PROMOD3_LOOP_OBJECT_LOADER_HH

#include <promod3/loop/torsion_sampler.hh>
#include <promod3/loop/frag_db.hh>
#include <promod3/loop/structure_db.hh>

namespace promod3 { namespace loop {

TorsionSamplerPtr LoadTorsionSampler(uint seed = 0);

TorsionSamplerPtr LoadTorsionSamplerHelical(uint seed = 0);

TorsionSamplerPtr LoadTorsionSamplerExtended(uint seed = 0);

TorsionSamplerPtr LoadTorsionSamplerCoil(uint seed = 0);

FragDBPtr LoadFragDB();

StructureDBPtr LoadStructureDB();

}} //ns

#endif
