// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/psipred_prediction.hh>

namespace promod3{ namespace loop{

PsipredPrediction::PsipredPrediction(const std::vector<char>& prediction,
	                                   const std::vector<int>& confidence){

  //check input for validity
  if(prediction.size() != confidence.size()){
  	throw promod3::Error("Size of psipred prediction and confidence is inconsistent!");
  }

  for(std::vector<char>::const_iterator i = prediction.begin(); 
  	  i != prediction.end(); ++i){
    if(!(*i == 'H' || *i == 'C' || *i == 'E')){
      throw promod3::Error("Psipred prediction must be element of ['H','E','C']!");
    }
  }

  for(std::vector<int>::const_iterator i = confidence.begin(); 
  	  i != confidence.end(); ++i){
  	if((*i)<0 || (*i)>9){
      throw promod3::Error("Psipred confidence must be in range [0,9]!");
  	}
  }

  prediction_ = prediction;
  confidence_ = confidence; 
}

PsipredPredictionPtr PsipredPrediction::Extract(uint from, uint to) const{

  if(to <= from){
  	throw promod3::Error("'from' parameter must be smaller than 'to' parameter!");
  }

  if(to > this->size()){
  	throw promod3::Error("Try to access invalid range in PsipredPrediction!");
  }

  std::vector<char> pred;
  std::vector<int> conf;

  for(uint i = from; i < to; ++i){
  	pred.push_back(prediction_[i]);
  	conf.push_back(confidence_[i]);
  }

  PsipredPredictionPtr p(new PsipredPrediction(pred,conf));
  return p;
}

void PsipredPrediction::Add(char prediction, int confidence){
  if(!(prediction == 'H' || prediction == 'E' || prediction == 'C')){
  	throw promod3::Error("Psipred prediction must be in ['H','E','C']!");
  }
  if(confidence < 0 || confidence > 9){
    throw promod3::Error("Psipred confidence must be in [0,9]!");
  }
  prediction_.push_back(prediction);
  confidence_.push_back(confidence);
}

char PsipredPrediction::GetPrediction(uint idx) const{
  if(idx >= this->size()){
    throw promod3::Error("Invalid idx for getting psipred prediction!");
  }

  return prediction_[idx];
}

int PsipredPrediction::GetConfidence(uint idx) const{
  if(idx >= this->size()){
    throw promod3::Error("Invalid idx for getting psipred confidence!");
  }
  return confidence_[idx];
}

PsipredPredictionPtr PsipredPrediction::FromHHM(const String& filename){

  std::vector<char> prediction;
  std::vector<int> confidence;

  boost::iostreams::filtering_stream<boost::iostreams::input> in;
  std::ifstream stream(filename.c_str()); 
  if(!stream){
    throw std::runtime_error("Could not open file!");
  }
  if (filename.rfind(".gz") == filename.size()-3) {
    in.push(boost::iostreams::gzip_decompressor());
  }

  bool in_sspred = false;
  bool in_ssconf = false;

  in.push(stream);
  std::string line;
  ost::StringRef sline;
  while (std::getline(in, line)) {
    sline=ost::StringRef(line.c_str(), line.length());
    sline = sline.trim();
    if (sline.empty()) continue;
    if (sline == ost::StringRef("//", 2)) break;

    if(sline.length() > 0 &&
       sline[0] == '>'){
      in_sspred = false;
      in_ssconf = false;
    }

    if (sline.length() > 8 && 
        sline.substr(0, 9)==ost::StringRef(">ss_pred ", 9)) {
      in_sspred = true;
      continue;
    }

    if (sline.length() > 8 && 
        sline.substr(0, 9)==ost::StringRef(">ss_conf ", 9)) {
      in_ssconf = true;
      continue;
    }

    if(in_sspred){
      for(uint i = 0; i < sline.size(); ++i){
        prediction.push_back(sline[i]);
      }
    }

    if(in_ssconf){
      for(uint i = 0; i < sline.size(); ++i){
        confidence.push_back(int(sline[i])-48); //note, that the number zero
                                                //has number 48 in ascii...
      }
    }
  }

  PsipredPredictionPtr p(new PsipredPrediction(prediction,confidence));
  return p;
}

PsipredPredictionPtr PsipredPrediction::FromHoriz(const String& filename){

  std::vector<char> prediction;
  std::vector<int> confidence;

  boost::iostreams::filtering_stream<boost::iostreams::input> in;
  std::ifstream stream(filename.c_str()); 
  if(!stream){
    throw std::runtime_error("Could not open file!");
  }
  if (filename.rfind(".gz") == filename.size()-3) {
    in.push(boost::iostreams::gzip_decompressor());
  }

  in.push(stream);
  std::string line;
  ost::StringRef sline;
  std::vector<ost::StringRef> split_sline;

  while (std::getline(in, line)) {
    sline=ost::StringRef(line.c_str(), line.length());
    sline = sline.trim();
    if (sline.empty()) continue;

    split_sline = sline.split();

    if(split_sline[0].str() == "Pred:" && split_sline.size() == 2){
      String data = split_sline[1].str();
      for(uint i = 0; i < data.size(); ++i){
        prediction.push_back(data[i]);
      }
    }

    if(split_sline[0].str() == "Conf:" && split_sline.size() == 2){
      String data = split_sline[1].str();
      for(uint i = 0; i < data.size(); ++i){
        confidence.push_back(int(data[i])-48); //note, that the number zero
                                               //has number 48 in ascii...
      }
    }
  }

  PsipredPredictionPtr p(new PsipredPrediction(prediction,confidence));
  return p;
}

}} //ns
