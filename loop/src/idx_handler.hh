// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_LOOP_IDX_HANDLER_HH
#define PROMOD3_LOOP_IDX_HANDLER_HH

#include <vector>
#include <promod3/loop/backbone.hh>

namespace promod3 { namespace loop {

class IdxHandler;
typedef boost::shared_ptr<IdxHandler> IdxHandlerPtr;
typedef boost::shared_ptr<const IdxHandler> ConstIdxHandlerPtr;

/// \brief Helper object to translate resnum and chain_idx to res_idx.
/// - resnum is in [1, GetChainSize(chain_idx)]
/// - chain_idx is in [0, GetNumChains()-1]
/// - res_idx is in [0, GetNumResidues()-1]
class IdxHandler {
public:
  IdxHandler() { }
  IdxHandler(const std::vector<size_t>& chain_sizes_);

  size_t GetNumChains() const { return num_chains_; }

  size_t GetNumResidues() const { return num_residues_; }

  size_t GetChainSize(uint chain_idx) const { return chain_sizes_[chain_idx]; }

  uint GetChainStartIdx(uint chain_idx) const {
    return chain_start_idx_[chain_idx];
  }

  // only use if you are sure that chain has at least 1 res.!
  uint GetChainLastIdx(uint chain_idx) const {
    return chain_start_idx_[chain_idx+1] - 1;
  }

  uint GetChainIdx(uint res_idx) const { return chain_indices_[res_idx]; }

  uint ToIdx(uint resnum, uint chain_idx) const;

  // returns vector of indices for given loop
  std::vector<uint> ToIdxVector(uint start_resnum, uint num_residues,
                                uint chain_idx) const;

  // returns vector of indices for multiple loops
  // -> uses ToLoopIndices below but loop_start_indices refer to returned index
  //    vector (all loop index vectors combined one after the other)
  std::vector<uint> ToIdxVector(const std::vector<uint>& start_resnums, 
                                const std::vector<uint>& num_residues,
                                const std::vector<uint>& chain_indices,
                                std::vector<uint>& loop_start_indices,
                                std::vector<uint>& loop_lengths,
                                bool enable_log = false) const;

  // check input and set commonly used variables
  void SetupScoreCalculation(const loop::BackboneList& bb_list,
                             uint start_resnum, uint chain_idx,
                             uint& bb_list_size, uint& start_idx,
                             uint& end_idx) const;
  void SetupScoreCalculation(uint start_resnum, uint num_residues,
                             uint chain_idx, std::vector<uint>& indices,
                             std::vector<bool>& occupied) const;
  void SetupScoreCalculation(const std::vector<uint>& start_resnum, 
                             const std::vector<uint>& num_residues,
                             const std::vector<uint>& chain_idx, 
                             std::vector<std::vector<uint> >& indices,
                             std::vector<bool>& occupied) const;
  void SetupScoreCalculation(uint start_resnum, uint num_residues,
                             uint chain_idx, std::vector<uint>& indices) const;
  void SetupScoreCalculation(const std::vector<uint>& start_resnum, 
                             const std::vector<uint>& num_residues,
                             const std::vector<uint>& chain_idx, 
                             std::vector<std::vector<uint> >& indices) const;
  void SetupScoreCalculation(uint start_resnum, uint num_residues,
                             uint chain_idx, uint& start_idx,
                             uint& end_idx) const;

  // returns start indices (as in ToIdx) and loops with resolved overlaps
  // -> enable_log is passed to ResolveOverlaps
  void ToLoopIndices(const std::vector<uint>& start_resnums, 
                     const std::vector<uint>& num_residues,
                     const std::vector<uint>& chain_indices,
                     std::vector<uint>& loop_start_indices,
                     std::vector<uint>& loop_lengths,
                     bool enable_log = false) const;

  // resolves overlaps in given loops
  // notes:
  // - loops of length 0 are removed
  // - loops are allowed to touch without being merged
  // - vectors are only modified if overlaps are found
  // - this is fast code and can safely be used with minimal overhead
  // - enable_log = true triggers LOG_INFO calls for resolved loops
  static void ResolveOverlaps(std::vector<uint>& loop_start_indices,
                              std::vector<uint>& loop_lengths,
                              bool enable_log = false);

private:

  size_t num_chains_;
  size_t num_residues_;
  std::vector<size_t> chain_sizes_;
  std::vector<uint> chain_start_idx_;
  std::vector<uint> chain_indices_;
};

}} //ns

#endif
