// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_LOOP_ALL_ATOM_POSITIONS_HH
#define PROMOD3_LOOP_ALL_ATOM_POSITIONS_HH

#include <vector>
#include <ost/mol/entity_handle.hh>
#include <ost/mol/residue_handle.hh>
#include <ost/geom/vecmat3_op.hh>
#include <promod3/core/message.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/loop/backbone.hh>

namespace promod3 { namespace loop {

class AllAtomPositions;
typedef boost::shared_ptr<AllAtomPositions> AllAtomPositionsPtr;
typedef boost::shared_ptr<const AllAtomPositions> ConstAllAtomPositionsPtr;

/// \brief Container for all heavy atoms of a list of amino acids.
/// Fastest access is with global atom indexing: use GetXXIndex to set up.
/// -> index = first_idx + atom_idx
///  -> use amino_acid_atoms.hh for atom_idx
///   -> ..AtomIndex enums or AminoAcidLookup::GetIndex..
/// -> Timings to extract BB pos. of 46 residues 1000000 times:
///    - GetPos(res_idx, atom_idx):            0.186 s
///    - GetPos(GetIndex(res_idx, atom_idx)):  0.186 s
///    - GetPos(first_idx + atom_idx):         0.186 s
///    - GetPos(GetIndex(res_idx, atom_name)): 11.10 s
/// Fast access methods defined here in header with no bound-checks!
/// Bound checks added for python exports and methods in .cc.
class AllAtomPositions {
public:
  // construct
  AllAtomPositions(const String& sequence);
  AllAtomPositions(const ost::mol::ResidueHandleList& res_list);
  AllAtomPositions(const String& sequence,
                   const ost::mol::ResidueHandleList& res_list);
  AllAtomPositions(const BackboneList& bb_list);

  // copy constructor and assignment operators copy all the data
  // -> need implementation due to aa_lookup_
  AllAtomPositions(const AllAtomPositions& other);
  AllAtomPositions& operator=(const AllAtomPositions& other);

  // set positions from residue
  // -> no atom checking done -> whatever matches expected atom names is used
  void SetResidue(uint res_idx, const ost::mol::ResidueHandle& res);
  // set positions from matching (checked) residue of other container
  void SetResidue(uint res_idx, const AllAtomPositions& other,
                  uint other_res_idx);
  // set single positions
  void SetPos(uint res_idx, uint atom_idx, const geom::Vec3& pos) {
    SetPos(GetIndex(res_idx, atom_idx), pos);
  }
  void SetPos(uint idx, const geom::Vec3& pos) {
    pos_[idx] = pos;
    is_set_[idx] = true;
  }
  // clear position (i.e. IsSet(..) will be false)
  void ClearPos(uint res_idx, uint atom_idx) {
    ClearPos(GetIndex(res_idx, atom_idx));
  }
  void ClearPos(uint idx) { is_set_[idx] = false; }
  // clear all positions of residue
  void ClearResidue(uint res_idx) {
    const uint first_idx = GetFirstIndex(res_idx);
    const uint last_idx = GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) ClearPos(idx);
  }

  // getters per atom
  const geom::Vec3& GetPos(uint res_idx, uint atom_idx) const {
    return GetPos(GetIndex(res_idx, atom_idx));
  }
  const geom::Vec3& GetPos(uint idx) const { return pos_[idx]; }
  AminoAcidAtom GetAAA(uint res_idx, uint atom_idx) const {
    return GetAAA(GetIndex(res_idx, atom_idx));
  }
  AminoAcidAtom GetAAA(uint idx) const { return aaa_[idx]; }
  bool IsSet(uint res_idx, uint atom_idx) const {
    return IsSet(GetIndex(res_idx, atom_idx));
  }
  bool IsSet(uint idx) const { return is_set_[idx]; }
  uint GetIndex(uint res_idx, uint atom_idx) const {
    return GetFirstIndex(res_idx) + atom_idx;
  }
  uint GetIndex(uint res_idx, const String& aname) const {
    return GetIndex(res_idx, aa_lookup_.GetIndex(GetAA(res_idx), aname));
  }

  // getters per residue
  uint GetFirstIndex(uint res_idx) const { return first_idx_[res_idx]; }
  uint GetLastIndex(uint res_idx) const { return first_idx_[res_idx+1] - 1; }
  ost::conop::AminoAcid GetAA(uint res_idx) const { return aa_[res_idx]; }
  // is any/all pos. set for this residue
  bool IsAnySet(uint res_idx) const {
    const uint first_idx = GetFirstIndex(res_idx);
    const uint last_idx = GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      if (IsSet(idx)) return true;
    }
    return false;
  }
  bool IsAllSet(uint res_idx) const {
    const uint first_idx = GetFirstIndex(res_idx);
    const uint last_idx = GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      if (!IsSet(idx)) return false;
    }
    return true;
  }

  // get torsions (returns default value if anything unset (no bound check!))
  Real GetPhiTorsion(uint res_idx, Real def_angle = -1.0472) const {
    // needs 0 < res_idx < GetNumResidues()
    const uint idx_Cp = GetIndex(res_idx-1, BB_C_INDEX);
    const uint idx_N = GetIndex(res_idx, BB_N_INDEX);
    const uint idx_CA = GetIndex(res_idx, BB_CA_INDEX);
    const uint idx_C = GetIndex(res_idx, BB_C_INDEX);
    if (!IsSet(idx_Cp) || !IsSet(idx_N) || !IsSet(idx_CA) || !IsSet(idx_C)) {
      return def_angle;
    } else {
      return geom::DihedralAngle(GetPos(idx_Cp), GetPos(idx_N),
                                 GetPos(idx_CA), GetPos(idx_C));
    }
  }
  Real GetPsiTorsion(uint res_idx, Real def_angle = -0.7854) const {
    // needs 0 <= res_idx < GetNumResidues() - 1
    const uint idx_N = GetIndex(res_idx, BB_N_INDEX);
    const uint idx_CA = GetIndex(res_idx, BB_CA_INDEX);
    const uint idx_C = GetIndex(res_idx, BB_C_INDEX);
    const uint idx_Nn = GetIndex(res_idx+1, BB_N_INDEX);
    if (!IsSet(idx_N) || !IsSet(idx_CA) || !IsSet(idx_C) || !IsSet(idx_Nn)) {
      return def_angle;
    } else {
      return geom::DihedralAngle(GetPos(idx_N), GetPos(idx_CA),
                                 GetPos(idx_C), GetPos(idx_Nn));
    }
  }
  Real GetOmegaTorsion(uint res_idx, Real def_angle = 3.14159) const {
    // needs 0 < res_idx < GetNumResidues()
    const uint idx_CAp = GetIndex(res_idx-1, BB_CA_INDEX);
    const uint idx_Cp = GetIndex(res_idx-1, BB_C_INDEX);
    const uint idx_N = GetIndex(res_idx, BB_N_INDEX);
    const uint idx_CA = GetIndex(res_idx, BB_CA_INDEX);
    if (!IsSet(idx_CAp) || !IsSet(idx_Cp) || !IsSet(idx_N) || !IsSet(idx_CA)) {
      return def_angle;
    } else {
      return geom::DihedralAngle(GetPos(idx_CAp), GetPos(idx_Cp),
                                 GetPos(idx_N), GetPos(idx_CA));
    }
  }

  // global getters
  uint GetNumAtoms() const { return num_atoms_; }
  uint GetNumResidues() const { return num_residues_; }
  String GetSequence() const;

  // extract full copy
  AllAtomPositionsPtr Copy() const {
    return AllAtomPositionsPtr(new AllAtomPositions(*this));
  }
  // extract subset from..to-1
  AllAtomPositionsPtr Extract(uint from, uint to) const;
  // extract new container with all given indices
  AllAtomPositionsPtr Extract(const std::vector<uint>& res_indices) const;
  // extract backbone for subset from..to-1 (throws if bb not set)
  BackboneList ExtractBackbone(uint from, uint to) const;
  // extract a single-chain entity with all the residues
  // -> connectivity is resolved with HeuristicProcessor
  ost::mol::EntityHandle ToEntity() const;
  // insert single residue into chain at given res. num. pos.
  // -> replaces existing data and (re)connects atoms
  // -> peptide links are set according to residue numbering
  void InsertInto(uint res_idx, ost::mol::ChainHandle& chain,
                  const ost::mol::ResNum& resnum) const;
  void InsertInto(uint res_idx, ost::mol::ChainHandle& chain,
                  uint resnum) const {
    InsertInto(res_idx, chain, ost::mol::ResNum(resnum));
  }

  // check bounds (throw error if out)
  void CheckResidueIndex(uint res_idx) const {
    if (res_idx >= GetNumResidues()) {
      throw promod3::Error("Invalid residue index!");
    }
  }
  void CheckIndex(uint idx) const {
    if (idx >= GetNumAtoms()) throw promod3::Error("Invalid index!");
  }
  void CheckIndex(uint res_idx, uint atom_idx) const {
    CheckResidueIndex(res_idx);
    if (GetIndex(res_idx, atom_idx) > GetLastIndex(res_idx)) {
      throw promod3::Error("Invalid atom index!");
    }
  }

  // compare objects
  bool operator==(const AllAtomPositions& other) const {
    // aaa_, first_idx_, num_atoms_, num_residues_ defined by aa_
    // -> no need to check them
    return pos_ == other.pos_ && is_set_ == other.is_set_ && aa_ == other.aa_;
  }
  bool operator!=(const AllAtomPositions& other) const {
    return !(other == (*this));
  }

private:

  // allocate data given sequence
  void Initialize(const String& sequence);

  // per atom data
  std::vector<geom::Vec3> pos_;
  std::vector<AminoAcidAtom> aaa_;
  std::vector<bool> is_set_;

  // per residue data
  std::vector<uint> first_idx_;  // one extra item = num_atoms_
  std::vector<ost::conop::AminoAcid> aa_;
  
  // global data
  uint num_atoms_;
  uint num_residues_;
  const AminoAcidLookup& aa_lookup_;
};

}} //ns

#endif
