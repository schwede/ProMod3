// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/idx_handler.hh>
#include <promod3/core/message.hh>
#include <ost/log.hh>

namespace promod3 { namespace loop {

IdxHandler::IdxHandler(const std::vector<size_t>& chain_sizes) {

  num_chains_ = chain_sizes.size();
  num_residues_ = 0;
  chain_sizes_ = chain_sizes;

  std::vector<uint> cur_chain_indices;
  for (uint i = 0; i < num_chains_; ++i) {
    chain_start_idx_.push_back(num_residues_);
    num_residues_ += chain_sizes[i];
    chain_indices_.resize(num_residues_, i);
  }
  chain_start_idx_.push_back(num_residues_);
}

uint IdxHandler::ToIdx(uint resnum, uint chain_idx) const {

  if (chain_idx >= num_chains_) {
    throw promod3::Error("Invalid chain idx!");
  }

  if (resnum == 0 || resnum - 1 >= chain_sizes_[chain_idx]) {
    throw promod3::Error("Invalid resnum!");
  }
  return chain_start_idx_[chain_idx] + (resnum - 1);
}

std::vector<uint> IdxHandler::ToIdxVector(uint start_resnum, uint num_residues,
                                          uint chain_idx) const {

  // check/get start index
  uint start_idx = ToIdx(start_resnum, chain_idx);
  // check size
  if (start_resnum-1 + num_residues > chain_sizes_[chain_idx]) {
    throw promod3::Error("Invalid End ResNum!");
  }
  // fill vector to return
  std::vector<uint> idx(num_residues);
  for (uint i = 0; i < num_residues; ++i) idx[i] = start_idx + i;
  return idx;
}

std::vector<uint>
IdxHandler::ToIdxVector(const std::vector<uint>& start_resnums, 
                        const std::vector<uint>& num_residues,
                        const std::vector<uint>& chain_indices,
                        std::vector<uint>& loop_start_indices,
                        std::vector<uint>& loop_lengths,
                        bool enable_log) const {
  // get loop indices (checks errors. resolves overlaps & reports merges/del.)
  std::vector<uint> env_loop_start_indices;
  ToLoopIndices(start_resnums, num_residues, chain_indices,
                env_loop_start_indices, loop_lengths, enable_log);

  // process output to get idx-vector
  std::vector<uint> indices;
  const uint num_loops = loop_lengths.size();
  loop_start_indices.resize(num_loops);
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    const uint loop_start_idx = indices.size();
    const uint loop_length = loop_lengths[i_loop];
    indices.resize(loop_start_idx + loop_length);
    loop_start_indices[i_loop] = loop_start_idx;
    for (uint i = 0; i < loop_length; ++i) {
      indices[loop_start_idx + i] = env_loop_start_indices[i_loop] + i;
    }
  }
  return indices;
}

void IdxHandler::SetupScoreCalculation(const loop::BackboneList& bb_list,
                                       uint start_resnum, uint chain_idx,
                                       uint& bb_list_size, uint& start_idx,
                                       uint& end_idx) const {
  // get bb_list_size and wrap to other variant
  bb_list_size = bb_list.size();
  SetupScoreCalculation(start_resnum, bb_list_size, chain_idx,
                        start_idx, end_idx);
}

void IdxHandler::SetupScoreCalculation(uint start_resnum, uint num_residues,
                           uint chain_idx, std::vector<uint>& indices,
                           std::vector<bool>& occupied) const {

  indices = this->ToIdxVector(start_resnum, num_residues, chain_idx);
  occupied.assign(this->GetNumResidues(), false);
  for(std::vector<uint>::const_iterator i = indices.begin(); 
      i != indices.end(); ++i) {
    occupied[*i] = true;
  }  
}

void IdxHandler::SetupScoreCalculation(const std::vector<uint>& start_resnum, 
                           const std::vector<uint>& num_residues,
                           const std::vector<uint>& chain_idx, 
                           std::vector<std::vector<uint> >& indices,
                           std::vector<bool>& occupied) const {

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in Score Setup!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in Score Setup!");
  }

  indices.resize(start_resnum.size());
  occupied.assign(this->GetNumResidues(), false);
  for(uint i = 0; i < start_resnum.size(); ++i) {
    indices[i] = this->ToIdxVector(start_resnum[i], num_residues[i],
                                   chain_idx[i]);
    const std::vector<uint>& vec = indices[i];
    for(std::vector<uint>::const_iterator j = vec.begin(); 
        j != vec.end(); ++j) {
      occupied[*j] = true;
    }
  }
}

void IdxHandler::SetupScoreCalculation(uint start_resnum, uint num_residues,
                           uint chain_idx, std::vector<uint>& indices) const {

  indices = this->ToIdxVector(start_resnum, num_residues, chain_idx);
}

void IdxHandler::SetupScoreCalculation(const std::vector<uint>& start_resnum, 
                           const std::vector<uint>& num_residues,
                           const std::vector<uint>& chain_idx, 
                           std::vector<std::vector<uint> >& indices) const {

  if(start_resnum.size() != num_residues.size()) {
    throw promod3::Error("Inconsistent input data in Score Setup!");
  }

  if(start_resnum.size() != chain_idx.size()) {
    throw promod3::Error("Inconsistent input data in Score Setup!");
  }

  indices.resize(start_resnum.size());
  for(uint i = 0; i < start_resnum.size(); ++i) {
    indices[i] = this->ToIdxVector(start_resnum[i], num_residues[i],
                                   chain_idx[i]);
  }
}

void IdxHandler::SetupScoreCalculation(uint start_resnum, uint num_residues,
                                       uint chain_idx, uint& start_idx,
                                       uint& end_idx) const {
  // check/get start index
  start_idx = ToIdx(start_resnum, chain_idx);
  // set/check end_idx
  end_idx = start_idx + num_residues - 1;
  if (end_idx >= chain_start_idx_[chain_idx] + chain_sizes_[chain_idx]) {
    throw promod3::Error("Invalid End ResNum!");
  }
}

void IdxHandler::ToLoopIndices(const std::vector<uint>& start_resnums,
                               const std::vector<uint>& num_residues,
                               const std::vector<uint>& chain_indices,
                               std::vector<uint>& loop_start_indices,
                               std::vector<uint>& loop_lengths,
                               bool enable_log) const {
  // check consistency
  const uint num_loops = start_resnums.size();
  if (num_loops != num_residues.size()) {
    throw promod3::Error("Size inconsistency of input loops!");
  }
  if (num_loops != chain_indices.size()) {
    throw promod3::Error("Size inconsistency of input loops!");
  }

  // get first guess of loops
  loop_start_indices.resize(num_loops);
  loop_lengths = num_residues;
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    const uint start_resnum = start_resnums[i_loop];
    const uint chain_idx = chain_indices[i_loop];
    // set start index
    loop_start_indices[i_loop] = ToIdx(start_resnum, chain_idx);
    // check size
    if (start_resnum-1 + num_residues[i_loop] > chain_sizes_[chain_idx]) {
      throw promod3::Error("Invalid End ResNum!");
    }
  }
  // resolve overlaps
  ResolveOverlaps(loop_start_indices, loop_lengths, enable_log);
}

void IdxHandler::ResolveOverlaps(std::vector<uint>& loop_start_indices,
                                 std::vector<uint>& loop_lengths,
                                 bool enable_log) {

  // strategy for each loop
  // - compare to previously processed loops
  // -> if no overlap, nothing happens
  //    (code is almost as fast as only checking overlaps)
  // -> if overlap, other loop deleted and merged into current one
  // - if merges needed, write new loop_vectors and swap after
  // -> things to be removed get loop_lengths == 0

  // keep track of loops to keep
  const uint num_loops = loop_start_indices.size();
  uint num_loops_new = num_loops;
  // check all loops
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    // check for dummy loops (skipped)
    if (loop_lengths[i_loop] == 0) {
      // kill and skip it
      --num_loops_new;
      if (enable_log) {
        LOG_INFO("Removed empty loop at index " << loop_start_indices[i_loop]);
      }
      continue;
    }
    // compare with others
    for (uint i_loop_other = 0; i_loop_other < i_loop; ++i_loop_other) {
      // loop from start_idx to start_idx + loop_lengths[i_loop] - 1
      const uint start_idx = loop_start_indices[i_loop];
      const uint end_idx = start_idx + loop_lengths[i_loop];
      const uint start_idx_other = loop_start_indices[i_loop_other];
      const uint end_idx_other = start_idx_other + loop_lengths[i_loop_other];
      // if not (end_idx <= start_idx_other || start_idx >= end_idx_other) ...
      if (end_idx > start_idx_other && start_idx < end_idx_other) {
        // update current loop
        if (start_idx_other < start_idx) {
          loop_start_indices[i_loop] = start_idx_other;
        }
        loop_lengths[i_loop] = std::max(end_idx, end_idx_other)
                             - loop_start_indices[i_loop];
        // kill other one
        loop_lengths[i_loop_other] = 0;
        --num_loops_new;
        if (enable_log) {
          LOG_INFO("Merged loops [" << start_idx << ", " << end_idx << "[ and ["
                   << start_idx_other << ", " << end_idx_other << "[");
        }
      }
    }
  }

  // build new vectors as needed
  if (num_loops_new != num_loops) {
    // new vectors containing only elements with length > 0
    std::vector<uint> loop_start_indices_new(num_loops_new);
    std::vector<uint> loop_lengths_new(num_loops_new);
    uint i_loop_new = 0;
    for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
      if (loop_lengths[i_loop] > 0) {
        assert(i_loop_new < num_loops_new); // guaranteed by algo above
        loop_start_indices_new[i_loop_new] = loop_start_indices[i_loop];
        loop_lengths_new[i_loop_new] = loop_lengths[i_loop];
        ++i_loop_new;
      }
    }
    // swap them (fast!)
    loop_start_indices.swap(loop_start_indices_new);
    loop_lengths.swap(loop_lengths_new);
  }
}

}} //ns
