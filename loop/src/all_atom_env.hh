// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_LOOP_ALL_ATOM_ENV_HH
#define PROMOD3_LOOP_ALL_ATOM_ENV_HH

#include <ost/mol/entity_handle.hh>
#include <ost/seq/sequence_list.hh>
#include <promod3/loop/all_atom_positions.hh>
#include <promod3/loop/backbone.hh>
#include <promod3/loop/idx_handler.hh>
#include <boost/shared_ptr.hpp>
#include <vector>

namespace promod3 { namespace loop {

class AllAtomEnv;
class AllAtomEnvListener;
class AllAtomEnvPositions;

typedef boost::shared_ptr<AllAtomEnv> AllAtomEnvPtr;
typedef boost::shared_ptr<AllAtomEnvListener> AllAtomEnvListenerPtr;
typedef boost::shared_ptr<AllAtomEnvPositions> AllAtomEnvPositionsPtr;

/// \brief General environment for all atom positions.
/// Can have listeners attached to it which are notified for pos. changes.
/// Pos. changes are either for continuous residues or vectors of res indices.
/// -> internal res. indexing is done via IdxHandler
class AllAtomEnv {
public:

  AllAtomEnv(const ost::seq::SequenceList& seqres);

  // set full env. (clears all exisiting first)
  // consistency is checked for all residues against seqres
  void SetInitialEnvironment(const ost::mol::EntityHandle& env);

  // fast (re)setting of environment
  void SetEnvironment(const AllAtomPositions& new_pos,
                      uint start_resnum, uint chain_idx = 0);
  void SetEnvironment(const AllAtomPositions& new_pos,
                      const std::vector<uint>& res_indices);
  void SetEnvironment(const AllAtomEnvPositions& new_env_pos);
  void SetEnvironment(const BackboneList& bb_list,
                      uint start_resnum, uint chain_idx = 0);

  void ClearEnvironment(uint start_resnum, uint num_residues, 
                        uint chain_idx = 0);

  // get env. pos. copy for backup/restore
  AllAtomEnvPositionsPtr GetEnvironment(uint start_resnum, uint num_residues,
                                        uint chain_idx = 0);
  AllAtomEnvPositionsPtr GetEnvironment(const std::vector<uint>& res_indices);

  // get internal data
  ost::seq::SequenceList GetSeqres() const { return seqres_; }
  ConstIdxHandlerPtr GetIdxHandlerData() const { return idx_handler_; }
  ConstAllAtomPositionsPtr GetAllPosData() const { return all_pos_; }

  // return listener with that ID or NULL if not existing
  AllAtomEnvListenerPtr GetListener(const String& id) const;
  // note that ownership of listener goes to this object which will eventually
  // delete the objects
  void AttachListener(AllAtomEnvListenerPtr listener);
  // ensure and return unique object of type T with given id (create if needed)
  template <typename T>
  boost::shared_ptr<T> UniqueListener(const String& id) {
    AllAtomEnvListenerPtr tst = this->GetListener(id);
    if (tst) {
      // it's already there, return it
      return boost::dynamic_pointer_cast<T>(tst);
    } else {
      // let's create a new one and attach it
      boost::shared_ptr<T> listener(new T());
      this->AttachListener(listener);
      return listener;
    }
  }

private:
  ost::seq::SequenceList seqres_;
  IdxHandlerPtr idx_handler_;
  AllAtomPositionsPtr all_pos_;

  std::vector<AllAtomEnvListenerPtr> listener_;
};

/// \brief Listener that gets notified when env. changes.
/// There is no notification which atoms where set/reset!
/// Indexing is with global res. idx. (usable for base_env.GetAllPosData())
class AllAtomEnvListener {

public:

  virtual ~AllAtomEnvListener() { }

  // called when listener is attached to environment (should happen only once!)
  virtual void Init(const AllAtomEnv& base_env) = 0;

  // called if full environment must be reset
  // -> called by AllAtomEnv::SetInitialEnvironment & AllAtomEnv::AttachListener
  virtual void ResetEnvironment(const AllAtomEnv& base_env) = 0;

  // called if any change (set/clear/reset) was requested for those residues
  // -> you may need to keep track of set stuff yourself in here
  virtual void UpdateEnvironment(const AllAtomEnv& base_env, 
                                 const std::vector<uint>& res_indices) = 0;

  // used as unique identifier of listener in AllAtomEnv
  virtual String WhoAmI() const = 0;
};

/// \brief Light-weight object to pass around positions and residue indices.
class AllAtomEnvPositions {
public:
  AllAtomPositionsPtr all_pos;
  std::vector<uint> res_indices;
};

}} //ns

#endif
