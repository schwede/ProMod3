// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_CORE_DATA_BAG_HH
#define PM3_CORE_DATA_BAG_HH

/*
  Author: Marco Biasini
 */

#include <vector>
#include <string.h>
#include <boost/scoped_array.hpp>
#include "dense_map.hh"
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3 {


/// \brief data bag iterator, that is basically just a small wrapper around
///     a dense map iterator
/// 
/// The databag iterator only provides const access to the items
template <typename C>
class TEMPLATE_EXPORT DataBagIterator {
public:
  typedef typename C::DenseMapType::ConstIterator DenseMapIterator;
  typedef std::pair<typename C::KeyType, typename C::IteratorPair> ValueType;
  DataBagIterator(const C* c, DenseMapIterator it):
    it_(it), c_(c) {    
  }
  bool operator==(const DataBagIterator<C>& rhs)
  {
    return rhs.it_==it_;
  }
  bool operator!=(const DataBagIterator<C>& rhs)
  {
    return !(*this==rhs);
  }  
  ValueType operator*()
  {
    return std::make_pair(it_->first, c_->Get(*this));
  }
  DataBagIterator<C>& operator++()
  {
    ++it_;
    return *this;
  }
  DenseMapIterator& It() { return it_; }
private:
  DenseMapIterator it_;
  const C*         c_;
};

/// A databag is an associative container for variably sized lists. 
/// 
/// The lists are made up of zero or more elements of V and are adressed over
/// the keytype K.
/// 
/// The table of contents is implemented with a DenseMap and thus a databag
/// requires you to pick a suitable hashing algorithm.
/// 
/// The databag supports two modes: readonly and readwrite mode. The internal
/// data layout is changed depending on the mode. In readonly mode, the data
/// is optimized for fast lookup and reading/writing to disk. In readwrite mode, 
/// the data layout is optimized for insertion and deletion of new elements.
/// 
/// One can always switch between these two modes by using #SetReadOnly().
///
/// NOTE: this class is only fully functional after SetEmptyKey() is called.
/// -> otherwise only Read() or Serialize() can be called
template <typename K, typename V, typename H>
class DataBag {
public:
  struct BagInfo {
    unsigned int id;
    unsigned int offset;

    // portable serialization
    // (cleanly element by element with fixed-width base-types)
    template <typename DS>
    void Serialize(DS& ds) {
      core::ConvertBaseType<uint32_t>(ds, id);
      core::ConvertBaseType<uint32_t>(ds, offset);
    }

    bool operator==(const BagInfo& rhs) const {
      return id == rhs.id && offset == rhs.offset;
    }
    bool operator!=(const BagInfo& rhs) const {
      return !this->operator==(rhs);
    }
  };
public:
  typedef DataBag<K, V, H> ClassType;
  typedef DenseMap<K, BagInfo, H>  DenseMapType;
  typedef K KeyType;
  typedef V ValueType;
  typedef std::vector<ValueType>  ValueVector;
  typedef std::pair<const ValueType*, const ValueType*> IteratorPair;
  typedef DataBagIterator<ClassType> Iterator;
public:
  DataBag(): 
    static_value_count_(0), readonly_(true)
  { }
  
  /// \brief insert new item into data bag. 
  /// 
  /// If an item of that key already exists, false is returned and no changes
  /// are made to the item. Otherwise true is returned and the sequence 
  /// begin, end is assigned to the item.
  /// 
  /// Do not call this function in read-only mode.
  /// 
  /// TODO: specialize for iterator categories here...
  template <typename I>
  bool Insert(const KeyType& key_type, I curr, I end)
  {
    assert(readonly_==false);    
    BagInfo info;
    info.id=toc_.Size();
    info.offset=0;
    std::pair<typename DenseMapType::Iterator, 
              bool> i=toc_.Insert(key_type, ValueVector());
    if (!i.second) {
      return false;
    }
    dynamic_data_.push_back(ValueVector());
    assert(toc_.Size()==static_cast<uint>(dynamic_data_.size()));
    for ( ; curr!=end; ++curr) {
      dynamic_data_.back().push_back(*curr);
    }
    return true;
  }
  
  DataBagIterator<ClassType> Begin() const
  {
    return DataBagIterator<ClassType>(this, toc_.Begin());
  }
  
  DataBagIterator<ClassType> End() const
  {
    return DataBagIterator<ClassType>(this, toc_.End());
  }
  
  void SetEmptyKey(const KeyType& key)
  {
    toc_.SetEmptyKey(key);
  }
  /// \brief append values to bag
  /// 
  /// If an item of that key already exists, the values in the range 
  /// [curr, end) are appended to the bag.
  /// 
  /// Do not call this function in read-only mode.
  /// 
  /// TODO: specialize for iterator categories here...  
  template <typename I>
  void Append(const KeyType& key, I begin, I end)
  {
    assert(readonly_==false);    
    BagInfo info;
    info.id=toc_.Size();
    info.offset=0;
    std::pair<typename DenseMapType::Iterator, bool> i=toc_.Insert(key, info);
    if (!i.second) {
      info=i.first->second;
    } else {
      dynamic_data_.push_back(ValueVector());      
    }
    assert(toc_.Size()==static_cast<uint>(dynamic_data_.size()));
    dynamic_data_[info.id].insert(dynamic_data_[info.id].end(), begin, end);
  }
  IteratorPair Get(const KeyType& key) const
  {
    if (readonly_) {
      return this->GetItemByKeyRO(key);
    } else {
      return this->GetItemByKeyRW(key);
    }
  }
  
  bool IsReadOnly() const { return readonly_; }
  
  void SetReadOnly(bool ro)
  {
    if (ro) {
      this->MakeStatic();
      readonly_=ro;      
    } else {
      this->MakeDynamic();
      readonly_=ro;
    }
  }
  
  void Write(std::ofstream& out_stream)
  {
    if (!readonly_) {
      this->MakeStatic();
    } 
    toc_.Write(out_stream);
    uint total_size=static_value_count_*sizeof(ValueType)+
                    toc_.Size()*sizeof(uint);
    out_stream.write(reinterpret_cast<char*>(&static_value_count_),
                     sizeof(uint));
    out_stream.write(reinterpret_cast<char*>(&total_size),
                     sizeof(uint));   
    out_stream.write(data_.get(), total_size);      
  }
  
  void Read(std::ifstream& in_stream)
  {
    if (!readonly_) {
      assert(0 && "don't do that. This is really stupid");
    }
    toc_.Read(in_stream);
    uint total_size;
    in_stream.read(reinterpret_cast<char*>(&static_value_count_),
                   sizeof(uint));
    in_stream.read(reinterpret_cast<char*>(&total_size),
                   sizeof(uint));
    data_.reset(new char[total_size]);
    in_stream.read(data_.get(), total_size);      
    readonly_=true;
  }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    // we need the dynamic version to work with clean serialization
    bool was_read_only = readonly_;
    if (readonly_ && !ds.IsSource()) this->MakeDynamic();
    readonly_ = false;

    // we "only" need to serialize hash table and dynamic_data_
    ds & toc_;
    ds & dynamic_data_;
    
    // keep read only if it was set before
    if (was_read_only) this->MakeStatic();
  }

  bool operator==(const DataBag& rhs) const {
    uint total_size = static_value_count_*sizeof(ValueType)
                    + toc_.Size()*sizeof(uint);
    return toc_ == rhs.toc_
        && dynamic_data_ == rhs.dynamic_data_
        && static_value_count_ == rhs.static_value_count_
        && !memcmp(data_.get(), rhs.data_.get(), total_size)
        && readonly_ == rhs.readonly_;
  }
  bool operator!=(const DataBag& rhs) const {
    return !this->operator==(rhs);
  }
  
  uint ItemCount() const
  {
    return toc_.Size();
  }
  
  uint Size() const 
  {
    typedef std::vector<ValueVector> X;
    uint size=0;
    if (readonly_==false) {
      for (typename X::const_iterator i=dynamic_data_.begin(),
           e=dynamic_data_.end(); i!=e; ++i) {
        size+=i->size();        
      }
      return size;
    } else {
      return static_value_count_;
    }
  }    
  IteratorPair Get(DataBagIterator<ClassType>& it) const
  {
    
    if (readonly_) {
      return this->GetItemRO(it.It()->second.offset);
    } else {
      return std::make_pair(&dynamic_data_[it.It()->second.id].front(),
                            &dynamic_data_[it.It()->second.id].front()+
                            dynamic_data_[it.It()->second.id].size());
    }
    return std::make_pair((ValueType*)NULL, (ValueType*)NULL);
  }  
private:
  std::pair<BagInfo, bool> GetInfo(const KeyType& key) const
  {
    typename DenseMapType::ConstIterator i=toc_.Find(key);
    if (i!=toc_.End()) {
      return std::make_pair(i->second, true);
    }
    return std::make_pair(BagInfo(), false);
  }
  IteratorPair GetItemByKeyRO(const KeyType& key) const
  {
    std::pair<BagInfo, bool> p=this->GetInfo(key);
    if (p.second) {
      return this->GetItemRO(p.first.offset);
    }
    return std::make_pair((ValueType*)NULL, (ValueType*)NULL);
  }
  IteratorPair GetItemRO(uint offset) const
  {
    const char* s=&data_[offset];
    uint len=*reinterpret_cast<const uint*>(s);
    const ValueType* begin=reinterpret_cast<const ValueType*>(s+sizeof(uint));
    return std::make_pair(begin, begin+len);
  }
  
  uint SetItemRO(uint offset, const ValueType* begin, const ValueType* end) 
  {
    char* s=&data_[offset];
    uint size=(end-begin);
    reinterpret_cast<uint&>(*s)=size;
    if (size>0) {
      memcpy(s+sizeof(uint), begin, sizeof(ValueType)*size);
    }
    return offset+sizeof(uint)+sizeof(ValueType)*size;
  }
  
  IteratorPair GetItemByKeyRW(const KeyType& key) const
  {
    std::pair<BagInfo, bool> p=this->GetInfo(key);
    if (p.second) {
      assert(!dynamic_data_[p.first.id].empty());
      return std::make_pair(&dynamic_data_[p.first.id].front(),
                            &dynamic_data_[p.first.id].front()+
                            dynamic_data_[p.first.id].size());
    }
    return std::make_pair((const ValueType*)NULL, (const ValueType*)NULL);
  }
  
  /// Convert static data into dynamic array
  void MakeDynamic()
  {
    if (readonly_ == false) return;

    // loop over all non-empty buckets
    dynamic_data_.resize(toc_.Size());
    for (typename DenseMapType::Iterator i = toc_.Begin(), 
         e = toc_.End(); i != e; ++i) {
      IteratorPair p = this->GetItemRO(i->second.offset);
      assert(dynamic_data_.size() > i->second.id);
      ValueVector& v = dynamic_data_[i->second.id];
      v.clear();
      std::copy(p.first, p.second, std::back_inserter(v));
    }

    readonly_ = false;
  }
  
  /// Convert dynamic to static data
  void MakeStatic()
  {
    if (readonly_ == true) return;

    // loop over all non-empty buckets
    uint size = this->Size();
    static_value_count_ = size;
    uint static_data_size = size*sizeof(ValueType) + toc_.Size()*sizeof(uint);
    data_.reset(new char[static_data_size]);
    uint curr_offset = 0;
    for (typename DenseMapType::Iterator i = toc_.Begin(), 
         e = toc_.End(); i != e; ++i) {
      i->second.offset = curr_offset;
      assert(i->second.id < dynamic_data_.size());
      ValueVector& v = dynamic_data_[i->second.id];
      assert(!v.empty());
      curr_offset = this->SetItemRO(curr_offset, &v.front(), 
                                    &v.front()+v.size());
    }

    readonly_ = true;
  }
  
  DenseMapType                         toc_;
  std::vector<ValueVector>             dynamic_data_;
  boost::scoped_array<char>            data_;
  uint                                 static_value_count_;
  bool                                 readonly_;
};

}

#endif

