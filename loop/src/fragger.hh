// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_FRAGGER_HH
#define PROMOD_LOOP_FRAGGER_HH

#include <promod3/loop/structure_db.hh>
#include <promod3/loop/backbone.hh>
#include <promod3/loop/torsion_sampler.hh>
#include <promod3/loop/psipred_prediction.hh>
#include <ost/io/binary_data_source.hh>
#include <ost/io/binary_data_sink.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <ost/seq/profile_handle.hh>
#include <ost/conop/amino_acids.hh>

#include <list>
#include <map>

namespace promod3 { namespace loop {


class Fragger;
class FraggerMap;
typedef boost::shared_ptr<Fragger> FraggerPtr;
typedef boost::shared_ptr<FraggerMap> FraggerMapPtr;
typedef std::vector<FraggerPtr> FraggerList;

enum FraggerScoreType {
  SeqSim,
  SeqID,
  SSAgree,
  TorsionProb,
  SequenceProfile,
  StructureProfile
};

// portable serialization (with fixed-width base-types)
inline void Serialize(ost::io::BinaryDataSource& ds, FraggerScoreType& st) {
  core::ConvertBaseType<uint8_t>(ds, st);
}
inline void Serialize(ost::io::BinaryDataSink& ds, FraggerScoreType& st) {
  core::ConvertBaseType<uint8_t>(ds, st);
}

class Fragger {

public:

  Fragger(const String& sequence);

  ~Fragger();

  void Fill(StructureDBPtr db, Real max_rmsd, uint num_fragments);

  void AddSeqIDParameters(Real w);

  void AddSeqSimParameters(Real w,
                           ost::seq::alg::SubstWeightMatrix& subst_matrix);

  void AddSSAgreeParameters(Real w, const PsipredPrediction& pp);

  void AddTorsionProbabilityParameters(Real w, TorsionSamplerPtr t_s,
                                       const String& before,
                                       const String& after);

  void AddTorsionProbabilityParameters(Real w, const TorsionSamplerList t_s,
                                       const String& before,
                                       const String& after);

  void AddSequenceProfileParameters(Real w, ost::seq::ProfileHandlePtr prof);

  void AddStructureProfileParameters(Real w, ost::seq::ProfileHandlePtr prof);

  size_t size() const { return fragments_.size(); }

  size_t parameter_size() const { return weights_.size(); }

  bool empty() const { return fragments_.empty(); }

  size_t fragment_size() const { return frag_sequence_.size(); }

  String fragment_seq() const { return frag_sequence_; }

  BackboneList& operator[](size_t index) { return fragments_[index]; }

  const BackboneList& operator[](size_t index) const { return fragments_[index]; }

  FragmentInfo GetFragmentInfo(uint index) const { return fragment_infos_[index]; }

  Real GetScore(uint index) const { return scores_[index]; } 

  Real GetScore(uint parameter_index, uint index) const { return single_scores_[index][parameter_index]; }

private:

  void LinearFill(StructureDBPtr, Real max_rmsd, uint num_fragments);

  void FillProfiles(StructureDBPtr db,
                    std::vector<std::vector<Real> >& profiles,
                    uint chain_idx) const;

  void FillTorsionSamplerIndices(const String& before, const String& after, 
                                 const TorsionSamplerList& t_sampler,
                                 std::vector<int>& indices);

  void GenerateSeqSimProfile(StructureDBPtr db, std::vector<Real>& profile,
                             uint index, const ost::seq::alg::SubstWeightMatrix& subst) const;

  void GenerateSeqIDProfile(StructureDBPtr db, std::vector<Real>& profile,
                            uint index) const;

  void GenerateSSAgreementProfile(StructureDBPtr db, std::vector<Real>& profile, uint index,
                                  const std::vector<int>& predicted_ss, 
                                  const std::vector<int>& confidence) const;

  void GenerateTorsionProbabilityProfile(StructureDBPtr db,
                              std::vector<Real>& profile, uint index,
                              const TorsionSamplerList& t_sampler,
                              const std::vector<int>& t_sampler_indices) const;

  void GenerateProfileProfile(StructureDBPtr db, std::vector<Real>& profile,
                              uint index,
                              const ost::PagedArray<AAFreq, 65536>& frequencies,
                              short** prof) const;


  //members holding stuff to calculate scores
  std::vector<ost::seq::alg::SubstWeightMatrix> subst_matrix_;
  std::vector<std::vector<int> > psipred_prediction_;
  std::vector<std::vector<int> > psipred_confidence_;
  std::vector<TorsionSamplerList> t_sampler_;
  std::vector<std::vector<int> > t_sampler_indices_;
  std::vector<short**> profiles_;

  //fragger members
  std::vector<BackboneList> fragments_;
  std::vector<FragmentInfo> fragment_infos_;
  std::vector<Real> scores_;
  std::vector<std::vector<Real> > single_scores_;
  std::vector<Real> weights_;
  std::vector<FraggerScoreType> score_types_;
  String frag_sequence_;

  // internal access to FraggerMap for storage
  friend class FraggerMap;
};

/// \brief Simple storable map of Fragger objects.
/// Idea is that one can use the map to cache fragger lists that have already
/// been generated. Serialization is meant to be temporary and is not portable.
class FraggerMap {
public:
  static FraggerMapPtr Load(const String& file_name, StructureDBPtr db);
  void Save(const String& filename);
  static FraggerMapPtr LoadBB(const String& file_name);
  void SaveBB(const String& filename);
  bool Contains(int id) { return map_.find(id) != map_.end(); }
  FraggerPtr& operator[](int id) { return map_[id]; }
private:
  static FraggerMapPtr Load_(const String& file_name, StructureDBPtr db);
  void Save_(const String& filename, bool with_bb);
  std::map<int, FraggerPtr> map_;
};

}}

#endif
