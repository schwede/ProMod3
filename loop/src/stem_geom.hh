// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_MELD_FRAGMENT_HH
#define PM3_MELD_FRAGMENT_HH

#include <string.h> /*for memcmp(); gcc 4.3*/

#include <boost/shared_ptr.hpp>
#include <boost/scoped_array.hpp>

#include <ost/base.hh>

namespace promod3 { namespace loop {

/// \brief Memory efficient storage of stem pairs for FragDB
/// \author Marco Biasini
struct StemPairGeom {
public:

  StemPairGeom(int aone, int atwo, int athree, int afour, int cad, int l):
    angle_one(static_cast<char>(aone)), 
    angle_two(static_cast<char>(atwo)), 
    angle_three(static_cast<char>(athree)),
    angle_four(static_cast<char>(afour)),
    ca_dist(static_cast<char>(cad)),
    length(static_cast<char>(l))
  { }  
  StemPairGeom():
   angle_one(0), angle_two(0), angle_three(0), angle_four(0), ca_dist(0), length(0)
  { }
  
  bool operator==(const StemPairGeom& rhs) const {
    return !memcmp(this, &rhs, sizeof(StemPairGeom));
  }
  bool operator!=(const StemPairGeom& rhs) const {
    return !this->operator==(rhs);
  }

  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    ds & angle_one;
    ds & angle_two;
    ds & angle_three;
    ds & angle_four;
    ds & ca_dist;
    ds & length;
  }

  char angle_one;
  char angle_two;
  char angle_three;
  char angle_four;
  char ca_dist;
  char length;
};

}}

#endif
