// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


/// \brief Defines functionality to deal with force fields.
///
/// Here, we define:
/// - an enxtension for ost::conop::AminoAcid which adds CYS2
/// - an indexing scheme for each amino acid in [0,N-1] (N = GetNumAtoms)
///   -> input is extended aa and index from enumerators in amino_acid_atoms.hh
///   -> note: only OXT indexing depends on whether it's terminal!
/// - for every aa: Charges, Masses, Sigmas, Epsilons (lists of len N)
///                 Connectivity (internal only)
/// - for every peptide bound aa-pair: Connectivity incl. peptide bond
/// - for disulfid bridges: Connectivity incl. disulfid bridge
/// - Connectivity = Bonds, Angles, Dihedrals, Impropers, LJPairs
///   -> each has indices and named parameters for topo.AddXX functions
///   -> indices for internal in [0, N-1]
///   -> indices for pairs: res1 in [0, N1-1], res2 in [N1, N1+N2-1]
///      (for disulfid bridge N1 = N2 = GetNumAtoms(FF_CYS2, false, false))
///
/// Static singleton access to a default lookup is provided
///
/// Example usage: see topology creation in AllAtomRelaxer

#ifndef PROMOD3_FORCEFIELD_LOOKUP_HH
#define PROMOD3_FORCEFIELD_LOOKUP_HH

#include <vector>
#include <ost/conop/amino_acids.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/core/portable_binary_serializer.hh>

namespace promod3 { namespace loop {

class ForcefieldLookup;
typedef boost::shared_ptr<ForcefieldLookup> ForcefieldLookupPtr;

// extension of ost::conop::AminoAcid (first 20 are identical!)
// -> extra: CYS2 for disulfid bridges and HISD for alt. HIS protanation
enum ForcefieldAminoAcid {
  FF_ALA = ost::conop::ALA,
  FF_ARG = ost::conop::ARG,
  FF_ASN = ost::conop::ASN,
  FF_ASP = ost::conop::ASP,
  FF_GLN = ost::conop::GLN,
  FF_GLU = ost::conop::GLU,
  FF_LYS = ost::conop::LYS,
  FF_SER = ost::conop::SER,
  FF_CYS = ost::conop::CYS,
  FF_MET = ost::conop::MET,
  FF_TRP = ost::conop::TRP,
  FF_TYR = ost::conop::TYR,
  FF_THR = ost::conop::THR,
  FF_VAL = ost::conop::VAL,
  FF_ILE = ost::conop::ILE,
  FF_LEU = ost::conop::LEU,
  FF_GLY = ost::conop::GLY,
  FF_PRO = ost::conop::PRO,
  FF_HISE = ost::conop::HIS,
  FF_PHE = ost::conop::PHE,
  FF_CYS2 = ost::conop::XXX,
  FF_HISD,
  FF_XXX
};

// define harmonic bond
struct ForcefieldBondInfo {
  uint index_one;
  uint index_two;
  Real bond_length;
  Real force_constant;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, index_one);
    core::ConvertBaseType<uint32_t>(ds, index_two);
    core::ConvertBaseType<float>(ds, bond_length);
    core::ConvertBaseType<float>(ds, force_constant);
  }
};

// define harmonic angle
struct ForcefieldHarmonicAngleInfo {
  uint index_one;
  uint index_two;
  uint index_three;
  Real angle;
  Real force_constant;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, index_one);
    core::ConvertBaseType<uint32_t>(ds, index_two);
    core::ConvertBaseType<uint32_t>(ds, index_three);
    core::ConvertBaseType<float>(ds, angle);
    core::ConvertBaseType<float>(ds, force_constant);
  }
};

// define UreyBradley angle
struct ForcefieldUreyBradleyAngleInfo {
  uint index_one;
  uint index_two;
  uint index_three;
  Real angle;
  Real angle_force_constant;
  Real bond_length;
  Real bond_force_constant;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, index_one);
    core::ConvertBaseType<uint32_t>(ds, index_two);
    core::ConvertBaseType<uint32_t>(ds, index_three);
    core::ConvertBaseType<float>(ds, angle);
    core::ConvertBaseType<float>(ds, angle_force_constant);
    core::ConvertBaseType<float>(ds, bond_length);
    core::ConvertBaseType<float>(ds, bond_force_constant);
  }
};

// define periodic dehidral or improper
struct ForcefieldPeriodicDihedralInfo {
  uint index_one;
  uint index_two;
  uint index_three;
  uint index_four;
  int multiplicity;
  Real phase;
  Real force_constant;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, index_one);
    core::ConvertBaseType<uint32_t>(ds, index_two);
    core::ConvertBaseType<uint32_t>(ds, index_three);
    core::ConvertBaseType<uint32_t>(ds, index_four);
    core::ConvertBaseType<int32_t>(ds, multiplicity);
    core::ConvertBaseType<float>(ds, phase);
    core::ConvertBaseType<float>(ds, force_constant);
  }
};

// define harmonic improper
struct ForcefieldHarmonicImproperInfo {
  uint index_one;
  uint index_two;
  uint index_three;
  uint index_four;
  Real angle;
  Real force_constant;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, index_one);
    core::ConvertBaseType<uint32_t>(ds, index_two);
    core::ConvertBaseType<uint32_t>(ds, index_three);
    core::ConvertBaseType<uint32_t>(ds, index_four);
    core::ConvertBaseType<float>(ds, angle);
    core::ConvertBaseType<float>(ds, force_constant);
  }
};

// define LJ pair
struct ForcefieldLJPairInfo {
  uint index_one;
  uint index_two;
  Real sigma;
  Real epsilon;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    core::ConvertBaseType<uint32_t>(ds, index_one);
    core::ConvertBaseType<uint32_t>(ds, index_two);
    core::ConvertBaseType<float>(ds, sigma);
    core::ConvertBaseType<float>(ds, epsilon);
  }
};

// combine all connectivity data
struct ForcefieldConnectivity {
  std::vector<ForcefieldBondInfo> harmonic_bonds;
  std::vector<ForcefieldHarmonicAngleInfo> harmonic_angles;
  std::vector<ForcefieldUreyBradleyAngleInfo> urey_bradley_angles;
  std::vector<ForcefieldPeriodicDihedralInfo> periodic_dihedrals;
  std::vector<ForcefieldPeriodicDihedralInfo> periodic_impropers;
  std::vector<ForcefieldHarmonicImproperInfo> harmonic_impropers;
  std::vector<ForcefieldLJPairInfo> lj_pairs;
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS> void Serialize(DS& ds) {
    ds & harmonic_bonds;
    ds & harmonic_angles;
    ds & urey_bradley_angles;
    ds & periodic_dihedrals;
    ds & periodic_impropers;
    ds & harmonic_impropers;
    ds & lj_pairs;
  }
};

// lookup
class ForcefieldLookup {
public:
  // singleton access to default instance
  static ForcefieldLookupPtr GetDefault() { return GetSingleton_(); }
  static void SetDefault(ForcefieldLookupPtr new_default) {
    GetSingleton_() = new_default;
  }

  // construct, load, save
  ForcefieldLookup();
  static ForcefieldLookupPtr Load(const String& filename);
  void Save(const String& filename);
  static ForcefieldLookupPtr LoadPortable(const String& filename);
  void SavePortable(const String& filename);

  // predefined force fields
  static ForcefieldLookupPtr LoadCHARMM();

  // indexing
  ost::conop::AminoAcid GetAA(ForcefieldAminoAcid aa) const {
    switch (aa) {
      case FF_CYS2: return ost::conop::CYS;
      case FF_HISD: return ost::conop::HIS;
      case FF_XXX:  return ost::conop::XXX;
      default:      return ost::conop::AminoAcid(aa);
    }
  }
  uint GetNumAtoms(ForcefieldAminoAcid aa, bool is_nter, bool is_cter) const {
    return num_atoms_[GetTerKey(is_nter, is_cter)][aa];
  }
  uint GetHeavyIndex(ForcefieldAminoAcid aa, uint atom_idx) const {
    return atom_idx;
  }
  uint GetHeavyIndex(ForcefieldAminoAcid aa, const String& aname) const {
    return GetHeavyIndex(aa, aa_lookup_.GetIndex(GetAA(aa), aname));
  }
  uint GetHydrogenIndex(ForcefieldAminoAcid aa, uint atom_idx) const {
    return hydrogen_atom_idx_[aa][atom_idx];
  }
  uint GetHydrogenIndex(ForcefieldAminoAcid aa, const String& aname) const {
    return GetHydrogenIndex(aa, aa_lookup_.GetHydrogenIndex(GetAA(aa), aname));
  }
  uint GetOXTIndex(ForcefieldAminoAcid aa, bool is_nter) const {
    return GetNumAtoms(aa, is_nter, true) - 1;
  }

  // read access
  Real GetFudgeLJ() const { return fudge_LJ_; }
  Real GetFudgeQQ() const { return fudge_QQ_; }

  const std::vector<Real>&
  GetCharges(ForcefieldAminoAcid aa, bool is_nter, bool is_cter) const {
    return charges_[GetTerKey(is_nter, is_cter)][aa];
  }
  const std::vector<Real>&
  GetMasses(ForcefieldAminoAcid aa, bool is_nter, bool is_cter) const {
    return masses_[GetTerKey(is_nter, is_cter)][aa];
  }
  const std::vector<Real>&
  GetSigmas(ForcefieldAminoAcid aa, bool is_nter, bool is_cter) const {
    return sigmas_[GetTerKey(is_nter, is_cter)][aa];
  }
  const std::vector<Real>&
  GetEpsilons(ForcefieldAminoAcid aa, bool is_nter, bool is_cter) const {
    return epsilons_[GetTerKey(is_nter, is_cter)][aa];
  }
  const ForcefieldConnectivity&
  GetInternalConnectivity(ForcefieldAminoAcid aa, bool is_nter,
                          bool is_cter) const {
    return connectivity_[GetTerKey(is_nter, is_cter)][aa];
  }
  const ForcefieldConnectivity&
  GetPeptideBoundConnectivity(ForcefieldAminoAcid aa_one,
                              ForcefieldAminoAcid aa_two,
                              bool is_nter, bool is_cter) const {
    return pb_connectivity_[GetTerKey(is_nter, is_cter)][aa_one][aa_two];
  }
  const ForcefieldConnectivity&
  GetDisulfidConnectivity() const { return disulfid_connectivity_; }

  // write access
  void SetFudgeLJ(Real f_lj) { fudge_LJ_ = f_lj; }
  void SetFudgeQQ(Real f_qq) { fudge_QQ_ = f_qq; }
  void SetCharges(ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                  const std::vector<Real>& charges) {
    charges_[GetTerKey(is_nter, is_cter)][aa] = charges;
  }
  void SetMasses(ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                 const std::vector<Real>& masses) {
    masses_[GetTerKey(is_nter, is_cter)][aa] = masses;
  }
  void SetSigmas(ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                 const std::vector<Real>& sigmas) {
    sigmas_[GetTerKey(is_nter, is_cter)][aa] = sigmas;
  }
  void SetEpsilons(ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                   const std::vector<Real>& epsilons) {
    epsilons_[GetTerKey(is_nter, is_cter)][aa] = epsilons;
  }
  void SetInternalConnectivity(ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                               const ForcefieldConnectivity& new_c) {
    connectivity_[GetTerKey(is_nter, is_cter)][aa] = new_c;
  }
  void SetPeptideBoundConnectivity(ForcefieldAminoAcid aa_one,
                                   ForcefieldAminoAcid aa_two,
                                   bool is_nter, bool is_cter,
                                   const ForcefieldConnectivity& new_c) {
    pb_connectivity_[GetTerKey(is_nter, is_cter)][aa_one][aa_two] = new_c;
  }
  void SetDisulfidConnectivity(const ForcefieldConnectivity& connectivity) {
    disulfid_connectivity_ = connectivity;
  }
  
  // portable serialization (element by element with fixed-width base-types)
  template <typename DS>
  void SerializeRealVectors(DS& ds, std::vector<Real>* v, uint size) {
    for (uint i = 0; i < size; ++i) {
      uint32_t N_v;
      if (!ds.IsSource()) N_v = v[i].size();
      ds & N_v;
      if (ds.IsSource()) v[i].resize(N_v);
      for (uint32_t j = 0; j < N_v; ++j) {
        core::ConvertBaseType<float>(ds, v[i][j]);
      }
    }
  }
  template <typename DS> void Serialize(DS& ds) {
    // we don't store num_atoms_ and hydrogen_atom_idx_ as they are constructed
    core::ConvertBaseType<float>(ds, fudge_LJ_);
    core::ConvertBaseType<float>(ds, fudge_QQ_);
    SerializeRealVectors(ds, &charges_[0][0], 4*(FF_XXX+1));
    SerializeRealVectors(ds, &masses_[0][0], 4*(FF_XXX+1));
    SerializeRealVectors(ds, &sigmas_[0][0], 4*(FF_XXX+1));
    SerializeRealVectors(ds, &epsilons_[0][0], 4*(FF_XXX+1));
    SerializeArray(ds, &connectivity_[0][0], 4*(FF_XXX+1));
    SerializeArray(ds, &pb_connectivity_[0][0][0], 4*(FF_XXX+1)*(FF_XXX+1));
    ds & disulfid_connectivity_;
  }

private:
  // internal singleton access
  static ForcefieldLookupPtr& GetSingleton_() {
    static ForcefieldLookupPtr instance(LoadCHARMM());
    return instance;
  }

  // map (is_nter, is_cter) to [0,3]
  uint GetTerKey(bool is_nter, bool is_cter) const {
    return is_nter*2 + is_cter;
  }

  // constructed lookup tables
  const AminoAcidLookup& aa_lookup_;
  uint num_atoms_[4][FF_XXX+1];
  std::vector<uint> hydrogen_atom_idx_[FF_XXX+1];

  // loaded/set lookup tables
  Real fudge_LJ_;
  Real fudge_QQ_;
  std::vector<Real> charges_[4][FF_XXX+1];
  std::vector<Real> masses_[4][FF_XXX+1];
  std::vector<Real> sigmas_[4][FF_XXX+1];
  std::vector<Real> epsilons_[4][FF_XXX+1];
  ForcefieldConnectivity connectivity_[4][FF_XXX+1];
  ForcefieldConnectivity pb_connectivity_[4][FF_XXX+1][FF_XXX+1];
  ForcefieldConnectivity disulfid_connectivity_;
};


}} // ns

#endif
