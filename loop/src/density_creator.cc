// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/density_creator.hh>
#include <promod3/core/message.hh>

namespace promod3{ namespace loop{

void FillDensityGaussianSpheres(const std::vector<geom::Vec3>& positions,
	                            const std::vector<Real>& molecular_weights,
	                            ost::img::MapHandle& map,
	                            Real resolution,
	                            bool clear_map, 
	                            bool high_resolution){

  if(positions.size() != molecular_weights.size()){
    throw promod3::Error("Positions and molecular sizes must be consistent in size!");
  }

  if (clear_map) {
    ost::img::MapHandle mm = 
      ost::img::CreateImage(ost::img::Extent(ost::img::Point(0, 0, 0),
                                             map.GetSize()));
    // swap newly created map into place
    mm.SetSpatialSampling(map.GetSpatialSampling());
    mm.SetAbsoluteOrigin(map.GetAbsoluteOrigin());
    map.Swap(mm);
  }

  Real k,C;
  if(high_resolution){
    k = (M_PI*M_PI)/(resolution * resolution);
    C = sqrt((M_PI * M_PI * M_PI) / (k * k * k));
  }
  else{
    k = (M_PI * M_PI)/((2.4 + 0.8 * resolution) * (2.4 + 0.8 * resolution));
    C = sqrt((M_PI * M_PI * M_PI) / (k * k * k));
  }

  Real sigma = std::sqrt(1.0 / (2.0 * k));
  Real three_sigma = 3.0 * sigma;
  Real three_sigma_squared = three_sigma * three_sigma;

  ost::img::Extent map_extent = map.GetExtent();
  geom::Vec3 map_start = map.IndexToCoord(map_extent.GetStart());
  geom::Vec3 sampling = map.GetSpatialSampling();

  uint x_extent = std::ceil(2.0 * three_sigma / sampling[0]) + 1;
  uint y_extent = std::ceil(2.0 * three_sigma / sampling[1]) + 1;
  uint z_extent = std::ceil(2.0 * three_sigma / sampling[2]) + 1;

  for(uint i = 0; i < positions.size(); ++i){

  	geom::Vec3 pos = positions[i];
  	Real atomic_weight = molecular_weights[i];

    //Get the positions in coord idx and cartesian positions
  	geom::Vec3 adjusted_pos = pos - map_start;
  	geom::Vec3 pixel_coord = map.CoordToIndex(pos);
  	ost::img::Point rounded_pixel_coord(round(pixel_coord[0]),
  		                                  round(pixel_coord[1]),
  		                                  round(pixel_coord[2]));

    ost::img::Extent pos_extent(ost::img::Size(x_extent,
    	                                         y_extent,
    	                                         z_extent),
                                rounded_pixel_coord);

    ost::img::Extent iteration_extent;
    try{
      iteration_extent = ost::img::Overlap(map_extent, pos_extent);
    }
    catch(std::exception& e){
      continue; //the stuff doesn't overlap
    }
    for(ost::img::ExtentIterator map_it(iteration_extent); 
    	!map_it.AtEnd(); ++map_it){

      ost::img::Point map_point(map_it);
      geom::Vec3 map_pos(map_point[0] * sampling[0],
      	                 map_point[1] * sampling[1],
      	                 map_point[2] * sampling[2]);

      Real dist_squared = geom::Length2(adjusted_pos - map_pos);
      if(dist_squared <= three_sigma_squared){
      	Real value = C * atomic_weight * std::exp(-k * dist_squared);
      	map.SetReal(map_point, map.GetReal(map_point) + value);
      }
    }
  }



 
}

}} //namespace
