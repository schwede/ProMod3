// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_DENSE_MAP_HH
#define PROMOD_LOOP_DENSE_MAP_HH

#include <fstream>

#include "dense_hash_table.hh"


namespace promod3 {

template <typename K, typename V, typename H>
struct TEMPLATE_EXPORT DenseMap
{
private:
  template <typename K1>
  struct KeyComp {
    bool operator()(const K1& k1, const K1& k2) const
    {
      return k1==k2;
    }
  };
  template <typename K1, typename V1>
  struct GetKeyFromPair {
    K1 operator()(const std::pair<K1, V1>& pair) const
    {
      return pair.first;
    }
  };  
  typedef DenseHashTable<std::pair<K, V>, K, H, GetKeyFromPair<K,V>, KeyComp<K>
                        > DenseHashTableType;
  DenseHashTableType ht_;
public:
  typedef typename DenseHashTableType::Iterator Iterator;
  typedef typename DenseHashTableType::ConstIterator ConstIterator;  
  typedef K KeyType;
  typedef V ValueType;
public:
  explicit DenseMap(uint expected_num_of_items=0):
    ht_(expected_num_of_items)
  { }
  std::pair<Iterator, bool> Insert(const KeyType& key, const ValueType& value)
  {
    return ht_.Insert(std::make_pair(key, value));
  }
  Iterator Begin()
  {
    return ht_.Begin();
  }
  ConstIterator Begin() const
  {
    return ht_.Begin();
  }
  void SetEmptyKey(const KeyType& key)
  {
    ht_.SetEmptyValue(std::make_pair(key, ValueType()));
  }
  
  Iterator End()
  {
    return ht_.End();
  }
  ConstIterator End() const
  {
    return ht_.End();
  }
  Iterator Find(const KeyType& key)
  {
    return ht_.Find(key);
  }
  ConstIterator Find(const KeyType& key) const
  {
    return ht_.Find(key);
  }  
  uint Size() const
  {
    return ht_.Size();
  }
  void Read(std::ifstream& in_stream)
  {
    ht_.Read(in_stream);
  }
  void Write(std::ofstream& out_stream)
  {
    ht_.Write(out_stream);
  }
  // portable serialization
  // (cleanly element by element with fixed-width base-types)
  template <typename DS>
  void Serialize(DS& ds) {
    ds & ht_;
  }
  // equality check (slow)
  bool operator==(const DenseMap& rhs) const {
    return ht_ == rhs.ht_;
  }
  bool operator!=(const DenseMap& rhs) const {
    return !this->operator==(rhs);
  }
};

}

#endif

