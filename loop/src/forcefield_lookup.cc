// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <fstream>
#include <promod3/loop/forcefield_lookup.hh>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/config.hh>

namespace promod3 { namespace loop {

namespace {

// raw read/write for POD-like type T in a vector
template <typename T>
void RawWriteVector(std::ofstream& out_stream, std::vector<T>& v) {
  uint N_v = v.size();
  out_stream.write(reinterpret_cast<char*>(&N_v), sizeof(uint));
  out_stream.write(reinterpret_cast<char*>(&v[0]), N_v * sizeof(T));
}
template <typename T>
void RawReadVector(std::ifstream& in_stream, std::vector<T>& v) {
  uint N_v;
  in_stream.read(reinterpret_cast<char*>(&N_v), sizeof(uint));
  v.resize(N_v);
  in_stream.read(reinterpret_cast<char*>(&v[0]), N_v * sizeof(T));
}

// raw read/write for fixed number of vectors of POD-type elements
template <typename T>
void RawWriteVectors(std::ofstream& out_stream, std::vector<T>* v, uint size) {
  for (uint i = 0; i < size; ++i) {
    RawWriteVector(out_stream, v[i]);
  }
}
template <typename T>
void RawReadVectors(std::ifstream& in_stream, std::vector<T>* v, uint size) {
  for (uint i = 0; i < size; ++i) {
    RawReadVector(in_stream, v[i]);
  }
}

// raw read/write for fixed number of ForcefieldConnectivity
void RawWriteVectors(std::ofstream& out_stream, ForcefieldConnectivity* v,
                     uint size) {
  for (uint i = 0; i < size; ++i) {
    RawWriteVector(out_stream, v[i].harmonic_bonds);
    RawWriteVector(out_stream, v[i].harmonic_angles);
    RawWriteVector(out_stream, v[i].urey_bradley_angles);
    RawWriteVector(out_stream, v[i].periodic_dihedrals);
    RawWriteVector(out_stream, v[i].periodic_impropers);
    RawWriteVector(out_stream, v[i].harmonic_impropers);
    RawWriteVector(out_stream, v[i].lj_pairs);
  }
}
void RawReadVectors(std::ifstream& in_stream, ForcefieldConnectivity* v,
                    uint size) {
  for (uint i = 0; i < size; ++i) {
    RawReadVector(in_stream, v[i].harmonic_bonds);
    RawReadVector(in_stream, v[i].harmonic_angles);
    RawReadVector(in_stream, v[i].urey_bradley_angles);
    RawReadVector(in_stream, v[i].periodic_dihedrals);
    RawReadVector(in_stream, v[i].periodic_impropers);
    RawReadVector(in_stream, v[i].harmonic_impropers);
    RawReadVector(in_stream, v[i].lj_pairs);
  }
}

} // anon ns

ForcefieldLookup::ForcefieldLookup(): aa_lookup_(AminoAcidLookup::GetInstance())
{
  // set num_atoms
  for (uint ter_idx = 0; ter_idx < 4; ++ter_idx) {
    bool is_nter = (ter_idx/2 == 1);
    bool is_cter = (ter_idx%2 == 1);
    for (uint aa_idx = 0; aa_idx < FF_XXX; ++aa_idx) {
      // hydrogens in aa_lookup_ contain all possible N hydrogens
      ost::conop::AminoAcid aa = GetAA(ForcefieldAminoAcid(aa_idx));
      uint num_atoms = aa_lookup_.GetNumAtoms(aa);
      num_atoms += aa_lookup_.GetNumHydrogens(aa);
      // remove AA dependent atoms (HIS-HD1/HE2, CYS2-HG)
      if (aa_idx == FF_HISE) num_atoms -= 1;
      else if (aa_idx == FF_HISD) num_atoms -= 1;
      else if (aa_idx == FF_CYS2) num_atoms -= 1;
      // check N hydrogen(s)
      if (is_nter) {
        // remove HN
        num_atoms -= (aa_idx == FF_PRO) ? 0 : 1;
      } else {
        // remove HX
        num_atoms -= (aa_idx == FF_PRO) ? 2 : 3;
      }
      // add OXT?
      if (is_cter) num_atoms += 1;
      num_atoms_[ter_idx][aa_idx] = num_atoms;
    }
    // UNK
    num_atoms_[ter_idx][FF_XXX] = 0;
  }

  // set hydrogen atom idx mapping
  for (uint aa_idx = 0; aa_idx < FF_XXX; ++aa_idx) {
    // setup hydrogen_atom_idx_ vector
    ost::conop::AminoAcid aa = GetAA(ForcefieldAminoAcid(aa_idx));
    uint num_hydrogens = aa_lookup_.GetNumHydrogens(aa);
    hydrogen_atom_idx_[aa_idx].resize(num_hydrogens);
    // keep track of new hydrogen index
    uint cur_idx = aa_lookup_.GetNumAtoms(aa);
    // skip N hydrogens for now
    int hn_idx[4] = {aa_lookup_.GetHNIndex(aa), aa_lookup_.GetH1Index(aa),
                     aa_lookup_.GetH2Index(aa), aa_lookup_.GetH3Index(aa)};
    for (uint h_idx = 0; h_idx < num_hydrogens; ++h_idx) {
      // check for N hydrogens and special stuff
      if (   int(h_idx) != hn_idx[0] && int(h_idx) != hn_idx[1]
          && int(h_idx) != hn_idx[2] && int(h_idx) != hn_idx[3]
          && !(aa_idx == FF_HISE && h_idx == HIS_HD1_INDEX)
          && !(aa_idx == FF_HISD && h_idx == HIS_HE2_INDEX)
          && !(aa_idx == FF_CYS2 && h_idx == CYS_HG_INDEX)) {
        hydrogen_atom_idx_[aa_idx][h_idx] = cur_idx;
        ++cur_idx;
      }
    }
    // set N hydrogens
    if (aa_idx == FF_PRO) {
      hydrogen_atom_idx_[aa_idx][hn_idx[1]] = cur_idx;
      hydrogen_atom_idx_[aa_idx][hn_idx[2]] = cur_idx+1;
    } else {
      hydrogen_atom_idx_[aa_idx][hn_idx[0]] = cur_idx;
      hydrogen_atom_idx_[aa_idx][hn_idx[1]] = cur_idx; // H1 replaces H
      hydrogen_atom_idx_[aa_idx][hn_idx[2]] = cur_idx+1;
      hydrogen_atom_idx_[aa_idx][hn_idx[3]] = cur_idx+2;
    }
  }
}

ForcefieldLookupPtr ForcefieldLookup::Load(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ForcefieldLookup::Load", 2);

  std::ifstream in_stream(filename.c_str(), std::ios::binary);
  if (!in_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // required types: uint, Real, int, ForcefieldXXInfo structs
  core::CheckTypeSize<uint>(in_stream);
  core::CheckTypeSize<Real>(in_stream);
  core::CheckTypeSize<int>(in_stream);
  core::CheckTypeSize<ForcefieldBondInfo>(in_stream);
  core::CheckTypeSize<ForcefieldHarmonicAngleInfo>(in_stream);
  core::CheckTypeSize<ForcefieldUreyBradleyAngleInfo>(in_stream);
  core::CheckTypeSize<ForcefieldPeriodicDihedralInfo>(in_stream);
  core::CheckTypeSize<ForcefieldHarmonicImproperInfo>(in_stream);
  core::CheckTypeSize<ForcefieldLJPairInfo>(in_stream);
  core::CheckBaseType<uint>(in_stream);
  core::CheckBaseType<Real>(in_stream);
  core::CheckBaseType<int>(in_stream);

  // data
  ForcefieldLookupPtr p(new ForcefieldLookup);
  in_stream.read(reinterpret_cast<char*>(&p->fudge_LJ_), sizeof(Real));
  in_stream.read(reinterpret_cast<char*>(&p->fudge_QQ_), sizeof(Real));
  RawReadVectors(in_stream, &p->charges_[0][0], 4*(FF_XXX+1));
  RawReadVectors(in_stream, &p->masses_[0][0], 4*(FF_XXX+1));
  RawReadVectors(in_stream, &p->sigmas_[0][0], 4*(FF_XXX+1));
  RawReadVectors(in_stream, &p->epsilons_[0][0], 4*(FF_XXX+1));
  RawReadVectors(in_stream, &p->connectivity_[0][0], 4*(FF_XXX+1));
  RawReadVectors(in_stream, &p->pb_connectivity_[0][0][0],
                 4*(FF_XXX+1)*(FF_XXX+1));
  RawReadVectors(in_stream, &p->disulfid_connectivity_, 1);
  return p;
}

void ForcefieldLookup::Save(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ForcefieldLookup::Save", 2);

  std::ofstream out_stream(filename.c_str(), std::ios::binary);
  if (!out_stream) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // see Load for required types...
  core::WriteTypeSize<uint>(out_stream);
  core::WriteTypeSize<Real>(out_stream);
  core::WriteTypeSize<int>(out_stream);
  core::WriteTypeSize<ForcefieldBondInfo>(out_stream);
  core::WriteTypeSize<ForcefieldHarmonicAngleInfo>(out_stream);
  core::WriteTypeSize<ForcefieldUreyBradleyAngleInfo>(out_stream);
  core::WriteTypeSize<ForcefieldPeriodicDihedralInfo>(out_stream);
  core::WriteTypeSize<ForcefieldHarmonicImproperInfo>(out_stream);
  core::WriteTypeSize<ForcefieldLJPairInfo>(out_stream);
  core::WriteBaseType<uint>(out_stream);
  core::WriteBaseType<Real>(out_stream);
  core::WriteBaseType<int>(out_stream);

  // data
  out_stream.write(reinterpret_cast<char*>(&fudge_LJ_), sizeof(Real));
  out_stream.write(reinterpret_cast<char*>(&fudge_QQ_), sizeof(Real));
  RawWriteVectors(out_stream, &charges_[0][0], 4*(FF_XXX+1));
  RawWriteVectors(out_stream, &masses_[0][0], 4*(FF_XXX+1));
  RawWriteVectors(out_stream, &sigmas_[0][0], 4*(FF_XXX+1));
  RawWriteVectors(out_stream, &epsilons_[0][0], 4*(FF_XXX+1));
  RawWriteVectors(out_stream, &connectivity_[0][0], 4*(FF_XXX+1));
  RawWriteVectors(out_stream, &pb_connectivity_[0][0][0],
                  4*(FF_XXX+1)*(FF_XXX+1));
  RawWriteVectors(out_stream, &disulfid_connectivity_, 1);
}

ForcefieldLookupPtr ForcefieldLookup::LoadPortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ForcefieldLookup::LoadPortable", 2);

  // open file
  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSource in_stream(in_stream_);

  // header for consistency checks
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream);
  if (version > 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  // here: only base-type-checks needed (we store Real as float!)
  core::CheckTypeSize<uint>(in_stream, true);
  core::CheckTypeSize<Real>(in_stream, true);
  core::CheckTypeSize<int>(in_stream, true);
  core::CheckBaseType<uint32_t>(in_stream);
  core::CheckBaseType<float>(in_stream);
  core::CheckBaseType<int32_t>(in_stream);

  // data
  ForcefieldLookupPtr p(new ForcefieldLookup);
  p->Serialize(in_stream);

  in_stream_.close();
  return p;
}

void ForcefieldLookup::SavePortable(const String& filename) {

  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ForcefieldLookup::SavePortable", 2);

  // open file
  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }
  core::PortableBinaryDataSink out_stream(out_stream_);

  // header for consistency checks
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  // required types: uint, Real, char
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);
  core::WriteTypeSize<int32_t>(out_stream);
  core::WriteBaseType<uint32_t>(out_stream);
  core::WriteBaseType<float>(out_stream);
  core::WriteBaseType<int32_t>(out_stream);

  // data
  this->Serialize(out_stream);

  out_stream_.close();
}

ForcefieldLookupPtr ForcefieldLookup::LoadCHARMM() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "ForcefieldLookup::LoadCHARMM", 2);
  String path = GetProMod3BinaryPath("loop_data", "ff_lookup_charmm.dat");
  ForcefieldLookupPtr p = ForcefieldLookup::Load(path);
  return p; 
}

}} //ns
