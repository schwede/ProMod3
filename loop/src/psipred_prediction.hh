// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_LOOP_PSIPRED_PREDICTION_HH
#define PROMOD_LOOP_PSIPRED_PREDICTION_HH

#include <vector>
#include <boost/shared_ptr.hpp>
#include <ost/string_ref.hh>
#include <promod3/core/message.hh>
#include <fstream>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>

namespace promod3 { namespace loop {

class PsipredPrediction;
typedef boost::shared_ptr<PsipredPrediction> PsipredPredictionPtr;
typedef std::vector<PsipredPredictionPtr> PsipredPredictionList;

class PsipredPrediction{

public:

  PsipredPrediction() {}

  PsipredPrediction(const std::vector<char>& prediction, 
                    const std::vector<int>& confidence);

  static PsipredPredictionPtr FromHHM(const String& filename);

  static PsipredPredictionPtr FromHoriz(const String& filename);

  PsipredPredictionPtr Extract(uint from, uint to) const;

  void Add(char prediction, int confidence);

  char GetPrediction(uint idx) const;

  int GetConfidence(uint idx) const;

  std::vector<char> GetPredictions() const { return prediction_; }

  std::vector<int> GetConfidences() const { return confidence_; }

  size_t size() const { return prediction_.size(); }

  bool operator==(const PsipredPrediction& other) {
    return (prediction_ == other.prediction_ &&
            confidence_ == other.confidence_);
  }

  bool operator!=(const PsipredPrediction& other) {
    return (!(*this == other));
  }

private:
  std::vector<char> prediction_;
  std::vector<int> confidence_;
};

}} //ns

#endif
