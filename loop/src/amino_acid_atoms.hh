// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


/// \brief Defines functionality to deal with all atoms of amino acids.
///
/// - ...AtomIndex are used to index atoms of each amino acid. The backbone atom
///   indices are shared and defined in BackboneAtomIndex.
/// - AminoAcidAtom defines unique IDs for each atom of each amino acid.
/// - AminoAcidLookup provides static functions to get olcs, indices and number
///   of atoms for each amino acid and each atom.
///
/// See extras/code_generation/amino_acid_atoms...py on how the data was
/// extracted from an ost compounds library and ost.mol.mm force fields.

#ifndef PROMOD3_AMINO_ACID_ATOMS_HH
#define PROMOD3_AMINO_ACID_ATOMS_HH

#include <ost/conop/amino_acids.hh>

namespace promod3 { namespace loop {

///////////////////////////////////////////////////////////////////////////////
// Enumerators for heavy atoms
enum BackboneAtomIndex {
  BB_N_INDEX, BB_CA_INDEX, BB_C_INDEX, BB_O_INDEX, BB_CB_INDEX
};

enum AlanineAtomIndex {
  ALA_CB_INDEX = 4, ALA_NUM_ATOMS
};

enum ArginineAtomIndex {
  ARG_CB_INDEX = 4, ARG_CG_INDEX, ARG_CD_INDEX, ARG_NE_INDEX, ARG_CZ_INDEX,
  ARG_NH1_INDEX, ARG_NH2_INDEX, ARG_NUM_ATOMS
};

enum AsparagineAtomIndex {
  ASN_CB_INDEX = 4, ASN_CG_INDEX, ASN_OD1_INDEX, ASN_ND2_INDEX, ASN_NUM_ATOMS
};

enum AsparticAcidAtomIndex {
  ASP_CB_INDEX = 4, ASP_CG_INDEX, ASP_OD1_INDEX, ASP_OD2_INDEX, ASP_NUM_ATOMS
};

enum GlutamineAtomIndex {
  GLN_CB_INDEX = 4, GLN_CG_INDEX, GLN_CD_INDEX, GLN_OE1_INDEX, GLN_NE2_INDEX,
  GLN_NUM_ATOMS
};

enum GlutamicAcidAtomIndex {
  GLU_CB_INDEX = 4, GLU_CG_INDEX, GLU_CD_INDEX, GLU_OE1_INDEX, GLU_OE2_INDEX,
  GLU_NUM_ATOMS
};

enum LysineAtomIndex {
  LYS_CB_INDEX = 4, LYS_CG_INDEX, LYS_CD_INDEX, LYS_CE_INDEX, LYS_NZ_INDEX,
  LYS_NUM_ATOMS
};

enum SerineAtomIndex {
  SER_CB_INDEX = 4, SER_OG_INDEX, SER_NUM_ATOMS
};

enum CysteineAtomIndex {
  CYS_CB_INDEX = 4, CYS_SG_INDEX, CYS_NUM_ATOMS
};

enum MethionineAtomIndex {
  MET_CB_INDEX = 4, MET_CG_INDEX, MET_SD_INDEX, MET_CE_INDEX, MET_NUM_ATOMS
};

enum TryptophanAtomIndex {
  TRP_CB_INDEX = 4, TRP_CG_INDEX, TRP_CD1_INDEX, TRP_CD2_INDEX, TRP_CE2_INDEX,
  TRP_NE1_INDEX, TRP_CE3_INDEX, TRP_CZ3_INDEX, TRP_CH2_INDEX, TRP_CZ2_INDEX,
  TRP_NUM_ATOMS
};

enum TyrosineAtomIndex {
  TYR_CB_INDEX = 4, TYR_CG_INDEX, TYR_CD1_INDEX, TYR_CD2_INDEX, TYR_CE1_INDEX,
  TYR_CE2_INDEX, TYR_CZ_INDEX, TYR_OH_INDEX, TYR_NUM_ATOMS
};

enum ThreonineAtomIndex {
  THR_CB_INDEX = 4, THR_OG1_INDEX, THR_CG2_INDEX, THR_NUM_ATOMS
};

enum ValineAtomIndex {
  VAL_CB_INDEX = 4, VAL_CG1_INDEX, VAL_CG2_INDEX, VAL_NUM_ATOMS
};

enum IsoleucineAtomIndex {
  ILE_CB_INDEX = 4, ILE_CG1_INDEX, ILE_CG2_INDEX, ILE_CD1_INDEX, ILE_NUM_ATOMS
};

enum LeucineAtomIndex {
  LEU_CB_INDEX = 4, LEU_CG_INDEX, LEU_CD1_INDEX, LEU_CD2_INDEX, LEU_NUM_ATOMS
};

enum GlycineAtomIndex {
  GLY_NUM_ATOMS = 4
};

enum ProlineAtomIndex {
  PRO_CB_INDEX = 4, PRO_CG_INDEX, PRO_CD_INDEX, PRO_NUM_ATOMS
};

enum HistidineAtomIndex {
  HIS_CB_INDEX = 4, HIS_CG_INDEX, HIS_ND1_INDEX, HIS_CD2_INDEX, HIS_CE1_INDEX,
  HIS_NE2_INDEX, HIS_NUM_ATOMS
};

enum PhenylalanineAtomIndex {
  PHE_CB_INDEX = 4, PHE_CG_INDEX, PHE_CD1_INDEX, PHE_CD2_INDEX, PHE_CE1_INDEX,
  PHE_CE2_INDEX, PHE_CZ_INDEX, PHE_NUM_ATOMS
};

// NOTE: order must follow indexing above! DO NOT CHANGE!
enum AminoAcidAtom {
  ALA_N, ALA_CA, ALA_C, ALA_O, ALA_CB, ARG_N, ARG_CA, ARG_C, ARG_O, ARG_CB,
  ARG_CG, ARG_CD, ARG_NE, ARG_CZ, ARG_NH1, ARG_NH2, ASN_N, ASN_CA, ASN_C,
  ASN_O, ASN_CB, ASN_CG, ASN_OD1, ASN_ND2, ASP_N, ASP_CA, ASP_C, ASP_O, ASP_CB,
  ASP_CG, ASP_OD1, ASP_OD2, GLN_N, GLN_CA, GLN_C, GLN_O, GLN_CB, GLN_CG,
  GLN_CD, GLN_OE1, GLN_NE2, GLU_N, GLU_CA, GLU_C, GLU_O, GLU_CB, GLU_CG,
  GLU_CD, GLU_OE1, GLU_OE2, LYS_N, LYS_CA, LYS_C, LYS_O, LYS_CB, LYS_CG,
  LYS_CD, LYS_CE, LYS_NZ, SER_N, SER_CA, SER_C, SER_O, SER_CB, SER_OG, CYS_N,
  CYS_CA, CYS_C, CYS_O, CYS_CB, CYS_SG, MET_N, MET_CA, MET_C, MET_O, MET_CB,
  MET_CG, MET_SD, MET_CE, TRP_N, TRP_CA, TRP_C, TRP_O, TRP_CB, TRP_CG, TRP_CD1,
  TRP_CD2, TRP_CE2, TRP_NE1, TRP_CE3, TRP_CZ3, TRP_CH2, TRP_CZ2, TYR_N, TYR_CA,
  TYR_C, TYR_O, TYR_CB, TYR_CG, TYR_CD1, TYR_CD2, TYR_CE1, TYR_CE2, TYR_CZ,
  TYR_OH, THR_N, THR_CA, THR_C, THR_O, THR_CB, THR_OG1, THR_CG2, VAL_N, VAL_CA,
  VAL_C, VAL_O, VAL_CB, VAL_CG1, VAL_CG2, ILE_N, ILE_CA, ILE_C, ILE_O, ILE_CB,
  ILE_CG1, ILE_CG2, ILE_CD1, LEU_N, LEU_CA, LEU_C, LEU_O, LEU_CB, LEU_CG,
  LEU_CD1, LEU_CD2, GLY_N, GLY_CA, GLY_C, GLY_O, PRO_N, PRO_CA, PRO_C, PRO_O,
  PRO_CB, PRO_CG, PRO_CD, HIS_N, HIS_CA, HIS_C, HIS_O, HIS_CB, HIS_CG, HIS_ND1,
  HIS_CD2, HIS_CE1, HIS_NE2, PHE_N, PHE_CA, PHE_C, PHE_O, PHE_CB, PHE_CG,
  PHE_CD1, PHE_CD2, PHE_CE1, PHE_CE2, PHE_CZ, XXX_NUM_ATOMS
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Enumerators for hydrogens
enum AlanineHydrogenIndex {
  ALA_H_INDEX, ALA_H1_INDEX, ALA_H2_INDEX, ALA_H3_INDEX, ALA_HA_INDEX,
  ALA_HB1_INDEX, ALA_HB2_INDEX, ALA_HB3_INDEX, ALA_NUM_HYDROGENS
};

enum ArginineHydrogenIndex {
  ARG_H_INDEX, ARG_H1_INDEX, ARG_H2_INDEX, ARG_H3_INDEX, ARG_HA_INDEX,
  ARG_HB2_INDEX, ARG_HB3_INDEX, ARG_HG2_INDEX, ARG_HG3_INDEX, ARG_HD2_INDEX,
  ARG_HD3_INDEX, ARG_HE_INDEX, ARG_HH11_INDEX, ARG_HH12_INDEX, ARG_HH21_INDEX,
  ARG_HH22_INDEX, ARG_NUM_HYDROGENS
};

enum AsparagineHydrogenIndex {
  ASN_H_INDEX, ASN_H1_INDEX, ASN_H2_INDEX, ASN_H3_INDEX, ASN_HA_INDEX,
  ASN_HB2_INDEX, ASN_HB3_INDEX, ASN_HD21_INDEX, ASN_HD22_INDEX,
  ASN_NUM_HYDROGENS
};

enum AsparticAcidHydrogenIndex {
  ASP_H_INDEX, ASP_H1_INDEX, ASP_H2_INDEX, ASP_H3_INDEX, ASP_HA_INDEX,
  ASP_HB2_INDEX, ASP_HB3_INDEX, ASP_NUM_HYDROGENS
};

enum GlutamineHydrogenIndex {
  GLN_H_INDEX, GLN_H1_INDEX, GLN_H2_INDEX, GLN_H3_INDEX, GLN_HA_INDEX,
  GLN_HB2_INDEX, GLN_HB3_INDEX, GLN_HG2_INDEX, GLN_HG3_INDEX, GLN_HE21_INDEX,
  GLN_HE22_INDEX, GLN_NUM_HYDROGENS
};

enum GlutamicAcidHydrogenIndex {
  GLU_H_INDEX, GLU_H1_INDEX, GLU_H2_INDEX, GLU_H3_INDEX, GLU_HA_INDEX,
  GLU_HB2_INDEX, GLU_HB3_INDEX, GLU_HG2_INDEX, GLU_HG3_INDEX, GLU_NUM_HYDROGENS
};

enum LysineHydrogenIndex {
  LYS_H_INDEX, LYS_H1_INDEX, LYS_H2_INDEX, LYS_H3_INDEX, LYS_HA_INDEX,
  LYS_HB2_INDEX, LYS_HB3_INDEX, LYS_HG2_INDEX, LYS_HG3_INDEX, LYS_HD2_INDEX,
  LYS_HD3_INDEX, LYS_HE2_INDEX, LYS_HE3_INDEX, LYS_HZ1_INDEX, LYS_HZ2_INDEX,
  LYS_HZ3_INDEX, LYS_NUM_HYDROGENS
};

enum SerineHydrogenIndex {
  SER_H_INDEX, SER_H1_INDEX, SER_H2_INDEX, SER_H3_INDEX, SER_HA_INDEX,
  SER_HB2_INDEX, SER_HB3_INDEX, SER_HG_INDEX, SER_NUM_HYDROGENS
};

enum CysteineHydrogenIndex {
  CYS_H_INDEX, CYS_H1_INDEX, CYS_H2_INDEX, CYS_H3_INDEX, CYS_HA_INDEX,
  CYS_HB2_INDEX, CYS_HB3_INDEX, CYS_HG_INDEX, CYS_NUM_HYDROGENS
};

enum MethionineHydrogenIndex {
  MET_H_INDEX, MET_H1_INDEX, MET_H2_INDEX, MET_H3_INDEX, MET_HA_INDEX,
  MET_HB2_INDEX, MET_HB3_INDEX, MET_HG2_INDEX, MET_HG3_INDEX, MET_HE1_INDEX,
  MET_HE2_INDEX, MET_HE3_INDEX, MET_NUM_HYDROGENS
};

enum TryptophanHydrogenIndex {
  TRP_H_INDEX, TRP_H1_INDEX, TRP_H2_INDEX, TRP_H3_INDEX, TRP_HA_INDEX,
  TRP_HB2_INDEX, TRP_HB3_INDEX, TRP_HD1_INDEX, TRP_HE1_INDEX, TRP_HE3_INDEX,
  TRP_HZ2_INDEX, TRP_HZ3_INDEX, TRP_HH2_INDEX, TRP_NUM_HYDROGENS
};

enum TyrosineHydrogenIndex {
  TYR_H_INDEX, TYR_H1_INDEX, TYR_H2_INDEX, TYR_H3_INDEX, TYR_HA_INDEX,
  TYR_HB2_INDEX, TYR_HB3_INDEX, TYR_HD1_INDEX, TYR_HD2_INDEX, TYR_HE1_INDEX,
  TYR_HE2_INDEX, TYR_HH_INDEX, TYR_NUM_HYDROGENS
};

enum ThreonineHydrogenIndex {
  THR_H_INDEX, THR_H1_INDEX, THR_H2_INDEX, THR_H3_INDEX, THR_HA_INDEX,
  THR_HB_INDEX, THR_HG1_INDEX, THR_HG21_INDEX, THR_HG22_INDEX, THR_HG23_INDEX,
  THR_NUM_HYDROGENS
};

enum ValineHydrogenIndex {
  VAL_H_INDEX, VAL_H1_INDEX, VAL_H2_INDEX, VAL_H3_INDEX, VAL_HA_INDEX,
  VAL_HB_INDEX, VAL_HG11_INDEX, VAL_HG12_INDEX, VAL_HG13_INDEX, VAL_HG21_INDEX,
  VAL_HG22_INDEX, VAL_HG23_INDEX, VAL_NUM_HYDROGENS
};

enum IsoleucineHydrogenIndex {
  ILE_H_INDEX, ILE_H1_INDEX, ILE_H2_INDEX, ILE_H3_INDEX, ILE_HA_INDEX,
  ILE_HB_INDEX, ILE_HG12_INDEX, ILE_HG13_INDEX, ILE_HG21_INDEX, ILE_HG22_INDEX,
  ILE_HG23_INDEX, ILE_HD11_INDEX, ILE_HD12_INDEX, ILE_HD13_INDEX,
  ILE_NUM_HYDROGENS
};

enum LeucineHydrogenIndex {
  LEU_H_INDEX, LEU_H1_INDEX, LEU_H2_INDEX, LEU_H3_INDEX, LEU_HA_INDEX,
  LEU_HB2_INDEX, LEU_HB3_INDEX, LEU_HG_INDEX, LEU_HD11_INDEX, LEU_HD12_INDEX,
  LEU_HD13_INDEX, LEU_HD21_INDEX, LEU_HD22_INDEX, LEU_HD23_INDEX,
  LEU_NUM_HYDROGENS
};

enum GlycineHydrogenIndex {
  GLY_H_INDEX, GLY_H1_INDEX, GLY_H2_INDEX, GLY_H3_INDEX, GLY_HA2_INDEX,
  GLY_HA3_INDEX, GLY_NUM_HYDROGENS
};

enum ProlineHydrogenIndex {
  PRO_H1_INDEX, PRO_H2_INDEX, PRO_HA_INDEX, PRO_HB2_INDEX, PRO_HB3_INDEX,
  PRO_HG2_INDEX, PRO_HG3_INDEX, PRO_HD2_INDEX, PRO_HD3_INDEX, PRO_NUM_HYDROGENS
};

enum HistidineHydrogenIndex {
  HIS_H_INDEX, HIS_H1_INDEX, HIS_H2_INDEX, HIS_H3_INDEX, HIS_HA_INDEX,
  HIS_HB2_INDEX, HIS_HB3_INDEX, HIS_HD1_INDEX, HIS_HD2_INDEX, HIS_HE1_INDEX,
  HIS_HE2_INDEX, HIS_NUM_HYDROGENS
};

enum PhenylalanineHydrogenIndex {
  PHE_H_INDEX, PHE_H1_INDEX, PHE_H2_INDEX, PHE_H3_INDEX, PHE_HA_INDEX,
  PHE_HB2_INDEX, PHE_HB3_INDEX, PHE_HD1_INDEX, PHE_HD2_INDEX, PHE_HE1_INDEX,
  PHE_HE2_INDEX, PHE_HZ_INDEX, PHE_NUM_HYDROGENS
};

enum AminoAcidHydrogen {
  ALA_H, ALA_H1, ALA_H2, ALA_H3, ALA_HA, ALA_HB1, ALA_HB2, ALA_HB3, ARG_H,
  ARG_H1, ARG_H2, ARG_H3, ARG_HA, ARG_HB2, ARG_HB3, ARG_HG2, ARG_HG3, ARG_HD2,
  ARG_HD3, ARG_HE, ARG_HH11, ARG_HH12, ARG_HH21, ARG_HH22, ASN_H, ASN_H1,
  ASN_H2, ASN_H3, ASN_HA, ASN_HB2, ASN_HB3, ASN_HD21, ASN_HD22, ASP_H, ASP_H1,
  ASP_H2, ASP_H3, ASP_HA, ASP_HB2, ASP_HB3, GLN_H, GLN_H1, GLN_H2, GLN_H3,
  GLN_HA, GLN_HB2, GLN_HB3, GLN_HG2, GLN_HG3, GLN_HE21, GLN_HE22, GLU_H,
  GLU_H1, GLU_H2, GLU_H3, GLU_HA, GLU_HB2, GLU_HB3, GLU_HG2, GLU_HG3, LYS_H,
  LYS_H1, LYS_H2, LYS_H3, LYS_HA, LYS_HB2, LYS_HB3, LYS_HG2, LYS_HG3, LYS_HD2,
  LYS_HD3, LYS_HE2, LYS_HE3, LYS_HZ1, LYS_HZ2, LYS_HZ3, SER_H, SER_H1, SER_H2,
  SER_H3, SER_HA, SER_HB2, SER_HB3, SER_HG, CYS_H, CYS_H1, CYS_H2, CYS_H3,
  CYS_HA, CYS_HB2, CYS_HB3, CYS_HG, MET_H, MET_H1, MET_H2, MET_H3, MET_HA,
  MET_HB2, MET_HB3, MET_HG2, MET_HG3, MET_HE1, MET_HE2, MET_HE3, TRP_H, TRP_H1,
  TRP_H2, TRP_H3, TRP_HA, TRP_HB2, TRP_HB3, TRP_HD1, TRP_HE1, TRP_HE3, TRP_HZ2,
  TRP_HZ3, TRP_HH2, TYR_H, TYR_H1, TYR_H2, TYR_H3, TYR_HA, TYR_HB2, TYR_HB3,
  TYR_HD1, TYR_HD2, TYR_HE1, TYR_HE2, TYR_HH, THR_H, THR_H1, THR_H2, THR_H3,
  THR_HA, THR_HB, THR_HG1, THR_HG21, THR_HG22, THR_HG23, VAL_H, VAL_H1, VAL_H2,
  VAL_H3, VAL_HA, VAL_HB, VAL_HG11, VAL_HG12, VAL_HG13, VAL_HG21, VAL_HG22,
  VAL_HG23, ILE_H, ILE_H1, ILE_H2, ILE_H3, ILE_HA, ILE_HB, ILE_HG12, ILE_HG13,
  ILE_HG21, ILE_HG22, ILE_HG23, ILE_HD11, ILE_HD12, ILE_HD13, LEU_H, LEU_H1,
  LEU_H2, LEU_H3, LEU_HA, LEU_HB2, LEU_HB3, LEU_HG, LEU_HD11, LEU_HD12,
  LEU_HD13, LEU_HD21, LEU_HD22, LEU_HD23, GLY_H, GLY_H1, GLY_H2, GLY_H3,
  GLY_HA2, GLY_HA3, PRO_H1, PRO_H2, PRO_HA, PRO_HB2, PRO_HB3, PRO_HG2, PRO_HG3,
  PRO_HD2, PRO_HD3, HIS_H, HIS_H1, HIS_H2, HIS_H3, HIS_HA, HIS_HB2, HIS_HB3,
  HIS_HD1, HIS_HD2, HIS_HE1, HIS_HE2, PHE_H, PHE_H1, PHE_H2, PHE_H3, PHE_HA,
  PHE_HB2, PHE_HB3, PHE_HD1, PHE_HD2, PHE_HE1, PHE_HE2, PHE_HZ,
  XXX_NUM_HYDROGENS
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// simple container for bonds (indices are from enums above)
struct BondInfo {
  BondInfo() { }
  BondInfo(uint a1, uint a2, uint o): atom_idx_one(a1)
                                    , atom_idx_two(a2)
                                    , order(o) { }
  uint atom_idx_one;
  uint atom_idx_two;
  uint order;
};

// Static functions to lookup stuff
class AminoAcidLookup {
public:
  // Singleton access to one constant instance
  // -> There is some C++ checks involved in here so GetInstance().GetXX() may
  //    not be inlined. For efficient use in loops, keep const ref to instance.
  static const AminoAcidLookup& GetInstance() {
    // note: this is guaranteed to be created on first use and destroyed at end
    static AminoAcidLookup instance;
    return instance;
  }

  // HEAVY ATOMS

  // data per amino acid
  char GetOLC(ost::conop::AminoAcid aa) const {
    return olc_[aa];
  }
  AminoAcidAtom GetFirstAAA(ost::conop::AminoAcid aa) const {
    return first_aaa_[aa];
  }
  AminoAcidAtom GetAAA(ost::conop::AminoAcid aa, uint atom_idx) const {
    return AminoAcidAtom(first_aaa_[aa] + atom_idx);
  }
  uint GetNumAtoms(ost::conop::AminoAcid aa) const {
    return num_atoms_[aa];
  }
  uint GetMaxNumAtoms() const {
    return max_num_atoms_;
  }
  const std::vector<BondInfo>& GetBonds(ost::conop::AminoAcid aa) const {
    return bonds_[aa];
  }
  // data per atom
  ost::conop::AminoAcid GetAA(AminoAcidAtom aaa) const {
    return aa_[aaa];
  }
  const String& GetAtomName(AminoAcidAtom aaa) const {
    return atom_name_[aaa];
  }
  const String& GetAtomNameCharmm(AminoAcidAtom aaa) const {
    // only exc. is ILE-CD (ff_name) <-> CD1 (pdb_name)
    if (aaa == ILE_CD1) return ff_name_ile_cd1_;
    else                return atom_name_[aaa];
  }
  const String& GetAtomNameAmber(AminoAcidAtom aaa) const {
    // only exc. is ILE-CD (ff_name) <-> CD1 (pdb_name)
    if (aaa == ILE_CD1) return ff_name_ile_cd1_;
    else                return atom_name_[aaa];
  }
  const String& GetElement(AminoAcidAtom aaa) const {
    return element_[aaa];
  }
  // these ones are slower and will throw an Error if atom name not valid
  uint GetIndex(ost::conop::AminoAcid aa, const String& aname) const;
  AminoAcidAtom GetAAA(ost::conop::AminoAcid aa, const String& aname) const {
    return GetAAA(aa, GetIndex(aa, aname));
  }
  // as GetIndex but return -1 instead of exception when not found
  int GetIndexNoExc(ost::conop::AminoAcid aa, const String& aname) const;

  // HYDROGEN ATOMS

  // data per amino acid 
  AminoAcidHydrogen GetFirstAAH(ost::conop::AminoAcid aa) const {
    return first_aah_[aa];
  }
  AminoAcidHydrogen GetAAH(ost::conop::AminoAcid aa, uint atom_idx) const {
    return AminoAcidHydrogen(first_aah_[aa] + atom_idx);
  }
  uint GetNumHydrogens(ost::conop::AminoAcid aa) const {
    return num_hydrogens_[aa];
  }
  uint GetMaxNumHydrogens() const {
    return max_num_hydrogens_;
  }
  // get indices for H attached to N (HN = def, H1,H2,H3 = N-terminal)
  // -> returns -1 if invalid request (i.e. HN/H3 for PRO or XXX)
  int GetHNIndex(ost::conop::AminoAcid aa) const { return hn_index_[aa]; }
  int GetH1Index(ost::conop::AminoAcid aa) const { return h1_index_[aa]; }
  int GetH2Index(ost::conop::AminoAcid aa) const { return h2_index_[aa]; }
  int GetH3Index(ost::conop::AminoAcid aa) const { return h3_index_[aa]; }
  // data per atom
  ost::conop::AminoAcid GetAA(AminoAcidHydrogen aah) const {
    return hydrogen_aa_[aah];
  }
  const String& GetAtomName(AminoAcidHydrogen aah) const {
    return hydrogen_name_[aah];
  }
  uint GetAnchorAtomIndex(AminoAcidHydrogen aah) const {
    return anchor_atom_idx_[aah];
  }
  const String& GetAtomNameCharmm(AminoAcidHydrogen aah) const {
    return hydrogen_name_charmm_[aah];
  }
  const String& GetAtomNameAmber(AminoAcidHydrogen aah) const {
    return hydrogen_name_amber_[aah];
  }
  // these ones are slower and will throw an Error if atom name not valid
  uint GetHydrogenIndex(ost::conop::AminoAcid aa, const String& aname) const;
  AminoAcidHydrogen GetAAH(ost::conop::AminoAcid aa, const String& aname) const {
    return GetAAH(aa, GetHydrogenIndex(aa, aname));
  }
  // as GetHydrogenIndex but return -1 instead of exception when not found
  int GetHydrogenIndexNoExc(ost::conop::AminoAcid aa, const String& aname) const;

private:
  // construction only inside here
  AminoAcidLookup();
  // lookup tables
  char olc_[ost::conop::XXX+1];
  // heavy atoms
  AminoAcidAtom first_aaa_[ost::conop::XXX+1];
  uint num_atoms_[ost::conop::XXX+1];
  uint max_num_atoms_;
  std::vector<BondInfo> bonds_[ost::conop::XXX+1];
  ost::conop::AminoAcid aa_[XXX_NUM_ATOMS+1];
  String atom_name_[XXX_NUM_ATOMS+1];
  String element_[XXX_NUM_ATOMS+1];
  // for AMBER and CHARMM force fields only name diff is ILE_CD1
  String ff_name_ile_cd1_;
  // hydrogens
  AminoAcidHydrogen first_aah_[ost::conop::XXX+1];
  uint num_hydrogens_[ost::conop::XXX+1];
  uint max_num_hydrogens_;
  int hn_index_[ost::conop::XXX+1];
  int h1_index_[ost::conop::XXX+1];
  int h2_index_[ost::conop::XXX+1];
  int h3_index_[ost::conop::XXX+1];
  ost::conop::AminoAcid hydrogen_aa_[XXX_NUM_HYDROGENS+1];
  String hydrogen_name_[XXX_NUM_HYDROGENS+1];
  // atom_idx is heavy atom index as in bonds_
  uint anchor_atom_idx_[XXX_NUM_HYDROGENS+1];
  // names for AMBER and CHARMM force fields
  String hydrogen_name_amber_[XXX_NUM_HYDROGENS+1];
  String hydrogen_name_charmm_[XXX_NUM_HYDROGENS+1];
};
///////////////////////////////////////////////////////////////////////////////

}} // ns

#endif
