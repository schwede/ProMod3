// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/all_atom_positions.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/geom_base.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/mol/bond_handle.hh>
#include <ost/conop/heuristic.hh>

namespace promod3 { namespace loop {

namespace {

// default chem. type and class for amino acids
const ost::mol::ChemClass aa_chem_class(ost::mol::ChemClass::L_PEPTIDE_LINKING);
const ost::mol::ChemType aa_chem_type(ost::mol::ChemType::AMINOACIDS);

// set residue props
void SetResidueProps(ost::mol::ResidueHandle& res, char olc) {
  res.SetOneLetterCode(olc);
  res.SetChemClass(aa_chem_class);
  res.SetChemType(aa_chem_type);
  res.SetIsProtein(true);
}

} // anon ns

///////////////////////////////////////////////////////////////////////////////

AllAtomPositions::AllAtomPositions(const String& sequence)
                                  : aa_lookup_(AminoAcidLookup::GetInstance()) {
  Initialize(sequence);
}

AllAtomPositions::AllAtomPositions(const ost::mol::ResidueHandleList& res_list)
                                  : aa_lookup_(AminoAcidLookup::GetInstance()) {
  String sequence = "";
  for (uint i = 0; i < res_list.size(); ++i) {
    sequence += res_list[i].GetOneLetterCode();
  }
  Initialize(sequence);
  for (uint i = 0; i < GetNumResidues(); ++i) SetResidue(i, res_list[i]);
}

AllAtomPositions::AllAtomPositions(const String& sequence,
                                   const ost::mol::ResidueHandleList& res_list)
                                  : aa_lookup_(AminoAcidLookup::GetInstance()) {
  // check consistency
  if (res_list.size() != sequence.size()) {
    throw promod3::Error("Must have same number of residues as aa letters "
                         "when initializing AllAtomPositions!");
  }
  Initialize(sequence);
  for (uint i = 0; i < GetNumResidues(); ++i) SetResidue(i, res_list[i]);
}

AllAtomPositions::AllAtomPositions(const BackboneList& bb_list)
                                  : aa_lookup_(AminoAcidLookup::GetInstance()) {
  Initialize(bb_list.GetSequence());
  for (uint i = 0; i < bb_list.size(); ++i) {
    SetPos(i, BB_N_INDEX, bb_list.GetN(i));
    SetPos(i, BB_CA_INDEX, bb_list.GetCA(i));
    SetPos(i, BB_C_INDEX, bb_list.GetC(i));
    SetPos(i, BB_O_INDEX, bb_list.GetO(i));
    if (GetAA(i) != ost::conop::GLY) SetPos(i, BB_CB_INDEX, bb_list.GetCB(i));
  }
}

AllAtomPositions::AllAtomPositions(const AllAtomPositions& other)
                                  : pos_(other.pos_)
                                  , aaa_(other.aaa_)
                                  , is_set_(other.is_set_)
                                  , first_idx_(other.first_idx_)
                                  , aa_(other.aa_)
                                  , num_atoms_(other.num_atoms_)
                                  , num_residues_(other.num_residues_)
                                  , aa_lookup_(AminoAcidLookup::GetInstance())
                                  { }

AllAtomPositions& AllAtomPositions::operator=(const AllAtomPositions& other) {
  // self-assignment check
  if (this != &other) {
    pos_ = other.pos_;
    aaa_ = other.aaa_;
    is_set_ = other.is_set_;
    first_idx_ = other.first_idx_;
    aa_ = other.aa_;
    num_atoms_ = other.num_atoms_;
    num_residues_ = other.num_residues_;
  }
  return *this;
}

void AllAtomPositions::Initialize(const String& sequence) {
  // setup per residue data
  num_atoms_ = 0;
  num_residues_ = sequence.size();
  first_idx_.resize(num_residues_+1);
  aa_.resize(num_residues_);
  for (uint i = 0; i < num_residues_; ++i) {
    first_idx_[i] = num_atoms_;
    aa_[i] = ost::conop::OneLetterCodeToAminoAcid(sequence[i]);
    if (aa_[i] == ost::conop::XXX) {
      std::stringstream ss;
      ss << "Invalid one letter code '" << sequence[i] << "' encountered in "
         << "seqres for AllAtomPositions! Can only handle standard amino "
         << "acids!";
      throw promod3::Error(ss.str());
    }
    num_atoms_ += aa_lookup_.GetNumAtoms(aa_[i]);
  }
  first_idx_[num_residues_] = num_atoms_;
  // setup per atom data
  pos_.resize(num_atoms_);
  aaa_.resize(num_atoms_);
  is_set_.assign(num_atoms_, false);
  for (uint res_idx = 0; res_idx < num_residues_; ++res_idx) {
    const uint first_idx = GetFirstIndex(res_idx);
    const uint last_idx = GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      aaa_[idx] = aa_lookup_.GetAAA(aa_[res_idx], idx - first_idx);
    }
  }
}

void AllAtomPositions::SetResidue(uint res_idx,
                                  const ost::mol::ResidueHandle& res) {
  // check consistency
  CheckResidueIndex(res_idx);
  // go over all atoms
  const uint first_idx = GetFirstIndex(res_idx);
  const uint last_idx = GetLastIndex(res_idx);
  for (uint idx = first_idx; idx <= last_idx; ++idx) {
    const String& aname = aa_lookup_.GetAtomName(GetAAA(idx));
    ost::mol::AtomHandle atom = res.FindAtom(aname);
    if (atom.IsValid()) {
      SetPos(idx, atom.GetPos());
    } else {
      ClearPos(idx);
    }
  }
}

void AllAtomPositions::SetResidue(uint res_idx, const AllAtomPositions& other,
                                  uint other_res_idx) {
  // check consistency
  CheckResidueIndex(res_idx);
  other.CheckResidueIndex(other_res_idx);
  if (GetAA(res_idx) != other.GetAA(other_res_idx)) {
    throw promod3::Error("Inconsistent residues in SetResidue!");
  }
  // copy positions
  const uint first_idx = GetFirstIndex(res_idx);
  const uint last_idx = GetLastIndex(res_idx);
  const uint other_first_idx = other.GetFirstIndex(other_res_idx);
  for (uint idx = first_idx; idx <= last_idx; ++idx) {
    const uint other_idx = other_first_idx + (idx - first_idx);
    if (other.IsSet(other_idx)) {
      SetPos(idx, other.GetPos(other_idx));
    } else {
      ClearPos(idx);
    }
  }
}

String AllAtomPositions::GetSequence() const {
  String return_string(GetNumResidues(), 'X');
  for (uint i = 0; i < GetNumResidues(); ++i) {
    return_string[i] = aa_lookup_.GetOLC(GetAA(i));
  }
  return return_string;
}

AllAtomPositionsPtr AllAtomPositions::Extract(uint from, uint to) const {
  // check range
  if (from >= to) {
    throw promod3::Error("Invalid range: 'from' must be smaller than 'to'!");
  }
  CheckResidueIndex(to-1);
  // get sequence
  String sequence(to-from, 'X');
  for (uint i = from; i < to; ++i) {
    sequence[i-from] = aa_lookup_.GetOLC(GetAA(i));
  }
  // setup new container
  AllAtomPositionsPtr atoms(new AllAtomPositions(sequence));
  for (uint i = from; i < to; ++i) {
    atoms->SetResidue(i-from, *this, i);
  }
  return atoms;
}

AllAtomPositionsPtr
AllAtomPositions::Extract(const std::vector<uint>& res_indices) const {
  // check indices
  const uint N = res_indices.size();
  for (uint i = 0; i < N; ++i) CheckResidueIndex(res_indices[i]);
  // get sequence
  String sequence(N, 'X');
  for (uint i = 0; i < N; ++i) {
    sequence[i] = aa_lookup_.GetOLC(GetAA(res_indices[i]));
  }
  // setup new container
  AllAtomPositionsPtr atoms(new AllAtomPositions(sequence));
  for (uint i = 0; i < N; ++i) {
    atoms->SetResidue(i, *this, res_indices[i]);
  }
  return atoms;
}

BackboneList AllAtomPositions::ExtractBackbone(uint from, uint to) const {
  // check range
  if (from >= to) {
    throw promod3::Error("Invalid range: 'from' must be smaller than 'to'!");
  }
  CheckResidueIndex(to-1);
  // get backbone
  BackboneList bb_list;
  bb_list.resize(to-from);
  for (uint i = from; i < to; ++i) {
    // check bb
    if (!IsSet(i, BB_N_INDEX) || !IsSet(i, BB_CA_INDEX)
        || !IsSet(i, BB_C_INDEX) || !IsSet(i, BB_O_INDEX)) {
      throw promod3::Error("Could not find all required backbone atoms in "
                           "given residue!");
    }
    // set all data
    if (GetAA(i) != ost::conop::GLY && IsSet(i, BB_CB_INDEX)) {
      bb_list.Set(i - from, GetPos(i, BB_N_INDEX), GetPos(i, BB_CA_INDEX),
                  GetPos(i, BB_CB_INDEX), GetPos(i, BB_C_INDEX),
                  GetPos(i, BB_O_INDEX), aa_lookup_.GetOLC(GetAA(i)));
    } else {
      bb_list.Set(i - from, GetPos(i, BB_N_INDEX), GetPos(i, BB_CA_INDEX),
                  GetPos(i, BB_C_INDEX), GetPos(i, BB_O_INDEX),
                  aa_lookup_.GetOLC(GetAA(i)));
    }
  }
  return bb_list;
}

ost::mol::EntityHandle AllAtomPositions::ToEntity() const {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPositions::ToEntity", 2);
  // setup
  ost::mol::EntityHandle ent = ost::mol::CreateEntity();
  ost::mol::XCSEditor edi = ent.EditXCS(ost::mol::BUFFERED_EDIT);
  ost::mol::ChainHandle chain = edi.InsertChain("A");
  // add all residues
  for (uint res_idx = 0; res_idx < GetNumResidues(); ++res_idx) {
    // add residue
    String rname = ost::conop::AminoAcidToResidueName(GetAA(res_idx));
    ost::mol::ResidueHandle res = edi.AppendResidue(chain, rname);
    // add set atoms
    const uint first_idx = GetFirstIndex(res_idx);
    const uint last_idx = GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      if (IsSet(idx)) {
        const String& aname = aa_lookup_.GetAtomName(GetAAA(idx));
        const String& element = aa_lookup_.GetElement(GetAAA(idx));
        edi.InsertAtom(res, aname, GetPos(idx), element);
      }
    }
    // set residue data
    SetResidueProps(res, aa_lookup_.GetOLC(GetAA(res_idx)));
  }
  // set connections
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(ent);
  return ent;
}

void AllAtomPositions::InsertInto(uint res_idx, ost::mol::ChainHandle& chain,
                                  const ost::mol::ResNum& resnum) const {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomPositions::InsertInto", 2);
  // checks
  if (!chain.IsValid()) {
    throw promod3::Error("Must provide valid chain in InsertInto!");
  }
  CheckResidueIndex(res_idx);
  if (!IsSet(res_idx, BB_N_INDEX) || !IsSet(res_idx, BB_CA_INDEX)
      || !IsSet(res_idx, BB_C_INDEX)) {
    throw promod3::Error("Need valid backbone in InsertInto!");
  }
  
  // setup
  ost::mol::XCSEditor edi = chain.GetEntity().EditXCS(ost::mol::BUFFERED_EDIT);
  ost::conop::AminoAcid aa = GetAA(res_idx);

  // setup residue
  bool residue_added = false;
  ost::mol::ResidueHandle res = chain.FindResidue(resnum);
  String rname = ost::conop::AminoAcidToResidueName(aa);
  if (!res.IsValid()) {
    // add new residue
    res = edi.AppendResidue(chain, rname, resnum);
    residue_added = true;
  } else if (res.GetName() != rname) {
    // rename if needed
    edi.RenameResidue(res, rname);
  }
  SetResidueProps(res, aa_lookup_.GetOLC(aa));

  // set new atom positions
  bool backbone_added = false;
  const uint first_idx = GetFirstIndex(res_idx);
  const uint last_idx = GetLastIndex(res_idx);
  // keep atom handles of set atoms (rest invalid)
  std::vector<ost::mol::AtomHandle> atoms(last_idx-first_idx+1);
  for (uint idx = first_idx; idx <= last_idx; ++idx) {
    // only consider set pos.
    if (IsSet(idx)) {
      const uint atom_idx = idx - first_idx;
      const String& aname = aa_lookup_.GetAtomName(GetAAA(idx));
      const String& element = aa_lookup_.GetElement(GetAAA(idx));
      ost::mol::AtomHandle atom = res.FindAtom(aname);
      if (!atom.IsValid()) {
        // add new atom
        atom = edi.InsertAtom(res, aname, GetPos(idx), element);
        backbone_added = (backbone_added || atom_idx <= BB_C_INDEX);
      } else {
        // just update
        edi.SetAtomPos(atom, GetPos(idx));
        atom.SetElement(element);
        // wipe out connectivity in sidechain
        if (atom_idx > BB_CB_INDEX) edi.DeleteBonds(atom.GetBondList());
      }
      atoms[atom_idx] = atom;
    }
  }
  // wipe out any unknown/unset atoms
  ost::mol::AtomHandleList atom_list = res.GetAtomList();
  for (uint al_idx = 0; al_idx < atom_list.size(); ++al_idx) {
    const String& aname = atom_list[al_idx].GetName();
    const int atom_idx = aa_lookup_.GetIndexNoExc(aa, aname);
    if (atom_idx < 0 || !atoms[atom_idx].IsValid()) {
      edi.DeleteAtom(atom_list[al_idx]);
    }
  }
  // set internal connectivity
  const std::vector<BondInfo>& bonds = aa_lookup_.GetBonds(aa);
  for (uint i = 0; i < bonds.size(); ++i) {
    const ost::mol::AtomHandle& atom_one = atoms[bonds[i].atom_idx_one];
    const ost::mol::AtomHandle& atom_two = atoms[bonds[i].atom_idx_two];
    if (atom_one.IsValid() && atom_two.IsValid()) {
      edi.Connect(atom_one, atom_two, bonds[i].order);
    }
  }

  // check if we have a new backbone
  if (backbone_added) {
    // add peptide linking
    ost::mol::ResidueHandle res_prev = chain.FindResidue(resnum-1);
    ost::mol::ResidueHandle res_next = chain.FindResidue(resnum+1);
    if (res_prev.IsValid()) {
      ost::mol::AtomHandle prev_c = res_prev.FindAtom("C");
      if (prev_c.IsValid()) edi.Connect(prev_c, atoms[BB_N_INDEX]);
    }
    if (res_next.IsValid()) {
      ost::mol::AtomHandle next_n = res_next.FindAtom("N");
      if (next_n.IsValid()) edi.Connect(atoms[BB_C_INDEX], next_n);
    }
    // add backbone torsions
    ost::conop::AssignBackboneTorsions(res_prev, res, res_next);
    // also for res_prev and res_next (if valid)
    if (res_prev.IsValid()) {
      ost::conop::AssignBackboneTorsions(res_prev.GetPrev(), res_prev, res);
    }
    if (res_next.IsValid()) {
      ost::conop::AssignBackboneTorsions(res, res_next, res_next.GetNext());
    }
  }

  // reorder residues if new residue added
  if (residue_added) edi.ReorderResidues(chain);
}

}} //ns
