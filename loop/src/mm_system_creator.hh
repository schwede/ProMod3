// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_MM_SYSTEM_CREATOR_HH
#define PROMOD3_MM_SYSTEM_CREATOR_HH

#include <vector>

#include <ost/mol/mm/topology.hh>
#include <ost/mol/mm/simulation.hh>

#include <promod3/loop/all_atom_positions.hh>
#include <promod3/loop/forcefield_lookup.hh>

namespace promod3 { namespace loop {

class MmSystemCreator;
typedef boost::shared_ptr<MmSystemCreator> MmSystemCreatorPtr;

// ---------
// DEV NOTES
// ---------
// internal naming conventions:
// - i_res = internal res. indexing (0..res_indices.size()-1)
// - res_idx = res_indices[i_res] -> into all_pos
// sizes:
// - ff_aa_.size() == res_indices.size()
// - first_idx_.size() == res_indices.size() + 1
// - positions_.size() == top->GetNumParticles()

/// \brief Create system for ost:mol:mm to work on loops.
class MmSystemCreator {
public:
  // typedefs for convenience
  typedef std::vector< std::pair<uint,uint> > DisulfidBridgeVector;

  // setup creator
  MmSystemCreator(ForcefieldLookupPtr ff_lookup,
                  bool fix_surrounding_hydrogens = true,
                  bool kill_electrostatics = false, Real nonbonded_cutoff = 8,
                  bool inaccurate_pot_energy = false);

  // helper to find disulfid bridges for CYS in all_pos[res_indices[i]]
  DisulfidBridgeVector
  GetDisulfidBridges(const AllAtomPositions& all_pos,
                     const std::vector<uint>& res_indices) const;

  // setup system (overwrites old one completely!)
  // single loop
  void SetupSystem(const AllAtomPositions& all_pos,
                   const std::vector<uint>& res_indices, uint loop_length,
                   const std::vector<bool>& is_n_ter,
                   const std::vector<bool>& is_c_ter,
                   const DisulfidBridgeVector& disulfid_bridges);
  // multi loop
  void SetupSystem(const AllAtomPositions& all_pos,
                   const std::vector<uint>& res_indices,
                   const std::vector<uint>& loop_start_indices,
                   const std::vector<uint>& loop_lengths,
                   const std::vector<bool>& is_n_ter,
                   const std::vector<bool>& is_c_ter,
                   const DisulfidBridgeVector& disulfid_bridges);

  // overwrite positions (input must be compatible with last SetupSystem call)
  void UpdatePositions(const AllAtomPositions& all_pos,
                       const std::vector<uint>& res_indices);

  // extract loop heavy atom positions from simulation object into AAP
  // v1: loop_pos[i] used as storage for loop
  void ExtractLoopPositions(AllAtomPositions& loop_pos);
  // v2: out_pos[res_indices[i]] used as storage for loop
  void ExtractLoopPositions(AllAtomPositions& out_pos,
                            const std::vector<uint>& res_indices);
  
  // getters for settings
  ForcefieldLookupPtr GetFfLookup() const { return ff_lookup_; }
  bool IsFixSurroundingHydrogens() const { return fix_surrounding_hydrogens_; }
  bool IsKillElectrostatics() const { return kill_electrostatics_; }
  Real GetNonbondedCutoff() const { return nonbonded_cutoff_; }
  bool HasInaccuratePotEnergy() const { return inaccurate_pot_energy_; }
  bool GetCpuPlatformSupport() const { return cpu_platform_support_; }

  // use to override CPU platform support setting (def.: CPU used if possible)
  void SetCpuPlatformSupport(bool cpu_platform_support);

  // getters for data
  uint GetNumResidues() const { return ff_aa_.size(); }
  uint GetNumLoopResidues() const { return num_loop_residues_; }
  const std::vector<uint>& GetLoopStartIndices() const {
    return loop_start_indices_;
  }
  const std::vector<uint>& GetLoopLengths() const { return loop_lengths_; }
  const std::vector<ForcefieldAminoAcid>& GetForcefieldAminoAcids() const {
    return ff_aa_;
  }
  const std::vector<uint>& GetIndexing() const { return first_idx_; }
  // this is the internal positions vector which is set in SetupSystem,
  // UpdatePositions and ExtractLoopPositions
  // -> to get current sim. pos. use GetSimulation()->GetPositions()
  const geom::Vec3List& GetInternalPositions() const { return positions_; }
  ost::mol::mm::TopologyPtr GetTopology() const { return top_; }
  ost::mol::mm::SimulationPtr GetSimulation() const { return simulation_; }

protected:
  // helpers for setup (require the ones above to be executed first)
  void SetupNextIRes_(const std::vector<uint>& res_indices);
  void SetupFfAa_(const AllAtomPositions& all_pos,
                  const std::vector<uint>& res_indices,
                  const DisulfidBridgeVector& disulfid_bridges);
  void SetupFirstIdx_();
  void SetupTopology_(const DisulfidBridgeVector& disulfid_bridges);
  void SetupPositions_(const AllAtomPositions& all_pos,
                       const std::vector<uint>& res_indices);
  void SetupSimulation_();

  // dealing with loops
  enum IndexLocation_ {
    // to be or-ed
    OUT_OF_LOOP = 0,
    ON_N_STEM = 1,
    ON_C_STEM = 2,
    IN_LOOP = 4
  };
  uint GetIndexLocation_(uint i_res) {
    // check all loops (no loops of length 0 possible!)
    const uint num_loops = loop_start_indices_.size();
    for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
      const uint tst = i_res - loop_start_indices_[i_loop];
      if (tst < loop_lengths_[i_loop]) {
        uint res = IN_LOOP;
        if (tst == 0) res |= ON_N_STEM;
        if (tst == loop_lengths_[i_loop]-1) res |= ON_C_STEM;
        return res;
      }
    }
    return OUT_OF_LOOP;
  }

  // settings
  ForcefieldLookupPtr ff_lookup_;
  bool fix_surrounding_hydrogens_;
  bool kill_electrostatics_;
  Real nonbonded_cutoff_;
  bool inaccurate_pot_energy_;
  bool cpu_platform_support_;

  // input data
  uint num_loop_residues_;                 // sum of loop lengths
  std::vector<uint> loop_start_indices_;   // index in [0, num_residues-1]
  std::vector<uint> loop_lengths_;         // len = len(loop_start_indices_)
  std::vector<bool> is_n_ter_;             // len = num_residues
  std::vector<bool> is_c_ter_;             // len = num_residues
  // internal res. idx. of next residue or -1 if no next res. in system
  std::vector<int> next_i_res_;            // len = num_residues
  // FFAA type for each res.
  std::vector<ForcefieldAminoAcid> ff_aa_; // len = num_residues
  // indexing into positions_ (first_idx_.back() == num_atoms)
  std::vector<uint> first_idx_;            // len = num_residues+1
  // for each res. i: positions[first_idx[i]..first_idx[i+1]-1]
  // -> internal ordering as defined by ff_lookup
  geom::Vec3List positions_;               // len = num_atoms
  ost::mol::mm::TopologyPtr top_;          // top->GetNumParticles() = num_atoms
  ost::mol::mm::SimulationPtr simulation_;

  // internal use only (to avoid passing around stuff)
  const AminoAcidLookup& aa_lookup_;
};

}} // ns

#endif
