// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/mm_system_creator.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>
#include <ost/mol/builder.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/seq/sequence_op.hh>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

ost::mol::mm::SimulationPtr GetSimRef(const AllAtomPositions& all_pos,
                                      bool kill_electrostatics = false,
                                      Real nonbonded_cutoff = 8) {
  // link to CHARMM
  ost::mol::mm::SettingsPtr settings(new ost::mol::mm::Settings);
  const String ff_path = "data/CHARMM27.dat";
  settings->forcefield = ost::mol::mm::Forcefield::Load(ff_path);
  // custom settings consistent to MmSystemCreator
  settings->add_cmaps = false;
  settings->kill_electrostatics = kill_electrostatics;
  if (nonbonded_cutoff < 0) {
    settings->nonbonded_method = ost::mol::mm::NoCutoff;
  } else {
    settings->nonbonded_method = ost::mol::mm::CutoffNonPeriodic;
    settings->nonbonded_cutoff = nonbonded_cutoff;
  }
  settings->platform = ost::mol::mm::Reference;
  // make topology (starting from AAP)
  ost::mol::EntityHandle mm_ent = all_pos.ToEntity();
  ost::mol::mm::TopologyPtr top;
  top = ost::mol::mm::TopologyCreator::Create(mm_ent, settings);
  ost::mol::mm::SimulationPtr sim;
  sim.reset(new ost::mol::mm::Simulation(top, mm_ent, settings));
  return sim;
}

MmSystemCreatorPtr GetSystem(const AllAtomPositions& all_pos,
                             bool kill_electrostatics = false,
                             Real nonbonded_cutoff = 8) {
  // generate MM sim
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::LoadCHARMM();
  MmSystemCreatorPtr mm_sim;
  mm_sim.reset(new MmSystemCreator(ff_lookup, true, kill_electrostatics,
                                   nonbonded_cutoff));
  // force use of reference platform for reproducible results
  mm_sim->SetCpuPlatformSupport(false);
  BOOST_CHECK(!mm_sim->GetCpuPlatformSupport());
  // get data for mm_sim
  const uint num_residues = all_pos.GetNumResidues();
  std::vector<uint> res_indices(num_residues);
  for (uint i = 0; i < num_residues; ++i) res_indices[i] = i;
  std::vector<bool> is_n_ter(num_residues, false);
  is_n_ter[0] = true;
  std::vector<bool> is_c_ter(num_residues, false);
  is_c_ter[num_residues-1] = true;
  std::vector< std::pair<uint,uint> > disulfid_bridges
   = mm_sim->GetDisulfidBridges(all_pos, res_indices);
  // build system and return it
  mm_sim->SetupSystem(all_pos, res_indices, num_residues, is_n_ter, is_c_ter,
                      disulfid_bridges);
  return mm_sim;
}

uint GetNameIndex(const String& atom_name, const ForcefieldLookupPtr ff_lookup,
                  const AminoAcidLookup& aa_lookup, ForcefieldAminoAcid ff_aa) {
  // OXT?
  if (atom_name == "OXT" || atom_name == "OT2") {
    // assume no two-ter here
    return ff_lookup->GetOXTIndex(ff_aa, false);
  } else if (atom_name == "OT1") {
    return ff_lookup->GetHeavyIndex(ff_aa, BB_O_INDEX);
  } else {
    // look for CHARMM names in aa lookups
    const ost::conop::AminoAcid aa = ff_lookup->GetAA(ff_aa);
    // heavy first
    const uint num_atoms = aa_lookup.GetNumAtoms(aa);
    for (uint idx = 0; idx < num_atoms; ++idx) {
      AminoAcidAtom aaa = aa_lookup.GetAAA(aa, idx);
      if (atom_name == aa_lookup.GetAtomNameCharmm(aaa)) {
        return ff_lookup->GetHeavyIndex(ff_aa, idx);
      }
    }
    // then hydrogen
    const uint num_hydrogens = aa_lookup.GetNumHydrogens(aa);
    for (uint idx = 0; idx < num_hydrogens; ++idx) {
      AminoAcidHydrogen aah = aa_lookup.GetAAH(aa, idx);
      if (atom_name == aa_lookup.GetAtomNameCharmm(aah)) {
        return ff_lookup->GetHydrogenIndex(ff_aa, idx);
      }
    }
  }
  // should never fail!
  throw promod3::Error("NAME NOT FOUND");
}

std::vector<uint> GetMapping(ost::mol::EntityHandle mm_ent,
                             MmSystemCreatorPtr mm_sim) {
  // setup
  const uint num_atoms = mm_ent.GetAtomCount();
  std::vector<uint> mapping(num_atoms);
  std::vector<bool> is_set(num_atoms, false);
  const ForcefieldLookupPtr ff_lookup = mm_sim->GetFfLookup();
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  
  // need ff_aa and first_idx from mm_sim
  const std::vector<ForcefieldAminoAcid>& ff_aa
   = mm_sim->GetForcefieldAminoAcids();
  const std::vector<uint>& first_idx = mm_sim->GetIndexing();
  BOOST_CHECK_EQUAL(ff_aa.size(), uint(mm_ent.GetResidueCount()));
  BOOST_CHECK_EQUAL(first_idx.back(), num_atoms);
  
  // loop over all atoms
  ost::mol::AtomHandleList atoms = mm_ent.GetAtomList();
  const int first_res_num = atoms[0].GetResidue().GetNumber().GetNum();
  for (uint i = 0; i < num_atoms; ++i) {
    // get res. index and related info
    const int res_num = atoms[i].GetResidue().GetNumber().GetNum();
    const uint res_idx = res_num - first_res_num;
    // find index for atom name
    const uint idx = GetNameIndex(atoms[i].GetName(), ff_lookup, aa_lookup,
                                  ff_aa[res_idx]);
    BOOST_CHECK(first_idx[res_idx] + idx < num_atoms);
    BOOST_CHECK(!is_set[first_idx[res_idx] + idx]);
    mapping[i] = first_idx[res_idx] + idx;
    is_set[first_idx[res_idx] + idx] = true;
  }
  // check if all set
  for (uint i = 0; i < num_atoms; ++i) {
    BOOST_CHECK(is_set[i]);
  }
  return mapping;
}

Real RunSimulation(ost::mol::mm::SimulationPtr sim,
                   int steps = 50, Real stop_criterion = 0.01) {
  sim->ApplySD(stop_criterion, steps);
  return sim->GetPotentialEnergy();
}

void CompareFullSimulation(const AllAtomPositions& all_pos,
                           bool kill_es, Real cutoff) {
  // check full simulation setup in OST
  ost::mol::mm::SimulationPtr sim_ref = GetSimRef(all_pos, kill_es, cutoff);
  Real e_ref = sim_ref->GetEnergy();
  Real e_ref_pot = sim_ref->GetPotentialEnergy();
  Real e_ref_kin = sim_ref->GetKineticEnergy();
  // ..and with MmSystemCreator
  MmSystemCreatorPtr mm_sim = GetSystem(all_pos, kill_es, cutoff);
  ost::mol::mm::SimulationPtr sim = mm_sim->GetSimulation();
  BOOST_CHECK_CLOSE(sim->GetEnergy(), e_ref, 1e-4);
  BOOST_CHECK_CLOSE(sim->GetPotentialEnergy(), e_ref_pot, 1e-4);
  BOOST_CHECK_CLOSE(sim->GetKineticEnergy(), e_ref_kin, 1e-4);

  // compare topology
  ost::mol::mm::TopologyPtr top = sim->GetTopology();
  ost::mol::mm::TopologyPtr top_ref = sim_ref->GetTopology();
  const uint num_atoms = top_ref->GetNumParticles();
  BOOST_CHECK_EQUAL(uint(sim_ref->GetEntity().GetAtomCount()), num_atoms);
  BOOST_CHECK_EQUAL(top->GetNumParticles(), num_atoms);
  BOOST_CHECK_EQUAL(uint(sim->GetEntity().GetAtomCount()), num_atoms);
  BOOST_CHECK_EQUAL(top->GetNumCMaps(),
                    top_ref->GetNumCMaps());
  BOOST_CHECK_EQUAL(top->GetNumDistanceConstraints(),
                    top_ref->GetNumDistanceConstraints());
  BOOST_CHECK_EQUAL(top->GetNumExclusions(),
                    top_ref->GetNumExclusions());
  BOOST_CHECK_EQUAL(top->GetNumFGMDHBondAcceptors(),
                    top_ref->GetNumFGMDHBondAcceptors());
  BOOST_CHECK_EQUAL(top->GetNumFGMDHBondDonors(),
                    top_ref->GetNumFGMDHBondDonors());
  BOOST_CHECK_EQUAL(top->GetNumHarmonicAngles(),
                    top_ref->GetNumHarmonicAngles());
  BOOST_CHECK_EQUAL(top->GetNumHarmonicBonds(),
                    top_ref->GetNumHarmonicBonds());
  BOOST_CHECK_EQUAL(top->GetNumHarmonicDistanceRestraints(),
                    top_ref->GetNumHarmonicDistanceRestraints());
  BOOST_CHECK_EQUAL(top->GetNumHarmonicImpropers(),
                    top_ref->GetNumHarmonicImpropers());
  BOOST_CHECK_EQUAL(top->GetNumHarmonicPositionRestraints(),
                    top_ref->GetNumHarmonicPositionRestraints());
  BOOST_CHECK_EQUAL(top->GetNumLJPairs(),
                    top_ref->GetNumLJPairs());
  BOOST_CHECK_EQUAL(top->GetNumPeriodicDihedrals(),
                    top_ref->GetNumPeriodicDihedrals());
  BOOST_CHECK_EQUAL(top->GetNumPeriodicImpropers(),
                    top_ref->GetNumPeriodicImpropers());
  BOOST_CHECK_EQUAL(top->GetNumUreyBradleyAngles(),
                    top_ref->GetNumUreyBradleyAngles());

  // let's get into details...
  geom::Vec3List pos = sim->GetPositions();
  geom::Vec3List pos_ref = sim_ref->GetPositions();
  geom::Vec3List vel = sim->GetVelocities();
  geom::Vec3List vel_ref = sim_ref->GetVelocities();
  geom::Vec3List forces = sim->GetForces();
  geom::Vec3List forces_ref = sim_ref->GetForces();
  BOOST_CHECK_EQUAL(pos.size(), num_atoms);
  BOOST_CHECK_EQUAL(pos_ref.size(), num_atoms);
  BOOST_CHECK_EQUAL(vel.size(), num_atoms);
  BOOST_CHECK_EQUAL(vel_ref.size(), num_atoms);
  BOOST_CHECK_EQUAL(forces.size(), num_atoms);
  BOOST_CHECK_EQUAL(forces_ref.size(), num_atoms);
  // compare entries
  std::vector<uint> mapping = GetMapping(sim_ref->GetEntity(), mm_sim);
  ost::mol::AtomHandleList atoms = sim_ref->GetEntity().GetAtomList();
  for (uint i = 0; i < pos_ref.size(); ++i) {
    BOOST_CHECK(geom::Distance(pos[mapping[i]], pos_ref[i]) < 1e-5);
    BOOST_CHECK(geom::Distance(vel[mapping[i]], vel_ref[i]) < 1e-5);
    BOOST_CHECK(geom::Distance(forces[mapping[i]], forces_ref[i]) < 1e-3);
  }

  // check mm_sim features (NOTE: DIFFs non-zero but that's expected...)
  sim->SetPositions(sim->GetPositions());
  BOOST_CHECK_CLOSE(sim->GetEnergy(), e_ref, 1e-3);
  BOOST_CHECK_CLOSE(sim->GetPotentialEnergy(), e_ref_pot, 1e-3);
  BOOST_CHECK_CLOSE(sim->GetKineticEnergy(), e_ref_kin, 1e-3);
  const uint num_residues = all_pos.GetNumResidues();
  std::vector<uint> res_indices(num_residues);
  for (uint i = 0; i < num_residues; ++i) res_indices[i] = i;
  mm_sim->UpdatePositions(all_pos, res_indices);
  sim = mm_sim->GetSimulation();
  BOOST_CHECK_CLOSE(sim->GetEnergy(), e_ref, 1e-3);
  BOOST_CHECK_CLOSE(sim->GetPotentialEnergy(), e_ref_pot, 1e-3);
  BOOST_CHECK_CLOSE(sim->GetKineticEnergy(), e_ref_kin, 1e-3);

  // check simulation runs
  Real e_ref_run = RunSimulation(sim_ref);
  Real e_run = RunSimulation(sim);
  BOOST_CHECK_CLOSE(e_run, e_ref_run, 1e-3);
  BOOST_CHECK(e_ref_pot > e_ref_run);
}

void SetupLoop(const AllAtomPositions& all_pos, uint start_res_idx,
               uint end_res_idx, MmSystemCreator& mm_sim) {
  // setup
  const uint num_residues = all_pos.GetNumResidues();
  const uint loop_length = end_res_idx - start_res_idx + 1;
  // get indices
  std::vector<uint> res_indices;
  for (uint i = 0; i < loop_length; ++i) {
    res_indices.push_back(start_res_idx + i);
  }
  for (uint i = 0; i < num_residues; ++i) {
    if (i < start_res_idx || i > end_res_idx) {
      res_indices.push_back(i);
    }
  }
  BOOST_CHECK_EQUAL(res_indices.size(), num_residues);
  // get terminal info
  std::vector<bool> is_n_ter(num_residues, false);
  std::vector<bool> is_c_ter(num_residues, false);
  for (uint i = 0; i < num_residues; ++i) {
    if (res_indices[i] == 0) is_n_ter[i] = true;
    if (res_indices[i] == num_residues-1) is_c_ter[i] = true;
  }
  // get disulfid bridges
  std::vector< std::pair<uint,uint> > disulfid_bridges;
  disulfid_bridges = mm_sim.GetDisulfidBridges(all_pos, res_indices);
  // build system
  mm_sim.SetupSystem(all_pos, res_indices, loop_length, is_n_ter, is_c_ter,
                     disulfid_bridges);
}

// same as above but passing loop_start_indices and loop_lengths
void SetupLoopShift(const AllAtomPositions& all_pos, uint start_res_idx,
                    uint end_res_idx, MmSystemCreator& mm_sim) {
  // setup
  const uint num_residues = all_pos.GetNumResidues();
  std::vector<uint> loop_start_indices(1, start_res_idx);
  std::vector<uint> loop_lengths(1, end_res_idx - start_res_idx + 1);
  // get indices
  std::vector<uint> res_indices;
  for (uint i = 0; i < num_residues; ++i) res_indices.push_back(i);
  // get terminal info
  std::vector<bool> is_n_ter(num_residues, false);
  std::vector<bool> is_c_ter(num_residues, false);
  is_n_ter[0] = true;
  is_c_ter[num_residues-1] = true;
  // get disulfid bridges
  std::vector< std::pair<uint,uint> > disulfid_bridges;
  disulfid_bridges = mm_sim.GetDisulfidBridges(all_pos, res_indices);
  // build system
  mm_sim.SetupSystem(all_pos, res_indices, loop_start_indices, loop_lengths,
                     is_n_ter, is_c_ter, disulfid_bridges);
}

void CheckLoops(const AllAtomPositions& all_pos, bool fix_surrounding_hydrogens,
                bool kill_es, Real cutoff) {
  // generate MM sim
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::GetDefault();
  MmSystemCreator mm_sim(ff_lookup, fix_surrounding_hydrogens, kill_es, cutoff);
  mm_sim.SetCpuPlatformSupport(false);
  SetupLoop(all_pos, 0, all_pos.GetNumResidues()-1, mm_sim);
  Real pot_ref = mm_sim.GetSimulation()->GetPotentialEnergy();
  SetupLoop(all_pos, 0, all_pos.GetNumResidues()-1, mm_sim);
  BOOST_CHECK_EQUAL(mm_sim.GetSimulation()->GetPotentialEnergy(), pot_ref);
  BOOST_CHECK_EQUAL(mm_sim.GetNumLoopResidues(), all_pos.GetNumResidues());
  // try some loops
  const uint loop_length = 5;
  SetupLoop(all_pos, 0, loop_length - 1, mm_sim);
  Real pot_ref_loop = mm_sim.GetSimulation()->GetPotentialEnergy();
  // we expect all loops to be similar (+- 5%)
  // -> expect lower than pot_ref and same order of magnitude
  BOOST_CHECK(pot_ref_loop < pot_ref);
  if (fix_surrounding_hydrogens && kill_es) {
    BOOST_CHECK_CLOSE(pot_ref_loop, pot_ref, 2000);
  } else {
    BOOST_CHECK_CLOSE(pot_ref_loop, pot_ref, 500);
  }
  for (uint i = 0; i <= all_pos.GetNumResidues() - loop_length; i += 10) {
    SetupLoop(all_pos, i, i + loop_length - 1, mm_sim);
    BOOST_CHECK_EQUAL(mm_sim.GetNumLoopResidues(), loop_length);
    if (fix_surrounding_hydrogens && kill_es) {
      BOOST_CHECK_CLOSE(mm_sim.GetSimulation()->GetPotentialEnergy(),
                        pot_ref_loop, 40);
    } else {
      BOOST_CHECK_CLOSE(mm_sim.GetSimulation()->GetPotentialEnergy(),
                        pot_ref_loop, 5);
    }
  }
}

void CheckLoopMinimize(const AllAtomPositions& all_pos,
                       bool fix_surrounding_hydrogens,
                       bool kill_es, Real cutoff,
                       uint start_res_idx, uint end_res_idx) {
  // generate MM sim with and w/o accurate pot. e.
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::GetDefault();
  MmSystemCreator mm_sim(ff_lookup, fix_surrounding_hydrogens, kill_es, cutoff);
  MmSystemCreator mm_sim_inaccurate(ff_lookup, fix_surrounding_hydrogens,
                                    kill_es, cutoff, true);
  mm_sim.SetCpuPlatformSupport(false);
  mm_sim_inaccurate.SetCpuPlatformSupport(false);
  // get loop res_indices
  std::vector<uint> res_indices;
  for (uint i = start_res_idx; i <= end_res_idx; ++i) res_indices.push_back(i);
  // run it for a bit
  SetupLoop(all_pos, start_res_idx, end_res_idx, mm_sim);
  ost::mol::mm::SimulationPtr sim = mm_sim.GetSimulation();
  Real pot_ref_loop = sim->GetPotentialEnergy();
  sim->ApplySD(0.01, 25);
  BOOST_CHECK(sim->GetPotentialEnergy() < pot_ref_loop);
  // check result
  AllAtomPositionsPtr new_pos = all_pos.Copy();
  mm_sim.ExtractLoopPositions(*new_pos, res_indices);
  const uint num_residues = all_pos.GetNumResidues();
  for (uint res_idx = 0; res_idx < num_residues; ++res_idx) {
    // expect stems out-of-loop and parts of stems to be fixed and rest moving
    const bool in_loop = (res_idx >= start_res_idx && res_idx <= end_res_idx);
    const bool is_nstem = (start_res_idx > 0 && res_idx == start_res_idx);
    const bool is_cstem = (   end_res_idx < num_residues - 1
                           && res_idx == end_res_idx);
    // check atoms
    const uint first_idx = all_pos.GetFirstIndex(res_idx);
    const uint last_idx = all_pos.GetLastIndex(res_idx);
    for (uint i = first_idx; i <= last_idx; ++i) {
      const uint atom_idx = i - first_idx;
      const Real dist = geom::Distance(all_pos.GetPos(i), new_pos->GetPos(i));
      if (!in_loop) {
        BOOST_CHECK_EQUAL(dist, Real(0));
      } else if (is_nstem && (   atom_idx == BB_N_INDEX
                              || atom_idx == BB_CA_INDEX
                              || atom_idx == BB_CB_INDEX)) {
        BOOST_CHECK(dist < Real(1e-6));
      } else if (is_cstem && (   atom_idx == BB_CA_INDEX
                              || atom_idx == BB_CB_INDEX
                              || atom_idx == BB_C_INDEX
                              || atom_idx == BB_O_INDEX)) {
        BOOST_CHECK(dist < Real(1e-6));
      } else {
        BOOST_CHECK(dist > 1e-6);
      }
    }
  }

  // repeat with inaccurate one
  SetupLoop(all_pos, start_res_idx, end_res_idx, mm_sim_inaccurate);
  sim = mm_sim_inaccurate.GetSimulation();
  sim->ApplySD(0.01, 25);
  // check that result is identical
  AllAtomPositionsPtr new_pos2 = all_pos.Copy();
  mm_sim_inaccurate.ExtractLoopPositions(*new_pos2, res_indices);
  for (uint i = 0; i < all_pos.GetNumAtoms(); ++i) {
    const Real dist = geom::Distance(new_pos->GetPos(i), new_pos2->GetPos(i));
    BOOST_CHECK_EQUAL(dist, Real(0));
  }

  // repeat without res_indices reshuffling (boring if idx = 0)
  if (start_res_idx > 0) {
    SetupLoopShift(all_pos, start_res_idx, end_res_idx, mm_sim);
    sim = mm_sim.GetSimulation();
    BOOST_CHECK_EQUAL(sim->GetPotentialEnergy(), pot_ref_loop);
    sim->ApplySD(0.01, 25);
    // check that result is identical
    new_pos2 = all_pos.Copy();
    std::vector<uint> res_indices2;
    for (uint i = 0; i <= end_res_idx; ++i) res_indices2.push_back(i);
    mm_sim.ExtractLoopPositions(*new_pos2, res_indices2);
    for (uint i = 0; i < all_pos.GetNumAtoms(); ++i) {
      const Real dist = geom::Distance(new_pos->GetPos(i), new_pos2->GetPos(i));
      BOOST_CHECK_EQUAL(dist, Real(0));
    }
  }
}

void CheckLoopMinimize(const AllAtomPositions& all_pos,
                       bool fix_surrounding_hydrogens,
                       bool kill_es, Real cutoff) {
  ////////////////////////////////////////////////////////////////////////////
  // run single loops
  ////////////////////////////////////////////////////////////////////////////
  const uint num_residues = all_pos.GetNumResidues();
  CheckLoopMinimize(all_pos, fix_surrounding_hydrogens, kill_es, cutoff, 0,
                    num_residues-1);
  CheckLoopMinimize(all_pos, fix_surrounding_hydrogens, kill_es, cutoff, 10,
                    15);
  CheckLoopMinimize(all_pos, fix_surrounding_hydrogens, kill_es, cutoff,
                    num_residues-5, num_residues-1);
  
  ////////////////////////////////////////////////////////////////////////////
  // set up multi-loop problem (follows CheckLoopMinimize/SetupLoopShift)
  ////////////////////////////////////////////////////////////////////////////
  // fix problem
  const uint start_res_idx1 = 10;
  const uint end_res_idx1 = 15;
  const uint loop_length1 = end_res_idx1 - start_res_idx1 + 1;
  const uint start_res_idx2 = num_residues-5;
  const uint end_res_idx2 = num_residues-1;
  const uint loop_length2 = end_res_idx2 - start_res_idx2 + 1;
  // setup mm_sim
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::GetDefault();
  MmSystemCreator mm_sim(ff_lookup, fix_surrounding_hydrogens, kill_es, cutoff);
  MmSystemCreator mm_sim_inaccurate(ff_lookup, fix_surrounding_hydrogens,
                                    kill_es, cutoff, true);
  mm_sim.SetCpuPlatformSupport(false);
  mm_sim_inaccurate.SetCpuPlatformSupport(false);
  std::vector<uint> res_indices;
  for (uint i = 0; i < num_residues; ++i) res_indices.push_back(i);
  // same loops as above
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  loop_start_indices.push_back(start_res_idx1);
  loop_lengths.push_back(loop_length1);
  loop_start_indices.push_back(start_res_idx2);
  loop_lengths.push_back(loop_length2);
  // get terminal info
  std::vector<bool> is_n_ter(num_residues, false);
  std::vector<bool> is_c_ter(num_residues, false);
  is_n_ter[0] = true;
  is_c_ter[num_residues-1] = true;
  // get disulfid bridges
  std::vector< std::pair<uint,uint> > disulfid_bridges;
  disulfid_bridges = mm_sim.GetDisulfidBridges(all_pos, res_indices);
  // build system
  mm_sim.SetupSystem(all_pos, res_indices, loop_start_indices, loop_lengths,
                     is_n_ter, is_c_ter, disulfid_bridges);
  // check it
  BOOST_CHECK_EQUAL(mm_sim.GetNumResidues(), num_residues);
  BOOST_CHECK_EQUAL(mm_sim.GetNumLoopResidues(), 6u + 5u);
  BOOST_CHECK(mm_sim.GetLoopStartIndices() == loop_start_indices);
  BOOST_CHECK(mm_sim.GetLoopLengths() == loop_lengths);
  // run it for a bit
  ost::mol::mm::SimulationPtr sim = mm_sim.GetSimulation();
  Real pot_ref_loop = sim->GetPotentialEnergy();
  sim->ApplySD(0.01, 25);
  BOOST_CHECK(sim->GetPotentialEnergy() < pot_ref_loop);
  // check result
  AllAtomPositionsPtr new_pos = all_pos.Copy();
  mm_sim.ExtractLoopPositions(*new_pos, res_indices);
  for (uint res_idx = 0; res_idx < num_residues; ++res_idx) {
    // expect stems out-of-loop and parts of stems to be fixed and rest moving
    const bool in_loop = (   (   res_idx >= start_res_idx1
                              && res_idx <= end_res_idx1)
                          || (res_idx >= start_res_idx2
                              && res_idx <= end_res_idx2));
    const bool is_nstem = (   res_idx == start_res_idx1
                           || res_idx == start_res_idx2);
    // HACK: end_res_idx2 is C-terminal!
    const bool is_cstem = (res_idx == end_res_idx1);
    // check atoms
    const uint first_idx = all_pos.GetFirstIndex(res_idx);
    const uint last_idx = all_pos.GetLastIndex(res_idx);
    for (uint i = first_idx; i <= last_idx; ++i) {
      const uint atom_idx = i - first_idx;
      const Real dist = geom::Distance(all_pos.GetPos(i), new_pos->GetPos(i));
      if (!in_loop) {
        BOOST_CHECK_EQUAL(dist, Real(0));
      } else if (is_nstem && (   atom_idx == BB_N_INDEX
                              || atom_idx == BB_CA_INDEX
                              || atom_idx == BB_CB_INDEX)) {
        BOOST_CHECK(dist < Real(1e-6));
      } else if (is_cstem && (   atom_idx == BB_CA_INDEX
                              || atom_idx == BB_CB_INDEX
                              || atom_idx == BB_C_INDEX
                              || atom_idx == BB_O_INDEX)) {
        BOOST_CHECK(dist < Real(1e-6));
      } else {
        BOOST_CHECK(dist > 1e-6);
      }
    }
  }
  // repeat with inaccurate one
  mm_sim_inaccurate.SetupSystem(all_pos, res_indices, loop_start_indices,
                                loop_lengths, is_n_ter, is_c_ter,
                                disulfid_bridges);
  sim = mm_sim_inaccurate.GetSimulation();
  sim->ApplySD(0.01, 25);
  // check that result is identical
  AllAtomPositionsPtr new_pos2 = all_pos.Copy();
  mm_sim_inaccurate.ExtractLoopPositions(*new_pos2, res_indices);
  for (uint i = 0; i < all_pos.GetNumAtoms(); ++i) {
    const Real dist = geom::Distance(new_pos->GetPos(i), new_pos2->GetPos(i));
    BOOST_CHECK_EQUAL(dist, Real(0));
  }

  ////////////////////////////////////////////////////////////////////////////
  // try loop of length 1 (only sc should move!)
  ////////////////////////////////////////////////////////////////////////////
  // find interesting case
  uint loop_idx = 1;
  for (; loop_idx < num_residues-1; ++loop_idx) {
    const uint first_idx = all_pos.GetFirstIndex(loop_idx);
    const uint last_idx = all_pos.GetLastIndex(loop_idx);
    if (last_idx - first_idx > 4) break;
  }
  // build system
  loop_start_indices.assign(1, loop_idx);
  loop_lengths.assign(1, 1);
  mm_sim.SetupSystem(all_pos, res_indices, loop_start_indices, loop_lengths,
                     is_n_ter, is_c_ter, disulfid_bridges);
  // run it for a bit
  sim = mm_sim.GetSimulation();
  pot_ref_loop = sim->GetPotentialEnergy();
  sim->ApplySD(0.01, 25);
  BOOST_CHECK(sim->GetPotentialEnergy() < pot_ref_loop);
  // check result
  new_pos = all_pos.Copy();
  mm_sim.ExtractLoopPositions(*new_pos, res_indices);
  const uint first_idx = all_pos.GetFirstIndex(loop_idx);
  const uint last_idx = all_pos.GetLastIndex(loop_idx);
  for (uint i = 0; i < all_pos.GetNumAtoms(); ++i) {
    const Real dist = geom::Distance(all_pos.GetPos(i), new_pos->GetPos(i));
    if (i < first_idx || i > last_idx) {
      BOOST_CHECK_EQUAL(dist, Real(0));
    } else {
      // expect backbone to be fixed
      const uint atom_idx = i - first_idx;
      if (   atom_idx == BB_N_INDEX || atom_idx == BB_CA_INDEX
          || atom_idx == BB_C_INDEX || atom_idx == BB_O_INDEX
          || atom_idx == BB_CB_INDEX) {
        BOOST_CHECK(dist < Real(1e-6));
      } else {
        BOOST_CHECK(dist > 1e-6);
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////////
  // try loop of length 0 (nothing should move!)
  ////////////////////////////////////////////////////////////////////////////
  // build system
  loop_lengths[0] = 0;
  mm_sim.SetupSystem(all_pos, res_indices, loop_start_indices, loop_lengths,
                     is_n_ter, is_c_ter, disulfid_bridges);
  // run it for a bit
  sim = mm_sim.GetSimulation();
  pot_ref_loop = sim->GetPotentialEnergy();
  sim->ApplySD(0.01, 25);
  if (fix_surrounding_hydrogens) {
    // nothing should move...
    BOOST_CHECK_CLOSE(sim->GetPotentialEnergy(), pot_ref_loop, 1);
  } else {
    // hydrogens should move and get better...
    BOOST_CHECK(sim->GetPotentialEnergy() < pot_ref_loop);
  }
  // check heavy atoms (none should move)
  new_pos = all_pos.Copy();
  mm_sim.ExtractLoopPositions(*new_pos, res_indices);
  for (uint i = 0; i < all_pos.GetNumAtoms(); ++i) {
    const Real dist = geom::Distance(all_pos.GetPos(i), new_pos->GetPos(i));
    BOOST_CHECK_EQUAL(dist, Real(0));
  }
}

} // anon ns

BOOST_AUTO_TEST_CASE(test_mm_system_creator_setup) {
  // test1
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::GetDefault();
  MmSystemCreator mm_sim(ff_lookup, false, false, 10, false);
  BOOST_CHECK(mm_sim.GetFfLookup() == ff_lookup);
  BOOST_CHECK_EQUAL(mm_sim.IsFixSurroundingHydrogens(), false);
  BOOST_CHECK_EQUAL(mm_sim.IsKillElectrostatics(), false);
  BOOST_CHECK_EQUAL(mm_sim.GetNonbondedCutoff(), Real(10));
  BOOST_CHECK_EQUAL(mm_sim.HasInaccuratePotEnergy(), false);
  // test2
  MmSystemCreator mm_sim2(ff_lookup, true, true, 20, true);
  BOOST_CHECK(mm_sim2.GetFfLookup() == ff_lookup);
  BOOST_CHECK_EQUAL(mm_sim2.IsFixSurroundingHydrogens(), true);
  BOOST_CHECK_EQUAL(mm_sim2.IsKillElectrostatics(), true);
  BOOST_CHECK_EQUAL(mm_sim2.GetNonbondedCutoff(), Real(20));
  BOOST_CHECK_EQUAL(mm_sim2.HasInaccuratePotEnergy(), true);
}

BOOST_AUTO_TEST_CASE(test_mm_system_creator_full_sim) {
  // setup
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/1CRN.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  // check full simulation
  CompareFullSimulation(all_pos, false, 8);
  CompareFullSimulation(all_pos, true, 5);
}

BOOST_AUTO_TEST_CASE(test_mm_system_creator_loops) {
  // setup
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/1CRN.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  // check loops
  CheckLoops(all_pos, false, false, 8);
  CheckLoops(all_pos, false, true, 5);
  CheckLoops(all_pos, true, false, 8);
  CheckLoops(all_pos, true, true, 5);
}

BOOST_AUTO_TEST_CASE(test_mm_system_creator_loop_minimize) {
  // setup
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/1CRN.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  // check loop minimization with and w/o inaccurate_pot_e
  CheckLoopMinimize(all_pos, false, false, 8);
  CheckLoopMinimize(all_pos, false, true, 5);
  CheckLoopMinimize(all_pos, true, false, 8);
  CheckLoopMinimize(all_pos, true, true, 5);
}

BOOST_AUTO_TEST_CASE(test_mm_system_creator_sizes) {
  // setup
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/1CRN.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::GetDefault();
  MmSystemCreator mm_sim(ff_lookup);
  // get data for system setup
  const uint my_length = 5;
  const uint loop_length = 3;
  std::vector<uint> res_indices(my_length);
  std::vector<bool> is_n_ter(my_length, false);
  std::vector<bool> is_c_ter(my_length, false);
  std::vector< std::pair<uint,uint> > dis_bridges;
  // get 5 good indices
  for (uint i = 0; i < my_length; ++i) res_indices[i] = 10 + i;
  // setup system
  mm_sim.SetupSystem(all_pos, res_indices, loop_length, is_n_ter, is_c_ter,
                     dis_bridges);
  // check data
  const std::vector<ForcefieldAminoAcid>&
  ff_aa = mm_sim.GetForcefieldAminoAcids();
  const std::vector<uint>& first_idx = mm_sim.GetIndexing();
  const geom::Vec3List& pos = mm_sim.GetInternalPositions();
  BOOST_CHECK_EQUAL(ff_aa.size(), my_length);
  BOOST_CHECK_EQUAL(first_idx.size(), my_length+1);
  const uint num_atoms = first_idx.back();
  BOOST_CHECK_EQUAL(pos.size(), num_atoms);
  BOOST_CHECK_EQUAL(pos.size(), mm_sim.GetTopology()->GetNumParticles());
}

BOOST_AUTO_TEST_CASE(test_mm_system_creator_loop_exceptions) {
  // setup
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/1CRN.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  ForcefieldLookupPtr ff_lookup = ForcefieldLookup::GetDefault();
  MmSystemCreator mm_sim(ff_lookup);
  const uint num_residues = all_pos.GetNumResidues();
  // get data for system setup
  const uint my_length = 5;
  std::vector<uint> res_indices(my_length);
  std::vector<bool> is_n_ter(my_length, false);
  std::vector<bool> is_c_ter(my_length, false);
  std::vector< std::pair<uint,uint> > dis_bridges;
  // get 5 good indices
  for (uint i = 0; i < my_length; ++i) res_indices[i] = 10 + i;
  // generate bad indices
  std::vector<uint> res_indices_bad = res_indices;
  res_indices_bad.back() = num_residues;  // out-of-bounds
  std::vector<uint> res_indices_bad2 = res_indices;
  res_indices_bad2.back() = 20; // inconsistent
  std::vector<uint> res_indices_short(my_length-1);
  for (uint i = 0; i < my_length-1; ++i) res_indices_short[i] = res_indices[i];
  // make sure we can make it work
  BOOST_CHECK_NO_THROW(mm_sim.SetupSystem(all_pos, res_indices, my_length-1,
                                          is_n_ter, is_c_ter, dis_bridges));
  BOOST_CHECK_NO_THROW(mm_sim.SetupSystem(all_pos, res_indices, my_length,
                                          is_n_ter, is_c_ter, dis_bridges));
  // break it...
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices_bad, my_length,
                                       is_n_ter, is_c_ter, dis_bridges),
                    promod3::Error);
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices, my_length+1,
                                       is_n_ter, is_c_ter, dis_bridges),
                    promod3::Error);
  std::vector<bool> is_ter_bad;
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices, my_length,
                                       is_ter_bad, is_c_ter, dis_bridges),
                    promod3::Error);
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices, my_length,
                                       is_n_ter, is_ter_bad, dis_bridges),
                    promod3::Error);
  std::vector< std::pair<uint,uint> > dis_bridges_bad;
  dis_bridges_bad.push_back(std::make_pair(0, my_length));
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices, my_length,
                                       is_n_ter, is_c_ter, dis_bridges_bad),
                    promod3::Error);
  AllAtomPositionsPtr bad_pos = all_pos.Copy();
  bad_pos->ClearPos(res_indices[1], BB_O_INDEX);
  BOOST_CHECK_THROW(mm_sim.SetupSystem(*bad_pos, res_indices, my_length,
                                       is_n_ter, is_c_ter, dis_bridges),
                    promod3::Error);

  // let's check UpdatePositions
  BOOST_CHECK_NO_THROW(mm_sim.UpdatePositions(all_pos, res_indices));
  BOOST_CHECK_THROW(mm_sim.UpdatePositions(*bad_pos, res_indices),
                    promod3::Error);
  BOOST_CHECK_THROW(mm_sim.UpdatePositions(all_pos, res_indices_bad),
                    promod3::Error);
  BOOST_CHECK_THROW(mm_sim.UpdatePositions(all_pos, res_indices_bad2),
                    promod3::Error);

  // let's check ExtractLoopPositions
  AllAtomPositionsPtr loop_pos = all_pos.Extract(res_indices);
  BOOST_CHECK_NO_THROW(mm_sim.ExtractLoopPositions(*loop_pos));
  BOOST_CHECK_NO_THROW(mm_sim.ExtractLoopPositions(all_pos, res_indices));
  BOOST_CHECK_NO_THROW(mm_sim.ExtractLoopPositions(*bad_pos, res_indices));
  BOOST_CHECK_THROW(mm_sim.ExtractLoopPositions(all_pos, res_indices_bad),
                    promod3::Error);
  BOOST_CHECK_THROW(mm_sim.ExtractLoopPositions(all_pos, res_indices_bad2),
                    promod3::Error);
  BOOST_CHECK_THROW(mm_sim.ExtractLoopPositions(all_pos, res_indices_short),
                    promod3::Error);
  loop_pos = all_pos.Extract(res_indices_bad2);
  BOOST_CHECK_THROW(mm_sim.ExtractLoopPositions(*loop_pos), promod3::Error);
  loop_pos = all_pos.Extract(res_indices_short);
  BOOST_CHECK_THROW(mm_sim.ExtractLoopPositions(*loop_pos), promod3::Error);

  // check/break multi-loop
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  BOOST_CHECK_NO_THROW(mm_sim.SetupSystem(all_pos, res_indices,
                                          loop_start_indices, loop_lengths,
                                          is_n_ter, is_c_ter, dis_bridges));
  loop_start_indices.push_back(1);
  loop_lengths.push_back(5);
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices, loop_start_indices,
                                       loop_lengths, is_n_ter, is_c_ter,
                                       dis_bridges),
                    promod3::Error);
  loop_lengths[0] = 0;
  BOOST_CHECK_NO_THROW(mm_sim.SetupSystem(all_pos, res_indices,
                                          loop_start_indices, loop_lengths,
                                          is_n_ter, is_c_ter, dis_bridges));
  loop_start_indices.push_back(1);
  BOOST_CHECK_THROW(mm_sim.SetupSystem(all_pos, res_indices, loop_start_indices,
                                       loop_lengths, is_n_ter, is_c_ter,
                                       dis_bridges),
                    promod3::Error);
}

BOOST_AUTO_TEST_SUITE_END();
