// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/amino_acid_atoms.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

BOOST_AUTO_TEST_CASE(test_lookup_one_letter_codes) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::ALA), 'A');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::ARG), 'R');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::ASN), 'N');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::ASP), 'D');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::CYS), 'C');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::GLU), 'E');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::GLN), 'Q');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::GLY), 'G');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::HIS), 'H');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::ILE), 'I');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::LEU), 'L');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::LYS), 'K');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::MET), 'M');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::PHE), 'F');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::PRO), 'P');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::SER), 'S');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::THR), 'T');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::TRP), 'W');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::TYR), 'Y');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::VAL), 'V');
  BOOST_CHECK_EQUAL(lookup.GetOLC(ost::conop::XXX), 'X');
}

BOOST_AUTO_TEST_CASE(test_lookup_first_aaa) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::ALA), ALA_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::ARG), ARG_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::ASN), ASN_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::ASP), ASP_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::CYS), CYS_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::GLU), GLU_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::GLN), GLN_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::GLY), GLY_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::HIS), HIS_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::ILE), ILE_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::LEU), LEU_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::LYS), LYS_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::MET), MET_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::PHE), PHE_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::PRO), PRO_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::SER), SER_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::THR), THR_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::TRP), TRP_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::TYR), TYR_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::VAL), VAL_N);
  BOOST_CHECK_EQUAL(lookup.GetFirstAAA(ost::conop::XXX), XXX_NUM_ATOMS);
}

BOOST_AUTO_TEST_CASE(test_lookup_num_atoms) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::ALA), uint(5));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::ARG), uint(11));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::ASN), uint(8));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::ASP), uint(8));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::CYS), uint(6));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::GLU), uint(9));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::GLN), uint(9));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::GLY), uint(4));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::HIS), uint(10));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::ILE), uint(8));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::LEU), uint(8));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::LYS), uint(9));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::MET), uint(8));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::PHE), uint(11));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::PRO), uint(7));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::SER), uint(6));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::THR), uint(7));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::TRP), uint(14));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::TYR), uint(12));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::VAL), uint(7));
  BOOST_CHECK_EQUAL(lookup.GetNumAtoms(ost::conop::XXX), uint(0));
}

BOOST_AUTO_TEST_CASE(test_lookup_aa) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAA(ALA_CB),  ost::conop::ALA);
  BOOST_CHECK_EQUAL(lookup.GetAA(ARG_CA),  ost::conop::ARG);
  BOOST_CHECK_EQUAL(lookup.GetAA(ASN_C),   ost::conop::ASN);
  BOOST_CHECK_EQUAL(lookup.GetAA(ASP_O),   ost::conop::ASP);
  BOOST_CHECK_EQUAL(lookup.GetAA(CYS_SG),  ost::conop::CYS);
  BOOST_CHECK_EQUAL(lookup.GetAA(GLU_OE1), ost::conop::GLU);
  BOOST_CHECK_EQUAL(lookup.GetAA(GLN_NE2), ost::conop::GLN);
  BOOST_CHECK_EQUAL(lookup.GetAA(GLY_N),   ost::conop::GLY);
  BOOST_CHECK_EQUAL(lookup.GetAA(HIS_CD2), ost::conop::HIS);
  BOOST_CHECK_EQUAL(lookup.GetAA(ILE_CG2), ost::conop::ILE);
  BOOST_CHECK_EQUAL(lookup.GetAA(LEU_CD1), ost::conop::LEU);
  BOOST_CHECK_EQUAL(lookup.GetAA(LYS_NZ),  ost::conop::LYS);
  BOOST_CHECK_EQUAL(lookup.GetAA(MET_SD),  ost::conop::MET);
  BOOST_CHECK_EQUAL(lookup.GetAA(PHE_CE2), ost::conop::PHE);
  BOOST_CHECK_EQUAL(lookup.GetAA(PRO_CD),  ost::conop::PRO);
  BOOST_CHECK_EQUAL(lookup.GetAA(SER_OG),  ost::conop::SER);
  BOOST_CHECK_EQUAL(lookup.GetAA(THR_OG1), ost::conop::THR);
  BOOST_CHECK_EQUAL(lookup.GetAA(TRP_NE1), ost::conop::TRP);
  BOOST_CHECK_EQUAL(lookup.GetAA(TYR_CZ),  ost::conop::TYR);
  BOOST_CHECK_EQUAL(lookup.GetAA(VAL_CG1), ost::conop::VAL);
  BOOST_CHECK_EQUAL(lookup.GetAA(XXX_NUM_ATOMS), ost::conop::XXX);
}

BOOST_AUTO_TEST_CASE(test_lookup_atom_name) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ALA_CB),  "CB");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ARG_CA),  "CA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ASN_C),   "C");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ASP_O),   "O");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(CYS_SG),  "SG");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(GLU_OE1), "OE1");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(GLN_NE2), "NE2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(GLY_N),   "N");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(HIS_CD2), "CD2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ILE_CG2), "CG2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(LEU_CD1), "CD1");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(LYS_NZ),  "NZ");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(MET_SD),  "SD");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(PHE_CE2), "CE2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(PRO_CD),  "CD");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(SER_OG),  "OG");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(THR_OG1), "OG1");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(TRP_NE1), "NE1");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(TYR_CZ),  "CZ");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(VAL_CG1), "CG1");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(XXX_NUM_ATOMS), "UNK");
  // CHARMM / AMBER
  for (uint i = 0; i < XXX_NUM_ATOMS+1; ++i) {
    AminoAcidAtom aaa = AminoAcidAtom(i);
    if (aaa == ILE_CD1) {
      BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(aaa), "CD");
      BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(aaa), "CD");
    } else {
      BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(aaa), lookup.GetAtomName(aaa));
      BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(aaa), lookup.GetAtomName(aaa));
    }
  }
}

BOOST_AUTO_TEST_CASE(test_lookup_element) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetElement(ALA_CB),  "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(ARG_CA),  "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(ASN_C),   "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(ASP_O),   "O");
  BOOST_CHECK_EQUAL(lookup.GetElement(CYS_SG),  "S");
  BOOST_CHECK_EQUAL(lookup.GetElement(GLU_OE1), "O");
  BOOST_CHECK_EQUAL(lookup.GetElement(GLN_NE2), "N");
  BOOST_CHECK_EQUAL(lookup.GetElement(GLY_N),   "N");
  BOOST_CHECK_EQUAL(lookup.GetElement(HIS_CD2), "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(ILE_CG2), "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(LEU_CD1), "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(LYS_NZ),  "N");
  BOOST_CHECK_EQUAL(lookup.GetElement(MET_SD),  "S");
  BOOST_CHECK_EQUAL(lookup.GetElement(PHE_CE2), "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(PRO_CD),  "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(SER_OG),  "O");
  BOOST_CHECK_EQUAL(lookup.GetElement(THR_OG1), "O");
  BOOST_CHECK_EQUAL(lookup.GetElement(TRP_NE1), "N");
  BOOST_CHECK_EQUAL(lookup.GetElement(TYR_CZ),  "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(VAL_CG1), "C");
  BOOST_CHECK_EQUAL(lookup.GetElement(XXX_NUM_ATOMS), "?");
}

BOOST_AUTO_TEST_CASE(test_lookup_index) {
  using namespace ost::conop;
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetIndex(ALA,  "CB"), uint(BB_CB_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(ARG,  "CA"), uint(BB_CA_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(ASN,   "C"), uint(BB_C_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(ASP,   "O"), uint(BB_O_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(CYS,  "SG"), uint(CYS_SG_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(GLU, "OE1"), uint(GLU_OE1_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(GLN, "NE2"), uint(GLN_NE2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(GLY,   "N"), uint(BB_N_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(HIS, "CD2"), uint(HIS_CD2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(ILE, "CG2"), uint(ILE_CG2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(LEU, "CD1"), uint(LEU_CD1_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(LYS,  "NZ"), uint(LYS_NZ_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(MET,  "SD"), uint(MET_SD_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(PHE, "CE2"), uint(PHE_CE2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(PRO,  "CD"), uint(PRO_CD_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(SER,  "OG"), uint(SER_OG_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(THR, "OG1"), uint(THR_OG1_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(TRP, "NE1"), uint(TRP_NE1_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(TYR,  "CZ"), uint(TYR_CZ_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetIndex(VAL, "CG1"), uint(VAL_CG1_INDEX));
  // throw exceptions when unknown
  BOOST_CHECK_THROW(lookup.GetIndex(XXX, "CA"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetIndex(GLY, "CB"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetIndex(PHE, "BLA"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetIndex(PHE, "CG1"), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_lookup_aaa) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::ALA,  "CB"), ALA_CB);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::ARG,  "CA"), ARG_CA);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::ASN,   "C"), ASN_C);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::ASP,   "O"), ASP_O);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::CYS,  "SG"), CYS_SG);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::GLU, "OE1"), GLU_OE1);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::GLN, "NE2"), GLN_NE2);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::GLY,   "N"), GLY_N);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::HIS, "CD2"), HIS_CD2);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::ILE, "CG2"), ILE_CG2);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::LEU, "CD1"), LEU_CD1);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::LYS,  "NZ"), LYS_NZ);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::MET,  "SD"), MET_SD);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::PHE, "CE2"), PHE_CE2);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::PRO,  "CD"), PRO_CD);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::SER,  "OG"), SER_OG);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::THR, "OG1"), THR_OG1);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::TRP, "NE1"), TRP_NE1);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::TYR,  "CZ"), TYR_CZ);
  BOOST_CHECK_EQUAL(lookup.GetAAA(ost::conop::VAL, "CG1"), VAL_CG1);
  // throw exceptions when unknown
  BOOST_CHECK_THROW(lookup.GetAAA(ost::conop::XXX, "CA"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetAAA(ost::conop::GLY, "CB"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetAAA(ost::conop::PHE, "BLA"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetAAA(ost::conop::PHE, "CG1"), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_lookup_index_noexc) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  // should always produce same result as GetIndex for valid names
  for (uint i = 0; i < XXX_NUM_ATOMS; ++i) {
    AminoAcidAtom aaa = AminoAcidAtom(i);
    ost::conop::AminoAcid aa = lookup.GetAA(aaa);
    String aname = lookup.GetAtomName(aaa);
    int idx = lookup.GetIndexNoExc(aa, aname);
    BOOST_CHECK(idx >= 0);
    BOOST_CHECK_EQUAL(uint(idx), lookup.GetIndex(aa, aname));
  }
  // check that bad ones give -1
  BOOST_CHECK_EQUAL(lookup.GetIndexNoExc(ost::conop::XXX, "CA"), -1);
  BOOST_CHECK_EQUAL(lookup.GetIndexNoExc(ost::conop::GLY, "CB"), -1);
  BOOST_CHECK_EQUAL(lookup.GetIndexNoExc(ost::conop::PHE, "BLA"), -1);
  BOOST_CHECK_EQUAL(lookup.GetIndexNoExc(ost::conop::PHE, "CG1"), -1);
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_aa) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAA(ALA_HB2),  ost::conop::ALA);
  BOOST_CHECK_EQUAL(lookup.GetAA(ARG_HD3),  ost::conop::ARG);
  BOOST_CHECK_EQUAL(lookup.GetAA(ASN_HB2),  ost::conop::ASN);
  BOOST_CHECK_EQUAL(lookup.GetAA(ASP_HA),   ost::conop::ASP);
  BOOST_CHECK_EQUAL(lookup.GetAA(GLN_HA),   ost::conop::GLN);
  BOOST_CHECK_EQUAL(lookup.GetAA(GLU_HB2),  ost::conop::GLU);
  BOOST_CHECK_EQUAL(lookup.GetAA(LYS_H),    ost::conop::LYS);
  BOOST_CHECK_EQUAL(lookup.GetAA(SER_HG),   ost::conop::SER);
  BOOST_CHECK_EQUAL(lookup.GetAA(CYS_HB3),  ost::conop::CYS);
  BOOST_CHECK_EQUAL(lookup.GetAA(MET_HE3),  ost::conop::MET);
  BOOST_CHECK_EQUAL(lookup.GetAA(TRP_HA),   ost::conop::TRP);
  BOOST_CHECK_EQUAL(lookup.GetAA(TYR_HD1),  ost::conop::TYR);
  BOOST_CHECK_EQUAL(lookup.GetAA(THR_HG22), ost::conop::THR);
  BOOST_CHECK_EQUAL(lookup.GetAA(VAL_HG23), ost::conop::VAL);
  BOOST_CHECK_EQUAL(lookup.GetAA(ILE_HD12), ost::conop::ILE);
  BOOST_CHECK_EQUAL(lookup.GetAA(LEU_H),    ost::conop::LEU);
  BOOST_CHECK_EQUAL(lookup.GetAA(GLY_HA2),  ost::conop::GLY);
  BOOST_CHECK_EQUAL(lookup.GetAA(PRO_HA),   ost::conop::PRO);
  BOOST_CHECK_EQUAL(lookup.GetAA(HIS_HA),   ost::conop::HIS);
  BOOST_CHECK_EQUAL(lookup.GetAA(PHE_HA),   ost::conop::PHE);
  BOOST_CHECK_EQUAL(lookup.GetAA(XXX_NUM_HYDROGENS), ost::conop::XXX);
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_atom_name) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ALA_HB2),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ARG_HD3),  "HD3");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ASN_HB2),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ASP_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(GLN_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(GLU_HB2),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(LYS_H),    "H");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(SER_HG),   "HG");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(CYS_HB3),  "HB3");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(MET_HE3),  "HE3");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(TRP_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(TYR_HD1),  "HD1");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(THR_HG22), "HG22");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(VAL_HG23), "HG23");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(ILE_HD12), "HD12");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(LEU_H),    "H");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(GLY_HA2),  "HA2");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(PRO_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(HIS_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(PHE_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomName(XXX_NUM_HYDROGENS), "UNK");
  // AMBER
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(ALA_HB2),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(ARG_HD3),  "HD2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(ASN_HB2),  "HB1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(ASP_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(GLN_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(GLU_HB2),  "HB1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(LYS_H),    "H");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(SER_HG),   "HG");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(CYS_HB3),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(MET_HE3),  "HE3");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(TRP_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(TYR_HD1),  "HD1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(THR_HG22), "HG22");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(VAL_HG23), "HG23");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(ILE_HD12), "HD2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(LEU_H),    "H");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(GLY_HA2),  "HA1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(PRO_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(HIS_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(PHE_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameAmber(XXX_NUM_HYDROGENS), "UNK");
  // CHARMM
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(ALA_HB2),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(ARG_HD3),  "HD2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(ASN_HB2),  "HB1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(ASP_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(GLN_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(GLU_HB2),  "HB1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(LYS_H),    "HN");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(SER_HG),   "HG1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(CYS_HB3),  "HB2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(MET_HE3),  "HE3");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(TRP_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(TYR_HD1),  "HD1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(THR_HG22), "HG22");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(VAL_HG23), "HG23");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(ILE_HD12), "HD2");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(LEU_H),    "HN");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(GLY_HA2),  "HA1");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(PRO_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(HIS_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(PHE_HA),   "HA");
  BOOST_CHECK_EQUAL(lookup.GetAtomNameCharmm(XXX_NUM_HYDROGENS), "UNK");
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_anchor) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(ALA_HB2),  BB_CB_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(ARG_HD3),  ARG_CD_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(ASN_HB2),  BB_CB_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(ASP_HA),   BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(GLN_HA),   BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(GLU_HB2),  BB_CB_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(LYS_H),    BB_N_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(SER_HG),   SER_OG_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(CYS_HB3),  CYS_CB_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(MET_HE3),  MET_CE_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(TRP_HA),   BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(TYR_HD1),  TYR_CD1_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(THR_HG22), THR_CG2_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(VAL_HG23), VAL_CG2_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(ILE_HD12), ILE_CD1_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(LEU_H),    BB_N_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(GLY_HA2),  BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(PRO_HA),   BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(HIS_HA),   BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(PHE_HA),   BB_CA_INDEX);
  BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(XXX_NUM_HYDROGENS), XXX_NUM_ATOMS);
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_index) {
  using namespace ost::conop;
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(ALA,  "HB2"), uint(ALA_HB2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(ARG,  "HD3"), uint(ARG_HD3_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(ASN,  "HB2"), uint(ASN_HB2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(ASP,   "HA"), uint(ASP_HA_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(GLN,   "HA"), uint(GLN_HA_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(GLU,  "HB2"), uint(GLU_HB2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(LYS,    "H"), uint(LYS_H_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(SER,   "HG"), uint(SER_HG_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(CYS,  "HB3"), uint(CYS_HB3_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(MET,  "HE3"), uint(MET_HE3_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(TRP,   "HA"), uint(TRP_HA_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(TYR,  "HD1"), uint(TYR_HD1_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(THR, "HG22"), uint(THR_HG22_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(VAL, "HG23"), uint(VAL_HG23_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(ILE, "HD12"), uint(ILE_HD12_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(LEU,    "H"), uint(LEU_H_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(GLY,  "HA2"), uint(GLY_HA2_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(PRO,   "HA"), uint(PRO_HA_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(HIS,   "HA"), uint(HIS_HA_INDEX));
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndex(PHE,   "HA"), uint(PHE_HA_INDEX));
  // throw exceptions when unknown
  BOOST_CHECK_THROW(lookup.GetHydrogenIndex(XXX, "H"),   promod3::Error);
  BOOST_CHECK_THROW(lookup.GetHydrogenIndex(GLY, "HB2"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetHydrogenIndex(PHE, "BLA"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetHydrogenIndex(PHE, "HD3"), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_aaa) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::ALA,  "HB2"), ALA_HB2);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::ARG,  "HD3"), ARG_HD3);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::ASN,  "HB2"), ASN_HB2);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::ASP,   "HA"), ASP_HA);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::GLN,   "HA"), GLN_HA);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::GLU,  "HB2"), GLU_HB2);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::LYS,    "H"), LYS_H);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::SER,   "HG"), SER_HG);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::CYS,  "HB3"), CYS_HB3);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::MET,  "HE3"), MET_HE3);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::TRP,   "HA"), TRP_HA);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::TYR,  "HD1"), TYR_HD1);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::THR, "HG22"), THR_HG22);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::VAL, "HG23"), VAL_HG23);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::ILE, "HD12"), ILE_HD12);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::LEU,    "H"), LEU_H);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::GLY,  "HA2"), GLY_HA2);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::PRO,   "HA"), PRO_HA);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::HIS,   "HA"), HIS_HA);
  BOOST_CHECK_EQUAL(lookup.GetAAH(ost::conop::PHE,   "HA"), PHE_HA);
  // throw exceptions when unknown
  BOOST_CHECK_THROW(lookup.GetAAH(ost::conop::XXX, "H"),   promod3::Error);
  BOOST_CHECK_THROW(lookup.GetAAH(ost::conop::GLY, "HB2"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetAAH(ost::conop::PHE, "BLA"), promod3::Error);
  BOOST_CHECK_THROW(lookup.GetAAH(ost::conop::PHE, "HD3"), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_index_noexc) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  // should always produce same result as GetIndex for valid names
  for (uint i = 0; i < XXX_NUM_HYDROGENS; ++i) {
    AminoAcidHydrogen aah = AminoAcidHydrogen(i);
    ost::conop::AminoAcid aa = lookup.GetAA(aah);
    String aname = lookup.GetAtomName(aah);
    int idx = lookup.GetHydrogenIndexNoExc(aa, aname);
    BOOST_CHECK(idx >= 0);
    BOOST_CHECK_EQUAL(uint(idx), lookup.GetHydrogenIndex(aa, aname));
  }
  // check that bad ones give -1
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndexNoExc(ost::conop::XXX, "H"),   -1);
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndexNoExc(ost::conop::GLY, "HB2"), -1);
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndexNoExc(ost::conop::PHE, "BLA"), -1);
  BOOST_CHECK_EQUAL(lookup.GetHydrogenIndexNoExc(ost::conop::PHE, "HD3"), -1);
}

BOOST_AUTO_TEST_CASE(test_lookup_max_nums) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  // max nums should be bigger than any atom index...
  const uint max_num_atoms = lookup.GetMaxNumAtoms();
  const uint max_num_hydrogens = lookup.GetMaxNumHydrogens();
  // check all heavy atoms
  for (uint i = 0; i < XXX_NUM_ATOMS; ++i) {
    AminoAcidAtom aaa = AminoAcidAtom(i);
    ost::conop::AminoAcid aa = lookup.GetAA(aaa);
    String aname = lookup.GetAtomName(aaa);
    BOOST_CHECK(max_num_atoms > lookup.GetIndex(aa, aname));
  }
  // check all hydrogens
  for (uint i = 0; i < XXX_NUM_HYDROGENS; ++i) {
    AminoAcidHydrogen aah = AminoAcidHydrogen(i);
    ost::conop::AminoAcid aa = lookup.GetAA(aah);
    String aname = lookup.GetAtomName(aah);
    BOOST_CHECK(max_num_hydrogens > lookup.GetHydrogenIndex(aa, aname));
  }
}

BOOST_AUTO_TEST_CASE(test_lookup_hydrogen_hx_index) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  // check all AA
  for (uint aa_idx = 0; aa_idx <= ost::conop::XXX; ++aa_idx) {
    const ost::conop::AminoAcid aa = ost::conop::AminoAcid(aa_idx);
    std::vector<uint> tocheck;
    if (aa == ost::conop::XXX) {
      BOOST_CHECK_EQUAL(lookup.GetHNIndex(aa), -1);
      BOOST_CHECK_EQUAL(lookup.GetH1Index(aa), -1);
      BOOST_CHECK_EQUAL(lookup.GetH2Index(aa), -1);
      BOOST_CHECK_EQUAL(lookup.GetH3Index(aa), -1);
    } else if (aa == ost::conop::PRO) {
      BOOST_CHECK_EQUAL(lookup.GetHNIndex(aa), -1);
      tocheck.push_back(lookup.GetH1Index(aa));
      tocheck.push_back(lookup.GetH2Index(aa));
      BOOST_CHECK_EQUAL(lookup.GetH3Index(aa), -1);
    } else {
      tocheck.push_back(lookup.GetHNIndex(aa));
      tocheck.push_back(lookup.GetH1Index(aa));
      tocheck.push_back(lookup.GetH2Index(aa));
      tocheck.push_back(lookup.GetH3Index(aa));
    }
    for (uint i = 0; i < tocheck.size(); ++i) {
      BOOST_CHECK(tocheck[i] >= 0);
      const AminoAcidHydrogen aah = lookup.GetAAH(aa, tocheck[i]);
      BOOST_CHECK_EQUAL(lookup.GetAnchorAtomIndex(aah), BB_N_INDEX);
    }
  }
}

BOOST_AUTO_TEST_SUITE_END();
