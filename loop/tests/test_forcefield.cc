// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/forcefield_lookup.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <promod3/loop/hydrogen_constructor.hh>

#include <ost/mol/bond_handle.hh>
#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

void CheckIndexing(const ForcefieldLookup& ff_lookup, ForcefieldAminoAcid ff_aa,
                   bool is_nter, const HydrogenStorage& hydrogens) {
  // get AA stuff
  ost::conop::AminoAcid aa = ff_lookup.GetAA(ff_aa);
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  // cook up bitset to hold all expected indices
  uint num_atoms = ff_lookup.GetNumAtoms(ff_aa, is_nter, false);
  std::vector<bool> idx_set(num_atoms, false);
  for (uint i = 0; i < aa_lookup.GetNumAtoms(aa); ++i) {
    uint my_idx = ff_lookup.GetHeavyIndex(ff_aa, i);
    BOOST_CHECK(my_idx < num_atoms);
    BOOST_CHECK(!idx_set[my_idx]);
    idx_set[my_idx] = true;
  }
  for (uint i = 0; i < aa_lookup.GetNumHydrogens(aa); ++i) {
    if (hydrogens.IsSet(i)) {
      uint my_idx = ff_lookup.GetHydrogenIndex(ff_aa, i);
      BOOST_CHECK(my_idx < num_atoms);
      BOOST_CHECK(!idx_set[my_idx]);
      idx_set[my_idx] = true;
    }
  }
  for (uint i = 0; i < num_atoms; ++i) {
    BOOST_CHECK(idx_set[i]);
  }
  // check C-TER OXT
  BOOST_CHECK_EQUAL(ff_lookup.GetNumAtoms(ff_aa, is_nter, true), num_atoms+1);
  // given that the rest is taken it must be last...
  BOOST_CHECK_EQUAL(ff_lookup.GetOXTIndex(ff_aa, is_nter), num_atoms);
}

void CheckIndexingAll(const ForcefieldLookup& ff_lookup,
                      const AllAtomPositions& all_pos, uint res_idx,
                      HydrogenStorage& hydrogens, ForcefieldAminoAcid ff_aa) {
  // check prot. state
  HisProtonationState his_prot_state;
  his_prot_state = (ff_aa == FF_HISD) ? PROT_STATE_HISD : PROT_STATE_HISE;
  // NON-N-TER
  ConstructHydrogens(all_pos, res_idx, hydrogens, false, his_prot_state);
  ConstructHydrogenN(all_pos, res_idx, -1.0472, hydrogens);
  if (ff_aa == FF_CYS2) hydrogens.ClearPos(CYS_HG_INDEX); // CYS2 -> remove HG
  CheckIndexing(ff_lookup, ff_aa, false, hydrogens);
  // N-TER
  ConstructHydrogens(all_pos, res_idx, hydrogens, false, his_prot_state);
  ConstructHydrogenNTerminal(all_pos, res_idx, hydrogens);
  if (ff_aa == FF_CYS2) hydrogens.ClearPos(CYS_HG_INDEX); // CYS2 -> remove HG
  CheckIndexing(ff_lookup, ff_aa, true, hydrogens);
}


} // anon ns

BOOST_AUTO_TEST_CASE(test_forcefield_amino_acid) {
  // first 20 AA must match conop::AminoAcid
  BOOST_CHECK_EQUAL(uint(FF_ALA), uint(ost::conop::ALA));
  BOOST_CHECK_EQUAL(uint(FF_ARG), uint(ost::conop::ARG));
  BOOST_CHECK_EQUAL(uint(FF_ASN), uint(ost::conop::ASN));
  BOOST_CHECK_EQUAL(uint(FF_ASP), uint(ost::conop::ASP));
  BOOST_CHECK_EQUAL(uint(FF_GLN), uint(ost::conop::GLN));
  BOOST_CHECK_EQUAL(uint(FF_GLU), uint(ost::conop::GLU));
  BOOST_CHECK_EQUAL(uint(FF_LYS), uint(ost::conop::LYS));
  BOOST_CHECK_EQUAL(uint(FF_SER), uint(ost::conop::SER));
  BOOST_CHECK_EQUAL(uint(FF_CYS), uint(ost::conop::CYS));
  BOOST_CHECK_EQUAL(uint(FF_MET), uint(ost::conop::MET));
  BOOST_CHECK_EQUAL(uint(FF_TRP), uint(ost::conop::TRP));
  BOOST_CHECK_EQUAL(uint(FF_TYR), uint(ost::conop::TYR));
  BOOST_CHECK_EQUAL(uint(FF_THR), uint(ost::conop::THR));
  BOOST_CHECK_EQUAL(uint(FF_VAL), uint(ost::conop::VAL));
  BOOST_CHECK_EQUAL(uint(FF_ILE), uint(ost::conop::ILE));
  BOOST_CHECK_EQUAL(uint(FF_LEU), uint(ost::conop::LEU));
  BOOST_CHECK_EQUAL(uint(FF_GLY), uint(ost::conop::GLY));
  BOOST_CHECK_EQUAL(uint(FF_PRO), uint(ost::conop::PRO));
  BOOST_CHECK_EQUAL(uint(FF_HISE), uint(ost::conop::HIS));
  BOOST_CHECK_EQUAL(uint(FF_PHE), uint(ost::conop::PHE));
}

BOOST_AUTO_TEST_CASE(test_forcefield_lookup_getaa) {
  // get lookup
  ForcefieldLookup ff_lookup;
  // check all ForcefieldAminoAcid
  for (uint i = 0; i <= FF_XXX; ++i) {
    ForcefieldAminoAcid ff_aa = ForcefieldAminoAcid(i);
    ost::conop::AminoAcid aa = ff_lookup.GetAA(ff_aa);
    if (i < ost::conop::XXX) {
      BOOST_CHECK_EQUAL(aa, ost::conop::AminoAcid(i));
    } else if (i < FF_XXX) {
      BOOST_CHECK(aa < ost::conop::XXX);
    } else {
      BOOST_CHECK_EQUAL(aa, ost::conop::XXX);
    }
  }
}

BOOST_AUTO_TEST_CASE(test_forcefield_lookup_indexing) {
  // get data
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_aa.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  HydrogenStorage hydrogens;
  // get lookups
  ForcefieldLookup ff_lookup;
  // check all residues
  for (uint res_idx = 0; res_idx < all_pos.GetNumResidues(); ++res_idx) {
    ost::conop::AminoAcid aa = all_pos.GetAA(res_idx);
    ForcefieldAminoAcid ff_aa = ForcefieldAminoAcid(aa);
    CheckIndexingAll(ff_lookup, all_pos, res_idx, hydrogens, ff_aa);
    // check modified AA
    if (aa == ost::conop::CYS) {
      CheckIndexingAll(ff_lookup, all_pos, res_idx, hydrogens, FF_CYS2);
    } else if (aa == ost::conop::HIS) {
      CheckIndexingAll(ff_lookup, all_pos, res_idx, hydrogens, FF_HISD);
    }
  }
}

BOOST_AUTO_TEST_SUITE_END();
