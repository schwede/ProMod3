// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/torsion_sampler.hh>
#include <promod3/core/message.hh>
#include <promod3/loop/loop_object_loader.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>
#include <ost/mol/builder.hh>
#include <math.h>
#include <cstdio>

namespace{

  Real Cosine(const std::vector<Real>& one, const std::vector<Real>& two){

    if(one.size() != two.size()){
      throw("aaargh");
    }

    Real l1 = 0.0;
    Real l2 = 0.0;
 
    for(uint i = 0; i < one.size(); ++i){
      l1 += one[i]*one[i];
      l2 += two[i]*two[i];
    }   

    l1 = std::sqrt(l1);
    l2 = std::sqrt(l2);

    Real cosine = 0.0;

    for(uint i = 0; i < one.size(); ++i){
      cosine += one[i]/l1*two[i]/l2;
    }
    
    return cosine;
  }
}


BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

BOOST_AUTO_TEST_CASE(test_torsion_sampler_initialization){
  //there must be three groups of residue definitions in one query
  std::vector<String> group_definitions_one;
  group_definitions_one.push_back("all-all-all-all");
  BOOST_CHECK_THROW(TorsionSampler sampler(group_definitions_one,36,0),promod3::Error);

  //following example uses a valid query, but ALL possible combinations of the 20 
  //standard amino acids must be covered
  std::vector<String> group_definitions_two;
  group_definitions_two.push_back("all-GLY-all");
  BOOST_CHECK_THROW(TorsionSampler sampler(group_definitions_two,36,0),promod3::Error);

  //let's see, wether valid group_definitions run through
  std::vector<String> group_definitions_three;
  group_definitions_three.push_back("all-GLY-all");
  group_definitions_three.push_back("all-PRO-all");
  group_definitions_three.push_back("all-all-PRO");
  group_definitions_three.push_back("all-ILE,VAL-all");
  group_definitions_three.push_back("all-all-all");
  TorsionSampler sampler(group_definitions_three,36,0);

  //check, whether the indices have been correctly initialized
  BOOST_CHECK_EQUAL(sampler.GetHistogramIndex(ost::conop::ALA,ost::conop::GLY,ost::conop::ALA),static_cast<uint>(0));
  BOOST_CHECK_EQUAL(sampler.GetHistogramIndex(ost::conop::ALA,ost::conop::PRO,ost::conop::ALA),static_cast<uint>(1));
  BOOST_CHECK_EQUAL(sampler.GetHistogramIndex(ost::conop::ALA,ost::conop::ALA,ost::conop::PRO),static_cast<uint>(2));
  BOOST_CHECK_EQUAL(sampler.GetHistogramIndex(ost::conop::ALA,ost::conop::ILE,ost::conop::ALA),static_cast<uint>(3));
  BOOST_CHECK_EQUAL(sampler.GetHistogramIndex(ost::conop::ALA,ost::conop::VAL,ost::conop::ALA),static_cast<uint>(3));
  BOOST_CHECK_EQUAL(sampler.GetHistogramIndex(ost::conop::ALA,ost::conop::ALA,ost::conop::ALA),static_cast<uint>(4));
}

BOOST_AUTO_TEST_CASE(test_torsion_sampler_general_things){

  //let's first create a simple torsion sampler with huge bin size...
  std::vector<String> group_definitions;
  group_definitions.push_back("all-GLY-all");
  group_definitions.push_back("all-PRO-all");
  group_definitions.push_back("all-all-PRO");
  group_definitions.push_back("all-ILE,VAL-all");
  group_definitions.push_back("all-all-all");
  TorsionSampler sampler(group_definitions,2,0);

  //load an entity to train...
  String pdb_name = "data/4nyk.pdb";
  String out_file = "data/saved_torsion_sampler.dat";

  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);

  ost::conop::ProcessorPtr processor(new ost::conop::HeuristicProcessor(false,
                                                                        true,
                                                                        true,
                                                                        true,
                                                                        true,
                                                     ost::conop::CONOP_SILENT));
  processor->Process(test_ent);

  ost::mol::EntityView test_ent_peptide = test_ent.Select("peptide=true");

  sampler.ExtractStatistics(test_ent_peptide);

  sampler.UpdateDistributions();

  sampler.Save(out_file);

  TorsionSamplerPtr loaded_sampler = TorsionSampler::Load(out_file, 0);

  //check equality of probabilities for all bins and all group definitions 
  for(uint i = 0; i < 4; ++i){
    BOOST_CHECK_EQUAL(sampler.GetProbability(i,std::make_pair(-M_PI/2,-M_PI/2)),
                      loaded_sampler->GetProbability(i,std::make_pair(-M_PI/2,-M_PI/2)));
    BOOST_CHECK_EQUAL(sampler.GetProbability(i,std::make_pair(-M_PI/2,M_PI/2)),
                      loaded_sampler->GetProbability(i,std::make_pair(-M_PI/2,M_PI/2)));
    BOOST_CHECK_EQUAL(sampler.GetProbability(i,std::make_pair(M_PI/2,-M_PI/2)),
                      loaded_sampler->GetProbability(i,std::make_pair(M_PI/2,-M_PI/2)));
    BOOST_CHECK_EQUAL(sampler.GetProbability(i,std::make_pair(M_PI/2,M_PI/2)),
                      loaded_sampler->GetProbability(i,std::make_pair(M_PI/2,M_PI/2)));
  }

  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::GLY,ost::conop::ALA,std::make_pair(-M_PI/2,-M_PI/2)),0.407407,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::PRO,ost::conop::ALA,std::make_pair(-M_PI/2,-M_PI/2)),0.2,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::ALA,ost::conop::PRO,std::make_pair(-M_PI/2,-M_PI/2)),0.0588235,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::ILE,ost::conop::ALA,std::make_pair(-M_PI/2,-M_PI/2)),0.487805,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::ALA,ost::conop::ALA,std::make_pair(-M_PI/2,-M_PI/2)),0.49837,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::GLY,ost::conop::ALA,std::make_pair(-M_PI/2,M_PI/2)),0.259259,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::PRO,ost::conop::ALA,std::make_pair(-M_PI/2,M_PI/2)),0.8,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::ALA,ost::conop::PRO,std::make_pair(-M_PI/2,M_PI/2)),0.882353,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::ILE,ost::conop::ALA,std::make_pair(-M_PI/2,M_PI/2)),0.512195,1e-3);
  BOOST_CHECK_CLOSE(sampler.GetProbability(ost::conop::ALA,ost::conop::ALA,ost::conop::ALA,std::make_pair(-M_PI/2,M_PI/2)),0.469055,1e-3);

  //clean up
  std::remove(out_file.c_str());
}

BOOST_AUTO_TEST_CASE(test_torsion_sampler_conditional_probabilities){

  TorsionSamplerPtr sampler = LoadTorsionSamplerCoil();

  std::pair<Real,Real> dihedral_pair;

  //check whether p(phi|psi) works properly
  std::vector<Real> full_prob, cond_prob;
  dihedral_pair.second = -0.78540;
  Real bin_size = sampler->GetBinSize();
  uint histogram_index = sampler->GetHistogramIndex(ost::conop::ALA,ost::conop::ALA,ost::conop::ALA);
  for(int i = 0; i < sampler->GetBinsPerDimension(); ++i){
    dihedral_pair.first = -M_PI + i * bin_size + bin_size / 2;
    full_prob.push_back(sampler->GetProbability(histogram_index,dihedral_pair));
    cond_prob.push_back(sampler->GetPhiProbabilityGivenPsi(histogram_index,dihedral_pair.first,dihedral_pair.second));
  }
  //check, whether cond probability sums up to one
  Real cond_sum = 0;
  for(std::vector<Real>::iterator i = cond_prob.begin(); i != cond_prob.end(); ++i){
    cond_sum += *i;
  }
  BOOST_CHECK_CLOSE(cond_sum,1.0,1e-4);

  Real full_sum = 0;
  for(std::vector<Real>::iterator i = full_prob.begin(); i != full_prob.end(); ++i){
    full_sum += *i;
  }
  for(uint i = 0; i < full_prob.size(); ++i){
    BOOST_CHECK_CLOSE(full_prob[i]/full_sum,cond_prob[i],1e-4);
  }

  //check whether drawing works
  std::vector<Real> draws(sampler->GetBinsPerDimension(),0);
  Real angle;
  int bin;
  for(int i = 0; i < 100000; ++i){
    angle = sampler->DrawPhiGivenPsi(histogram_index,-0.78540);
    bin = std::floor((angle + M_PI)/bin_size);
    draws[bin] += 1;
  }

  Real cosine = Cosine(draws,cond_prob);

  BOOST_CHECK(cosine > 0.999); //if the cosine between the drawn angles and actual probabilities
                               //close to one, the results can expected to be consistent

  //check whether p(psi|phi) works properly
  dihedral_pair.first = -1.0472;
  full_prob.clear();
  cond_prob.clear();

  for(int i = 0; i < sampler->GetBinsPerDimension(); ++i){
    dihedral_pair.second = -M_PI + i * bin_size + bin_size / 2;
    full_prob.push_back(sampler->GetProbability(histogram_index,dihedral_pair));
    cond_prob.push_back(sampler->GetPsiProbabilityGivenPhi(histogram_index,dihedral_pair.second,dihedral_pair.first));
  }
  //check, whether cond probability sums up to one
  cond_sum = 0;
  for(std::vector<Real>::iterator i = cond_prob.begin(); i != cond_prob.end(); ++i){
    cond_sum += *i;
  }
  BOOST_CHECK_CLOSE(cond_sum,1.0,1e-4);

  full_sum = 0;
  for(std::vector<Real>::iterator i = full_prob.begin(); i != full_prob.end(); ++i){
    full_sum += *i;
  }
  for(uint i = 0; i < full_prob.size(); ++i){
    BOOST_CHECK_CLOSE(full_prob[i]/full_sum,cond_prob[i],1e-4);
  }

  //check whether drawing works
  memset(&draws[0],0,sampler->GetBinsPerDimension()*sizeof(Real));
  for(int i = 0; i < 100000; ++i){
    angle = sampler->DrawPsiGivenPhi(histogram_index,-1.0472);
    bin = std::floor((angle + M_PI)/bin_size);
    draws[bin] += 1;
  }

  cosine = Cosine(draws,cond_prob);

  BOOST_CHECK(cosine > 0.999); //if the cosine between the drawn angles and actual probabilities
                               //close to one, the results can expected to be consistent



}



BOOST_AUTO_TEST_SUITE_END();
