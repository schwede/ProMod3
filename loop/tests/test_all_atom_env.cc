// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/all_atom_env.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>
#include <ost/mol/builder.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/seq/sequence_op.hh>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

AllAtomEnv GenerateAllAtomEnv(
      const String& seqresstr = "MGSLVREWVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQAEGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQVVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHVDAAALEHHHHHH") {

  ost::seq::SequenceHandle seqres = ost::seq::CreateSequence("A", seqresstr);
  ost::seq::SequenceList seqres_list = ost::seq::CreateSequenceList();
  seqres_list.AddSequence(seqres);

  AllAtomEnv env(seqres_list);
  return env;
}

// test listener: checks for predefined expected outcome
class TestListener: public AllAtomEnvListener {
public:
  // all public data for convenience
  TestListener(String myid = "TestListener"): init_done(false),
                                              reset_done(false),
                                              myid_(myid) { }
  bool init_done;
  bool reset_done;
  std::vector<uint> last_update;

  // AllAtomEnvListener interface
  virtual void Init(const AllAtomEnv& base_env) {
    init_done = true;
  }
  virtual void ResetEnvironment(const AllAtomEnv& base_env) {
    reset_done = true;
  }
  virtual void UpdateEnvironment(const AllAtomEnv& base_env, 
                                 const std::vector<uint>& res_indices) {
    last_update = res_indices;
  }
  virtual String WhoAmI() const { return myid_; }

private:
  String myid_;
};
typedef boost::shared_ptr<TestListener> TestListenerPtr;

} // anon ns

BOOST_AUTO_TEST_CASE(test_all_atom_env_setup) {

  // check normal setup
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/3DEFA.pdb");
  AllAtomEnv env = GenerateAllAtomEnv();
  BOOST_CHECK_NO_THROW(env.SetInitialEnvironment(test_ent));

  // check invalid olc
  BOOST_CHECK_THROW(GenerateAllAtomEnv("MUX"), promod3::Error);
  // check inconsistent env.
  String wrong_seqres = "MGSLVAAAVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQAEGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQVVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHVDAAALEHHHHHH";
  AllAtomEnv wrong_env = GenerateAllAtomEnv(wrong_seqres);
  BOOST_CHECK_THROW(wrong_env.SetInitialEnvironment(test_ent), promod3::Error);
  // check wrong numbering
  test_ent.EditXCS().RenumberAllResidues(42, true);
  BOOST_CHECK_THROW(env.SetInitialEnvironment(test_ent), promod3::Error);

  // same thing, this time with AllAtomPositions
  AllAtomPositions new_pos("LVREWVGF");
  BOOST_CHECK_NO_THROW(env.SetEnvironment(new_pos, 4, 0));
  BOOST_CHECK_THROW(env.SetEnvironment(new_pos, 5, 0), promod3::Error);
  BOOST_CHECK_THROW(env.SetEnvironment(new_pos, -1, 0), promod3::Error);
  BOOST_CHECK_THROW(env.SetEnvironment(new_pos, 260, 0), promod3::Error);
  BOOST_CHECK_THROW(wrong_env.SetEnvironment(new_pos, 4, 0), promod3::Error);

  // same thing with BackboneList
  ConstAllAtomPositionsPtr env_all_pos = env.GetAllPosData();
  AllAtomPositionsPtr ref_all_pos = env_all_pos->Copy();
  BackboneList my_loop("KLIEFFGKLK");
  env.SetEnvironment(my_loop, 21);
  BOOST_CHECK(*env_all_pos != *ref_all_pos);
  BOOST_CHECK(*env_all_pos->Extract(0, 20) == *ref_all_pos->Extract(0, 20));
  BOOST_CHECK(*env_all_pos->Extract(20, 30) != *ref_all_pos->Extract(20, 30));
  BOOST_CHECK(*env_all_pos->Extract(20, 30) == AllAtomPositions(my_loop));
  BOOST_CHECK(env_all_pos->ExtractBackbone(20, 30) == my_loop);
  const uint N = env_all_pos->GetNumResidues();
  BOOST_CHECK(*env_all_pos->Extract(30, N) == *ref_all_pos->Extract(30, N));
  // check exceptions
  BOOST_CHECK_THROW(env.SetEnvironment(my_loop, 5, 0), promod3::Error);
  BOOST_CHECK_THROW(env.SetEnvironment(my_loop, -1, 0), promod3::Error);
  BOOST_CHECK_THROW(env.SetEnvironment(my_loop, 260, 0), promod3::Error);
  BOOST_CHECK_THROW(wrong_env.SetEnvironment(my_loop, 4, 0), promod3::Error);

  // check AllAtomEnvPositions
  AllAtomEnvPositions new_env_pos;
  BOOST_CHECK_THROW(env.SetEnvironment(new_env_pos), promod3::Error);
  new_env_pos.all_pos = ref_all_pos;
  new_env_pos.res_indices = env.GetIdxHandlerData()->ToIdxVector(1, N, 0);
  env.SetEnvironment(new_env_pos);
  BOOST_CHECK(*env_all_pos == *ref_all_pos);
}

BOOST_AUTO_TEST_CASE(test_all_atom_env_listener) {
  // setup (note: start res. num. = 6, gap at num. 70)
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/3DEFA.pdb");
  AllAtomEnv env = GenerateAllAtomEnv();
  
  // check missing listener
  BOOST_CHECK(env.GetListener("TestListener") == NULL);
  // add unique listener
  TestListenerPtr listener = env.UniqueListener<TestListener>("TestListener");
  BOOST_CHECK(listener != NULL);
  BOOST_CHECK(env.GetListener("TestListener") == listener);
  BOOST_CHECK(env.UniqueListener<TestListener>("TestListener") == listener);
  BOOST_CHECK(listener->init_done);
  BOOST_CHECK(listener->reset_done);
  
  // set initial env. -> should call ResetEnvironment
  listener->reset_done = false;
  env.SetInitialEnvironment(test_ent);
  BOOST_CHECK(listener->reset_done);
  BOOST_CHECK(listener->last_update.empty());
  // keep subset for later
  AllAtomPositionsPtr new_pos = env.GetAllPosData()->Extract(7,12);
  new_pos->ClearResidue(0);
  new_pos->ClearPos(1, BB_O_INDEX); // should have no effect

  // clear res. nums 10,11 (indices = num - 1)
  std::vector<uint> exp_res_indices;
  exp_res_indices.push_back(9);
  exp_res_indices.push_back(10);
  env.ClearEnvironment(10, 2);
  BOOST_CHECK(listener->last_update == exp_res_indices);

  // reset nums 8-12 (indices = num - 1)
  exp_res_indices.clear();
  exp_res_indices.push_back(7);
  exp_res_indices.push_back(8);
  exp_res_indices.push_back(9);
  exp_res_indices.push_back(10);
  exp_res_indices.push_back(11);
  // stretch
  env.SetEnvironment(*new_pos, 8);
  BOOST_CHECK(listener->last_update == exp_res_indices);
  // res. indices
  listener->last_update.clear();
  env.SetEnvironment(*new_pos, exp_res_indices);
  BOOST_CHECK(listener->last_update == exp_res_indices);
  // AllAtomEnvPositions
  AllAtomEnvPositions new_env_pos;
  new_env_pos.all_pos = new_pos;
  new_env_pos.res_indices = exp_res_indices;
  listener->last_update.clear();
  env.SetEnvironment(new_env_pos);
  BOOST_CHECK(listener->last_update == exp_res_indices);
  // BackboneList
  listener->last_update.clear();
  env.SetEnvironment(BackboneList(new_pos->GetSequence()), 8);
  BOOST_CHECK(listener->last_update == exp_res_indices);

  // attach new listener (should set all)
  TestListenerPtr listener2(new TestListener("TL2"));
  env.AttachListener(listener2);
  BOOST_CHECK(listener2->init_done);
  BOOST_CHECK(listener2->reset_done);
  BOOST_CHECK(listener2->last_update.empty());
}

BOOST_AUTO_TEST_SUITE_END();
