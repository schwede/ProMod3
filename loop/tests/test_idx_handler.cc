// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/idx_handler.hh>
#include <promod3/loop/backbone.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

namespace {

bool HasLoop(const std::vector<uint>& loop_start_indices,
             const std::vector<uint>& loop_lengths,
             uint exp_start_idx, uint exp_length) {
  // check if exp_... appear in vector
  const uint num_loops = loop_start_indices.size();
  for (uint i_loop = 0; i_loop < num_loops; ++i_loop) {
    if (loop_start_indices[i_loop] == exp_start_idx
        && loop_lengths[i_loop] == exp_length) return true;
  }
  return false;
}

} // anon ns

BOOST_AUTO_TEST_CASE(test_idx_handler) {
  // setup
  std::vector<size_t> chain_sizes;
  chain_sizes.push_back(3);
  chain_sizes.push_back(0);
  chain_sizes.push_back(2);
  IdxHandler idx_handler(chain_sizes);

  // global checks
  BOOST_CHECK_EQUAL(idx_handler.GetNumChains(), size_t(3));
  BOOST_CHECK_EQUAL(idx_handler.GetNumResidues(), size_t(5));
  // per chain tests
  BOOST_CHECK_EQUAL(idx_handler.GetChainSize(0), size_t(3));
  BOOST_CHECK_EQUAL(idx_handler.GetChainSize(1), size_t(0));
  BOOST_CHECK_EQUAL(idx_handler.GetChainSize(2), size_t(2));
  BOOST_CHECK_EQUAL(idx_handler.GetChainStartIdx(0), uint(0));
  BOOST_CHECK_EQUAL(idx_handler.GetChainStartIdx(1), uint(3));
  BOOST_CHECK_EQUAL(idx_handler.GetChainStartIdx(2), uint(3));
  BOOST_CHECK_EQUAL(idx_handler.GetChainLastIdx(0), uint(2));
  BOOST_CHECK_EQUAL(idx_handler.GetChainLastIdx(2), uint(4));
  // per residue tests
  BOOST_CHECK_EQUAL(idx_handler.ToIdx(1,0), uint(0));
  BOOST_CHECK_EQUAL(idx_handler.ToIdx(2,0), uint(1));
  BOOST_CHECK_EQUAL(idx_handler.ToIdx(3,0), uint(2));
  BOOST_CHECK_EQUAL(idx_handler.ToIdx(1,2), uint(3));
  BOOST_CHECK_EQUAL(idx_handler.ToIdx(2,2), uint(4));
  BOOST_CHECK_EQUAL(idx_handler.GetChainIdx(0), uint(0));
  BOOST_CHECK_EQUAL(idx_handler.GetChainIdx(1), uint(0));
  BOOST_CHECK_EQUAL(idx_handler.GetChainIdx(2), uint(0));
  BOOST_CHECK_EQUAL(idx_handler.GetChainIdx(3), uint(2));
  BOOST_CHECK_EQUAL(idx_handler.GetChainIdx(4), uint(2));
  // vector
  std::vector<uint> res_indices = idx_handler.ToIdxVector(1, 3, 0);
  BOOST_CHECK_EQUAL(res_indices.size(), size_t(3));
  BOOST_CHECK_EQUAL(res_indices[0], uint(0));
  BOOST_CHECK_EQUAL(res_indices[1], uint(1));
  BOOST_CHECK_EQUAL(res_indices[2], uint(2));

  // exceptions
  BOOST_CHECK_THROW(idx_handler.ToIdx(4, 0), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdx(1, 1), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdx(0, 2), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdx(1, 3), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdxVector(1, 4, 0), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdxVector(1, 1, 1), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdxVector(0, 1, 2), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.ToIdxVector(1, 1, 3), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_idx_handler_bb_list) {
  // setup
  std::vector<size_t> chain_sizes;
  chain_sizes.push_back(10);
  IdxHandler idx_handler(chain_sizes);
  String seq = "HELLYEAH";
  BackboneList bb_list(seq);

  // check
  uint bb_list_size, start_idx, end_idx;
  idx_handler.SetupScoreCalculation(bb_list, 1, 0, bb_list_size, start_idx,
                                    end_idx);
  BOOST_CHECK_EQUAL(bb_list_size, uint(8));
  BOOST_CHECK_EQUAL(start_idx, uint(0));
  BOOST_CHECK_EQUAL(end_idx, uint(7));
  idx_handler.SetupScoreCalculation(bb_list, 3, 0, bb_list_size, start_idx,
                                    end_idx);
  BOOST_CHECK_EQUAL(bb_list_size, uint(8));
  BOOST_CHECK_EQUAL(start_idx, uint(2));
  BOOST_CHECK_EQUAL(end_idx, uint(9));

  // exceptions
  BOOST_CHECK_THROW(idx_handler.SetupScoreCalculation(bb_list, 0, 0,
                                                      bb_list_size, start_idx,
                                                      end_idx), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.SetupScoreCalculation(bb_list, 4, 0,
                                                      bb_list_size, start_idx,
                                                      end_idx), promod3::Error);
  BOOST_CHECK_THROW(idx_handler.SetupScoreCalculation(bb_list, 1, 1,
                                                      bb_list_size, start_idx,
                                                      end_idx), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_idx_handler_overlaps) {
  // setup
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  loop_start_indices.push_back(10);
  loop_start_indices.push_back(5);
  loop_start_indices.push_back(15);
  loop_lengths.push_back(2);
  loop_lengths.push_back(2);
  loop_lengths.push_back(3);

  // check
  std::vector<uint> loop_start_indices_res = loop_start_indices;
  std::vector<uint> loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK(loop_start_indices == loop_start_indices_res);
  BOOST_CHECK(loop_lengths == loop_lengths_res);

  // check empty loop
  loop_lengths[1] = 0;
  loop_start_indices_res = loop_start_indices;
  loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK_EQUAL(loop_start_indices_res.size(), 2u);
  BOOST_CHECK_EQUAL(loop_lengths_res.size(), 2u);
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 10, 2));
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 15, 3));

  // check touch (no overlap)
  loop_lengths[1] = 5;
  loop_start_indices_res = loop_start_indices;
  loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK(loop_start_indices == loop_start_indices_res);
  BOOST_CHECK(loop_lengths == loop_lengths_res);

  // check partial overlap
  loop_lengths[1] = 6;
  loop_start_indices_res = loop_start_indices;
  loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK_EQUAL(loop_start_indices_res.size(), 2u);
  BOOST_CHECK_EQUAL(loop_lengths_res.size(), 2u);
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 5, 7));
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 15, 3));

  // check full overlap
  loop_lengths[1] = 8;
  loop_start_indices_res = loop_start_indices;
  loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK_EQUAL(loop_start_indices_res.size(), 2u);
  BOOST_CHECK_EQUAL(loop_lengths_res.size(), 2u);
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 5, 8));
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 15, 3));

  // check double overlap
  loop_lengths[1] = 20;
  loop_start_indices_res = loop_start_indices;
  loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK_EQUAL(loop_start_indices_res.size(), 1u);
  BOOST_CHECK_EQUAL(loop_lengths_res.size(), 1u);
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 5, 20));

  // check double overlap with loop in loop
  loop_lengths[1] = 12;
  loop_start_indices_res = loop_start_indices;
  loop_lengths_res = loop_lengths;
  IdxHandler::ResolveOverlaps(loop_start_indices_res, loop_lengths_res);
  BOOST_CHECK_EQUAL(loop_start_indices_res.size(), 1u);
  BOOST_CHECK_EQUAL(loop_lengths_res.size(), 1u);
  BOOST_CHECK(HasLoop(loop_start_indices_res, loop_lengths_res, 5, 13));
}

BOOST_AUTO_TEST_CASE(test_idx_handler_multi_loops) {
  // setup
  std::vector<size_t> chain_sizes;
  chain_sizes.push_back(10);
  chain_sizes.push_back(0);
  chain_sizes.push_back(8);
  IdxHandler idx_handler(chain_sizes);
  // input data
  std::vector<uint> start_resnums;
  std::vector<uint> num_residues;
  std::vector<uint> chain_indices;
  start_resnums.push_back(1);
  num_residues.push_back(2);
  chain_indices.push_back(2);
  start_resnums.push_back(6);
  num_residues.push_back(2);
  chain_indices.push_back(0);
  start_resnums.push_back(6);
  num_residues.push_back(3);
  chain_indices.push_back(2);
  // output data
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;

  // check
  idx_handler.ToLoopIndices(start_resnums, num_residues, chain_indices,
                            loop_start_indices, loop_lengths);
  BOOST_CHECK_EQUAL(loop_start_indices.size(), 3u);
  BOOST_CHECK_EQUAL(loop_lengths.size(), 3u);
  BOOST_CHECK(HasLoop(loop_start_indices, loop_lengths, 10, 2));
  BOOST_CHECK(HasLoop(loop_start_indices, loop_lengths, 5, 2));
  BOOST_CHECK(HasLoop(loop_start_indices, loop_lengths, 15, 3));

  // check overlap fixing (just one of the cases)
  num_residues[0] = 6;
  idx_handler.ToLoopIndices(start_resnums, num_residues, chain_indices,
                            loop_start_indices, loop_lengths);
  BOOST_CHECK_EQUAL(loop_start_indices.size(), 2u);
  BOOST_CHECK_EQUAL(loop_lengths.size(), 2u);
  BOOST_CHECK(HasLoop(loop_start_indices, loop_lengths, 10, 8));
  BOOST_CHECK(HasLoop(loop_start_indices, loop_lengths, 5, 2));

  // check ToIdxVector
  std::vector<uint> res_indices
   = idx_handler.ToIdxVector(start_resnums, num_residues, chain_indices,
                             loop_start_indices, loop_lengths);
  BOOST_CHECK_EQUAL(res_indices.size(), 10u);
  BOOST_CHECK_EQUAL(loop_start_indices.size(), 2u);
  BOOST_CHECK_EQUAL(loop_lengths.size(), 2u);
  BOOST_CHECK_EQUAL(loop_start_indices[0], 0u);
  BOOST_CHECK_EQUAL(loop_start_indices[1], loop_lengths[0]);
  // we cannot assume any order so we need to resolve both options
  if (loop_lengths[0] == 8) {
    BOOST_CHECK_EQUAL(loop_lengths[1], 2u);
    for (uint i = 0; i < 10; ++i) {
      if (i < 8) BOOST_CHECK_EQUAL(res_indices[i], 10 + i);
      else       BOOST_CHECK_EQUAL(res_indices[i], 5 + i - 8);
    }
  } else {
    BOOST_CHECK_EQUAL(loop_lengths[0], 2u);
    BOOST_CHECK_EQUAL(loop_lengths[1], 8u);
    for (uint i = 0; i < 10; ++i) {
      if (i < 2) BOOST_CHECK_EQUAL(res_indices[i], 5 + i);
      else       BOOST_CHECK_EQUAL(res_indices[i], 10 + i - 2);
    }
  }

  // exceptions
  num_residues[0] = 2;
  start_resnums[0] = 9;
  BOOST_CHECK_THROW(idx_handler.ToLoopIndices(start_resnums, num_residues,
                                              chain_indices, loop_start_indices,
                                              loop_lengths), promod3::Error);
  start_resnums[0] = 8;
  BOOST_CHECK_THROW(idx_handler.ToLoopIndices(start_resnums, num_residues,
                                              chain_indices, loop_start_indices,
                                              loop_lengths), promod3::Error);
  start_resnums[0] = 0;
  BOOST_CHECK_THROW(idx_handler.ToLoopIndices(start_resnums, num_residues,
                                              chain_indices, loop_start_indices,
                                              loop_lengths), promod3::Error);
  start_resnums[0] = 1;
  // bad chain
  chain_indices[0] = 1;
  BOOST_CHECK_THROW(idx_handler.ToLoopIndices(start_resnums, num_residues,
                                              chain_indices, loop_start_indices,
                                              loop_lengths), promod3::Error);
  chain_indices[0] = 3;
  BOOST_CHECK_THROW(idx_handler.ToLoopIndices(start_resnums, num_residues,
                                              chain_indices, loop_start_indices,
                                              loop_lengths), promod3::Error);
  chain_indices[0] = 2;
  // bad vector sizes
  start_resnums.push_back(1);
  BOOST_CHECK_THROW(idx_handler.ToLoopIndices(start_resnums, num_residues,
                                              chain_indices, loop_start_indices,
                                              loop_lengths), promod3::Error);
}

BOOST_AUTO_TEST_SUITE_END();
