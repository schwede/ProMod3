// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/hydrogen_rule_lookup.hh>
#include <promod3/loop/hydrogen_constructor.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/mol/atom_view.hh>
#include <ost/mol/entity_view.hh>
#include <ost/mol/bond_handle.hh>
#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

// get atom-atom RMSD and check that all atoms exist in both
Real GetCheckRMSD(const ost::mol::EntityHandle& ent, const String& ref_path) {
  // get ref
  ost::mol::EntityHandle ent2 = LoadTestStructure(ref_path);
  // compare atom by atom
  Real sum = 0;
  ost::mol::ResidueHandleList res_list = ent.GetResidueList();
  ost::mol::ResidueHandleList res_list2 = ent2.GetResidueList();
  BOOST_CHECK_EQUAL(ent.GetResidueCount(), ent2.GetResidueCount());
  for (int i_r = 0; i_r < ent.GetResidueCount(); ++i_r) {
    const ost::mol::ResidueHandle& res = res_list[i_r];
    const ost::mol::ResidueHandle& res2 = res_list2[i_r];
    ost::mol::AtomHandleList atoms = res.GetAtomList();
    BOOST_CHECK_EQUAL(res.GetAtomCount(), res2.GetAtomCount());
    for (int i_a = 0; i_a < res.GetAtomCount(); ++i_a) {
      ost::mol::AtomHandle atom2 = res2.FindAtom(atoms[i_a].GetName());
      BOOST_CHECK(atom2.IsValid());
      sum += geom::Length2(atoms[i_a].GetPos() - atom2.GetPos());
    }
  }
  return std::sqrt(sum / ent.GetAtomCount());
}

ost::mol::ResidueView GetHIS(ost::mol::EntityHandle& ent) {
  ost::mol::EntityView ev_his = ent.Select("rname=HIS");
  ost::mol::ResidueViewList rvl_his = ev_his.GetResidueList();
  BOOST_CHECK_EQUAL(rvl_his.size(), size_t(1));
  return rvl_his[0];
}

template <typename T>
void CheckProtonations(const T& prot) {
  ost::mol::EntityHandle new_ent;
  Real rmsd;
  // check default
  new_ent = ConstructProtonatedEntity(prot);
  rmsd = GetCheckRMSD(new_ent, "data/all_aa_prot_ref.pdb");
  BOOST_CHECK(rmsd < 0.01);
  // check prot. states -> should add/replace HD1/HE2 of HIS
  ost::mol::ResidueView rv_his = GetHIS(new_ent);
  BOOST_CHECK(!rv_his.FindAtom("HD1").IsValid());
  BOOST_CHECK(rv_his.FindAtom("HE2").IsValid());
  int ref_count = new_ent.GetAtomCount();
  new_ent = ConstructProtonatedEntity(prot, false, PROT_STATE_HISD);
  BOOST_CHECK_EQUAL(new_ent.GetAtomCount(), ref_count);
  rv_his = GetHIS(new_ent);
  BOOST_CHECK(rv_his.FindAtom("HD1").IsValid());
  BOOST_CHECK(!rv_his.FindAtom("HE2").IsValid());
  new_ent = ConstructProtonatedEntity(prot, false, PROT_STATE_HISH);
  BOOST_CHECK_EQUAL(new_ent.GetAtomCount(), ref_count+1);
  rv_his = GetHIS(new_ent);
  BOOST_CHECK(rv_his.FindAtom("HD1").IsValid());
  BOOST_CHECK(rv_his.FindAtom("HE2").IsValid());
  // check some combinations
  new_ent = ConstructProtonatedEntity(prot, true);
  rmsd = GetCheckRMSD(new_ent, "data/all_aa_prot_polar_hise.pdb");
  BOOST_CHECK(rmsd < 0.01);
  new_ent = ConstructProtonatedEntity(prot, true, PROT_STATE_HISH);
  rmsd = GetCheckRMSD(new_ent, "data/all_aa_prot_polar_hish.pdb");
  BOOST_CHECK(rmsd < 0.01);
  new_ent = ConstructProtonatedEntity(prot, true, PROT_STATE_HISD);
  rmsd = GetCheckRMSD(new_ent, "data/all_aa_prot_polar_hisd.pdb");
  BOOST_CHECK(rmsd < 0.01);
}

} // anon ns

BOOST_AUTO_TEST_CASE(test_construct_hydrogens_ost_ent) {
  // get data
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_aa.pdb");
  // check protonations
  CheckProtonations(test_ent);
}

BOOST_AUTO_TEST_CASE(test_construct_hydrogens_aap) {
  // get data
  ost::mol::EntityHandle test_ent = LoadTestStructure("data/all_aa.pdb");
  AllAtomPositions all_pos(test_ent.GetResidueList());
  // check protonations
  CheckProtonations(all_pos);
}

BOOST_AUTO_TEST_CASE(test_hydrogen_single_rules) {
  // check if all single rules are defined as expected
  const AminoAcidLookup& aa_lookup = AminoAcidLookup::GetInstance();
  const HydrogenRuleLookup& hy_lookup = HydrogenRuleLookup::GetInstance();
  for (uint i = 0; i <= XXX_NUM_HYDROGENS; ++i) {
    const AminoAcidHydrogen aah = AminoAcidHydrogen(i);
    const HydrogenRule* p_rule = hy_lookup.GetRule(aah);
    if (aa_lookup.GetAnchorAtomIndex(aah) == BB_N_INDEX) {
      BOOST_CHECK(p_rule == NULL);
    } else if (aah == XXX_NUM_HYDROGENS) {
      BOOST_CHECK(p_rule == NULL);
    } else {
      BOOST_CHECK(p_rule != NULL);
      // check that we actually construct this hydrogen with it
      const ost::conop::AminoAcid aa = aa_lookup.GetAA(aah);
      bool found_it = false;
      for (uint j = 0; j < p_rule->num_hydrogens; ++j) {
        if (aah == aa_lookup.GetAAH(aa, p_rule->hydro_idx[j])) {
          found_it = true;
          break;
        }
      }
      BOOST_CHECK(found_it);
    }
  }
}

BOOST_AUTO_TEST_SUITE_END();
