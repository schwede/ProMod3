// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/loop/psipred_prediction.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( loop );
using namespace promod3::loop;

BOOST_AUTO_TEST_CASE(test_loading_from_hhm){

  PsipredPredictionPtr p = PsipredPrediction::FromHHM("data/1A88A.hhm");

  String expected_prediction = "CEEEECCCCEEEEEEECCCCCCEEEEECCCCCCHHHHHHHHHHHHH";
  expected_prediction += "CCCEEEEECCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCCCCEEEEEECHH";
  expected_prediction += "HHHHHHHHHHCCHHHCCEEEEECCCCCCCCCCCCCCCCCCHHHHHHHHHHHH";
  expected_prediction += "HHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHH";
  expected_prediction += "HCCCHHHHHHHCCCCEEEEEECCCCCCCHHHHHHHHHHHCCCCEEEEECCCC";
  expected_prediction += "CCHHHHCHHHHHHHHHHHHHC";

  int expected_confidence[] = {9,5,9,9,1,4,9,9,2,8,9,9,9,9,8,1,5,8,8,9,9,8,6,9,
  	                           9,9,8,9,9,9,9,9,7,7,7,9,9,9,9,9,9,9,9,9,8,6,8,9,
  	                           8,8,9,9,9,6,6,8,9,9,8,9,9,9,9,9,9,8,8,8,9,9,9,9,
  	                           9,9,9,9,9,9,9,9,9,9,9,8,1,9,9,9,8,6,9,9,9,9,8,8,
  	                           8,8,9,9,9,9,9,9,9,9,8,6,3,8,0,1,1,1,3,8,9,9,9,8,
  	                           7,8,7,6,1,0,0,0,1,3,3,3,5,4,3,2,3,2,7,8,9,9,9,9,
  	                           9,9,9,9,8,7,6,4,8,9,9,9,9,8,6,3,1,2,1,1,1,1,0,1,
  	                           3,4,3,1,0,1,0,2,7,8,9,9,9,9,9,9,9,8,6,3,1,1,0,6,
  	                           8,8,9,9,9,9,9,9,7,5,1,0,1,2,6,7,7,8,8,5,3,8,9,9,
  	                           9,8,9,9,9,9,5,8,9,9,8,6,6,9,8,9,9,9,9,9,9,9,9,8,
  	                           4,8,9,9,8,8,9,9,9,8,9,9,9,8,6,4,4,5,5,2,9,9,9,9,
  	                           9,9,9,9,9,9,9,9,8,4,9};

  BOOST_CHECK(p->size() == expected_prediction.size());

  for(uint i = 0; i < expected_prediction.size(); ++i){
  	BOOST_CHECK(expected_prediction[i] == p->GetPrediction(i));
  	BOOST_CHECK(expected_confidence[i] == p->GetConfidence(i));
  }
}

BOOST_AUTO_TEST_CASE(test_loading_from_horiz){

  PsipredPredictionPtr p = PsipredPrediction::FromHoriz("data/1akyA.horiz");

  String expected_prediction = "CCCEEEEECCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHCCCH";
  expected_prediction += "HHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCCCCCCCEEECCCCCCHHHHHH";
  expected_prediction += "HHHHHHHCCCCCCEEEEEECCHHHHHHHHHCCCCCCCCCCEEECCCCCCCCC";
  expected_prediction += "CCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCHHHHHHHHCCCEEEECCCC";
  expected_prediction += "CHHHHHHHHHHHHCCC";

  int expected_confidence[] = {9,6,0,4,9,9,9,7,6,9,9,9,9,9,3,5,1,5,7,7,9,9,9,8,
  	                           8,2,9,9,6,0,1,5,5,2,8,9,9,9,9,9,9,8,2,9,9,4,3,4,
  	                           9,9,9,9,9,9,7,6,3,9,9,9,6,2,8,9,9,9,9,9,9,9,9,9,
  	                           7,4,1,4,9,9,9,8,7,7,7,0,1,1,1,8,9,9,9,9,8,9,8,9,
  	                           9,9,9,9,9,9,9,9,7,2,9,9,9,7,6,7,9,9,9,8,4,3,9,6,
  	                           8,9,9,9,9,9,7,5,4,5,3,2,3,7,9,9,9,6,2,3,2,0,1,2,
  	                           7,9,9,8,7,8,8,7,7,7,8,7,9,7,6,3,2,1,5,8,9,9,9,9,
  	                           9,9,9,9,9,9,9,9,9,9,9,5,0,5,0,4,6,9,9,9,9,6,6,1,
  	                           9,7,7,8,8,3,0,7,9,9,9,5,7,8,9,9,9,9,9,9,9,7,3,4,
  	                           5,9};

  BOOST_CHECK(p->size() == expected_prediction.size());

  for(uint i = 0; i < expected_prediction.size(); ++i){
  	BOOST_CHECK(expected_prediction[i] == p->GetPrediction(i));
  	BOOST_CHECK(expected_confidence[i] == p->GetConfidence(i));
  }
}

BOOST_AUTO_TEST_CASE(test_init){


  std::vector<char> invalid_pred;
  std::vector<int> invalid_conf_one;
  std::vector<int> invalid_conf_two;
  std::vector<char> valid_pred;
  std::vector<int> valid_conf;
  std::vector<char> long_pred;

  invalid_pred.push_back('H');
  invalid_pred.push_back('X');
  invalid_conf_one.push_back(4);
  invalid_conf_one.push_back(-1);
  invalid_conf_two.push_back(4);
  invalid_conf_two.push_back(10);
  valid_pred.push_back('C');
  valid_pred.push_back('E');
  valid_conf.push_back(1);
  valid_conf.push_back(2);
  long_pred.push_back('E');
  long_pred.push_back('C');
  long_pred.push_back('H');

  BOOST_CHECK_THROW(PsipredPrediction(invalid_pred,valid_conf),promod3::Error);
  BOOST_CHECK_THROW(PsipredPrediction(valid_pred,invalid_conf_one),promod3::Error);
  BOOST_CHECK_THROW(PsipredPrediction(valid_pred,invalid_conf_two),promod3::Error);
  BOOST_CHECK_THROW(PsipredPrediction(long_pred,valid_conf),promod3::Error);
  BOOST_CHECK_NO_THROW(PsipredPrediction(valid_pred,valid_conf));
}

BOOST_AUTO_TEST_CASE(test_remaining_stuff){

  std::vector<char> pred;
  std::vector<int> conf;

  pred.push_back('H');
  pred.push_back('E');
  pred.push_back('C');
  pred.push_back('E');

  conf.push_back(9);
  conf.push_back(2);
  conf.push_back(3);
  conf.push_back(5);

  PsipredPrediction p(pred,conf);

  //check, whether the add functionality throws the expected exceptions
  BOOST_CHECK(p.size() == 4);
  BOOST_CHECK_THROW(p.Add('X',4),promod3::Error);
  BOOST_CHECK_THROW(p.Add('H',-1),promod3::Error);
  BOOST_CHECK_THROW(p.Add('H',10),promod3::Error);
  BOOST_CHECK_NO_THROW(p.Add('H',5));
  
  //Check, wether the Getter functions properly check the indices
  BOOST_CHECK_THROW(p.GetPrediction(-1),promod3::Error);
  BOOST_CHECK_THROW(p.GetPrediction(10),promod3::Error);

  BOOST_CHECK_THROW(p.GetConfidence(-1),promod3::Error);
  BOOST_CHECK_THROW(p.GetConfidence(10),promod3::Error);

  //check the size function
  BOOST_CHECK(p.size() == 5);

  //lets see whether the expected stuff is filled in...
  String expected_pred = "HECEH";
  int expected_conf[] = {9,2,3,5,5};

  for(int i = 0; i < 5; ++i){
  	BOOST_CHECK(expected_pred[i] == p.GetPrediction(i));
  	BOOST_CHECK(expected_conf[i] == p.GetConfidence(i));
  }

  //Check, whether the extract function does the right thing
  BOOST_CHECK_THROW(p.Extract(1,1),promod3::Error);
  BOOST_CHECK_THROW(p.Extract(1,10),promod3::Error);
  BOOST_CHECK_THROW(p.Extract(3,1),promod3::Error);

  PsipredPredictionPtr p_two = p.Extract(1,3);
  BOOST_CHECK(p_two->size() == 2);
  BOOST_CHECK(p_two->GetPrediction(0) == 'E');
  BOOST_CHECK(p_two->GetPrediction(1) == 'C');
  BOOST_CHECK(p_two->GetConfidence(0) == 2);
  BOOST_CHECK(p_two->GetConfidence(1) == 3);
}

BOOST_AUTO_TEST_SUITE_END();
