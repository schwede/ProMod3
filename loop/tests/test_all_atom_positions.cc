// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/loop/all_atom_positions.hh>
#include <promod3/core/message.hh>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/mol/bond_handle.hh>
#include <ost/io/mol/pdb_reader.hh>
#include <ost/conop/heuristic.hh>

BOOST_AUTO_TEST_SUITE( loop );

using namespace promod3::loop;

namespace {

ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

void TestIndexing(AllAtomPositions& atoms, uint res_idx, uint atom_idx,
                  const String& atom_name) {
  geom::Vec3 vec2(3,2,1);
  atoms.SetPos(res_idx, atom_idx, vec2);
  BOOST_CHECK_EQUAL(atoms.GetPos(res_idx, atom_idx), vec2);
  uint idx = atoms.GetIndex(res_idx, atom_idx);
  BOOST_CHECK_EQUAL(atoms.GetPos(idx), vec2);
  BOOST_CHECK_EQUAL(atoms.GetIndex(res_idx, atom_name), idx);
}

// doesn't compare res. number, otherwise EVERYTHING checked
void CompareResidues(const ost::mol::ResidueHandle& new_res,
                     const ost::mol::ResidueHandle& res) {
  BOOST_CHECK_EQUAL(new_res.GetName(), res.GetName());
  BOOST_CHECK_EQUAL(new_res.GetOneLetterCode(), res.GetOneLetterCode());
  // note: load + Heuristic are not reliable here
  const char aa_chem_class = ost::mol::ChemClass::L_PEPTIDE_LINKING;
  const char aa_chem_type = ost::mol::ChemType::AMINOACIDS;
  BOOST_CHECK(new_res.GetChemClass() == res.GetChemClass()
              || char(new_res.GetChemClass()) == aa_chem_class);
  BOOST_CHECK(new_res.GetChemType() == res.GetChemType()
              || char(new_res.GetChemType()) == aa_chem_type);
  // peptide linking should be consistent though
  BOOST_CHECK_EQUAL(new_res.IsPeptideLinking(), res.IsPeptideLinking());
  BOOST_CHECK_EQUAL(ost::mol::InSequence(new_res.GetPrev(), new_res),
                    ost::mol::InSequence(res.GetPrev(), res));
  // check atoms
  BOOST_CHECK_EQUAL(new_res.GetAtomCount(), res.GetAtomCount());
  ost::mol::AtomHandleList atom_list = res.GetAtomList();
  for (uint atom_idx = 0; atom_idx < atom_list.size(); ++atom_idx) {
    const ost::mol::AtomHandle& atom = atom_list[atom_idx];
    const ost::mol::AtomHandle new_atom = new_res.FindAtom(atom.GetName());
    BOOST_CHECK(new_atom.IsValid());
    BOOST_CHECK_EQUAL(new_atom.GetPos(), atom.GetPos());
    BOOST_CHECK_EQUAL(new_atom.GetElement(), atom.GetElement());
    // check bonds
    BOOST_CHECK_EQUAL(new_atom.GetBondCount(), atom.GetBondCount());
    ost::mol::AtomHandleList partner_list = atom.GetBondPartners();
    for (uint ip = 0; ip < partner_list.size(); ++ip) {
      // find new partner (ignore peptide bonds checked above)
      const ost::mol::AtomHandle& partner = partner_list[ip];
      if (atom.GetResidue() == partner.GetResidue()) {
        ost::mol::AtomHandle new_partner = new_res.FindAtom(partner.GetName());
        BOOST_CHECK(new_atom.FindBondToAtom(new_partner).IsValid());
      }
    }
  }
}

void CheckEntity(const ost::mol::EntityHandle& ent) {
  // look at all residues
  ost::mol::ResidueHandleList res_list = ent.GetResidueList();
  for (uint res_idx = 0; res_idx < res_list.size(); ++res_idx) {
    // get residues
    const ost::mol::ResidueHandle& r = res_list[res_idx];
    const ost::mol::ResidueHandle r_next = r.GetNext();
    const ost::mol::ResidueHandle r_prev = r.GetPrev();
    // check types
    BOOST_CHECK(r.IsPeptideLinking());
    BOOST_CHECK(r.IsProtein());
    // peptide links checked in CompareResidues
    // check torsions
    if (r_prev.IsValid() && ost::mol::InSequence(r_prev, r)) {
      BOOST_CHECK(r.GetPhiTorsion().IsValid());
      BOOST_CHECK(r.GetOmegaTorsion().IsValid());
    }
    if (r_next.IsValid() && ost::mol::InSequence(r, r_next)) {
      BOOST_CHECK(r.GetPsiTorsion().IsValid());
    }
  }
}

}

BOOST_AUTO_TEST_CASE(test_aap_setup) {
  // setup example
  String seq = "HELLYEAH";
  // expected atom sizes
  uint res_atoms[] = {
    HIS_NUM_ATOMS,
    GLU_NUM_ATOMS,
    LEU_NUM_ATOMS,
    LEU_NUM_ATOMS,
    TYR_NUM_ATOMS,
    GLU_NUM_ATOMS,
    ALA_NUM_ATOMS,
    HIS_NUM_ATOMS
  };
  uint num_atoms = 0;
  uint num_res = seq.size();
  for (uint i = 0; i < num_res; ++i) {
    num_atoms += res_atoms[i];
  }

  // get all atom container
  AllAtomPositions atoms(seq);

  // check entries
  BOOST_CHECK_EQUAL(atoms.GetNumAtoms(), num_atoms);
  BOOST_CHECK_EQUAL(atoms.GetNumResidues(), num_res);
  BOOST_CHECK_EQUAL(atoms.GetSequence(), seq);
  // loop over all residues
  for (uint i = 0; i < num_res; ++i) {
    uint num = atoms.GetLastIndex(i) - atoms.GetFirstIndex(i) + 1;
    BOOST_CHECK_EQUAL(num, res_atoms[i]);
  }
  // loop over all atoms
  for (uint i = 0; i < num_atoms; ++i) {
    BOOST_CHECK_EQUAL(atoms.IsSet(i), false);
  }

  // check for exceptions
  ost::mol::ResidueHandleList res_list;
  BOOST_CHECK_THROW(AllAtomPositions atoms(seq, res_list), promod3::Error);
  String seq_bad = "HELLO"; // O = invalid OLC
  BOOST_CHECK_THROW(AllAtomPositions atoms(seq_bad), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_aap_set_pos) {
  // setup example
  String seq = "HELLYEAH";
  AllAtomPositions atoms(seq);

  // loop over all atoms
  geom::Vec3 vec(1,2,3);
  for (uint i = 0; i < atoms.GetNumAtoms(); ++i) {
    BOOST_CHECK_EQUAL(atoms.IsSet(i), false);
    atoms.SetPos(i, vec);
    BOOST_CHECK_EQUAL(atoms.GetPos(i), vec);
    BOOST_CHECK_EQUAL(atoms.IsSet(i), true);
  }

  // try an example
  geom::Vec3 vec2(3,2,1);
  atoms.SetPos(1, GLU_CG_INDEX, vec2);
  BOOST_CHECK_EQUAL(atoms.GetPos(1, GLU_CG_INDEX), vec2);
  uint idx = atoms.GetIndex(1, GLU_CG_INDEX);
  BOOST_CHECK_EQUAL(atoms.GetPos(idx), vec2);
  idx = atoms.GetIndex(1, "CG");
  BOOST_CHECK_EQUAL(atoms.GetPos(idx), vec2);
}

BOOST_AUTO_TEST_CASE(test_aap_is_set) {
  // setup example
  String seq = "HELLYEAH";
  AllAtomPositions atoms(seq);

  // check residues (nothing set)
  for (uint i = 0; i < atoms.GetNumResidues(); ++i) {
    BOOST_CHECK(!atoms.IsAnySet(i));
    BOOST_CHECK(!atoms.IsAllSet(i));
  }

  // set one
  geom::Vec3 vec(1,2,3);
  atoms.SetPos(1, BB_N_INDEX, vec);
  BOOST_CHECK(atoms.IsAnySet(1));
  BOOST_CHECK(!atoms.IsAllSet(1));

  // set all
  for (uint i = 0; i < atoms.GetNumAtoms(); ++i) {
    atoms.SetPos(i, vec);
  }
  for (uint i = 0; i < atoms.GetNumResidues(); ++i) {
    BOOST_CHECK(atoms.IsAnySet(i));
    BOOST_CHECK(atoms.IsAllSet(i));
  }

  // clear one
  atoms.ClearPos(1, BB_N_INDEX);
  BOOST_CHECK(atoms.IsAnySet(1));
  BOOST_CHECK(!atoms.IsAllSet(1));
  
  // clear residue
  atoms.ClearResidue(2);
  BOOST_CHECK(!atoms.IsAnySet(2));
  BOOST_CHECK(!atoms.IsAllSet(2));
}

BOOST_AUTO_TEST_CASE(test_aap_indexing) {
  // setup example
  String seq = "HELLYEAH";
  AllAtomPositions atoms(seq);

  // try a few examples
  TestIndexing(atoms, 0, HIS_ND1_INDEX, "ND1");
  TestIndexing(atoms, 1, BB_N_INDEX, "N");
  TestIndexing(atoms, 2, BB_CA_INDEX, "CA");
  TestIndexing(atoms, 3, LEU_CG_INDEX, "CG");
  TestIndexing(atoms, 4, BB_O_INDEX, "O");
  TestIndexing(atoms, 5, BB_C_INDEX, "C");
  TestIndexing(atoms, 6, ALA_CB_INDEX, "CB");
  TestIndexing(atoms, 7, HIS_NE2_INDEX, "NE2");

  // check exceptions
  BOOST_CHECK_THROW(atoms.GetIndex(0, "X"), promod3::Error);
  BOOST_CHECK_THROW(atoms.GetIndex(1, "XX"), promod3::Error);
  BOOST_CHECK_THROW(atoms.GetIndex(2, "BA"), promod3::Error);
  BOOST_CHECK_THROW(atoms.GetIndex(3, "ND1"), promod3::Error);
  BOOST_CHECK_THROW(atoms.GetIndex(4, "NE2"), promod3::Error);
  BOOST_CHECK_THROW(atoms.GetIndex(6, "CG"), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_aap_aaa) {
  // setup example
  String seq = "HELLYEAH";
  AllAtomPositions atoms(seq);

  // try a few examples
  BOOST_CHECK_EQUAL(atoms.GetAAA(0, HIS_ND1_INDEX), HIS_ND1);
  BOOST_CHECK_EQUAL(atoms.GetAAA(1, BB_N_INDEX), GLU_N);
  BOOST_CHECK_EQUAL(atoms.GetAAA(2, BB_CA_INDEX), LEU_CA);
  BOOST_CHECK_EQUAL(atoms.GetAAA(3, LEU_CG_INDEX), LEU_CG);
  BOOST_CHECK_EQUAL(atoms.GetAAA(4, BB_O_INDEX), TYR_O);
  BOOST_CHECK_EQUAL(atoms.GetAAA(5, BB_C_INDEX), GLU_C);
  BOOST_CHECK_EQUAL(atoms.GetAAA(6, ALA_CB_INDEX), ALA_CB);
  BOOST_CHECK_EQUAL(atoms.GetAAA(7, HIS_NE2_INDEX), HIS_NE2);

  // check internal consistency
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  for (uint i = 0; i < atoms.GetNumResidues(); ++i) {
    AminoAcidAtom first_aaa = lookup.GetFirstAAA(atoms.GetAA(i));
    uint num_aaa = lookup.GetNumAtoms(atoms.GetAA(i));
    for (uint j = 0; j < num_aaa; ++j) {
      AminoAcidAtom exp_aaa = AminoAcidAtom(first_aaa + j);
      BOOST_CHECK_EQUAL(atoms.GetAAA(i, j), exp_aaa);
    }
  }
}

BOOST_AUTO_TEST_CASE(test_aap_from_ost) {
  // setup example
  ost::mol::EntityHandle prot = LoadTestStructure("data/3DEFA.pdb");
  ost::mol::ResidueHandleList res_list = prot.GetResidueList();
  // from python
  String exp_seq = "REWVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQAGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQVVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVVYAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHV";
  // NOTES:
  // - 4 sidechain atoms of res. num. 244 (idx. 237) GLN is missing
  // - res. num. 70 missing (gap)

  // DO IT
  AllAtomPositions atoms(res_list);
  // CHECK
  BOOST_CHECK_EQUAL(atoms.GetSequence(), exp_seq);
  BOOST_CHECK_EQUAL(atoms.GetNumAtoms(), uint(prot.GetAtomCount() + 4));
  BOOST_CHECK_EQUAL(atoms.GetNumResidues(), uint(prot.GetResidueCount()));
  // all set apart from known missing atoms?
  const uint res_idx = 237;
  uint first_idx = atoms.GetFirstIndex(res_idx) + GLN_CG_INDEX;
  uint last_idx = atoms.GetLastIndex(res_idx);
  for (uint i = 0; i < atoms.GetNumAtoms(); ++i) {
    if (i < first_idx || i > last_idx) {
      BOOST_CHECK(atoms.IsSet(i));
    } else {
      BOOST_CHECK(!atoms.IsSet(i));
    }
  }
}

BOOST_AUTO_TEST_CASE(test_aap_torsions) {
  // setup example (note: res. num. 70 missing!)
  ost::mol::EntityHandle prot = LoadTestStructure("data/3DEFA.pdb");
  ost::mol::ResidueHandleList res_list = prot.GetResidueList();
  AllAtomPositions atoms(res_list);
  BOOST_CHECK_EQUAL(res_list.size(), atoms.GetNumResidues());
  // check all available angles
  ost::mol::TorsionHandle tor;
  for (uint i = 0; i < res_list.size(); ++i) {
    // terminal?
    const bool n_ter = (i == 0);
    const bool c_ter = (i == res_list.size()-1);
    // phi
    if (!n_ter) {
      tor = res_list[i].GetPhiTorsion();
      if (tor.IsValid()) {
        BOOST_CHECK_CLOSE(atoms.GetPhiTorsion(i), tor.GetAngle(), 1e-5);
      }
    }
    // psi
    if (!c_ter) {
      tor = res_list[i].GetPsiTorsion();
      if (tor.IsValid()) {
        BOOST_CHECK_CLOSE(atoms.GetPsiTorsion(i), tor.GetAngle(), 1e-5);
      }
    }
    // omega
    if (!n_ter) {
      tor = res_list[i].GetOmegaTorsion();
      if (tor.IsValid()) {
        BOOST_CHECK_CLOSE(atoms.GetOmegaTorsion(i), tor.GetAngle(), 1e-5);
      }
    }
  }
  // check default angles (when stuff is not set)
  AllAtomPositionsPtr atoms_sub = atoms.Extract(0, 10);
  atoms_sub->ClearResidue(2);
  BOOST_CHECK_EQUAL(atoms_sub->GetPhiTorsion(1, -4), atoms.GetPhiTorsion(1));
  BOOST_CHECK_EQUAL(atoms_sub->GetPhiTorsion(2, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPhiTorsion(3, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPsiTorsion(1, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPsiTorsion(2, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPsiTorsion(3, -4), atoms.GetPsiTorsion(3));
  BOOST_CHECK_EQUAL(atoms_sub->GetOmegaTorsion(1, -4), atoms.GetOmegaTorsion(1));
  BOOST_CHECK_EQUAL(atoms_sub->GetOmegaTorsion(2, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetOmegaTorsion(3, -4), -4);
  atoms_sub->ClearPos(5, BB_C_INDEX);
  BOOST_CHECK_EQUAL(atoms_sub->GetPhiTorsion(4, -4), atoms.GetPhiTorsion(4));
  BOOST_CHECK_EQUAL(atoms_sub->GetPhiTorsion(5, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPhiTorsion(6, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPsiTorsion(4, -4), atoms.GetPsiTorsion(4));
  BOOST_CHECK_EQUAL(atoms_sub->GetPsiTorsion(5, -4), -4);
  BOOST_CHECK_EQUAL(atoms_sub->GetPsiTorsion(6, -4), atoms.GetPsiTorsion(6));
  BOOST_CHECK_EQUAL(atoms_sub->GetOmegaTorsion(4, -4), atoms.GetOmegaTorsion(4));
  BOOST_CHECK_EQUAL(atoms_sub->GetOmegaTorsion(5, -4), atoms.GetOmegaTorsion(5));
  BOOST_CHECK_EQUAL(atoms_sub->GetOmegaTorsion(6, -4), -4);
}

BOOST_AUTO_TEST_CASE(test_aap_copy) {
  // setup example
  String seq = "HELLYEAH";
  AllAtomPositions atoms(seq);
  // set dummy positions
  for (uint i = 0; i < atoms.GetNumAtoms(); ++i) {
    if (i != 23) atoms.SetPos(i, geom::Vec3(i,0,0));
  }
  // check copying
  AllAtomPositionsPtr atoms_copy = atoms.Copy();
  BOOST_CHECK(atoms == *atoms_copy);
  // check comparison
  atoms.SetPos(23, geom::Vec3(23,0,0));
  BOOST_CHECK(atoms != *atoms_copy);
}

BOOST_AUTO_TEST_CASE(test_aap_extract) {
  // setup example
  String seq = "HELLYEAH";
  AllAtomPositions atoms(seq);
  // set dummy positions
  for (uint i = 0; i < atoms.GetNumAtoms(); ++i) {
    if (i != 23) atoms.SetPos(i, geom::Vec3(i,0,0));
  }
  
  // check ranged subset
  AllAtomPositionsPtr atoms_sub = atoms.Extract(2, 5);
  BOOST_CHECK_EQUAL(atoms_sub->GetSequence(), "LLY");
  BOOST_CHECK_EQUAL(atoms_sub->GetNumResidues(), uint(3));
  uint pos_start = atoms.GetFirstIndex(2);
  for (uint i = 0; i < atoms_sub->GetNumAtoms(); ++i) {
    const uint exp_pos = i + pos_start;
    if (exp_pos != 23) {
      BOOST_CHECK_EQUAL(atoms_sub->GetPos(i), geom::Vec3(exp_pos,0,0));
      BOOST_CHECK(atoms_sub->IsSet(i));
    } else {
      BOOST_CHECK(!atoms_sub->IsSet(i));
    }
    BOOST_CHECK_EQUAL(atoms_sub->GetAAA(i), atoms.GetAAA(exp_pos));
  }
  // check cherry picked subset
  uint indices[] = {0, 2, 6, 7};
  atoms_sub = atoms.Extract(std::vector<uint>(indices, indices+4));
  BOOST_CHECK_EQUAL(atoms_sub->GetSequence(), "HLAH");
  BOOST_CHECK_EQUAL(atoms_sub->GetNumResidues(), uint(4));
  for (uint res_idx = 0; res_idx < atoms_sub->GetNumResidues(); ++res_idx) {
    pos_start = atoms.GetFirstIndex(indices[res_idx]);
    const uint first_idx = atoms_sub->GetFirstIndex(res_idx);
    const uint last_idx = atoms_sub->GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      const uint exp_pos = pos_start + (idx - first_idx);
      if (exp_pos != 23) {
        BOOST_CHECK_EQUAL(atoms_sub->GetPos(idx), geom::Vec3(exp_pos,0,0));
        BOOST_CHECK(atoms_sub->IsSet(idx));
      } else {
        BOOST_CHECK(!atoms_sub->IsSet(idx));
      }
      BOOST_CHECK_EQUAL(atoms_sub->GetAAA(idx), atoms.GetAAA(exp_pos));
    }
  }

  // border cases
  atoms_sub = atoms.Extract(0, 2);
  BOOST_CHECK_EQUAL(atoms_sub->GetSequence(), "HE");
  atoms_sub = atoms.Extract(2, 8);
  BOOST_CHECK_EQUAL(atoms_sub->GetSequence(), "LLYEAH");

  // check exceptions
  BOOST_CHECK_THROW(atoms.Extract(3, 2), promod3::Error);
  BOOST_CHECK_THROW(atoms.Extract(3, 3), promod3::Error);
  BOOST_CHECK_THROW(atoms.Extract(3, 9), promod3::Error);
  BOOST_CHECK_THROW(atoms.Extract(8, 9), promod3::Error);
  BOOST_CHECK_THROW(atoms.SetResidue(0, atoms, 9), promod3::Error);
  BOOST_CHECK_THROW(atoms.SetResidue(0, atoms, 5), promod3::Error);
  BOOST_CHECK_THROW(atoms.SetResidue(9, atoms, 0), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_aap_to_ost) {
  // setup example
  ost::mol::EntityHandle prot = LoadTestStructure("data/3DEFA.pdb");
  ost::mol::ResidueHandleList res_list = prot.GetResidueList();
  // DO IT
  AllAtomPositions atoms(res_list);
  ost::mol::EntityHandle new_prot = atoms.ToEntity();
  // CHECK
  BOOST_CHECK_EQUAL(new_prot.GetAtomCount(), prot.GetAtomCount());
  BOOST_CHECK_EQUAL(new_prot.GetResidueCount(), prot.GetResidueCount());
  ost::mol::ResidueHandleList new_res_list = new_prot.GetResidueList();
  for (uint res_idx = 0; res_idx < res_list.size(); ++res_idx) {
    CompareResidues(new_res_list[res_idx], res_list[res_idx]);
  }
  CheckEntity(new_prot);
}

BOOST_AUTO_TEST_CASE(test_aap_into_ost) {
  // setup example
  ost::mol::EntityHandle prot = LoadTestStructure("data/3DEFA.pdb");
  ost::mol::ResidueHandleList res_list = prot.GetResidueList();
  AllAtomPositions atoms(res_list);
  // fill empty entity
  ost::mol::EntityHandle new_prot = ost::mol::CreateEntity();
  ost::mol::XCSEditor edi = new_prot.EditXCS(ost::mol::BUFFERED_EDIT);
  ost::mol::ChainHandle chain = edi.InsertChain("A");

  // manually add some residues
  atoms.InsertInto(2, chain, res_list[2].GetNumber());
  atoms.InsertInto(4, chain, res_list[4].GetNumber());
  // check residues
  ost::mol::ResidueHandle res2 = chain.FindResidue(res_list[2].GetNumber());
  ost::mol::ResidueHandle res4 = chain.FindResidue(res_list[4].GetNumber());
  BOOST_CHECK(res2.IsValid());
  BOOST_CHECK(res4.IsValid());
  BOOST_CHECK_EQUAL(res2, res4.GetPrev());
  BOOST_CHECK_EQUAL(res2.GetNext(), res4);
  BOOST_CHECK_EQUAL(res2, chain.GetResidueByIndex(0));
  BOOST_CHECK_EQUAL(res4, chain.GetResidueByIndex(1));
  // no peptide links expected
  BOOST_CHECK(!ost::mol::InSequence(res2.GetPrev(), res2));
  BOOST_CHECK(!ost::mol::InSequence(res2, res4));
  BOOST_CHECK(!ost::mol::InSequence(res4, res4.GetNext()));
  // add in between -> should add links
  atoms.InsertInto(3, chain, res_list[3].GetNumber());
  ost::mol::ResidueHandle res3 = chain.FindResidue(res_list[3].GetNumber());
  BOOST_CHECK(!ost::mol::InSequence(res2.GetPrev(), res2));
  BOOST_CHECK(ost::mol::InSequence(res2, res3));
  BOOST_CHECK(ost::mol::InSequence(res3, res4));
  BOOST_CHECK(!ost::mol::InSequence(res4, res4.GetNext()));
  // check reordering
  BOOST_CHECK_EQUAL(res2, res3.GetPrev());
  BOOST_CHECK_EQUAL(res3.GetNext(), res4);
  BOOST_CHECK_EQUAL(res3, chain.GetResidueByIndex(1));
  BOOST_CHECK_EQUAL(res4, chain.GetResidueByIndex(2));
  // add before
  atoms.InsertInto(1, chain, res_list[1].GetNumber());
  BOOST_CHECK(ost::mol::InSequence(res2.GetPrev(), res2));
  // add after
  atoms.InsertInto(5, chain, res_list[5].GetNumber());
  BOOST_CHECK(ost::mol::InSequence(res4, res4.GetNext()));

  // add rest (note: start res. num. = 6, gap at num. 70)
  for (uint i = 0; i < atoms.GetNumResidues(); ++i) {
    atoms.InsertInto(i, chain, res_list[i].GetNumber());
  }
  // check
  BOOST_CHECK_EQUAL(new_prot.GetAtomCount(), prot.GetAtomCount());
  BOOST_CHECK_EQUAL(new_prot.GetResidueCount(), prot.GetResidueCount());
  for (uint res_idx = 0; res_idx < res_list.size(); ++res_idx) {
    ost::mol::ResidueHandle new_res = chain.GetResidueByIndex(res_idx);
    BOOST_CHECK_EQUAL(new_res.GetNumber(), res_list[res_idx].GetNumber());
    CompareResidues(new_res, res_list[res_idx]);
  }
  CheckEntity(new_prot);

  // check overwrite of residue with killed sidechain atom
  geom::Vec3 new_pos = atoms.GetPos(2, BB_C_INDEX) + geom::Vec3(1,1,1);
  atoms.SetPos(2, BB_C_INDEX, new_pos);
  atoms.ClearPos(2, BB_CB_INDEX);
  atoms.InsertInto(2, chain, res_list[2].GetNumber());
  // we should have kept the residue, killed CB and updated C
  BOOST_CHECK_EQUAL(res2, chain.GetResidueByIndex(2));
  BOOST_CHECK_EQUAL(res2, chain.FindResidue(res_list[2].GetNumber()));
  ost::mol::AtomHandle atom = res2.FindAtom("C");
  BOOST_CHECK_EQUAL(atom.GetPos(), new_pos);
  BOOST_CHECK(!res2.FindAtom("CB").IsValid());

  // check overwrite with different residue (note: res. idx. 2 is TRP)
  AllAtomPositions other_atoms("H");
  for (uint i = 0; i < other_atoms.GetNumAtoms(); ++i) {
    other_atoms.SetPos(i, geom::Vec3(i,0,0));
  }
  other_atoms.InsertInto(0, chain, res_list[2].GetNumber());
  BOOST_CHECK_EQUAL(res2.GetName(), "HIS");
  BOOST_CHECK_EQUAL(res2.GetOneLetterCode(), 'H');
  BOOST_CHECK_EQUAL(res2.GetAtomCount(), HIS_NUM_ATOMS);
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  for (uint i = 0; i < other_atoms.GetNumAtoms(); ++i) {
    const String& aname = lookup.GetAtomName(other_atoms.GetAAA(i));
    atom = res2.FindAtom(aname);
    BOOST_CHECK(atom.IsValid());
    BOOST_CHECK_EQUAL(atom.GetPos(), geom::Vec3(i,0,0));
  }

  // trigger exceptions
  BOOST_CHECK_THROW(atoms.InsertInto(1234, chain, 1234), promod3::Error);
  ost::mol::ChainHandle invalid_chain;
  BOOST_CHECK_THROW(atoms.InsertInto(0, invalid_chain, 0), promod3::Error);
  atoms.ClearPos(0, BB_N_INDEX);
  BOOST_CHECK_THROW(atoms.InsertInto(0, chain, 0), promod3::Error);
}

BOOST_AUTO_TEST_CASE(test_aap_bb_list_convert) {
  // setup example
  ost::mol::EntityHandle prot = LoadTestStructure("data/3DEFA.pdb");
  ost::mol::ResidueHandleList res_list = prot.GetResidueList();
  AllAtomPositions atoms(res_list);
  const uint num_residues = atoms.GetNumResidues();
  // reference bb list
  BackboneList full_bb(atoms.GetSequence(), res_list);
  BOOST_CHECK_EQUAL(full_bb.size(), num_residues);
  BOOST_CHECK_EQUAL(full_bb.GetSequence(), atoms.GetSequence());
  // check conversions
  BOOST_CHECK(atoms.ExtractBackbone(0, num_residues) == full_bb);
  BOOST_CHECK(atoms.ExtractBackbone(10, 20) == full_bb.Extract(10, 20));
  AllAtomPositions tst_all_pos(full_bb);
  BOOST_CHECK_EQUAL(tst_all_pos.GetNumResidues(), num_residues);
  for (uint res_idx = 0; res_idx < num_residues; ++res_idx) {
    const uint first_idx = tst_all_pos.GetFirstIndex(res_idx);
    const uint last_idx = tst_all_pos.GetLastIndex(res_idx);
    for (uint idx = first_idx; idx <= last_idx; ++idx) {
      if (idx - first_idx <= BB_CB_INDEX) {
        BOOST_CHECK(tst_all_pos.IsSet(idx));
      } else {
        BOOST_CHECK(!tst_all_pos.IsSet(idx));
      }
    }
  }
  BOOST_CHECK(tst_all_pos.ExtractBackbone(0, num_residues) == full_bb);
  // check exceptions
  atoms.ClearPos(15, BB_N_INDEX);
  atoms.ClearPos(20, BB_CB_INDEX);
  BOOST_CHECK_THROW(atoms.ExtractBackbone(10, 20), promod3::Error);
  BOOST_CHECK_THROW(atoms.ExtractBackbone(15, 20), promod3::Error);
  BOOST_CHECK_THROW(atoms.ExtractBackbone(10, 16), promod3::Error);
  BOOST_CHECK_NO_THROW(atoms.ExtractBackbone(10, 15));
  BOOST_CHECK_NO_THROW(atoms.ExtractBackbone(16, 25));
}

BOOST_AUTO_TEST_SUITE_END();
