// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/structure_db.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;


namespace {

  BackboneList wrap_get_bb_list_one(StructureDBPtr db,
                                    const FragmentInfo& info,
                                    const String& sequence) {
    BackboneList bb_list;
    db->FillBackbone(bb_list, info, sequence);
    return bb_list;
  }

  BackboneList wrap_get_bb_list_two(StructureDBPtr db,
                                    const ost::mol::ResidueHandle& stem_one,
                                    const ost::mol::ResidueHandle& stem_two,
                                    const FragmentInfo& info,
                                    const String& sequence) {
    BackboneList bb_list;
    db->FillBackbone(bb_list, stem_one, stem_two, info, sequence);
    return bb_list;
  }

  BackboneList wrap_get_bb_list_three(StructureDBPtr db,
                                      uint coord_idx,
                                      const String& sequence) {
    BackboneList bb_list;
    db->FillBackbone(bb_list, coord_idx, sequence);
    return bb_list;
  }

  BackboneList wrap_get_bb_list_four(StructureDBPtr db,
                                     const ost::mol::ResidueHandle& stem_one,
                                     const ost::mol::ResidueHandle& stem_two,
                                     uint coord_idx,
                                     const String& sequence) {
    BackboneList bb_list;
    db->FillBackbone(bb_list, stem_one, stem_two, coord_idx, sequence);
    return bb_list;
  }

  boost::python::list WrapGetCoordIdx(StructureDBPtr db,
                                      const String& id,
                                      const String& chain_name) {

    std::vector<int> indices = db->GetCoordIdx(id, chain_name);
    boost::python::list return_list;
    core::AppendVectorToList(indices, return_list);
    return return_list;
  }

  String WrapGetSequence(StructureDBPtr db, FragmentInfo& info) {
    return db->GetSequence(info);
  }

  String WrapGetDSSPStates(StructureDBPtr db, FragmentInfo& info) {
    return db->GetDSSPStates(info);
  }

  boost::python::list WrapGetDihedralAngles(StructureDBPtr db, FragmentInfo& info){
    std::vector<std::pair<Real,Real> > dihedrals = db->GetDihedralAngles(info);
    boost::python::list return_list;
    core::AppendVectorToList(dihedrals, return_list);
    return return_list;
  }

  boost::python::list WrapGetResidueDepths(StructureDBPtr db, FragmentInfo& info){
    std::vector<Real> depths = db->GetResidueDepths(info);
    boost::python::list return_list;
    core::AppendVectorToList(depths, return_list);
    return return_list;
  }

  boost::python::list WrapGetSolventAccessibilities(StructureDBPtr db, FragmentInfo& info){
    std::vector<int> acc = db->GetSolventAccessibilities(info);
    boost::python::list return_list;
    core::AppendVectorToList(acc, return_list);
    return return_list;
  }

  ost::seq::ProfileHandlePtr WrapGetSequenceProfile(StructureDBPtr db, FragmentInfo& info) {
    return db->GetSequenceProfile(info);
  }

  ost::seq::ProfileHandlePtr WrapGetStructureProfile(StructureDBPtr db, FragmentInfo& info) {
    return db->GetStructureProfile(info);
  }

  String WrapGetFullSequence(StructureDBPtr db, int coord_idx) {
    return db->GetSequence(coord_idx);
  }

  String WrapGetFullDSSPStates(StructureDBPtr db, int coord_idx) {
    return db->GetDSSPStates(coord_idx);
  }

  boost::python::list WrapGetFullDihedralAngles(StructureDBPtr db, int coord_idx){
    std::vector<std::pair<Real,Real> > dihedrals = db->GetDihedralAngles(coord_idx);
    boost::python::list return_list;
    core::AppendVectorToList(dihedrals, return_list);
    return return_list;
  }

  boost::python::list WrapGetFullResidueDepths(StructureDBPtr db, int coord_idx){
    std::vector<Real> depths = db->GetResidueDepths(coord_idx);
    boost::python::list return_list;
    core::AppendVectorToList(depths, return_list);
    return return_list;
  }

  boost::python::list WrapGetFullSolventAccessibilities(StructureDBPtr db, int coord_idx){
    std::vector<int> acc = db->GetSolventAccessibilities(coord_idx);
    boost::python::list return_list;
    core::AppendVectorToList(acc, return_list);
    return return_list;
  }

  ost::seq::ProfileHandlePtr WrapGetFullSequenceProfile(StructureDBPtr db, int coord_idx) {
    return db->GetSequenceProfile(coord_idx);
  }

  ost::seq::ProfileHandlePtr WrapGetFullStructureProfile(StructureDBPtr db, int coord_idx) {
    return db->GetStructureProfile(coord_idx);
  }

  StructureDBPtr WrapGetSubDB(StructureDBPtr db, 
                              const boost::python::list& indices){
    std::vector<uint> v_indices;
    core::ConvertListToVector(indices, v_indices);
    return db->GetSubDB(v_indices);
  }

  ost::seq::ProfileHandlePtr WrapGenerateStructureProfile(StructureDBPtr db,
                                                          const BackboneList& bb_list,
                                                          const list& residue_depths) {
    std::vector<Real> v_residue_depths;
    core::ConvertListToVector(residue_depths, v_residue_depths);
    return db->GenerateStructureProfile(bb_list, v_residue_depths);
  } 

  std::vector<int> WrapAddCoordinatesView(StructureDBPtr db,
                                          const String& id, 
                                          const String& chain_name,
                                          ost::mol::EntityView& ent,
                                          ost::seq::SequenceHandle& seqres,
                                          ost::seq::ProfileHandlePtr prof,
                                          bool only_longest_stretch) {
    return db->AddCoordinates(id, chain_name, ent, seqres, prof, 
                              only_longest_stretch);
  }

  std::vector<int> WrapAddCoordinatesHandle(StructureDBPtr db,
                                            const String& id, 
                                            const String& chain_name,
                                            ost::mol::EntityHandle& ent,
                                            ost::seq::SequenceHandle& seqres,
                                            ost::seq::ProfileHandlePtr prof,
                                            bool only_longest_stretch) {
    return db->AddCoordinates(id, chain_name, ent, seqres, prof, 
                              only_longest_stretch);
  }
}


void export_StructureDB(){

  class_<CoordInfo>("CoordInfo", no_init)
    .def_readonly("id", &CoordInfo::id)
    .def_readonly("chain_name", &CoordInfo::chain_name)
    .def_readonly("offset", &CoordInfo::offset)
    .def_readonly("size", &CoordInfo::size)
    .def_readonly("start_resnum", &CoordInfo::start_resnum)
    .def_readonly("shift", &CoordInfo::shift)
  ;

  class_<FragmentInfo>
    ("FragmentInfo", init<unsigned int, unsigned short, unsigned short>())
    .def_readwrite("chain_index", &FragmentInfo::chain_index)
    .def_readwrite("offset", &FragmentInfo::offset)
    .def_readwrite("length", &FragmentInfo::length)
    .def(self == self)
    .def(self != self)
  ;

  enum_<StructureDB::DBDataType>("StructureDBDataType")
    .value("All", StructureDB::All)
    .value("Minimal", StructureDB::Minimal)
    .value("Dihedrals", StructureDB::Dihedrals)
    .value("SolventAccessibilities", StructureDB::SolventAccessibilities)
    .value("ResidueDepths", StructureDB::ResidueDepths)
    .value("DSSP", StructureDB::DSSP)
    .value("AAFrequencies", StructureDB::AAFrequencies)
    .value("AAFrequenciesStruct", StructureDB::AAFrequenciesStruct)
  ;

  class_<StructureDB, boost::noncopyable> ("StructureDB", init<int>())
    .def("Load", &StructureDB::Load,
         (arg("filename")))
    .staticmethod("Load")
    .def("Save", &StructureDB::Save, (arg("filename")))
    .def("LoadPortable", &StructureDB::LoadPortable, (arg("filename")))
    .staticmethod("LoadPortable")
    .def("SavePortable", &StructureDB::SavePortable, (arg("filename")))
    .def("HasData", &StructureDB::HasData, (arg("structure_db_data_type")))
    .def("AddCoordinates", &WrapAddCoordinatesView,
         (arg("id"), arg("chain_name"), 
          arg("ent"), arg("seqres"), 
          arg("prof")=ost::seq::ProfileHandlePtr(), 
          arg("only_longest_stretch")=true))
    .def("AddCoordinates", &WrapAddCoordinatesHandle,
         (arg("id"), arg("chain_name"), 
          arg("ent"), arg("seqres"), 
          arg("prof")=ost::seq::ProfileHandlePtr(), 
          arg("only_longest_stretch")=true))
    .def("RemoveCoordinates", &StructureDB::RemoveCoordinates, 
         (arg("coord_idx")))
    .def("GetCoordIndex", &WrapGetCoordIdx,
         (arg("pdb_id"), arg("chain_name")))
    .def("GetBackboneList", &wrap_get_bb_list_one,
         (arg("fragment"), arg("sequence")=""))
    .def("GetBackboneList", &wrap_get_bb_list_two,
         (arg("stem_one"), arg("stem_two"), arg("fragment"), arg("sequence")=""))
   .def("GetBackboneList", &wrap_get_bb_list_three,
         (arg("coord_idx"), arg("sequence")=""))
    .def("GetBackboneList", &wrap_get_bb_list_four,
         (arg("stem_one"), arg("stem_two"), arg("coord_idx"), arg("sequence")=""))
    .def("GetCoordInfo", &StructureDB::GetCoordInfo,(arg("index")), 
         return_value_policy<reference_existing_object>())
    .def("GetNumCoords", &StructureDB::GetNumCoords)
    .def("PrintStatistics", &StructureDB::PrintStatistics)
    .def("GetSequence", &WrapGetSequence, (arg("fragment")))
    .def("GetDSSPStates", &WrapGetDSSPStates, (arg("fragment")))
    .def("GetDihedralAngles", &WrapGetDihedralAngles, (arg("fragment")))
    .def("GetResidueDepths", &WrapGetResidueDepths, (arg("fragment")))
    .def("GetSolventAccessibilities", &WrapGetSolventAccessibilities,
         (arg("fragment")))
    .def("GetSequenceProfile", &WrapGetSequenceProfile,
         (arg("fragment")))
    .def("GetStructureProfile", &WrapGetStructureProfile,
         (arg("fragment")))
    .def("GetSequence", &WrapGetFullSequence, (arg("coord_idx")))
    .def("GetDSSPStates", &WrapGetFullDSSPStates, (arg("coord_idx")))
    .def("GetDihedralAngles", &WrapGetFullDihedralAngles, (arg("coord_idx")))
    .def("GetResidueDepths", &WrapGetFullResidueDepths, (arg("coord_idx")))
    .def("GetSolventAccessibilities", &WrapGetFullSolventAccessibilities,
         (arg("coord_idx")))
    .def("GetSequenceProfile", &WrapGetFullSequenceProfile,
         (arg("coord_idx")))
    .def("GetStructureProfile", &WrapGetFullStructureProfile,
         (arg("fragment")))
    .def("GetStartResnum", &StructureDB::GetStartResnum, (arg("fragment")))
    .def("GenerateStructureProfile", &WrapGenerateStructureProfile,
         (arg("bb_list"), arg("residue_depths")))
    .def("SetStructureProfile", &StructureDB::SetStructureProfile,
         (arg("chain_idx"), arg("prof")))
    .def("GetSubDB", &WrapGetSubDB,(arg("indices")))
  ;

  register_ptr_to_python<StructureDBPtr>(); 
}
