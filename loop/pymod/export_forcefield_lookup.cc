// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/message.hh>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/forcefield_lookup.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

// WRAPPERS
namespace {

boost::python::list
WrapGetHarmonicBonds(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.harmonic_bonds, l);
  return l;
}
void WrapSetHarmonicBonds(ForcefieldConnectivity& ff_c, boost::python::list l) {
  core::ConvertListToVector(l, ff_c.harmonic_bonds);
}

boost::python::list
WrapGetHarmonicAngles(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.harmonic_angles, l);
  return l;
}
void WrapSetHarmonicAngles(ForcefieldConnectivity& ff_c,
                           boost::python::list l) {
  core::ConvertListToVector(l, ff_c.harmonic_angles);
}

boost::python::list
WrapGetUreyBradleyAngles(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.urey_bradley_angles, l);
  return l;
}
void WrapSetUreyBradleyAngles(ForcefieldConnectivity& ff_c,
                              boost::python::list l) {
  core::ConvertListToVector(l, ff_c.urey_bradley_angles);
}

boost::python::list
WrapGetPeriodicDihedrals(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.periodic_dihedrals, l);
  return l;
}
void WrapSetPeriodicDihedrals(ForcefieldConnectivity& ff_c,
                              boost::python::list l) {
  core::ConvertListToVector(l, ff_c.periodic_dihedrals);
}

boost::python::list
WrapGetPeriodicImpropers(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.periodic_impropers, l);
  return l;
}
void WrapSetPeriodicImpropers(ForcefieldConnectivity& ff_c,
                              boost::python::list l) {
  core::ConvertListToVector(l, ff_c.periodic_impropers);
}

boost::python::list
WrapGetHarmonicImpropers(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.harmonic_impropers, l);
  return l;
}
void WrapSetHarmonicImpropers(ForcefieldConnectivity& ff_c,
                              boost::python::list l) {
  core::ConvertListToVector(l, ff_c.harmonic_impropers);
}

boost::python::list
WrapGetLjPairs(const ForcefieldConnectivity& ff_c) {
  boost::python::list l;
  core::AppendVectorToList(ff_c.lj_pairs, l);
  return l;
}
void WrapSetLjPairs(ForcefieldConnectivity& ff_c, boost::python::list l) {
  core::ConvertListToVector(l, ff_c.lj_pairs);
}

uint (ForcefieldLookup::*WrapGetHeavyIndex)(ForcefieldAminoAcid, uint) const
  = &ForcefieldLookup::GetHeavyIndex;
uint (ForcefieldLookup::*WrapGetHeavyIndexN)(ForcefieldAminoAcid,
                                             const String&) const
  = &ForcefieldLookup::GetHeavyIndex;

uint (ForcefieldLookup::*WrapGetHydrogenIndex)(ForcefieldAminoAcid, uint) const
  = &ForcefieldLookup::GetHydrogenIndex;
uint (ForcefieldLookup::*WrapGetHydrogenIndexN)(ForcefieldAminoAcid,
                                                const String&) const
  = &ForcefieldLookup::GetHydrogenIndex;

boost::python::list
WrapGetCharges(const ForcefieldLookup& ff_lookup,
               ForcefieldAminoAcid aa, bool is_nter, bool is_cter) {
  boost::python::list l;
  core::AppendVectorToList(ff_lookup.GetCharges(aa, is_nter, is_cter), l);
  return l;
}
boost::python::list
WrapGetMasses(const ForcefieldLookup& ff_lookup,
              ForcefieldAminoAcid aa, bool is_nter, bool is_cter) {
  boost::python::list l;
  core::AppendVectorToList(ff_lookup.GetMasses(aa, is_nter, is_cter), l);
  return l;
}
boost::python::list
WrapGetSigmas(const ForcefieldLookup& ff_lookup,
              ForcefieldAminoAcid aa, bool is_nter, bool is_cter) {
  boost::python::list l;
  core::AppendVectorToList(ff_lookup.GetSigmas(aa, is_nter, is_cter), l);
  return l;
}
boost::python::list
WrapGetEpsilons(const ForcefieldLookup& ff_lookup,
                ForcefieldAminoAcid aa, bool is_nter, bool is_cter) {
  boost::python::list l;
  core::AppendVectorToList(ff_lookup.GetEpsilons(aa, is_nter, is_cter), l);
  return l;
}

void WrapSetCharges(ForcefieldLookup& ff_lookup,
                    ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                    boost::python::list l) {
  std::vector<Real> v;
  core::ConvertListToVector(l, v);
  ff_lookup.SetCharges(aa, is_nter, is_cter, v);
}
void WrapSetMasses(ForcefieldLookup& ff_lookup,
                   ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                   boost::python::list l) {
  std::vector<Real> v;
  core::ConvertListToVector(l, v);
  ff_lookup.SetMasses(aa, is_nter, is_cter, v);
}
void WrapSetSigmas(ForcefieldLookup& ff_lookup,
                   ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                   boost::python::list l) {
  std::vector<Real> v;
  core::ConvertListToVector(l, v);
  ff_lookup.SetSigmas(aa, is_nter, is_cter, v);
}
void WrapSetEpsilons(ForcefieldLookup& ff_lookup,
                     ForcefieldAminoAcid aa, bool is_nter, bool is_cter,
                     boost::python::list l) {
  std::vector<Real> v;
  core::ConvertListToVector(l, v);
  ff_lookup.SetEpsilons(aa, is_nter, is_cter, v);
}

} // anon wrapper ns

void export_ForcefieldLookup() {

  enum_<ForcefieldAminoAcid>("ForcefieldAminoAcid")
    .value("FF_ALA", FF_ALA)
    .value("FF_ARG", FF_ARG)
    .value("FF_ASN", FF_ASN)
    .value("FF_ASP", FF_ASP)
    .value("FF_GLN", FF_GLN)
    .value("FF_GLU", FF_GLU)
    .value("FF_LYS", FF_LYS)
    .value("FF_SER", FF_SER)
    .value("FF_CYS", FF_CYS)
    .value("FF_MET", FF_MET)
    .value("FF_TRP", FF_TRP)
    .value("FF_TYR", FF_TYR)
    .value("FF_THR", FF_THR)
    .value("FF_VAL", FF_VAL)
    .value("FF_ILE", FF_ILE)
    .value("FF_LEU", FF_LEU)
    .value("FF_GLY", FF_GLY)
    .value("FF_PRO", FF_PRO)
    .value("FF_HISE", FF_HISE)
    .value("FF_PHE", FF_PHE)
    .value("FF_CYS2", FF_CYS2)
    .value("FF_HISD", FF_HISD)
    .value("FF_XXX", FF_XXX)
  ;

  class_<ForcefieldBondInfo>("ForcefieldBondInfo", init<>())
    .def_readwrite("index_one", &ForcefieldBondInfo::index_one)
    .def_readwrite("index_two", &ForcefieldBondInfo::index_two)
    .def_readwrite("bond_length", &ForcefieldBondInfo::bond_length)
    .def_readwrite("force_constant", &ForcefieldBondInfo::force_constant)
  ;

  class_<ForcefieldHarmonicAngleInfo>("ForcefieldHarmonicAngleInfo", init<>())
    .def_readwrite("index_one", &ForcefieldHarmonicAngleInfo::index_one)
    .def_readwrite("index_two", &ForcefieldHarmonicAngleInfo::index_two)
    .def_readwrite("index_three", &ForcefieldHarmonicAngleInfo::index_three)
    .def_readwrite("angle", &ForcefieldHarmonicAngleInfo::angle)
    .def_readwrite("force_constant", &ForcefieldHarmonicAngleInfo::force_constant)
  ;

  class_<ForcefieldUreyBradleyAngleInfo>
    ("ForcefieldUreyBradleyAngleInfo", init<>())
    .def_readwrite("index_one", &ForcefieldUreyBradleyAngleInfo::index_one)
    .def_readwrite("index_two", &ForcefieldUreyBradleyAngleInfo::index_two)
    .def_readwrite("index_three", &ForcefieldUreyBradleyAngleInfo::index_three)
    .def_readwrite("angle", &ForcefieldUreyBradleyAngleInfo::angle)
    .def_readwrite("angle_force_constant",
                   &ForcefieldUreyBradleyAngleInfo::angle_force_constant)
    .def_readwrite("bond_length", &ForcefieldUreyBradleyAngleInfo::bond_length)
    .def_readwrite("bond_force_constant",
                   &ForcefieldUreyBradleyAngleInfo::bond_force_constant)
  ;

  class_<ForcefieldPeriodicDihedralInfo>
    ("ForcefieldPeriodicDihedralInfo", init<>())
    .def_readwrite("index_one", &ForcefieldPeriodicDihedralInfo::index_one)
    .def_readwrite("index_two", &ForcefieldPeriodicDihedralInfo::index_two)
    .def_readwrite("index_three", &ForcefieldPeriodicDihedralInfo::index_three)
    .def_readwrite("index_four", &ForcefieldPeriodicDihedralInfo::index_four)
    .def_readwrite("multiplicity",
                   &ForcefieldPeriodicDihedralInfo::multiplicity)
    .def_readwrite("phase", &ForcefieldPeriodicDihedralInfo::phase)
    .def_readwrite("force_constant",
                   &ForcefieldPeriodicDihedralInfo::force_constant)
  ;

  class_<ForcefieldHarmonicImproperInfo>
    ("ForcefieldHarmonicImproperInfo", init<>())
    .def_readwrite("index_one", &ForcefieldHarmonicImproperInfo::index_one)
    .def_readwrite("index_two", &ForcefieldHarmonicImproperInfo::index_two)
    .def_readwrite("index_three", &ForcefieldHarmonicImproperInfo::index_three)
    .def_readwrite("index_four", &ForcefieldHarmonicImproperInfo::index_four)
    .def_readwrite("angle", &ForcefieldHarmonicImproperInfo::angle)
    .def_readwrite("force_constant",
                   &ForcefieldHarmonicImproperInfo::force_constant)
  ;

  class_<ForcefieldLJPairInfo>("ForcefieldLJPairInfo", init<>())
    .def_readwrite("index_one", &ForcefieldLJPairInfo::index_one)
    .def_readwrite("index_two", &ForcefieldLJPairInfo::index_two)
    .def_readwrite("sigma", &ForcefieldLJPairInfo::sigma)
    .def_readwrite("epsilon", &ForcefieldLJPairInfo::epsilon)
  ;

  class_<ForcefieldConnectivity>("ForcefieldConnectivity", init<>())
    .add_property("harmonic_bonds", WrapGetHarmonicBonds,
                                    WrapSetHarmonicBonds)
    .add_property("harmonic_angles", WrapGetHarmonicAngles,
                                     WrapSetHarmonicAngles)
    .add_property("urey_bradley_angles", WrapGetUreyBradleyAngles,
                                         WrapSetUreyBradleyAngles)
    .add_property("periodic_dihedrals", WrapGetPeriodicDihedrals,
                                        WrapSetPeriodicDihedrals)
    .add_property("periodic_impropers", WrapGetPeriodicImpropers,
                                        WrapSetPeriodicImpropers)
    .add_property("harmonic_impropers", WrapGetHarmonicImpropers,
                                        WrapSetHarmonicImpropers)
    .add_property("lj_pairs", WrapGetLjPairs,
                              WrapSetLjPairs)
  ;

  class_<ForcefieldLookup, ForcefieldLookupPtr>
    ("ForcefieldLookup", init<>())
    .def("GetDefault", &ForcefieldLookup::GetDefault)
    .staticmethod("GetDefault")
    .def("SetDefault", &ForcefieldLookup::SetDefault, (arg("new_default")))
    .staticmethod("SetDefault")
    .def("Load", &ForcefieldLookup::Load, (arg("filename")))
    .staticmethod("Load")
    .def("Save", &ForcefieldLookup::Save, (arg("filename")))
    .def("LoadPortable", &ForcefieldLookup::LoadPortable,
         (arg("filename")))
    .staticmethod("LoadPortable")
    .def("SavePortable", &ForcefieldLookup::SavePortable,
         (arg("filename")))
    .def("LoadCHARMM", &ForcefieldLookup::LoadCHARMM)
    .staticmethod("LoadCHARMM")

    .def("GetAA", &ForcefieldLookup::GetAA, (arg("ff_aa")))
    .def("GetNumAtoms", &ForcefieldLookup::GetNumAtoms,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter")))
    .def("GetHeavyIndex", WrapGetHeavyIndex, (arg("ff_aa"), arg("atom_idx")))
    .def("GetHeavyIndex", WrapGetHeavyIndexN, (arg("ff_aa"), arg("atom_name")))
    .def("GetHydrogenIndex", WrapGetHydrogenIndex,
         (arg("ff_aa"), arg("atom_idx")))
    .def("GetHydrogenIndex", WrapGetHydrogenIndexN,
         (arg("ff_aa"), arg("atom_name")))
    .def("GetOXTIndex", &ForcefieldLookup::GetOXTIndex,
         (arg("ff_aa"), arg("is_nter")))
    
    .def("GetFudgeLJ", &ForcefieldLookup::GetFudgeLJ)
    .def("GetFudgeQQ", &ForcefieldLookup::GetFudgeQQ)
    .def("GetCharges", WrapGetCharges,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter")))
    .def("GetMasses", WrapGetMasses,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter")))
    .def("GetSigmas", WrapGetSigmas,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter")))
    .def("GetEpsilons", WrapGetEpsilons,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter")))
    .def("GetInternalConnectivity", &ForcefieldLookup::GetInternalConnectivity,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter")),
         return_value_policy<reference_existing_object>())
    .def("GetPeptideBoundConnectivity",
         &ForcefieldLookup::GetPeptideBoundConnectivity,
         (arg("ff_aa_one"), arg("ff_aa_two"), arg("is_nter"), arg("is_cter")),
         return_value_policy<reference_existing_object>())
    .def("GetDisulfidConnectivity", &ForcefieldLookup::GetDisulfidConnectivity,
         return_value_policy<reference_existing_object>())

    .def("SetFudgeLJ", &ForcefieldLookup::SetFudgeLJ, (arg("fudge")))
    .def("SetFudgeQQ", &ForcefieldLookup::SetFudgeQQ, (arg("fudge")))
    .def("SetCharges", WrapSetCharges,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter"), arg("charges")))
    .def("SetMasses", WrapSetMasses,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter"), arg("masses")))
    .def("SetSigmas", WrapSetSigmas,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter"), arg("sigmas")))
    .def("SetEpsilons", WrapSetEpsilons,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter"), arg("epsilons")))
    .def("SetInternalConnectivity", &ForcefieldLookup::SetInternalConnectivity,
         (arg("ff_aa"), arg("is_nter"), arg("is_cter"), arg("connectivity")))
    .def("SetPeptideBoundConnectivity",
         &ForcefieldLookup::SetPeptideBoundConnectivity,
         (arg("ff_aa_one"), arg("ff_aa_two"), arg("is_nter"), arg("is_cter"),
          arg("connectivity")))
    .def("SetDisulfidConnectivity", &ForcefieldLookup::SetDisulfidConnectivity,
         (arg("connectivity")))
  ;

}
