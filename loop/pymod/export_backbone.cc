// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/backbone.hh>
#include <promod3/loop/bb_trace_param.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

// WRAPPERS
namespace {

BackboneListPtr FullInitWrapper(const String& sequence,
                                const boost::python::list& l) {

  if (boost::python::len(l) == 0) return BackboneListPtr(new BackboneList);

  // check whether first item is convertible to a tuple,
  // otherwise we assume the list to be a ResidueHandleList
  boost::python::extract<boost::python::tuple> extract_tuple(l[0]);
  if (extract_tuple.check()) {
    std::vector<Real> phi_angles;
    std::vector<Real> psi_angles;
    std::vector<Real> omega_angles;
    boost::python::tuple actual_tuple;
    for(uint i = 0; i < boost::python::len(l); ++i){
      actual_tuple = boost::python::extract<boost::python::tuple>(l[i]);
      switch(boost::python::len(actual_tuple)){
        case 2:{
          phi_angles.push_back(boost::python::extract<Real>(actual_tuple[0]));
          psi_angles.push_back(boost::python::extract<Real>(actual_tuple[1]));
          omega_angles.push_back(M_PI);
          break;
        }
        case 3:{
          phi_angles.push_back(boost::python::extract<Real>(actual_tuple[0]));
          psi_angles.push_back(boost::python::extract<Real>(actual_tuple[1]));
          omega_angles.push_back(boost::python::extract<Real>(actual_tuple[2]));
          break;
        }
        default:{
          throw promod3::Error("Every tuple defining backbone dihedrals must "
                               "have two or three elements. Two elements define"
                               " a phi/psi pair assuming an idealized omega "
                               "dihedral of 180 degrees. The optional third "
                               "element explicitely defines that omega torsion.");
        }
      }
    }
    BackboneListPtr p(new BackboneList(sequence, phi_angles, psi_angles, 
                                       omega_angles));
    return p;
  } else {
    ost::mol::ResidueHandleList residues;
    core::ConvertListToVector(l, residues);
    BackboneListPtr p(new BackboneList(sequence,residues));
    return p;    
  }
}

BackboneListPtr FullInitWrapperRHList(ost::mol::ResidueHandleList& l) {
  if (l.size() == 0) return BackboneListPtr(new BackboneList);
  BackboneListPtr p(new BackboneList(l));
  return p;
}

BackboneListPtr FullInitWrapperList(list& l) {
  ost::mol::ResidueHandleList residues;
  core::ConvertListToVector(l, residues);
  BackboneListPtr p(new BackboneList(residues));
  return p;
}

BackboneListPtr FullInitWrapperSeqRHList(const String& sequence, 
                                         ost::mol::ResidueHandleList& l) {
  if (l.size() == 0) return BackboneListPtr(new BackboneList);
  BackboneListPtr p(new BackboneList(sequence,l));
  return p;    
}

BackboneListPtr WrapCopy(BackboneListPtr p){
  BackboneListPtr return_p(new BackboneList(*p));
  return return_p;
}

void InsertInto_int(BackboneListPtr p, ost::mol::ChainHandle& chain,
                    uint start_resnum) {
  p->InsertInto(chain, start_resnum);
}
void InsertInto_resnum(BackboneListPtr p, ost::mol::ChainHandle& chain,
                       const ost::mol::ResNum start_resnum) {
  p->InsertInto(chain, start_resnum);
}
void InsertInto_map(BackboneListPtr p, ost::img::MapHandle& map, 
                    Real resolution, bool high_resolution) {
  p->InsertInto(map, resolution, high_resolution);
}

void Set_all(BackboneListPtr p, uint idx, const geom::Vec3& n,
             const geom::Vec3& ca, const geom::Vec3& cb, const geom::Vec3& c,
             const geom::Vec3& o, char olc) {
  p->Set(idx, n, ca, cb, c, o, olc);
}
void Set_nocb(BackboneListPtr p, uint idx, const geom::Vec3& n,
              const geom::Vec3& ca, const geom::Vec3& c, const geom::Vec3& o,
              char olc) {
  p->Set(idx, n, ca, c, o, olc);
}
void Set_res(BackboneListPtr p, uint idx, const ost::mol::ResidueHandle& res,
             char olc) {
  p->Set(idx, res, olc);
}

void push_back_all(BackboneListPtr p, const geom::Vec3& n, const geom::Vec3& ca,
                   const geom::Vec3& cb, const geom::Vec3& c,
                   const geom::Vec3& o, char olc) {
  p->push_back(n, ca, cb, c, o, olc);
}
void push_back_nocb(BackboneListPtr p, const geom::Vec3& n, const geom::Vec3& ca,
                    const geom::Vec3& c, const geom::Vec3& o, char olc) {
  p->push_back(n, ca, c, o, olc);
}
void push_back_res(BackboneListPtr p, const ost::mol::ResidueHandle& res,
                   char olc) {
  p->push_back(res, olc);
}

void ApplyTransformOne(BackboneListPtr p, uint index, const geom::Mat4& t) {
  p->ApplyTransform(index, t);
}
void ApplyTransformRange(BackboneListPtr p, uint from, uint to,
                         const geom::Mat4& t) {
  p->ApplyTransform(from, to, t);
}
void ApplyTransformMat4(BackboneListPtr p, const geom::Mat4& t) {
  p->ApplyTransform(t);
}
void ApplyTransformTransform(BackboneListPtr p, const geom::Transform& t) {
  p->ApplyTransform(t);
}

geom::Mat4 GetTransformOne(const BackboneList& bbl, uint index,
                           const BackboneList& other, uint other_index) {
  return bbl.GetTransform(index, other, other_index);
}
geom::Mat4 GetTransformRes(const BackboneList& bbl, uint index,
                           const ost::mol::ResidueHandle& res) {
  return bbl.GetTransform(index, res);
}
geom::Mat4 GetTransformBBL(const BackboneList& bbl, const BackboneList& other) {
  return bbl.GetTransform(other);
}

void SetBackrubAllAngles(BackboneList& bbl, uint index, 
                         Real primary_rot_angle,
                         Real flanking_rot_angle_one, 
                         Real flanking_rot_angle_two){
  bbl.SetBackrub(index, primary_rot_angle, flanking_rot_angle_one, 
                 flanking_rot_angle_two);
}

void SetBackrubSingleAngle(BackboneList& bbl, uint index, 
                           Real primary_rot_angle){
  bbl.SetBackrub(index, primary_rot_angle);
}

boost::python::tuple WrapBBTraceParam(char olc) {
  Real n_ca_bond, ca_c_bond, c_n_bond, c_n_ca_angle, n_ca_c_angle, ca_c_n_angle;
  promod3::loop::BBTraceParam(olc, n_ca_bond, ca_c_bond, c_n_bond, c_n_ca_angle,
                              n_ca_c_angle, ca_c_n_angle);
  return boost::python::make_tuple(n_ca_bond, ca_c_bond, c_n_bond,
                                   c_n_ca_angle, n_ca_c_angle, ca_c_n_angle);
}

}//ns

void export_Backbone() {

  class_<BackboneList>("BackboneList", no_init)
    .def(init<>())
    .def(init<const String&>())
    .def("__init__", make_constructor(&FullInitWrapper))
    .def("__init__", make_constructor(&FullInitWrapperList))
    .def("__init__", make_constructor(&FullInitWrapperRHList))
    .def("__init__", make_constructor(&FullInitWrapperSeqRHList))
    .def("Copy", &WrapCopy)
    
    .def("ToDensity", &BackboneList::ToDensity,
         (arg("padding")=10.0, arg("sampling")=geom::Vec3(1.0,1.0,1.0),
          arg("resolution")=3.0, arg("high_resolution")=false))
    .def("ToEntity", &BackboneList::ToEntity)
    .def("InsertInto", &InsertInto_int, (arg("chain"), arg("start_resnum")))
    .def("InsertInto", &InsertInto_resnum, (arg("chain"), arg("start_resnum")))
    .def("InsertInto", &InsertInto_map,
         (arg("map"), arg("resolution")=3.0, arg("high_resolution")=false))
    .def("GetBounds", &BackboneList::GetBounds, (arg("all_atom")=true))

    .def("GetSequence", &BackboneList::GetSequence)
    .def("SetSequence", &BackboneList::SetSequence, (arg("sequence")))

    .def("Extract", &BackboneList::Extract, (arg("from"), arg("to")))
    .def("ReplaceFragment", &BackboneList::ReplaceFragment,
         (arg("sub_fragment"), arg("index"), arg("superpose_stems")))

    .def("GetN", &BackboneList::GetN, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("GetCA", &BackboneList::GetCA, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("GetCB", &BackboneList::GetCB, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("GetC", &BackboneList::GetC, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("GetO", &BackboneList::GetO, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("GetOLC", &BackboneList::GetOLC, (arg("index")))
    .def("GetAA", &BackboneList::GetAA, (arg("index")))
    .def("SetN", &BackboneList::SetN, (arg("index"), arg("pos")))
    .def("SetCA", &BackboneList::SetCA, (arg("index"), arg("pos")))
    .def("SetCB", &BackboneList::SetCB, (arg("index"), arg("pos")))
    .def("SetC", &BackboneList::SetC, (arg("index"), arg("pos")))
    .def("SetO", &BackboneList::SetO, (arg("index"), arg("pos")))
    .def("SetOLC", &BackboneList::SetOLC, (arg("index"), arg("olc")))
    .def("SetAA", &BackboneList::SetAA, (arg("index"), arg("aa")))
    .def("Set", &Set_all, (arg("index"), arg("n"), arg("ca"), arg("cb"),
                           arg("c"), arg("o"), arg("olc")))
    .def("Set", &Set_nocb, (arg("index"), arg("n"), arg("ca"), arg("c"),
                            arg("o"), arg("olc")))
    .def("Set", &Set_res, (arg("index"), arg("res"), arg("olc")='\0'))

    .def("__len__", &BackboneList::size)
    .def("resize", &BackboneList::resize, (arg("new_size")))
    .def("empty", &BackboneList::empty)
    .def("append", &push_back_all, (arg("n"), arg("ca"), arg("cb"), arg("c"),
                                    arg("o"), arg("olc")))
    .def("append", &push_back_nocb, (arg("n"), arg("ca"), arg("c"), arg("o"),
                                     arg("olc")))
    .def("append", &push_back_res, (arg("res"), arg("olc")='\0'))
    .def("clear", &BackboneList::clear)
    .def(self == self)
    .def(self != self)

    .def("ReconstructCBetaPositions", &BackboneList::ReconstructCBetaPositions)
    .def("ReconstructOxygenPositions", &BackboneList::ReconstructOxygenPositions,
         (arg("last_psi")=-0.78540))
    .def("ReconstructCStemOxygen", &BackboneList::ReconstructCStemOxygen,
         (arg("after_c_stem")))

    .def("ApplyTransform", &ApplyTransformOne,
         (arg("index"), arg("transformation_matrix")))
    .def("ApplyTransform", &ApplyTransformRange,
         (arg("from"), arg("to"), arg("transformation_matrix")))
    .def("ApplyTransform", &ApplyTransformMat4, (arg("transformation_matrix")))
    .def("ApplyTransform", &ApplyTransformTransform, (arg("transform")))
    .def("GetTransform", &GetTransformOne,
         (arg("index"), arg("other"), arg("other_index")))
    .def("GetTransform", &GetTransformRes, (arg("index"), arg("res")))
    .def("GetTransform", &GetTransformBBL, (arg("other")))
    .def("SuperposeOnto", &BackboneList::SuperposeOnto, (arg("other")))
    .def("GetTransformIterative", &BackboneList::GetTransformIterative,
         (arg("other")))
    .def("SuperposeOntoIterative", &BackboneList::SuperposeOntoIterative,
         (arg("other")))

    .def("RotateAroundPhiTorsion", &BackboneList::RotateAroundPhiTorsion,
         (arg("index"), arg("angle"), arg("sequential")=false))
    .def("RotateAroundPsiTorsion", &BackboneList::RotateAroundPsiTorsion,
         (arg("index"), arg("angle"), arg("sequential")=false))
    .def("RotateAroundOmegaTorsion", &BackboneList::RotateAroundOmegaTorsion,
         (arg("index"), arg("angle"), arg("sequential")=false))
    .def("RotateAroundPhiPsiTorsion", &BackboneList::RotateAroundPhiPsiTorsion,
         (arg("index"), arg("phi"), arg("psi"), arg("sequential")=false))
    
    .def("GetPhiTorsion", &BackboneList::GetPhiTorsion, (arg("index")))
    .def("GetPsiTorsion", &BackboneList::GetPsiTorsion, (arg("index")))
    .def("GetOmegaTorsion", &BackboneList::GetOmegaTorsion, (arg("index")))
    .def("SetPhiTorsion", &BackboneList::SetPhiTorsion,
         (arg("index"), arg("angle"), arg("sequential")=false))
    .def("SetPsiTorsion", &BackboneList::SetPsiTorsion,
         (arg("index"), arg("angle"), arg("sequential")=false))
    .def("SetOmegaTorsion", &BackboneList::SetOmegaTorsion,
         (arg("index"), arg("angle"), arg("sequential")=false))
    .def("SetPhiPsiTorsion", &BackboneList::SetPhiPsiTorsion,
         (arg("index"), arg("phi"), arg("psi"), arg("sequential")=false))

    .def("TransOmegaTorsions", &BackboneList::TransOmegaTorsions,
         (arg("thresh")=20.0/180*M_PI, arg("allow_prepro_cis")=true))

    .def("SetBackrub", &SetBackrubAllAngles,
         (arg("index"),arg("primary_rot_angle"),arg("flanking_rot_angle_one"),
          arg("flanking_rot_angle_two")))
    .def("SetBackrub", &SetBackrubSingleAngle,
         (arg("index"),arg("primary_rot_angle"),arg("scaling")=1.0))

    .def("MinCADistance", &BackboneList::MinCADistance, (arg("other")))
    .def("CARMSD", &BackboneList::CARMSD,
         (arg("other"), arg("superposed_rmsd")=false))
    .def("RMSD", &BackboneList::RMSD,
         (arg("other"), arg("superposed_rmsd")=false))
  ;
  register_ptr_to_python<BackboneListPtr>();

  def("BBTraceParam", &WrapBBTraceParam, (arg("olc")));
}
