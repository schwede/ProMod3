// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/torsion_sampler.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

namespace {

  boost::python::tuple WrapDrawNames(TorsionSamplerPtr p, 
                                     ost::conop::AminoAcid before, 
                                     ost::conop::AminoAcid central, 
                                     ost::conop::AminoAcid after){
    std::pair<Real,Real> angles = p->Draw(before,central,after);
    return boost::python::make_tuple(angles.first,angles.second);
  }

  boost::python::tuple WrapDrawIndex(TorsionSamplerPtr p, uint index){
    std::pair<Real,Real> angles = p->Draw(index);
    return boost::python::make_tuple(angles.first,angles.second);
  }

  Real WrapDrawPhiGivenPsiNames(TorsionSamplerPtr p, 
                                ost::conop::AminoAcid before, 
                                ost::conop::AminoAcid center, 
                                ost::conop::AminoAcid after, Real psi){
    return p->DrawPhiGivenPsi(before,center,after,psi);
  }

  Real WrapDrawPhiGivenPsiIndex(TorsionSamplerPtr p, uint index, Real psi){
    return p->DrawPhiGivenPsi(index,psi);
  }

  Real WrapDrawPsiGivenPhiNames(TorsionSamplerPtr p, 
                                ost::conop::AminoAcid before, 
                                ost::conop::AminoAcid center, 
                                ost::conop::AminoAcid after, Real phi){
    return p->DrawPsiGivenPhi(before,center,after,phi);
  }

  Real WrapDrawPsiGivenPhiIndex(TorsionSamplerPtr p, uint index, Real phi){
    return p->DrawPsiGivenPhi(index,phi);
  }

  Real WrapGetProbabilityIndex(TorsionSamplerPtr p, uint histogram_index, Real phi, Real psi){
    return p->GetProbability(histogram_index,std::make_pair(phi,psi));
  }

  Real WrapGetProbabilityNames(TorsionSamplerPtr p, 
                               ost::conop::AminoAcid before, 
                               ost::conop::AminoAcid central, 
                               ost::conop::AminoAcid after, 
                               Real phi, Real psi){
    return p->GetProbability(before,central,after,std::make_pair(phi,psi));
  }

  Real WrapGetPhiProbabilityGivenPsiIndex(TorsionSamplerPtr p, uint histogram_index, Real phi, Real psi){
    return p->GetPhiProbabilityGivenPsi(histogram_index,phi,psi);
  }

  Real WrapGetPhiProbabilityGivenPsiNames(TorsionSamplerPtr p, 
                                          ost::conop::AminoAcid before, 
                                          ost::conop::AminoAcid central, 
                                          ost::conop::AminoAcid after, 
                                          Real phi, Real psi){
    return p->GetPhiProbabilityGivenPsi(before,central,after,phi,psi);
  }

  Real WrapGetPsiProbabilityGivenPhiIndex(TorsionSamplerPtr p, uint histogram_index, Real phi, Real psi){
    return p->GetPsiProbabilityGivenPhi(histogram_index,psi,phi);
  }

  Real WrapGetPsiProbabilityGivenPhiNames(TorsionSamplerPtr p, 
                                          ost::conop::AminoAcid before, 
                                          ost::conop::AminoAcid central, 
                                          ost::conop::AminoAcid after, 
                                          Real phi, Real psi){
    return p->GetPsiProbabilityGivenPhi(before,central,after,psi,phi);
  }

  TorsionSamplerPtr WrapInit(list group_definitions, int bin_size, int seed) {
    std::vector<String> v_group_definitions;
    core::ConvertListToVector(group_definitions, v_group_definitions);
    return TorsionSamplerPtr(new TorsionSampler(v_group_definitions, bin_size,
                                                seed));
  }

  boost::python::list WrapGetHistogramIndices(TorsionSamplerPtr p,
                                              const String& sequence) {
    std::vector<uint> indices = p->GetHistogramIndices(sequence);
    boost::python::list ret_list;
    core::AppendVectorToList(indices, ret_list);
    return ret_list;
  }

}

void export_TorsionSampler(){
  class_<TorsionSampler>("TorsionSampler", no_init)
    .def("__init__",make_constructor(&WrapInit))
    .def("Load", &TorsionSampler::Load, (arg("filename"),arg("seed"))).staticmethod("Load")
    .def("Save", &TorsionSampler::Save, (arg("filename")))
    .def("LoadPortable", &TorsionSampler::LoadPortable, (arg("filename"),arg("seed"))).staticmethod("LoadPortable")
    .def("SavePortable", &TorsionSampler::SavePortable, (arg("filename")))
    .def("ExtractStatistics", &TorsionSampler::ExtractStatistics, (arg("ent")))
    .def("Draw", &WrapDrawNames, (arg("before"),arg("central"),arg("after")))
    .def("Draw", &WrapDrawIndex, (arg("index")))
    .def("DrawPhiGivenPsi", &WrapDrawPhiGivenPsiNames,(arg("before"),arg("central"),arg("after"),arg("psi")))
    .def("DrawPhiGivenPsi", &WrapDrawPhiGivenPsiIndex,(arg("index"),arg("psi")))
    .def("DrawPsiGivenPhi", &WrapDrawPsiGivenPhiNames,(arg("before"),arg("central"),arg("after"),arg("phi")))
    .def("DrawPsiGivenPhi", &WrapDrawPsiGivenPhiIndex,(arg("index"),arg("phi")))
    .def("GetProbability", & WrapGetProbabilityIndex, (arg("index"),arg("phi"),arg("psi")))
    .def("GetProbability", & WrapGetProbabilityNames, (arg("before"),arg("central"),arg("after"),arg("phi"),arg("psi")))
    .def("GetPhiProbabilityGivenPsi", & WrapGetPhiProbabilityGivenPsiIndex, (arg("index"),arg("phi"),arg("psi")))
    .def("GetPhiProbabilityGivenPsi", & WrapGetPhiProbabilityGivenPsiNames, (arg("before"),arg("central"),arg("after"),arg("phi"),arg("psi")))
    .def("GetPsiProbabilityGivenPhi", & WrapGetPsiProbabilityGivenPhiIndex, (arg("index"),arg("psi"),arg("phi")))
    .def("GetPsiProbabilityGivenPhi", & WrapGetPsiProbabilityGivenPhiNames, (arg("before"),arg("central"),arg("after"),arg("psi"),arg("phi")))
    .def("GetHistogramIndex",&TorsionSampler::GetHistogramIndex, (arg("before"),arg("central"),arg("after")))
    .def("GetHistogramIndices",&WrapGetHistogramIndices,(arg("sequence")))
    .def("GetBinsPerDimension",&TorsionSampler::GetBinsPerDimension)
    .def("GetBinSize",&TorsionSampler::GetBinSize)
    .def("UpdateDistributions", &TorsionSampler::UpdateDistributions)
  ;
  register_ptr_to_python<TorsionSamplerPtr>(); 

  class_<TorsionSamplerList>("TorsionSamplerList", no_init)
    .def(vector_indexing_suite<TorsionSamplerList>()) 
  ;
}
