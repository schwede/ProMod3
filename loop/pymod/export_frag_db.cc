// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/frag_db.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;


namespace{
  FragDBPtr WrapInit(Real dist_bin_s, int ang_bin_s){
    return FragDBPtr(new FragDB(dist_bin_s,ang_bin_s,false));
  }

  boost::python::list WrapSearchDB(FragDBPtr p,
                                   const ost::mol::ResidueHandle& n_stem, 
                                   const ost::mol::ResidueHandle& c_stem,
                                   uint frag_size,
                                   uint extra_bins) {
    std::vector<FragmentInfo> fragments;
    p->SearchDB(n_stem, c_stem, frag_size, fragments, extra_bins);
    boost::python::list return_list;
    core::AppendVectorToList(fragments, return_list);

    return return_list;
  }

  int GetNumStemPairs(FragDBPtr p){ return p->GetNumStemPairs(); }
  int GetNumStemPairsGivenLength(FragDBPtr p, uint length) {return p->GetNumStemPairs(length); }

  int GetNumFragments(FragDBPtr p){ return p->GetNumFragments(); }
  int GetNumFragmentsGivenLength(FragDBPtr p, uint length) {return p->GetNumFragments(length); }
}

void export_FragDB(){
  //note, that only basic functionality of the frag database is exported...
  class_<FragDB, boost::noncopyable> ("FragDB", no_init)
    .def("__init__", make_constructor(&WrapInit))
    .def("Load", &FragDB::Load, (arg("filename")))
    .staticmethod("Load")
    .def("Save", &FragDB::Save, (arg("filename")))
    .def("LoadPortable", &FragDB::LoadPortable, (arg("filename")))
    .staticmethod("LoadPortable")
    .def("SavePortable", &FragDB::SavePortable, (arg("filename")))
    .def("GetAngularBinSize", &FragDB::GetAngularBinSize)
    .def("GetDistBinSize", &FragDB::GetDistBinSize)
    .def("AddFragments", &FragDB::AddFragments,
         (arg("fragment_length"), arg("rmsd_cutoff"), arg("structure_db")))
    .def("PrintStatistics", &FragDB::PrintStatistics)
    .def("GetNumStemPairs", &GetNumStemPairs)
    .def("GetNumStemPairs", &GetNumStemPairsGivenLength,(arg("loop_length")))
    .def("GetNumFragments", &GetNumFragments)
    .def("GetNumFragments", &GetNumFragmentsGivenLength,(arg("loop_length")))
    .def("HasFragLength", &FragDB::HasFragLength, (arg("loop_length")))
    .def("MaxFragLength", &FragDB::MaxFragLength)
    .def("SearchDB", &WrapSearchDB,
         (arg("n_stem"), arg("c_stem"), arg("frag_size"), arg("extra_bins")=0))
  ;

  register_ptr_to_python<FragDBPtr>(); 
  
}

