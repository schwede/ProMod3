// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/all_atom_env.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

// WRAPPERS
namespace {

AllAtomEnvPtr WrapInitSeq(const ost::seq::SequenceHandle& seqres) {
  ost::seq::SequenceList seq_list = ost::seq::CreateSequenceList();
  seq_list.AddSequence(seqres);
  AllAtomEnvPtr env(new AllAtomEnv(seq_list));
  return env;
}

AllAtomEnvPtr WrapInitSeqList(const ost::seq::SequenceList& seqres) {
  AllAtomEnvPtr env(new AllAtomEnv(seqres));
  return env;
}

AllAtomEnvPtr WrapInitStr(const String& seqres) {
  ost::seq::SequenceList seq_list = ost::seq::CreateSequenceList();
  ost::seq::SequenceHandle s = ost::seq::CreateSequence("A", seqres);
  seq_list.AddSequence(s);
  AllAtomEnvPtr env(new AllAtomEnv(seq_list));
  return env;
}

AllAtomEnvPtr WrapInitStrList(const boost::python::list& seqres) {
  ost::seq::SequenceList seq_list = ost::seq::CreateSequenceList();
  for (uint i = 0; i < boost::python::len(seqres); ++i) {
    String s = boost::python::extract<String>(seqres[i]);
    ost::seq::SequenceHandle sh = ost::seq::CreateSequence("A", s);
    seq_list.AddSequence(sh);
  }
  AllAtomEnvPtr env(new AllAtomEnv(seq_list));
  return env;
}

void (AllAtomEnv::*WrapSetEnvironment)(const AllAtomPositions&, uint, uint)
                                      = &AllAtomEnv::SetEnvironment;

void WrapSetEnvironmentResNum(AllAtomEnvPtr env,
                              const AllAtomPositions& new_pos,
                              const ost::mol::ResNum& start_resnum,
                              uint chain_index) {
  uint num = start_resnum.GetNum();
  env->SetEnvironment(new_pos, num, chain_index);
}

void (AllAtomEnv::*WrapSetEnvironmentEnvPos)(const AllAtomEnvPositions&)
                                            = &AllAtomEnv::SetEnvironment;

void (AllAtomEnv::*WrapSetEnvironmentBB)(const BackboneList&, uint, uint)
                                        = &AllAtomEnv::SetEnvironment;

void WrapSetEnvironmentBBResNum(AllAtomEnvPtr env,
                                const BackboneList& bb_list,
                                const ost::mol::ResNum& start_resnum,
                                uint chain_index) {
  uint num = start_resnum.GetNum();
  env->SetEnvironment(bb_list, num, chain_index);
}

AllAtomEnvPositionsPtr (AllAtomEnv::*WrapGetEnvironment)(uint, uint, uint)
                                                 = &AllAtomEnv::GetEnvironment;

boost::python::list WrapGetResIndices(const AllAtomEnvPositions& all_env_pos) {
  boost::python::list return_list;
  core::AppendVectorToList(all_env_pos.res_indices, return_list);
  return return_list;
}

void WrapSetResIndices(AllAtomEnvPositions& all_env_pos,
                       const boost::python::list& res_idx_list) {
  core::ConvertListToVector(res_idx_list, all_env_pos.res_indices);
}

} // anon wrapper ns

void export_AllAtomEnv() {

  class_<AllAtomEnv, AllAtomEnvPtr>("AllAtomEnv", no_init)
    .def("__init__", make_constructor(&WrapInitSeq))
    .def("__init__", make_constructor(&WrapInitSeqList))
    .def("__init__", make_constructor(&WrapInitStr))
    .def("__init__", make_constructor(&WrapInitStrList))
    .def("SetInitialEnvironment", &AllAtomEnv::SetInitialEnvironment,
         (arg("env_structure")))
    .def("SetEnvironment", WrapSetEnvironment,
         (arg("new_pos"), arg("start_resnum"), arg("chain_idx") = 0))
    .def("SetEnvironment", WrapSetEnvironmentResNum,
         (arg("new_pos"), arg("start_resnum"), arg("chain_idx") = 0))
    .def("SetEnvironment", WrapSetEnvironmentEnvPos, (arg("new_env_pos")))
    .def("SetEnvironment", WrapSetEnvironmentBB,
         (arg("bb_list"), arg("start_resnum"), arg("chain_idx") = 0))
    .def("SetEnvironment", WrapSetEnvironmentBBResNum,
         (arg("bb_list"), arg("start_resnum"), arg("chain_idx") = 0))
    .def("ClearEnvironment", &AllAtomEnv::ClearEnvironment,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))
    .def("GetEnvironment", WrapGetEnvironment,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx") = 0))
    .def("GetSeqres", &AllAtomEnv::GetSeqres)
    .def("GetAllAtomPositions", &AllAtomEnv::GetAllPosData)
  ;

  class_<AllAtomEnvPositions, AllAtomEnvPositionsPtr>
    ("AllAtomEnvPositions", init<>())
    .def_readwrite("all_pos", &AllAtomEnvPositions::all_pos)
    .add_property("res_indices", &WrapGetResIndices, &WrapSetResIndices)
  ;

}
