// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>

void export_FragDB();
void export_StructureDB();
void export_TorsionSampler();
void export_Backbone();
void export_LoopObjectLoader();
void export_Fragger();
void export_PsipredPrediction();
void export_AllAtomPositions();
void export_AllAtomEnv();
void export_AminoAcidAtoms();
void export_ForcefieldLookup();
void export_MmSystemCreator();

using namespace boost::python;

BOOST_PYTHON_MODULE(_loop)
{
  export_FragDB(); 
  export_StructureDB();
  export_TorsionSampler();
  export_Backbone();
  export_LoopObjectLoader();
  export_Fragger();
  export_PsipredPrediction();
  export_AllAtomPositions();
  export_AllAtomEnv();
  export_AminoAcidAtoms();
  export_ForcefieldLookup();
  export_MmSystemCreator();
}
