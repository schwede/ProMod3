// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/fragger.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

namespace{

  BackboneList& wrap_GetItem(Fragger& fragger, uint index){
    if(index >= fragger.size()){
      throw promod3::Error("Invalid Index observed!");
    }
    return fragger[index];
  }

  FragmentInfo wrap_GetFragmentInfo(Fragger& fragger, uint index){
    if(index >= fragger.size()){
      throw promod3::Error("Invalid Index observed!");
    }
    return fragger.GetFragmentInfo(index);    
  }

  Real wrap_GetScore(Fragger& fragger, uint index){
    if(index >= fragger.size()){
      throw promod3::Error("Invalid Index observed!");
    }
    return fragger.GetScore(index);        
  }

  Real wrap_GetSingleParameterScore(Fragger& fragger, uint parameter_index, uint index){
    if(parameter_index >= fragger.parameter_size()){
      std::stringstream ss;
      ss << "Provided "<<parameter_index<<" as parameter index for a Fragger with ";
      ss << fragger.parameter_size()<<" parameters!";
      throw promod3::Error(ss.str());
    }
    if(index >= fragger.size()){
      throw promod3::Error("Invalid Index observed!");
    }
    return fragger.GetScore(parameter_index,index);
  }

  void WrapTorsionSingle(Fragger& fragger, Real w,
                         TorsionSamplerPtr t_s, const String& before,
                         const String& after){
    fragger.AddTorsionProbabilityParameters(w,t_s,before,after);
  }
  
  void WrapTorsionList(Fragger& fragger, Real w,
                       const boost::python::list& t_s, const String& before,
                       const String& after){
    TorsionSamplerList v_t_s;
    core::ConvertListToVector(t_s, v_t_s);
    fragger.AddTorsionProbabilityParameters(w,v_t_s,before,after);
  }

  FraggerPtr fm_getitem(FraggerMap& fm, int i) { return fm[i]; }
  void fm_setitem(FraggerMap& fm, int i, FraggerPtr item) { fm[i] = item; }
}

void export_Fragger() {

  class_<Fragger, FraggerPtr>("Fragger", init<String>())
    .def("AddSeqIDParameters",&Fragger::AddSeqIDParameters,(arg("weight")))
    .def("AddSeqSimParameters",&Fragger::AddSeqSimParameters,(arg("weight"),arg("subst_matrix")))
    .def("AddSSAgreeParameters",&Fragger::AddSSAgreeParameters,(arg("weight"),arg("psipred_prediction")))
    .def("AddTorsionProbabilityParameters",&WrapTorsionSingle,(arg("weight"),arg("torsion_sampler"),arg("aa_before")="ALA",arg("aa_after")="ALA"))
    .def("AddTorsionProbabilityParameters",&WrapTorsionList,(arg("weight"),arg("torsion_sampler_list"),arg("aa_before")="ALA",arg("aa_after")="ALA"))
    .def("AddSequenceProfileParameters",&Fragger::AddSequenceProfileParameters,(arg("weight"),arg("prof")))
    .def("AddStructureProfileParameters",&Fragger::AddStructureProfileParameters,(arg("weight"),arg("prof")))
    .def("Fill",&Fragger::Fill,(arg("structure_db"),arg("max_rmsd"),arg("num_fragments")))
    .def("__len__", &Fragger::size)
    .def("__getitem__", &wrap_GetItem,return_value_policy<reference_existing_object>())
    .def("GetFragmentInfo",&wrap_GetFragmentInfo,(arg("index")))
    .def("GetScore",&wrap_GetScore,(arg("index")))
    .def("GetScore",&wrap_GetSingleParameterScore,(arg("parameter_index"),arg("index")))
  ;

  class_<FraggerMap, FraggerMapPtr>("FraggerMap", init<>())
    .def("Load", &FraggerMap::Load, (arg("filename"), arg("db")))
    .staticmethod("Load")
    .def("Save", &FraggerMap::Save, (arg("filename")))
    .def("LoadBB", &FraggerMap::LoadBB, (arg("filename")))
    .staticmethod("LoadBB")
    .def("SaveBB", &FraggerMap::SaveBB, (arg("filename")))
    .def("Contains", &FraggerMap::Contains, (arg("id")))
    .def("__getitem__", &fm_getitem, (arg("id")))
    .def("__setitem__", &fm_setitem, (arg("id"), arg("fragger")))
  ;
  
  class_<FraggerList>("FraggerList", no_init)
    .def(vector_indexing_suite<FraggerList>()) 
  ;
}
