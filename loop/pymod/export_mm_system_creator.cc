// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/mm_system_creator.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

// WRAPPERS
namespace {

boost::python::list
WrapGetDisulfidBridges(const MmSystemCreator& mm_sys,
                       const AllAtomPositions& all_pos,
                       const boost::python::list& res_idx_list) {
  // get indices
  std::vector<uint> res_indices;
  core::ConvertListToVector(res_idx_list, res_indices);
  // get bridges and convert into list
  MmSystemCreator::DisulfidBridgeVector bridges
   = mm_sys.GetDisulfidBridges(all_pos, res_indices);
  boost::python::list return_list;
  core::AppendVectorToList(bridges, return_list);
  return return_list;
}

void ConvertBridges(const boost::python::list& bridge_list,
                    MmSystemCreator::DisulfidBridgeVector& bridges) {
  // special conversion for bridges (list of tuples)
  const uint num_bridges = boost::python::len(bridge_list);
  bridges.resize(num_bridges);
  for (uint i = 0; i < num_bridges; ++i) {
    boost::python::extract<boost::python::tuple> extract_tuple(bridge_list[i]);
    if (!extract_tuple.check()) {
      throw promod3::Error("Invalid bridge list: Must contain tuples!");
    }
    if (boost::python::len(bridge_list[i]) != uint(2)) {
      throw promod3::Error("Invalid bridge list: Tuples must contain two "
                           "elements!");
    }
    bridges[i].first = boost::python::extract<uint>(bridge_list[i][0]);
    bridges[i].second = boost::python::extract<uint>(bridge_list[i][1]);
  }
}

void WrapSetupSystem(MmSystemCreator& mm_sys,
                     const AllAtomPositions& all_pos,
                     const boost::python::list& res_idx_list,
                     uint loop_length, const boost::python::list& is_n_ter_list,
                     const boost::python::list& is_c_ter_list,
                     const boost::python::list& bridge_list) {
  // get vectors
  std::vector<uint> res_indices;
  std::vector<bool> is_n_ter;
  std::vector<bool> is_c_ter;
  MmSystemCreator::DisulfidBridgeVector bridges;
  core::ConvertListToVector(res_idx_list, res_indices);
  core::ConvertListToVector(is_n_ter_list, is_n_ter);
  core::ConvertListToVector(is_c_ter_list, is_c_ter);
  ConvertBridges(bridge_list, bridges);
  // finally setup system
  mm_sys.SetupSystem(all_pos, res_indices, loop_length, is_n_ter, is_c_ter,
                     bridges);
}

void WrapSetupSystemMulti(MmSystemCreator& mm_sys,
                          const AllAtomPositions& all_pos,
                          const boost::python::list& res_idx_list,
                          const boost::python::list& loop_start_indices_list,
                          const boost::python::list& loop_lengths_list,
                          const boost::python::list& is_n_ter_list,
                          const boost::python::list& is_c_ter_list,
                          const boost::python::list& bridge_list) {
  // get vectors
  std::vector<uint> res_indices;
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  std::vector<bool> is_n_ter;
  std::vector<bool> is_c_ter;
  MmSystemCreator::DisulfidBridgeVector bridges;
  core::ConvertListToVector(res_idx_list, res_indices);
  core::ConvertListToVector(loop_start_indices_list, loop_start_indices);
  core::ConvertListToVector(loop_lengths_list, loop_lengths);
  core::ConvertListToVector(is_n_ter_list, is_n_ter);
  core::ConvertListToVector(is_c_ter_list, is_c_ter);
  ConvertBridges(bridge_list, bridges);
  // finally setup system
  mm_sys.SetupSystem(all_pos, res_indices, loop_start_indices, loop_lengths,
                     is_n_ter, is_c_ter, bridges);
}

void WrapUpdatePositions(MmSystemCreator& mm_sys,
                         const AllAtomPositions& all_pos,
                         const boost::python::list& res_idx_list) {
  // get indices
  std::vector<uint> res_indices;
  core::ConvertListToVector(res_idx_list, res_indices);
  // update positions
  mm_sys.UpdatePositions(all_pos, res_indices);
}

void WrapExtractLoopPositions(MmSystemCreator& mm_sys,
                              AllAtomPositions& loop_pos) {
  mm_sys.ExtractLoopPositions(loop_pos);
}

void WrapExtractLoopPositionsIndexed(MmSystemCreator& mm_sys,
                                     AllAtomPositions& out_pos,
                                     const boost::python::list& res_idx_list) {
  // get indices
  std::vector<uint> res_indices;
  core::ConvertListToVector(res_idx_list, res_indices);
  // update positions
  mm_sys.ExtractLoopPositions(out_pos, res_indices);
}

boost::python::list WrapGetLoopStartIndices(const MmSystemCreator& mm_sys) {
  // convert into list
  boost::python::list return_list;
  core::AppendVectorToList(mm_sys.GetLoopStartIndices(), return_list);
  return return_list;
}

boost::python::list WrapGetLoopLengths(const MmSystemCreator& mm_sys) {
  // convert into list
  boost::python::list return_list;
  core::AppendVectorToList(mm_sys.GetLoopLengths(), return_list);
  return return_list;
}

boost::python::list WrapGetFfAa(const MmSystemCreator& mm_sys) {
  // convert into list
  boost::python::list return_list;
  core::AppendVectorToList(mm_sys.GetForcefieldAminoAcids(), return_list);
  return return_list;
}

boost::python::list WrapGetIndexing(const MmSystemCreator& mm_sys) {
  // convert into list
  boost::python::list return_list;
  core::AppendVectorToList(mm_sys.GetIndexing(), return_list);
  return return_list;
}

} // anon wrapper ns

void export_MmSystemCreator() {

  class_<MmSystemCreator, MmSystemCreatorPtr>("MmSystemCreator", no_init)
    .def(init<ForcefieldLookupPtr, bool, bool, Real, bool>(
         (arg("ff_lookup"), arg("fix_surrounding_hydrogens")=true,
          arg("kill_electrostatics")=false, arg("nonbonded_cutoff")=8,
          arg("inaccurate_pot_energy")=false)))
    .def("GetDisulfidBridges", WrapGetDisulfidBridges,
         (arg("all_pos"), arg("res_indices")))
    .def("SetupSystem", WrapSetupSystem,
         (arg("all_pos"), arg("res_indices"), arg("loop_length"),
          arg("is_n_ter"), arg("is_c_ter"), arg("disulfid_bridges")))
    .def("SetupSystem", WrapSetupSystemMulti,
         (arg("all_pos"), arg("res_indices"), arg("loop_start_indices"),
          arg("loop_lengths"), arg("is_n_ter"), arg("is_c_ter"),
          arg("disulfid_bridges")))
    .def("UpdatePositions", WrapUpdatePositions,
         (arg("all_pos"), arg("res_indices")))
    .def("ExtractLoopPositions", WrapExtractLoopPositions, (arg("loop_pos")))
    .def("ExtractLoopPositions", WrapExtractLoopPositionsIndexed,
         (arg("out_pos"), arg("res_indices")))
    .def("GetSimulation", &MmSystemCreator::GetSimulation)
    .def("GetNumResidues", &MmSystemCreator::GetNumResidues)
    .def("GetNumLoopResidues", &MmSystemCreator::GetNumLoopResidues)
    .def("GetLoopStartIndices", WrapGetLoopStartIndices)
    .def("GetLoopLengths", WrapGetLoopLengths)
    .def("GetForcefieldAminoAcids", WrapGetFfAa)
    .def("GetIndexing", WrapGetIndexing)
    .def("GetCpuPlatformSupport", &MmSystemCreator::GetCpuPlatformSupport)
    .def("SetCpuPlatformSupport", &MmSystemCreator::SetCpuPlatformSupport,
         (arg("cpu_platform_support")))
  ;

}
