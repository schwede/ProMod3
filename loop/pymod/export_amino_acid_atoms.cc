// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/message.hh>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/amino_acid_atoms.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

// WRAPPERS
namespace {

char WrapGetOLC(ost::conop::AminoAcid aa) {
  return AminoAcidLookup::GetInstance().GetOLC(aa);
}
AminoAcidAtom WrapGetAAA(ost::conop::AminoAcid aa, uint atom_idx) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  if (atom_idx >= lookup.GetNumAtoms(aa)) {
    throw promod3::Error("Atom index is out of bounds!");
  }
  return lookup.GetAAA(aa, atom_idx);
}
AminoAcidAtom WrapGetAAAName(ost::conop::AminoAcid aa, const String& aname) {
  return AminoAcidLookup::GetInstance().GetAAA(aa, aname);
}
AminoAcidHydrogen WrapGetAAH(ost::conop::AminoAcid aa, uint atom_idx) {
  const AminoAcidLookup& lookup = AminoAcidLookup::GetInstance();
  if (atom_idx >= lookup.GetNumHydrogens(aa)) {
    throw promod3::Error("Atom index is out of bounds!");
  }
  return lookup.GetAAH(aa, atom_idx);
}
AminoAcidHydrogen WrapGetAAHName(ost::conop::AminoAcid aa, const String& aname) {
  return AminoAcidLookup::GetInstance().GetAAH(aa, aname);
}
uint WrapGetIndex(ost::conop::AminoAcid aa, const String& aname) {
  return AminoAcidLookup::GetInstance().GetIndex(aa, aname);
}
uint WrapGetHydrogenIndex(ost::conop::AminoAcid aa, const String& aname) {
  return AminoAcidLookup::GetInstance().GetHydrogenIndex(aa, aname);
}
uint WrapGetNumAtoms(ost::conop::AminoAcid aa) {
  return AminoAcidLookup::GetInstance().GetNumAtoms(aa);
}
uint WrapGetMaxNumAtoms() {
  return AminoAcidLookup::GetInstance().GetMaxNumAtoms();
}
uint WrapGetNumHydrogens(ost::conop::AminoAcid aa) {
  return AminoAcidLookup::GetInstance().GetNumHydrogens(aa);
}
uint WrapGetMaxNumHydrogens() {
  return AminoAcidLookup::GetInstance().GetMaxNumHydrogens();
}
ost::conop::AminoAcid WrapGetAA(AminoAcidAtom aaa) {
  return AminoAcidLookup::GetInstance().GetAA(aaa);
}
ost::conop::AminoAcid WrapGetHydrogenAA(AminoAcidHydrogen aah) {
  return AminoAcidLookup::GetInstance().GetAA(aah);
}
String WrapGetAtomName(AminoAcidAtom aaa) {
  return AminoAcidLookup::GetInstance().GetAtomName(aaa);
}
String WrapGetAtomHydrogenName(AminoAcidHydrogen aah) {
  return AminoAcidLookup::GetInstance().GetAtomName(aah);
}
String WrapGetAtomNameCharmm(AminoAcidAtom aaa) {
  return AminoAcidLookup::GetInstance().GetAtomNameCharmm(aaa);
}
String WrapGetAtomHydrogenNameCharmm(AminoAcidHydrogen aah) {
  return AminoAcidLookup::GetInstance().GetAtomNameCharmm(aah);
}
String WrapGetAtomNameAmber(AminoAcidAtom aaa) {
  return AminoAcidLookup::GetInstance().GetAtomNameAmber(aaa);
}
String WrapGetAtomHydrogenNameAmber(AminoAcidHydrogen aah) {
  return AminoAcidLookup::GetInstance().GetAtomNameAmber(aah);
}
String WrapGetElement(AminoAcidAtom aaa) {
  return AminoAcidLookup::GetInstance().GetElement(aaa);
}

boost::python::list WrapGetBonds(ost::conop::AminoAcid aa) {
  const std::vector<BondInfo>& bonds
    = AminoAcidLookup::GetInstance().GetBonds(aa);
  boost::python::list result;
  core::AppendVectorToList(bonds, result);
  return result; 
}

uint WrapGetAnchorAtomIndex(AminoAcidHydrogen aah) {
  return AminoAcidLookup::GetInstance().GetAnchorAtomIndex(aah);
}

uint WrapGetHNIndex(ost::conop::AminoAcid aa) {
  int idx = AminoAcidLookup::GetInstance().GetHNIndex(aa);
  if (idx < 0) {
    std::stringstream ss;
    ss << "There is no HN atom in " << ost::conop::AminoAcidToResidueName(aa);
    throw promod3::Error(ss.str());
  } else {
    return uint(idx);
  }
}
uint WrapGetH1Index(ost::conop::AminoAcid aa) {
  int idx = AminoAcidLookup::GetInstance().GetH1Index(aa);
  if (idx < 0) {
    std::stringstream ss;
    ss << "There is no H1 atom in " << ost::conop::AminoAcidToResidueName(aa);
    throw promod3::Error(ss.str());
  } else {
    return uint(idx);
  }
}
uint WrapGetH2Index(ost::conop::AminoAcid aa) {
  int idx = AminoAcidLookup::GetInstance().GetH2Index(aa);
  if (idx < 0) {
    std::stringstream ss;
    ss << "There is no H2 atom in " << ost::conop::AminoAcidToResidueName(aa);
    throw promod3::Error(ss.str());
  } else {
    return uint(idx);
  }
}
uint WrapGetH3Index(ost::conop::AminoAcid aa) {
  int idx = AminoAcidLookup::GetInstance().GetH3Index(aa);
  if (idx < 0) {
    std::stringstream ss;
    ss << "There is no H3 atom in " << ost::conop::AminoAcidToResidueName(aa);
    throw promod3::Error(ss.str());
  } else {
    return uint(idx);
  }
}

} // anon wrapper ns

void export_AminoAcidAtoms() {

  enum_<AminoAcidAtom>("AminoAcidAtom")
    .value("ALA_N", ALA_N)
    .value("ALA_CA", ALA_CA)
    .value("ALA_C", ALA_C)
    .value("ALA_O", ALA_O)
    .value("ALA_CB", ALA_CB)
    .value("ARG_N", ARG_N)
    .value("ARG_CA", ARG_CA)
    .value("ARG_C", ARG_C)
    .value("ARG_O", ARG_O)
    .value("ARG_CB", ARG_CB)
    .value("ARG_CG", ARG_CG)
    .value("ARG_CD", ARG_CD)
    .value("ARG_NE", ARG_NE)
    .value("ARG_CZ", ARG_CZ)
    .value("ARG_NH1", ARG_NH1)
    .value("ARG_NH2", ARG_NH2)
    .value("ASN_N", ASN_N)
    .value("ASN_CA", ASN_CA)
    .value("ASN_C", ASN_C)
    .value("ASN_O", ASN_O)
    .value("ASN_CB", ASN_CB)
    .value("ASN_CG", ASN_CG)
    .value("ASN_OD1", ASN_OD1)
    .value("ASN_ND2", ASN_ND2)
    .value("ASP_N", ASP_N)
    .value("ASP_CA", ASP_CA)
    .value("ASP_C", ASP_C)
    .value("ASP_O", ASP_O)
    .value("ASP_CB", ASP_CB)
    .value("ASP_CG", ASP_CG)
    .value("ASP_OD1", ASP_OD1)
    .value("ASP_OD2", ASP_OD2)
    .value("GLN_N", GLN_N)
    .value("GLN_CA", GLN_CA)
    .value("GLN_C", GLN_C)
    .value("GLN_O", GLN_O)
    .value("GLN_CB", GLN_CB)
    .value("GLN_CG", GLN_CG)
    .value("GLN_CD", GLN_CD)
    .value("GLN_OE1", GLN_OE1)
    .value("GLN_NE2", GLN_NE2)
    .value("GLU_N", GLU_N)
    .value("GLU_CA", GLU_CA)
    .value("GLU_C", GLU_C)
    .value("GLU_O", GLU_O)
    .value("GLU_CB", GLU_CB)
    .value("GLU_CG", GLU_CG)
    .value("GLU_CD", GLU_CD)
    .value("GLU_OE1", GLU_OE1)
    .value("GLU_OE2", GLU_OE2)
    .value("LYS_N", LYS_N)
    .value("LYS_CA", LYS_CA)
    .value("LYS_C", LYS_C)
    .value("LYS_O", LYS_O)
    .value("LYS_CB", LYS_CB)
    .value("LYS_CG", LYS_CG)
    .value("LYS_CD", LYS_CD)
    .value("LYS_CE", LYS_CE)
    .value("LYS_NZ", LYS_NZ)
    .value("SER_N", SER_N)
    .value("SER_CA", SER_CA)
    .value("SER_C", SER_C)
    .value("SER_O", SER_O)
    .value("SER_CB", SER_CB)
    .value("SER_OG", SER_OG)
    .value("CYS_N", CYS_N)
    .value("CYS_CA", CYS_CA)
    .value("CYS_C", CYS_C)
    .value("CYS_O", CYS_O)
    .value("CYS_CB", CYS_CB)
    .value("CYS_SG", CYS_SG)
    .value("MET_N", MET_N)
    .value("MET_CA", MET_CA)
    .value("MET_C", MET_C)
    .value("MET_O", MET_O)
    .value("MET_CB", MET_CB)
    .value("MET_CG", MET_CG)
    .value("MET_SD", MET_SD)
    .value("MET_CE", MET_CE)
    .value("TRP_N", TRP_N)
    .value("TRP_CA", TRP_CA)
    .value("TRP_C", TRP_C)
    .value("TRP_O", TRP_O)
    .value("TRP_CB", TRP_CB)
    .value("TRP_CG", TRP_CG)
    .value("TRP_CD1", TRP_CD1)
    .value("TRP_CD2", TRP_CD2)
    .value("TRP_NE1", TRP_NE1)
    .value("TRP_CE2", TRP_CE2)
    .value("TRP_CE3", TRP_CE3)
    .value("TRP_CZ2", TRP_CZ2)
    .value("TRP_CZ3", TRP_CZ3)
    .value("TRP_CH2", TRP_CH2)
    .value("TYR_N", TYR_N)
    .value("TYR_CA", TYR_CA)
    .value("TYR_C", TYR_C)
    .value("TYR_O", TYR_O)
    .value("TYR_CB", TYR_CB)
    .value("TYR_CG", TYR_CG)
    .value("TYR_CD1", TYR_CD1)
    .value("TYR_CD2", TYR_CD2)
    .value("TYR_CE1", TYR_CE1)
    .value("TYR_CE2", TYR_CE2)
    .value("TYR_CZ", TYR_CZ)
    .value("TYR_OH", TYR_OH)
    .value("THR_N", THR_N)
    .value("THR_CA", THR_CA)
    .value("THR_C", THR_C)
    .value("THR_O", THR_O)
    .value("THR_CB", THR_CB)
    .value("THR_OG1", THR_OG1)
    .value("THR_CG2", THR_CG2)
    .value("VAL_N", VAL_N)
    .value("VAL_CA", VAL_CA)
    .value("VAL_C", VAL_C)
    .value("VAL_O", VAL_O)
    .value("VAL_CB", VAL_CB)
    .value("VAL_CG1", VAL_CG1)
    .value("VAL_CG2", VAL_CG2)
    .value("ILE_N", ILE_N)
    .value("ILE_CA", ILE_CA)
    .value("ILE_C", ILE_C)
    .value("ILE_O", ILE_O)
    .value("ILE_CB", ILE_CB)
    .value("ILE_CG1", ILE_CG1)
    .value("ILE_CG2", ILE_CG2)
    .value("ILE_CD1", ILE_CD1)
    .value("LEU_N", LEU_N)
    .value("LEU_CA", LEU_CA)
    .value("LEU_C", LEU_C)
    .value("LEU_O", LEU_O)
    .value("LEU_CB", LEU_CB)
    .value("LEU_CG", LEU_CG)
    .value("LEU_CD1", LEU_CD1)
    .value("LEU_CD2", LEU_CD2)
    .value("GLY_N", GLY_N)
    .value("GLY_CA", GLY_CA)
    .value("GLY_C", GLY_C)
    .value("GLY_O", GLY_O)
    .value("PRO_N", PRO_N)
    .value("PRO_CA", PRO_CA)
    .value("PRO_C", PRO_C)
    .value("PRO_O", PRO_O)
    .value("PRO_CB", PRO_CB)
    .value("PRO_CG", PRO_CG)
    .value("PRO_CD", PRO_CD)
    .value("HIS_N", HIS_N)
    .value("HIS_CA", HIS_CA)
    .value("HIS_C", HIS_C)
    .value("HIS_O", HIS_O)
    .value("HIS_CB", HIS_CB)
    .value("HIS_CG", HIS_CG)
    .value("HIS_ND1", HIS_ND1)
    .value("HIS_CD2", HIS_CD2)
    .value("HIS_CE1", HIS_CE1)
    .value("HIS_NE2", HIS_NE2)
    .value("PHE_N", PHE_N)
    .value("PHE_CA", PHE_CA)
    .value("PHE_C", PHE_C)
    .value("PHE_O", PHE_O)
    .value("PHE_CB", PHE_CB)
    .value("PHE_CG", PHE_CG)
    .value("PHE_CD1", PHE_CD1)
    .value("PHE_CD2", PHE_CD2)
    .value("PHE_CE1", PHE_CE1)
    .value("PHE_CE2", PHE_CE2)
    .value("PHE_CZ", PHE_CZ)
    .value("XXX_NUM_ATOMS", XXX_NUM_ATOMS)
  ;

  enum_<AminoAcidHydrogen>("AminoAcidHydrogen")
    .value("ALA_H", ALA_H)
    .value("ALA_H1", ALA_H1)
    .value("ALA_H2", ALA_H2)
    .value("ALA_H3", ALA_H3)
    .value("ALA_HA", ALA_HA)
    .value("ALA_HB1", ALA_HB1)
    .value("ALA_HB2", ALA_HB2)
    .value("ALA_HB3", ALA_HB3)
    .value("ARG_H", ARG_H)
    .value("ARG_H1", ARG_H1)
    .value("ARG_H2", ARG_H2)
    .value("ARG_H3", ARG_H3)
    .value("ARG_HA", ARG_HA)
    .value("ARG_HB2", ARG_HB2)
    .value("ARG_HB3", ARG_HB3)
    .value("ARG_HG2", ARG_HG2)
    .value("ARG_HG3", ARG_HG3)
    .value("ARG_HD2", ARG_HD2)
    .value("ARG_HD3", ARG_HD3)
    .value("ARG_HE", ARG_HE)
    .value("ARG_HH11", ARG_HH11)
    .value("ARG_HH12", ARG_HH12)
    .value("ARG_HH21", ARG_HH21)
    .value("ARG_HH22", ARG_HH22)
    .value("ASN_H", ASN_H)
    .value("ASN_H1", ASN_H1)
    .value("ASN_H2", ASN_H2)
    .value("ASN_H3", ASN_H3)
    .value("ASN_HA", ASN_HA)
    .value("ASN_HB2", ASN_HB2)
    .value("ASN_HB3", ASN_HB3)
    .value("ASN_HD21", ASN_HD21)
    .value("ASN_HD22", ASN_HD22)
    .value("ASP_H", ASP_H)
    .value("ASP_H1", ASP_H1)
    .value("ASP_H2", ASP_H2)
    .value("ASP_H3", ASP_H3)
    .value("ASP_HA", ASP_HA)
    .value("ASP_HB2", ASP_HB2)
    .value("ASP_HB3", ASP_HB3)
    .value("GLN_H", GLN_H)
    .value("GLN_H1", GLN_H1)
    .value("GLN_H2", GLN_H2)
    .value("GLN_H3", GLN_H3)
    .value("GLN_HA", GLN_HA)
    .value("GLN_HB2", GLN_HB2)
    .value("GLN_HB3", GLN_HB3)
    .value("GLN_HG2", GLN_HG2)
    .value("GLN_HG3", GLN_HG3)
    .value("GLN_HE21", GLN_HE21)
    .value("GLN_HE22", GLN_HE22)
    .value("GLU_H", GLU_H)
    .value("GLU_H1", GLU_H1)
    .value("GLU_H2", GLU_H2)
    .value("GLU_H3", GLU_H3)
    .value("GLU_HA", GLU_HA)
    .value("GLU_HB2", GLU_HB2)
    .value("GLU_HB3", GLU_HB3)
    .value("GLU_HG2", GLU_HG2)
    .value("GLU_HG3", GLU_HG3)
    .value("LYS_H", LYS_H)
    .value("LYS_H1", LYS_H1)
    .value("LYS_H2", LYS_H2)
    .value("LYS_H3", LYS_H3)
    .value("LYS_HA", LYS_HA)
    .value("LYS_HB2", LYS_HB2)
    .value("LYS_HB3", LYS_HB3)
    .value("LYS_HG2", LYS_HG2)
    .value("LYS_HG3", LYS_HG3)
    .value("LYS_HD2", LYS_HD2)
    .value("LYS_HD3", LYS_HD3)
    .value("LYS_HE2", LYS_HE2)
    .value("LYS_HE3", LYS_HE3)
    .value("LYS_HZ1", LYS_HZ1)
    .value("LYS_HZ2", LYS_HZ2)
    .value("LYS_HZ3", LYS_HZ3)
    .value("SER_H", SER_H)
    .value("SER_H1", SER_H1)
    .value("SER_H2", SER_H2)
    .value("SER_H3", SER_H3)
    .value("SER_HA", SER_HA)
    .value("SER_HB2", SER_HB2)
    .value("SER_HB3", SER_HB3)
    .value("SER_HG", SER_HG)
    .value("CYS_H", CYS_H)
    .value("CYS_H1", CYS_H1)
    .value("CYS_H2", CYS_H2)
    .value("CYS_H3", CYS_H3)
    .value("CYS_HA", CYS_HA)
    .value("CYS_HB2", CYS_HB2)
    .value("CYS_HB3", CYS_HB3)
    .value("CYS_HG", CYS_HG)
    .value("MET_H", MET_H)
    .value("MET_H1", MET_H1)
    .value("MET_H2", MET_H2)
    .value("MET_H3", MET_H3)
    .value("MET_HA", MET_HA)
    .value("MET_HB2", MET_HB2)
    .value("MET_HB3", MET_HB3)
    .value("MET_HG2", MET_HG2)
    .value("MET_HG3", MET_HG3)
    .value("MET_HE1", MET_HE1)
    .value("MET_HE2", MET_HE2)
    .value("MET_HE3", MET_HE3)
    .value("TRP_H", TRP_H)
    .value("TRP_H1", TRP_H1)
    .value("TRP_H2", TRP_H2)
    .value("TRP_H3", TRP_H3)
    .value("TRP_HA", TRP_HA)
    .value("TRP_HB2", TRP_HB2)
    .value("TRP_HB3", TRP_HB3)
    .value("TRP_HD1", TRP_HD1)
    .value("TRP_HE1", TRP_HE1)
    .value("TRP_HE3", TRP_HE3)
    .value("TRP_HZ2", TRP_HZ2)
    .value("TRP_HZ3", TRP_HZ3)
    .value("TRP_HH2", TRP_HH2)
    .value("TYR_H", TYR_H)
    .value("TYR_H1", TYR_H1)
    .value("TYR_H2", TYR_H2)
    .value("TYR_H3", TYR_H3)
    .value("TYR_HA", TYR_HA)
    .value("TYR_HB2", TYR_HB2)
    .value("TYR_HB3", TYR_HB3)
    .value("TYR_HD1", TYR_HD1)
    .value("TYR_HD2", TYR_HD2)
    .value("TYR_HE1", TYR_HE1)
    .value("TYR_HE2", TYR_HE2)
    .value("TYR_HH", TYR_HH)
    .value("THR_H", THR_H)
    .value("THR_H1", THR_H1)
    .value("THR_H2", THR_H2)
    .value("THR_H3", THR_H3)
    .value("THR_HA", THR_HA)
    .value("THR_HB", THR_HB)
    .value("THR_HG1", THR_HG1)
    .value("THR_HG21", THR_HG21)
    .value("THR_HG22", THR_HG22)
    .value("THR_HG23", THR_HG23)
    .value("VAL_H", VAL_H)
    .value("VAL_H1", VAL_H1)
    .value("VAL_H2", VAL_H2)
    .value("VAL_H3", VAL_H3)
    .value("VAL_HA", VAL_HA)
    .value("VAL_HB", VAL_HB)
    .value("VAL_HG11", VAL_HG11)
    .value("VAL_HG12", VAL_HG12)
    .value("VAL_HG13", VAL_HG13)
    .value("VAL_HG21", VAL_HG21)
    .value("VAL_HG22", VAL_HG22)
    .value("VAL_HG23", VAL_HG23)
    .value("ILE_H", ILE_H)
    .value("ILE_H1", ILE_H1)
    .value("ILE_H2", ILE_H2)
    .value("ILE_H3", ILE_H3)
    .value("ILE_HA", ILE_HA)
    .value("ILE_HB", ILE_HB)
    .value("ILE_HG12", ILE_HG12)
    .value("ILE_HG13", ILE_HG13)
    .value("ILE_HG21", ILE_HG21)
    .value("ILE_HG22", ILE_HG22)
    .value("ILE_HG23", ILE_HG23)
    .value("ILE_HD11", ILE_HD11)
    .value("ILE_HD12", ILE_HD12)
    .value("ILE_HD13", ILE_HD13)
    .value("LEU_H", LEU_H)
    .value("LEU_H1", LEU_H1)
    .value("LEU_H2", LEU_H2)
    .value("LEU_H3", LEU_H3)
    .value("LEU_HA", LEU_HA)
    .value("LEU_HB2", LEU_HB2)
    .value("LEU_HB3", LEU_HB3)
    .value("LEU_HG", LEU_HG)
    .value("LEU_HD11", LEU_HD11)
    .value("LEU_HD12", LEU_HD12)
    .value("LEU_HD13", LEU_HD13)
    .value("LEU_HD21", LEU_HD21)
    .value("LEU_HD22", LEU_HD22)
    .value("LEU_HD23", LEU_HD23)
    .value("GLY_H", GLY_H)
    .value("GLY_H1", GLY_H1)
    .value("GLY_H2", GLY_H2)
    .value("GLY_H3", GLY_H3)
    .value("GLY_HA2", GLY_HA2)
    .value("GLY_HA3", GLY_HA3)
    .value("PRO_H1", PRO_H1)
    .value("PRO_H2", PRO_H2)
    .value("PRO_HA", PRO_HA)
    .value("PRO_HB2", PRO_HB2)
    .value("PRO_HB3", PRO_HB3)
    .value("PRO_HG2", PRO_HG2)
    .value("PRO_HG3", PRO_HG3)
    .value("PRO_HD2", PRO_HD2)
    .value("PRO_HD3", PRO_HD3)
    .value("HIS_H", HIS_H)
    .value("HIS_H1", HIS_H1)
    .value("HIS_H2", HIS_H2)
    .value("HIS_H3", HIS_H3)
    .value("HIS_HA", HIS_HA)
    .value("HIS_HB2", HIS_HB2)
    .value("HIS_HB3", HIS_HB3)
    .value("HIS_HD1", HIS_HD1)
    .value("HIS_HD2", HIS_HD2)
    .value("HIS_HE1", HIS_HE1)
    .value("HIS_HE2", HIS_HE2)
    .value("PHE_H", PHE_H)
    .value("PHE_H1", PHE_H1)
    .value("PHE_H2", PHE_H2)
    .value("PHE_H3", PHE_H3)
    .value("PHE_HA", PHE_HA)
    .value("PHE_HB2", PHE_HB2)
    .value("PHE_HB3", PHE_HB3)
    .value("PHE_HD1", PHE_HD1)
    .value("PHE_HD2", PHE_HD2)
    .value("PHE_HE1", PHE_HE1)
    .value("PHE_HE2", PHE_HE2)
    .value("PHE_HZ", PHE_HZ)
    .value("XXX_NUM_HYDROGENS", XXX_NUM_HYDROGENS)
  ;

  class_<BondInfo>("BondInfo", no_init)
    .def_readonly("atom_idx_one", &BondInfo::atom_idx_one)
    .def_readonly("atom_idx_two", &BondInfo::atom_idx_two)
    .def_readonly("order", &BondInfo::order)
  ;

  class_<AminoAcidLookup>("AminoAcidLookup", no_init)
    .def("GetOLC", WrapGetOLC, (arg("aa")))
    .staticmethod("GetOLC")
    .def("GetAAA", WrapGetAAA, (arg("aa"), arg("atom_idx")))
    .def("GetAAA", WrapGetAAAName, (arg("aa"), arg("atom_name")))
    .staticmethod("GetAAA")
    .def("GetAAH", WrapGetAAH, (arg("aa"), arg("atom_idx")))
    .def("GetAAH", WrapGetAAHName, (arg("aa"), arg("atom_name")))
    .staticmethod("GetAAH")
    .def("GetIndex", WrapGetIndex, (arg("aa"), arg("atom_name")))
    .staticmethod("GetIndex")
    .def("GetHydrogenIndex", WrapGetHydrogenIndex, (arg("aa"), arg("atom_name")))
    .staticmethod("GetHydrogenIndex")
    .def("GetNumAtoms", WrapGetNumAtoms, (arg("aa")))
    .staticmethod("GetNumAtoms")
    .def("GetMaxNumAtoms", WrapGetMaxNumAtoms)
    .staticmethod("GetMaxNumAtoms")
    .def("GetNumHydrogens", WrapGetNumHydrogens, (arg("aa")))
    .staticmethod("GetNumHydrogens")
    .def("GetMaxNumHydrogens", WrapGetMaxNumHydrogens)
    .staticmethod("GetMaxNumHydrogens")
    .def("GetAA", WrapGetAA, (arg("aaa")))
    .def("GetAA", WrapGetHydrogenAA, (arg("aah")))
    .staticmethod("GetAA")
    .def("GetAtomName", WrapGetAtomName, (arg("aaa")))
    .def("GetAtomName", WrapGetAtomHydrogenName, (arg("aah")))
    .staticmethod("GetAtomName")
    .def("GetAtomNameCharmm", WrapGetAtomNameCharmm, (arg("aaa")))
    .def("GetAtomNameCharmm", WrapGetAtomHydrogenNameCharmm, (arg("aah")))
    .staticmethod("GetAtomNameCharmm")
    .def("GetAtomNameAmber", WrapGetAtomNameAmber, (arg("aaa")))
    .def("GetAtomNameAmber", WrapGetAtomHydrogenNameAmber, (arg("aah")))
    .staticmethod("GetAtomNameAmber")
    .def("GetElement", WrapGetElement, (arg("aaa")))
    .staticmethod("GetElement")
    .def("GetBonds", WrapGetBonds, (arg("aa")))
    .staticmethod("GetBonds")
    .def("GetAnchorAtomIndex", WrapGetAnchorAtomIndex, (arg("aah")))
    .staticmethod("GetAnchorAtomIndex")
    .def("GetHNIndex", WrapGetHNIndex, (arg("aa")))
    .staticmethod("GetHNIndex")
    .def("GetH1Index", WrapGetH1Index, (arg("aa")))
    .staticmethod("GetH1Index")
    .def("GetH2Index", WrapGetH2Index, (arg("aa")))
    .staticmethod("GetH2Index")
    .def("GetH3Index", WrapGetH3Index, (arg("aa")))
    .staticmethod("GetH3Index")
  ;

}
