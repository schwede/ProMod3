// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/loop/loop_object_loader.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

void export_LoopObjectLoader() {
  def("LoadTorsionSampler", &LoadTorsionSampler, (arg("seed")=0));
  def("LoadTorsionSamplerHelical", &LoadTorsionSamplerHelical, (arg("seed")=0));
  def("LoadTorsionSamplerExtended", &LoadTorsionSamplerExtended,
      (arg("seed")=0));
  def("LoadTorsionSamplerCoil", &LoadTorsionSamplerCoil, (arg("seed")=0));
  def("LoadFragDB", &LoadFragDB);
  def("LoadStructureDB", &LoadStructureDB);
}
