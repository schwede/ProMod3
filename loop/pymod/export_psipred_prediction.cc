// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/make_constructor.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/psipred_prediction.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;


namespace {

  PsipredPredictionPtr WrapEmtpyConstructor(){
    PsipredPredictionPtr p(new PsipredPrediction);
    return p;
  }

  PsipredPredictionPtr WrapListConstructor(const boost::python::list& prediction, 
                                           const boost::python::list& confidence){
    
    std::vector<char> v_prediction;
    std::vector<int> v_confidence;
    core::ConvertListToVector(prediction, v_prediction);
    core::ConvertListToVector(confidence, v_confidence);
    PsipredPredictionPtr p(new PsipredPrediction(v_prediction,v_confidence));
    return p;
  }

  boost::python::list WrapGetPredictions(const PsipredPrediction& p){
    std::vector<char> v_predictions = p.GetPredictions();
    boost::python::list predictions;
    core::AppendVectorToList(v_predictions, predictions);
    return predictions;
  }

  boost::python::list WrapGetConfidences(const PsipredPrediction& p){
    std::vector<int> v_confidences = p.GetConfidences();
    boost::python::list confidences;
    core::AppendVectorToList(v_confidences, confidences);
    return confidences;
  }
}


void export_PsipredPrediction(){

  class_<PsipredPrediction> ("PsipredPrediction", no_init)
    .def("__init__",make_constructor(&WrapEmtpyConstructor))
    .def("__init__",make_constructor(&WrapListConstructor))
    .def("FromHHM",&PsipredPrediction::FromHHM,(arg("filename"))).staticmethod("FromHHM")
    .def("FromHoriz",&PsipredPrediction::FromHoriz,(arg("filename"))).staticmethod("FromHoriz")
    .def("Extract",&PsipredPrediction::Extract,(arg("from"),arg("to")))
    .def("Add",&PsipredPrediction::Add,(arg("prediction"),arg("confidence")))
    .def("GetPrediction",&PsipredPrediction::GetPrediction,(arg("idx")))
    .def("GetConfidence",&PsipredPrediction::GetConfidence,(arg("idx")))
    .def("GetPredictions",&WrapGetPredictions)
    .def("GetConfidences",&WrapGetConfidences)
    .def("__len__",&PsipredPrediction::size)
  ;

  register_ptr_to_python<PsipredPredictionPtr>(); 

}
