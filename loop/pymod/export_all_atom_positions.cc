// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/loop/all_atom_positions.hh>

using namespace promod3;
using namespace promod3::loop;
using namespace boost::python;

// WRAPPERS
namespace {

// using function pointer wrappers where possible

void (AllAtomPositions::*WrapSetResidueOst)(uint,
                                            const ost::mol::ResidueHandle&)
                                           = &AllAtomPositions::SetResidue;
void (AllAtomPositions::*WrapSetResidue)(uint, const AllAtomPositions&, uint)
                                        = &AllAtomPositions::SetResidue;

// only global indexing exported (otherwise we need enums too...)
void WrapSetPos(AllAtomPositions& atoms, uint idx, const geom::Vec3& pos) {
  atoms.CheckIndex(idx);
  atoms.SetPos(idx, pos);
}
void WrapClearPos(AllAtomPositions& atoms, uint idx) {
  atoms.CheckIndex(idx);
  atoms.ClearPos(idx);
}
const geom::Vec3& WrapGetPos(const AllAtomPositions& atoms, uint idx) {
  atoms.CheckIndex(idx);
  return atoms.GetPos(idx);
}
bool WrapIsSet(const AllAtomPositions& atoms, uint idx) {
  atoms.CheckIndex(idx);
  return atoms.IsSet(idx);
}
uint WrapGetIndex(const AllAtomPositions& atoms, uint idx, const String& aname) {
  atoms.CheckIndex(idx);
  return atoms.GetIndex(idx, aname);
}
Real WrapGetPhiTorsion(const AllAtomPositions& atoms, uint idx, Real def) {
  atoms.CheckResidueIndex(idx-1);
  atoms.CheckResidueIndex(idx);
  return atoms.GetPhiTorsion(idx, def);
}
Real WrapGetPsiTorsion(const AllAtomPositions& atoms, uint idx, Real def) {
  atoms.CheckResidueIndex(idx+1);
  return atoms.GetPsiTorsion(idx, def);
}
Real WrapGetOmegaTorsion(const AllAtomPositions& atoms, uint idx, Real def) {
  atoms.CheckResidueIndex(idx-1);
  atoms.CheckResidueIndex(idx);
  return atoms.GetOmegaTorsion(idx, def);
}

AllAtomPositionsPtr (AllAtomPositions::*WrapExtract)(uint, uint) const
                                                 = &AllAtomPositions::Extract;
AllAtomPositionsPtr WrapExtractList(const AllAtomPositions& atoms,
                                    const boost::python::list& res_idx_list) {
  std::vector<uint> res_indices;
  core::ConvertListToVector(res_idx_list, res_indices);
  return atoms.Extract(res_indices);
}

void (AllAtomPositions::*WrapInsertIntoResNum)(uint, ost::mol::ChainHandle&,
                                               const ost::mol::ResNum&) const
                                              = &AllAtomPositions::InsertInto;
void (AllAtomPositions::*WrapInsertIntoInt)(uint, ost::mol::ChainHandle&,
                                            uint) const 
                                           = &AllAtomPositions::InsertInto;

} // anon wrapper ns

void export_AllAtomPositions() {

  class_<AllAtomPositions, AllAtomPositionsPtr>("AllAtomPositions", no_init)
    .def(init<const String&>(arg("sequence")))
    .def(init<const ost::mol::ResidueHandleList&>(arg("res_list")))
    .def(init<const String&, const ost::mol::ResidueHandleList&>(
         (arg("sequence"), arg("res_list"))))
    .def(init<const BackboneList&>(arg("bb_list")))

    .def("SetResidue", WrapSetResidueOst,
         (arg("res_index"), arg("res")))
    .def("SetResidue", WrapSetResidue,
         (arg("res_index"), arg("other"), arg("other_res_index")))
    .def("SetPos", WrapSetPos, (arg("index"), arg("pos")))
    .def("ClearPos", WrapClearPos, (arg("index")))
    .def("ClearResidue", &AllAtomPositions::ClearResidue, (arg("res_index")))

    .def("GetPos", WrapGetPos, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("IsSet", WrapIsSet, (arg("index")))
    
    .def("GetIndex", WrapGetIndex, (arg("res_index"), arg("atom_name")))
    .def("GetFirstIndex", &AllAtomPositions::GetFirstIndex, (arg("res_index")))
    .def("GetLastIndex", &AllAtomPositions::GetLastIndex, (arg("res_index")))
    .def("GetAA", &AllAtomPositions::GetAA, (arg("res_index")))
    .def("IsAnySet", &AllAtomPositions::IsAnySet, (arg("res_index")))
    .def("IsAllSet", &AllAtomPositions::IsAllSet, (arg("res_index")))

    .def("GetPhiTorsion", WrapGetPhiTorsion,
         (arg("res_index"), arg("def_angle")=-1.0472))
    .def("GetPsiTorsion", WrapGetPsiTorsion,
         (arg("res_index"), arg("def_angle")=-0.7854))
    .def("GetOmegaTorsion", WrapGetOmegaTorsion,
         (arg("res_index"), arg("def_angle")=3.14159))

    .def("GetNumAtoms", &AllAtomPositions::GetNumAtoms)
    .def("GetNumResidues", &AllAtomPositions::GetNumResidues)
    .def("GetSequence", &AllAtomPositions::GetSequence)

    .def("Copy", &AllAtomPositions::Copy)
    .def("Extract", WrapExtract, (arg("from"), arg("to")))
    .def("Extract", WrapExtractList, (arg("res_indices")))
    .def("ExtractBackbone", &AllAtomPositions::ExtractBackbone,
         (arg("from"), arg("to")))
    .def("ToEntity", &AllAtomPositions::ToEntity)
    .def("InsertInto", WrapInsertIntoResNum,
         (arg("res_index"), arg("chain"), arg("res_num")))
    .def("InsertInto", WrapInsertIntoInt,
         (arg("res_index"), arg("chain"), arg("res_num")))
  ;

  register_ptr_to_python<ConstAllAtomPositionsPtr>();

}
