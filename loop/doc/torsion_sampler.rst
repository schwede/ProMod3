..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Sampling Dihedral Angles
================================================================================

.. currentmodule:: promod3.loop


The torsion sampler is the basic object used to sample the backbone torsion 
angles phi and psi. It can be used to calculate the probability distributions 
of backbone torsion angles from structures and save them, as well as loading 
distributions and drawing from them.
It uses distributions specific for triplets of residues, so that when drawing 
a phi/psi pair for a certain residue, the distribution from which the pair is 
drawn also depends on the identity of the residues before and after the residue 
in question.
The distributions of the sampler are internally stored in a vector, so that 
most methods which need to access a specific distribution can either take 3 
residue names or an index as input.

As a showcase example, we randomly sample from a given torsion sample and
store the resulting samples as a scatter plot:

.. literalinclude:: ../../../tests/doc/scripts/loop_torsion_sampler.py


Defining Amino Acid triplets
--------------------------------------------------------------------------------

Since the torsion sampler considers triplets of amino acids, we need to define
them. This is done with the so called torsion group definitions.
Three strings represent the according positions of the consecutive 
amino acids. They are combined by "-". It is either possible to
use the keyword "all", or write out all allowed amino acids by their
three letter code and separate them by ",". An example would be: "all-
VAL,ILE-PRO". There are cases where a tripeptide can match several
group definitions. The list of group definitions is iterated for every
combination of three consecutive amino acids and the first hit is
decisive.



The Torsion Sampler Class
--------------------------------------------------------------------------------

.. class:: TorsionSampler(group_definitions, bins_per_dimension, seed)
    
  Basic object used to sample the backbone torsion angles phi and psi.

  :param group_definitions:  List of group definitions defining amino acid triplets
  :param bins_per_dimension: Number of bins to represent the 360 degrees of each
                             torsion angle
  :param seed:               Seed for random number generator

  :type group_definitions:   :class:`list` of :class:`str`
  :type binsa_per_dimension: :class:`int`
  :type seed:                :class:`int`

  :raises:                   :class:`RuntimeException` when there is a
                             possible combination of the 20 standard amino
                             acids not matching any of the group definitions.

  .. method:: ExtractStatistics(view)
    
    Extracts backbone torsion angles from the structure and adds them to the appropriate histograms in the sampler.

    :param view:          structure from which parameters will be extracted

    :type view:           :class:`ost.mol.EntityView`

  .. method:: UpdateDistributions()
    
    Recalculates the probability distributions from the histograms.

  .. staticmethod:: Load(filename, seed)
                    LoadPortable(filename, seed)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load the sampler.
    :type filename:  :class:`str`
    :param seed: Seed for random number generator (not saved in file).
    :type seed:  :class:`int`

    :returns: A torsion sampler
    :rtype:   :class:`TorsionSampler`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where the sampler will be saved
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.

  .. method:: GetHistogramIndex(before,central,after)

    :param before:        id of the residue before *central*
    :param central:       id of the residue for the central residue
    :param after:         id of the residue after *central*

    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`

    :returns:             The index of the histogram corresponding to the triplet of residues specified.

  .. method:: GetHistogramIndices(sequence)

    :param sequence:      Sequence of length n from which histogram indices
                          should created.

    :type sequence:       :class:`str`

    :returns:             List of length n-2 containing histogram indices of
                          all consecutive amino acid triplets in **sequence**

    :raises:  :exc:`~exceptions.RuntimeError` if **sequence** contains non
              standard amino acid


  .. method:: Draw(before,central,after)
    
    Draws a pair of dihedral angles for the *central* residue from the distribution specific for such a triplet of residues.

    :param before:        id of the residue before *central*
    :param central:       id of the residue for which torsion angles will be drawn
    :param after:         id of the residue after *central*
    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`

    :returns:           A pair of phi/psi angles

  .. method:: Draw(index)

    Draws a pair of dihedral angles from the distribution specified by the *index*.

    :param index:         The index of the distribution from which a phi/psi pair will be drawn.

    :type index:          :class:`int`

    :returns:           A pair of phi/psi angles


  .. method:: DrawPhiGivenPsi(before,central,after,psi)
    
    Draws a *phi* angle for the *central* residue from the conditional distribution P( *phi* | *psi* ) specific for such a triplet of residues.

    :param before:        id of the residue before *central*
    :param central:       id of the residue for which the *phi* will be drawn
    :param after:         id of the residue after *central*
    :param psi:           *psi* angle

    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`
    :type psi:            :class:`float`

    :returns:           An angle

  .. method:: DrawPhiGivenPsi(index,psi)

    Draws a *phi* angle from the conditional distribution P( *phi* | *psi* ) specified by the *index*.

    :param index:         The index of the distribution from which a *phi* angle will be drawn.
    :param psi:           *psi* angle

    :type index:          :class:`int`
    :type psi:            :class:`float`

    :returns:           An angle

  .. method:: DrawPsiGivenPhi(before,central,after,phi)
    
    Draws a *phi* angle for the *central* residue from the conditional distribution P( *psi* | *phi* ) specific for such a triplet of residues.

    :param before:        id of the residue before *central*
    :param central:       id of the residue for which the *psi* angle will be drawn
    :param after:         id of the residue after *central*
    :param phi:           *phi* angle

    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`
    :type phi:            :class:`float`

    :returns:           An angle

  .. method:: DrawPsiGivenPhi(index,phi)

    Draws a *phi* angle from the conditional distribution P( *psi* | *phi* ) specified by the *index*.

    :param index:         The index of the distribution from which a psi angle will be drawn.
    :param phi:           *phi* angle

    :type index:          :class:`int`
    :type phi:            :class:`float`

    :returns:           An angle

  .. method:: GetProbability(before,central,after,phi,psi)
    
    Returns the probability of a specific pair of phi/psi angles for the central residue from the corresponding distribution.

    :param before:        id of the residue before *central*
    :param central:       id of the residue for which the probability is calculated.
    :param after:         id of the residue after *central*
    :param phi:           phi angle
    :param psi:           psi angle

    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`
    :type phi:            :class:`float`
    :type psi:            :class:`float`

    :returns:             A probability

  .. method:: GetProbability(index,phi,psi)
    
    Returns the probability of a specific pair of phi/psi angles calulated from the distribution specified by *index*.

    :param index:         The index of the distribution.
    :param phi:           phi angle
    :param psi:           psi angle

    :type index:          :class:`int`
    :type phi:            :class:`float`
    :type psi:            :class:`float`

    :returns:             A probability

  .. method:: GetPhiProbabilityGivenPsi(before,central,after,phi,psi)
    
    Returns P( *phi* | *psi* ) for the central residue from the corresponding distribution.

    :param before:        id of the residue before *central*
    :param central:       id of the residue for which the probability is calculated.
    :param after:         id of the residue after *central*
    :param phi:           phi angle
    :param psi:           psi angle

    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`
    :type phi:            :class:`float`
    :type psi:            :class:`float`

    :returns:             A probability

  .. method:: GetPsiProbabilityGivenPhi(before,central,after,psi,phi)
    
    Returns P( *psi* | *phi* ) for the central residue from the corresponding distribution.

    :param before:        id of the residue before *central*
    :param central:       id of the residue for which the probability is calculated.
    :param after:         id of the residue after *central*
    :param psi:           phi angle
    :param phi:           psi angle

    :type before:         :class:`ost.conop.AminoAcid`
    :type central:        :class:`ost.conop.AminoAcid`
    :type after:          :class:`ost.conop.AminoAcid`
    :type phi:            :class:`float`
    :type psi:            :class:`float`

    :returns:             A probability


  .. method:: GetPhiProbabilityGivenPsi(index,phi,psi)
    
    Returns P( *phi* | *psi* ) for the central residue from the corresponding distribution.

    :param index:         The index of the distribution.
    :param psi:           phi angle
    :param phi:           psi angle

    :type phi:            :class:`float`
    :type psi:            :class:`float`
    :type index:          :class:`int`

    :returns:             A probability

  .. method:: GetPsiProbabilityGivenPhi(index,psi,phi)
    
    Returns P( *psi* | *phi* ) for the central residue from the corresponding distribution.

    :param index:         The index of the distribution.
    :param psi:           phi angle
    :param phi:           psi angle


    :type phi:            :class:`float`
    :type psi:            :class:`float`
    :type index:          :class:`int`

    :returns:             A probability

  .. method:: GetBinsPerDimension()
    
    Returns the number of bins per dimension of the distributions.

    :rtype:               :class:`int`    

  .. method:: GetBinSize()

    Returns the size of the bins (in radians) of the distributions.

    :rtype:               :class:`float`  
