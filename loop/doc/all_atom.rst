..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Handling All Atom Positions
================================================================================

.. currentmodule:: promod3.loop

To represent and handle all atom loops, we provide a container
(:class:`AllAtomPositions`) to represent arbitrary amino acid sequences with the
positions of all their heavy atoms and an environment (:class:`AllAtomEnv`) to
handle changes during loop modelling.

The example below showcases some operations on the two classes:

.. literalinclude:: ../../../tests/doc/scripts/loop_all_atom.py


The AllAtomEnv class
--------------------------------------------------------------------------------

.. class:: AllAtomEnv(seqres)

  The all atom environment contains and handles positions of all atoms during
  loop modelling. It is linked to a (list of) seqres (one per chain) at
  construction. The idea is to initialize it at the beginning of the modelling
  process with all known positions and then update the environment whenever a
  new loop is being added.

  :param seqres: Internal SEQRES to be set (single chain or list with one per
                 chain). Whenever setting structural data, consistency with this SEQRES is enforced.
  :type seqres:  :class:`str` / :class:`ost.seq.SequenceHandle` / 
                 :class:`list` of :class:`str` / :class:`ost.seq.SequenceList`

  Indexing to access parts of the environment:

  * *chain_idx*: Index of chain as it occurs in *seqres* (0 for single sequence)
  * *start_resnum*: Residue number defining the position in the SEQRES of chain
    with index *chain_idx*. **The numbering starts for every chain with the
    value 1**.
  * internal residue indexing: all residues of all chains are simply
    concatenated one after each other (indexing starts at 0)

  .. method:: SetInitialEnvironment(env_structure)

    Sets full environment. Existing data is cleared first.

    :param env_structure:  Structral data to be set as environment. The chains
                           in *env_structure* are expected to be in the same
                           order as the SEQRES items provided in constructor.
    :type env_structure:   :class:`ost.mol.EntityHandle`

    :raises:  :exc:`~exceptions.RuntimeError` if *env* is inconsistent with
              SEQRES set in constructor. This can be because of corrupt residue
              numbers or sequence mismatches.

  .. method:: SetEnvironment(new_env_pos)
              SetEnvironment(new_pos, start_resnum, chain_idx=0)
              SetEnvironment(bb_list, start_resnum, chain_idx=0)

    Add/update atom positions in environment. In the end, all residues covered
    in *new_env_pos* / *new_pos* / *bb_list* will be set as defined there. This
    means, that positions in the env. may be reset, newly set or cleared.

    :param new_env_pos: Structural data to be set as environment.
    :type new_env_pos:  :class:`AllAtomEnvPositions`
    :param new_pos: Structural data to be set as environment.
    :type new_pos:  :class:`AllAtomPositions`
    :param bb_list: Backbone data to be set as environment.
    :type bb_list:  :class:`BackboneList`
    :param start_resnum: Res. number defining the start position in the SEQRES.
    :type start_resnum:  :class:`int` / :class:`ost.mol.ResNum`
    :param chain_idx: Index of chain the structural data belongs to.
    :type chain_idx:  :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` if *new_pos* / *new_env_pos* /
              *bb_list* is inconsistent with SEQRES set in constructor or when
              either *start_resnum* or *chain_idx* point to invalid positions in
              the SEQRES.

  .. method:: ClearEnvironment(start_resnum, num_residues, chain_idx=0)

    Clears a stretch of length *num_residues* in the environment in chain 
    with idx *chain_idx* starting from residue number *start_resnum*

    :param start_resnum: Start of stretch to clear
    :type start_resnum:  :class:`int`
    :param num_residues: Length of stretch to clear
    :type num_residues:  :class:`int`
    :param chain_idx: Chain the stretch belongs to
    :type chain_idx:  :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` when either *start_resnum* or 
              *chain_idx* point to invalid positions in the SEQRES.

  .. method:: GetEnvironment(start_resnum, num_residues, chain_idx=0)

    :return: Copy of stretch of structural data in environment. Useful to store
             a loop to reset later with :meth:`SetEnvironment`.
    :rtype:  :class:`AllAtomEnvPositions`

    :param start_resnum: Start of stretch to store
    :type start_resnum:  :class:`int`
    :param num_residues: Length of stretch to store
    :type num_residues:  :class:`int`
    :param chain_idx: Chain the stretch belongs to
    :type chain_idx:  :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` when either *start_resnum* or 
              *chain_idx* point to invalid positions in the SEQRES.

  .. method:: GetSeqres()

    :return: SEQRES that was set in constructor (one sequence per chain).
    :rtype:  :class:`ost.seq.SequenceList`

  .. method:: GetAllAtomPositions()

    :return: Reference (use with caution!) to the internal storage of all atom
             positions for the environment. All residues of all chains are
             stored continuously in there. To get a copy of some positions use
             :meth:`GetEnvironment`.
    :rtype:  :class:`AllAtomPositions`


The AllAtomPositions class
--------------------------------------------------------------------------------

.. class:: AllAtomPositions

  Container for the positions of all heavy atoms of an arbitrary amino acid sequence. This is tailored for fast operations within |C++| codes. The Python export described here, is mainly meant for debugging or to initialize the object and feed it to other classes using it.

  Indexing of positions and residues:

  - residue indexing is in the range [0, :meth:`GetNumResidues`-1] and is in the
    order of the sequence used to initialize the object
  - indexing of single atoms is in the range [0, :meth:`GetNumAtoms`-1]. For
    each residue you can find the bounds with :meth:`GetFirstIndex` and
    :meth:`GetLastIndex` or find a single atom with :meth:`GetIndex`

  Each atom position is initially unset (unless a list of residues is passed
  when constructing it) and can only be set with calls to :meth:`SetPos` or
  :meth:`SetResidue`.

  .. method:: AllAtomPositions(sequence)

    Creates a container for the given *sequence* with all positions unset.

    :param sequence: Sequence of amino acid one letter codes.
    :type sequence:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *sequence* contains a one letter
             code which is not one of the 20 default amino acids.

  .. method:: AllAtomPositions(residues)

    Creates a container for the given *residues*. Both sequence and positions
    are extracted from the given residues.

    :param residues: List of residues
    :type residues:  :class:`ost.mol.ResidueHandleList`

    :raises: :exc:`~exceptions.RuntimeError` if any residue has a one letter
             code which is not one of the 20 default amino acids.

  .. method:: AllAtomPositions(sequence, residues)

    Creates a container for the given *sequence* and extracts positions from
    *residues*. The residues may be different amino acids than the given
    *sequence* (see :meth:`SetResidue`).

    :param sequence: Sequence of amino acid one letter codes.
    :type sequence:  :class:`str`
    :param residues: List of residues from which positions are extracted.
    :type residues:  :class:`ost.mol.ResidueHandleList`

    :raises: :exc:`~exceptions.RuntimeError` if *sequence* contains a one letter
             code which is not one of the 20 default amino acids or if
             *sequence* and *residues* are inconsistent in size.

  .. method:: AllAtomPositions(bb_list)

    Creates a container for the given backbone. Both sequence and backbone
    positions are extracted from the given residues.

    :param bb_list: Backbone list of residues
    :type bb_list:  :class:`BackboneList`

    :raises: :exc:`~exceptions.RuntimeError` if any residue has a one letter
             code which is not one of the 20 default amino acids.


  .. method:: SetResidue(res_index, res)

    Set positions for residue at index *res_index* given the atoms in *res*.
    For each expected heavy atom, we search for an atom of that name in *res*
    and if found set the corresponding position, otherwise we unset it.

    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param res: Residue providing atoms
    :type res:  :class:`ost.mol.ResidueHandle`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.

  .. method:: SetResidue(res_index, other, other_res_index)

    Set positions for residue at index *res_index* given the positions of
    residue at index *other_res_index* in *other* position container. Each
    position is set or cleared according to the data in *other*.

    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param other: Position container from which we take data
    :type other:  :class:`AllAtomPositions`
    :param other_res_index: Residue index in *other*
    :type other_res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* or *other_res_index*
             out of bounds or if residues in the two containers are inconsistent
             (different amino acids).

  .. method:: SetPos(index, pos)

    :param index: Set position at that index.
    :type index:  :class:`int`
    :param pos: Set position to *pos*.
    :type pos:  :class:`ost.geom.Vec3`

    :raises: :exc:`~exceptions.RuntimeError` if *index* out of bounds.

  .. method:: ClearPos(index)

    :param index: Unset position at that index.
    :type index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *index* out of bounds.

  .. method:: ClearResidue(res_index)

    :param res_index: Unset all positions for residue at that index.
    :type res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.


  .. method:: GetPos(index)

    :return: Position at given index.
    :rtype:  :class:`ost.geom.Vec3`
    :param index: Atom position index.
    :type index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *index* out of bounds.

  .. method:: IsSet(index)

    :return: True, if the position at that index is currently set.
    :rtype:  :class:`bool`
    :param index: Atom position index.
    :type index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *index* out of bounds.


  .. method:: GetIndex(res_index, atom_name)

    :return: Atom position index for atom named *atom_name* (standard PDB
             naming) for residue at index *res_index*.
    :rtype:  :class:`int`
    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds or
             if *atom_name* is not one of that residue's heavy atoms.

  .. method:: GetFirstIndex(res_index)

    :return: First atom position index for residue at index *res_index*.
    :rtype:  :class:`int`
    :param res_index: Residue index
    :type res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.

  .. method:: GetLastIndex(res_index)

    :return: Last atom position index for residue at index *res_index*.
    :rtype:  :class:`int`
    :param res_index: Residue index
    :type res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.

  .. method:: GetAA(res_index)

    :return: Amino acid type of residue at index *res_index*.
    :rtype:  :class:`ost.conop.AminoAcid`
    :param res_index: Residue index
    :type res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.

  .. method:: IsAnySet(res_index)

    :return: True, if any atom position of residue at index *res_index* is set.
    :rtype:  :class:`bool`
    :param res_index: Residue index
    :type res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.

  .. method:: IsAllSet(res_index)

    :return: True, if all atom positions of residue at index *res_index* are set.
    :rtype:  :class:`bool`
    :param res_index: Residue index
    :type res_index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds.


  .. method:: GetPhiTorsion(res_index, def_angle=-1.0472)

    :return: Phi torsion angle of residue at index *res_index* or *def_angle*
             if required atom positions (C-N-CA-C) are not set.
    :rtype:  :class:`float`
    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param def_angle: Default phi angle.
    :type def_angle:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* - 1 or *res_index*
             out of bounds.

  .. method:: GetPsiTorsion(res_index, def_angle=-0.7854)

    :return: Psi torsion angle of residue at index *res_index* or *def_angle*
             if required atom positions (N-CA-C-N) are not set.
    :rtype:  :class:`float`
    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param def_angle: Default psi angle.
    :type def_angle:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* or *res_index* + 1
             out of bounds.

  .. method:: GetOmegaTorsion(res_index, def_angle=3.14159)

    :return: Omega torsion angle of residue at index *res_index* or *def_angle*
             if required atom positions (CA-C-N-CA) are not set.
             Here, we use CA-C of residue *res_index* - 1 and N-CA of residue
             *res_index* (consistent with OST's
             :meth:`~ost.mol.ResidueHandle.GetOmegaTorsion`).
    :rtype:  :class:`float`
    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param def_angle: Default omega angle.
    :type def_angle:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* - 1 or *res_index*
             out of bounds.

  .. method:: GetNumAtoms()

    :return: Number of atom positions stored in this container.
    :rtype:  :class:`int`

  .. method:: GetNumResidues()

    :return: Number of residues stored in this container.
    :rtype:  :class:`int`

  .. method:: GetSequence()

    :return: Sequence of one letter codes of all residues stored here.
    :rtype:  :class:`str`


  .. method:: Copy()

    :return: Full copy of this object.
    :rtype:  :class:`AllAtomPositions`

  .. method:: Extract(from, to)

    :return: Container with residues with indices in range [*from*, *to*-1].
    :rtype:  :class:`AllAtomPositions`
    :param from: First residue index
    :type from:  :class:`int`
    :param to: One after last residue index
    :type to:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *from* >= *to* or if any residue
             index is out of bounds.

  .. method:: Extract(res_indices)

    :return: Container with residues with indices in *res_indices*.
    :rtype:  :class:`AllAtomPositions`
    :param res_indices: List of residue index
    :type res_indices:  :class:`list` of :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if any residue index is out of
             bounds.

  .. method:: ExtractBackbone(from, to)

    :return: Backbone list of residues with indices in range [*from*, *to*-1].
             CB atoms are reconstructed if unset.
    :rtype:  :class:`BackboneList`
    :param from: First residue index
    :type from:  :class:`int`
    :param to: One after last residue index
    :type to:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *from* >= *to*, if any residue
             index is out of bounds or if any residue has any unset backbone
             atom (N, CA, C, O).

  .. method:: ToEntity()

    :return: All residues packed in a single chain as an OST entity.
             Connectivity resolved with :class:`ost.conop.HeuristicProcessor`.
    :rtype:  :class:`ost.mol.EntityHandle`

  .. method:: InsertInto(res_index, chain, res_num)

    Insert a single residue (taken from given index) into the *chain* (with
    given res. number). Existing data is replaced and atoms are (re)connected
    according to the default connectivity of that amino acid. Peptide links to
    neighboring residues are set according to residue numbering. To make this
    function efficient, we require the backbone atoms (N, C, CA) to be set.

    :param res_index: Residue index
    :type res_index:  :class:`int`
    :param chain: Chain into which we insert
    :type chain:  :class:`ost.mol.ChainHandle`
    :param start_resnum: Residue number for the inserted residue
    :type start_resnum:  :class:`int` / :class:`ost.mol.ResNum`

    :raises: :exc:`~exceptions.RuntimeError` if *res_index* out of bounds, if
             *chain* is invalid or if not all backbone atoms (N, C, CA) set.


The AllAtomEnvPositions class
--------------------------------------------------------------------------------

.. class:: AllAtomEnvPositions

  To link the arbitrary amino acid sequence defined in :class:`AllAtomPositions`
  and the SEQRES of :class:`AllAtomEnv`, we provide a helper class containing
  structural data as well as a mapping to the internal residue indices of
  :class:`AllAtomEnv`.

  .. attribute:: all_pos

    Container for the positions of all heavy atoms of some residues.

    :type: :class:`AllAtomPositions`

  .. attribute:: res_indices

    Residue indices to be used by :class:`AllAtomEnv` for each residue defined
    in *all_pos*.

    :type: :class:`list` of :class:`int`


Distinguishing amino acid atoms
--------------------------------------------------------------------------------

.. class:: AminoAcidAtom
  
  Enumerates all heavy atoms of all amino acids. The naming scheme is TLC_AN,
  where TLC is the standard three letter code of the amino acid and AN is the
  atom name (standard PDB naming) of the heavy atom. Examples: *ALA_CB*,
  *ARG_CA*, *ASN_C*, *ASP_O*, *CYS_SG*, *GLU_OE1*, *GLN_NE2*, *GLY_N*.

  We include all heavy atoms that amino acids have when they are peptide bound
  to other residues (i.e. no OXT).

  Additionally, there is the value *XXX_NUM_ATOMS*, which corresponds to the
  number of atoms in the enumerator. Each heavy atom hence corresponds to an
  integer in the range [0, *XXX_NUM_ATOMS*-1].

.. class:: AminoAcidHydrogen
  
  Enumerates all hydrogens of all amino acids. The naming scheme is TLC_AN,
  where TLC is the standard three letter code of the amino acid and AN is the
  atom name (standard PDB naming) of the hydrogen. Examples: *ALA_H*,
  *ARG_HD3*, *ASN_HB2*, *ASP_HA*, *CYS_HB3*, *LEU_H*.

  We include all hydrogens that amino acids can have including *H1* (def. PDB
  name = "H"), *H2* and (if not PRO) *H3* for charged N-terminal residues. Note
  that the H atom attached to N when peptide bound (*H*) is distinct from the N
  terminal hydrogens (e.g. *ALA_H* != *ALA_H1*). For *HIS* we consider the fully
  protonated state, while *ASP* and *GLU* are included in their charged state.

  Additionally, there is the value *XXX_NUM_HYDROGENS*, which corresponds to the
  number of hydrogens in the enumerator. Each hydrogen hence corresponds to an
  integer in the range [0, *XXX_NUM_HYDROGENS*-1].

.. class:: AminoAcidLookup
  
  Collection of static methods to lookup properties of amino acid types
  (:class:`ost.conop.AminoAcid`), heavy atom types (:class:`AminoAcidAtom`) and
  hydrogen types (:class:`AminoAcidHydrogen`).

  .. staticmethod:: GetOLC(aa)

    :return: One letter code for the given amino acid
    :rtype:  :class:`str`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`

  .. staticmethod:: GetAAA(aa, atom_idx)
                    GetAAA(aa, atom_name)

    :return: Heavy atom type for the given amino acid and atom.
    :rtype:  :class:`AminoAcidAtom`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`
    :param atom_idx: Atom index (in [0, GetNumAtoms(aa)-1])
    :type atom_idx:  :class:`int`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *atom_idx* out of bounds or if
             *atom_name* is not one of the heavy atoms of *aa*.

  .. staticmethod:: GetAAH(aa, atom_idx)
                    GetAAH(aa, atom_name)

    :return: Hydrogen type for the given amino acid and atom.
    :rtype:  :class:`AminoAcidHydrogen`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`
    :param atom_idx: Atom index (in [0, GetNumHydrogens(aa)-1])
    :type atom_idx:  :class:`int`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *atom_idx* out of bounds or if
             *atom_name* is not one of the hydrogens of *aa*.

  .. staticmethod:: GetIndex(aa, atom_name)

    :return: Atom index (in [0, GetNumAtoms(aa)-1]) for the given amino acid and
             atom.
    :rtype:  :class:`int`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *atom_name* is not one of the
             heavy atoms of *aa*.

  .. staticmethod:: GetHydrogenIndex(aa, atom_name)

    :return: Atom index (in [0, GetNumHydrogens(aa)-1]) for the given amino acid
             and atom.
    :rtype:  :class:`int`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *atom_name* is not one of the
             hydrogens of *aa*.

  .. staticmethod:: GetNumAtoms(aa)

    :return: Number of heavy atoms of the given amino acid
    :rtype:  :class:`int`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`

  .. staticmethod:: GetMaxNumAtoms()

    :return: Max. number of heavy atoms for any amino acid
    :rtype:  :class:`int`

  .. staticmethod:: GetNumHydrogens(aa)

    :return: Number of hydrogens of the given amino acid
    :rtype:  :class:`int`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`

  .. staticmethod:: GetMaxNumHydrogens()

    :return: Max. number of hydrogens for any amino acid
    :rtype:  :class:`int`

  .. staticmethod:: GetAA(aaa)
                    GetAA(aah)

    :return: Amino acid type of the given heavy atom type
    :rtype:  :class:`~ost.conop.AminoAcid`
    :param aaa: Heavy atom type
    :type aaa:  :class:`AminoAcidAtom`
    :param aah: Hydrogen type
    :type aah:  :class:`AminoAcidHydrogen`

  .. staticmethod:: GetAtomName(aaa)
                    GetAtomName(aah)
                    GetAtomNameCharmm(aaa)
                    GetAtomNameCharmm(aah)
                    GetAtomNameAmber(aaa)
                    GetAtomNameAmber(aah)

    :return: Atom name of the given heavy atom type according to PDB (default),
             CHARMM or AMBER naming.
    :rtype:  :class:`str`
    :param aaa: Heavy atom type
    :type aaa:  :class:`AminoAcidAtom`
    :param aah: Hydrogen type
    :type aah:  :class:`AminoAcidHydrogen`

  .. staticmethod:: GetElement(aaa)

    :return: Chemical element of the given heavy atom type
    :rtype:  :class:`str`
    :param aaa: Heavy atom type
    :type aaa:  :class:`AminoAcidAtom`

  .. staticmethod:: GetAnchorAtomIndex(aah)

    :return: Atom index (in [0, GetNumAtoms(GetAA(aah))-1]) of the anchor to
             which the given hydrogen is attached.
    :rtype:  :class:`int`
    :param aah: Hydrogen type
    :type aah:  :class:`AminoAcidHydrogen`

  .. staticmethod:: GetHNIndex(aa)

    :return: Atom index (in [0, GetNumHydrogens(aa)-1]) of H atom attached to N
             when residue is peptide bound.
    :rtype:  :class:`int`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`

    :raises: :exc:`~exceptions.RuntimeError` if no such atom (i.e. PRO)

  .. staticmethod:: GetH1Index(aa)
                    GetH2Index(aa)
                    GetH3Index(aa)

    :return: Atom index (in [0, GetNumHydrogens(aa)-1]) of H atom attached to N
             when residue is N terminal.
    :rtype:  :class:`int`
    :param aa: Amino acid type
    :type aa:  :class:`~ost.conop.AminoAcid`

    :raises: :exc:`~exceptions.RuntimeError` if no such atom (i.e. H3 for PRO)
