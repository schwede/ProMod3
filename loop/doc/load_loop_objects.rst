..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Loading Precomputed Objects
================================================================================

.. currentmodule:: promod3.loop


Several data objects are used throughout the loop module.
|project| offers to load precomputed instances for direct usage.

.. method:: LoadTorsionSampler(seed=0)

  Loads and returns a torsion sampler with an amino acid grouping
  as defined by [solis2006]_ that has been trained on 
  non-redundant protein structures.

  :param seed:          Seed for internal random number generator
  :type seed:           :class:`int`

  :returns:             The torsion sampler

  :rtype:               :class:`TorsionSampler`


.. method:: LoadTorsionSamplerCoil(seed=0)

  Loads and returns a torsion sampler with an amino acid grouping
  as defined by [solis2006]_ that has been trained on coil
  residues of  non-redundant protein structures.

  :param seed:          Seed for internal random number generator
  :type seed:           :class:`int`

  :returns:             The torsion sampler

  :rtype:               :class:`TorsionSampler`


.. method:: LoadTorsionSamplerHelical(seed=0)

  Loads and returns a torsion sampler with an amino acid grouping
  as defined by [solis2006]_ that has been trained on helical
  residues of  non-redundant protein structures.

  :param seed:          Seed for internal random number generator
  :type seed:           :class:`int`

  :returns:             The torsion sampler

  :rtype:               :class:`TorsionSampler`


.. method:: LoadTorsionSamplerExtended(seed=0)

  Loads and returns a torsion sampler with an amino acid grouping
  as defined by [solis2006]_ that has been trained on extended
  residues of  non-redundant protein structures.

  :param seed:          Seed for internal random number generator
  :type seed:           :class:`int`

  :returns:             The torsion sampler

  :rtype:               :class:`TorsionSampler`


.. method:: LoadStructureDB()

  Loads and returns a structure db containing roughly 21000 chains form the
  PDB with seqid redundancy cutoff of 60%

  :returns:             The structure db
  :rtype:               :class:`StructureDB`


.. method:: LoadFragDB()

  Loads and returns a FragDB containing fragments up to the length of 14,
  therefore capable of bridging gaps up to the length of 12. The returned
  databases contains the location of fragments in the :class:`StructureDB`
  returned by :meth:`LoadStructureDB`.

  :returns:             The Fragment database
  :rtype:               :class:`FragDB`
  