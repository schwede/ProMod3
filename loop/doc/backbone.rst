..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Representing Loops
================================================================================

.. currentmodule:: promod3.loop


The most simple representation of structural information in |project| is the
:class:`BackboneList`. It provides a way to store the backbone positions of
residues. They provide structural manipulations, they can be manipulated and
converted from, to, or inserted to a :class:`ost.mol.EntityHandle`.

.. literalinclude:: ../../../tests/doc/scripts/loop_backbone.py


The BackboneList class
--------------------------------------------------------------------------------

.. class:: BackboneList()
  
  Container for the positions of the backbone atoms (nitrogen (N), alpha carbon
  (CA), beta carbon (CB), carbon (C), oxygen (O)), the one letter codes and
  amino acid types of a segment. This object allows to get, set or modify the
  atom positions, etc through a set of functions (**indexing to access each
  residue starts at 0**). To simplify algorithms, which use this structure, each
  residue has a CB position defined (i.e. even if it's a glycine) and we only
  allow amino acid types belonging to the 20 default amino acids. Note that the
  omega torsion angle defined here for residue *i* is the dihedral between CA-C
  of residue *i* and N-CA of residue *i+1* (this is shifted by 1 residue
  compared to OST's :meth:`~ost.mol.ResidueHandle.GetOmegaTorsion`).

  .. method:: BackboneList()

    Creates empty BackboneList

  .. method:: BackboneList(sequence)

    Creates a helical BackboneList from given sequence

    :param sequence:    Sequence of created BackboneList

    :type sequence:     :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *sequence* contains a one letter
             code which is not one of the 20 default amino acids.

  .. method:: BackboneList(sequence, dihedral_angles)

    Creates a BackboneList from given *sequence* and *dihedral_angles*

    :param sequence:    Sequence of created BackboneList
    :param dihedral_angles: List of :class:`tuple` objects defining the backbone
                            dihedral angles of created BackboneList. Every
                            :class:`tuple` must either have two or three
                            elements. Two elements are considered to define the
                            phi and psi angles, leading to an idealized omega
                            angle of 180 degrees. In case of three elements, all
                            angles are defined.

    :type sequence:     :class:`str`
    :type dihedral_angles: :class:`list`

    :raises: :exc:`~exceptions.RuntimeError` if *sequence* contains a one letter
             code which is not one of the 20 default amino acids or if
             *sequence* and *dihedral_angles* are inconsistent in size.

  .. method:: BackboneList(residues)

    Creates a BackboneList with positions and sequence extracted from
    *residues*.

    :param residues:    List of :class:`ost.mol.ResidueHandle` objects from
                        which the backbone positions and one letter codes
                        are extracted.

    :type residues:     :class:`list`

    :raises: :exc:`~exceptions.RuntimeError` if a residue in *residues*
              contains a one letter code which is not one of the 20 default
              amino acids or when there is a residue not providing all
              required positions.

  .. method:: BackboneList(sequence, residues)

    Creates a BackboneList from given *sequence* and positions extracted from
    *residues*.

    :param sequence:    Sequence of created BackboneList
    :param residues:    List of :class:`ost.mol.ResidueHandle` objects from
                        which the backbone positions are extracted.

    :type sequence:     :class:`str`
    :type residues:     :class:`list`

    :raises: :exc:`~exceptions.RuntimeError` if *sequence* contains a one letter
             code which is not one of the 20 default amino acids or if
             *sequence* and *residues* are inconsistent in size or when there is
             a residue not providing all necessary positions. 


  .. method:: ToDensity(padding=10.0, sampling=Vec3(1.0,1.0,1.0), \
                        resolution=3.0, high_resolution=false)
    
    :return: The whole backbone list converted to a density map.
    :rtype:  :class:`ost.img.ImageHandle`

    .. note that ImageHandle and MapHandle are the same, but former is mentioned
       in OST doc, so we use that for documentation purposes

    :param padding:
    :type padding:  :class:`float`
    :param sampling:
    :type sampling:  :class:`~ost.geom.Vec3`
    :param resolution:
    :type resolution:  :class:`float`
    :param high_resolution:
    :type high_resolution:  :class:`bool`

  .. method:: ToEntity()

    :return: The whole backbone list converted to an OST entity.
    :rtype:  :class:`ost.mol.EntityHandle`

  .. method:: InsertInto(chain, start_resnum)

    Inserts the backbone list into the *chain*. If the residues corresponding
    to the :class:`BackboneList` are already present in the entity, they will
    be replaced, otherwise they will be added to the entity.

    :param chain:        The chain
    :type chain:         :class:`ost.mol.ChainHandle`
    :param start_resnum: Residue number defining the start location of insertion
    :type start_resnum:  :class:`int` / :class:`ost.mol.ResNum`

  .. method:: InsertInto(map, resolution=3.0, high_resolution=false)

    :param map: 
    :type map:  :class:`ost.img.ImageHandle`
    :param resolution:
    :type resolution:  :class:`float`
    :param high_resolution:
    :type high_resolution:  :class:`bool`

  .. method:: GetBounds(all_atom=True)

    :returns:
    :rtype:   :class:`ost.geom.AlignedCuboid`
    :param all_atom: 
    :type all_atom:  :class:`bool`


  .. method:: GetSequence()

    :return: The amino acid sequence.
    :rtype:  :class:`str`

  .. method:: SetSequence(sequence)

    :param sequence: Set amino acid sequence to this.
    :type sequence:  :class:`str`

    :raises: :exc:`~exceptions.RuntimeError` if *sequence* contains a one letter
             code which is not one of the 20 default amino acids or size of 
             *sequence* does not match.


  .. method:: Extract(from, to)

    :returns: Items with indices *from*, ..., *to*-1 of this list.
    :rtype:   :class:`BackboneList`

  .. method:: ReplaceFragment(sub_fragment, index, superpose_stems)

    Replaces a fragment of the :class:`BackboneList` starting at position
    *index* by the *sub_fragment*.

    :param sub_fragment:          The fragment to be inserted
    :param index:                 The position at which the fragment 
                                  replacement will begin
    :param superpose_stems:       If set to false, the function will simply
                                  replace the according 
                                  :class:`Backbone` objects. If set to True,
                                  the n-terminal and c-terminal tails are superposed onto the *sub_fragment* 
                                  stems using the positions at *index* and 
                                  *index* + len( *sub_fragment* )-1.
    :type sub_fragment:           :class:`BackboneList`
    :type index:                  :class:`int`
    :type superpose_stems:        :class:`bool`

    :raises:                      :exc:`~exceptions.RuntimeError` if
                                  *sub_fragment* does not fully fit into
                                  actual fragment at specified *index*


  .. method:: GetN(index)
              GetCA(index)
              GetCB(index)
              GetC(index)
              GetO(index)

    :return: Position of nitrogen / alpha carbon / beta carbon / carbon / oxygen
             atom for residue at given index.
    :rtype:  :class:`~ost.geom.Vec3`
    :param index: Residue index.
    :type index:  :class:`int`

  .. method:: SetN(index, pos)
              SetCA(index, pos)
              SetCB(index, pos)
              SetC(index, pos)
              SetO(index, pos)

    :param index: Residue index.
    :type index:  :class:`int`
    :param pos: Set position of nitrogen / alpha carbon / beta carbon / carbon
                / oxygen atom to this.
    :type pos:  :class:`~ost.geom.Vec3`

  .. method:: GetOLC(index)

    :return: One letter code of the residue at given index.
    :rtype:  :class:`char`
    :param index: Residue index.
    :type index:  :class:`int`

  .. method:: SetOLC(index, olc)

    :param index: Residue index.
    :type index:  :class:`int`
    :param olc: Set one letter code of the residue to this.
    :type olc:  :class:`char`

  .. method:: GetAA(index)

    :return: Amino acid type of the residue at given index.
    :rtype:  :class:`ost.conop.AminoAcid`
    :param index: Residue index.
    :type index:  :class:`int`

  .. method:: SetAA(index, aa)

    :param index: Residue index.
    :type index:  :class:`int`
    :param aa: Set amino acid type of the residue to this.
    :type aa:  :class:`ost.conop.AminoAcid`

    :raises: :exc:`~exceptions.RuntimeError` if *aa* == ost.conop.XXX.


  .. method:: Set(index, n_pos, ca_pos, cb_pos, c_pos, o_pos, olc)
              Set(index, n_pos, ca_pos, c_pos, o_pos, olc)
              Set(index, res, olc)

    Set all backbone informations. This will reconstruct CB positions if needed
    and set the amino acid type according to the given one letter code.

    :param index: Residue index.
    :type index:  :class:`int`
    :param res: Residue from which to extract backbone atom positions
    :type res:  :class:`~ost.mol.ResidueHandle`
    :param n_pos: Set position of nitrogen atom to this.
    :type n_pos:  :class:`~ost.geom.Vec3`
    :param ca_pos: Set position of alpha carbon atom to this.
    :type ca_pos:  :class:`~ost.geom.Vec3`
    :param cb_pos: Set position of beta carbon atom to this.
    :type cb_pos:  :class:`~ost.geom.Vec3`
    :param c_pos: Set position of carbon atom to this.
    :type c_pos:  :class:`~ost.geom.Vec3`
    :param o_pos: Set position of oxygen atom to this.
    :type o_pos:  :class:`~ost.geom.Vec3`
    :param olc: Set one letter code of the residue to this.
    :type olc:  :class:`char`


  .. method:: __len__()

    :returns: Number of residues in this backbone list.
    :rtype:   :class:`int`

  .. method:: resize(new_size)

    :param new_size: Resize backbone list to contain this number of residues.
                     If new residues are added, their properties will not be
                     initialized. Existing residues are untouched unless the
                     list is shrinked (in that case extra residues are deleted).
    :type new_size:  :class:`int`

  .. method:: empty()

    :returns: True if the list is empty (i.e. size 0).
    :rtype:   :class:`bool`

  .. method:: append(n_pos, ca_pos, cb_pos, c_pos, o_pos, olc)
              append(n_pos, ca_pos, c_pos, o_pos, olc)
              append(res, olc)

    Appends a new residue at the end of the :class:`BackboneList`. This will
    reconstruct CB positions if needed and set the amino acid type according
    to the given one letter code.

    :param res: Residue from which to extract backbone atom positions
    :type res:  :class:`~ost.mol.ResidueHandle`
    :param n_pos: Set position of nitrogen atom to this.
    :type n_pos:  :class:`~ost.geom.Vec3`
    :param ca_pos: Set position of alpha carbon atom to this.
    :type ca_pos:  :class:`~ost.geom.Vec3`
    :param cb_pos: Set position of beta carbon atom to this.
    :type cb_pos:  :class:`~ost.geom.Vec3`
    :param c_pos: Set position of carbon atom to this.
    :type c_pos:  :class:`~ost.geom.Vec3`
    :param o_pos: Set position of oxygen atom to this.
    :type o_pos:  :class:`~ost.geom.Vec3`
    :param olc: Set one letter code of the residue to this.
    :type olc:  :class:`char`

  .. method:: clear()

    Remove all residues from this list.

  .. method:: Copy()
    
    Creates and returns a deep copy of this :class:`BackboneList`. This can be
    useful, since Python uses reference assignments.

    :rtype:  :class:`BackboneList`

  .. method:: ReconstructCBetaPositions()

    Does a simple reconstruction of all CB positions based on the current
    N, CA and C positions.

  .. method:: ReconstructOxygenPositions(last_psi=-0.78540)

    Does a simple reconstruction of all oxygen positions based on the actual
    N, CA and C positions. The position of the last oxygen depends on the next
    residue, an additional parameter is therefore required.

    :param last_psi:    Psi angle of the last BackboneList residue,
                        the default value corresponds to a typical alpha-helix.
    :type last_psi:     :class:`float`

  .. method:: ReconstructCStemOxygen(after_c_stem)

    Reconstructs the last oxygen of this backbone list. The oxygen position
    depends on the residue following the C stem. The position is only
    reconstructed if the residue handle is valid.

    :param after_c_stem: Residue following the C stem (C stem residue is last
                         element of this backbone list)
    :type after_c_stem:  :class:`ost.mol.ResidueHandle`


  .. method:: ApplyTransform(index, transform)

    Applies a transformation to the positions of a single residue.

    :param index: Residue index.
    :type index:  :class:`int`
    :param transform:  The transformation
    :type transform:   :class:`ost.geom.Mat4`

  .. method:: ApplyTransform(from, to, transform)

    Applies a transformation to the positions of the residues with indices
    *from*, ..., *to*-1 of this list.

    :param from: Start index.
    :type from:  :class:`int`
    :param to: End index (one past last residue to transform).
    :type to:  :class:`int`
    :param transform:  The transformation
    :type transform:   :class:`ost.geom.Mat4`

  .. method:: ApplyTransform(transform)

    Applies a transformation to all positions of this list.

    :param transform:  The transformation
    :type transform:   :class:`ost.geom.Transform` / :class:`ost.geom.Mat4`

  .. method:: GetTransform(index, res)
              GetTransform(index, other, other_index)

    :returns: Minimum RMSD transformation of residue *index* onto *res* or
              residue *other_index* of *other* backbone list considering the
              positions of the N, CA and C atoms.
    :rtype:   :class:`ost.geom.Mat4`

    :param index: Residue index.
    :type index:  :class:`int`
    :param res:   The other residue.
    :type res:    :class:`ost.mol.ResidueHandle`
    :param other: The other backbone list.
    :type other:  :class:`BackboneList`
    :param other_index: Residue index in *other* backbone list.
    :type other_index:  :class:`int`

  .. method:: GetTransform(other)

    :returns: Get minimum RMSD transformation of CA positions of this backbone
              list onto CA positions of *other* backbone list.
    :rtype:   :class:`ost.geom.Mat4`

    :param other: The other backbone list.
    :type other:  :class:`BackboneList`

  .. method:: SuperposeOnto(other)

    Superposes this backbone list onto another one using CA positions.

    :param other: The other backbone list.
    :type other:  :class:`BackboneList`


  .. method:: RotateAroundPhiTorsion(index, phi, sequential=False)
              RotateAroundPsiTorsion(index, psi, sequential=False)
              RotateAroundOmegaTorsion(index, omega, sequential=False)
              RotateAroundPhiPsiTorsion(index, phi, psi, sequential=False)
              SetAroundPhiTorsion(index, phi, sequential=False)
              SetAroundPsiTorsion(index, psi, sequential=False)
              SetAroundOmegaTorsion(index, omega, sequential=False)
              SetAroundPhiPsiTorsion(index, phi, psi, sequential=False)

    Rotates/sets the phi/psi/omega torsion angle of the backbone at position
    *index* in the backbone list by/to the given angle(s).

    :param index: Residue index.
    :type index:  :class:`int`
    :param phi: Angle by which to rotate phi torsion.
    :type phi:  :class:`float`
    :param psi: Angle by which to rotate psi torsion.
    :type psi:  :class:`float`
    :param omega: Angle by which to rotate omega torsion.
    :type omega:  :class:`float`
    :param sequential: If *True*, the rotation will be propagated to all
                       residues after the one with *index*. Otherwise, it will
                       be propagated in the direction where it will affect the
                       least number of residues.
    :type sequential:  :class:`bool`

  .. method:: GetPhiTorsion(index)
              GetPsiTorsion(index)
              GetOmegaTorsion(index)

    :return: The phi/psi/omega torsion angle for residue at given index.
    :rtype:  :class:`float`
    :param index: Residue index.
    :type index:  :class:`int`


  .. method:: TransOmegaTorsions(thresh=20/180*pi, allow_prepro_cis=True)

    Checks whether this backbone list only contains trans omega torsion angles.
    Usually, you would want this to be the case, but it can fail if you have any
    unfavorable omega torsion angle in your backbone.

    :param thresh: Allowed deviation from ideal trans angle (pi)
    :type thresh:  :class:`float`
    :param allow_prepro_cis: Flag, whether cis-omega torsions should be
                             allowed in case of a subsequent proline.
    :type allow_prepro_cis:  :class:`bool`

    :returns: whether *bb_list* only contains trans-omega torsions.
    :rtype:   :class:`bool`

    :raises: :exc:`~exceptions.RuntimeError` if size of this backbone list is
             smaller than 3.

  .. method:: SetBackrub(index, primary_rot_angle, flanking_rot_angle_one, flanking_rot_angle_two)

    Applies a backrub motion [davis2006]_ at residue defined by **index**. 
    The first rotation axis is defined by the CA positions from residues at
    **index** -1 and **index** +1. All atoms in between get rotated around this
    axis by **primary_rot_angle**. To restore the the hydrogen bond network
    of the two transformed oxygens, the backrub motion gets completed by
    two compensatory rotations. The first rotation is around the axis defined by 
    the CA position from residue **index**-1 and the transformed central CA
    position with an angle of **flanking_rot_angle_one**. The second rotation is 
    around the axis from the transformed central CA position to the CA position 
    from residue at position **index** +1 with an angle of 
    **flanking_rot_angle_two**.

    :param index:       Position of central residue of the backrub motion
    :param primary_rot_angle: Angle of the first rotation in radians
    :param flanking_rot_angle_one: Angle of the first compensatory rotation 
                                   in radians
    :param flanking_rot_angle_two: Angle of the second compensatory rotation 
                                   in radians

    :type index:        :class:`int`
    :type primary_rot_angle: :class:`float`
    :type flanking_rot_angle_one: :class:`float`
    :type flanking_rot_angle_two: :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if **index** is smaller 1 or
                                             larger size of BackboneList - 2

  .. method:: SetBackrub(index, primary_rot_angle, [scaling=1.0])

    Applies the backrub motion described above but calculates the ideal angles
    for the compensatory rotations in a way, that the new oxygen positions are
    as close as possible to the original ones. The ideal angles can be scaled
    down by **scaling** to make them less extreme.

    :param index:       Position of central residue of the backrub motion
    :param primary_rot_angle: Angle of the first rotation in radians
    :param scaling:     The ideal angles for the compensatory rotations will
                        be scaled by this value. 

    :type index:        :class:`int`
    :type primary_rot_angle: :class:`float`
    :type scaling:      :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if **index** is smaller 1 or
                                             larger size of BackboneList - 2



  .. method:: MinCADistance(other)

    :returns: Minimal pairwise CA-distance between this and the *other*
              backbone list.
    :rtype:   :class:`float`

    :param other: The other backbone list.
    :type other:  :class:`BackboneList`

  .. method:: RMSD(other, superposed_rmsd=False)
              CARMSD(other, superposed_rmsd=False)

    :returns: RMSD / C-alpha RMSD between this and the *other* backbone list.
    :rtype:   :class:`float`

    :param other: The other backbone list.
    :type other:  :class:`BackboneList`
    :param superposed_rmsd: Whether to superpose before calculating the RMSD.
    :type superposed_rmsd:  :class:`bool`
