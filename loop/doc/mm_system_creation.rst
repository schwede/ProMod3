..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Generate :mod:`ost.mol.mm` systems
================================================================================

.. currentmodule:: promod3.loop

To simplify the creation of :mod:`ost.mol.mm` / OpenMM simulations for loops in
proteins, we define a system creator for loops (:class:`MmSystemCreator`) and a
specialized forcefield lookup for amino acids (:class:`ForcefieldLookup`).

The example below showcases the creation and use of an MM system:

.. literalinclude:: ../../../tests/doc/scripts/loop_mm_sys_creation.py

.. note::

  To simplify use when sidechains may be missing and the region of interest for
  the MM system has to be determined, it might be better to use the simpler
  :class:`promod3.modelling.SidechainReconstructor` and
  :class:`promod3.modelling.AllAtomRelaxer` classes. Even if all the sidechains
  are available, those classes will be helpful since the overhead to check
  sidechains without reconstructing them is minimal.


Create MM systems for loops
--------------------------------------------------------------------------------

.. class:: MmSystemCreator(ff_lookup, fix_surrounding_hydrogens=True, \
                           kill_electrostatics=False, nonbonded_cutoff=8, \
                           inaccurate_pot_energy=False)

  Setup a system creator for a specific forcefield. The constructor only stores
  the settings. Most setup work is done by :meth:`SetupSystem`.

  The idea is to have a set of movable loop residues and a set of fixed
  surrounding residues which interact with the loop.

  :param ff_lookup: Forcefield to use with this system creator.
  :type ff_lookup:  :class:`ForcefieldLookup`
  :param fix_surrounding_hydrogens: If False, the hydrogens of the surrounding
                                    residues can move to improve H-bond building
                                    (True by default as it only has a minor
                                    impact on the result and a big one on
                                    performance).
  :type fix_surrounding_hydrogens:  :class:`bool`
  :param kill_electrostatics: If True, all charges are removed from the system.
                              This is good for H-bond building, but may be bad
                              if residues in the surrounding are missing (gaps).
  :type kill_electrostatics:  :class:`bool`
  :param nonbonded_cutoff: Defines cutoff to set for non bonded interactions.
                           Recommended values: 5 if kill_electrostatics = True,
                           8 otherwise. Negative value means no cutoff.
  :type nonbonded_cutoff:  :class:`float`
  :param inaccurate_pot_energy: If True, we do not set correct non-bonded
                                interactions between fixed atoms. This leads
                                to inaccurate pot. energies but it is faster and
                                has no effect on simulation runs (e.g. ApplySD)
                                otherwise.
  :type inaccurate_pot_energy:  :class:`bool`

  .. method:: GetDisulfidBridges(all_pos, res_indices)

    :return: Pairs of indices (i,j), where res_indices[i] and res_indices[j] are
             assumed to have a disulfid bridge (CYS-SG pairs within 2.5 A).
    :rtype:  :class:`list` of :class:`tuple` with two :class:`int`

    :param all_pos: Provides positions for each *res_indices[i]*.
    :type all_pos:  :class:`AllAtomPositions`
    :param res_indices: Residue indices into *all_pos*.
    :type res_indices:  :class:`list` of :class:`int`

  .. method:: SetupSystem(all_pos, res_indices, loop_length, is_n_ter, \
                          is_c_ter, disulfid_bridges)
              SetupSystem(all_pos, res_indices, loop_start_indices, \
                          loop_lengths, is_n_ter, is_c_ter, disulfid_bridges)

    Setup a new system for the loop / surrounding defined by *all_pos* and
    *res_indices*. Positions are taken from *all_pos[res_indices[i]]* and each
    of them must have all heavy atoms defined. Residue *all_pos[i]* is assumed
    to be peptide bound to *all_pos[i-1]* unless res. *i* is labelled as
    N-terminal. If *all_pos[i-1]* has an unset C (could happen for gaps while
    modelling), res. *i* will still be treated as if peptide-bound and the
    hydrogen attached to N is built assuming a helical structure as an
    approximation. Similarly, we do not generate OXT atoms for residues followed
    by a gap unless they are specifically labelled as C-terminal.

    Each loop is defined by an entry in *loop_start_indices* and *loop_lengths*.
    For loop *i_loop*, *res_indices[loop_start_indices[i_loop]]* is the N-stem
    and *res_indices[loop_start_indices[i_loop] + loop_lengths[i_loop] - 1]* is
    the C-stem of the loop. The loop indices are expected to be contiguous in
    *res_indices* between the stems. The stems of the loop are kept fixed (N,
    CA, CB for N-stem / CA, CB, C, O for C-stem) unless they are terminal
    residues. Overlapping loops are merged and 0-length loops are removed.

    If *loop_length* is given, a single loop with start index 0 and the given
    loop length is defined.

    If a system was setup previously, it is overwritten completely.

    If possible, this uses the "CPU" platform in OpenMM using the env. variable
    ``PM3_OPENMM_CPU_THREADS`` to define the number of desired threads (1 thread
    is used if variable is undefined).

    :param all_pos: Provides positions for each *res_indices[i]*.
    :type all_pos:  :class:`AllAtomPositions`
    :param res_indices: Residue indices into *all_pos*.
    :type res_indices:  :class:`list` of :class:`int`
    :param loop_length: Length of single loop (incl. stems).
    :type loop_length:  :class:`int`
    :param loop_start_indices: Start indices of loops.
    :type loop_start_indices:  :class:`list` of :class:`int`
    :param loop_lengths: Lengths of loops (incl. stems).
    :type loop_lengths:  :class:`list` of :class:`int`
    :param is_n_ter: For each *res_indices[i]*, *is_n_ter[i]* defines whether
                     that residue is N-terminal.
    :type is_n_ter:  :class:`list` of :class:`bool`
    :param is_c_ter: For each *res_indices[i]*, *is_c_ter[i]* defines whether
                     that residue is C-terminal.
    :type is_c_ter:  :class:`list` of :class:`bool`
    :param disulfid_bridges: Pairs of indices (i,j), where res_indices[i] and
                             res_indices[j] form a disulfid bridge (see
                             :meth:`GetDisulfidBridges`)
    :type disulfid_bridges:  :class:`list` of :class:`tuple` with two
                             :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if loops out of bounds with respect
             to *res_indices*, *loop_start_indices* / *loop_lengths* or
             *res_indices* / *is_n_ter* / *is_c_ter* have inconsistent lengths,
             or if any *all_pos[res_indices[i]]* is invalid or has unset heavy
             atoms.

  .. method:: UpdatePositions(all_pos, res_indices)

    Updates the positions in the system. Even though :meth:`SetupSystem` is
    already very fast, this can speed up resetting a simulation. The data must
    be consistent with the data used in the last :meth:`SetupSystem` call.

    :param all_pos: Provides positions for each *res_indices[i]*.
    :type all_pos:  :class:`AllAtomPositions`
    :param res_indices: Residue indices into *all_pos*.
    :type res_indices:  :class:`list` of :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if data is inconsistent with last
             :meth:`SetupSystem` call (same number of residues, same AA).

  .. method:: ExtractLoopPositions(loop_pos)
              ExtractLoopPositions(out_pos, res_indices)

    Extracts loop positions from the current simulation. If the simulation was
    run (via :meth:`GetSimulation`), we internally have new positions for the
    residues corresponding to *all_pos[res_indices[i]]* passed in
    :meth:`SetupSystem`. This function then extracts these positions back into a
    compatible output storage.

    If *loop_pos* is passed, only the loops are stored. The loop residues are
    stored contiguously and the loops are stored in the order given by
    :meth:`GetLoopStartIndices` / :meth:`GetLoopLengths`. The *loop_pos* storage
    must have at least :meth:`GetNumLoopResidues` residues and must have
    matching amino acid types with respect to the loop residues passed as
    *all_pos* of :meth:`SetupSystem`.

    If *out_pos* and *res_indices* are passed, residues must have matching amino
    acid types for *out_pos[res_indices[i]]* and *all_pos[res_indices[i]]* of
    :meth:`SetupSystem`. Only loop residues with *i* in the ranges defined by
    :meth:`GetLoopStartIndices` / :meth:`GetLoopLengths` are touched.

    :param loop_pos: Reduced storage only covering loop positions.
    :type loop_pos:  :class:`AllAtomPositions`
    :param out_pos: Storage for loop positions linked to *res_indices*.
    :type out_pos:  :class:`AllAtomPositions`
    :param res_indices: Residue indices into *out_pos*.
    :type res_indices:  :class:`list` of :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if data is inconsistent with last
             :meth:`SetupSystem` call (big enough and matching AA).

  .. method:: GetSimulation

    :return: Simulation object setup by :meth:`SetupSystem`. Use this to run
             MM simulations.
    :rtype:  :class:`ost.mol.mm.Simulation`

  .. method:: GetNumResidues

    :return: Number of residues of current simulation setup.
    :rtype:  :class:`int`

  .. method:: GetNumLoopResidues

    :return: Number of loop residues (incl. stems) of current simulation setup.
    :rtype:  :class:`int`

  .. method:: GetLoopStartIndices

    :return: Start indices of loops (see :meth:`SetupSystem`).
    :rtype:  :class:`list` of :class:`int`

  .. method:: GetLoopLengths

    :return: Lengths of loops (see :meth:`SetupSystem`).
    :rtype:  :class:`list` of :class:`int`

  .. method:: GetForcefieldAminoAcids

    :return: Forcefield-specific amino acid type for each residue of last
             :meth:`SetupSystem` call.
    :rtype:  :class:`list` of :class:`ForcefieldAminoAcid`

  .. method:: GetIndexing

    The atoms of residue *i* are stored from *idx[i]* to *idx[i+1]-1*, where
    *idx* is the list returned by this function. The atoms themselves are
    ordered according to the indexing defined by :class:`ForcefieldLookup`.

    :return: Indexing to positions vector used by the simulation object.
             The last item of the list contains the number of atoms in the
             system.
    :rtype:  :class:`list` of :class:`int`

  .. method:: GetCpuPlatformSupport

    :return: True, if we will use OpenMM's "CPU" platform (enabled by default
             if platform is available). False, if we use "Reference" platform.
    :rtype:  :class:`bool`

  .. method:: SetCpuPlatformSupport(cpu_platform_support)

    Override "CPU" platform support setting. Useful to force the use of the
    OpenMM's "Reference" platform for testing (by default we use "CPU" if it is
    available).

    :param cpu_platform_support: True, if "CPU" platform desired (ignored if
                                 platform not available). False, if "Reference"
                                 platform desired.
    :type cpu_platform_support:  :class:`bool`


Forcefield lookup for amino acids
--------------------------------------------------------------------------------

The :class:`ForcefieldLookup` class and its helpers define a fast way to extract
FF specific data for amino acids in a protein. We distinguish amino acid types
(and a few variants thereof) which may all be N- and/or C-terminal.


.. class:: ForcefieldLookup

  This class provides all functionality to generate :class:`ost.mol.mm.Simulation` objects. Specifically, we can:

  - get a consistent indexing of each atom of each residue in [*0, N-1*], where
    *N* = :meth:`GetNumAtoms` (note that only OXT indexing depends on whether a
    residue is terminal)
  - extract masses, charges and LJ-parameters for each atom (list of length *N*)
  - extract connectivities (:class:`ForcefieldConnectivity`), which include all
    possible bonds, angles, dihedrals, impropers and LJ pairs

  There is functionality to adapt the lookup and store it as needed or you can
  load a predefined one with :meth:`GetDefault` or :meth:`LoadCHARMM`.

  The atom indexing and :meth:`GetAA` are independent of the loaded file.


  .. staticmethod:: GetDefault()

    :return: A default singleton instance (shared throughout the Python process)
             of this class with all data defined. Using this instance has the
             advantage that the object is only loaded once!
    :rtype:  :class:`ForcefieldLookup`

  .. staticmethod:: SetDefault(new_default)

    Sets default singleton instance for all future :meth:`GetDefault` calls.

    :param new_default: Lookup object to use as the new default.
    :type new_default:  :class:`ForcefieldLookup`

  .. staticmethod:: LoadCHARMM()

    :returns: Predefined lookup object extracted from a CHARMM forcefield
              (loaded from disk)
    :rtype:   :class:`ForcefieldLookup`

  .. staticmethod:: Load(filename)
                    LoadPortable(filename)

    Loads raw binary file generated with :meth:`Save` (optimized for fast
    reading) / portable file generated with :meth:`SavePortable` (slower but
    less machine-dependent).

    :param filename: Path to the file from which to load.
    :type filename:  :class:`str`

    :returns: The loaded lookup object
    :rtype:   :class:`ForcefieldLookup`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened or if
              file cannot be parsed (see :ref:`here <portableIO>` for details).

  .. method:: Save(filename)
              SavePortable(filename)

    Saves a raw / portable binary representation. Use portable files for
    distribution and convert locally to raw files. See :ref:`here <portableIO>`
    for details.

    :param filename: Path to the file where it will be saved.
    :type filename:  :class:`str`

    :raises:  :exc:`~exceptions.RuntimeError` if file cannot be opened.


  .. method:: GetAA(ff_aa)

    :return: Amino acid type for given *ff_aa*
    :rtype:  :class:`ost.conop.AminoAcid`

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`

  .. method:: GetNumAtoms(ff_aa, is_nter, is_cter)

    :return: Number of atoms for given input.
    :rtype:  :class:`int`

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`
    :param is_cter: True if C-terminal variant desired
    :type is_cter:  :class:`bool`

  .. method:: GetHeavyIndex(ff_aa, atom_idx)
              GetHeavyIndex(ff_aa, atom_name)

    :return: Internal index for given heavy atom in [0, :meth:`GetNumAtoms`]
    :rtype:  :class:`int`

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param atom_idx: Atom index as returned by :meth:`AminoAcidLookup.GetIndex`
    :type atom_idx:  :class:`int`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

  .. method:: GetHydrogenIndex(ff_aa, atom_idx)
              GetHydrogenIndex(ff_aa, atom_name)

    :return: Internal index for given hydrogen atom in [0, :meth:`GetNumAtoms`]
    :rtype:  :class:`int`

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param atom_idx: Atom index as returned by
                     :meth:`AminoAcidLookup.GetHydrogenIndex`
    :type atom_idx:  :class:`int`
    :param atom_name: Atom name
    :type atom_name:  :class:`str`

  .. method:: GetOXTIndex(ff_aa, is_nter)

    :return: Internal index of OXT atom for C-terminal residue
    :rtype:  :class:`int`

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`


  .. method:: GetFudgeLJ()

    :return: Dampening factor for LJ 1,4 interactions (see
             :meth:`ost.mol.mm.Topology.SetFudgeLJ`)
    :rtype:  :class:`float`

  .. method:: GetFudgeQQ()

    :return: Dampening factor for electrostatic 1,4 interactions (see
             :meth:`ost.mol.mm.Topology.SetFudgeQQ`)
    :rtype:  :class:`float`


  .. method:: GetMasses(ff_aa, is_nter, is_cter)

    :return: Mass for each atom (see :meth:`ost.mol.mm.Topology.SetMasses`)
    :rtype:  :class:`list` of :class:`float` (length = :meth:`GetNumAtoms`)

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`
    :param is_cter: True if C-terminal variant desired
    :type is_cter:  :class:`bool`

  .. method:: GetCharges(ff_aa, is_nter, is_cter)

    :return: Charge for each atom (see :meth:`ost.mol.mm.Topology.SetCharges`)
    :rtype:  :class:`list` of :class:`float` (length = :meth:`GetNumAtoms`)

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`
    :param is_cter: True if C-terminal variant desired
    :type is_cter:  :class:`bool`

  .. method:: GetSigmas(ff_aa, is_nter, is_cter)

    :return: Sigma in nm for each atom
             (see :meth:`ost.mol.mm.Topology.SetSigmas`)
    :rtype:  :class:`list` of :class:`float` (length = :meth:`GetNumAtoms`)

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`
    :param is_cter: True if C-terminal variant desired
    :type is_cter:  :class:`bool`

  .. method:: GetEpsilons(ff_aa, is_nter, is_cter)

    :return: Epsilon in kJ/mol for each atom
             (see :meth:`ost.mol.mm.Topology.SetEpsilons`)
    :rtype:  :class:`list` of :class:`float` (length = :meth:`GetNumAtoms`)

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`
    :param is_cter: True if C-terminal variant desired
    :type is_cter:  :class:`bool`

  .. method:: GetInternalConnectivity(ff_aa, is_nter, is_cter)

    :return: Internal connectivity of a residue
    :rtype:  :class:`ForcefieldConnectivity`

    :param ff_aa: Forcefield-specific amino acid type
    :type ff_aa:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if N-terminal variant desired
    :type is_nter:  :class:`bool`
    :param is_cter: True if C-terminal variant desired
    :type is_cter:  :class:`bool`

  .. method:: GetPeptideBoundConnectivity(ff_aa_one, ff_aa_two, is_nter,\
                                          is_cter)

    :return: All connectivity which include peptide bond between two residues
             (additional to :meth:`GetInternalConnectivity`)
    :rtype:  :class:`ForcefieldConnectivity`

    :param ff_aa_one: Forcefield-specific amino acid type of first residue
    :type ff_aa_one:  :class:`ForcefieldAminoAcid`
    :param ff_aa_two: Forcefield-specific amino acid type of second residue
    :type ff_aa_two:  :class:`ForcefieldAminoAcid`
    :param is_nter: True if first residue is N-terminal
    :type is_nter:  :class:`bool`
    :param is_cter: True if second residue is C-terminal
    :type is_cter:  :class:`bool`

  .. method:: GetDisulfidConnectivity()

    :return: All connectivity which include disulfid bridge between two cysteins
             (additional to :meth:`GetInternalConnectivity`)
    :rtype:  :class:`ForcefieldConnectivity`


  .. method:: SetFudgeLJ(fudge)

    Set value for future :meth:`GetFudgeLJ` calls to *fudge*.

  .. method:: SetFudgeQQ(fudge)

    Set value for future :meth:`GetFudgeQQ` calls to *fudge*.

  .. method:: SetMasses(ff_aa, is_nter, is_cter, masses)

    Set value for future :meth:`GetMasses` calls to *masses*.

  .. method:: SetCharges(ff_aa, is_nter, is_cter, charges)

    Set value for future :meth:`GetCharges` calls to *charges*.

  .. method:: SetSigmas(ff_aa, is_nter, is_cter, sigmas)

    Set value for future :meth:`GetSigmas` calls to *sigmas*.

  .. method:: SetEpsilons(ff_aa, is_nter, is_cter, epsilons)

    Set value for future :meth:`GetEpsilons` calls to *epsilons*.

  .. method:: SetInternalConnectivity(ff_aa, is_nter, is_cter, connectivity)

    Set value for future :meth:`GetInternalConnectivity` calls to
    *connectivity*.

  .. method:: SetPeptideBoundConnectivity(ff_aa_one, ff_aa_two, is_nter,\
                                          is_cter, connectivity)

    Set value for future :meth:`GetPeptideBoundConnectivity` calls to
    *connectivity*.

  .. method:: SetDisulfidConnectivity(connectivity)

    Set value for future :meth:`GetDisulfidConnectivity` calls to
    *connectivity*.


.. class:: ForcefieldAminoAcid

  Enumerates the amino acid types for forcefields. The first 20 values
  correspond to the 20 values of :class:`ost.conop.AminoAcid`. Additionally,
  there are values for disulfid bridges (*FF_CYS2*), d-protonated histidine
  (*FF_HISD*, default for *ost.conop.HIS* is *FF_HISE*) and *FF_XXX* for unknown
  types. The full list of values is:

    *FF_ALA*, *FF_ARG*, *FF_ASN*, *FF_ASP*, *FF_GLN*, *FF_GLU*, *FF_LYS*,
    *FF_SER*, *FF_CYS*, *FF_MET*, *FF_TRP*, *FF_TYR*, *FF_THR*, *FF_VAL*,
    *FF_ILE*, *FF_LEU*, *FF_GLY*, *FF_PRO* *FF_HISE*, *FF_PHE*, *FF_CYS2*,
    *FF_HISD*, *FF_XXX*


.. class:: ForcefieldConnectivity
  
  Contains lists of bonds, angles, dihedrals, impropers and LJ pairs (exclusions
  are the combination of all bonds and 1,3 pairs of angles and are not stored
  separately). Each type of connectivity has it's own class (see below) storing
  indices and parameters to be used for methods of :class:`ost.mol.mm.Topology`.
  The indexing of atoms for internal connectivities is in [*0, N-1*], where *N*
  = :meth:`ForcefieldLookup.GetNumAtoms`. For connectivities of pairs of
  residues, atoms of the first residue are in [*0, N1-1*] and atoms of the
  second one are in [*N1, N1+N2-1*], where *N1* and *N2* are the number of atoms
  of the two residues. For disulfid bridges, *N1* = *N2* = *GetNumAtoms(FF_CYS2,
  False, False)*.

  .. attribute:: harmonic_bonds
    
    List of harmonic bonds.
    
    :type: :class:`list` of :class:`ForcefieldBondInfo`

  .. attribute:: harmonic_angles
    
    List of harmonic angles.
    
    :type: :class:`list` of :class:`ForcefieldHarmonicAngleInfo`

  .. attribute:: urey_bradley_angles
    
    List of Urey-Bradley angles.
    
    :type: :class:`list` of :class:`ForcefieldUreyBradleyAngleInfo`

  .. attribute:: periodic_dihedrals
    
    List of periodic dihedrals.
    
    :type: :class:`list` of :class:`ForcefieldPeriodicDihedralInfo`

  .. attribute:: periodic_impropers
    
    List of periodic impropers.
    
    :type: :class:`list` of :class:`ForcefieldPeriodicDihedralInfo`

  .. attribute:: harmonic_impropers
    
    List of harmonic impropers.
    
    :type: :class:`list` of :class:`ForcefieldHarmonicImproperInfo`

  .. attribute:: lj_pairs
    
    List of LJ pairs.
    
    :type: :class:`list` of :class:`ForcefieldLJPairInfo`


.. class:: ForcefieldBondInfo

  Define harmonic bond (see :meth:`ost.mol.mm.Topology.AddHarmonicBond`)

  .. attribute:: index_one

    Index of particle 1

    :type: :class:`int`

  .. attribute:: index_two

    Index of particle 2

    :type: :class:`int`

  .. attribute:: bond_length

    Bond length in nm

    :type: :class:`float`

  .. attribute:: force_constant

    Force constant in kJ/mol/nm^2

    :type: :class:`float`


.. class:: ForcefieldHarmonicAngleInfo

  Define harmonic angle (see :meth:`ost.mol.mm.Topology.AddHarmonicAngle`)

  .. attribute:: index_one

    Index of particle 1

    :type: :class:`int`

  .. attribute:: index_two

    Index of particle 2

    :type: :class:`int`

  .. attribute:: index_three

    Index of particle 3

    :type: :class:`int`

  .. attribute:: angle

    Angle in radians

    :type: :class:`float`

  .. attribute:: force_constant

    Force constant in kJ/mol/radian^2 

    :type: :class:`float`


.. class:: ForcefieldUreyBradleyAngleInfo

  Define Urey-Bradley angle
  (see :meth:`ost.mol.mm.Topology.AddUreyBradleyAngle`)

  .. attribute:: index_one

    Index of particle 1

    :type: :class:`int`

  .. attribute:: index_two

    Index of particle 2

    :type: :class:`int`

  .. attribute:: index_three

    Index of particle 3

    :type: :class:`int`

  .. attribute:: angle

    Angle in radians

    :type: :class:`float`

  .. attribute:: angle_force_constant

    Angle force constant kJ/mol/radian^2 

    :type: :class:`float`

  .. attribute:: bond_length

    Bond length in nm

    :type: :class:`float`

  .. attribute:: bond_force_constant

    Bond force constant kJ/mol/nm^2 

    :type: :class:`float`
    

.. class:: ForcefieldPeriodicDihedralInfo
  
  Define periodic dihedral or improper (see
  :meth:`ost.mol.mm.Topology.AddPeriodicDihedral` and
  :meth:`ost.mol.mm.Topology.AddPeriodicImproper`)

  .. attribute:: index_one

    Index of particle 1

    :type: :class:`int`

  .. attribute:: index_two

    Index of particle 2

    :type: :class:`int`

  .. attribute:: index_three

    Index of particle 3

    :type: :class:`int`

  .. attribute:: index_four

    Index of particle 4

    :type: :class:`int`

  .. attribute:: multiplicity

    Multiplicity

    :type: :class:`int`

  .. attribute:: phase

    Phase in radians

    :type: :class:`float`

  .. attribute:: force_constant

    Force constant in kJ/mol/radian^2 

    :type: :class:`float`


.. class:: ForcefieldHarmonicImproperInfo

  Define harmonic improper (see :meth:`ost.mol.mm.Topology.AddHarmonicImproper`)
  
  .. attribute:: index_one

    Index of particle 1

    :type: :class:`int`

  .. attribute:: index_two

    Index of particle 2

    :type: :class:`int`

  .. attribute:: index_three

    Index of particle 3

    :type: :class:`int`

  .. attribute:: index_four

    Index of particle 4

    :type: :class:`int`

  .. attribute:: angle

    Angle in radians

    :type: :class:`float`

  .. attribute:: force_constant

    Force constant kJ/mol/radian^2 

    :type: :class:`float`


.. class:: ForcefieldLJPairInfo

  Define LJ pair (see :meth:`ost.mol.mm.Topology.AddLJPair`)

  .. attribute:: index_one

    Index of particle 1

    :type: :class:`int`

  .. attribute:: index_two

    Index of particle 2

    :type: :class:`int`

  .. attribute:: sigma

    Sigma in nm

    :type: :class:`float`

  .. attribute:: epsilon

    Epsilon in kJ/mol

    :type: :class:`float`

