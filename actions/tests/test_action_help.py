# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Unit tests for the 'help' action.

We do not test much here so this is a perfect example on how to test Promod3
actions.
"""
import sys

# this is needed so there will be no test_actions.pyc created in the source
# directory
sys.dont_write_bytecode = True

import test_actions

class HelpActionTests(test_actions.ActionTestCase):
    def __init__(self, *args, **kwargs):
        test_actions.ActionTestCase.__init__(self, *args, **kwargs)
        self.pm_action = 'help'

    def testExit0(self):
        self.RunExitStatusTest(0, list())

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
