# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Unit tests for the 'build-rawmodel' action.
"""
import sys

# this is needed so there will be no test_actions.pyc created in the source
# directory
sys.dont_write_bytecode = True

import test_actions
import os
from ost import io
from promod3.core import helper

class HelpActionTests(test_actions.ActionTestCase):
    def __init__(self, *args, **kwargs):
        test_actions.ActionTestCase.__init__(self, *args, **kwargs)
        self.pm_action = 'build-rawmodel'

    def testExit0(self):
        # test with def. help flag
        self.RunExitStatusTest(0, ['--help'])
        # test with proper inputs (and check resulting file)
        pdbpath = os.path.join('data', 'gly.pdb')
        faspath = os.path.join('data', 'seq.fasta')
        my_args = ['-p', pdbpath, '-f', faspath]
        self.RunExitStatusTest(0, my_args)
        io.LoadPDB('model.pdb')
        self.RunExitStatusTest(0, my_args + ['-o', 'mymodel.pdb'])
        io.LoadPDB('mymodel.pdb')
        # clean up this test
        os.remove('model.pdb')
        os.remove('mymodel.pdb')

    def testExitX(self):
        # test error codes
        pdbpath = os.path.join('data', 'gly.pdb')
        faspath = os.path.join('data', 'seq.fasta')
        my_args = ['-p', pdbpath, '-f', faspath]
        # 2 - wrong input (most of this tested in pm3argparse!)
        self.RunExitStatusTest(2, ['-p', pdbpath])
        # 3 - failed to generate raw model (internal error)
        # -> we cannot really test this one...
        # 4 - failed to write results to file
        failpath = os.path.join('cannotpossiblyexist', 'whatever.pdb')
        self.RunExitStatusTest(4, ['-o', failpath] + my_args)

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
