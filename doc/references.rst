..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


References
==========

.. [biasini2013] Biasini M, Schmidt T, Bienert S, Mariani V, Studer G, Haas J, 
                 Johner N, Schenk AD, Philippsen A and Schwede T (2013). 
                 OpenStructure: an integrated software framework for 
                 computational structural biology. Acta Cryst.

.. [canutescu2003] Canutescu AA and Dunbrack RL Jr. (2003). 
                   Cyclic coordinate descent: A robotics algorithm for protein 
                   loop closure. Protein Sci.

.. [canutescu2003b] Canutescu AA, Shelenkov AA, Dunbrack RL Jr. (2003). 
                    A graph-theory algorithm for rapid protein side-chain 
                    prediction. Protein Sci.

.. [coutsias2005] Coutsias EA, Seok C, Wester MJ, Dill KA (2005).
                  Resultants and loop closure. International Journal of Quantum 
                  Chemistry. 

.. [chakravarty1999] Chakravarty S, Varadarajan R (1999). 
                     Residue depth: a novel parameter for the analysis of 
                     protein structure and stability. Structure.

.. [davis2006] Davis IW, Arendall WB, Richardson DC, Richardson JS (2006).
               The backrub motion: how protein backbone shrugs when a sidechain 
               dances. Structure.

.. [goldstein1994] Goldstein RF (1994). 
                   Efficient rotamer elimination applied to protein side-chains 
                   and related spin glasses. Biophys J.

.. [Jones1999] Jones DT (1999). 
               Protein secondary structure prediction based on position-specific 
               scoring matrices. J. Mol. Biol. 

.. [kabsch1983] Kabsch W, Sander C (1983). 
                Dictionary of protein secondary structure: pattern recognition of 
                hydrogen-bonded and geometrical features. Biopolymers.

.. [krivov2009] Krivov GG, Shapovalov MV and Dunbrack RL Jr. (2009). 
                Improved prediction of protein side-chain conformations with 
                SCWRL4. Proteins.

.. [leach1998] Leach AR, Lemon AP (1998). 
               Exploring the conformational space of protein side chains using 
               dead-end elimination and the A* algorithm. Proteins.

.. [nussinov1991] Nussinov R and Wolfson HJ (1991).
                  Efficient detection of three-dimensional structural motifs in
                  biological macromolecules by computer vision techniques. PNAS.

.. [shapovalov2011] Shapovalov MV and Dunbrack RL Jr. (2011). 
                    A smoothed backbone-dependent rotamer library for proteins 
                    derived from adaptive kernel density estimates and 
                    regressions. Structure.

.. [soding2005] Söding J (2005). 
                Protein homology detection by HMM-HMM comparison. 
                Bioinformatics.

.. [solis2006] Solis AD, Rackovsky S (2006). Improvement of statistical 
               potentials and threading score functions using information 
               maximization. Proteins. 

.. [trott2010] Trott O, Olson AJ (2010). AutoDock Vina: improving the speed and 
               accuracy of docking with a new scoring function, efficient 
               optimization and multithreading. J Comput Chem

.. [zhou2005] Zhou H, Zhou Y (2005). 
              Fold Recognition by Combining Sequence Profiles Derived From 
              Evolution and From Depth-Dependent Structural Alignment of 
              Fragments. Proteins.

