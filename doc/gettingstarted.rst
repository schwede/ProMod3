..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Getting Started
================================================================================

Get and Run |project|
--------------------------------------------------------------------------------
Either utilize :ref:`containers <container>` or start from source code:

#. Fetch the source code from https://git.scicore.unibas.ch/schwede/ProMod3.git.
   Or directly pull it with git:

   .. code-block:: console
  
     $ git clone https://git.scicore.unibas.ch/schwede/ProMod3.git <DIR>  

#. Obtain all dependencies and compile |project| with ``cmake`` and ``make``
   (see :ref:`here <building-promod>`).
#. Ensure that you have a ``stage/bin`` folder which includes a ``pm`` executable.
   For convenience, add the folder to your ``PATH`` env. variable.
#. You can now execute |project| by running the following in your terminal:

  .. code-block:: console
    
    $ pm <COMMAND>
 
  Here ``<COMMAND>`` can be:

  - a predefined "action" (see :ref:`here <promod-actions>`)
  - the path to a python script, such as the following example:

    .. literalinclude:: ../../tests/doc/scripts/hello_world.py

    The execution of this script should generate a ``test.pdb`` file, which you
    can visualize with any pdb reader (e.g. ``dng`` from |ost_s|).


Modelling pipeline
--------------------------------------------------------------------------------
|project| is meant to help you perform operations for protein homology modelling.
Commonly, your input is a template structure and an alignment of the template to 
the desired target sequence. The modelling steps then are:

- Build a raw model from a template by copying the part of the template which
  is conserved
- Perform loop modelling to close all gaps (see :mod:`~promod3.loop` module)
- Reconstruct sidechains (using :mod:`~promod3.sidechain` module)
- Minimize energy of final model using molecular mechanics
  (using :mod:`ost.mol.mm` from |ost_s|)

Since a good amount of time is spent in OpenMM routines to minimize energy, we
try to use the fast and multi-threaded "CPU" platform of OpenMM (should be
included and working on any hardware supported by OpenMM). If the platform is
not available, we use the slower "Reference" platform. For the "CPU" platform,
multiple CPU threads can be used by setting the env. variable
``PM3_OPENMM_CPU_THREADS`` to the number of desired threads. If the variable is
not set, 1 thread will be used by default.
