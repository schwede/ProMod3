..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.

Contributing
================================================================================

Please contact the |project| developers if you implemented a modelling pipeline
that you believe is worth sharing with other users.
They can review the code and add it to the repository at:
https://git.scicore.unibas.ch/schwede/ProMod3. Consider
opening an account for the GitLab instance at sciCORE, University of Basel if
you directly want to contribute to |project|'s core features. You can find more
information on that matter in the developer section of the documentation
(:ref:`how-to-contribute`).
