..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.

.. _container:

|project| and Containers
========================


|project| offers build recipes for Docker and Singularity in 
<PATH_TO_PROMOD3_CHECKOUT>/container. To avoid code duplication, 
the Singularity container bootstraps from the Docker one and adds 
some sugar on top.

.. toctree::
  :maxdepth: 1

  Docker <docker>
  Singularity <singularity>






