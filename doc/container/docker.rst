..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Docker
======

|project|'s Own Docker Registry
-------------------------------

For the current stable release of |project|, its
`GitLab project <https://git.scicore.unibas.ch/schwede/ProMod3>`_ is equipped with its own
`registry for Docker images <https://git.scicore.unibas.ch/schwede/ProMod3/container_registry>`_. There you
can explore the current tag and simply pull a ready made built:

.. code-block:: bash

  docker pull registry.scicore.unibas.ch/schwede/promod3:<TAG>

Build Docker Image
------------------

In order to build the image:

.. code-block:: bash

  docker build --tag <IMAGE_NAME> -f Dockerfile <PATH_TO_DOCKERFILE_DIR>

You can chose any image name (tag) eg. promod. 


Run scripts and actions with pm executable
------------------------------------------

If script or action requires some external files eg. PDBs, they have to be located in the
path accessible via mounted volume and should be accessed via docker (NOT LOCAL)
path. Eg. assuming that we have a struc.pdb file in /home/<USER>/pdbs directory and
a script.py in /home/<USER> we could mount the /home/<USER> to /home in docker as
above by specifying -v /home/<USER>:/home. To run the script we thus need to
provide the (relative) path to the script and (relative) path to the file eg:

.. code-block:: bash

  docker run --rm -v /home/<USER>:/home <IMAGE_NAME> script.py \
  pdbs/struct.pdb

or with absolute paths:

.. code-block:: bash

  docker run --rm -v /home/<USER>:/home <IMAGE_NAME> /home/script.py \
  /home/pdbs/struct.pdb

An alternative is to mount the current working directory into the docker home:

.. code-block:: bash

  docker run --rm -v $(pwd):/home <IMAGE_NAME> script.py pdbs/struct.pdb


.. _docker_compound_lib:

The Compound Library
--------------------

At build time of the container, a :class:`~ost.conop.CompoundLib` is generated.
Compound libraries contain information on chemical compounds, such as their 
connectivity, chemical class and one-letter-code. The compound library has 
several uses, but the most important one is to provide the connectivy 
information for the rule-based processor.

The compound library is generated with the components.cif dictionary provided by 
the PDB. As the PDB updates regularly, the compound library shipped with the 
container is quickly outdated. For most use cases, this is not problematic. 
However, if you rely on correct connectivity information of the latest and 
greatest compounds, you have to keep the compound library up to date manually.

The suggested way of doing this is to generate your own compound library and
mount it into the container where the original compound lib resides to 
override it.

The simplest way to create a compound library is to use the 
:program:`chemdict_tool` available in the container. The program allows you 
to import the chemical description of the compounds from a mmCIF dictionary, 
e.g. the components.cif dictionary provided by the PDB. 
The latest dictionary can be downloaded from the 
`wwPDB site <http://www.wwpdb.org/ccd.html>`_. 
The files are rather large, it is therefore recommended to download the 
gzipped version. 

After downloading the file use :program:`chemdict_tool` in the container to 
convert the mmCIF  dictionary into our internal format: 

.. code-block:: bash

  docker run --rm -v $(pwd):/home --entrypoint chemdict_tool <IMAGE_NAME> \
  create components.cif.gz compounds.chemlib

To run a script with the upated compound library, use the -v option for mounting/overriding:

.. code-block:: bash

  docker run --rm -v /home/<USER>:/home \
  -v <COMPLIB_DIR_LOCALHOST>/compounds.chemlib:<COMPLIB_DIR_CONTAINER>/compounds.chemlib \
  <IMAGE_NAME> script.py pdbs/struct.pdb

with COMPLIB_DIR_LOCALHOST being the directory that contains the newly generated 
compound library with name compounds.chemlib and COMPLIB_DIR_CONTAINER the 
according path in the container.
If you didnt change anything in the Dockerfile, the latter should be
/usr/local/share/openstructure
  
You can check whether the default lib is successfully overriden by looking at the
output when running a Python script with following code in the container:

.. code-block:: python

  import promod3 # required to setup default lib
  from ost import conop
  lib = conop.GetDefaultLib()
  print(lib.GetCreationDate())

