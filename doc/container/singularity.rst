..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Singularity
===========

We do not provide a "standalone" Singularity image, but rather bootstrap from a 
Docker image.

Change to the directory containing the Singularity file
(<PROMOD3_CHECKOUT>/container) and build the image with:

.. code-block:: bash

  sudo singularity build promod.img Singularity

Available apps
--------------

This container includes the following apps:
 * **OST** - OpenStructure executable
 * **PM** - |project| executable
 * **IPython** - OST/|project|-powered iPython shell
 * **Notebook** - A Jupyter notebook playground with OST, |project| and nglview
 * **lDDT** - The Local Distance Difference Test
 * **Molck** - Molecular checker
 * **ChemdictTool** - Creating or update a compound library

To see the help for each individual app run:

.. code-block:: bash

    singularity run-help --app <APP NAME> <PATH TO PROMOD IMAGE>

Eg.:

.. code-block:: bash

    singularity run-help --app PM promod.img

To list all available |project| modelling actions:

.. code-block:: bash

    singularity run --app PM promod.img help

So you can either run an action with:

.. code-block:: bash

    singularity run --app PM promod.img build-model [options]

or an arbitrary Python script using OpenStructure / |project| with:

.. code-block:: bash

    singularity run --app PM promod.img my_script.py [options] 

The Notebook app provides a jupyter notebook playground with OST, |project|, and
nglview available. You can fire it up with:

.. code-block:: bash

    singularity run --app Notebook promod.img

connect to the notebook, start a new OST kernel and test it with the following 
code snippet:

.. code-block:: python

    from ost import io
    from promod3 import loop
    import nglview

    # generate backbone with dihedrals of a helix and store it
    sequence = "HELLYEAH"
    bb_list = loop.BackboneList(sequence)
    io.SavePDB(bb_list.ToEntity(), "test.pdb")

    # display stored file
    view = nglview.show_file("test.pdb")
    view

The Compound Library
--------------------

You'll have the exact same problem with outdated compound libraries as in the 
raw Docker image. You can find more information on that matter in the Docker 
section of the documentation: :ref:`docker_compound_lib`. 

The same trick of mounting an up to date compound library from the local host into
the container applies. The two relevant commands for Singularity are building 
a new library and mount it.

Build a new library:

.. code-block:: bash

  singularity run --app ChemdictTool <IMAGE> create components.cif.gz \
  compounds.chemlib

Run some script with an updated compound library from localhost:

.. code-block:: bash

  singularity run \
  -B <COMPLIB_DIR_LOCALHOST>/compounds.chemlib:<COMPLIB_DIR_CONTAINER>/compounds.chemlib \
  --app PM <IMAGE> my_script.py

Same as for the Docker, if you didn't meddle with the original Dockerfile, 
<COMPLIB_DIR_CONTAINER> should be /usr/local/share/openstructure. 
<COMPLIB_DIR_LOCALHOST> is the directory that contains the compound lib with the 
name compounds.chemlib that you created before. Make sure that everything works
as expected by executing the exact same lines of Python code as described 
in the Docker documentation: :ref:`docker_compound_lib`. 
