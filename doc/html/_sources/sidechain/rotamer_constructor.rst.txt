..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Rotamer Constructor
================================================================================

.. currentmodule:: promod3.sidechain

Instead of creating rotamers or frame residues by yourself, you can use the 
convenient functionality provided by |project|.


The RotamerConstructor Baseclass
--------------------------------------------------------------------------------


.. class:: RotamerConstructor

  Abstract base class that cannot be initialized from Python. It builds 
  an interface implemented by scoring function specific constructors 
  (e.g. :class:`SCWRL4RotamerConstructor`). 

  .. method:: ConstructRRMRotamerGroup(res, id, residue_index, rot_lib,\
                                       [phi = -1.0472, psi = -0.7854, \
                                        n_ter = False, c_ter = False, \
                                        probability_cutoff = 0.98])
  .. method:: ConstructRRMRotamerGroup(all_atom_pos, aa_res_idx, id,\
                                       residue_index, rot_lib,\
                                       [phi = -1.0472, psi = -0.7854, \
                                        n_ter = False, c_ter = False, \
                                        probability_cutoff = 0.98])
  .. method:: ConstructRRMRotamerGroup(res, id, residue_index, rot_lib_entries,\
                                       [phi = -1.0472, psi = -0.7854, \
                                        n_ter = False, c_ter = False, \
                                        probability_cutoff = 0.98])
  .. method:: ConstructRRMRotamerGroup(all_atom_pos, aa_res_idx, id,\
                                       residue_index, rot_lib_entries,\
                                       [phi = -1.0472, psi = -0.7854, \
                                        n_ter = False, c_ter = False, \
                                        probability_cutoff = 0.98])

    All functions are also avaible for their flexible rotamer model counterpart.
    =>ConstructFRMRotamerGroup(...) with the same parameters. 

    :param res:         To extract the required backbone atoms
    :param all_atom_pos: To extract the required backbone atoms
    :param aa_res_idx:  Index of residue in **all_atom_pos** from which to
                        extract the required backbone atoms
    :param id:          Identifies the sidechain.
    :param residue_index: Important for the energy calculations towards the 
                          :class:`Frame` you don't want to calculate a pairwise
                          energy of the sidechain particles towards particles
                          representing the own backbone...
    :param rot_lib:     To search for rotamers
    :param rot_lib_entries: :class:`RotamerLibEntry` objects to circumvent the 
                            direct use of a rotamer library
    :param phi:         Phi dihedral angle 
    :param psi:         Psi dihedral angle 
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal
    :param probability_cutoff: For some rotamers, there might be many low 
                        probability entries in the library. 
                        The function adds single rotamers to the group until 
                        the cumulative probability of the added rotamers is 
                        larger or equal **probability_cutoff**.

    :returns:           The rotamer group containing all constructed rotamers 
                        with internal energies assigned. 

    :type res:          :class:`ost.mol.ResidueHandle`
    :type all_atom_pos: :class:`promod3.loop.AllAtomPositions`
    :type aa_res_idx:   :class:`int`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type rot_lib:      :class:`RotamerLib` / :class:`BBDepRotamerLib`
    :type rot_lib_entries: :class:`list`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`
    :type probability_cutoff: :class:`float`
    :rtype:             :class:`RRMRotamerGroup`

    :raises:  :exc:`~exceptions.RuntimeError` when not all required backbone
              atoms are present in **residue** or not all required atom
              positions are set in **all_atom_pos**


  .. method:: ConstructBackboneFrameResidue(res, id, residue_index, \
                                            [phi = -1.0472, psi = -0.7854, \
                                             n_ter = False, c_ter = False])

  .. method:: ConstructBackboneFrameResidue(all_atom_pos, aa_res_idx, id,\
                                            residue_index, \
                                            [phi = -1.0472, psi = -0.7854\
                                             n_ter = False, c_ter = False])

    Constructs frame residues only containing backbone atoms (the ones that 
    don't show up in a rotamer).

    :param res:         Residue from which to extract the backbone positions
    :param all_atom_pos: To extract the backbone positions
    :param aa_res_idx:  Index of residue in **all_atom_pos** from which to 
                        extract the backbone positions
    :param id:          Identifies the sidechain
    :param residue_index: Important for the energy calculations towards the 
                          :class:`Frame` you don't want to calculate a pairwise
                          energy of the sidechain particles towards particles
                          representing the own backbone...
    :param phi:         The dihedral angle of the current residue
    :param psi:         The dihedral angle of the current residue
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal
    :type res:          :class:`ost.mol.ResidueHandle`
    :type all_atom_pos: :class:`promod3.loop.AllAtomPositions`
    :type aa_res_idx:   :class:`int`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`

    :rtype:             :class:`FrameResidue`


    :raises:  :exc:`~exceptions.RuntimeError` when not all required backbone
              atoms are present in **residue** or not all required atom
              positions are set in **all_atom_pos**.


  .. method:: ConstructSidechainFrameResidue(res, id, residue_index, \
                                             [phi = -1.0472, psi = -0.7854, \
                                              n_ter = False, c_ter = False])

  .. method:: ConstructSidechainFrameResidue(all_atom_pos, aa_res_idx, id,\
                                             residue_index, \
                                             [phi = -1.0472, psi = -0.7854\
                                             n_ter = False, c_ter = False])

    Constructs frame residues only containing sidechain atoms (the ones that
    you observe in a rotamer).

    :param res:         Residue from which to extract the backbone positions
    :param all_atom_pos: To extract the backbone positions
    :param aa_res_idx:  Index of residue in **all_atom_pos** from which to 
                        extract the backbone positions
    :param id:          Identifies the sidechain
    :param residue_index: Important for the energy calculations towards the 
                          :class:`Frame` you don't want to calculate a pairwise
                          energy of the sidechain particles towards particles
                          representing the own backbone...
    :param phi:         The dihedral angle of the current residue
    :param psi:         The dihedral angle of the current residue
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal
    :type res:          :class:`ost.mol.ResidueHandle`
    :type all_atom_pos: :class:`promod3.loop.AllAtomPositions`
    :type aa_res_idx:   :class:`int`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`

    :rtype:             :class:`FrameResidue`


    :raises:  :exc:`~exceptions.RuntimeError` when not all required sidechain
              atoms are present in **residue** or not all required sidechain 
              atom positions are set in **all_atom_pos**.

  .. method:: AssignInternalEnergies(rot_group, id, residue_index, \
                                     [phi = -1.0472, psi = -0.7854, \
                                      n_ter = False, c_ter = False])

    Assigns an internal energy to every rotamer in *rot_group*, i.e. an energy
    value before looking at any structural component of the energy function.
    The default implementation simply assigns 0.0 to every rotamer, it's up
    to the energy function specific constructors to override that behaviour.

    :param rot_group:   containing all rotamers for which internal energies have
                        to be assigned
    :param id:          Identifies the sidechain
    :param residue_index: The index of the residue which is represented by 
                          *rot_group*    
    :param phi:         The dihedral angle of the current residue
    :param psi:         The dihedral angle of the current residue
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal

    :type rot_group:    :class:`RRMRotamerGroup` / :class:`FRMRotamerGroup`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`



Scoring Function Specific RotamerConstructors 
--------------------------------------------------------------------------------


.. class:: SCWRL4RotamerConstructor(cb_in_sidechain)

  This object implements the full interface defined in 
  :class:`RotamerConstructor` and constructs rotamers and frame residues that 
  are parametrized according to the SCWRL4 method. They contain all heavy atoms, 
  but also the polar hydrogens. 

  :param cb_in_sidechain: If set to true, all constructed rotamers will contain 
                          the cb atom. This flag also affects the construction 
                          of frame residues and controls whether the cb atom 
                          shows up in the backbone frame residues or sidechain 
                          frame residues.
                          This is useful when you want to represent ALA or 
                          GLY with actual rotamers, but be aware of increased 
                          runtime. This flag can be set to False for most
                          modeling applications and you just don't generate
                          any rotamers for ALA and GLY.

  :type cb_in_sidechain: :class:`bool`

  .. method:: ConstructFrameResidue(residue, residue_index)
  
    Constructs a :class:`FrameResidue` from a :class:`ost.mol.ResidueHandle`.
    This can be useful to mark a region occupied by a ligand. Note, that
    there won't be any parametrization of hbonds in this function. All heavy 
    atoms of the residue will be represented as carbons and hydrogens are 
    skipped.

    :param residue:       Residue from which all atoms will be taken to
                        construct a :class:`FrameResidue`.
    :param residue_index: Index this :class:`FrameResidue` belongs to.

    :type residue:        :class:`ost.mol.ResidueHandle`
    :type residue_index:  :class:`int`

    :returns:             :class:`FrameResidue`

  .. method:: ConstructFrameResidueHeuristic(residue, residue_index, comp_lib)

    Constructs a :class:`FrameResidue` from a :class:`ost.mol.ResidueHandle` using
    a heuristic treatment of the atoms based on the passed compounds library.
    This is meant to be used as an alternative to :func:`ConstructFrameResidue`,
    which will be called by this function if the residue is not known by the given
    compounds library.
    Only non-hydrogen atoms are considered and by default added as uncharged
    carbons. Special treatment is used for atoms known by the compounds library
    in the following cases:
  
    - carbons, nitrogens, oxygens and sulfur particles use an appropriate type
      as in the :class:`SidechainParticle` enumeration
    - carbonyls are added as charged oxygen particles as hbond acceptors
  
    :param residue:       Residue from which all atoms will be taken to
                          construct a :class:`FrameResidue`.
    :param residue_index: Index this :class:`FrameResidue` belongs to.
    :param comp_lib:      OST compound library to use 
  
    :type residue:        :class:`ost.mol.ResidueHandle`
    :type residue_index:  :class:`int`
    :type comp_lib:       :class:`ost.conop.CompoundLib`
  
    :returns:             :class:`FrameResidue`

  .. method:: AssignInternalEnergies(rot_group, id, residue_index, \
                                     [phi = -1.0472, psi = -0.7854, \
                                      n_ter = False, c_ter = False])

    Overrides the method defined in :class:`RotamerConstructor`.
    Takes the rotamer group and assigns every single rotamer its internal
    energy based on the probabilistic approach used by SCWRL4.
    => -internal_e_prefac*log(p/max_p), where internal_e_prefac and p are
    rotamer specific and max_p is the maximum probablity of any of the rotamers
    in **rot_group**. If you construct a rotamer group by the
    ConstructRRMRotamerGroup/ConstructFRMRotamerGroup functions, this function 
    is already called at construction and the energies are properly assigned.

    :param rot_group:   containing all rotamers for which internal energies have
                        to be assigned
    :param id:          Identifies the sidechain
    :param residue_index: The index of the residue which is represented by 
                          *rot_group*    
    :param phi:         The dihedral angle of the current residue
    :param psi:         The dihedral angle of the current residue
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal

    :type rot_group:    :class:`RRMRotamerGroup` / :class:`FRMRotamerGroup`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`




.. class:: SCWRL3RotamerConstructor(cb_in_sidechain)

  This object implements the full interface defined in 
  :class:`RotamerConstructor` and constructs rotamers and frame residues that 
  are parametrized according to the SCWRL3 method. They contain only heavy atoms. 

  :param cb_in_sidechain: If set to true, all constructed rotamers will contain 
                          the cb atom. This flag also affects the construction 
                          of frame residues and controls whether the cb atom 
                          shows up in the backbone frame residues or sidechain 
                          frame residues.
                          This is useful when you want to represent ALA or 
                          GLY with actual rotamers, but be aware of increased 
                          runtime. This flag can be set to False for most
                          modeling applications and you just don't generate
                          any rotamers for ALA and GLY.

  :type cb_in_sidechain: :class:`bool`


  .. method:: AssignInternalEnergies(rot_group, id, residue_index, \
                                     [phi = -1.0472, psi = -0.7854, \
                                      n_ter = False, c_ter = False])

    Overrides the method defined in :class:`RotamerConstructor`.
    Takes the rotamer group and assigns every single rotamer its internal
    energy based on the probabilistic approach used by SCWRL3.
    => -internal_e_prefac*log(p/max_p), where internal_e_prefac and p are
    rotamer specific and max_p is the maximum probablity of any of the rotamers
    in **rot_group**. If you construct a rotamer group by the
    ConstructRRMRotamerGroup/ConstructFRMRotamerGroup functions, this function 
    is already called at construction and the energies are properly assigned.

    :param rot_group:   containing all rotamers for which internal energies have
                        to be assigned
    :param id:          Identifies the sidechain
    :param residue_index: The index of the residue which is represented by 
                          *rot_group*    
    :param phi:         The dihedral angle of the current residue
    :param psi:         The dihedral angle of the current residue
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal

    :type rot_group:    :class:`RRMRotamerGroup` / :class:`FRMRotamerGroup`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`


.. class:: VINARotamerConstructor(cb_in_sidechain)

  This object implements the full interface defined in 
  :class:`RotamerConstructor` and constructs rotamers and frame residues that 
  are parametrized according to the VINA method. They contain only heavy atoms. 

  :param cb_in_sidechain: If set to true, all constructed rotamers will contain 
                          the cb atom. This flag also affects the construction 
                          of frame residues and controls whether the cb atom 
                          shows up in the backbone frame residues or sidechain 
                          frame residues.
                          This is useful when you want to represent ALA or 
                          GLY with actual rotamers, but be aware of increased 
                          runtime. This flag can be set to False for most
                          modeling applications and you just don't generate
                          any rotamers for ALA and GLY.

  :type cb_in_sidechain: :class:`bool`


  .. method:: AssignInternalEnergies(rot_group, id, residue_index, \
                                     [phi = -1.0472, psi = -0.7854, \
                                      n_ter = False, c_ter = False])

    Overrides the method defined in :class:`RotamerConstructor`.
    Takes the rotamer group and assigns every single rotamer its internal
    energy based on the probabilistic approach used by SCWRL3/SCWRL4.
    => -internal_e_prefac*log(p/max_p), where internal_e_prefac and p are
    rotamer specific and max_p is the maximum probablity of any of the rotamers
    in **rot_group**. If you construct a rotamer group by the
    ConstructRRMRotamerGroup/ConstructFRMRotamerGroup functions, this function 
    is already called at construction and the energies are properly assigned.

    :param rot_group:   containing all rotamers for which internal energies have
                        to be assigned
    :param id:          Identifies the sidechain
    :param residue_index: The index of the residue which is represented by 
                          *rot_group*    
    :param phi:         The dihedral angle of the current residue
    :param psi:         The dihedral angle of the current residue
    :param n_ter:       Whether the residue is n-terminal
    :param c_ter:       Whether the residue is c-terminal

    :type rot_group:    :class:`RRMRotamerGroup` / :class:`FRMRotamerGroup`
    :type id:           :class:`RotamerID`
    :type residue_index: :class:`int`
    :type phi:          :class:`float`
    :type psi:          :class:`float`
    :type n_ter:        :class:`bool`
    :type c_ter:        :class:`bool`


  .. method:: ConstructFrameResidueHeuristic(res, res_idx)

    Constructs a :class:`FrameResidue` from a :class:`ost.mol.ResidueHandle` using
    a heuristic treatment of the atoms. It is important that the residue has 
    proper bonds assigned, as they influence the atom typing procedure.
    Furthermore, you need hydrogens to automatically estimate the correct
    atom type for oxygens and nitrogens (hydrogen bond donor/acceptor). 
    Alternatively you can assign generic properties to oxygens and nitrogens
    to circumvent the requirement of hydrogens. This is further described for
    the case of oxygen.

    * Carbon is assigned C_VINAParticle :class:`VINAParticleType` if its only 
      bound to other carbons or hydrogens (and deuterium). All other carbons are
      assigned C_P_VINAParticle :class:`VINAParticleType`.
    * In case of oxygen, the heuristic first checks for set generic properties.
      If the atom has the bool properties "is_hbond_acceptor" AND 
      "is_hbond_donor" set, it decides between the according oxygen types
      in :class:`VINAParticleType`. If the generic properties are not set,
      every oxygen is assumed to be an hbond acceptor. But only an hbond donor 
      if its bound to a hydrogen (or deuterium). You can set the generic 
      properties for an :class:`ost.mol.AtomHandle` by calling 
      at.SetBoolProp("is_hbond_donor", False) and 
      at.SetBoolProp("is_hbond_acceptor", True). An oxygen with those 
      generic properties is assigned O_A_VINAParticle :class:`VINAParticleType`.
    * In case of nitrogen, the heuristic again first checks for set generic
      properties.
      If the atom has the bool properties "is_hbond_acceptor" AND 
      "is_hbond_donor" set, it decides between the according nitrogen types
      in :class:`VINAParticleType`. If not, nitrogen is expected to be an
      hbond donor if it is bound to a hydrogen (or deuterium) and 
      an hbond acceptor if it is bound to less than 3 other atoms (sounds
      horrible but works surprisingly well).
    * Atoms of elements ["MG", "MN", "ZN", "CA", "FE"] are assigned 
      M_VINAParticle :class:`VINAParticleType`.
    * Atoms of elements ["S", "P", "F", "CL", "BR", "I"] are assigned their
      corresponding :class:`VINAParticleType`.
    * All other atoms are neglected and not added to the returned
      :class:`FrameResidue`.

    :param res:         Residue from which to create the 
                        :class:`FrameResidue`
    :param res_idx:     Index that is set in :class:`FrameResidue`
    :type res:          :class:`ost.mol.ResidueHandle`
    :type res_idx:      :class:`int`
    :rtype:             :class:`FrameResidue`


  .. method:: ConstructRRMRotamerHeuristic(res)

    Construct a :class:`RRMRotamer` with the atom typing heuristic
    as in the :meth:`ConstructFrameResidueHeuristic` method.

    :param res:         Residue from which to create the 
                        :class:`RRMRotamer`
    :type res:          :class:`ost.mol.ResidueHandle`
    :rtype:             :class:`RRMRotamer`


  .. method:: ConstructFRMRotamerHeuristic(res)

    Construct a :class:`FRMRotamer` with the atom typing heuristic
    as in the :meth:`ConstructFrameResidueHeuristic` method. The
    constructed :class:`FRMRotamer` only contains one subrotamer that
    contains the atoms from *residue*.

    :param res:         Residue from which to create the 
                        :class:`FRMRotamer`
    :type res:          :class:`ost.mol.ResidueHandle`
    :rtype:             :class:`FRMRotamer`
