..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Loading Rotamer Libraries
================================================================================

.. currentmodule:: promod3.sidechain

There are several rotamer libraries that can be used in |project| . |project|
is optimized for the use with backbone dependent rotamer libraries such
as the 2010 library provided by the Dunbrack lab [shapovalov2011]_. 
You can request a licence `here <http://dunbrack.fccc.edu/bbdep2010/>`_
and generate such a library as described in 
extras/data_generation/rotamer_library/README. Alternatively, |project|
provides its own backbone dependent or backbone independent libraries 
that can be loaded with :meth:`LoadBBDepLib` / :meth:`LoadLib`.

.. method:: LoadBBDepLib()

  A backbone dependent rotamer library shipped with |project|. You can find
  details on how it is created in extras/data_generation/rotamer_library/README.
  All scripts to build it are in the same directory as the README file and
  build the basis for custom versions.

  :returns:             The requested Library
  :rtype:               :class:`BBDepRotamerLib`


.. method:: LoadLib()

  A backbone independent rotamer library shipped with |project|. You can find
  details on how it is created in extras/data_generation/rotamer_library/README.
  All scripts to build it are in the same directory as the README file and
  build the basis for custom versions.

  :returns:             The requested library
  :rtype:               :class:`RotamerLib`


.. method:: ReadDunbrackFile(filename)

  Reads a file as it is provided when you get a licence for the 2010 library of
  the Dunbrack lab. It can only read the classic version, where all rotamers
  are in a single file. Specific distributions of non-rotameric sidechains 
  cannot be read. You can find an example described in 
  extras/data_generation/rotamer_library/README

  :param filename:      Name of the file
  :type param:          :class:`str`

  :throws:  :exc:`~exceptions.RuntimeError` if file does not exist, the format
            is not valid or when the library is incomplete leading to problems
            to make the library static. Most likely your input file is
            incomplete if the last problem gets triggered.

  :returns: The read library
  :rtype:   :class:`BBDepRotamerLib`
