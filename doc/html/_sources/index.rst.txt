..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


ProMod3
=======

|project| is a modelling engine based on the `OpenStructure <https://openstructure.org>`_ [biasini2013]_ 
computational structural biology framework that can perform all steps required 
to generate a protein model by homology. Its modular design aims at 
implementing flexible modelling pipelines and fast prototyping of novel 
algorithms.

The source code can be found here: https://git.scicore.unibas.ch/schwede/ProMod3


Documentation
=============

.. toctree::
  :maxdepth: 2

  Users <users>

.. toctree::
  :maxdepth: 1  

  Developers <developers>
  License <license>
  References <references>
  Changelog <changelog>
