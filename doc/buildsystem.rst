..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


.. _building-promod:

Building |project|
================================================================================

--------------------------------------------------------------------------------
Dependencies
--------------------------------------------------------------------------------

|project| is build on top of |ost_l|_ (|ost_s|), requiring at least version 
|ost_version|. |ost_s| must be configured and compiled with ``ENABLE_MM=1`` to
use |openmm|_. To create the build system, |cmake|_ is required. The same
versions of |python|_ and |boost|_ are needed as used in |ost_s|. For |eigen3|_
we need at least version 3.3.0. To build the documentation, |sphinx|_ is
required. We do not provide backwards compatibility to Python 2.7. The last
release supporting Python 2.7 is 2.1.0 which also requires |ost_s| to be 
compiled with Python 2.7.

The currently preferred versions are:

* |ost_s|_ |ost_version|
* |openmm|_ 7.1.1
* |cmake|_ 3.12.1
* |python|_ 3.6.0
* |boost|_ 1.68.0
* |eigen3|_ 3.3.1
* |sphinx|_ 1.4.9

--------------------------------------------------------------------------------
 Using |cmake|
--------------------------------------------------------------------------------
|cmake| is used to configure the build system and in the end produces makefiles
and certain directories needed for building |project|. Basically it is called
right from a shell with the directory containing the top-level
:file:`CMakeLists.txt` as an argument. The preferred approach is to generate a
build folder and configure and compile in there:

.. code-block:: console

   # execute this in the ProMod3 root folder
   $ mkdir build
   $ cd build
   $ cmake .. -DOST_ROOT=<PATH TO OST> 

For us, at least pointer to the |ost_s| installation directory is needed, 
handed over to |cmake| by ``-D`` into the variable ``OST_ROOT`` (e.g. |ost_s|
headers would be located in ``OST_ROOT/include/ost``).

Similarly, one can specify folders for |boost|, |python| and |eigen3| 
if multiple versions exist and/or they are not installed in a default location.
These are set with the ``BOOST_ROOT`` (make sure that's the same as for |ost_s|), 
``Python_ROOT_DIR`` and ``EIGEN3_INCLUDE_DIR``.

Here is a list of more options used within |project|:

* ``DISABLE_DOCUMENTATION`` Don't build documentation, don't search for |sphinx|
* ``DISABLE_DOCTEST`` Don't run example code from documentation on
  ``make check`` (implicit by ``DISABLE_DOCUMENTATION``)
* ``DISABLE_LINKCHECK`` Don't test links from documentation on ``make check``
  (implicit by ``DISABLE_DOCUMENTATION``)
* ``ENABLE_SSE`` Allow for more agressive optimization by adding -msse4 flag to
  the compiler. At some places within the code, we use SSE intrinsics explicitely.
  This is only enabled if |ost_s| has been compiled with single precision.
  (``OST_DOUBLE_PRECISION`` flag)

Since we use |ost_s| in the background, some of its options for |cmake| are
also relevant, here. Basically they need to be set to exactly the same value.
Even if |project| builds with different settings, it may start producing funny
results at an unexpected point. If you do not know the values, grep the option
in the :file:`CMakeCache.txt` of |ost_s|:

* ``OPTIMIZE``
* ``OST_DOUBLE_PRECISION``

.. doesn't work (yet?)
  Instead of providing all those options at the command line, you can also use
  ``ccmake`` instead of ``cmake``, which allows you to set the |cmake| variables
  interactively. Press 'c' to try to configure the build. If it fails, enter the
  requested paths and configure again. Advanced settings can be edited by 
  pressing 't'.

Instead of calling |cmake| by yourself, there is the :file:`conf-scripts`
directory, providing smallish scripts to invoke |cmake| the right way for
various systems. Usually those scripts just need the |ost_s| path
and the location of the top-level :file:`CMakeLists.txt`.

As mentioned earlier, we highly recommend to use out-of-source builds. 
This way, you can have several builds with different configurations. Also if 
anything goes wrong, just remove the build directory to get to a clean state
again. No searching for |cmake| cache files or checking if certain files
really got rebuild and similar things required.

--------------------------------------------------------------------------------
Running Make
--------------------------------------------------------------------------------
After configuring, you want to build |project| by

.. code-block:: console

   $ make

to populate the :file:`stage` directory with a ready-to-go version of the
latest code.

.. index::
   single: Make targets

Beside the usual ``make all`` and other default targets, there are a few
special targets:

* ``check`` :index:`make check` Runs unit tests and if |cmake| was invoked in
  its standard configuration also documentation examples (``doctest``) and
  links in the doc. (``linkcheck``) are checked
* ``html`` :index:`make html` Creates documentation as web page using the
  |sphinx| ``html`` builder
* ``man`` :index:`make man` Creates a manual page using the |sphinx| ``man``
  builder
* ``doc`` :index:`make doc` Creates documentation using the ``html`` and
  ``man`` targets
* ``help`` :index:`make help` Prints a list of targets available

--------------------------------------------------------------------------------
Installing |project|
--------------------------------------------------------------------------------
If you wish to install |project| (note that you can also safely keep it all in
the :file:`stage` directory), you can use

.. code-block:: console

   $ make install

By default, this will copy the :file:`stage` directory to :file:`/usr/local`. To
override this, you can add the ``-DCMAKE_INSTALL_PREFIX=<INSTALL PATH>`` flag to
your ``cmake`` call.

After the installation, you should be able to delete the whole source folder. To
ensure that everything works, you can use a set of automated "sanity checks".
Please follow the instructions in :file:`extras/sanity_checks/README` to setup
and run those tests after moving the source folder. If everything works, you can
safely delete the whole source folder.


.. |qmean| replace:: QMEAN
.. |eigen3| replace:: Eigen 3
.. |openmm| replace:: OpenMM
.. _qmean: https://swissmodel.expasy.org/qmean/
.. _ost_l: https://www.OpenStructure.org
.. _cmake: https://cmake.org/
.. _python: https://www.python.org/
.. _boost: https://www.boost.org/
.. _eigen3: http://eigen.tuxfamily.org/index.php?title=Main_Page
.. _openmm: https://github.com/openmm/openmm

..  LocalWords:  cmake makefiles CMakeLists txt DOCTEST LINKCHECK conf html
..  LocalWords:  doctest linkcheck qmean DQMEAN eigen CMakeCache
..  LocalWords:  OPTIMIZE
