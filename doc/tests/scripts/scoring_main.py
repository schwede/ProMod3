from ost import io, seq
from promod3 import loop, scoring

# load data
ent = io.LoadPDB("data/1CRN.pdb")
ent_seq = seq.SequenceFromChain('A', ent.FindChain('A'))

# setup score environment linked to entity
score_env = scoring.BackboneScoreEnv(ent_seq)
score_env.SetInitialEnvironment(ent)

# setup scorers attached to that env.
clash_scorer = scoring.ClashScorer()
clash_scorer.AttachEnvironment(score_env)
cbeta_scorer = scoring.LoadCBetaScorer()
cbeta_scorer.AttachEnvironment(score_env)

# calculate scores for 10 residues starting at residue number 23.
# all required structural information comes from the environment
# that can evolve as the modelling proceeds.
print("Clash-Score", clash_scorer.CalculateScore(23, 10))
print("CBeta-Score", cbeta_scorer.CalculateScore(23, 10))
