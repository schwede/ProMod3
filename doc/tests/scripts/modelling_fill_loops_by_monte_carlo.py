from ost import io, seq
from promod3 import modelling, loop

# setup
tpl = io.LoadPDB('data/1crn_cut.pdb')
seq_trg = 'TTCCPSIVARSNFNVCRLPGTPEAICATYTGCIIIPGATCPGDYAN'
seq_tpl = 'TTCCPSIVARSNFNVCRLPGTPEA------GCIIIPGATCPGDYAN'
aln = seq.CreateAlignment(seq.CreateSequence('trg', seq_trg),
                          seq.CreateSequence('tpl', seq_tpl))
aln.AttachView(1, tpl.CreateFullView())
mhandle = modelling.BuildRawModel(aln)
# close gaps
print('Number of gaps before: %d' % len(mhandle.gaps))
modelling.FillLoopsByMonteCarlo(mhandle,
                              	loop.LoadTorsionSamplerCoil())
print('Number of gaps after: %d' % len(mhandle.gaps))
