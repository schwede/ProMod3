from promod3 import loop
from ost import conop
# this requires matplotlib and numpy
import matplotlib
# change next line, if you wish to use a GUI-based plot-output
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np

# load a default sampler
t_sampler = loop.LoadTorsionSampler()

# dihedral angles will be stored in here
phi = list()
psi = list()

# draw from a random distribution
for i in range(1000):
    dihedral_pair = t_sampler.Draw(conop.ALA, 
                                   conop.PRO, 
                                   conop.ALA)
    phi.append(dihedral_pair[0])
    psi.append(dihedral_pair[1])

# and plot it
plt.xlim(-np.pi, np.pi)
plt.ylim(-np.pi, np.pi)
plt.plot(phi, psi, '.')
plt.xlabel("phi", fontsize='large')
plt.ylabel("psi", fontsize='large')
plt.title("ALA-PRO-ALA")

# store plot as png file
plt.savefig("torsion_plot.png")
