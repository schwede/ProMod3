from ost import io, seq
from promod3 import modelling

# setup
tpl = io.LoadPDB('data/1crn_cut.pdb')
seq_trg = 'TTCCPSIVARSNFNVCRLPGTPEAICATGYTCIIIPGATCPGDYAN'
seq_tpl = 'TTCCPSIVARSNFNVCRLPGTPEA----G--CIIIPGATCPGDYAN'
aln = seq.CreateAlignment(seq.CreateSequence('trg', seq_trg),
                          seq.CreateSequence('tpl', seq_tpl))
aln.AttachView(1, tpl.CreateFullView())
mhandle = modelling.BuildRawModel(aln)
# merge gaps
print('Number of gaps before: %d' % len(mhandle.gaps))
modelling.MergeGapsByDistance(mhandle, 0)
print('Number of gaps after: %d' % len(mhandle.gaps))
