from ost import io
from promod3 import loop, scoring, modelling
import numpy as np

# setup protein
prot = io.LoadPDB('data/1CRN.pdb')

chain_index = 0
start_resnum = 1
terminal_len = 8

seqres = ''.join([r.one_letter_code for r in prot.residues])
terminal_seqres = seqres[:terminal_len]

# setup mc_sampler
torsion_sampler = loop.LoadTorsionSampler()
mc_sampler = modelling.SoftSampler(terminal_seqres, torsion_sampler,
                                   10.0 / 180 * np.pi)

# setup mc_closer
mc_closer = modelling.NTerminalCloser(prot.residues[terminal_len-1])

# setup backbone scorer with clash and cbeta scoring
score_env = scoring.BackboneScoreEnv(seqres)
score_env.SetInitialEnvironment(prot)
scorer = scoring.BackboneOverallScorer()
scorer["cbeta"] = scoring.LoadCBetaScorer()
scorer["clash"] = scoring.ClashScorer()
scorer.AttachEnvironment(score_env)

# set up mc_scorer
weights = dict()
weights["cbeta"] = 10.0
weights["clash"] = 0.1
mc_scorer = modelling.LinearScorer(scorer, score_env,
                                   start_resnum, terminal_len,
                                   chain_index, weights)

# setup mc_cooler
mc_cooler = modelling.ExponentialCooler(100, 100, 0.9)

# create BackboneList from n-terminus
bb_list = loop.BackboneList(terminal_seqres,
                            prot.residues[:terminal_len])

# shake it!
modelling.SampleMonteCarlo(mc_sampler, mc_closer, mc_scorer,
                           mc_cooler, 10000, bb_list, False, 0)

# save down the result
io.SavePDB(bb_list.ToEntity(), "sampled_frag.pdb")
