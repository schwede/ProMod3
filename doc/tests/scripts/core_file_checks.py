"""Test for file reading."""
from promod3.core import helper
from promod3.core import pm3argparse

p = pm3argparse.PM3ArgumentParser(__doc__)
p.add_argument('file', type=str)
opts = p.Parse()

helper.FileExists('Test file', 1, opts.file)

opts.name, opts.ext, opts.gz = helper.FileExtension('Test file', 2,
                                                    opts.file,
                                                    ('pdb', 'mmcif'),
                                                    gzip=True)
