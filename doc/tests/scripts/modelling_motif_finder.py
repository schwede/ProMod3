# Example script that loads protein structures that contain ATP and
# generates motif queries describing their binding pockets.
# In a second step, those pockets are matched against a protein 
# structure that only contains an ATP analog. The ATP from every 
# match is transformed and stored to disk for further processing.

from ost import io, geom, mol
from promod3 import modelling

files = ['data/1E2Q.pdb', 'data/1KO5.pdb', 'data/2IYW.pdb']

atp_list = list()
query_list = list()

for f in files:

    prot = io.LoadPDB(f)
    peptide_sel = prot.Select("peptide=true")
    atp_sel = prot.Select("rname=ATP")

    # generate a single query for each ATP pocket
    for atp_idx, atp_r in enumerate(atp_sel.residues):
        pocket_view = peptide_sel.CreateEmptyView()
        for atp_at in atp_r.atoms:
            close_at = peptide_sel.FindWithin(atp_at.GetPos(), 4.5)
            for at in close_at:
                r = at.handle.GetResidue()
                add_flag = mol.INCLUDE_ATOMS | mol.CHECK_DUPLICATES
                pocket_view.AddResidue(r, add_flag)

        ca_positions = geom.Vec3List()
        for res in pocket_view.residues:
            ca_positions.append(res.FindAtom("CA").GetPos())
        i = "%s_%i"%(f, atp_idx)
        query = modelling.MotifQuery(ca_positions, i, 4.0, 9.0, 1.0)
        query_list.append(query)
     
        # create an entity from atp for later use
        atp_view = prot.CreateEmptyView()
        atp_view.AddResidue(atp_r, mol.INCLUDE_ATOMS)
        atp_list.append(mol.CreateEntityFromView(atp_view, True))

# That's it, let's combine the single queries
full_query = modelling.MotifQuery(query_list)

prot = io.LoadPDB("data/1AKE.pdb")
peptide_sel = prot.Select("peptide=true")
ca_positions = geom.Vec3List()
for r in peptide_sel.residues:
    ca_positions.append(r.FindAtom("CA").GetPos())

# search all matches, fetch the corresponding atps,
# transform them onto the 1ake binding site and dump them to disk
matches = modelling.FindMotifs(full_query, ca_positions)
for m_idx, m in enumerate(matches):
    atp = atp_list[m.query_idx].Copy()
    atp.EditXCS().ApplyTransform(m.mat)
    io.SavePDB(atp, "m_%i.pdb"%(m_idx))

