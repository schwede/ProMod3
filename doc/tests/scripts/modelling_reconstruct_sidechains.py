from ost import io, mol
from promod3 import modelling

# load a protein 
prot = io.LoadPDB('data/1CRN.pdb')
# get only amino acids
prot = mol.CreateEntityFromView(prot.Select("peptide=true"), True)
io.SavePDB(prot, 'sidechain_test_orig.pdb')
# reconstruct sidechains
modelling.ReconstructSidechains(prot, keep_sidechains=False)
io.SavePDB(prot, 'sidechain_test_rec.pdb')
