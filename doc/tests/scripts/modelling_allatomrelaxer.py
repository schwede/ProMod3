from ost import io
from promod3 import modelling, loop

# load example (has res. numbering starting at 1)
prot = io.LoadPDB('data/1CRN.pdb')
res_list = prot.residues
seqres_str = ''.join([r.one_letter_code for r in res_list])

# initialize AllAtom environment and sidechain reconstructor
env = loop.AllAtomEnv(seqres_str)
env.SetInitialEnvironment(prot)
sc_rec = modelling.SidechainReconstructor()
sc_rec.AttachEnvironment(env)

# "reconstruct" subset (res. num. 6..10) -> sidechains kept here
sc_result = sc_rec.Reconstruct(6, 5)
# setup sys creator
ff_lookup = loop.ForcefieldLookup.GetDefault()
mm_sys = loop.MmSystemCreator(ff_lookup)
relaxer = modelling.AllAtomRelaxer(sc_result, mm_sys)
# relax loop
pot_e = relaxer.Run(sc_result, 300, 0.1)
print("Potential energy after: %g" % pot_e)
# update environment with solution
env.SetEnvironment(sc_result.env_pos)
# store all positions of environment
io.SavePDB(env.GetAllAtomPositions().ToEntity(), 'aa_relax_test.pdb')
