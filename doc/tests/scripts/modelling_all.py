from ost import io
from promod3 import modelling

# get raw model
tpl = io.LoadPDB('data/1crn_cut.pdb')
aln = io.LoadAlignment('data/1crn.fasta')
aln.AttachView(1, tpl.CreateFullView())
mhandle = modelling.BuildRawModel(aln)

# build final model
final_model = modelling.BuildFromRawModel(mhandle)
io.SavePDB(final_model, 'model.pdb')
