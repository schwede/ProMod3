from ost import io, seq
from promod3 import loop

# load an example structure
prot = io.LoadPDB('data/1CRN.pdb')

# extract some additional information
seqres = ''.join([r.one_letter_code for r in prot.residues])
frag_pos = 35
frag_length = 9
frag_seq = seqres[frag_pos:frag_pos+frag_length]
frag_residues = prot.residues[frag_pos:frag_pos+frag_length]
ref_backbone = loop.BackboneList(frag_seq, frag_residues)
n_stem = prot.residues[frag_pos]
c_stem = prot.residues[frag_pos+frag_length-1]

# extract potential loops from fragment database based on geometry
frag_db = loop.LoadFragDB()
fragments = frag_db.SearchDB(n_stem, c_stem, frag_length)
print("Num. fragments found in FragDB: %d" % len(fragments))
# compare with reference
struct_db = loop.LoadStructureDB()
for i in range(len(fragments)):
    # get structure from structural database
    bb_list = struct_db.GetBackboneList(n_stem, c_stem,
                                        fragments[i], frag_seq)
    ca_rmsd = bb_list.CARMSD(ref_backbone, True)
    print("-> fragment %d has CA RMSD of %.3f" % (i, ca_rmsd))

# extract potential loops from fragment database based on sequence
fragger = loop.Fragger(frag_seq)
# for simplicity we just use a sequence similarity score
fragger.AddSeqSimParameters(1.0, seq.alg.BLOSUM62)
fragger.Fill(struct_db, 1.0, 5)
print("Num. fragments found in Fragger: %d" % len(fragger))
# compare fraggers with reference
for i in range(len(fragger)):
    ca_rmsd = fragger[i].CARMSD(ref_backbone, True)
    print("-> fragment %d has CA RMSD of %.3f" % (i, ca_rmsd))
