from ost import io, geom
from promod3 import loop

# load example (has res. numbering starting at 1)
ent = io.LoadPDB("data/1CRN.pdb")
res_list = ent.residues
seqres_str = ''.join([r.one_letter_code for r in res_list])

# initialize an environment
env = loop.AllAtomEnv(seqres_str)
env.SetInitialEnvironment(ent)

# get all atom position for part of it
all_atoms = loop.AllAtomPositions(res_list[35:44])
# change pos. at res. index 1 (= res. number 37)
idx_ca_res_37 = all_atoms.GetIndex(1, "CA")
new_pos = all_atoms.GetPos(idx_ca_res_37) + geom.Vec3(0.2, 0.2, 0.2)
all_atoms.SetPos(idx_ca_res_37, new_pos)
# insert into ent and store updated entity
all_atoms.InsertInto(1, ent.chains[0], 37)
io.SavePDB(ent, "all_atom_pos.pdb")

# update environment with new positions
env.SetEnvironment(all_atoms, 36)
# store all positions of environment
io.SavePDB(env.GetAllAtomPositions().ToEntity(), "all_atom_env.pdb")
