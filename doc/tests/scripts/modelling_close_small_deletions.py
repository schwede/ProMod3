from ost import io, seq
from promod3 import modelling

# setup
tpl = io.LoadPDB('data/gly.pdb')
aln = seq.CreateAlignment(seq.CreateSequence('trg', 'GGGG-GGGG'),
                          seq.CreateSequence('tpl', 'GGGGAGGGG'))
aln.AttachView(1, tpl.CreateFullView())
mhandle = modelling.BuildRawModel(aln)
# close small deletion
print('Number of gaps before: %d' % len(mhandle.gaps))
modelling.CloseSmallDeletions(mhandle)
print('Number of gaps after: %d' % len(mhandle.gaps))
