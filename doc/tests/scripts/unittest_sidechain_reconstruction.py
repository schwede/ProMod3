import unittest
from promod3 import modelling
from ost import io,mol
import os

class ReconstructTests(unittest.TestCase):
    def testReconstruct(self):
        in_file = os.path.join('data', '1eye.pdb')
        ref_file = os.path.join('data', '1eye_rec.pdb')
        # get and reconstruct 1eye
        prot = io.LoadPDB(in_file)
        modelling.ReconstructSidechains(prot, keep_sidechains=False)
        # compare with reference solution
        prot_rec = io.LoadPDB(ref_file)
        self.assertEqual(prot.GetAtomCount(), prot_rec.GetAtomCount())

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
