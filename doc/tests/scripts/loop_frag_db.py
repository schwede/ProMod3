from ost import io
from promod3 import loop

# let's load the default structure_db 
structure_db = loop.StructureDB.LoadPortable("data/port_str_db.dat")
# in practice you might want to use the default StructureDB instead:
# structure_db = loop.LoadStructureDB()

# and our beloved crambin...
structure = io.LoadPDB('data/1CRN.pdb')

# we now want to connect the residue with index 17 and 21
sub_res_list = structure.residues[17:22]
n_stem = sub_res_list[0]
c_stem = sub_res_list[-1]
frag_seq = ''.join([r.one_letter_code for r in sub_res_list])

# a custom FragDB can be built to identify fragments
# fulfilling these particular geometric constraints
frag_db = loop.FragDB(1.0, 20)

# at this point we add all possible fragments of length 5 
# with an RMSD threshold of 0.5
frag_db.AddFragments(5, 0.5, structure_db)

# the FragDB can now be used to extract FragmentInfo objects to
# finally query the StructureDB
fragment_infos = frag_db.SearchDB(n_stem, c_stem, 5)

# get the fragments in form of BackboneList objects and store them
for i,f_i in enumerate(fragment_infos):
    bb_list = structure_db.GetBackboneList(n_stem, c_stem,
                                           f_i, frag_seq)
    io.SavePDB(bb_list.ToEntity(), str(i) + ".pdb")
