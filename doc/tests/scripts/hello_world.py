from ost import io
from promod3 import loop

# generate backbone with dihedrals of a helix and store it
sequence = "HELLYEAH"
bb_list = loop.BackboneList(sequence)
io.SavePDB(bb_list.ToEntity(), "test.pdb")
