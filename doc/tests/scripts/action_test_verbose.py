import sys

# this is needed so there will be no test_actions.pyc created in the source
# directory
sys.dont_write_bytecode = True

import test_actions

class HelpActionTests(test_actions.ActionTestCase):
    def __init__(self, *args, **kwargs):
        test_actions.ActionTestCase.__init__(self, *args, **kwargs)
        self.pm_action = 'help'

    def testExit0(self):
        self.RunExitStatusTest(0, list(), verbose=True)

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
