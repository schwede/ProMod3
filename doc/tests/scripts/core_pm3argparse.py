"""
Place the description of your script right in the file and import
it via '__doc__' as description to the parser ('-h', '--help').
"""

from promod3.core import pm3argparse

# make sure we see output when passing '-h'
import ost
ost.PushVerbosityLevel(2) 

parser = pm3argparse.PM3ArgumentParser(__doc__)
parser.AddAlignment()
parser.AssembleParser()
opts = parser.Parse()
