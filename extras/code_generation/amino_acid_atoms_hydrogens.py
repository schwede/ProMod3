# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Code generation of code snippets for hydrogen part of amino_acid_atoms."""

import promod3
from ost import conop
from ost.mol import mm

########################################################################
# HELPER
def CheckLine(line, new_txt):
  if len(line) + len(new_txt) > 80:
      print(line[:-1])
      return "  " + new_txt
  else:
    return my_txt + new_txt

def NameIndex(aa_tlc, name):
  if name in ["N", "CA", "C", "O", "CB"]:
    return "BB_%s_INDEX" % name
  else:
    return "%s_%s_INDEX" % (aa_tlc, name)

def GetHeavyAtomName(aa_tlc, ff_name):
  # only exc. is ILE-CD (ff_name) -> CD1 (pdb_name) for both ff
  if aa_tlc == "ILE" and ff_name == "CD":
    return "CD1"
  else:
    return ff_name

def GetFfResName(ff, aa_tlc):
  # fix residues to have fully protanated variants
  # -> ignored for ASP (ASPH) and GLU (GLUH) as it never happens
  if aa_tlc == "HIS":
    return ff.GetResidueRenamingMain("HISH")
  else:
    return ff.GetResidueRenamingMain(aa_tlc)

def GetHydrogenRules(ff, aa_tlc):
  # rule-format: (number, method, names, anchors)
  hc = ff.GetHydrogenConstructor(GetFfResName(ff, aa_tlc))
  return hc.GetHydrogenRules()

def GetMapping(ff, atom_names):
  atom_mapping_pdb2ff = list()
  for i in range(conop.XXX):
    aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
    # get ff hydro names
    ff_names = list()
    for rule in GetHydrogenRules(ff, aa_tlc):
      ff_names.extend(rule[2])
    # remove H / HN for now
    if "HN" in ff_names:
      ff_names.remove("HN")
    elif "H" in ff_names:
      ff_names.remove("H")
    # check
    pdb_names = atom_names[i]
    assert(len(ff_names) == len(pdb_names))
    # generate mapping
    cur_dict = dict()
    for pdb_name, ff_name in zip(sorted(pdb_names), sorted(ff_names)):
      cur_dict[pdb_name] = ff_name
    atom_mapping_pdb2ff.append(cur_dict)
  return atom_mapping_pdb2ff
########################################################################

# get hydrogen atoms for all amino acids
atom_names = list()
aa_names = list()
comp_lib = conop.GetDefaultLib()
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  c = comp_lib.FindCompound(aa_tlc)
  aa_names.append(c.name.title().replace(" ", ""))
  cur_names = list()
  for a in c.atom_specs:
    # we skip non-Hydrogens, leaving H, HN, ASP-HD2 and GLU-HE2
    # -> HN added specially later
    if     a.element == "H" and not a.is_leaving and a.name != "H" \
       and not (aa_tlc == "ASP" and a.name == "HD2") \
       and not (aa_tlc == "GLU" and a.name == "HE2"):
      cur_names.append(a.name.strip())
  # print "DONE", aa_tlc, cur_names, aa_names[-1]
  atom_names.append(cur_names)

# get renaming rules from FF hydrogen builders
ffc = mm.LoadCHARMMForcefield()
atom_mapping_pdb2charmm = GetMapping(ffc, atom_names)
ffa = mm.LoadAMBERForcefield()
atom_mapping_pdb2amber = GetMapping(ffa, atom_names)

# add special names for nitrogen H (incl. termini) and update naming rules
# -> note H = HN for CHARMM and H1 = H for PDB!
atom_names_pdb = list()
atom_names_charmm = list()
atom_names_amber = list()
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  if aa_tlc == "PRO":
    new_names = ["H1", "H2"]
    new_names_pdb = ["H", "H2"]
    new_names_charmm = ["H1", "H2"]
  else:
    new_names = ["H", "H1", "H2", "H3"]
    new_names_pdb = ["H", "H", "H2", "H3"]
    new_names_charmm = ["HN", "H1", "H2", "H3"]
  # add rest and put into new/old lists
  atom_names_pdb.append(new_names_pdb + atom_names[i])
  new_names_amber = list(new_names)
  for atom_name in atom_names[i]:
    new_names_charmm.append(atom_mapping_pdb2charmm[i][atom_name])
    new_names_amber.append(atom_mapping_pdb2amber[i][atom_name])
  atom_names_charmm.append(new_names_charmm)
  atom_names_amber.append(new_names_amber)
  atom_names[i] = new_names + atom_names[i]
  print("DONE", aa_tlc, aa_names[i])
  if aa_tlc == "PRO":
    print("-> IDX:   ", atom_names[i])
    print("-> PDB:    ", atom_names_pdb[i])
    print("-> CHARMM:", atom_names_charmm[i])
    print("-> AMBER: ", atom_names_amber[i])
  else:
    print("-> IDX:   ", atom_names[i])
    print("-> PDB:     ", atom_names_pdb[i])
    print("-> CHARMM:", atom_names_charmm[i])
    print("-> AMBER:  ", atom_names_amber[i])
  assert(len(atom_names[i]) == len(atom_names_pdb[i]))
  assert(len(atom_names[i]) == len(atom_names_charmm[i]))
  assert(len(atom_names[i]) == len(atom_names_amber[i]))

# get back mapping to CHARMM FF which we'll use to get anchors
ff = ffc;
atom_mapping_ff2pdb = list()
for i in range(conop.XXX):
  cur_dict = dict()
  for pdb_name, ff_name in zip(atom_names_pdb[i], atom_names_charmm[i]):
    cur_dict[ff_name] = pdb_name

# use to get hydrogen construction rules
anchors = dict()
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  for rule in GetHydrogenRules(ff, aa_tlc):
    # all rules have first anchor being place where H is connected
    anchor = GetHeavyAtomName(aa_tlc, rule[3][0])
    for h_name in rule[2]:
      # get pdb name
      idx = atom_names_charmm[i].index(h_name)
      aah = "%s_%s" % (aa_tlc, atom_names_pdb[i][idx])
      assert(aah not in anchors)
      anchors[aah] = NameIndex(aa_tlc, anchor)
  # add terminal N
  anchors["%s_H1" % (aa_tlc)] = NameIndex(aa_tlc, "N")
  anchors["%s_H2" % (aa_tlc)] = NameIndex(aa_tlc, "N")
  if aa_tlc != "PRO":
    anchors["%s_H3" % (aa_tlc)] = NameIndex(aa_tlc, "N")

print("XXX_NUM_HYDROGENS =", sum(len(atom_name) for atom_name in atom_names))
########################################################################

print("\n" + "="*77)
print("I META PROGRAM and HENCE I AM")
print("="*77)

# print out aa-enums
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  print("\nenum %sHydrogenIndex {" % aa_names[i])
  my_txt = "  "
  for name in atom_names[i]:
    new_txt = "%s_%s_INDEX" % (aa_tlc, name)
    new_txt += ", "
    my_txt = CheckLine(my_txt, new_txt)
  new_txt = "%s_NUM_HYDROGENS" % aa_tlc
  my_txt = CheckLine(my_txt, new_txt)
  print(my_txt)
  print("};")

# print out all atoms
print("\nenum AminoAcidHydrogen {")
my_txt = "  "
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  for name in atom_names[i]:
    aah = "%s_%s" % (aa_tlc, name)
    my_txt = CheckLine(my_txt, aah + ", ")
my_txt = CheckLine(my_txt, "XXX_NUM_HYDROGENS")
print(my_txt)
print("};")

# code snippets (assumes "using namespace ost::conop")
print("\n" + "-"*77)
print("\n// HYDROGEN AA-LUT")
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  print("first_aah_[%s] = %s_%s;" \
        % (aa_tlc, aa_tlc, atom_names[i][0]))
  print("num_hydrogens_[%s] = %s_NUM_HYDROGENS;" \
        % (aa_tlc, aa_tlc))
# last one
print('first_aah_[XXX] = XXX_NUM_HYDROGENS;')
print('num_hydrogens_[XXX] = 0;')

# generate code
print("\n" + "-"*77)
print("\n// HYDROGEN AAA-LUT")
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  for idx_name, pdb_name in zip(atom_names[i], atom_names_pdb[i]):
    aah = "%s_%s" % (aa_tlc, idx_name)
    print('hydrogen_name_[%s] = "%s";' % (aah, pdb_name))
    print('anchor_atom_idx_[%s] = %s;' % (aah, anchors[aah]))
# last one
print('hydrogen_name_[XXX_NUM_HYDROGENS] = "UNK";')
print('anchor_atom_idx_[XXX_NUM_HYDROGENS] = XXX_NUM_ATOMS;')

# amber has less renamings so we do that first
print("\n// TODO: add loop to copy hydrogen_name_ to hydrogen_name_amber_")
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  for j in range(len(atom_names[i])):
    if atom_names_amber[i][j] != atom_names_pdb[i][j]:
      print('hydrogen_name_amber_[%s_%s] = "%s";' \
            % (aa_tlc, atom_names[i][j], atom_names_amber[i][j]))
print("\n// TODO: add loop to copy hydrogen_name_amber_ to hydrogen_name_charmm_")
for i in range(conop.XXX):
  aa_tlc = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  for j in range(len(atom_names[i])):
    if atom_names_amber[i][j] != atom_names_charmm[i][j]:
      print('hydrogen_name_charmm_[%s_%s] = "%s";' \
            % (aa_tlc, atom_names[i][j], atom_names_charmm[i][j]))
