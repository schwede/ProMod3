# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from qmean import *
import os
from promod3 import loop
from ost import io, mol
from ost.mol.alg import AssignSecStruct

# Loads the same structure as in the backbone scoring unit test 
# and estimates the statistical potential scores with qmean while
# applying some variations to the structure. The exact values
# can then be compared to the outcome with ProMod3

cb_packing_pot = CBPackingPotential.Load("../data_generation/backbone_scorer/cb_packing_pot.dat")
cb_pot = CBetaPotential.Load("../data_generation/backbone_scorer/cbeta_pot.dat")
reduced_pot = ReducedPotential.Load("../data_generation/backbone_scorer/reduced_pot.dat")
torsion_pot = TorsionPotential.Load("../data_generation/backbone_scorer/torsion_pot.dat")
hbond_pot = HBondPotential.Load("../data_generation/backbone_scorer/hbond_pot.dat")

prot = io.LoadPDB("3DEFA.pdb")

seqres = "MGSLVREWVGFQQFPAATQEKLIEFFGKLKQKDMNSMTVLVLGKGGVGKSSTVNSLIGEQVVRVSPFQA"
seqres += "EGLRPVMVSRTMGGFTINIIDTPGLVEAGYVNHQALELIKGFLVNRTIDVLLYVDRLDVYAVDELDKQ"
seqres += "VVIAITQTFGKEIWCKTLLVLTHAQFSPPDELSYETFSSKRSDSLLKTIRAGSKMRKQEFEDSAIAVV"
seqres += "YAENSGRCSKNDKDEKALPNGEAWIPNLVKAITDVATNQRKAIHVDAAALEHHHHHH"

psipred_ss = "CCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCHHHHHHH"
psipred_ss += "HHCCCCCCCCCCCCEEEEEEEEEEECCCEEEEEEECCCCCCCCCHHHHHHHHHHHHHCCCC"
psipred_ss += "CCEEEEEEECCCCCCCHHHHHHHHHHHHHHCCCCCCCEEEEEECCCCCCCCCCCHHHHHHH"
psipred_ss += "HHHHHHHHHHHHCCCCCCCCCCCCCCEEEEECCCCEEECCCCCEECCCCHHHHHHHHHHHH"
psipred_ss += "HHHHHCCCCCCCCHHHHHHCCCCC"

psipred_conf = "964034131146778999999999999988615887289998899998899999996787653"
psipred_conf += "35667546888999984186689999799840122037999999998633479988999984"
psipred_conf += "78665547799999999998342457858999986001774446899999987999999999"
psipred_conf += "73300011221135865898624523556886300777258999999999999982799633"
psipred_conf += "1777775302479"

data = dict()

data["seq"] = seqres
data["ss"] = psipred_ss
data["conf"] = psipred_conf

psipred_handler = PSIPREDHandler(data)


# before doing anything we define the residue range to be checked
start_rnum = 26
end_rnum = 39

def WriteValueVectors(selection, environment, start_rnum, 
                     end_rnum, vector_base_name):

  AssignSecStruct(environment)

  cb_packing_scores = cb_packing_pot.GetEnergies(selection, environment)
  cb_scores = cb_pot.GetEnergies(selection, environment,False)
  reduced_scores = reduced_pot.GetEnergies(selection, environment,False)
  torsion_scores = torsion_pot.GetEnergies(selection)
  
  full_env_hbond_scores = hbond_pot.GetEnergies(environment)
  hbond_scores = list()
  for s, r in zip(full_env_hbond_scores, environment.residues):
    num = r.GetNumber().GetNum()
    if num >= start_rnum and num <= end_rnum:
      hbond_scores.append(s)
  
  full_ss_agreement_scores = psipred_handler.GetSSAgreementFromChain(environment.chains[0])
  ss_agreement_scores = list()
  for s, r in zip(full_ss_agreement_scores, environment.residues):
    num = r.GetNumber().GetNum()
    if num >= start_rnum and num <= end_rnum:
      ss_agreement_scores.append(s)

  # header
  gaps = " " * (15 + len(vector_base_name))
  print("void Get%sScores(std::vector<Real>& %s_cb_packing," \
        % (vector_base_name.title(), vector_base_name))
  print("%sstd::vector<Real>& %s_cb," % (gaps, vector_base_name))
  print("%sstd::vector<Real>& %s_reduced," % (gaps, vector_base_name))
  print("%sstd::vector<Real>& %s_torsion," % (gaps, vector_base_name))
  print("%sstd::vector<Real>& %s_hbond," % (gaps, vector_base_name))
  print("%sstd::vector<Real>& %s_ss_agreement) {" % (gaps, vector_base_name))

  # values
  for s in cb_packing_scores:
    print("  " + vector_base_name + "_cb_packing.push_back(%.4f);" % (s))
  print()

  for s in cb_scores:
    print("  " + vector_base_name + "_cb.push_back(%.4f);"%(s))
  print()

  for s in reduced_scores:
    print("  " + vector_base_name + "_reduced.push_back(%.4f);"%(s))
  print()

  for s in torsion_scores:
    print("  " + vector_base_name + "_torsion.push_back(%.4f);"%(s))
  print()

  for s in hbond_scores:
    print("  " + vector_base_name + "_hbond.push_back(%.4f);"%(s))
  print()

  for s in ss_agreement_scores:
    print("  " + vector_base_name + "_ss_agreement.push_back(%.4f);"%(s))
  print("}")
  print()

  # use
  print("  std::vector<Real> %s_cb_packing;" % (vector_base_name))
  print("  std::vector<Real> %s_cb;" % (vector_base_name))
  print("  std::vector<Real> %s_reduced;" % (vector_base_name))
  print("  std::vector<Real> %s_torsion;" % (vector_base_name))
  print("  std::vector<Real> %s_hbond;" % (vector_base_name))
  print("  std::vector<Real> %s_ss_agreement;" % (vector_base_name))
  gaps = " " * (12 + len(vector_base_name))
  print("  Get%sScores(%s_cb_packing," \
        % (vector_base_name.title(), vector_base_name))
  print("%s%s_cb," % (gaps, vector_base_name))
  print("%s%s_reduced," % (gaps, vector_base_name))
  print("%s%s_torsion," % (gaps, vector_base_name))
  print("%s%s_hbond," % (gaps, vector_base_name))
  print("%s%s_ss_agreement);" % (gaps, vector_base_name))
  print()


selection = prot.Select("rnum=%s:%s"%(str(start_rnum), str(end_rnum)))
environment = prot.CreateFullView()

WriteValueVectors(selection, environment, start_rnum, end_rnum, "orig")

# not quite a helix (so that we don't land on border cases for torsion scores)
helix_sequence = "FGKLKQKD"
bb_angles = [(-1, -0.75)] * len(helix_sequence)
bb_list_helix = loop.BackboneList(helix_sequence, bb_angles)

n_stem = prot.chains[0].FindResidue(mol.ResNum(26))
transform = bb_list_helix.GetTransform(0, n_stem)
bb_list_helix.ApplyTransform(transform)
bb_list_helix.InsertInto(prot.chains[0], 26)

selection = prot.Select("rnum=%s:%s"%(str(start_rnum), str(end_rnum)))
environment = prot.CreateFullView()
WriteValueVectors(selection, environment, start_rnum, end_rnum, "reset")

rnums_to_delete = list()
for i in range(6):
  rnums_to_delete.append(mol.ResNum(83 + i))

ed = prot.EditXCS()
for rnum in rnums_to_delete:
  ed.DeleteResidue(prot.chains[0].FindResidue(rnum))

selection = prot.Select("rnum=%s:%s"%(str(start_rnum), str(end_rnum)))
environment = prot.CreateFullView()
WriteValueVectors(selection, environment, start_rnum, end_rnum, "deleted")
