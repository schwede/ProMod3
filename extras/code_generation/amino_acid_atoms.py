# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Code generation of code snippets for heavy atom part of amino_acid_atoms."""

from ost import conop
import promod3

# hard-coded backbone atoms
bb_atom_names = ["N", "CA", "C", "O", "CB"]

########################################################################
# HELPER
def CheckLine(line, new_txt):
  if len(line) + len(new_txt) > 80:
      print(line[:-1])
      return "  " + new_txt
  else:
    return my_txt + new_txt

def NameIndex(aa_name, name, aa_atom_names):
  if name in aa_atom_names:
    return "%s_%s_INDEX" % (aa_name, name)
  elif name in bb_atom_names:
    return "BB_%s_INDEX" % name
  else:
    return None
########################################################################

# get heavy sidechain atoms for all amino acids
atom_names = list()
aa_names = list()
element_types = set()
comp_lib = conop.GetDefaultLib()
for i in range(conop.XXX):
  aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  c = comp_lib.FindCompound(aa_name)
  aa_names.append(c.name.title().replace(" ", ""))
  cur_names = list()
  for a in c.atom_specs:
    element_types.add(a.element)
    if a.name == "CB":
      # always first
      cur_names.insert(0, "CB")
    if not a.name in bb_atom_names and \
       not a.is_leaving and not a.name == "OXT" and \
       not a.element in ["D", "H"]:
      cur_names.append(a.name.strip())
  # override TRP (to be consistent with stuff in sidechain module)
  if aa_name == "TRP":
    old_names = cur_names
    cur_names = ["CB", "CG", "CD1", "CD2", "CE2", "NE1", "CE3", "CZ3", "CH2", "CZ2"]
    assert(sorted(old_names) == sorted(cur_names))
  print("DONE", aa_name, cur_names, aa_names[-1])
  atom_names.append(cur_names)

print("XXX_NUM_ATOMS =", sum(len(atom_name) for atom_name in atom_names))

# print out backbone
print("\n" + "-" * 79)
print("\nenum BackboneAtomIndex {")
my_txt = "  "
for name in bb_atom_names:
  my_txt += "BB_%s_INDEX" % name
  if name != bb_atom_names[-1]:
    my_txt += ", "
print(my_txt)
print("};")

# print out aa-enums
for i in range(conop.XXX):
  aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  print("\nenum %sAtomIndex {" % aa_names[i])
  my_txt = "  "
  for name in atom_names[i]:
    new_txt = "%s_%s_INDEX" % (aa_name, name)
    if name == "CB":
      new_txt += " = 4"
    new_txt += ", "
    my_txt = CheckLine(my_txt, new_txt)
  new_txt = "%s_NUM_ATOMS" % aa_name
  if len(atom_names[i]) == 0:
    new_txt += " = 4"
  my_txt = CheckLine(my_txt, new_txt)
  print(my_txt)
  print("};")

# print out all atoms
aaa_list = list()
print("\n" + "-" * 79)
print("\nenum AminoAcidAtom {")
my_txt = "  "
for i in range(conop.XXX):
  aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  for name in bb_atom_names[:4] + atom_names[i]:
    cur_aaa = "%s_%s" % (aa_name, name)
    aaa_list.append(cur_aaa)
    my_txt = CheckLine(my_txt, cur_aaa + ", ")
my_txt = CheckLine(my_txt, "XXX_NUM_ATOMS")
print(my_txt)
print("};")

# print out element types
print("\n" + "-" * 79)
print("\nELEMENTS: ", element_types)

# code snippets (assumes "using namespace ost::conop")
print("\n" + "-" * 79)
print("// AA-LUT\n" + "-" * 79)
for i in range(conop.XXX):
  aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  print("olc_[%s] = '%s';" \
        % (aa_name, conop.ResidueNameToOneLetterCode(aa_name)))
  print("first_aaa_[%s] = %s_N;" \
        % (aa_name, aa_name))
  print("num_atoms_[%s] = %s_NUM_ATOMS;" \
        % (aa_name, aa_name))
# last one
print("olc_[XXX] = 'X';")
print("first_aaa_[XXX] = XXX_NUM_ATOMS;")
print("num_atoms_[XXX] = 0;")

print("\n" + "-" * 79)
print("// AAA-LUT\n" + "-" * 79)
for aaa in aaa_list:
  print('atom_name_[%s] = "%s";' % (aaa, aaa[4:]))
# last one
print('atom_name_[XXX_NUM_ATOMS] = "UNK";')

# print "\n" + "-" * 79
# print "GetIndex\n" + "-" * 79
# for idx, name in enumerate(bb_atom_names[:4]):
#   print 'if (atom_name == "%s"%*s) return %d;' % (name, 2-len(name), "", idx)
# for i in range(conop.XXX):
#   if len(atom_names[i]) == 0:
#     continue
#   aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
#   print "case %s: {" % aa_name
#   for idx, name in enumerate(atom_names[i]):
#     print '  if (atom_name == "%s"%*s) return %d;' \
#           % (name, 3-len(name), "", idx+4)
#   print '  break;'
#   print '}'

# bonds
print("\n" + "-" * 79)
print("// heavy-atom bonding\n" + "-" * 79)
for i in range(conop.XXX):
  aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  c = comp_lib.FindCompound(aa_name)
  anames = [a.name for a in c.atom_specs]
  for b in c.bond_specs:
    idx_str_one = NameIndex(aa_name, anames[b.atom_one], atom_names[i])
    idx_str_two = NameIndex(aa_name, anames[b.atom_two], atom_names[i])
    if not (idx_str_one is None or idx_str_two is None):
      print("bonds_[%s].push_back(BondInfo(%s, %s, %d));" \
            % (aa_name, idx_str_one, idx_str_two, b.order))

# check elements for all heavy sidechain atoms for all amino acids
comp_lib = conop.GetDefaultLib()
for i in range(conop.XXX):
  aa_name = conop.AminoAcidToResidueName(conop.AminoAcid(i))
  c = comp_lib.FindCompound(aa_name)
  for a in c.atom_specs:
    # check name consistency
    if a.name[:1] != a.element:
      print("MISMATCH", aa_name, a.name, a.element)
      