# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

This message should intentionally fail the script.
This script was not ported to Python 3. Should be done along the next time a
real use case pops up.

import os, json

###############################################################################
# SETUP
###############################################################################
# full paths to IN files
in_loop_infos = "loop_infos_train.json"
# use CMA version?
use_cma = True
# output: paths for submission scripts and std outputs
# -> scripts will be executed from current working directory
out_path_sub_scripts = "cma_submission_scripts"
out_path_out_stuff = "cma_out_stuff"
# SCORES to try
score_ids = ["BB", "BB_DB", "BB_DB_AANR", "BB_AANR", "BB_DB_AAR", "BB_AAR"]

###############################################################################
# MAIN
###############################################################################
# check paths
if not os.path.exists(out_path_sub_scripts):
  print("Creating output folder", out_path_sub_scripts)
  os.mkdir(out_path_sub_scripts)
if not os.path.exists(out_path_out_stuff):
  print("Creating output folder", out_path_out_stuff)
  os.mkdir(out_path_out_stuff)

# get loop lengths
json_obj = json.load(open(in_loop_infos, "r"))
length_set = json_obj["length_set"]

# do all score IDs for all length sets
for score_id in score_ids:
  for ll_idx in range(len(length_set) + 1):
    # setup command
    cmd_args = score_id
    if ll_idx < len(length_set):
      cmd_args += " %d" % ll_idx
      file_suffix = "%s_LL%d" % (score_id, ll_idx)
      print("Executing with args %s for lengths in %s" % \
            (cmd_args, str(length_set[ll_idx])))
    else:
      file_suffix = score_id
      print("Executing with args %s for all lengths" % cmd_args)

    # setup script to run
    mystdout = os.path.join(out_path_out_stuff, "out_"+file_suffix+".stdout")
    mystderr = os.path.join(out_path_out_stuff, "out_"+file_suffix+".stderr")

    sub_script_head = [
                        '#!/bin/bash',
                        '#$ -cwd',
                        '#$ -o %s' % mystdout,
                        '#$ -e %s' % mystderr,
                        '#$ -l h=!lmi*',
                        '#$ -l runtime=06:00:00',
                        '#$ -l membycore=3G',
                        '#$ -m as -M gerardo.tauriello@unibas.ch',
                        'ml Python/2.7.5-goolf-1.4.10',
                        'ml numpy/1.9.1-goolf-1.4.10-Python-2.7.5'
                        ]

    sub_script_path = os.path.join(out_path_sub_scripts, "run_"+file_suffix+".sh")
    with open(sub_script_path, 'w') as sub_script:
      sub_script.write('\n'.join(sub_script_head))
      sub_script.write('\n\n')
      if use_cma:
        sub_script.write("python optimize_weights_cma.py " + cmd_args + '\n')
      else:
        sub_script.write("python optimize_weights.py " + cmd_args + '\n')
      sub_script.close()

    os.system("qsub " + sub_script_path)
