Mean text supposed to make any attempt to run the script fail ;)
This script is not ported to Python 3, yet. THe reason is simply that it was not used in a long time plus for tresting you first need the proper use case.

#!/bin/bash
#$ -t 1-500
#$ -cwd
#$ -o run_out_$TASK_ID.stdout
#$ -e run_out_$TASK_ID.stderr
#$ -l h=!lmi*
#$ -l runtime=24:00:00
#$ -l membycore=8G
#$ -m as -M gerardo.tauriello@unibas.ch
ml Boost/1.53.0-goolf-1.4.10-Python-2.7.5
ml Python/2.7.5-goolf-1.4.10
ml numpy/1.9.1-goolf-1.4.10-Python-2.7.5
ml OpenMM/6.1-Linux64

export PROMOD3_ROOT=/scicore/home/schwede/taurielg/ProMod3/build/stage

echo $SGE_TASK_ID
$PROMOD3_ROOT/bin/pm generate_bft_chunks.py chunk $SGE_TASK_ID 100
