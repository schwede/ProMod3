# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: split BFT into training and test data
IN:
- loop_bft.dat and loop_infos.json from generate_bft.py
OUT:
- loop_bft_X.dat and loop_infos_X.json: as in generate_bft.py
-> X: train: data used for training / optimization
      test: data used for validation
-> 2 new fields in json file:
  -> "length_set": length_set[i] is list of loop lengths grouped together
  -> "first_indices_ll": range(first_indices_ll[i], first_indices_ll[i+1])
                         belong to length range length_set[i]
- loop_bft_std.json: dict with list of stds for each key in loop_data_keys
-> first len(length_set) entries: std of BFT for that key and loop length range
-> last entry: std of BFT for that key and all loop lengths
-> data is extracted from training set only and can hence be used in optimizers
   -> 1/std is a good initial guess for weights of a lin. combination of scores
"""

import random, json, os
import numpy as np

###############################################################################
# SETUP
###############################################################################
# full paths to IN files
in_loop_bft = "loop_bft.dat"
in_loop_infos = "loop_infos.json"
# full paths to OUT files (data will be sorted by length groups!)
out_loop_bft_train = "loop_bft_train.dat"
out_loop_infos_train = "loop_infos_train.json"
out_loop_bft_test = "loop_bft_test.dat"
out_loop_infos_test = "loop_infos_test.json"
out_loop_bft_std = "loop_bft_std.json"
# we choose fraction of data for each length range as training data (rest test)
train_fraction = 0.8  # 0.8 (= 80%) should be a good value for this...
length_steps = 2      # x lengths taken together as groups
random.seed(42)       # fixed seed for reproducibilty

###############################################################################
# HELPERS
###############################################################################
def GetFirstIndices(bft_list):
  # indexing into BFT: range(first_indices[i], first_indices[i+1]) for loop i
  num_loops = len(bft_list)
  first_indices = list()
  total_num_lc = 0
  for i in range(num_loops):
    num_lc = bft_list[i].shape[0]
    first_indices.append(total_num_lc)
    total_num_lc += num_lc
  first_indices.append(total_num_lc)
  return first_indices

###############################################################################
# MAIN
###############################################################################
# load input data
bft = np.load(in_loop_bft)
json_obj = json.load(open(in_loop_infos, "r"))
loop_data_keys = json_obj["loop_data_keys"]
first_indices = json_obj["first_indices"]
loop_lengths = json_obj["loop_lengths"]
fragment_indices = json_obj["fragment_indices"]
print("LOADED DATA", bft.nbytes, bft.shape)

# setup
Nrows = bft.shape[0]
Ncols = bft.shape[1]
Nloops = len(first_indices)-1

# length extraction
unique_ll = sorted(set(loop_lengths))
unique_ll_idx = list(range(0, len(unique_ll), length_steps))
length_set = [unique_ll[i:i+length_steps] for i in unique_ll_idx]
Nlengths = len(length_set)

# get loop indices for each loop length
loop_indices_per_ll = list()
for _ in range(Nlengths):
  loop_indices_per_ll.append(list())
for i in range(Nloops):
  ll_idx = [loop_lengths[i] in llr for llr in length_set].index(True)
  loop_indices_per_ll[ll_idx].append(i)

# choose fraction for each
bft_list_train = []
bft_list_test = []
fragment_indices_train = []
loop_lengths_train = []
first_indices_ll_train = [0]
fragment_indices_test = []
loop_lengths_test = []
first_indices_ll_test = [0]
for i_l in range(Nlengths):
  # choose indices
  Nloops_l = len(loop_indices_per_ll[i_l])
  Nloops_train = int(round(train_fraction * Nloops_l))
  Nloops_test = Nloops_l - Nloops_train
  print("LOOP LENGTH RANGE", length_set[i_l], \
        "TRAIN", Nloops_train, "TEST", Nloops_test)
  train_indices = sorted(random.sample(loop_indices_per_ll[i_l], Nloops_train))
  test_indices = sorted(set(loop_indices_per_ll[i_l]) - set(train_indices))
  assert(len(train_indices) == Nloops_train)
  assert(len(test_indices) == Nloops_test)
  # update training lists
  total_num_lc = 0
  for i in train_indices:
    range_start = first_indices[i]
    range_end = first_indices[i+1]
    bft_list_train.append(bft[range_start:range_end, :])
    fragment_indices_train.append(fragment_indices[i])
    loop_lengths_train.append(loop_lengths[i])
    total_num_lc += range_end - range_start
  first_indices_ll_train.append(first_indices_ll_train[-1] + total_num_lc)
  # update testing lists
  total_num_lc = 0
  for i in test_indices:
    range_start = first_indices[i]
    range_end = first_indices[i+1]
    bft_list_test.append(bft[range_start:range_end, :])
    fragment_indices_test.append(fragment_indices[i])
    loop_lengths_test.append(loop_lengths[i])
    total_num_lc += range_end - range_start
  first_indices_ll_test.append(first_indices_ll_test[-1] + total_num_lc)

# combine data
first_indices_train = GetFirstIndices(bft_list_train)
first_indices_test = GetFirstIndices(bft_list_test)
bft_train = np.concatenate(bft_list_train)
bft_test = np.concatenate(bft_list_test)

# get stds from train data for all parts of BFT
bft_std = {key: [] for key in loop_data_keys}
for ll_idx in range(len(length_set)+1):
  if ll_idx == len(length_set):
    my_bft = bft_train
  else:
    bft_start = first_indices_ll_train[ll_idx]
    bft_end = first_indices_ll_train[ll_idx+1]
    my_bft = bft_train[bft_start:bft_end, :]
  for i_c, key in enumerate(loop_data_keys):
    my_std = min(1e100, float(np.std(my_bft[:, i_c])))
    bft_std[key].append(my_std)
    
# dump data
bft_train.dump(out_loop_bft_train)
with open(out_loop_infos_train, "w") as json_file:
  json_obj = {"loop_data_keys": loop_data_keys,
              "first_indices": first_indices_train,
              "loop_lengths": loop_lengths_train,
              "fragment_indices": fragment_indices_train,
              "first_indices_ll": first_indices_ll_train,
              "length_set": length_set}
  json.dump(json_obj, json_file)
# same for test
bft_test.dump(out_loop_bft_test)
with open(out_loop_infos_test, "w") as json_file:
  json_obj = {"loop_data_keys": loop_data_keys,
              "first_indices": first_indices_test,
              "loop_lengths": loop_lengths_test,
              "fragment_indices": fragment_indices_test,
              "first_indices_ll": first_indices_ll_test,
              "length_set": length_set}
  json.dump(json_obj, json_file)
# dump std part
with open(out_loop_bft_std, "w") as json_file:
  json.dump(bft_std, json_file)
