# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: extract subset of DBs for clean scoring
IN: fragments.json from generate_bft_testset.py
OUT:
- sub_structure_db.dat: structure DB excluding extracted chains
- sub_frag_db.dat: fragment DB trained
"""

import json
from promod3 import loop

###############################################################################
# SETUP
###############################################################################
in_fragments = "fragments.json"  # full paths
out_structure_db = "sub_structure_db.dat"
out_frag_db = "sub_frag_db.dat"

###############################################################################
# MAIN
###############################################################################
# init
structure_db = loop.LoadStructureDB()

# get fragments
with open(in_fragments, "r") as json_file:
  json_obj = json.load(json_file)
  fragments = [loop.FragmentInfo(f[0], f[1], f[2]) for f in json_obj]

# get unique chains from fragments
chosen_coord_indices = set([f.chain_index for f in fragments])
# get indices to extract sub DB
N_coord_db = structure_db.GetNumCoords()
indices = [i for i in range(N_coord_db) if i not in chosen_coord_indices]
sub_structure_db = structure_db.GetSubDB(indices)

# build FragDB (settings as in extras/data_generation/frag_db/build_frag_db.py)
max_pairwise_rmsd = 1.0
dist_bin_size = 1.0
angle_bin_size = 20
sub_frag_db = loop.FragDB(dist_bin_size, angle_bin_size)
for i in range(3,15):
  print("start to add fragments of length %d" % i)
  sub_frag_db.AddFragments(i, max_pairwise_rmsd, sub_structure_db)

# dump them
sub_structure_db.PrintStatistics()
sub_structure_db.Save(out_structure_db)
sub_frag_db.PrintStatistics()
sub_frag_db.Save(out_frag_db)
