# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: analyze correlation of scores to ca_rmsd
IN:
- loop_bft.dat and loop_infos.json from generate_bft.py
OUT: (in plots_corr folder)
- corr_X[_LLY|_ALL].png plots: binned ca_rmsd vs score colored by log(count)
-> ca_rmsd axis is restricted to [0,4]
-> score axis is restricted to remove the worst 5%
-> Y: split up in loop length ranges -> Y is starting loop length of range
-> _ALL: all loop lengths considered
"""

import random, json, csv, os
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from common import *

###############################################################################
# SETUP
###############################################################################
# full paths to IN files
in_loop_bft = "loop_bft.dat"
in_loop_infos = "loop_infos.json"
# where to store output files (plots and csv files)
out_path = "plots_corr"
# x lengths taken together as groups
length_steps = 2
# special keys (must match keys in loop_infos.json)
ca_key = "ca_rmsd"
# keys to consider (must be in loop_infos.json)
BBkeys = ["old_weights", "cb_packing", "cbeta", "reduced", "clash", "hbond",
          "torsion"]
AAkeys = ["aa_interaction", "aa_packing", "aa_clash",
          "aa_interaction_no_relax", "aa_packing_no_relax", "aa_clash_no_relax"]
other_keys = ["seq_prof_score", "str_prof_score", "stem_rmsd"]
# weights to try (PM3 weights before Nov. 2016)
old_weight_key = "old_weights"
old_weights = GetOldWeights()

###############################################################################
# MAIN
###############################################################################
# check output path
if not os.path.exists(out_path):
  print("Creating output folder", out_path)
  os.mkdir(out_path)

# load input data
bft = np.load(in_loop_bft)
json_obj = json.load(open(in_loop_infos, "r"))
loop_data_keys = json_obj["loop_data_keys"]
first_indices = json_obj["first_indices"]
loop_lengths = json_obj["loop_lengths"]
fragment_indices = json_obj["fragment_indices"]
print("LOADED DATA", bft.nbytes, bft.shape)

# setup (extra columns = weighted scores)
Ncols = bft.shape[1] + 1
Nloops = len(first_indices)-1
ca_rmsd_idx = loop_data_keys.index(ca_key)

# fix keys
loop_data_keys.append(old_weight_key)
old_weight_vector = np.zeros((bft.shape[1],), dtype=np.float32)
for key in old_weights:
  i_c = loop_data_keys.index(key)
  old_weight_vector[i_c] = old_weights[key]

# length check
unique_ll = sorted(set(loop_lengths))
unique_ll_idx = list(range(0, len(unique_ll), length_steps))
length_set = [unique_ll[i:i+length_steps] for i in unique_ll_idx]
Nlengths = len(length_set)
cur_idx = 0
ll_first = [0] * Nlengths + [bft.shape[0]]
for i in range(Nloops):
  ll_idx = [loop_lengths[i] in llr for llr in length_set].index(True)
  if ll_idx != cur_idx:
    assert(ll_idx == cur_idx+1)
    cur_idx = ll_idx
    ll_first[cur_idx] = first_indices[i]

# get some base data
ca_rmsd_col = bft[:, ca_rmsd_idx]
old_weights_col = bft.dot(old_weight_vector)
# get extent for RMSD
# ca_rmsd_ext = (np.min(ca_rmsd_col), np.percentile(ca_rmsd_col, 95))
ca_rmsd_ext = (np.min(ca_rmsd_col), 4)

# do for all keys and collect stats
my_stats = dict()
keys_to_check = BBkeys + AAkeys + other_keys
for key in keys_to_check:
  # get test column
  if key == old_weight_key:
    to_test = old_weights_col
  else:
    i_c = loop_data_keys.index(key)
    to_test = bft[:, i_c]

  # do for all ranges
  my_stats[key] = list()
  for i_l in range(Nlengths+1):
    # check case
    if i_l < Nlengths:
      range_start = ll_first[i_l]
      range_end = ll_first[i_l+1]
      to_test_x = to_test[range_start:range_end]
      to_test_y = ca_rmsd_col[range_start:range_end]
      file_suffix = "_LL" + str(length_set[i_l][0])
      plot_title = "Correlation for lengths in " + str(length_set[i_l])
    else:
      to_test_x = to_test
      to_test_y = ca_rmsd_col
      file_suffix = "_ALL"
      plot_title = "Correlation for all lengths"

    # get correlations and std
    pr_value = stats.pearsonr(to_test_x, to_test_y)[0]
    sr_value = stats.spearmanr(to_test_x, to_test_y)[0]
    my_stats[key].append([pr_value, sr_value, np.median(to_test_x),
                          np.mean(to_test_x), np.std(to_test_x)])
    print("DOING", key + file_suffix)

    # what to show?
    if "prof" in key:
      extent = (np.percentile(to_test_x, 5), np.max(to_test_x)) + ca_rmsd_ext
    else:
      extent = (np.min(to_test_x), np.percentile(to_test_x, 95)) + ca_rmsd_ext

    # display
    plt.hexbin(to_test_x, to_test_y, bins='log', extent=extent)
    plt.title(plot_title, fontsize='x-large')
    plt.xlabel(key + " value", fontsize='x-large')
    plt.ylabel("ca_rmsd", fontsize='x-large')
    plt.axis(extent)
    plt.colorbar()
    plt.savefig(os.path.join(out_path, "corr_" + key + file_suffix + ".png"))
    plt.close()

# show table of stats
max_key_len = max(len(key) for key in keys_to_check)
my_headers = ["%-*s" % (max_key_len, "CRITERIUM"), "length"]
my_headers += [" Pearson-R", "Spearman-R", "    median", "      mean",
               "       std"]
print("="*79)
print("STATS TABLE")
print("-"*79)
print(", ".join(my_headers))
for key in keys_to_check:
  for i_l in range(Nlengths+1):
    val_list = ["%-*s" % (max_key_len, key)]
    if i_l < Nlengths:
      range_str = "%d-%d" % (length_set[i_l][0], length_set[i_l][-1])
      val_list += ["%-6s" % range_str]
    else:
      val_list += ["all   "]
    val_list += ["%10.5g" % val for val in my_stats[key][i_l]]
    print(", ".join(val_list))
