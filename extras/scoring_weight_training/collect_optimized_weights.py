# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: collect optimized weights from run_all_optimizations*.py
IN: 
- a set of PREFIX_SCORES[_LLX].json files (prefix given via command line)
  from optimize_weights.py or optimize_weights_cma.py
OUT:
- a json file (given via command line) with collected scorer weights
-> the result can then be analyzed with analyze_weights.py
"""

import json, os, sys

usage_string = """
USAGE: python collect_optimized_weights.py IN_PREFIX OUT_FILE
-> IN_PREFIX = IN_PREFIX_SCORES[_LLX].json files expected
-> OUT_FILE = json file dict (key = "SCORES[_LLX]") of dicts of all weights 
              (key = scorer, val = weight)
"""

###############################################################################
# MAIN
###############################################################################
# get input path from command line
if len(sys.argv) < 3:
  print(usage_string)
  sys.exit(1)
in_prefix = sys.argv[1]
out_path = sys.argv[2]

# split prefix into path and file prefix
in_prefix_split = os.path.split(in_prefix)
in_prefix_dir = in_prefix_split[0]
if in_prefix_dir == '':
  in_prefix_dir = '.'
in_prefix_file = in_prefix_split[1]

# collect weights
# -> dict with SCORES[_LLX] as key -> each is dict (key = scorer, val = weight)
file_list = os.listdir(in_prefix_dir)
scorer_weights = dict()
len_to_cut = len(in_prefix_file)+1
for f in sorted(file_list):
  if f.startswith(in_prefix_file):
    my_id = os.path.splitext(f)[0][len_to_cut:]
    file_path = os.path.join(in_prefix_dir, f)
    weights = json.load(open(file_path, "r"))
    scorer_weights[my_id] = weights
    print(my_id, {str(k): v for k,v in weights.items()})

# dump it
with open(out_path, "w") as json_file:
  json.dump(scorer_weights, json_file)
