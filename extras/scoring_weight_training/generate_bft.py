# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: combine chunks into a single BFT
IN:
- loop_data_range_X_Y.json from generate_bft_chunks.py
OUT:
- loop_bft.dat: big table pickled from numpy.matrix (load with numpy.load)
- loop_infos.json: info for each loop:
  -> "loop_data_keys": keys identifying each column in the numpy table
  -> "first_indices": indexing into bft: bft rows in range(first_indices[i],
                      first_indices[i+1]) belong to loop i
  -> "loop_lengths": loop_lengths[i] is length of loop i
  -> "fragment_indices": fragment_indices[i] is loop index into fragments.json
"""

import json, os, numpy, time

###############################################################################
# SETUP
###############################################################################
# full paths to IN and OUT files
in_path = "."  # all json files in that path with correct keys are ok
out_loop_bft = "loop_bft.dat"
out_loop_infos = "loop_infos.json"

###############################################################################
# MAIN
###############################################################################
# collect some files
exp_keys = sorted(["frag_start", "frag_end", "loop_datas"])
file_list = os.listdir(in_path)
# only consider json files
json_files = [f for f in file_list if os.path.splitext(f)[1] == ".json"]
# collect data:
# - BFT -> numpy matrices for each loop
# - fragment_indices -> fragment index (into fragments.json) for each loop
# - loop_lengths -> loop length for each loop
# - loop_data_keys -> sorted keys in lc_data (must be same for each loop!)
bft_list = list()
fragment_indices = list()
loop_lengths = list()
loop_data_keys = None
for file_name in sorted(json_files):
  # try to load json...if we fail, we ignore...
  start_time = time.time()
  try:
    with open(os.path.join(in_path, file_name), "r") as json_file:
      json_obj = json.load(json_file)
  except Exception as ex:
    continue
  # we expect dict
  if type(json_obj) == dict:
    # check for keys (take care for unicode mess)
    cur_keys = sorted([str(key) for key in list(json_obj.keys())])
    if cur_keys == exp_keys:
      # extract data
      frag_start = json_obj["frag_start"]
      frag_end = json_obj["frag_end"]
      loop_datas = json_obj["loop_datas"]
      N_loops = frag_end - frag_start
      assert(len(loop_datas) == N_loops)
      # get loops
      num_lc_range = 0
      for i_loop in range(N_loops):
        loop_data = loop_datas[i_loop]
        lc_data = loop_data["lc_data"]
        if loop_data_keys is None:
          loop_data_keys = sorted(lc_data.keys())
        else:
          assert(loop_data_keys == sorted(lc_data.keys()))
        num_keys = len(loop_data_keys)
        assert(num_keys > 0)
        num_lc = len(lc_data[loop_data_keys[0]])
        # get BFT for this loop
        cur_bft = numpy.empty((num_lc, num_keys), dtype=numpy.float32)
        for i_lc in range(num_lc):
          for i_key in range(num_keys):
            cur_bft[i_lc, i_key] = lc_data[loop_data_keys[i_key]][i_lc]
        # keep relevant data
        bft_list.append(cur_bft)
        fragment_indices.append(frag_start + i_loop)
        loop_lengths.append(loop_data["loop_length"])
        num_lc_range += num_lc
      # report
      print("ADDED %d LC for LOOPS in [%d,%d[ in %g s" \
            % (num_lc_range, frag_start, frag_end, time.time() - start_time))

# check covered ranges
max_frag_idx = max(fragment_indices)
unique_frag_idx = set(fragment_indices)
if len(unique_frag_idx) != max_frag_idx+1:
  missing_indices = sorted(set(range(max_frag_idx+1)) - unique_frag_idx)
  print("MISSING LOOPS FOR", missing_indices)
if len(unique_frag_idx) != len(fragment_indices):
  for frag_idx in unique_frag_idx:
    if fragment_indices.count(frag_idx) > 1:
      print("DUPLICATE LOOPS FOR", frag_idx)

# consistency check
num_loops = len(bft_list)
assert(len(fragment_indices) == num_loops)
assert(len(loop_lengths) == num_loops)

# get indexing into bft: range(first_indices[i], first_indices[i+1]) for loop i
# -> while doing it we also remove cases with 0 or 1 loop candidate
first_indices = list()
total_num_lc = 0
to_remove = []
for i in range(num_loops):
  num_lc = bft_list[i].shape[0]
  if num_lc < 2:
    to_remove.append(i)
  else:
    first_indices.append(total_num_lc)
    total_num_lc += num_lc
# last one must be full size
print("REMOVED", num_loops - len(first_indices), "LOOPS")
first_indices.append(total_num_lc)
# clean up other lists
for i in reversed(to_remove):
  del bft_list[i]
  del fragment_indices[i]
  del loop_lengths[i]
# consistency check
num_loops = len(bft_list)
assert(len(fragment_indices) == num_loops)
assert(len(loop_lengths) == num_loops)
assert(len(first_indices) == num_loops + 1)

# BUILD BFT
num_keys = len(loop_data_keys)
print("BUILDING BFT with %d rows and %d cols" % (total_num_lc, num_keys))
bft = numpy.concatenate(bft_list)
assert(bft.shape[0] == total_num_lc)
assert(bft.shape[1] == num_keys)

# dump data
bft.dump(out_loop_bft)
with open(out_loop_infos, "w") as json_file:
  json_obj = {"loop_data_keys": loop_data_keys,
              "first_indices": first_indices,
              "loop_lengths": loop_lengths,
              "fragment_indices": fragment_indices}
  json.dump(json_obj, json_file)

# some stats
for loop_length in set(loop_lengths):
  num_lc_ll = 0
  num_loops_ll = 0
  for i in range(num_loops):
    if loop_lengths[i] == loop_length:
      num_lc = first_indices[i+1] - first_indices[i]
      num_lc_ll += num_lc
      num_loops_ll += 1
  print("LL", loop_length, "LC", num_lc_ll, "LOOPS", num_loops_ll, \
        "AVG-LC", float(num_lc_ll) / num_loops_ll)
