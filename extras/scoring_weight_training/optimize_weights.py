# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: optimize weights for best possible AUC using Nelder-Mead method
IN:
- loop_bft_train.dat, loop_infos_train.json, loop_bft_std.json from 
  generate_training_bft.py
OUT:
- cma_result_SCORES[_LLX].json with optimized scoring weights
-> launch with "python run_all_optimizations.py" (use_cma = False)
-> collect results with
   $ python collect_optimized_weights.py opt_result opt_weights.json
"""

import random, json, os, sys, time
import numpy as np
from scipy import optimize
from common import *

usage_string = """
USAGE: python optimize_weights.py SCORES [LOOP-LENGTH-RANGE]
-> SCORES = string combination defining scores to optimize for. Strings:
            - 'BB' (Backbone scores)
            - 'AAR' (AllAtom scores after relaxation)
            - 'AANR' (AllAtom scores before relaxation)
            - 'DB' (FragDB specific: profile scores and stem RMSD)
-> LOOP-LENGTH-RANGE = index into 'length_set' array defining length range
                       (if not given, all lengths are considered)
-> e.g. python optimize_weights.py BB_AANR 1
"""

###############################################################################
# SETUP
###############################################################################
# full paths to IN files
in_loop_bft = "loop_bft_train.dat"
in_loop_infos = "loop_infos_train.json"
in_loop_bft_std = "loop_bft_std.json"
# output is json of chosen final weights dict (key = scorer, val = weight)
out_res_json = "opt_result"   # filename = out_res_json + _SCORES[_LLX]
# special keys (must match keys in loop_infos.json)
ca_key = "ca_rmsd"
# keys to consider (must be in loop_infos.json)
# -> we will optimize these keys given command line input
all_keys = dict()
all_keys['BB'] = ["cb_packing", "cbeta", "reduced", "clash", "hbond", "torsion"]
all_keys['AAR'] = ["aa_interaction", "aa_packing", "aa_clash"]
all_keys['AANR'] = ["aa_interaction_no_relax", "aa_packing_no_relax",
                    "aa_clash_no_relax"]
all_keys['DB'] = ["stem_rmsd", "seq_prof_score", "str_prof_score"]
base_weight_key = "hbond"  # if possible limit weight to 1 for that one
# restrict number of function evaluations so they can take at most max_time [h]
max_time = 5  # for 6h queue (with buffer...)
# to optimize: int(CDF(x)) with CDF(x) = P(ca_rmsd <= x) for chosen candidates
drmsd = 0.001     # stepsize for discretization of CDF
max_rmsd = 3      # max. x for CDF(x)

###############################################################################
# MAIN
###############################################################################
# load input data
bft = np.load(in_loop_bft)
bft_std = json.load(open(in_loop_bft_std, "r"))
json_obj = json.load(open(in_loop_infos, "r"))
loop_data_keys = json_obj["loop_data_keys"]
first_indices = json_obj["first_indices"]
loop_lengths = json_obj["loop_lengths"]
fragment_indices = json_obj["fragment_indices"]
first_indices_ll = json_obj["first_indices_ll"]
length_set = json_obj["length_set"]
print("LOADED DATA", bft.nbytes, bft.shape)

# check command line
if len(sys.argv) < 2:
  print(usage_string)
  sys.exit(1)
score_ids = sys.argv[1]
do_all = len(sys.argv) == 2
if do_all:
  ll_idx = len(length_set)
  print("DOING ALL LOOPS")
else:
  ll_idx = int(sys.argv[2])
  assert(ll_idx < len(length_set))
  print("RESTRICTED TO LOOP LENGTHS", length_set[ll_idx])

# get scoring keys
scoring_keys = []
for key in all_keys:
  if key in score_ids:
    scoring_keys += all_keys[key]
scoring_keys.sort()
# if needed we fallback to the first key as base scorer
if base_weight_key not in scoring_keys:
  # we cannot have weight 1 for prof. scores!
  candidate_keys = [key for key in scoring_keys if "prof" not in key]
  assert(len(candidate_keys) > 0)
  base_weight_key = candidate_keys[0]

# setup stuff
ca_rmsd_idx = loop_data_keys.index(ca_key)
base_weight_vector = np.zeros((bft.shape[1],), dtype=np.float32)
weight_indices = []
print("=" * 79)
print("INIT weights:")
for key in scoring_keys:
  # get base weights
  i_c = loop_data_keys.index(key)
  my_weight = bft_std[base_weight_key][ll_idx] / bft_std[key][ll_idx]
  if "prof" in key:
    my_weight = -my_weight
  base_weight_vector[i_c] = my_weight
  print("- %s: %g" % (key, my_weight))
  # only add indices apart from base key
  if key != base_weight_key:
    weight_indices.append(i_c)

# restrict data if requested
if not do_all:
  bft_start = first_indices_ll[ll_idx]
  bft_end = first_indices_ll[ll_idx+1]
  lr_start = first_indices.index(bft_start)
  lr_end = first_indices.index(bft_end)
  bft = bft[bft_start:bft_end, :]
  first_indices = [fi - bft_start for fi in first_indices[lr_start:lr_end+1]]

# setup AUC calculator
Nloops = len(first_indices)-1
ca_rmsd_col = bft[:, ca_rmsd_idx]
auc_calculator = AucCalculator(bft, weight_indices, base_weight_vector,
                               ca_rmsd_col, list(range(Nloops)), first_indices,
                               drmsd, max_rmsd)
initial_weights = base_weight_vector[weight_indices]
print("INIT score:", auc_calculator.GetAUCFromWeightI(initial_weights))

# estimate runtime
start_time = time.time()
res = optimize.minimize(auc_calculator.ToMinimize, initial_weights,
                        method="nelder-mead", options={"maxfev": 10})
el_time = time.time() - start_time
maxfev = int(10 * max_time * 3600 / el_time)

# let's do it...
print("=" * 79)
print("OPTIMIZE with at most %d function evaluations" % maxfev)
start_time = time.time()
res = optimize.minimize(auc_calculator.ToMinimize, initial_weights,
                        method="nelder-mead", options={"maxfev": maxfev})
print("FITTED in %gs" % (time.time() - start_time))
print(res)
print("=" * 79)
final_weights = res.x
print("BEST score:", auc_calculator.GetAUCFromWeightI(final_weights))
print("BEST weights:")
cur_idx = 0
weights_dict = dict()
for key in scoring_keys:
  if key == base_weight_key:
    my_weight = 1
  else:
    my_weight = final_weights[cur_idx]
    cur_idx += 1
  print("- %s: %g" % (key, my_weight))
  weights_dict[key] = my_weight

# dump it
file_name = out_res_json + "_" + score_ids
if do_all:
  file_name += ".json"
else:
  file_name += "_LL%d.json" % ll_idx
with open(file_name, "w") as json_file:
  json.dump(weights_dict, json_file)
