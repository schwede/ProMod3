# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


'''Common functionality for analysis scripts.'''

import json, random
import numpy as np

###############################################################################
# EXPORTED FUNCTIONS
def GetRgbColors():
  """Return list of nice looking colors for plots."""
  rgb_colors = list()
  rgb_colors.append((0.0/255,147.0/255,217.0/255))  # 0: bright blue
  rgb_colors.append((204.0/255,0.0/255,0.0/255))    # 1: red
  rgb_colors.append((1.0,141.0/255,0.0/255))        # 2: orange
  rgb_colors.append((171.0/255,180.0/255,0.0/255))  # 3: green (oliv)
  rgb_colors.append((78.0/255,55.0/255,107.0/255))  # 4: purple
  rgb_colors.append((102.0/255,255.0/255,0.0/255))  # 5: neon green
  rgb_colors.append((0.0/255,0.0/255,255.0/255))    # 6: blue
  return rgb_colors

def GetOldWeights():
  """Return 'old' weights as used in PM3 until Nov. 2016"""
  old_weights = {"hbond": 1,
                 "clash": 0.08880086,
                 "torsion": 0.69712072,
                 "reduced": 2.13535565,
                 "cbeta": 3.59177335,
                 "cb_packing": 1.17280667}
  return old_weights

###############################################################################
# EXPORTED CLASSES
class AucCalculator():
  """Calculator for AUC (etc) for weighted combination of scores."""
  def __init__(self, bft, weight_indices, base_weight_vector, ca_rmsd_col,
               loop_range, first_indices, drmsd, max_rmsd):
    # keep relevant data
    self.bft = bft
    self.weight_indices = weight_indices
    self.base_weight_vector = base_weight_vector
    self.ca_rmsd_col = ca_rmsd_col
    self.loop_range = loop_range
    self.num_loops = len(loop_range)
    self.first_indices = first_indices
    self.drmsd = drmsd
    self.max_rmsd = max_rmsd
    self.num_bins = round(max_rmsd / drmsd) + 1
    # precompute best CA RMSDs
    best_indices = self._GetBestIndices(ca_rmsd_col)
    self.best_ca_rmsds = ca_rmsd_col[best_indices]

  def _GetBestIndices(self, choice_col):
    # get indices of best ones (for some reason this is faster than np.array)
    # -> also it's faster to get indices instead of filling ca_rmsds directly
    best_indices = [0] * self.num_loops
    for i, loop_idx in enumerate(self.loop_range):
      range_start = self.first_indices[loop_idx]
      range_end = self.first_indices[loop_idx+1]
      best_indices[i] = range_start + choice_col[range_start:range_end].argmin()
    return best_indices

  def _GetAUC(self, ca_rmsds):
    # AUC (only look at bins from 0 to num_bins-1)
    ca_rmsd_bin = np.minimum(np.floor(ca_rmsds / self.drmsd), self.num_bins)
    auc = np.sum((self.num_bins - ca_rmsd_bin) * self.drmsd) / self.num_loops
    return auc

  def GetAUCPlain(self, choice_col):
    """Return AUC of CA RMSD dist."""
    best_indices = self._GetBestIndices(choice_col)
    ca_rmsds = self.ca_rmsd_col[best_indices]
    return self._GetAUC(ca_rmsds)

  def GetAUCLoss(self, choice_col):
    """Return AUC of CA RMSD loss dist."""
    best_indices = self._GetBestIndices(choice_col)
    ca_rmsd_losses = self.ca_rmsd_col[best_indices] - self.best_ca_rmsds
    return self._GetAUC(ca_rmsd_losses)

  def GetAUCFromWeightV(self, weight_vector):
    """Return (loss) AUC given weight vector to be applied to BFT."""
    weighted_col = self.bft.dot(weight_vector)
    return self.GetAUCLoss(weighted_col)

  def GetAUCFromWeightI(self, weights):
    """Return (loss) AUC given selected weights (as in weight_indices)."""
    weight_vector = self.base_weight_vector  # no copy needed here...
    weight_vector[self.weight_indices] = weights
    return self.GetAUCFromWeightV(weight_vector)

  def GetAUCFromWeightIN(self, norm_weights):
    """Return (loss) AUC given selected weights (as in weight_indices)
    multiplied by base weights (as in base_weight_vector)."""
    weight_vector = self.base_weight_vector.copy()
    weight_vector[self.weight_indices] *= norm_weights
    return self.GetAUCFromWeightV(weight_vector)

  def ToMinimize(self, weights):
    """Wrapper for minimizers to maximize GetAUCFromWeightI."""
    return -self.GetAUCFromWeightI(weights)

  def ToMinimizeN(self, weights):
    """Wrapper for minimizers to maximize GetAUCFromWeightIN."""
    return -self.GetAUCFromWeightIN(weights)

  def GetCDF(self, weight_vector):
    """Return (loss) CDF used to get AUC with GetAUCFromWeightV."""
    # get CA RMSDs as in GetAUCFromWeightV
    weighted_col = self.bft.dot(weight_vector)
    best_indices = self._GetBestIndices(weighted_col)
    ca_rmsds = self.ca_rmsd_col[best_indices] - self.best_ca_rmsds
    # get CDF
    hist = np.histogram(ca_rmsds, bins=self.num_bins-1, range=[0,self.max_rmsd])
    return hist[0].cumsum() / float(self.num_loops)

  def GetCDFrandom(self):
    """Return (loss) CDF of random loop candidate choice."""
    # get random indices
    best_indices = [0] * self.num_loops
    for i, loop_idx in enumerate(self.loop_range):
      range_start = self.first_indices[loop_idx]
      range_end = self.first_indices[loop_idx+1]
      best_indices[i] = random.randrange(range_start, range_end)
    ca_rmsds = self.ca_rmsd_col[best_indices] - self.best_ca_rmsds
    # get CDF
    hist = np.histogram(ca_rmsds, bins=self.num_bins-1, range=[0,self.max_rmsd])
    return hist[0].cumsum() / float(self.num_loops)

  def GetCDFll(self, weight_vectors, first_indices_ll):
    """Return (loss) CDF assuming different weights for different BFT parts."""
    # get CA RMSDs as in GetAUCFromWeightV
    weighted_cols = []
    Nlengths = len(weight_vectors)
    assert(len(first_indices_ll) == Nlengths + 1)
    for ll_idx in range(Nlengths):
      bft_start = first_indices_ll[ll_idx]
      bft_end = first_indices_ll[ll_idx+1]
      weighted_col = self.bft[bft_start:bft_end, :].dot(weight_vectors[ll_idx])
      weighted_cols.append(weighted_col)
    weighted_col = np.concatenate(weighted_cols)
    best_indices = self._GetBestIndices(weighted_col)
    ca_rmsds = self.ca_rmsd_col[best_indices] - self.best_ca_rmsds
    # get CDF
    hist = np.histogram(ca_rmsds, bins=self.num_bins-1, range=[0,self.max_rmsd])
    return hist[0].cumsum() / float(self.num_loops)

  def GetCDFexp(self, weight_vector_cheap, weight_vector_exp, max_num_exp):
    """Return (loss) CDF with a combination of cheap and expensive scores."""
    weighted_col_cheap = self.bft.dot(weight_vector_cheap)
    weighted_col_exp = self.bft.dot(weight_vector_exp)
    # get best indices
    best_indices = [0] * self.num_loops
    for i, loop_idx in enumerate(self.loop_range):
      range_start = self.first_indices[loop_idx]
      range_end = self.first_indices[loop_idx+1]
      sort_indices = weighted_col_cheap[range_start:range_end].argsort()
      sub_exp = weighted_col_exp[range_start + sort_indices[:max_num_exp]]
      best_indices[i] = range_start + sort_indices[sub_exp.argmin()]
    # get CA RMSDs
    ca_rmsds = self.ca_rmsd_col[best_indices] - self.best_ca_rmsds
    # get CDF
    hist = np.histogram(ca_rmsds, bins=self.num_bins-1, range=[0,self.max_rmsd])
    return hist[0].cumsum() / float(self.num_loops)

# these will be exported when doing import * from...
__all__ = ('GetRgbColors', 'GetOldWeights', 'AucCalculator')
