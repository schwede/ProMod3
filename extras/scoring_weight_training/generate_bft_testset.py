# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: extract loop examples from structure DB
OUT: fragments.json: list of fragment infos ([chain_idx, offset, length])
-> fragments are linked to PM3's default structure DB
-> each fragment has at least 50% coils and doesn't touch termini
"""

import random, json
from ost import conop
from promod3 import loop

###############################################################################
# SETUP
###############################################################################
min_coord_size = 50        # min. size for StructureDB entry to be considered
terminal_res_to_skip = 2   # first and last x residues are never chosen for loop
num_chains = 4000          # get x random chains from structure DB
num_loops_per_len = 5000   # number of loops to pick for each loop length
loop_range = list(range(3,15))   # range of loop lengths (uniformly distributed)
random.seed(42)            # fixed seed for reproducibilty
out_fragments = "fragments.json"  # full path!

###############################################################################
# HELPERS
###############################################################################
def IsValidSequence(sequence):
  # False if any non-standard AA
  for olc in sequence:
    if conop.OneLetterCodeToAminoAcid(olc) == conop.XXX:
      return False
  return True

###############################################################################
# MAIN
###############################################################################
# init
structure_db = loop.LoadStructureDB()
assert(terminal_res_to_skip >= 1) # no terminal loops

# pick chains (keep coord indices for each good one)
coord_indices = set()
N_coord_db = structure_db.GetNumCoords()
while len(coord_indices) < num_chains:
  # pick random one that is not in there yet
  coord_idx = random.randrange(0, N_coord_db)
  while coord_idx in coord_indices:
    coord_idx = random.randrange(0, N_coord_db)
  # check sequence and add if valid and long enough
  coord_info = structure_db.GetCoordInfo(coord_idx)
  frag_info = loop.FragmentInfo(coord_idx, 0, coord_info.size)
  sequence = structure_db.GetSequence(frag_info)
  if IsValidSequence(sequence) and coord_info.size > min_coord_size:
    coord_indices.add(coord_idx)

# extract big lists appending all chains
print("NUM CHAINS = %d" % num_chains)
is_coil = []      # 1 = coil, 0 = not, len = all residues
first_idx = {}    # first_idx[coord_idx] = pos. of 1st res. in is_coil
back_mapping = [] # back mapping to coord index, len = all residues
res_to_skip = []  # True = no loop can cover that, len = all residues
for coord_idx in coord_indices:
  # get coilness
  coord_info = structure_db.GetCoordInfo(coord_idx)
  chain_length = coord_info.size
  frag_info = loop.FragmentInfo(coord_idx, 0, chain_length)
  dssp_states = structure_db.GetDSSPStates(frag_info)
  # add data
  is_coil.extend([1 if el in ['T','C','S'] else 0 for el in dssp_states])
  first_idx[coord_idx] = len(back_mapping)
  back_mapping.extend([coord_idx] * chain_length)
  res_to_skip.extend(  [True] * terminal_res_to_skip \
                     + [False] * (chain_length - terminal_res_to_skip*2) \
                     + [True] * terminal_res_to_skip)

# add sentinel
num_residues = len(is_coil)
# check
assert(len(first_idx) == num_chains)
assert(len(back_mapping) == num_residues)
assert(len(res_to_skip) == num_residues)
print("NUM RESIDUES = %d with %d coils" % (num_residues, sum(is_coil)))

# extract loops for each loop length (list of [chain_idx, offset, length])
fragments = []
for loop_length in loop_range:
  # any entry in here is a valid start index
  possible_start_indices = []
  num_coil_thresh = round(loop_length*0.5)
  range_end = num_residues - loop_length - terminal_res_to_skip
  for start_idx in range(terminal_res_to_skip, range_end):
    num_coil = sum(is_coil[start_idx:start_idx+loop_length])
    to_skip = any(res_to_skip[start_idx:start_idx+loop_length])
    if num_coil >= num_coil_thresh and not to_skip:
      possible_start_indices.append(start_idx)
  # show it
  print("LOOP LEN %d: %d choices" % (loop_length, len(possible_start_indices)))
  # random choice
  start_indices = random.sample(possible_start_indices, num_loops_per_len)
  for start_idx in start_indices:
    coord_idx = back_mapping[start_idx]
    offset = start_idx - first_idx[coord_idx]
    fragments.append(loop.FragmentInfo(coord_idx, offset, loop_length))

# check all fragments
for fragment in fragments:
  # check termini
  coord_info = structure_db.GetCoordInfo(fragment.chain_index)
  assert(fragment.offset >= terminal_res_to_skip)
  assert(fragment.offset + fragment.length + terminal_res_to_skip\
         <= coord_info.size)
  # check coils
  frag_info = loop.FragmentInfo(fragment.chain_index, 0, coord_info.size)
  dssp_states = structure_db.GetDSSPStates(frag_info)
  coilness = [1 if el in ['T','C','S'] else 0 for el in dssp_states]
  assert(len(coilness) == coord_info.size)
  num_coil = sum(coilness[fragment.offset:fragment.offset+fragment.length])
  assert(num_coil >= round(fragment.length*0.5))

# store it
with open(out_fragments, "w") as json_file:
  json_obj = [[f.chain_index, f.offset, f.length] for f in fragments]
  json.dump(json_obj, json_file)

# just checking...
chosen_coord_indices = set([f.chain_index for f in fragments])
print("NEVER CHOSE %d chains" % (num_chains - len(chosen_coord_indices)))
# for coord_idx in (coord_indices - chosen_coord_indices):
#   coord_info = structure_db.GetCoordInfo(coord_idx)
#   frag_info = loop.FragmentInfo(coord_idx, 0, coord_info.size)
#   dssp_states = structure_db.GetDSSPStates(frag_info)
#   num_coil = sum(1 for el in dssp_states if el in ['T','C','S'])
#   print "NEVER CHOSE:", coord_idx, coord_info.size, num_coil

# look at overlaps
covered = [0] * num_residues
for fragment in fragments:
  start_idx = first_idx[fragment.chain_index] + fragment.offset
  for i in range(start_idx, start_idx+fragment.length):
    covered[i] += 1

# report overlaps
print("COVERAGE NEEDED", sum(i*num_loops_per_len for i in loop_range))
for i in range(max(covered)+1):
  print("COVERAGE: %d loops on %d residues" % (i, covered.count(i)))
