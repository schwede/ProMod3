# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: analyze performance of single scores and effect of within_bounds
-> need to manually tune wb_mode variable below to check within_bounds
IN:
- loop_bft.dat and loop_infos.json from generate_bft.py
OUT: (in plots folder)
- X[_loss][_LLY][_wbZ].png plots: prob. to find loop candidate with ca_rmsd <= x
-> we select the best LC according to a single criterium or the old BB weights
   used in PM3 (before Nov. 2016)
-> we compare also against the best LC and a randomly chosen one as baseline
-> X: aa_scores: compare all all atom scores
      bb_scores: compare all all atom scores
      other_scores: profile scores, stem RMSD and pot. E. after relaxation
-> _loss: instead of ca_rmsd we look at ca_rmsd difference to best one
-> Y: split up in loop length ranges -> Y is starting loop length of range
-> Z: wb_mode setting (see below)
- auc[_loss][_wbZ].csv: AUC table with integrals of prob. func. hown in plots
-> _loss and Z as above
"""

import random, json, csv, os
import matplotlib.pyplot as plt
import numpy as np
from common import *

###############################################################################
# SETUP
###############################################################################
# full paths to IN files
in_loop_bft = "loop_bft.dat"
in_loop_infos = "loop_infos.json"
# where to store output files (plots and csv files)
out_path = "plots"
# goal: compute CDF(x) = P(ca_rmsd <= x) for chosen candidates
drmsd = 0.01      # stepsize for discretization of CDF
max_rmsd = 3      # max. x for CDF(x)
wb_mode = 0       # 0 = all loops, 1 = all lc, only loops with lc within bounds
                  #                2 = as 1 but only lc within bounds
length_steps = 2  # x lengths taken together as groups
random.seed(42)   # fixed seed for reproducibilty
# special keys (must match keys in loop_infos.json)
ca_key = "ca_rmsd"
wb_key = "within_bounds"
# what to show (one plot for each -> each must be in loop_infos.json)
BBkeys = ["old_weights", "cb_packing", "cbeta", "reduced", "clash", "hbond",
          "torsion"]
AAkeys = ["aa_interaction", "aa_packing", "aa_clash",
          "aa_interaction_no_relax", "aa_packing_no_relax", "aa_clash_no_relax"]
other_keys = ["seq_prof_score", "str_prof_score", "stem_rmsd", "relax_pot_e"]
file_ending = "png"  # common choices: png or pdf as output for plots
# nice looking colors for plotting
rgb_colors = GetRgbColors()
# global var for internal use (key name for random choice)
rand_key = "random"
# weights to try (PM3 weights before Nov. 2016)
old_weight_key = "old_weights"
old_weights = GetOldWeights()

###############################################################################
# HELPERS
###############################################################################
def GetCDF(ca_rmsd, drmsd, max_rmsd):
  num_bins = round(max_rmsd / drmsd)
  hist = np.histogram(ca_rmsd, bins=num_bins, range=[0,max_rmsd])
  return hist[0].cumsum() / float(ca_rmsd.size)

def GenPlot(x, cdf, keys, loop_data_keys, file_name, i_l=None):
  # always in new figure
  plt.figure()
  # check keys
  had_ca_rmsd = ca_key in keys
  if had_ca_rmsd:
    keys.remove(ca_key)
  had_random = rand_key in keys
  if had_random:
    keys.remove(rand_key)
  # CA RMSD as base?
  if had_ca_rmsd:
    if i_l is None:
      y = cdf[ca_rmsd_idx]
    else:
      y = cdf[ca_rmsd_idx][i_l]
    plt.plot(np.insert(x,0,0), np.insert(y,0,0), color='k', linewidth=2,
             linestyle='dashed', label="best cand.")
  # draw scores
  for i,key in enumerate(keys):
    if key not in [ca_key, rand_key]:
      i_c = loop_data_keys.index(key)
      if i_l is None:
        y = cdf[i_c]
      else:
        y = cdf[i_c][i_l]
      plt.plot(np.insert(x,0,0), np.insert(y,0,0), color=rgb_colors[i],
               linewidth=1.5, label=key)
  # add random?
  if had_random:
    i_c = loop_data_keys.index(rand_key)
    if i_l is None:
      y = cdf[i_c]
    else:
      y = cdf[i_c][i_l]
    plt.plot(np.insert(x,0,0), np.insert(y,0,0), color='k', linewidth=2,
             linestyle='dotted', label="random cand.")
  # make it pretty
  car_label = "ca_rmsd"
  if "_loss" in file_name:
    car_label += "_loss"
  plt.title("Prob. to find loop candidate with " + car_label + " <= x",
            fontsize='x-large')
  plt.xlabel("x [Angstrom]", fontsize='x-large')
  plt.ylabel("P[" + car_label + " <= x]", fontsize='x-large')
  plt.tick_params(axis='both', which='major', labelsize='large')
  plt.tick_params(axis='both', which='minor', labelsize='large')
  plt.axis((0, max_rmsd, 0, 1))
  plt.legend(loc='lower right', frameon=False)
  plt.savefig(file_name)
  #plt.show()
  # clean up
  plt.close()

###############################################################################
# MAIN
###############################################################################
# check output path
if not os.path.exists(out_path):
  print("Creating output folder", out_path)
  os.mkdir(out_path)

# load input data
bft = np.load(in_loop_bft)
json_obj = json.load(open(in_loop_infos, "r"))
loop_data_keys = json_obj["loop_data_keys"]
first_indices = json_obj["first_indices"]
loop_lengths = json_obj["loop_lengths"]
fragment_indices = json_obj["fragment_indices"]
print("LOADED DATA", bft.nbytes, bft.shape)

# extract all possible data
# -> ca_rmsds[C][L] = list of ca_rmsd of chosen loop for loops of
#    length in length_set[L] and using column C as selection criterium
unique_ll = sorted(set(loop_lengths))
unique_ll_idx = list(range(0, len(unique_ll), length_steps))
length_set = [unique_ll[i:i+length_steps] for i in unique_ll_idx]
ca_rmsds = []
# setup (extra columns = random choice, weighted scores)
Ncols = bft.shape[1] + 2
Nloops = len(first_indices)-1
Nlengths = len(length_set)
ca_rmsd_idx = loop_data_keys.index(ca_key)
wb_idx = loop_data_keys.index(wb_key)
for _ in range(Ncols):
  new_list = []
  for _ in range(Nlengths):
    new_list.append([])
  ca_rmsds.append(new_list)

# fix keys
loop_data_keys.append(rand_key)
loop_data_keys.append(old_weight_key)
old_weight_vector = np.zeros((bft.shape[1],))
for key in old_weights:
  i_c = loop_data_keys.index(key)
  old_weight_vector[i_c] = old_weights[key]
assert(len(loop_data_keys) == Ncols)
# for each col, do we need to check max or min?
get_max = ["prof" in key for key in loop_data_keys]

# get best CA RMSD for each loop
for i in range(Nloops):
  ll_idx = [loop_lengths[i] in llr for llr in length_set].index(True)
  range_start = first_indices[i]
  range_end = first_indices[i+1]
  loop_bft = bft[range_start:range_end, :]
  # filter bft
  if wb_mode > 0:
    mask = loop_bft[:, wb_idx] > 0
    # skip ones without and lc within bounds
    if np.count_nonzero(mask) == 0:
      continue
    if wb_mode == 2:
      loop_bft = loop_bft[mask, :]
  # check columns
  for j in range(Ncols):
    if j == Ncols-2:
      # special random column
      best_idx = random.randrange(loop_bft.shape[0])
    elif j == Ncols-1:
      # use old weights
      best_idx = loop_bft.dot(old_weight_vector).argmin()
    elif get_max[j]:
      best_idx = loop_bft[:, j].argmax()
    else:
      best_idx = loop_bft[:, j].argmin()
    ca_rmsd = loop_bft[best_idx, ca_rmsd_idx]
    ca_rmsds[j][ll_idx].append(ca_rmsd)

# get CDF for each key: CDF(x) = P(ca_rmsd <= x)
# -> cdf[C] = cdf for all loops combined
# -> cdf_per_ll[C][L] = cdf for each length set (indexing like length_set)
# -> .._loss = same but using ca_rmsd[key] - ca_rmsd[ca_key]
cdf = []
cdf_loss = []
cdf_per_ll = []
cdf_loss_per_ll = []
for i_c in range(Ncols):
  # collect all lengths
  ca_rmsd = np.array([], dtype=np.float32)
  ca_rmsd_loss = np.array([], dtype=np.float32)
  cur_cdf_per_ll = []
  cur_cdf_loss_per_ll = []
  for i_l in range(Nlengths):
    cur_ca_rmsd = np.asarray(ca_rmsds[i_c][i_l])
    cur_ca_rmsd_loss = cur_ca_rmsd - np.asarray(ca_rmsds[ca_rmsd_idx][i_l])
    ca_rmsd = np.concatenate((ca_rmsd, cur_ca_rmsd))
    ca_rmsd_loss = np.concatenate((ca_rmsd_loss, cur_ca_rmsd_loss))
    cur_cdf_per_ll.append(GetCDF(cur_ca_rmsd, drmsd, max_rmsd))
    cur_cdf_loss_per_ll.append(GetCDF(cur_ca_rmsd_loss, drmsd, max_rmsd))
  # update data
  cdf.append(GetCDF(ca_rmsd, drmsd, max_rmsd))
  cdf_loss.append(GetCDF(ca_rmsd_loss, drmsd, max_rmsd))
  cdf_per_ll.append(cur_cdf_per_ll)
  cdf_loss_per_ll.append(cur_cdf_loss_per_ll)

# plot stuff
x = np.arange(0, max_rmsd, drmsd) + drmsd
if wb_mode > 0:
  file_suffix = "_wb" + str(wb_mode) + "." + file_ending
else:
  file_suffix = "." + file_ending
# do per length plot for CA RMSD
plt.figure()
for i_l in range(Nlengths):
  y = cdf_per_ll[ca_rmsd_idx][i_l]
  plt.plot(np.insert(x,0,0), np.insert(y,0,0), color=rgb_colors[i_l],
           linewidth=1.5, label="loop length in " + str(length_set[i_l]))
y = cdf[ca_rmsd_idx]
plt.plot(np.insert(x,0,0), np.insert(y,0,0), color='k', linewidth=2,
         linestyle='dashed', label="all loop lengths")
# make it pretty
plt.title("Prob. to find best loop candidate with ca_rmsd <= x",
          fontsize='x-large')
plt.xlabel("x [Angstrom]", fontsize='x-large')
plt.ylabel("P[ca_rmsd <= x]", fontsize='x-large')
plt.tick_params(axis='both', which='major', labelsize='large')
plt.tick_params(axis='both', which='minor', labelsize='large')
plt.axis((0, max_rmsd, 0, 1))
plt.legend(loc='lower right', frameon=False)
plt.savefig(os.path.join(out_path, "best_per_ll" + file_suffix))
plt.close()
# CA RMSD vs all criteria
GenPlot(x, cdf, [ca_key, rand_key] + BBkeys, loop_data_keys,
        os.path.join(out_path, "bb_scores" + file_suffix))
GenPlot(x, cdf, [ca_key, rand_key] + AAkeys, loop_data_keys,
        os.path.join(out_path, "aa_scores" + file_suffix))
GenPlot(x, cdf, [ca_key, rand_key] + other_keys, loop_data_keys,
        os.path.join(out_path, "other_scores" + file_suffix))
if wb_mode != 2:
  GenPlot(x, cdf_loss, [rand_key] + BBkeys, loop_data_keys,
          os.path.join(out_path, "bb_scores_loss" + file_suffix))
  GenPlot(x, cdf_loss, [rand_key] + AAkeys, loop_data_keys,
          os.path.join(out_path, "aa_scores_loss" + file_suffix))
  GenPlot(x, cdf_loss, [rand_key] + other_keys, loop_data_keys,
          os.path.join(out_path, "other_scores_loss" + file_suffix))
for i_l in range(Nlengths):
  file_suffix_ll = "_LL%d%s" % (length_set[i_l][0], file_suffix)
  GenPlot(x, cdf_per_ll, [ca_key, rand_key] + BBkeys, loop_data_keys, 
          os.path.join(out_path, "bb_scores" + file_suffix_ll), i_l)
  GenPlot(x, cdf_per_ll, [ca_key, rand_key] + AAkeys, loop_data_keys,
          os.path.join(out_path, "aa_scores" + file_suffix_ll), i_l)
  GenPlot(x, cdf_per_ll, [ca_key, rand_key] + other_keys, loop_data_keys,
          os.path.join(out_path, "other_scores" + file_suffix_ll), i_l)
  if wb_mode != 2:
    GenPlot(x, cdf_loss_per_ll, [rand_key] + BBkeys, loop_data_keys,
            os.path.join(out_path, "bb_scores_loss" + file_suffix_ll), i_l)
    GenPlot(x, cdf_loss_per_ll, [rand_key] + AAkeys, loop_data_keys,
            os.path.join(out_path, "aa_scores_loss" + file_suffix_ll), i_l)
    GenPlot(x, cdf_loss_per_ll, [rand_key] + other_keys, loop_data_keys,
            os.path.join(out_path, "other_scores_loss" + file_suffix_ll), i_l)

# get AUC table (same layout as cdfs but with dict for key)
auc_keys = [ca_key, rand_key] + BBkeys + AAkeys + other_keys
auc = dict()
auc_loss = dict()
auc_per_ll = dict()
auc_loss_per_ll = dict()
for key in auc_keys:
  i_c = loop_data_keys.index(key)
  auc[key] = cdf[i_c].sum() * drmsd
  auc_loss[key] = cdf_loss[i_c].sum() * drmsd
  auc_per_ll[key] = []
  auc_loss_per_ll[key] = []
  for i_l in range(Nlengths):
    auc_per_ll[key].append(cdf_per_ll[i_c][i_l].sum() * drmsd)
    auc_loss_per_ll[key].append(cdf_loss_per_ll[i_c][i_l].sum() * drmsd)

# get best sorting
to_sort = [(auc[key],key) for key in auc_keys]
sorted_keys = [key for _,key in sorted(to_sort, reverse=True)]

# show AUC table (row = key, cols = lengths)
max_key_len = max(len(key) for key in auc_keys)
my_headers = ["  ALL"] + ["LL%3d" % llr[0] for llr in length_set]
print("="*79)
print("AUC TABLE")
print("-"*79)
print(("%-*s " % (max_key_len, "CRITERIUM")) + ", ".join(my_headers))
for key in sorted_keys:
  val_list = ["%5.2f" % auc[key]]
  val_list += ["%5.2f" % auc_per_ll[key][i_l] for i_l in range(Nlengths)]
  print(("%-*s:" % (max_key_len, key)) + ", ".join(val_list))
if wb_mode != 2:
  print("-"*79)
  print(("%-*s " % (max_key_len, "CRITERIUM (LOSS)")) + ", ".join(my_headers))
  for key in sorted_keys:
    val_list = ["%5.2f" % auc_loss[key]]
    val_list += ["%5.2f" % auc_loss_per_ll[key][i_l] for i_l in range(Nlengths)]
    print(("%-*s:" % (max_key_len, key)) + ", ".join(val_list))

# dump to CSV
my_headers = ["Criterium", "all lengths"]
my_headers += ["lengths in " + str(llr) for llr in length_set]
csv_rows = [my_headers]
csv_rows_loss = [my_headers]
for key in sorted_keys:
  val_list = [key, auc[key]]
  val_list += [auc_per_ll[key][i_l] for i_l in range(Nlengths)]
  csv_rows.append(val_list)
  val_list = [key, auc_loss[key]]
  val_list += [auc_loss_per_ll[key][i_l] for i_l in range(Nlengths)]
  csv_rows_loss.append(val_list)
if wb_mode > 0:
  file_suffix = "_wb" + str(wb_mode) + ".csv"
else:
  file_suffix = ".csv"
with open(os.path.join(out_path, "auc" + file_suffix), 'w') as csvfile:
  csv_writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
  csv_writer.writerows(csv_rows)
if wb_mode != 2:
  with open(os.path.join(out_path, "auc_loss" + file_suffix), 'w') as csvfile:
    csv_writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
    csv_writer.writerows(csv_rows_loss)

# NOTE:
# - fast integral -> sum of cum. sum can be done as weighted sum
# -> binning with floor, cap to max_bin + 1, sum((max_bin + 1 - bin) * bin_size)
