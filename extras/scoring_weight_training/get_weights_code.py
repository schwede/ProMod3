# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: get code snippets for weights of interest
IN:
- a json file (given via command line) generated with collect_optimized_weights
OUT:
- Python code snippet with all weights
"""

import json, sys

usage_string = """
USAGE: python get_weights_code.py VAR_NAME SCORE_ID WEIGHTS_FILE
-> VAR_NAME = var. name to store weights to
-> SCORE_ID = key as used in output o  collect_optimized_weights.py
-> WEIGHTS_FILE = json file as generated by collect_optimized_weights.py
"""

###############################################################################
# MAIN
###############################################################################
# get input path from command line
if len(sys.argv) < 4:
  print(usage_string)
  sys.exit(1)
var_name = sys.argv[1]
score_id = sys.argv[2]
in_path = sys.argv[3]

# get weights
scorer_weights = json.load(open(in_path, "r"))
weights = scorer_weights[score_id]

# check redundant scores
keys = []
for key, weight in weights.items():
  if    ("prof" in key and weight >= 0) \
     or ("prof" not in key and weight <= 0):
    print("REDUNDANT (presumably) score %s for set %s (weight was %g)" \
          % (key, score_id, weight))
  else:
    keys.append(key)

# parse it
keys.sort()
Nkeys = len(keys)
for i in range(Nkeys):
  weight = weights[keys[i]]
  my_txt = '"%s": %g' % (keys[i], weight)
  if i == 0:
    my_txt = var_name + " = {" + my_txt
  else:
    my_txt = (" " * (len(var_name)+4)) + my_txt
  if i < Nkeys-1:
    my_txt += ","
  else:
    my_txt += "}"
  print(my_txt)
