# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
GOAL: analyze number of loop candidates in BFT (also considering within_bounds)
IN:
- loop_bft.dat and loop_infos.json from generate_bft.py
OUT: (in plots folder)
- lc_counts.png: plot of prob. to find at least x loop candidates
- lc_counts_wb.png: same but only considering LC with within_bounds = 1
"""

import json, os
import matplotlib.pyplot as plt
import numpy as np
from common import *

###############################################################################
# SETUP
###############################################################################
# full paths to IN files
in_loop_bft = "loop_bft.dat"
in_loop_infos = "loop_infos.json"
out_path = "plots"          # where to store output files (plots)
wb_key = "within_bounds"    # WB key (must match keys in loop_infos.json)
length_steps = 2            # x lengths taken together as groups
max_count = 40              # CDF goes up to max_count loop candidates
# nice looking colors for plotting
rgb_colors = GetRgbColors()
file_ending = "png"  # common choices: png or pdf as output for plots

###############################################################################
# HELPERS
###############################################################################
def GetCDF(lc_counts):
  # cdf[i] = P(lc_count > i)
  hist = np.histogram(lc_counts, bins=max_count, range=[0,max_count-0.01])
  return 1 - hist[0].cumsum() / float(lc_counts.size)

def GenPlot(x, cdf, cdf_per_ll, length_set, file_name=None):
  # always in new figure
  plt.figure()
  # start with full set
  y = cdf
  plt.plot(np.insert(x, 0, 0.0), np.insert(y, 0, 1.0), color='k',
           linewidth=2, linestyle='dashed', label="all lengths")
  # then rest
  for i_l, llr in enumerate(length_set):
    y = cdf_per_ll[i_l]
    color = rgb_colors[i_l % len(rgb_colors)]
    label = "lengths in " + str(llr)
    plt.plot(np.insert(x, 0, 0.0), np.insert(y, 0, 1.0), color=color,
             linewidth=1.5, label=label)
  # make it pretty
  plt.title("Prob. to find at least x loop candidates",
            fontsize='x-large')
  plt.xlabel("x [count]", fontsize='x-large')
  plt.ylabel("P[lc_count >= x]", fontsize='x-large')
  plt.tick_params(axis='both', which='major', labelsize='large')
  plt.tick_params(axis='both', which='minor', labelsize='large')
  plt.axis((0, max_count, 0, 1))
  plt.legend(loc='best', frameon=False)
  if file_name is not None:
    plt.savefig(file_name)
  #plt.show()
  # clean up
  plt.close()

def PrintStats(label, lc_count, lc_count_wb):
  num_loops = lc_count.size
  num_lc = lc_count.sum()
  num_lc_wb = lc_count_wb.sum()
  num_non_zero_wb = np.count_nonzero(lc_count_wb)
  print(label, "- LOOPS", num_loops, num_non_zero_wb, \
        "LC %d, %.3g" % (num_lc, float(num_lc) / num_loops), \
        "LC-WB %d, %.3g" % (num_lc_wb, float(num_lc_wb) / num_loops))

###############################################################################
# MAIN
###############################################################################
# check output path
if not os.path.exists(out_path):
  print("Creating output folder", out_path)
  os.mkdir(out_path)

# load input data
bft = np.load(in_loop_bft)
json_obj = json.load(open(in_loop_infos, "r"))
loop_data_keys = json_obj["loop_data_keys"]
first_indices = json_obj["first_indices"]
loop_lengths = json_obj["loop_lengths"]
fragment_indices = json_obj["fragment_indices"]
print("LOADED DATA", bft.nbytes, bft.shape)

# setup
wb_idx = loop_data_keys.index(wb_key)
unique_ll = sorted(set(loop_lengths))
unique_ll_idx = list(range(0, len(unique_ll), length_steps))
length_set = [unique_ll[i:i+length_steps] for i in unique_ll_idx]
Nloops = len(first_indices)-1
Nlengths = len(length_set)

# count total LC and LC within bounds
# -> lc_counts..[L] = num. LC for each loop of length in length_set[L]
lc_counts = []
lc_counts_wb = []
for _ in range(Nlengths):
  lc_counts.append([])
  lc_counts_wb.append([])

# get data
for i in range(Nloops):
  ll_idx = [loop_lengths[i] in llr for llr in length_set].index(True)
  range_start = first_indices[i]
  range_end = first_indices[i+1]
  wb_col = bft[range_start:range_end, wb_idx]
  lc_counts[ll_idx].append(wb_col.shape[0])
  lc_counts_wb[ll_idx].append(np.count_nonzero(wb_col > 0))

# get CDFs and stats for them
cdf_per_ll = []
cdf_wb_per_ll = []
all_lc_counts = np.array([], dtype=np.float32)
all_lc_counts_wb = np.array([], dtype=np.float32)
for i_l in range(Nlengths):
  cur_lc_counts = np.asarray(lc_counts[i_l])
  cur_lc_counts_wb = np.asarray(lc_counts_wb[i_l])
  all_lc_counts = np.concatenate((all_lc_counts, cur_lc_counts))
  all_lc_counts_wb = np.concatenate((all_lc_counts_wb, cur_lc_counts_wb))
  cdf_per_ll.append(GetCDF(cur_lc_counts))
  cdf_wb_per_ll.append(GetCDF(cur_lc_counts_wb))
  PrintStats("STATS for LL in " + str(length_set[i_l]), \
             cur_lc_counts, cur_lc_counts_wb)

# full stuff
cdf = GetCDF(all_lc_counts)
cdf_wb = GetCDF(all_lc_counts_wb)
PrintStats("FULL STATS", all_lc_counts, all_lc_counts_wb)

# plot stuff
x = np.arange(max_count) + 1
GenPlot(x, cdf, cdf_per_ll, length_set,
        os.path.join(out_path, "lc_counts." + file_ending))
GenPlot(x, cdf_wb, cdf_wb_per_ll, length_set,
        os.path.join(out_path, "lc_counts_wb." + file_ending))
