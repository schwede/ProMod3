# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
This is a bunch of default functions for the coding style checker.
"""

import sys, os, difflib, subprocess

def FailMsg(msg, ecode):
    """
    Write a message to stderr and exit with the given status.

    :param msg: The message to be written to stderr.
    :type msg: :class:`str`
    :param ecode: The status code to be returned by the Python interpreter.
    :type ecode: :class:`int`
    """
    if msg.endswith('.'):
        emp = ''
    else:
        emp = '.'
    sys.stderr.write("%s%s\nAborting commit.\n" % (msg, emp))
    sys.exit(ecode)

def CheckLatestHookCode(script_file, source_loc,
                        script_name='pre-commit',
                        git_path='.git/hooks/pre-commit'):
    """
    Compare the source of the pre-commit script with the copy in .git/hooks in
    a diff-like manner. If the hook code is outdated, complain and kill the
    interpreter.

    :param script_file: The absolute path to the pre-commit hook used by
                        'git commit', usually the path to
                        '.git/hooks/pre-commit'.
    :type script_file: :class:`str`
    :param source_loc: Location of the source of the pre-commit hook. Relative
                       to the repository root.
    :type source_loc: :class:`str`
    :param script_name: Name for the script. Usually 'pre-commit'.
    :type script_name: :class:`str`
    :param git_path: Path to the hook inside the repo. Usually '.git/hooks'.
    :type git_path: :class:`str`
    """
    git_script = os.path.join(git_path, script_name)
    git_lines = open(script_file, 'U').readlines()
    repo_path = os.path.join(script_file[:-len(git_script)], source_loc)
    repo_lines = open(repo_path, 'U').readlines()
    tr_d = difflib.unified_diff(git_lines, repo_lines)
    tr_o = ''.join(tr_d)
    if len(tr_o):
        FailMsg("There are differences in '%s' and " % git_script+
                "'%s':\n\n" % source_loc +
                "%s\nPlease update your local copy" % tr_o, 5)

def CheckInstalled(tool):
    """
    Verify that we can find a certain tool. Exits the interpreter on failure.

    :param tool:  Tool to be checked.
    :type tool: :class:`str`
    """
    try:
        retval = subprocess.call("command -v %s" % tool,
                                 stdout=open(os.devnull, 'wb'),
                                 shell=True)
    except subprocess.CalledProcessError as err:
        FailMsg("Failed to run 'command -v %s': '%s'" % (tool,
                                                         str(err)), 16)
    if retval > 0:
        FailMsg("Programm '%s' not found." % tool, 17)

__all__ = ('FailMsg', 'CheckLatestHookCode', 'CheckInstalled')
