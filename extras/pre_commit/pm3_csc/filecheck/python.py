# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Checking Python code.
"""

import re, subprocess, os
from .. import pm3_csc
from . import base


class Python(base.FileCheck):
    """
    Checking Python code.
    """
    def __init__(self, filepath):
        base.FileCheck.__init__(self, filepath)

    def CheckPylint(self, outline, rcfile):
        """
        Translate Pylint messages to something more meaningful.

        :param outline: Single line of output from Pylint.
        :type outline: :class:`str`
        """
        msg = outline.split(':')
        if len(msg) != 2:
            pm3_csc.FailMsg("ERROR in parsing pylint output, message seems to"+
                            "contain more than one ':': '%s'" % outline, 15)
        # for the message translation, we provide tags %(line)s and %(file)s,
        # they will be filled if a message gets invoked. The original pylint
        # command runs with '--msg-template="{line}:{symbol}"' to reveal the
        # first bit of an entry.
        msg_dict = {
            'missing-docstring':
            "Line %(line)s: Documentation is missing. We want "+
            "documentation for source files, classes, function/ methods. Even "+
            "if it seems to be a bit much to also put a line at the beginning "+
            "of each source file, sometimes a file name is reused by "+
            "different modules and that way we can easily see where we are.",
            'unused-variable':
            "Line %(line)s: Variable not used. Those things just confuse "+
            "people trying to read your code. Please remove variables which "+
            "do nothing.",
            'bad-indentation':
            "Line %(line)s: Wrong indentation. Its best practice in modern "+
            "Python code, to use 4 white-spaces as indent. We go with that "+
            "rule.",
            'bad-whitespace':
            "Line %(line)s: Wrong no. of white spaces. To keep everything "+
            "smoothly readable, use white spaces like in natural language: "+
            "instead of 'foo=bar', do 'use = bar', also use a space after "+
            "','. Definitions serve as the exception: default values to "+
            "parameters go unspaced in PEP8, so its def 'Foo(bar=None)'.",
            'unused-import':
            "Line %(line)s: A module is imported but never used in the code. "+
            "This increases execution time and may create an unnecessary "+
            "dependency. Please remove the import.",
            'redefined-builtin':
            "Line %(line)s: Some function or variable name is defined which "+
            "already exists in Python. This may lead to funny effects and "+
            "needs to be avoided.",
            'bad-continuation':
            "Line %(line)s: A multi-line command is not aligned like Python "+
            "wants it. Usually things should all align along opening "+
            "parenthesis or similar landmarks. Sometimes this means to "+
            "introduce an awkward looking '=\\' in a function call.",
            'invalid-name':
            "Line %(line)s: Some variable/ function/ parameter does not "+
            "comply with naming conventions. E.g. variables usually need at "+
            "least three characters.",
            'undefined-variable':
            "Line %(line)s: Pylint found an undefined variable.",
            'unused-argument':
            "Line %(line)s: Argument defined which is not used by the "+
            "function/ method. This should be avoided since it may create "+
            "unnecessary dependencies inside the calling code.",
            'no-self-use':
            "Line %(line)s: This class method does not use 'self', its own "+
            "instance. Therefore it should be a '@staticmethod' or not a "+
            "member of this class at all."
        }
        if msg[1] not in list(msg_dict.keys()):
            pm3_csc.FailMsg("Found a pylint message for the first time: "+
                            "'%s'. You can get more information on " % outline +
                            "the kind of issue by running 'pylint "+
                            "--help-msg=%s'. Once this is resolved, " % msg[1] +
                            "you could extend '%s' with more " % __file__ +
                            "meaningful information. You can run pylint on "+
                            "this file by 'pylint --rcfile=%s " % rcfile +
                            "-r n %s'." % self.absfilepath, 16)
        subst_dict = {'line' : msg[0], 'file' : self.filepath}
        return msg_dict[msg[1]] % subst_dict

    def Check(self, ignore_line_width=False):
        # first check for forbidden code fragments
        print_re = re.compile(r'print\s')
        import_re = re.compile(r'import\s+\*')
        for line in self.GetLine(ignore_line_width):
            line = line.strip()
            m_line = re.match(r'([^#]*)', line)
            ex_line = m_line.group(1)
            if len(ex_line):
                if print_re.match(ex_line):
                    pm3_csc.FailMsg("'%s' contains a 'print' " % self.filepath +
                                    "statement. Use OST loggers instead.", 14)
                if import_re.match(ex_line):
                    pm3_csc.FailMsg("'%s' imports *. This clutters namespaces."\
                                    % self.filepath, 14)
        # run pylint
        # determine rc file for test/ non test stuff
        if re.match(r'.*tests/.*test_.*', self.filepath):
            pylintrc = os.path.join(os.path.split(__file__)[0],
                                    'pylint-unittest-rc')
        else:
            pylintrc = os.path.join(os.path.split(__file__)[0], 'pylintrc')
        job = subprocess.Popen(['pylint', '-r', 'n',
                                '--msg-template="{line}:{symbol}"',
                                '--rcfile', pylintrc,
                                self.filepath],
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        sout, serr = job.communicate()
        msg_stack = list()
        for line in sout.splitlines():
            if line.startswith('*************'):
                continue
            msg_stack.append(self.CheckPylint(line, pylintrc))
        for line in serr.splitlines():
            if line.startswith('*************'):
                continue
            msg_stack.append(self.CheckPylint(line, pylintrc))
        if len(msg_stack):
            pm3_csc.FailMsg(os.linesep + "Found issues:" + os.linesep +
                            os.linesep.join(msg_stack), 17)

__all__ = ('Python', )

#  LocalWords:  multi
