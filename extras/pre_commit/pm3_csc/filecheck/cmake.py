# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Checking CMake files.
"""

import re, os
from .. import pm3_csc
from . import base


class Cmake(base.FileCheck):
    """
    Checking CMake setups/ code.
    """
    def __init__(self, filepath):
        self._documented_macros = list()
        self.doc_file = "<NOT YET SET>"
        base.FileCheck.__init__(self, filepath)

    @property
    def documented_macros(self):
        '''
        Load documentation only on demand.
        '''
        if len(self._documented_macros):
            return self._documented_macros
        doc_path = os.path.split(__file__)[0]
        doc_path = os.path.join(doc_path, os.pardir, os.pardir, os.pardir,
                                os.pardir)
        doc_path = os.path.join(doc_path, 'cmake_support', 'doc', 'index.rst')
        doc_path = os.path.abspath(doc_path)
        self.doc_file = doc_path
        dfh = open(doc_path)
        for d_line in dfh:
            # we follow our own format very rigidly, here: commands are defined
            # by '.. cmake:command:: <command name>', with all the white spaces
            m_dc = re.match(r'\.\.\scmake:command::\s([^\s]+)', d_line)
            if m_dc:
                m_dcn = m_dc.group(1)
                self._documented_macros.append(m_dcn)
        dfh.close()
        return self._documented_macros

    def CheckClosingStatement(self, line):
        '''
        Check closing statement to carry identifier for the opening one.
        '''
        closers = ['endmacro']
        for close_stmnt in closers:
            fclose_stmnt = r'(?:^|\s)' + close_stmnt + r'\((.*)\)'
            m_stmnt = re.match(fclose_stmnt, line)
            if m_stmnt:
                stmnt_id = m_stmnt.group(1)
                if not len(stmnt_id):
                    pm3_csc.FailMsg("Line %d: " % self.current_line+
                                    "No identifier in closing statement. The "+
                                    "call to '%s()' should " % close_stmnt+
                                    "go by the same name as the statement "+
                                    "opening that block. That improves "+
                                    "readability plus future versions of "+
                                    "CMake will complain about this issue, "+
                                    "too", 15)

    def CheckWhitespace(self, line):
        '''
        Check that there is no white space between a function call and its
        opening parenthesis.
        '''
        openers = ['if', 'else', 'endif']
        for open_stmnt in openers:
            fopen_stmnt = r'(?:^|\s)' + open_stmnt + r'(\s*)\('
            m_stmnt = re.match(fopen_stmnt, line)
            if m_stmnt:
                stmnt_ws = m_stmnt.group(1)
                if len(stmnt_ws):
                    pm3_csc.FailMsg("Line %d: " % self.current_line+
                                    "White space(s) found between "+
                                    "call to '%s' and its " % open_stmnt+
                                    "opening parenthesis. Since in CMake "+
                                    "this is a function/ macro call, "+
                                    "omit the space", 16)

    def CheckFuncDoc(self, line):
        '''
        Check that functions/ macros have documentation in the correpsonding
        cmake/support/doc/index.rst.
        '''
        m_open = re.match(r'(?:macro|function)\(([^\s]*)\)', line)
        if m_open:
            m_name = m_open.group(1)
            if not len(m_name):
                pm3_csc.FailMsg("Line %d: Found a " % self.current_line+
                                "'nameless' function macro definition? "+
                                "This should not happen.", 17)
            if m_name not in self.documented_macros:
                pm3_csc.FailMsg("Line %d: Command '%s' " % (self.current_line,
                                                            m_name)+
                                "has no documentation in '%s'." % self.doc_file+
                                "If you just wrote that command, please write "+
                                "documentation. CMake code is rarely visited, "+
                                "so what exactly a command does is forgotten "+
                                "rather quickly. So do write down, WHAT the "+
                                "command does and HOW, the strategy behind it.",
                                18)

    def Check(self, ignore_line_width=False):
        #   for .cmake files: documentation of macros in index.rst
        for line in self.GetLine(ignore_line_width):
            line = line.strip()
            m_line = re.match(r'([^#]*)', line)
            ex_line = m_line.group(1)
            if len(ex_line):
                self.CheckClosingStatement(ex_line)
                self.CheckWhitespace(line)
                if self.filepath.endswith('.cmake'):
                    self.CheckFuncDoc(ex_line)

__all__ = ('Cmake', )
