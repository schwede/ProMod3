# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Generic class for checking code from files.
"""

import os
from .. import base

class FileCheck(object):
    """
    Meant to serve as parent for dedicated file check classes. The idea is to
    have one class per file type/ programming language.

    .. attribute:: filepath

    File to be checked with its path.

    :type: :class:`str`

    .. attribute:: current_line

    If in a Check(), this will tell you the current line number.

    :type: :class:`int`
    """
    def __init__(self, filepath):
        self.filepath = filepath
        self.absfilepath = os.path.abspath(filepath)
        self.current_line = None

    def Check(self, ignore_line_width=False):
        """
        This function will do the actual checks. Should be implemented by each
        class on its own.
        """
        raise NotImplementedError('Needs to be implemented in subclasses')

    def GetLine(self, ignore_line_width=False):
        """
        Fetch the next line from file. Also performs the line-length test. If
        a line is to long, the interpreter is terminated.
        """
        fh = open(self.filepath, "U")
        self.current_line = 1
        for line in fh:
            line = line.rstrip(os.linesep)
            if not ignore_line_width:
                if len(line) > 140:
                    base.FailMsg("Line width in '%s:%d' " % (self.filepath,
                                                             self.current_line)+
                                 " exceeds 140 characters. For better "+
                                 "readability of code, this is prohibited. "+
                                 "It allows to go beyond the historic 80 "+
                                 "characters border while 'ugly' line "+
                                 "wrapping only spans two lines with a "+
                                 "significant tail of 'empty' in the end.", 15)
            yield line
            self.current_line += 1
        fh.close()
__all__ = ('FileCheck', )
