# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Checking reStructuredText docmentation files.
"""

import re
from .. import pm3_csc
from . import base


class Rest(base.FileCheck):
    """
    Checking reStructuredText.
    """
    def __init__(self, filepath):
        base.FileCheck.__init__(self, filepath)

    def Check(self, ignore_line_width=False):
        re_head1 = re.compile(r"\s*(?:=+|-+)\s*")
        re_title1 = re.compile(r"(?:\:mod\:`.*`)?(.*)")

        # headlines start with all upercase but articles
        last_line = ''
        for line in self.GetLine(ignore_line_width):
            if re_head1.match(line) and len(last_line):
                ma_title = re_title1.match(last_line)
                if ma_title:
                    wl_title = ma_title.group(1).replace("(", " ")
                    wl_title = wl_title.replace(")", " ")
                    wl_title = wl_title.split()
                    for wrd in wl_title:
                        if wrd not in ['-', 'a', 'an', 'the'] and wrd[0] != '|':
                            if re.match(r':.+:`.+`', wrd):
                                continue
                            if not wrd[0].isupper():
                                pm3_csc.FailMsg("Line %d"%(self.current_line-1)+
                                                " seems to be a headline "+
                                                "which is not 'all "+
                                                "uppercase'. Only articles "+
                                                "'a, an, the' are allowed "+
                                                "lowercase, even 'by, for, "+
                                                "of, to, ...' are written "+
                                                "uppercase since those are "+
                                                "prepositions like 'around, "+
                                                "under'. Look for '%s'" % wrd,
                                                16)
            last_line = line

__all__ = ('Rest', )
