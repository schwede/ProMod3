# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Checking just text.
"""

from . import base


class Text(base.FileCheck):
    """
    Checking text files.
    """
    def __init__(self, filepath):
        base.FileCheck.__init__(self, filepath)

    def Check(self, ignore_line_width=False):
        for line in self.GetLine(ignore_line_width):
            # pylint: disable=unused-variable
            lll = line

__all__ = ('Text', )
