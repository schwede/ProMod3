# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Keep all the functions calling git in one place.
"""

import subprocess, re, os
from . import base

def CheckVersion(minversion):
    """
    Verify the version of Git used. Exits the interpreter if outdated.

    :param minversion: Minimum version number required like '1.7.5.1'.
    :type minversion: :class:`str`
    """
    try:
        gvo = subprocess.check_output(['git', '--version'], shell=False)
    except subprocess.CalledProcessError as err:
        base.FailMsg("Failed to run 'git --version': '%s'" % str(err), 1)
    gvs = re.match(r"git version ((:?\d+\.?)+)", gvo)
    if not gvs:
        base.FailMsg("Failed to fetch Git version from string: '%s'" % \
                     gvo.strip(), 2)
    gva = gvs.group(1).split('.')
    gmva = minversion.split('.')
    gvl = len(gva)
    if gvl > len(gmva):
        gvl = len(gmva)
    for i in range(0, gvl):
        if gva[i] < gmva[i]:
            base.FailMsg("Git version outdated, is '%s', " % '.'.join(gva) +
                         "should be at least '%s'" % '.'.join(gmva), 3)
        elif gva[i] > gmva[i]:
            break

def CheckWhitespaces(exclude):
    """
    In changed files, check for trailing whitespaces by git. Exits the
    interpreter if trailing whitespaces are found.

    :param exclude: List of regular expressions describing files to
                               be excluded from being checked.
    :type exclude: :class:`list` of :class:`str`
    """
    try:
        retval = subprocess.call(['git', 'rev-parse', '--verify', 'HEAD'],
                                 stdout=open(os.devnull, 'wb'),
                                 shell=False)
    except subprocess.CalledProcessError as err:
        base.FailMsg("Failed to run 'git rev-parse --verify HEAD'': "+
                     "'%s'" % str(err), 6)
    if not retval: # shell returns 0 as True!
        try:
            di_o = subprocess.check_output(['git', 'diff-index', '-M',
                                            '--cached', '--check', 'HEAD'],
                                           shell=False)
        except subprocess.CalledProcessError as err:
            if err.returncode == 2:
                di_o = err.output
            else:
                base.FailMsg("Failed to run 'git diff-index -M --cached "+
                             "--check HEAD': '%s'" % str(err), 7)
        if len(di_o):
            # we have trailing whitespaces, compile exclude list into regexps
            # and prepare output
            re_excludes = list()
            for ex in exclude:
                re_excludes.append(re.compile(ex))
            output = list()
            for line in di_o.splitlines():
                skip = False
                for ex_re in re_excludes:
                    if re.match(ex_re, line):
                        skip = True
                        break
                if skip:
                    continue
                if line.endswith('trailing whitespace.'):
                    output.append('  %s' % line)
            if len(output):
                base.FailMsg("Found trailing whitespace(s):"+
                             "\n%s" % '\n'.join(output), 8)

def _GetFileType(filepath):
    """
    Determine file formats. The easy way is to go by extension, if this does
    not help, we run 'file' on the file and interpret the returned text. 'file'
    is an elegant idea but does not always work, its implemented via a vast
    amount of conditionals which may drop out of the wrong branch from time to
    time... that way text files containing English language are sometimes marked
    as 'Pascal'. If we cannot determine the format, exits the interpreter.

    :param filepath: The file to be checked with its path component.
    :type filepath: :class:`str`
    """
    known_extensions = {'.py' : 'python',
                        '.py.in' : 'python',
                        '.rst' : 'rest',
                        'CMakeLists.txt' : 'cmake',
                        '.cmake' : 'cmake',
                        '.pdb' : 'ukn',
                        '.pdb.gz': 'ukn',
                        '.fasta' : 'ukn',
                        '.fas' : 'ukn'}
    for ext in list(known_extensions.keys()):
        if filepath.endswith(ext):
            return known_extensions[ext]
    try:
        fout = subprocess.check_output(['file', filepath], shell=False)
    except subprocess.CalledProcessError as err:
        base.FailMsg("Failed to run 'file %s': '%s'" % (filepath, str(err)), 11)
    fout = fout.strip()
    known_file_types = {"%s: a python script text executable" :
                        "python",
                        "%s: a /usr/bin/env python script text executable" :
                        "python",
                        "%s: a ost script text executable" :
                        "python",
                        "%s: a /usr/bin/env ost script text executable" :
                        "python",
                        "%s: Bourne shell script text executable" :
                        "shell-script",
                        "%s: Bourne-Again shell script text executable" :
                        "shell-script",
                        "%s: POSIX shell script text executable" :
                        "shell-script",
                        "%s: ASCII C program text" :
                        "C",
                        "%s: SQLite 3.x database" :
                        "ukn",
                        "%s: ASCII English text" :
                        "text"}
    for file_resp in list(known_file_types.keys()):
        if fout == file_resp % filepath:
            return known_file_types[file_resp]
    base.FailMsg("Could not determine file type of '%s'. " % filepath +
                 "Please go to 'extras/pre_commit/pm3_csc/git.py' and extend " +
                 "'_GetFileType'. Output of 'file %s' was '%s'" % (filepath,
                                                                   fout), 12)

def GetModifiedFiles(exclude, committed_only=True):
    """
    Get the list of modified files in a commit. Files are sorted into a
    dictionary of lists according to file type. If a file of unknown type is
    found, the interpreter will exit with a message. To fetch the list of
    files, Git is used.

    :param exclude: List of regular expressions describing files to
                               be excluded from being checked.
    :type exclude: :class:`list` of :class:`str`

    :param committed_only: Only return files marked for comit.
    :type committed_only: :class:`bool`

    :returns: :class:`dict` of :class:`list` of :class:`str`, with file types
    as keys and file names as items in the lists.
    """
    file_lists = dict()
    file_lists['python'] = list()
    file_lists['rest'] = list()
    file_lists['cmake'] = list()
    file_lists['shell-script'] = list()
    file_lists['C'] = list()
    file_lists['ukn'] = list()
    file_lists['text'] = list()
    re_excludes = list()
    # get path to repo root
    try:
        cdup = subprocess.check_output(['git', 'rev-parse', '--show-cdup'],
                                       shell=False)
    except subprocess.CalledProcessError as err:
        base.FailMsg("Failed to run 'git rev-parse --show-cdup': "+
                     "'%s'" % str(err), 10)
    cdup = cdup.strip()
    # fetch modified/ added files
    try:
        gso = subprocess.check_output(['git', 'status', '--porcelain'],
                                      shell=False)
    except subprocess.CalledProcessError as err:
        base.FailMsg("Failed to run 'git status --porcelain': '%s'" % str(err),
                     9)
    # prepare exclude list
    for ex in exclude:
        re_excludes.append(re.compile(ex))
    # sort into file type-lists
    for line in gso.splitlines():
        status_line = line.split()
        if status_line[0][0] == 'D':
            # file was deleted, skip from checks
            continue
        if status_line[0] == '??':
            # file is not tracked by repo, ignore
            continue
        if committed_only:
            if line[0] == ' ':
                # Files marked for commit should have 'M'/ 'A' in the first
                # column. If its left empty, skip
                continue
        tfp = os.path.join(cdup, status_line[-1])
        skip = False
        for ex_re in re_excludes:
            if re.match(ex_re, tfp):
                skip = True
                break
        if skip:
            continue
        ftype = _GetFileType(tfp)
        if ftype in list(file_lists.keys()):
            file_lists[ftype].append(tfp)
        else:
            base.FailMsg("Not supposed to be ever seen. There must be a file "+
                         "type recognised by the pre-commit hook which is not "+
                         "handled.", 13)
    return file_lists


__all__ = ('CheckVersion', 'CheckWhitespaces', 'GetModifiedFiles', )
