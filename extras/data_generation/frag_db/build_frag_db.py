# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from promod3 import loop


#This script loads the default StructureDB and generates
#FragDB on top of it...

max_pairwise_rmsd = 1.0
dist_bin_size = 1.0
angle_bin_size = 20

fragdb = loop.FragDB(dist_bin_size,angle_bin_size)
structure_db = loop.LoadStructureDB()

for i in range(3,15):
  print("start to add fragments of length ",i)
  fragdb.AddFragments(i,max_pairwise_rmsd,structure_db)

fragdb.PrintStatistics()
fragdb.SavePortable('frag_db.dat')

