import os
import tarfile
import gzip
import argparse
import pickle

from ost import io
from promod3.modelling import FSStructureServer

def parse_args():
    desc = ("Extracts sequences from pickled files generated with "
            "create_fsserver_data_chunks.py and relates them to entries in an "
            "FSStructureServer object. I.e. Dumps a sequence list in Fasta "
            "format with entry indices as sequence names. This file serves as "
            "starting point for an actual PentaMatch object. Requires faf.py "
            "to be importable.") 

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("--data_chunks", help="Directory with all data chunks "
                        "generated with create_fsserver_data_chunks.py")
    parser.add_argument("--fs_server", help="Directory containing the files "
                        "for an FSStructureServer object derived from "
                        "data_chunks directory")
    parser.add_argument("--out", help="Outfile in Fasta format that will "
                        "contain all sequences with their respective index in "
                        "the underlying FSStructureServer object as names")
    return parser.parse_args()


def main():
    args = parse_args()
    fs_server = FSStructureServer(args.fs_server)
    chunk_files = os.listdir(args.data_chunks)
    outfile = open(args.out, 'w')

    for cf_idx, cf in enumerate(chunk_files):
        print(f"processing chunk {cf_idx}, {cf}")
        with open(os.path.join(args.data_chunks, cf), 'rb') as fh:
            data = pickle.load(fh)
        for entry in data:
            uniprot_ac = entry[0]
            fragment = entry[1]
            version = entry[2]
            omf_data = entry[3]
            idx = fs_server.GetIdx(uniprot_ac, fragment = fragment,
                                   version = version)
            omf = io.OMF.FromBytes(gzip.decompress(omf_data))
            assert(omf.GetChainNames() == ["A"])
            sequence = omf.GetSequence("A")
            outfile.write(f">{idx}\n")
            seq_chunks = [sequence[i:i+80] for i in range(0, len(sequence), 80)]
            outfile.write('\n'.join(seq_chunks))
            outfile.write('\n')
    outfile.close()

if __name__ == '__main__':
    main()

