import os

# path to Singularity container with recent OpenStructure installation
omf_sif = "<PATH_TO_SIF>"

# path to directory that contains AFDB - one tarball per proteom
afdb_dir = "/scicore/data/managed/AF_UniProt/frozen_221115T101000/proteomes"

# file with one proteom filename per line - in principle the content of os.listdir(afdb_dir)
proteom_file = "<WHEREVER>/afdb_proteom_files.txt"

# number of proteoms that end up in one data chunk
chunk_size = 100

# path to directory where chunks will be dumped
chunk_out_dir = "<WHEREVER>/afdb_data_chunks"

# path to script that does the job
fancy_script = "<WHEREVER>/afdb_proteom_to_data_chunks.py"

# whether structures should be reduced to CA only representation
ca_only = False

# path to file into which the commands are written
cmd_file = "allatom_commands.cmd"


with open(proteom_file, 'r') as fh:
  proteom_data = fh.readlines()
N = len(proteom_data)

commands = list()
chunk_idx = 0
for i in range(0, N, chunk_size):
    start = i
    end = min(N, i+chunk_size)
    out_file = os.path.join(chunk_out_dir, str(chunk_idx) + ".pkl")
    cmd = ["singularity", "exec", "--bind", f"{afdb_dir}:{afdb_dir}",
           omf_sif, "ost", fancy_script,
           "--afdb_dir", afdb_dir,
           "--tarballs", proteom_file,
           "--start", str(start),
           "--end", str(end),
           "--out_file", out_file]
    if ca_only:
        cmd.append("--ca_only")
    commands.append(' '.join(cmd))
    chunk_idx += 1

with open(cmd_file, 'w') as fh:
    fh.write('\n'.join(commands))

