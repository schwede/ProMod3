import os
import tarfile
import gzip
import argparse
import pickle

from ost import io

def parse_args():
    desc = ("Parses proteom tarballs from AFDB and dumps pickle files ready "
            "for database creation") 

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("--afdb_dir", help="Directory containing the "
                        "tarballs")
    parser.add_argument("--tarballs", help=" File containing tarball filenames")
    parser.add_argument("--start", help="idx in that file to start", type=int)
    parser.add_argument("--end", help="idx in that file to end", type=int)
    parser.add_argument("--out_file", help="Output file where stuff gets "
                        "pickled")
    parser.add_argument("--ca_only", help="Whether to use CA-only "
                        "representation", action="store_true")
    return parser.parse_args()

def iterate_proteom(tarball):
    """ Helper: iterate over proteome tarball provided by EBI-AFDB

    Yields mmcif data
    in detail: tuple with types :class:`str`, :class:`ost.mol.EntityHandle`,
    :class:`ost.io.MMFicInfo` and :class:`ost.seq.SequenceList`

    :param tarball: Path to tarball to iterate
    :type tarball: :class:`str`
    :returns: Generator of specified tuple
    """
    tf = tarfile.open(tarball)
    prof = io.profiles.Get("STRICT")

    # hack to correct for corrupt cif files in AFDB
    corrupt_cit = "_citation.id                      primary"
    correct_cit = "_citation.id                      1"
    while True:
        tar_info = tf.next()
        if tar_info is None:
            break # we're done
        if tar_info.name.endswith("cif.gz"):
            fh = tf.extractfile(tar_info)
            mmcif_str = gzip.decompress(fh.read()).decode("utf-8")
            if corrupt_cit in mmcif_str:
                mmcif_str = mmcif_str.replace(corrupt_cit, correct_cit)
            ent, mmcif_info, seqres = io.MMCifStrToEntity(mmcif_str,
                                                          profile=prof,
                                                          process=True)
            yield (tar_info.name, ent, mmcif_info, seqres)

def process_proteom(tarball, ca_only=False, omf_max_error=0.0):
    """ Helper: Creates list of entries for one proteom

    :param tarball: Path to proteom tarball
    :type tarball: :class:`str`
    :ca_only: Whether to select for only CA positions
    :type ca_only: :class:`bool`
    :returns: :class: list of :class:`tuple` in format required by
              :func:`FromDataChunks`
    """
    omf_opts = io.OMFOption.DEFAULT_PEPLIB | \
               io.OMFOption.AVG_BFACTORS | io.OMFOption.ROUND_BFACTORS | \
               io.OMFOption.SKIP_SS | io.OMFOption.INFER_PEP_BONDS

    entries = list()
    for entry in iterate_proteom(tarball):
        uniprot_ac = entry[0].split('-')[1]
        fragment = entry[0].split('-')[2]
        version = entry[0].split('-')[3]
        version = version.split('_')[1]
        version = version.split('.')[0]
        ent = entry[1]
        if ca_only:
            ent = ent.Select("aname=CA")
            ent = mol.CreateEntityFromView(ent, False)
        ent_name = f"{uniprot_ac} {fragment} {version}"
        ent.SetName(ent_name)
        omf = io.OMF.FromEntity(ent, max_error=omf_max_error, options=omf_opts)
        omf = gzip.compress(omf.ToBytes())
        entries.append([uniprot_ac, fragment, version, omf])
    return entries

def create_data_chunk(tarballs, afdb_dir, start, end, chunk_out,
                      ca_only=False):
    """ Create full data chunk

    Requires preprocessing step of creating a text file that lists all
    proteom tarballs.

    :param tarballs: Path to file where each line corresponds to one proteom
                     tarball, i.e. its filename
    :type tarballs: :class:`str`
    :param afdb_dir: Directory containing the tarball files specified in
                     *tarballs*
    :type afdb_dir: :class:`str`
    :param start: Start of tarball slice to be added to chunk 
    :type start: :class:`int`
    :param end: End of tarball slice to be added to chunk (*end* won't be
                added...)
    :type end: :class:`str`
    :param chunk_out: Chunk will be pickled to that file
    :type chunk_out: :class:`str`
    :ca_only: Whether to select for only CA positions
    :type ca_only: :class:`bool`
    """
    with open(tarballs, 'r') as fh:
        tarballs = fh.readlines()
    entries = list()
    for tarball in tarballs[start:end]:
        full_path = os.path.join(afdb_dir, tarball.strip())
        entries += process_proteom(full_path, ca_only=ca_only)
    with open(chunk_out, 'wb') as fh:
        pickle.dump(entries, fh)

def main():
    args = parse_args()
    create_data_chunk(args.tarballs, args.afdb_dir, args.start, args.end,
                      args.out_file, ca_only = args.ca_only)

if __name__ == '__main__':
    main()
