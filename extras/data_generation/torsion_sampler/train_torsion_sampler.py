# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from promod3 import loop
from ost import mol
import os
import pickle
import traceback

# Tested group definitions are:
#   1. simple_group_definitions.txt
#   2. solis_group_definitions.txt
#
# The first group definition file is taken from:
#
# R. J. Read, P. D. Adams, W. B. Arendall, A. T. Brunger, P. Em-
# sley, R. P. Joosten, G. J. Kleywegt, E. B. Krissinel, T. Lutteke,
# Z. Otwinowski, A. Perrakis, J. S. Richardson, W. H. Sheffler, J. L.
# Smith, I. J. Tickle, G. Vriend, and P. H. Zwart. A new genera-
# tion of crystallographic validation tools for the protein data bank.
# Structure, Oct 2011.
#
# The latter from:
#
# A. D. Solis and S. Rackovsky. Improvement of statistical poten-
# tials and threading score functions using information maximiza-
# tion. Proteins, Mar 2006.
#
#
# The file containing the prot data has been pulled from:
# http://dunbrack.fccc.edu/PISCES.php
#
# To finally train the torsion samplers using this information
# you have to  get the pdb structure files from somewhere and dump
# them into the structure_dir defined below.
# Their name must be in the format pdb_id + chain_id
# e.g.   1AKEA.pdb

group_definition_file = "solis_group_definitions.txt"
structure_dir = "/home/schdaude/workspace/promod_test/structures"
prot_data = open('cullpdb_pc50_res2.5_R1.0_d140401_chains19001','r').readlines()


group_definitions = list()
group_data = open(group_definition_file,'r')
for line in group_data:
  group_definitions.append(line.strip())

# initialize one sampler being trained with all data
# and three secondary structure dependent samplers
sampler = loop.TorsionSampler(group_definitions, 72,0)
coil_sampler = loop.TorsionSampler(group_definitions, 72,0)
helix_sampler = loop.TorsionSampler(group_definitions,72,0)
sheet_sampler = loop.TorsionSampler(group_definitions,72,0)


# Internally, the TorsionHandles get used to extract the
# phi/psi dihedrals. We have to make sure, that they're really
# assigned when loading a structure
proc = conop.HeuristicProcessor(assign_torsions=True)
prof = io.IOProfile(dialect='PDB', fault_tolerant=False,
                    quack_mode=False, processor=proc)

for i,line in enumerate(prot_data[1:]):
    prot_id = line[:4]
    chain_id = line[4]
    try:
        print('processing: ',prot_id,' on line: ',i)
        prot = io.LoadPDB(os.path.join(structure_dir,prot_id+chain_id+'.pdb'), 
                          profile=prof).Select('peptide=true and cname='+chain_id)
        mol.alg.AssignSecStruct(prot)
        for r in prot.residues:
            if r.GetSecStructure().IsHelical():
                r.SetIntProp('ss',0)
                continue
            if r.GetSecStructure().IsExtended():
                r.SetIntProp('ss',1)
                continue
            r.SetIntProp('ss',2)
        helix_selection = prot.Select('grss=0')
        extended_selection = prot.Select('grss=1')
        coil_selection = prot.Select('grss=2')
        try:
            sampler.ExtractStatistics(prot)
            helix_sampler.ExtractStatistics(helix_selection)
            coil_sampler.ExtractStatistics(coil_selection)
            sheet_sampler.ExtractStatistics(extended_selection)
        except:
            traceback.print_exc()
    except:
        traceback.print_exc()

sampler.Save('torsion_sampler.dat')
helix_sampler.Save('torsion_sampler_helical.dat')
sheet_sampler.Save('torsion_sampler_extended.dat')
coil_sampler.Save('torsion_sampler_coil.dat')
