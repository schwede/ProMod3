# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import ost
from qmean import *
from promod3 import scoring, loop
import numpy as np

"""
requires QMEAN to be in place and the PYTHONPATH set accordingly

as default it just loads the provided potentials from this directory,
has to be adjusted accordingly for different needs
"""

# convert AminoAcidAtom to 
def AAA2CT(aaa):
  return GetAtomTypeByName(loop.AminoAcidLookup.GetAA(aaa),
                           loop.AminoAcidLookup.GetAtomName(aaa))

# INIT
int_pot = InteractionPotential.Load("interaction_pot.dat")
int_opts = int_pot.GetOptions()
packing_pot = PackingPotential.Load("packing_pot.dat")
packing_opts = packing_pot.GetOptions()

print("InteractionPotential", int_opts.upper_cutoff, int_opts.number_of_bins, int_opts.sequence_sep)
print("PackingPotential", packing_opts.cutoff, packing_opts.max_counts)

# check whether the provided potentials are parametrized in a way PM can use

# check InteractionPotential
if int_opts.lower_cutoff != 0.0:
  raise ValueError("Require lower cutoff in InteractionPotential to be zero")
if int_opts.number_of_bins < 1:
  raise ValueError("Number of dist bins in InteractionPotential must be >= 1")
if int_opts.sequence_sep < 1:
  raise ValueError("Sequence separation in InteractionPotential must be >= 1")

# SETUP AllAtomInteractionScorer
aa_scorer = scoring.AllAtomInteractionScorer(int_opts.upper_cutoff, int_opts.number_of_bins, int_opts.sequence_sep)
# fill AllAtomInteractionScorer energies
aa_dist_bin_size = int_opts.upper_cutoff / int_opts.number_of_bins
for i in range(loop.AminoAcidAtom.XXX_NUM_ATOMS):
  aaa_one = loop.AminoAcidAtom(i)
  ct_one = AAA2CT(aaa_one)
  for j in range(loop.AminoAcidAtom.XXX_NUM_ATOMS):
    aaa_two = loop.AminoAcidAtom(j)
    ct_two = AAA2CT(aaa_two)
    dist = 0.001
    for k in range(int_opts.number_of_bins):
      e = int_pot.GetEnergy(ct_one, ct_two, dist)
      aa_scorer.SetEnergy(aaa_one, aaa_two, k, e)
      dist += aa_dist_bin_size
# save (portable one to be moved)
aa_scorer.SavePortable("portable_aa_scorer.dat")
aa_scorer.Save("aa_scorer.dat")

# SETUP AllAtomPackingScorer
aa_packing_scorer = scoring.AllAtomPackingScorer(packing_opts.cutoff, packing_opts.max_counts)
# fill AllAtomPackingScorer energies
for i in range(loop.AminoAcidAtom.XXX_NUM_ATOMS):
  aaa = loop.AminoAcidAtom(i)
  ct = AAA2CT(aaa)
  for j in range(packing_opts.max_counts+1):
    e = packing_pot.GetEnergy(ct, j)
    aa_packing_scorer.SetEnergy(aaa, j, e)
# save (portable one to be moved)
aa_packing_scorer.SavePortable("portable_aa_packing_scorer.dat")
aa_packing_scorer.Save("aa_packing_scorer.dat")
