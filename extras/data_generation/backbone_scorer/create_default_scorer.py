# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import ost
from qmean import *
from promod3 import scoring
import numpy as np

"""
requires QMEAN to be in place and the PYTHONPATH set accordingly

as default it just loads the provided potentials from this directory,
has to be adjusted accordingly for different needs
"""

cb_packing_pot = CBPackingPotential.Load("cb_packing_pot.dat")
cb_pot = CBetaPotential.Load("cbeta_pot.dat")
reduced_pot = ReducedPotential.Load("reduced_pot.dat")
torsion_pot = TorsionPotential.Load("torsion_pot.dat")
hbond_pot = HBondPotential.Load("hbond_pot.dat")

cb_packing_opts = cb_packing_pot.GetOptions()
cb_opts = cb_pot.GetOptions()
reduced_opts = reduced_pot.GetOptions()
torsion_opts = torsion_pot.GetOptions()
hbond_opts = hbond_pot.GetOptions()

torsion_group_identifiers = list()
for item in torsion_opts.group_identifier:
  torsion_group_identifiers.append(item)

# check whether the provided potentials are parametrized in a way 
# promod can use them

# check torsion
if torsion_opts.number_of_bins[0] != 1:
  raise ValueError("Require torsion potential only looking at central phi/psi angles!")
if torsion_opts.number_of_bins[1] != 1:
  raise ValueError("Require torsion potential only looking at central phi/psi angles!")
if torsion_opts.number_of_bins[4] != 1:
  raise ValueError("Require torsion potential only looking at central phi/psi angles!")
if torsion_opts.number_of_bins[5] != 1:
  raise ValueError("Require torsion potential only looking at central phi/psi angles!")
if torsion_opts.number_of_bins[2] != torsion_opts.number_of_bins[3]:
  raise ValueError("Require same amount of phi and psi bin for central amino acid")


# check cbeta interaction
if cb_opts.lower_cutoff != 0.0:
  raise ValueError("Require lower cutoff in cbeta potential to be zero in cbeta potential")

if cb_opts.number_of_bins < 1:
  raise ValueError("Number of dist bins in cbeta potential must be >= 1")

if cb_opts.sequence_sep < 1:
  raise ValueError("Sequence separation in cbeta potential must be >= 1")

# check reduced
if reduced_opts.lower_cutoff != 0.0:
  raise ValueError("Require lower cutoff in reduced potential to be zero in reduced potential")

if reduced_opts.num_dist_bins < 1:
  raise ValueError("Number of dist bins in reduced potential must be >= 1")

if reduced_opts.num_angle_bins < 1:
  raise ValueError("Number of angle bins in reduced potential must be >= 1")

if reduced_opts.num_dihedral_bins < 1:
  raise ValueError("Number of dihedral bins in reduced potential must be >= 1")

if reduced_opts.sequence_sep < 1:
  raise ValueError("Sequence separation in reduced potential must be >= 1")
 
 # check hbond
if(hbond_opts.seq_sep != 1):
  raise ValueError("Expect sequence separation to be 1 in hbond potential!")

# SETUP CBPackingScorer
cb_packing_scorer = scoring.CBPackingScorer(cb_packing_opts.cutoff, cb_packing_opts.max_counts)
# fill cb packing potential
for i in range(ost.conop.XXX):
  aa = ost.conop.AminoAcid(i)
  for j in range(cb_packing_opts.max_counts+1):
    e = cb_packing_pot.GetEnergy(aa,j)
    cb_packing_scorer.SetEnergy(aa,j,e)

cb_packing_scorer.SavePortable("portable_cb_packing_scorer.dat")
cb_packing_scorer.Save("cb_packing_scorer.dat")

# SETUP CBetaScorer
cbeta_scorer = scoring.CBetaScorer(cb_opts.upper_cutoff, cb_opts.number_of_bins, cb_opts.sequence_sep)
# fill CBetaScorer potential
cbeta_bin_size = cb_opts.upper_cutoff / cb_opts.number_of_bins
for i in range(ost.conop.XXX):
  aa_one = ost.conop.AminoAcid(i)
  for j in range(ost.conop.XXX):
    aa_two = ost.conop.AminoAcid(j)
    d = 0.001
    for actual_bin in range(cb_opts.number_of_bins):
      e = cb_pot.GetEnergy(aa_one,aa_two,d)
      cbeta_scorer.SetEnergy(aa_one, aa_two, actual_bin, e)
      d += cbeta_bin_size
# save (portable one to be moved)
cbeta_scorer.SavePortable("portable_cbeta_scorer.dat")
cbeta_scorer.Save("cbeta_scorer.dat")


# SETUP ReducedScorer
reduced_scorer = scoring.ReducedScorer(reduced_opts.upper_cutoff, reduced_opts.num_dist_bins, reduced_opts.num_angle_bins, reduced_opts.num_dihedral_bins, reduced_opts.sequence_sep)
# fill ReducedScorer potential
reduced_dist_bin_size = reduced_opts.upper_cutoff / reduced_opts.num_dist_bins
reduced_angle_bin_size = np.pi / reduced_opts.num_angle_bins
reduced_dihedral_bin_size = 2 * np.pi / reduced_opts.num_dihedral_bins
for i in range(ost.conop.XXX):
  aa_one = ost.conop.AminoAcid(i)
  for j in range(ost.conop.XXX):
    aa_two = ost.conop.AminoAcid(j)
    dist = 0.001
    for k in range(reduced_opts.num_dist_bins):
      alpha = 0.001
      for l in range(reduced_opts.num_angle_bins):
        beta = 0.001
        for m in range(reduced_opts.num_angle_bins):
          gamma = -np.pi + 0.001
          for n in range(reduced_opts.num_dihedral_bins):
            e = reduced_pot.GetEnergy(aa_one, aa_two, dist, alpha, beta, gamma)
            reduced_scorer.SetEnergy(aa_one, aa_two, k, l, m, n, e)
            gamma += reduced_dihedral_bin_size
          beta += reduced_angle_bin_size
        alpha += reduced_angle_bin_size
      dist += reduced_dist_bin_size
# save (portable one to be moved)
reduced_scorer.SavePortable("portable_reduced_scorer.dat")
reduced_scorer.Save("reduced_scorer.dat")


# SETUP HBondScorer
hbond_scorer = scoring.HBondScorer(hbond_opts.d_min, hbond_opts.d_max,
                                   hbond_opts.alpha_min, hbond_opts.alpha_max,
                                   hbond_opts.beta_min, hbond_opts.beta_max,
                                   hbond_opts.gamma_min, hbond_opts.gamma_max,
                                   hbond_opts.d_bins, hbond_opts.alpha_bins,
                                   hbond_opts.beta_bins, hbond_opts.gamma_bins)
# fill hbond potential
d_bin_size = (hbond_opts.d_max - hbond_opts.d_min) / hbond_opts.d_bins
alpha_bin_size = (hbond_opts.alpha_max - hbond_opts.alpha_min) / hbond_opts.alpha_bins
beta_bin_size = (hbond_opts.beta_max - hbond_opts.beta_min) / hbond_opts.beta_bins
gamma_bin_size = (hbond_opts.gamma_max - hbond_opts.gamma_min) / hbond_opts.gamma_bins

for i in range(3):
  d = hbond_opts.d_min + 0.001
  for j in range(hbond_opts.d_bins):
    alpha = hbond_opts.alpha_min + 0.001
    for k in range(hbond_opts.alpha_bins):
      beta = hbond_opts.beta_min + 0.001
      for l in range(hbond_opts.beta_bins):
        gamma = hbond_opts.gamma_min + 0.001
        for m in range(hbond_opts.gamma_bins):
          energy = hbond_pot.GetEnergy(i,d,alpha,beta,gamma)
          hbond_scorer.SetEnergy(i,j,k,l,m,energy)
          gamma += gamma_bin_size
        beta += beta_bin_size
      alpha += alpha_bin_size
    d += d_bin_size
hbond_scorer.SavePortable("portable_hbond_scorer.dat")
hbond_scorer.Save("hbond_scorer.dat")


# SETUP SSAgreementScorer
ss_agreement_scorer = scoring.SSAgreementScorer()
# fill ss_agreement
psipred_states = ['H','E','C']
psipred_confidences = [0,1,2,3,4,5,6,7,8,9]
dssp_states = ['H','E','C','G','B','S','T','I']

qmean_ss_agreement = SSAgreement()

for p_s in psipred_states:
  for p_c in psipred_confidences:
    for d_s in dssp_states:
      score = qmean_ss_agreement.LogOddScore(d_s,p_s,p_c)
      ss_agreement_scorer.SetScore(p_s,p_c,d_s,score)
ss_agreement_scorer.SavePortable("portable_ss_agreement_scorer.dat")
ss_agreement_scorer.Save("ss_agreement_scorer.dat")


# SETUP TorsionScorer
torsion_scorer = scoring.TorsionScorer(torsion_group_identifiers, torsion_opts.number_of_bins[3])

torsion_angles = list()
for i in range(6):
  torsion_angles.append(0.0)

torsion_bin_size = 2*np.pi/torsion_opts.number_of_bins[2]

for i, group_id in enumerate(torsion_group_identifiers):
  phi = -np.pi+0.001
  for j in range(torsion_opts.number_of_bins[2]):
    psi = -np.pi+0.001
    for k in range(torsion_opts.number_of_bins[3]):
      torsion_angles[2] = phi
      torsion_angles[3] = psi
      energy = torsion_pot.GetEnergy(group_id,torsion_angles)
      torsion_scorer.SetEnergy(i,j,k,energy)
      psi += torsion_bin_size
    phi += torsion_bin_size

torsion_scorer.SavePortable("portable_torsion_scorer.dat")
torsion_scorer.Save("torsion_scorer.dat")
