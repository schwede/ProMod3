# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import time
import os
import pickle
import traceback
from sm import smtl

# This is how we extract all required data for a StructureDB from the 
# SWISS-MODEL template library given a non redundant set of proteins as
# pulled from Pisces: http://dunbrack.fccc.edu/PISCES.php. 
# If you have no working SWISS-MODEL installation you have two possibilities:
#
# 1. Ping someone from the mighty SCHWEDE-Lab to run this script for you 
#    with your desired PISCES file
#
# 2. Fetch the data manually from the PDB and obtain the profiles with 
#    the hh-suite available at: https://github.com/soedinglab/hh-suite.

pisces_path = "cullpdb_pc20_res2.0_R0.25_d180524_chains6670"
smtl_path = "PATH_TO_SMTL" # TO BE SET

profile_out_dir = "hmms"
structure_out_dir = "structures"

tpl_lib = smtl.SMTL(smtl_path)
pisces_data = open(pisces_path).readlines()

# We have to do some mapping magic, since the chain names of SMTL and pdb
# do not match. The PISCES data however specifies the pdb names
# in here we store pdb id and chain name relative to the SMTL
fetched_structures = list()

if not os.path.exists(profile_out_dir):
  os.makedirs(profile_out_dir)
if not os.path.exists(structure_out_dir):
  os.makedirs(structure_out_dir)

for i,line in enumerate(pisces_data[1:]):

  prot_id = line[:4]
  chain_id = line[4]
  print("processing ", prot_id, chain_id)

  try:
    bus = tpl_lib.GetAll(prot_id.upper())
  except:
    print("could not find entry for ", prot_id, chain_id)
    continue

  bu = None
  if len(bus) == 0:
    print("didnt find any valid biounit!")
    continue
  if len(bus) == 1:
    bu = bus[0]
  else:
    # If there are several biounits we search for the one that
    # is likely to have the relevant oligomeric state
    bu = bus[0]
    for b in bus:
      if "author" in b.details and "software" in b.details:
        bu = b
        break
 
  for polypeptide in bu.polypeptides:

    if polypeptide.is_polypeptide:
      found_chain = False

      for ch in polypeptide.chains:

        if ch.orig_pdb_name == chain_id:
              
          unique_name = ch.unique_id
          structure = ch.structure
          profile_path = tpl_lib.ProfileForTemplate(unique_name)

          profile_out_path = os.path.join(profile_out_dir, prot_id + ch.name + ".hhm")
          structure_out_path = os.path.join(structure_out_dir, prot_id + ".pdb")

          # copy over the profile
          os.system(' '.join(["cp", profile_path, profile_out_path]))

          # get the structure
          full_structure = bu.GetSubstructure(bu.polypeptides)
          ost.io.SavePDB(full_structure,structure_out_path)
          found_chain=True

          # add the actually extracted pdb id and chain name
          fetched_structures.append((prot_id, ch.name))

          print("found the according data...")
          break

      if found_chain:
        break
  


outfile = open(pisces_path + "_data_extracted_from_smtl.txt",'w')
outfile.write('\n'.join([(x[0] + ' ' + x[1]) for x in fetched_structures]))
outfile.close()

