# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from promod3 import loop
import os

profiles_per_job = 60
structure_db_path = "/scicore/home/schwede/studga00/data/promod3_databases/final_shit/structure_db_60.dat"
source_structure_db_path = "/scicore/home/schwede/studga00/data/promod3_databases/final_shit/structure_db_20.dat"
profile_db_out_dir = "/scicore/home/schwede/studga00/data/promod3_databases/final_shit/structure_profile_dbs"
awesome_script = "/scicore/home/schwede/studga00/data/promod3_databases/final_shit/create_structure_profiles.py"


structure_db = loop.StructureDB.Load(structure_db_path)
n_entries = structure_db.GetNumCoords()

from_idx = 0
to_idx = profiles_per_job

n_jobs = int(n_entries / profiles_per_job) + 1

commands = list()

for job_idx in range(n_jobs):

  current_cmd = list()
  current_cmd.append("pm")
  current_cmd.append(awesome_script)
  current_cmd.append(structure_db_path)
  current_cmd.append(source_structure_db_path)
  current_cmd.append(str(from_idx))
  current_cmd.append(str(to_idx))
  current_cmd.append(os.path.join(profile_db_out_dir, str(job_idx)+".dat"))

  commands.append(' '.join(current_cmd))  

  from_idx = min(n_entries - 1, from_idx + profiles_per_job)
  to_idx = min(n_entries, to_idx + profiles_per_job)

outfile = open("structure_profile_generation.cmd", 'w')
outfile.write('\n'.join(commands))
outfile.close()

stuff_to_write = list()
stuff_to_write.append("#!/bin/bash")
stuff_to_write.append("#SBATCH --job-name=structure_scoring_array")
stuff_to_write.append("#SBATCH --time=06:00:00")
stuff_to_write.append("#SBATCH --output=%s"%(os.path.join("stdout", "%A_%a.out")))
stuff_to_write.append("#SBATCH --mem=8G")
stuff_to_write.append("#SBATCH --array=1-%i"%(len(commands)))
stuff_to_write.append("source /scicore/home/schwede/studga00/data/promod3_databases/final_shit/setup_bc2_centos7")
stuff_to_write.append("SEEDFILE=%s"%(os.path.join(os.getcwd(), "structure_profile_generation.cmd")))
stuff_to_write.append("SEED=$(sed -n ${SLURM_ARRAY_TASK_ID}p $SEEDFILE)")
stuff_to_write.append("eval $SEED")

outfile = open("structure_profile_generation_array.sh", 'w')
outfile.write('\n'.join(stuff_to_write))
outfile.close()


