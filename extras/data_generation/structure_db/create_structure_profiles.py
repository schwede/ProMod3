# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
from promod3 import loop
from ost import seq
import time
import traceback

if len(sys.argv) != 6:

  params = ["structure_db_path", "structure_db_source_path",\
            "start", "end", "profile_db_out_path"]
  print("USAGE: ost create_structure_profiles.py " + ' '.join(params))
  sys.exit()

# loads a StructureDB that can be arbitrarily large and estimates the
# structure profile for a subset of its entries with the structural information
# from another StructureDB. This other DB, the "source" DB should not be too
# large because its size heavily influences compuation time.

# structure_db_path: path to database for which you want to create
#                    StructureProfiles
#
# structure_db_source_path: path to database from which you get the 
#                           structural information to calculate the profiles
#                           this database should not be much larger than 5000
#                           entries in order to keep computation time in a 
#                           feasible range
#
# start: index of entry in StructureDB at structure_db_path to start with
#
# end: index of entry in StructureDB at structure_db_path to end with 
#      (not inclusive)
#
# profile_db_out_path: All structure profiles will be stored in a 
#                      ost.seq.HMMDB() that will be dumped at this path

structure_db_path = sys.argv[1]
structure_db_source_path = sys.argv[2]
start = int(sys.argv[3])
end = int(sys.argv[4])
profile_db_out_path = sys.argv[5]

structure_db = loop.StructureDB.Load(structure_db_path)
structure_db_source = loop.StructureDB.Load(structure_db_source_path)

profile_db = seq.ProfileDB()

for i in range(start,end):
  print("processing chain with idx", i)

  try:
    # generate fragment info object
    coord_info = structure_db.GetCoordInfo(i)
    fragment_info = loop.FragmentInfo(i,0,coord_info.size)
    sequence = structure_db.GetSequence(fragment_info)
    bb_list = structure_db.GetBackboneList(fragment_info, sequence)
    residue_depths = structure_db.GetResidueDepths(fragment_info)  

    print("id: ", coord_info.id)
    print("chain_name: ", coord_info.chain_name)
    print("sequence length: ", len(sequence))

    # generate the structure profile given the source StructureDB
    start = time.time()
    profile = structure_db_source.GenerateStructureProfile(bb_list, residue_depths)
    end = time.time()

    print("it took: ", end-start)

    profile_db.AddProfile('_'.join([coord_info.id, coord_info.chain_name]), profile)

  except:
    traceback.print_exc()
    print("failed to create structure profile... skip...")

profile_db.Save(profile_db_out_path)

