# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from promod3 import loop
from ost import conop, io
from ost.mol import mm

########################################################################
def FixMapping(mapping_ff2idx, bm, ff_lookup, ff_aa, is_nter):
  # modifies mapping_ff2idx!!
  # apply rename rules to mapping
  for rule in bm.GetReplaceRules():
    old_name = rule[0]
    new_name = rule[1]
    if old_name == "OXT":
      mapping_ff2idx[new_name] = ff_lookup.GetOXTIndex(ff_aa, is_nter)
    elif old_name != new_name:
      my_idx = mapping_ff2idx[old_name]
      del mapping_ff2idx[old_name]
      mapping_ff2idx[new_name] = my_idx

def GetIndices(mapping_ff2idx, bb):
  bb2idx = dict()
  for aname in bb.GetAtoms():
    bb2idx[aname] = mapping_ff2idx[aname]
  return bb2idx

def GetFFAAString(ff_aa):
  return str(loop.ForcefieldAminoAcid.values[ff_aa])

def GetIDString(my_id):
  # ID = (ff_aa, is_nter, is_cter)
  result = GetFFAAString(my_id[0])
  if my_id[1] and my_id[2]:
    result += "-2ter"
  elif my_id[1]:
    result += "-Nter"
  elif my_id[2]:
    result += "-Cter"
  return result

# all internal connectivity apart from impropers starting from bonds
# -> bonds = tuple of indices
# -> bound_to[idx] = list of partner-indices
# -> ff = force field to get parameters for given atom types
# -> atom_types[idx] = atom type for idx
def GetConnectivity(bonds, bound_to, ff, atom_types):
  # get angles
  angles = set()
  for i,j in bonds:
    for k in bound_to[i]:
      # k-i-j
      if k != j:
        angles.add((min(j,k), i, max(j,k)))
    for k in bound_to[j]:
      # i-j-k
      if k != i:
        angles.add((min(i,k), j, max(i,k)))
  # get dihedrals
  dihedrals = set()
  for i,j,k in angles:
    for l in bound_to[i]:
      # l-i-j-k
      if l != j:
        if l < k:
          dihedrals.add((l,i,j,k))
        else:
          dihedrals.add((k,j,i,l))
    for l in bound_to[k]:
      # i-j-k-l
      if l != j:
        if l < i:
          dihedrals.add((l,k,j,i))
        else:
          dihedrals.add((i,j,k,l))
  # get exclusions = bonds + 1,3 pairs
  exclusions = set()
  for i,j in bonds:
    assert((i,j) not in exclusions)
    exclusions.add((i,j))
  for i,j,k in angles:
    assert((i,k) not in exclusions)
    exclusions.add((i,k))
  # get LJ pairs = 1,4 pairs - exclusions
  pairs_14 = set()
  for i,j,k,l in dihedrals:
    if (i,l) not in exclusions:
      pairs_14.add((i,l))
  # parametrize bonds
  harmonic_bonds = list()
  for i,j in bonds:
    # get params
    interaction = ff.GetBond(atom_types[i], atom_types[j])
    assert(interaction.IsParametrized())
    assert(interaction.GetFuncType() == mm.FuncType.HarmonicBond)
    params = interaction.GetParam()
    # add bond info
    bond_info = loop.ForcefieldBondInfo()
    bond_info.index_one = i
    bond_info.index_two = j
    bond_info.bond_length = params[0]
    bond_info.force_constant = params[1]
    harmonic_bonds.append(bond_info)
  # parametrize angles
  urey_bradley_angles = list()
  for i,j,k in angles:
    # get params
    interaction = ff.GetAngle(atom_types[i], atom_types[j], atom_types[k])
    assert(interaction.IsParametrized())
    assert(interaction.GetFuncType() == mm.FuncType.UreyBradleyAngle)
    params = interaction.GetParam()
    # add angle info
    angle_info = loop.ForcefieldUreyBradleyAngleInfo()
    angle_info.index_one = i
    angle_info.index_two = j
    angle_info.index_three = k
    angle_info.angle = params[0]
    angle_info.angle_force_constant = params[1]
    angle_info.bond_length = params[2]
    angle_info.bond_force_constant = params[3]
    urey_bradley_angles.append(angle_info)
  # parametrize dihedrals
  periodic_dihedrals = list()
  for i,j,k,l in dihedrals:
    # get interactions
    interactions = ff.GetDihedrals(atom_types[i], atom_types[j], \
                                   atom_types[k], atom_types[l])
    for interaction in interactions:
      # get params
      assert(interaction.IsParametrized())
      assert(interaction.GetFuncType() == mm.FuncType.PeriodicDihedral)
      params = interaction.GetParam()
      # add angle info
      dihedral_info = loop.ForcefieldPeriodicDihedralInfo()
      dihedral_info.index_one = i
      dihedral_info.index_two = j
      dihedral_info.index_three = k
      dihedral_info.index_four = l
      dihedral_info.multiplicity = int(params[0])
      dihedral_info.phase = params[1]
      dihedral_info.force_constant = params[2]
      periodic_dihedrals.append(dihedral_info)
  # parametrize LJ pairs
  lj_pairs = list()
  for i,j in pairs_14:
    # get params
    interaction = ff.GetLJ(atom_types[i], atom_types[j], True)
    assert(interaction.IsParametrized())
    assert(interaction.GetFuncType() == mm.FuncType.LJPair)
    params = interaction.GetParam()
    # add bond info
    lj_info = loop.ForcefieldLJPairInfo()
    lj_info.index_one = i
    lj_info.index_two = j
    lj_info.sigma = params[0]
    lj_info.epsilon = params[1]
    lj_pairs.append(lj_info)
  # get connectivity
  connectivity = loop.ForcefieldConnectivity()
  connectivity.harmonic_bonds = harmonic_bonds
  connectivity.harmonic_angles = []
  connectivity.urey_bradley_angles = urey_bradley_angles
  connectivity.periodic_dihedrals = periodic_dihedrals
  connectivity.periodic_impropers = []
  connectivity.harmonic_impropers = []
  connectivity.lj_pairs = lj_pairs
  return connectivity

def GetNumConnectivities(connectivity, to_add=None):
  result = [len(connectivity.harmonic_bonds), \
            len(connectivity.harmonic_angles), \
            len(connectivity.urey_bradley_angles), \
            len(connectivity.periodic_dihedrals), \
            len(connectivity.periodic_impropers), \
            len(connectivity.harmonic_impropers), \
            len(connectivity.lj_pairs)]
  if to_add is None:
    return result
  else:
    assert(len(to_add) == len(result))
    return [n1+n2 for n1,n2 in zip(to_add,result)]
########################################################################

# setup MM and PM3 force field
ff = mm.LoadCHARMMForcefield()
ff_lookup = loop.ForcefieldLookup()

# get data to look at: (ff_aa and ff_res_name)
res_ids = list()
for i in range(loop.ForcefieldAminoAcid.FF_XXX):
  ff_aa = loop.ForcefieldAminoAcid(i)
  # get ff_name
  if ff_aa == loop.ForcefieldAminoAcid.FF_CYS2:
    gromacs_name = "CYS2"
  elif ff_aa == loop.ForcefieldAminoAcid.FF_HISE:
    gromacs_name = "HISE"
  elif ff_aa == loop.ForcefieldAminoAcid.FF_HISD:
    gromacs_name = "HISD"
  else:
    gromacs_name = conop.AminoAcidToResidueName(ff_lookup.GetAA(ff_aa))
  res_ids.append((ff_aa, ff.GetResidueRenamingMain(gromacs_name)))

# get rename rules for atoms:
# -> atom_mapping_ff2pdb[ff_aa][aname_ff] = aname
# -> atom_mapping_ff2idx[ff_aa][aname_ff] = atom_idx
atom_mapping_ff2pdb = list()
atom_mapping_ff2idx = list()
for i in range(loop.ForcefieldAminoAcid.FF_XXX):
  ff_aa = loop.ForcefieldAminoAcid(i)
  aa = ff_lookup.GetAA(ff_aa)
  cur_mapping_ff2pdb = dict()
  cur_mapping_ff2idx = dict()
  # heavy atoms
  for j in range(loop.AminoAcidLookup.GetNumAtoms(aa)):
    aaa = loop.AminoAcidLookup.GetAAA(aa, j)
    aname_ff = loop.AminoAcidLookup.GetAtomNameCharmm(aaa)
    aname = loop.AminoAcidLookup.GetAtomName(aaa)
    cur_mapping_ff2pdb[aname_ff] = aname
    cur_mapping_ff2idx[aname_ff] = ff_lookup.GetHeavyIndex(ff_aa, j)
    assert(cur_mapping_ff2idx[aname_ff] != 0 or aname == 'N')
  # hydrogens
  for j in range(loop.AminoAcidLookup.GetNumHydrogens(aa)):
    aah = loop.AminoAcidLookup.GetAAH(aa, j)
    aname_ff = loop.AminoAcidLookup.GetAtomNameCharmm(aah)
    aname = loop.AminoAcidLookup.GetAtomName(aah)
    # skip special cases
    if ff_aa == loop.ForcefieldAminoAcid.FF_HISE and aname == "HD1":
      continue
    if ff_aa == loop.ForcefieldAminoAcid.FF_HISD and aname == "HE2":
      continue
    if ff_aa == loop.ForcefieldAminoAcid.FF_CYS2 and aname == "HG":
      continue
    cur_mapping_ff2pdb[aname_ff] = aname
    cur_mapping_ff2idx[aname_ff] = ff_lookup.GetHydrogenIndex(ff_aa, j)
    assert(cur_mapping_ff2idx[aname_ff] != 0)
  atom_mapping_ff2pdb.append(cur_mapping_ff2pdb)
  atom_mapping_ff2idx.append(cur_mapping_ff2idx)

# get building blocks etc for all terminal cases
# -> indexing-tuple = (ff_aa, is_nter, is_cter)
building_blocks = dict()
mapping_bb2idx = dict()
for ff_aa, ff_res_name in res_ids:
  # get modifiers
  bmn = ff.GetNTerModifier(ff_res_name)
  bmc = ff.GetCTerModifier(ff_res_name)
  # non-ter
  my_id = (ff_aa, False, False)
  building_blocks[my_id] = ff.GetBuildingBlock(ff_res_name)
  mapping_bb2idx[my_id] = GetIndices(atom_mapping_ff2idx[ff_aa],
                                     building_blocks[my_id])
  # n-ter
  my_id = (ff_aa, True, False)
  building_blocks[my_id] = ff.GetBuildingBlock(ff_res_name)
  bmn.ApplyOnBuildingBlock(building_blocks[my_id])
  building_blocks[my_id].RemoveInteractionsToPrev()
  mapping_ff2idx = dict(atom_mapping_ff2idx[ff_aa])
  FixMapping(mapping_ff2idx, bmn, ff_lookup, ff_aa, True)
  mapping_bb2idx[my_id] = GetIndices(mapping_ff2idx, building_blocks[my_id])
  # c-ter
  my_id = (ff_aa, False, True)
  building_blocks[my_id] = ff.GetBuildingBlock(ff_res_name)
  bmc.ApplyOnBuildingBlock(building_blocks[my_id])
  building_blocks[my_id].RemoveInteractionsToNext()
  mapping_ff2idx = dict(atom_mapping_ff2idx[ff_aa])
  FixMapping(mapping_ff2idx, bmc, ff_lookup, ff_aa, False)
  mapping_bb2idx[my_id] = GetIndices(mapping_ff2idx, building_blocks[my_id])
  # two-ter
  my_id = (ff_aa, True, True)
  building_blocks[my_id] = ff.GetBuildingBlock(ff_res_name)
  bmn.ApplyOnBuildingBlock(building_blocks[my_id])
  building_blocks[my_id].RemoveInteractionsToPrev()
  bmc.ApplyOnBuildingBlock(building_blocks[my_id])
  building_blocks[my_id].RemoveInteractionsToNext()
  mapping_ff2idx = dict(atom_mapping_ff2idx[ff_aa])
  FixMapping(mapping_ff2idx, bmn, ff_lookup, ff_aa, True)
  FixMapping(mapping_ff2idx, bmc, ff_lookup, ff_aa, True)
  mapping_bb2idx[my_id] = GetIndices(mapping_ff2idx, building_blocks[my_id])

# get internal data for all atoms
# -> sets charges/masses/sigmas/epsilons for each
# -> all_atom_types[my_id][atom_idx] = atom type for atom_idx
# -> all_bound_to[my_id][atom_idx] = set of bonds for atom_idx
# -> all_ext_impropers[my_id] = list of 4 atom names including peptide bond
all_atom_types = dict()
all_bound_to = dict()
all_ext_impropers = dict()
my_ids = sorted(mapping_bb2idx.keys())
for ff_aa, is_nter, is_cter in my_ids:
  # extract stuff for readability
  my_id = (ff_aa, is_nter, is_cter)
  bb = building_blocks[my_id]
  bb2idx = mapping_bb2idx[my_id]
  # sanity check of indices -> all covered?
  num_atoms = ff_lookup.GetNumAtoms(ff_aa, is_nter, is_cter)
  assert(list(range(num_atoms)) == sorted(bb2idx.values()))
  # get atom_names, atom_types, charges, masses, sigmas, epsilons
  atom_names = [None] * num_atoms
  atom_types = [None] * num_atoms
  charges = [None] * num_atoms
  masses = [None] * num_atoms
  sigmas = [None] * num_atoms
  epsilons = [None] * num_atoms
  for aname in bb.GetAtoms():
    # get data
    atype = bb.GetType(aname)
    lj_inter = ff.GetLJ(atype)
    assert(lj_inter.IsParametrized())
    assert(lj_inter.GetFuncType() == mm.FuncType.LJ)
    lj_params = lj_inter.GetParam()
    assert(len(lj_params) == 2)
    # store it
    idx = bb2idx[aname]
    atom_names[idx] = aname
    atom_types[idx] = atype
    charges[idx] = bb.GetCharge(aname)
    masses[idx] = ff.GetMass(atype)
    sigmas[idx] = lj_params[0]
    epsilons[idx] = lj_params[1]
  assert(None not in atom_types + charges + masses + sigmas + epsilons)
  # check custom stuff in BB (none expected!)
  assert(len(bb.GetAngles()) == 0)
  assert(len(bb.GetConstraints()) == 0)
  assert(len(bb.GetDihedrals()) == 0)
  assert(len(bb.GetExclusions()) == 0)
  # get bonds from BB
  bonds = set()
  bound_to = list()
  for i in range(num_atoms):
    bound_to.append(set())
  for bond in bb.GetBonds():
    # we assume that parameters are in FF
    assert(not bond.IsParametrized())
    # get names and check for peptide bond
    bond_names = bond.GetNames()
    assert(len(bond_names) == 2)
    if sorted(bond_names) == sorted(["+N", "C"]):
      assert(not is_cter)
    elif sorted(bond_names) == sorted(["-C", "N"]):
      assert(not is_nter)
    else:
      # there should not be any other bond going to other residues!
      assert(not any('-' in name or '+' in name for name in bond_names))
      # get indices
      idx_one = bb2idx[bond_names[0]]
      idx_two = bb2idx[bond_names[1]]
      bound_to[idx_one].add(idx_two)
      bound_to[idx_two].add(idx_one)
      bonds.add((min(idx_one,idx_two), max(idx_one,idx_two)))
  # check bonds
  bonds_tst = set()
  for i in range(num_atoms):
    for j in bound_to[i]:
      bonds_tst.add((min(i,j), max(i,j)))
  assert(bonds_tst == bonds)
  # get connectivity
  connectivity = GetConnectivity(bonds, bound_to, ff, atom_types)
  # get/parametrize impropers
  harmonic_impropers = list()
  ext_impropers = list()
  for improper in bb.GetImpropers():
    # we assume that parameters are in FF
    assert(not improper.IsParametrized())
    # get names and check for peptide bond
    atom_names = improper.GetNames()
    if "-C" in atom_names or "+N" in atom_names:
      # keep external ones for later
      ext_impropers.append(atom_names)
      assert(not ("-C" in atom_names and "+N" in atom_names))
      assert(not (is_nter and is_cter))
    else:
      # there should not be any other improper going to other residues!
      assert(not any('-' in name or '+' in name for name in atom_names))
      # parametrize internal ones
      imp_indices = [bb2idx[aname] for aname in atom_names]
      imp_types = [atom_types[idx] for idx in imp_indices]
      interactions = ff.GetImpropers(imp_types[0], imp_types[1], \
                                     imp_types[2], imp_types[3])
      for interaction in interactions:
        # get params
        assert(interaction.IsParametrized())
        assert(interaction.GetFuncType() == mm.FuncType.HarmonicImproper)
        params = interaction.GetParam()
        # add angle info
        improper_info = loop.ForcefieldHarmonicImproperInfo()
        improper_info.index_one = imp_indices[0]
        improper_info.index_two = imp_indices[1]
        improper_info.index_three = imp_indices[2]
        improper_info.index_four = imp_indices[3]
        improper_info.angle = params[0]
        improper_info.force_constant = params[1]
        harmonic_impropers.append(improper_info)
  connectivity.harmonic_impropers = harmonic_impropers
  # store into ff_lookup
  ff_lookup.SetCharges(ff_aa, is_nter, is_cter, charges)
  ff_lookup.SetMasses(ff_aa, is_nter, is_cter, masses)
  ff_lookup.SetSigmas(ff_aa, is_nter, is_cter, sigmas)
  ff_lookup.SetEpsilons(ff_aa, is_nter, is_cter, epsilons)
  ff_lookup.SetInternalConnectivity(ff_aa, is_nter, is_cter, connectivity)
  # keep for future
  all_atom_types[my_id] = atom_types
  all_bound_to[my_id] = bound_to
  all_ext_impropers[my_id] = ext_impropers

# handle peptide bonds between res_one and res_two
for ff_aa_one, is_nter_one, is_cter_one in my_ids:
  # setup and skip C-ter for res_one
  if is_cter_one: continue
  my_id_one = (ff_aa_one, is_nter_one, is_cter_one)
  bb2idx_one = mapping_bb2idx[my_id_one]
  num_atoms_one = len(all_atom_types[my_id_one])
  assert(num_atoms_one == len(all_bound_to[my_id_one]))
  # combine with every other one
  for ff_aa_two, is_nter_two, is_cter_two in my_ids:
    # setup and skip N-ter for res_two
    if is_nter_two: continue
    my_id_two = (ff_aa_two, is_nter_two, is_cter_two)
    num_atoms_two = len(all_atom_types[my_id_two])
    assert(num_atoms_two == len(all_bound_to[my_id_two]))
    # fix indices in bb2idx_two
    bb2idx_two = dict()
    for aname in mapping_bb2idx[my_id_two]:
      bb2idx_two[aname] = mapping_bb2idx[my_id_two][aname] + num_atoms_one
    # combine atom_types and bound_to (must fix indices for second one)
    atom_types = all_atom_types[my_id_one] + all_atom_types[my_id_two]
    bound_to = list(all_bound_to[my_id_one])
    for i in range(num_atoms_two):
      bound_to.append(set())
      for j in all_bound_to[my_id_two][i]:
        bound_to[-1].add(j + num_atoms_one)
    # build peptide bond
    bonds = [(bb2idx_one['C'], bb2idx_two['N'])]
    connectivity = GetConnectivity(bonds, bound_to, ff, atom_types)
    # get impropers
    impropers = list()
    for imp_names in all_ext_impropers[my_id_one]:
      if "+N" in imp_names:
        imp_indices = list()
        for name in imp_names:
          if name == "+N":
            imp_indices.append(bb2idx_two['N'])
          else:
            imp_indices.append(bb2idx_one[name])
        impropers.append(imp_indices)
    for imp_names in all_ext_impropers[my_id_two]:
      if "-C" in imp_names:
        imp_indices = list()
        for name in imp_names:
          if name == "-C":
            imp_indices.append(bb2idx_one['C'])
          else:
            imp_indices.append(bb2idx_two[name])
        impropers.append(imp_indices)
    # parametrize impropers
    harmonic_impropers = list()
    for i,j,k,l in impropers:
      # get interactions
      interactions = ff.GetImpropers(atom_types[i], atom_types[j], \
                                     atom_types[k], atom_types[l])
      for interaction in interactions:
        # get params
        assert(interaction.IsParametrized())
        assert(interaction.GetFuncType() == mm.FuncType.HarmonicImproper)
        params = interaction.GetParam()
        # add angle info
        improper_info = loop.ForcefieldHarmonicImproperInfo()
        improper_info.index_one = i
        improper_info.index_two = j
        improper_info.index_three = k
        improper_info.index_four = l
        improper_info.angle = params[0]
        improper_info.force_constant = params[1]
        harmonic_impropers.append(improper_info)
    connectivity.harmonic_impropers = harmonic_impropers
    # store into ff_lookup
    ff_lookup.SetPeptideBoundConnectivity(ff_aa_one, ff_aa_two, is_nter_one,
                                          is_cter_two, connectivity)

# handle disulfid bridges -> as pairs above but CYS2-CYS2
cys2_id = (loop.ForcefieldAminoAcid.FF_CYS2, False, False)
num_atoms = len(all_atom_types[cys2_id])
# combine atom_types and bound_to (must fix indices for second one)
atom_types = all_atom_types[cys2_id] + all_atom_types[cys2_id]
bound_to = list(all_bound_to[cys2_id])
for i in range(num_atoms):
  bound_to.append(set())
  for j in all_bound_to[cys2_id][i]:
    bound_to[-1].add(j + num_atoms)
# build disulfid bridge connectivity
sg_name = loop.AminoAcidLookup.GetAtomNameCharmm(loop.AminoAcidAtom.CYS_SG)
idx_one = mapping_bb2idx[cys2_id][sg_name]
bonds = [(idx_one, idx_one + num_atoms)]
connectivity = GetConnectivity(bonds, bound_to, ff, atom_types)
# store into ff_lookup
ff_lookup.SetDisulfidConnectivity(connectivity)

# add fudges
ff_lookup.SetFudgeLJ(ff.GetFudgeLJ())
ff_lookup.SetFudgeQQ(ff.GetFudgeQQ())

# store it
# ff_lookup.Save("ff_lookup_charmm.dat")
ff_lookup.SavePortable("portable_ff_lookup_charmm.dat")

# STATS
all_num_conn = [0]*7
for ff_aa, is_nter, is_cter in my_ids:
  my_id = (ff_aa, is_nter, is_cter)
  # get stats
  num_atoms = len(ff_lookup.GetCharges(ff_aa, is_nter, is_cter))
  assert(num_atoms == len(ff_lookup.GetMasses(ff_aa, is_nter, is_cter)))
  assert(num_atoms == len(ff_lookup.GetSigmas(ff_aa, is_nter, is_cter)))
  assert(num_atoms == len(ff_lookup.GetEpsilons(ff_aa, is_nter, is_cter)))
  connectivity = ff_lookup.GetInternalConnectivity(ff_aa, is_nter, is_cter)
  num_connectivities = GetNumConnectivities(connectivity)
  # add from pairs
  for i in range(loop.ForcefieldAminoAcid.FF_XXX):
    ff_aa_two = loop.ForcefieldAminoAcid(i)
    connectivity = ff_lookup.GetPeptideBoundConnectivity(ff_aa, ff_aa_two,
                                                         is_nter, is_cter)
    num_connectivities = GetNumConnectivities(connectivity, num_connectivities)
  # add to total
  all_num_conn = [n1+n2 for n1,n2 in zip(num_connectivities,all_num_conn)]
  # report
  print(GetIDString(my_id), "N", num_atoms,\
        "B", num_connectivities[0],\
        "HA", num_connectivities[1], \
        "UA", num_connectivities[2], \
        "PD", num_connectivities[3], \
        "PI", num_connectivities[4], \
        "HI", num_connectivities[5], \
        "P", num_connectivities[6])
# bridge
connectivity = ff_lookup.GetDisulfidConnectivity()
num_connectivities = GetNumConnectivities(connectivity)
print("-"*70, "\nDISULFID BRIDGE", \
        "B", num_connectivities[0],\
        "HA", num_connectivities[1], \
        "UA", num_connectivities[2], \
        "PD", num_connectivities[3], \
        "PI", num_connectivities[4], \
        "HI", num_connectivities[5], \
        "P", num_connectivities[6])
# SUMMARY
print("-"*70, "\nTOTAL",\
      "B", all_num_conn[0],\
      "HA", all_num_conn[1], \
      "UA", all_num_conn[2], \
      "PD", all_num_conn[3], \
      "PI", all_num_conn[4], \
      "HI", all_num_conn[5], \
      "P", all_num_conn[6])
print("FUDGES", ff_lookup.GetFudgeLJ(), ff_lookup.GetFudgeQQ())

