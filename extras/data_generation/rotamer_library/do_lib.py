# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os, math, sys
import numpy as np
from scipy.ndimage import gaussian_filter
from promod3 import sidechain

if len(sys.argv) != 3:
  print("USAGE: ost do_lib.py <path_to_data> <library_out_path>") 
  sys.exit(0)

# the csv file you created with fetch_data.py
data_file_path = sys.argv[1]

# library will be dumped here using the SavePortable() method
library_out_path = sys.argv[2]


# default parametrization

# Width of bins to sample for non rotameric dihedral angles (in degrees)
# e.g. in case of GLN you'll have 360 / 30 = 12 conformations for the
# terminal dihedral angle if you keep the default (30).
non_rot_sampling = 30 

# An initial pdf is generated for the non-rotameric chi angles. The pdf is
# smoothed with a circular gaussian function with following standard 
# deviation (in degrees)
non_rot_smooth_std = 10

def GetNonRotChiStats(chi, non_rot_sampling, symmetric, smooth_std):

  # stupid check 
  if 180 % non_rot_sampling != 0:
    raise RuntimeError("180 must be dividable by non_rot_sampling!")

  n_sampled_rotamers = int(360 / non_rot_sampling)
  if symmetric:
    n_sampled_rotamers = int(180 / non_rot_sampling)

  # symmetric or not, we first fill in a histogram covering a range of 2 pi
  n_histogram_bins = 2 * 360 
  histogram = np.zeros(n_histogram_bins)
  bin_size = 2*np.pi/n_histogram_bins

  # we add up the histogram by weights as estimated with the mises distribution
  angle_bins = np.clip(np.floor((chi + np.pi) / bin_size), 0, len(histogram)-1)
  for angle_bin in angle_bins:
    histogram[int(angle_bin)] += 1.0

  # if we're dealing with a symmetric distribution, we simply add up the
  # first and second half of the histogram. The histogram has then an angle
  # range of [-pi,0.0[
  if symmetric:
    n_histogram_bins = int(n_histogram_bins / 2)
    histogram = np.add(histogram[:n_histogram_bins], 
                       histogram[n_histogram_bins:])

  # translate the smooth_std, which is in degree, to bins in the histogram
  # and apply it with a simple gaussian filter
  gaussian_std = float(smooth_std) / 180 * np.pi / bin_size
  histogram = gaussian_filter(histogram, gaussian_std, mode="wrap")
  histogram = histogram / sum(histogram)

  # find the maximum element, as this is the center of the first "window"
  # of width non_rot_sampling
  max_idx = np.argmax(histogram)
  current_angle = max_idx * bin_size - np.pi

  # concatenate, so we dont have to care for circularity anymore
  histogram = np.concatenate((histogram, histogram, histogram, histogram))
  
  rad_non_rot_sampling = float(non_rot_sampling) / 180 * np.pi
  first_start_angle = current_angle - rad_non_rot_sampling / 2

  if first_start_angle < -np.pi:
    # With this first angle we would get an underflow...
    # we did so many concatenatinons, that we can add 360 degrees, even for
    # the symmetric case
    current_angle += 2*np.pi

  probabilities = np.zeros(n_sampled_rotamers)
  mean_values = np.zeros(n_sampled_rotamers)
  std_values = np.zeros(n_sampled_rotamers)

  for i in range(n_sampled_rotamers):
    start_angle = current_angle - rad_non_rot_sampling / 2
    end_angle = current_angle + rad_non_rot_sampling / 2

    start_bin = int((start_angle + np.pi) / bin_size)
    end_bin = int((end_angle + np.pi) / bin_size)
    local_values = histogram[start_bin:end_bin+1]

    if sum(local_values) == 0:
      # no data points, let's create a zero probability rotamer
      probabilities[i] = 0.0
      mean_values[i] = current_angle
      std_values[i] = np.pi # just give rather large value
    else:
      normalized_local_values = local_values / sum(local_values)
      angle_values = np.linspace(start_angle, end_angle, len(local_values))

      probabilities[i] = np.sum(local_values)
      mean_values[i] = np.sum(np.multiply(normalized_local_values, angle_values))
      squared_std = np.sum(np.multiply(np.square(angle_values - mean_values[i]), 
                                                 normalized_local_values))
      std_values[i] = np.sqrt(squared_std)

    current_angle += rad_non_rot_sampling

  # The probabilities of all ranges should add up to one...
  # however, depending on the rounding etc., this might not exactly be the case.
  # All probabilities need an additional normalization in the end
  probabilities /= np.sum(probabilities)

  # due to the concatenation, we still have mean values > pi.
  # let's enforce valid ranges...
  for i in range(len(mean_values)):
    while mean_values[i] > np.pi:
      mean_values[i] -= 2*np.pi

  return probabilities, mean_values, std_values

def GetChiStats(chi):
  # do the mean
  a = np.sum(np.sin(chi))
  b = np.sum(np.cos(chi))
  mean_value = np.arctan2(a, b)
  # do the std
  diffs = np.minimum(np.minimum(np.absolute(chi - mean_value), 
                                np.absolute(chi - (mean_value + 2*np.pi))), 
                     np.absolute(chi - (mean_value - 2*np.pi)))
  squared_std = np.sum(np.square(diffs)) / len(diffs)
  return (mean_value, np.sqrt(squared_std))


def DoIt(aa_name, data_file_path, library, non_rot_sampling = 30, 
         non_rot_smooth_std = 10):

  print("processing", aa_name)
  
  # Parse CSV file
  data = open(data_file_path, 'r').readlines()

  # get data indices
  header = [item.strip() for item in data[0].split(',')]

  # will throw an error if not there
  rname_idx = header.index("rname")
  chi1_idx = header.index("chi1")
  chi2_idx = header.index("chi2")
  chi3_idx = header.index("chi3")
  chi4_idx = header.index("chi4")
  conf1_idx = header.index("conf1")
  conf2_idx = header.index("conf2")
  conf3_idx = header.index("conf3")
  conf4_idx = header.index("conf4")
  str_c = ["GAUCHE_MINUS", "GAUCHE_PLUS", "TRANS", "NON_ROTAMERIC", "INVALID"]
  promod_c = [sidechain.GAUCHE_MINUS, sidechain.GAUCHE_PLUS, sidechain.TRANS, 
              sidechain.NON_ROTAMERIC, sidechain.INVALID]

  # get the data
  aa_data = {"chi1" : [], "chi2" : [], "chi3" : [], "chi4" : [], 
             "conf1" : [], "conf2" : [], "conf3" : [], "conf4" : []}
  
  for line in data[1:]:
    split_line = [item.strip() for item in line.split(',')]
    rname = split_line[rname_idx]
    if rname == aa_name:
      aa_data["chi1"].append(float(split_line[chi1_idx]))
      aa_data["chi2"].append(float(split_line[chi2_idx]))
      aa_data["chi3"].append(float(split_line[chi3_idx]))
      aa_data["chi4"].append(float(split_line[chi4_idx]))
      aa_data["conf1"].append(promod_c[str_c.index(split_line[conf1_idx])])
      aa_data["conf2"].append(promod_c[str_c.index(split_line[conf2_idx])])
      aa_data["conf3"].append(promod_c[str_c.index(split_line[conf3_idx])])
      aa_data["conf4"].append(promod_c[str_c.index(split_line[conf4_idx])])
  
  # convert all dihedral angles into numpy arrays
  aa_data["chi1"] = np.array(aa_data["chi1"])
  aa_data["chi2"] = np.array(aa_data["chi2"])
  aa_data["chi3"] = np.array(aa_data["chi3"])
  aa_data["chi4"] = np.array(aa_data["chi4"])

  # the configurations in aa_data are stored in four lists with keys 
  # conf1, conf2... we translate that to a unique string
  configurations = list()
  for a, b, c, d in zip(aa_data["conf1"], aa_data["conf2"], 
                        aa_data["conf3"], aa_data["conf4"]):
    configurations.append("%i%i%i%i"%(int(a), int(b), int(c), int(d)))
  unique_configurations = list(set(configurations))

  # we need configuration dependent chix
  conf_chi1 = dict()
  conf_chi2 = dict()
  conf_chi3 = dict()
  conf_chi4 = dict()
  rotameric_probabilities = dict()

  for uc in unique_configurations:
    indices = [i for i, c in enumerate(configurations) if c == uc]
    conf_chi1[uc] = aa_data["chi1"][indices]
    conf_chi2[uc] = aa_data["chi2"][indices]
    conf_chi3[uc] = aa_data["chi3"][indices]
    conf_chi4[uc] = aa_data["chi4"][indices]
    rotameric_probabilities[uc] = float(len(indices)) / len(configurations)

  rot_confs = [int(sidechain.TRANS), int(sidechain.GAUCHE_MINUS),
               int(sidechain.GAUCHE_PLUS)] 

  for uc in unique_configurations:

    rotameric_chi1 = float("nan")
    rotameric_sig1 = float("nan")
    rotameric_chi2 = float("nan")
    rotameric_sig2 = float("nan")
    rotameric_chi3 = float("nan")
    rotameric_sig3 = float("nan")
    rotameric_chi4 = float("nan")
    rotameric_sig4 = float("nan")

    # do chi1
    if int(uc[0]) in rot_confs:
      mean, std = GetChiStats(conf_chi1[uc])
      rotameric_chi1 = mean
      rotameric_sig1 = std
    # do chi2
    if int(uc[1]) in rot_confs:
      mean, std = GetChiStats(conf_chi2[uc])
      rotameric_chi2 = mean
      rotameric_sig2 = std
    # do chi3
    if int(uc[2]) in rot_confs:
      mean, std = GetChiStats(conf_chi3[uc])
      rotameric_chi3 = mean
      rotameric_sig3 = std
    # do chi4
    if int(uc[3]) in rot_confs:
      mean, std = GetChiStats(conf_chi4[uc])
      rotameric_chi4 = mean
      rotameric_sig4 = std

    # so far we only treated rotameric dihedral angles. 
    # ASN(chi2), ASP(chi2), PHE(chi2), TYR(chi2), HIS(chi2), TRP(chi2), 
    # GLN(chi3) and GLU(chi3) exhibit so called non-rotameric degrees
    # of freedom and require further sampling of their terminal 
    # dihedral angle.
    if aa_name in ["ASN", "ASP", "PHE", "TYR", "HIS", "TRP", "GLN", "GLU"]:
      non_rotameric_chi_idx = 1
      if aa_name in ["GLN", "GLU"]:
        non_rotameric_chi_idx = 2
      symmetric = False
      if aa_name in ["ASP", "PHE", "TYR", "GLU"]:
        symmetric = True
      chi_angles = None
      if non_rotameric_chi_idx == 1:
        chi_angles = conf_chi2[uc]
      elif non_rotameric_chi_idx == 2:
        chi_angles = conf_chi3[uc]
      probs, mean_values, std_values = GetNonRotChiStats(chi_angles,
                                                         non_rot_sampling,
                                                         symmetric, 
                                                         non_rot_smooth_std)
      for p, m, s in zip(probs, mean_values, std_values):
        rot = sidechain.RotamerLibEntry(p*rotameric_probabilities[uc],
                                        rotameric_chi1, rotameric_chi2,
                                        rotameric_chi3, rotameric_chi4,
                                        rotameric_sig1, rotameric_sig2,
                                        rotameric_sig3, rotameric_sig4)
        if non_rotameric_chi_idx == 1:
          rot.chi2 = m
          rot.sig2 = s
        elif non_rotameric_chi_idx == 2:
          rot.chi3 = m
          rot.sig3 = s
        lib.AddRotamer(sidechain.TLCToRotID(aa_name), rot)
    else:
      rot = sidechain.RotamerLibEntry(rotameric_probabilities[uc],
                                      rotameric_chi1, rotameric_chi2,
                                      rotameric_chi3, rotameric_chi4,
                                      rotameric_sig1, rotameric_sig2,
                                      rotameric_sig3, rotameric_sig4)
      lib.AddRotamer(sidechain.TLCToRotID(aa_name), rot)



lib = sidechain.RotamerLib()
DoIt("ARG", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("ASN", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("ASP", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("GLN", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("GLU", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("LYS", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("SER", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("CYH", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("CYD", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("CYS", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("MET", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("TRP", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("TYR", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("THR", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("VAL", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("ILE", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("LEU", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("CPR", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("TPR", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("PRO", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("HIS", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
DoIt("PHE", data_file_path, lib, non_rot_sampling = non_rot_sampling, 
     non_rot_smooth_std = non_rot_smooth_std)
lib.MakeStatic()
lib.SavePortable(library_out_path)
