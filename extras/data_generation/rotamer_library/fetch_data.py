# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
from promod3 import sidechain
import numpy as np

# file of non redundant X-Ray structures. 
# Get one at: http://dunbrack.fccc.edu/Guoli/pisces_download.php
pisces_filename = "cullpdb_pc40_res1.6_R0.25_d180913_chains5877"

# Directory from where to load the structures. 
# Filename name of pisces entry 5XDTA in that directory should be: 5xdt.pdb.
# Chain A gets then selected automatically at the data extraction step.
# If you set the structure_dir to none, the file is automatically fetched from
# rcsb but this takes much longer...
structure_dir = None

# filename for output data that will be in csv format
out_path = "raw_data.csv"

sidechain_data = dict()
sidechain_data["ARG"] = list()
sidechain_data["ASN"] = list()
sidechain_data["ASP"] = list()
sidechain_data["GLN"] = list()
sidechain_data["GLU"] = list()
sidechain_data["LYS"] = list()
sidechain_data["SER"] = list()
sidechain_data["CYH"] = list()
sidechain_data["CYD"] = list()
sidechain_data["CYS"] = list()
sidechain_data["MET"] = list()
sidechain_data["TRP"] = list()
sidechain_data["TYR"] = list()
sidechain_data["THR"] = list()
sidechain_data["VAL"] = list()
sidechain_data["ILE"] = list()
sidechain_data["LEU"] = list()
sidechain_data["CPR"] = list()
sidechain_data["TPR"] = list()
sidechain_data["PRO"] = list()
sidechain_data["HIS"] = list()
sidechain_data["PHE"] = list()

dihedral_data = dict()
for k in list(sidechain_data.keys()):
  dihedral_data[k] = list()

infile = open(pisces_filename, 'r')
data = infile.readlines()
infile.close()

for line in data[1:]:

  pdb_id = line[:4]
  chain_id = line[4]

  print("processing", pdb_id)

  prot = None

  try:
    if structure_dir == None:
      prot = io.LoadPDB(pdb_id.lower(), remote=True)
    else:
      prot = io.LoadPDB(os.path.join(structure_dir, pdb_id.lower() + ".pdb"))
    prot = prot.Select("peptide=true and cname="+chain_id)
  except:
    print("Failed to get the structure! skip...")
    continue

  for r in prot.residues:

    rname = r.GetName()
    phi_handle = r.GetPhiTorsion()
    psi_handle = r.GetPsiTorsion()
    if not (phi_handle.IsValid() and psi_handle.IsValid()):
      continue
    phi = phi_handle.GetAngle()
    psi = psi_handle.GetAngle()

    # no processing of boring stuff
    if rname == "ALA" or rname == "GLY":
      continue

    # do the special cases first
    if rname == "CYS":
      sg = r.FindAtom("SG")
      
      if not sg.IsValid():
        continue
  
      close_atoms = prot.FindWithin(sg.GetPos(), 2.5)
      disulfid = False
 
      for a in close_atoms:
        if a.name == "SG":
          if a.GetResidue().GetHandle() != r.handle:
            disulfid = True
            break

      if disulfid:
        try:
          rot_lib_entry = sidechain.RotamerLibEntry.FromResidue(r, sidechain.CYD)
        except:
          continue
        sidechain_data["CYD"].append(rot_lib_entry)  
        dihedral_data["CYD"].append((phi, psi))
        sidechain_data["CYS"].append(rot_lib_entry)  
        dihedral_data["CYS"].append((phi, psi))  

      else:
        try:
          rot_lib_entry = sidechain.RotamerLibEntry.FromResidue(r, sidechain.CYH)
        except:
          continue
        sidechain_data["CYH"].append(rot_lib_entry)    
        dihedral_data["CYH"].append((phi, psi)) 
        sidechain_data["CYS"].append(rot_lib_entry)  
        dihedral_data["CYS"].append((phi, psi)) 

    elif rname == "PRO":
      rprev = r.handle.GetPrev()
      if not rprev.IsValid():
        continue
      if not mol.InSequence(rprev, r.handle):
        continue
      
      caprev = rprev.FindAtom("CA")
      cprev = rprev.FindAtom("C")
      n = r.FindAtom("N")
      ca = r.FindAtom("CA")

      if not (caprev.IsValid() and cprev.IsValid() and n.IsValid() and ca.IsValid()):
        continue

      omega = geom.DihedralAngle(caprev.GetPos(), cprev.GetPos(), n.GetPos(), ca.GetPos())

      if abs(omega) > 90.0 / 180 * np.pi:
        try:
          rot_lib_entry = sidechain.RotamerLibEntry.FromResidue(r, sidechain.TPR)
        except:
          continue
        sidechain_data["TPR"].append(rot_lib_entry)
        dihedral_data["TPR"].append((phi, psi))
        sidechain_data["PRO"].append(rot_lib_entry)
        dihedral_data["PRO"].append((phi, psi))
      else:
        try:
          rot_lib_entry = sidechain.RotamerLibEntry.FromResidue(r, sidechain.CPR)
        except:
          continue
        sidechain_data["CPR"].append(rot_lib_entry)
        dihedral_data["CPR"].append((phi, psi))
        sidechain_data["PRO"].append(rot_lib_entry)
        dihedral_data["PRO"].append((phi, psi))

    else:
      # we don't process exotic stuff...
      if rname not in sidechain_data:
        continue
      rot_id = sidechain.TLCToRotID(rname)
      try:
        rot_lib_entry = sidechain.RotamerLibEntry.FromResidue(r, rot_id)
      except:
        # not all atoms present...
        continue
      sidechain_data[rname].append(rot_lib_entry)
      dihedral_data[rname].append((phi, psi))

# data output
file_content = ["rname, phi, psi, chi1, chi2, chi3, chi4, conf1, conf2, conf3, conf4"]
for rname, entry_list in sidechain_data.items():
  rot_id = sidechain.TLCToRotID(rname)
  for e, dihedral_angles in zip(entry_list, dihedral_data[rname]):
    temp = [rname, "%.4f"%(dihedral_angles[0]), "%.4f"%(dihedral_angles[1]), 
            "%.4f"%(e.chi1), "%.4f"%(e.chi2), "%.4f"%(e.chi3), "%.4f"%(e.chi4),
            str(sidechain.GetDihedralConfiguration(e, rot_id, 0)),
            str(sidechain.GetDihedralConfiguration(e, rot_id, 1)),
            str(sidechain.GetDihedralConfiguration(e, rot_id, 2)),
            str(sidechain.GetDihedralConfiguration(e, rot_id, 3))]

    file_content.append(','.join(temp))

outfile = open(out_path, 'w')
outfile.write('\n'.join(file_content))
outfile.close()

