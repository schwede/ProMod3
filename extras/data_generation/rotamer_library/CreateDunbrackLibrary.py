# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from promod3 import sidechain

# the file is available here: http://dunbrack.fccc.edu/bbdep2010/
# -> the library comes with a license and hence cannot be distributed here
f_name = '<PATH_TO_DUNBRACK_2010_LIBRARY>/ExtendedOpt2-5/ALL.bbdep.rotamers.lib'

lib = sidechain.ReadDunbrackFile(f_name)

lib.Save("2010DunbrackLibWithNonRotameric.dat")

