# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os, math, sys
import numpy as np
from scipy.ndimage import gaussian_filter
from promod3 import sidechain

if len(sys.argv) != 3:
  print("USAGE: ost do_lib.py <path_to_data> <aa_name>") 
  sys.exit(0)

# the csv file you created with fetch_data.py
data_file_path = sys.argv[1]

# the name of the amino acids for which you want to create a library
# => can be any amino acid you have in the provided datafile
aa_name = sys.argv[2]

# The mises distribution depends on kappa to determine the smoothness. 
# When thinking of a classical gaussian, r = sqrt(1.0/kappa) corresponds to
# the radius of a circle around the mean that covers 67% of the density.
# In order to generate adaptive distributions with respect to phi/psi, 
# kappa is estimated such that this "circle" includes exactly N data points.
# Be aware that N needs to be increased in case of higher dimensional 
# probability functions (e.g. more rotameric dihedral angles)
p_threshold_67 = 30
if aa_name in ["LYS", "ARG"]:
  p_threshold_67 = 50

# Same threshold but for the chi angles
# In this case we're considering only data with same configuration
chi_threshold_67 = 20

# if you want to have 10 degree bins for backbone dihedral angles, use 36
n_bins = 36

# Width of bins to sample for non rotameric dihedral angles (in degrees)
# e.g. in case of GLN you'll have 360 / 30 = 12 conformations for the
# terminal dihedral angle if you keep the default (30).
non_rotameric_sampling = 30 

# An initial pdf is generated for the non-rotameric chi angles. The pdf is
# smoothed with a circular gaussian function with following standard 
# deviation (in degrees)
non_rotameric_smooth_std = 10

def EvalMises(phi0, psi0, kappa, phi, psi):
  prefac = 1.0 / (4*np.pi*np.pi*math.pow(np.i0(kappa), 2))
  exponent = kappa*(np.add(np.cos(phi-phi0), np.cos(psi-psi0)))
  return prefac * np.exp(exponent)

def EstimateKappa(n_within_one_std, phi, psi, phi_data, psi_data):
  min_phi_diff = np.minimum(np.minimum(np.absolute(phi_data - phi), 
                                       np.absolute(phi_data - (phi + 2*np.pi))), 
                            np.absolute(phi_data - (phi - 2*np.pi)))
  min_psi_diff = np.minimum(np.minimum(np.absolute(psi_data - psi), 
                                       np.absolute(psi_data - (psi + 2*np.pi))), 
                            np.absolute(psi_data - (psi - 2*np.pi)))
  squared_radii = np.add(np.square(min_phi_diff), np.square(min_psi_diff))
  squared_radii = np.sort(squared_radii)
  # if theres simply not enough data, the returned kappe will be super small
  radius_at_n = 1000. 
  if n_within_one_std < len(squared_radii):
    radius_at_n = np.sqrt(squared_radii[n_within_one_std - 1])

  return 1.0 / (radius_at_n * radius_at_n)

def GetChiStats(current_phi, current_psi, phi, psi, chi, kappa):
  weights = EvalMises(current_phi, current_psi, kappa, 
                      phi, psi)
  weights /= np.sum(weights)
  # do the mean
  a = np.sum(np.multiply(np.sin(chi), weights))
  b = np.sum(np.multiply(np.cos(chi), weights))
  mean_value = np.arctan2(a, b)
  # do the std
  diffs = np.minimum(np.minimum(np.absolute(chi - mean_value), 
                                np.absolute(chi - (mean_value + 2*np.pi))), 
                     np.absolute(chi - (mean_value - 2*np.pi)))
  squared_std = np.sum(np.multiply(np.square(diffs), weights))

  return (mean_value, np.sqrt(squared_std))

def GetNonRotChiStats(current_phi, current_psi, phi, psi, chi, 
                      non_rotameric_sampling, symmetric, kappa,
                      smooth_std):
  # stupid check 
  if 180 % non_rotameric_sampling != 0:
    raise RuntimeError("180 must be dividable by non_rotameric_sampling!")

  n_sampled_rotamers = int(360 / non_rotameric_sampling)
  if symmetric:
    n_sampled_rotamers = int(180 / non_rotameric_sampling)

  # symmetric or not, we first fill in a histogram covering a range of 2 pi
  n_histogram_bins = 2 * 360 
  histogram = np.zeros(n_histogram_bins)
  bin_size = 2*np.pi/n_histogram_bins

  # we add up the histogram by weights as estimated with the mises distribution
  weights = EvalMises(current_phi, current_psi, kappa, 
                      phi, psi)
  angle_bins = np.clip(np.floor((chi + np.pi) / bin_size), 0, len(histogram)-1)
  for angle_bin, w in zip(angle_bins, weights):
    histogram[int(angle_bin)] += w

  # if we're dealing with a symmetric distribution, we simply add up the
  # first and second half of the histogram. The histogram has then an angle
  # range of [-pi,0.0[
  if symmetric:
    n_histogram_bins = int(n_histogram_bins / 2)
    histogram = np.add(histogram[:n_histogram_bins], 
                       histogram[n_histogram_bins:])

  # translate the smooth_std, which is in degree, to bins in the histogram
  # and apply it with a simple gaussian filter
  gaussian_std = float(smooth_std) / 180 * np.pi / bin_size
  histogram = gaussian_filter(histogram, gaussian_std, mode="wrap")
  histogram = histogram / sum(histogram)

  # find the maximum element, as this is the center of the first "window"
  # of width non_rotameric_sampling
  max_idx = np.argmax(histogram)
  current_angle = max_idx * bin_size - np.pi

  # concatenate, so we dont have to care for circularity anymore
  histogram = np.concatenate((histogram, histogram, histogram, histogram))
  
  rad_non_rotameric_sampling = float(non_rotameric_sampling) / 180 * np.pi
  first_start_angle = current_angle - rad_non_rotameric_sampling / 2

  if first_start_angle < -np.pi:
    # With this first angle we would get an underflow...
    # we did so many concatenatinons, that we can add 360 degrees, even for
    # the symmetric case
    current_angle += 2*np.pi

  probabilities = np.zeros(n_sampled_rotamers)
  mean_values = np.zeros(n_sampled_rotamers)
  std_values = np.zeros(n_sampled_rotamers)

  for i in range(n_sampled_rotamers):
    start_angle = current_angle - rad_non_rotameric_sampling / 2
    end_angle = current_angle + rad_non_rotameric_sampling / 2

    start_bin = int((start_angle + np.pi) / bin_size)
    end_bin = int((end_angle + np.pi) / bin_size)
    local_values = histogram[start_bin:end_bin+1]
    normalized_local_values = local_values / sum(local_values)
    angle_values = np.linspace(start_angle, end_angle, len(local_values))

    probabilities[i] = np.sum(local_values)
    mean_values[i] = np.sum(np.multiply(normalized_local_values, angle_values))
    squared_std = np.sum(np.multiply(np.square(angle_values - mean_values[i]), 
                                               normalized_local_values))
    std_values[i] = np.sqrt(squared_std)

    current_angle += rad_non_rotameric_sampling

  # The probabilities of all ranges should add up to one...
  # however, depending on the rounding etc., this might not exactly be the case.
  # All probabilities need an additional normalization in the end
  probabilities /= np.sum(probabilities)

  # due to the concatenation, we still have mean values > pi.
  # let's enforce valid ranges...
  for i in range(len(mean_values)):
    while mean_values[i] > np.pi:
      mean_values[i] -= 2*np.pi

  return probabilities, mean_values, std_values
  
def DoIt(aa_name, aa_data, n_bins, p_n_67, chi_n_67, out_path,
         non_rotameric_sampling=30, non_rotameric_smooth_std = 10):

  # output lines are added here
  out_stuff = list()

  # the configurations in aa_data are stored in four lists with keys 
  # conf1, conf2... we tranlate that to a unique string
  configurations = list()
  for a, b, c, d in zip(aa_data["conf1"], aa_data["conf2"], 
                        aa_data["conf3"], aa_data["conf4"]):
    configurations.append("%i%i%i%i"%(int(a), int(b), int(c), int(d)))
  unique_configurations = list(set(configurations))

  # we need configuration dependent phi, psi, chix
  conf_phi = dict()
  conf_psi = dict()
  conf_chi1 = dict()
  conf_chi2 = dict()
  conf_chi3 = dict()
  conf_chi4 = dict()

  # the actual output is stored in following configuration dependent matrices
  rotameric_probabilities = dict()
  rotameric_chi1 = dict()
  rotameric_chi2 = dict()
  rotameric_chi3 = dict()
  rotameric_chi4 = dict()
  rotameric_sig1 = dict()
  rotameric_sig2 = dict()
  rotameric_sig3 = dict()
  rotameric_sig4 = dict()

  for uc in unique_configurations:
    indices = [i for i, c in enumerate(configurations) if c == uc]
    conf_phi[uc] = aa_data["phi"][indices]
    conf_psi[uc] = aa_data["psi"][indices]
    conf_chi1[uc] = aa_data["chi1"][indices]
    conf_chi2[uc] = aa_data["chi2"][indices]
    conf_chi3[uc] = aa_data["chi3"][indices]
    conf_chi4[uc] = aa_data["chi4"][indices]
    rotameric_probabilities[uc] = np.zeros((n_bins, n_bins))
    rotameric_chi1[uc] = np.zeros((n_bins, n_bins))
    rotameric_chi2[uc] = np.zeros((n_bins, n_bins))
    rotameric_chi3[uc] = np.zeros((n_bins, n_bins))
    rotameric_chi4[uc] = np.zeros((n_bins, n_bins))
    rotameric_sig1[uc] = np.zeros((n_bins, n_bins))
    rotameric_sig2[uc] = np.zeros((n_bins, n_bins))
    rotameric_sig3[uc] = np.zeros((n_bins, n_bins))
    rotameric_sig4[uc] = np.zeros((n_bins, n_bins))

  ########################################
  # GET PHI/PSI DEPENDENT DATA SITUATION #
  ########################################
  # the data in n_data_points will be used to fill the second column in the
  # output file: number of data points for that backbone dihedral bin
  bin_size = 2*np.pi/n_bins
  n_data_points = np.zeros((n_bins, n_bins))
  for phi, psi in zip(aa_data["phi"], aa_data["psi"]):
    phi_bin = int((phi + np.pi) / bin_size)
    psi_bin = int((psi + np.pi) / bin_size)
    # enforce range (should be fulfilled anyway)
    phi_bin = max(min(phi_bin, n_bins-1), 0)
    psi_bin = max(min(psi_bin, n_bins-1), 0)
    # the bins are now just rounded down... if they're rounded by more than
    # half of the bin size, we count them to the bin after
    d_phi = (phi+np.pi) - phi_bin*bin_size
    d_psi = (psi+np.pi) - psi_bin*bin_size
    if d_phi > bin_size/2.:
      phi_bin += 1
    if d_psi > bin_size/2.:
      psi_bin += 1
    # deal with circularity
    if phi_bin == n_bins:
      phi_bin = 0
    if psi_bin == n_bins:
      psi_bin = 0
    n_data_points[(phi_bin, psi_bin)] += 1

  # Do not allow super large kappas. This happens in high density regions of
  # of the Ramachandran plot and given a binning of 10 degrees, only a fraction 
  # of the data is considered. This might lead to a bumpy probability landscape
  min_kappa = 0.1
  max_kappa = 1.0 / math.pow(bin_size / 2., 2)

  for i in range(n_bins):
    current_phi = -np.pi + i*(2*np.pi/n_bins)
    for j in range(n_bins):
      current_psi = -np.pi + j*(2*np.pi/n_bins)

      print("processing", aa_name, "phi:",current_phi,"psi:",current_psi)

      ###############################################################
      # GET THE PROBABILITIES OF ALL CONFIGURATIONS GIVEN PHI / PSI #
      ###############################################################
      conf_counter = dict()
      for uc in unique_configurations:
        conf_counter[uc] = 0.0

      # bb_kappa determines the smoothness of the mises distribution that
      # is used for the weighted counting approach to determine the 
      # probabilities of rotamer configurations
      bb_kappa = EstimateKappa(p_n_67, current_phi, current_psi, 
                               aa_data["phi"], aa_data["psi"])
      bb_kappa = max(min(bb_kappa, max_kappa), min_kappa)

      bb_weights = EvalMises(current_phi, current_psi, bb_kappa, 
                             aa_data["phi"], aa_data["psi"])
      sum_bb_weights = np.sum(bb_weights)
      for w, c in zip(bb_weights, configurations):
        conf_counter[c] += w 
      for uc in unique_configurations:
        rotameric_probabilities[uc][(i,j)] = conf_counter[uc] / sum_bb_weights

      #####################################################################
      # GET THE DIHEDRAL STATISTICS OF ALL CONFIGURATIONS GIVEN PHI / PSI #
      #####################################################################
      # non_rotameric_configurations remains empty for rotameric-only sidechains
      non_rotameric_configurations = dict()

      for uc in unique_configurations:
        # conf_kappa determines the smoothness of the mises distribution that
        # is used for the weighted mean approach to determine chi angles / stds
        conf_kappa = EstimateKappa(chi_n_67, current_phi, current_psi,
                                   conf_phi[uc], conf_psi[uc])
        conf_kappa = max(min(conf_kappa, max_kappa), min_kappa)
        rot_confs = [int(sidechain.TRANS), int(sidechain.GAUCHE_MINUS),
                     int(sidechain.GAUCHE_PLUS)] 
        # do chi1
        if int(uc[0]) in rot_confs:
          mean, std = GetChiStats(current_phi, current_psi, conf_phi[uc], 
                                  conf_psi[uc], conf_chi1[uc], conf_kappa)
          rotameric_chi1[uc][(i,j)] = mean
          rotameric_sig1[uc][(i,j)] = std
        # do chi2
        if int(uc[1]) in rot_confs:
          mean, std = GetChiStats(current_phi, current_psi, conf_phi[uc], 
                                  conf_psi[uc], conf_chi2[uc], conf_kappa)
          rotameric_chi2[uc][(i,j)] = mean
          rotameric_sig2[uc][(i,j)] = std
        # do chi3
        if int(uc[2]) in rot_confs:
          mean, std = GetChiStats(current_phi, current_psi, conf_phi[uc], 
                                  conf_psi[uc], conf_chi3[uc], conf_kappa)
          rotameric_chi3[uc][(i,j)] = mean
          rotameric_sig3[uc][(i,j)] = std
        # do chi4
        if int(uc[3]) in rot_confs:
          mean, std = GetChiStats(current_phi, current_psi, conf_phi[uc], 
                                  conf_psi[uc], conf_chi4[uc], conf_kappa)
          rotameric_chi4[uc][(i,j)] = mean
          rotameric_sig4[uc][(i,j)] = std

        # so far we only treated rotameric dihedral angles. 
        # ASN(chi2), ASP(chi2), PHE(chi2), TYR(chi2), HIS(chi2), TRP(chi2), 
        # GLN(chi3) and GLU(chi3) exhibit so called non-rotameric degrees
        # of freedom and require further sampling of their terminal 
        # dihedral angle.
        if aa_name in ["ASN", "ASP", "PHE", "TYR", "HIS", "TRP", "GLN", "GLU"]:
          non_rotameric_chi_idx = 1
          if aa_name in ["GLN", "GLU"]:
            non_rotameric_chi_idx = 2
          symmetric = False
          if aa_name in ["ASP", "PHE", "TYR", "GLU"]:
            symmetric = True
          chi_angles = None
          if non_rotameric_chi_idx == 1:
            chi_angles = conf_chi2[uc]
          elif non_rotameric_chi_idx == 2:
            chi_angles = conf_chi3[uc]

          non_rot_kappa = conf_kappa
          if symmetric:
            non_rot_kappa = EstimateKappa(int(chi_n_67/2), current_phi, 
                                          current_psi, conf_phi[uc], 
                                          conf_psi[uc])
            non_rot_kappa = max(min(non_rot_kappa, max_kappa), min_kappa)

          probs, mean_values, std_values = GetNonRotChiStats(current_phi, 
                                                      current_psi,
                                                      conf_phi[uc], 
                                                      conf_psi[uc],
                                                      chi_angles,
                                                      non_rotameric_sampling,
                                                      symmetric, 
                                                      non_rot_kappa,
                                                      non_rotameric_smooth_std)

          non_rotameric_configurations[uc] = (non_rotameric_chi_idx,
                                              probs, mean_values, std_values)
      # generate output
      for uc in unique_configurations:
        if uc in non_rotameric_configurations:
          for k in range(len(non_rotameric_configurations[uc][1])):
            c2 = str(uc[1])
            chi2 = rotameric_chi2[uc][(i,j)]
            sig2 = rotameric_sig2[uc][(i,j)]
            c3 = str(uc[2])            
            chi3 = rotameric_chi3[uc][(i,j)]
            sig3 = rotameric_sig3[uc][(i,j)]
            non_rot_idx = non_rotameric_configurations[uc][0]
            if non_rot_idx == 1:
              c2 = str(k+1)
              chi2 = non_rotameric_configurations[uc][2][k]
              sig2 = non_rotameric_configurations[uc][3][k]
            elif non_rot_idx == 2:
              c3 = str(k+1)
              chi3 = non_rotameric_configurations[uc][2][k]
              sig3 = non_rotameric_configurations[uc][3][k]
            else:
              raise RuntimeError("Invalid non rot idx!")

            out = list()
            out.append(aa_name)
            out.append(str(-180 + i*10))
            out.append(str(-180 + j*10))
            out.append(str(int(n_data_points[(i,j)])))
            out.append(' '.join([str(uc[0]), c2, c3, str(uc[3])]))
            out.append("%.4f"%(non_rotameric_configurations[uc][1][k] * \
                               rotameric_probabilities[uc][(i,j)]))
            out.append("%.2f"%(rotameric_chi1[uc][(i,j)] / np.pi * 180))
            out.append("%.2f"%(chi2 / np.pi * 180))
            out.append("%.2f"%(chi3 / np.pi * 180))
            out.append("%.2f"%(rotameric_chi4[uc][(i,j)] / np.pi * 180))
            out.append("%.2f"%(rotameric_sig1[uc][(i,j)] / np.pi * 180))
            out.append("%.2f"%(sig2 / np.pi * 180))
            out.append("%.2f"%(sig3 / np.pi * 180))
            out.append("%.2f"%(rotameric_sig4[uc][(i,j)] / np.pi * 180))
            outline = ' '.join(out)
            out_stuff.append(outline)

        else:
          out = list()
          out.append(aa_name)
          out.append(str(-180 + i*10))
          out.append(str(-180 + j*10))
          out.append(str(int(n_data_points[(i,j)])))
          out.append(' '.join([str(uc[0]), str(uc[1]), str(uc[2]), str(uc[3])]))
          out.append("%.4f"%(rotameric_probabilities[uc][(i,j)]))
          out.append("%.2f"%(rotameric_chi1[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_chi2[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_chi3[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_chi4[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_sig1[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_sig2[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_sig3[uc][(i,j)] / np.pi * 180))
          out.append("%.2f"%(rotameric_sig4[uc][(i,j)] / np.pi * 180))
          outline = ' '.join(out)
          out_stuff.append(outline)

  outfile = open(out_path, 'w')
  outfile.write('\n'.join(out_stuff))
  outfile.close()

# read csv file (generated with fetch_data.py)
data = open(data_file_path, 'r').readlines()

# get data indices
header = [item.strip() for item in data[0].split(',')]

# will throw an error if not there
rname_idx = header.index("rname")
phi_idx = header.index("phi")
psi_idx = header.index("psi")
chi1_idx = header.index("chi1")
chi2_idx = header.index("chi2")
chi3_idx = header.index("chi3")
chi4_idx = header.index("chi4")
conf1_idx = header.index("conf1")
conf2_idx = header.index("conf2")
conf3_idx = header.index("conf3")
conf4_idx = header.index("conf4")

# helpers for parsing...
str_c = ["GAUCHE_MINUS", "GAUCHE_PLUS", "TRANS", "NON_ROTAMERIC", "INVALID"]
promod_c = [sidechain.GAUCHE_MINUS, sidechain.GAUCHE_PLUS, sidechain.TRANS, 
            sidechain.NON_ROTAMERIC, sidechain.INVALID]

# get the data
aa_data = {"phi" : [], "psi" : [], "chi1" : [], "chi2" : [], "chi3" : [], 
           "chi4" : [], "conf1" : [], "conf2" : [], "conf3" : [], "conf4" : []}

for line in data[1:]:
  split_line = [item.strip() for item in line.split(',')]
  rname = split_line[rname_idx]
  if rname == aa_name:
    aa_data["phi"].append(float(split_line[phi_idx]))
    aa_data["psi"].append(float(split_line[psi_idx]))
    aa_data["chi1"].append(float(split_line[chi1_idx]))
    aa_data["chi2"].append(float(split_line[chi2_idx]))
    aa_data["chi3"].append(float(split_line[chi3_idx]))
    aa_data["chi4"].append(float(split_line[chi4_idx]))
    aa_data["conf1"].append(promod_c[str_c.index(split_line[conf1_idx])])
    aa_data["conf2"].append(promod_c[str_c.index(split_line[conf2_idx])])
    aa_data["conf3"].append(promod_c[str_c.index(split_line[conf3_idx])])
    aa_data["conf4"].append(promod_c[str_c.index(split_line[conf4_idx])])

# convert all dihedral angles into numpy arrays
aa_data["phi"] = np.array(aa_data["phi"])
aa_data["psi"] = np.array(aa_data["psi"])
aa_data["chi1"] = np.array(aa_data["chi1"])
aa_data["chi2"] = np.array(aa_data["chi2"])
aa_data["chi3"] = np.array(aa_data["chi3"])
aa_data["chi4"] = np.array(aa_data["chi4"])

###############################################
# GENERATE THE LIBRARY FOR DESIRED AMINO ACID #
###############################################
DoIt(aa_name, aa_data, n_bins, p_threshold_67, chi_threshold_67,
     aa_name+".txt", non_rotameric_sampling=non_rotameric_sampling, 
     non_rotameric_smooth_std = non_rotameric_smooth_std)
