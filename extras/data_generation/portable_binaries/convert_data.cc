// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


// Test code for promod3

#include <promod3/loop/loop_object_loader.hh>
#include <promod3/sidechain/sidechain_object_loader.hh>
#include <promod3/core/check_io.hh>
#include <promod3/config.hh>
#include <ost/base.hh>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <time.h>

// check if content in two streams is binary equal
template <typename STREAM>
void check_binary_equal(STREAM& in_stream, STREAM& in_stream_ref) {
  // buffered read (MUCH FASTER)
  const unsigned int BUF_SIZE = 2048;
  int file_len = 0;
  bool more_to_read = true;
  while (more_to_read) {
    // try to read both files
    char c[BUF_SIZE];
    char c2[BUF_SIZE];
    in_stream.read(c, BUF_SIZE);
    in_stream_ref.read(c2, BUF_SIZE);
    // check if we're done
    more_to_read = in_stream && in_stream_ref;
    int len_buf = BUF_SIZE;
    if (!more_to_read) {
      if (in_stream.gcount() < in_stream_ref.gcount()) {
        std::cout << "FAIL! New File is shorter!" << std::endl;
      }
      if (in_stream.gcount() > in_stream_ref.gcount()) {
        std::cout << "FAIL! Ref. File is shorter!" << std::endl;
      }
      len_buf = in_stream.gcount();
    }
    // compare buffers
    for (int i = 0; i < len_buf; ++i) {
      if (c[i] != c2[i]) {
        std::cout << "FAIL! Value mismatch at byte " << file_len+i << std::endl;
        abort();
      }
    }
    // update file_len
    file_len += len_buf;
  }
  std::cout << "Read " << file_len << " bytes. All good!" << std::endl;
}

void convert_torsion_sampler(String file_name) {
  // convert it
  uint seed = 0;
  String path_ref = promod3::GetProMod3BinaryPath("loop_data", file_name);
  promod3::loop::TorsionSamplerPtr p = promod3::loop::TorsionSampler::Load(path_ref, seed);
  String path_new = file_name;
  p->Save(path_new);
  // check them
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  // skip headers
  promod3::core::CheckMagicNumber(in_stream);
  uint32_t version = promod3::core::GetVersionNumber(in_stream);
  // required types: int, uint, char
  promod3::core::CheckTypeSize<int>(in_stream);
  promod3::core::CheckTypeSize<uint>(in_stream);
  promod3::core::CheckBaseType<int>(in_stream);
  promod3::core::CheckBaseType<uint>(in_stream);
  promod3::core::CheckBaseType<char>(in_stream);
  // compare
  check_binary_equal(in_stream, in_stream_ref);
}

void convert_frag_db(String file_name) {
  // convert it
  String path_ref = promod3::GetProMod3BinaryPath("loop_data", file_name);
  promod3::loop::FragDBPtr p = promod3::loop::FragDB::Load(path_ref);
  String path_new = file_name;
  p->Save(path_new);
  // check them
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  // skip header
  promod3::core::CheckMagicNumber(in_stream);
  uint32_t version = promod3::core::GetVersionNumber(in_stream);
  // see Save for required types...
  typedef promod3::loop::FragDB::FragmentData::BagInfo BI;
  typedef std::pair<promod3::loop::StemPairGeom,BI> MyPair;
  promod3::core::CheckTypeSize<int>(in_stream);
  promod3::core::CheckTypeSize<Real>(in_stream);
  promod3::core::CheckTypeSize<uint>(in_stream);
  promod3::core::CheckTypeSize<char>(in_stream);
  promod3::core::CheckTypeSize<unsigned int>(in_stream);
  promod3::core::CheckTypeSize<unsigned short>(in_stream);
  promod3::core::CheckTypeSize<MyPair>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::StemPairGeom>(in_stream);
  promod3::core::CheckTypeSize<BI>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::FragmentInfo>(in_stream);

  promod3::core::CheckBaseType<int>(in_stream);
  promod3::core::CheckBaseType<Real>(in_stream);
  promod3::core::CheckBaseType<uint>(in_stream);
  promod3::core::CheckBaseType<char>(in_stream);
  promod3::core::CheckBaseType<unsigned int>(in_stream);
  promod3::core::CheckBaseType<unsigned short>(in_stream);

  // compare
  check_binary_equal(in_stream, in_stream_ref);
}

void convert_structure_db(String path_ref, String path_new) {
  // convert it
  promod3::loop::StructureDBPtr p = promod3::loop::StructureDB::Load(path_ref, true);
  p->Save(path_new);
  // check them
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  // skip header
  promod3::core::CheckMagicNumber(in_stream);
  uint32_t version = promod3::core::GetVersionNumber(in_stream);
  // see Save for required types...
  typedef ost::FixedString<5> MyString;
  promod3::core::CheckTypeSize<uint>(in_stream);
  promod3::core::CheckTypeSize<Real>(in_stream);
  promod3::core::CheckTypeSize<unsigned short>(in_stream);
  promod3::core::CheckTypeSize<uint64_t>(in_stream);
  promod3::core::CheckTypeSize<short>(in_stream);
  promod3::core::CheckTypeSize<MyString>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::CoordInfo>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::Peptide>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::AAFreq>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::PeptideCoords>(in_stream);
  promod3::core::CheckTypeSize<promod3::UShortVec>(in_stream);
  promod3::core::CheckTypeSize<promod3::loop::DihedralInfo>(in_stream);

  promod3::core::CheckBaseType<uint>(in_stream);
  promod3::core::CheckBaseType<Real>(in_stream);
  promod3::core::CheckBaseType<char>(in_stream);
  promod3::core::CheckBaseType<unsigned short>(in_stream);
  promod3::core::CheckBaseType<uint64_t>(in_stream);
  promod3::core::CheckBaseType<short>(in_stream);
  // compare
  check_binary_equal(in_stream, in_stream_ref);
}

void convert_structure_db(String file_name) {
  String path_ref = promod3::GetProMod3BinaryPath("loop_data", file_name);
  String path_new = file_name;
  convert_structure_db(path_ref, path_new);
}

void convert_BBDepRotamerLib(String file_name) {
  String path_ref = promod3::GetProMod3BinaryPath("sidechain_data", file_name);
  String path_new = file_name;
  // convert it
  promod3::sidechain::BBDepRotamerLibPtr p = promod3::sidechain::BBDepRotamerLib::Load(path_ref);
  p->Save(path_new);
  // check them
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  // skip header
  promod3::core::CheckMagicNumber(in_stream);
  uint32_t version = promod3::core::GetVersionNumber(in_stream);
  // see Save for required types...
  promod3::core::CheckTypeSize<uint>(in_stream);
  promod3::core::CheckTypeSize<bool>(in_stream);
  promod3::core::CheckTypeSize<uint64_t>(in_stream);
  promod3::core::CheckTypeSize<Real>(in_stream);
  promod3::core::CheckTypeSize<promod3::sidechain::RotamerID>(in_stream);
  promod3::core::CheckTypeSize<promod3::sidechain::RotamerLibEntry>(in_stream);

  promod3::core::CheckBaseType<uint>(in_stream);
  promod3::core::CheckBaseType<bool>(in_stream, true);
  promod3::core::CheckBaseType<uint64_t>(in_stream);
  promod3::core::CheckBaseType<Real>(in_stream);
  promod3::core::CheckBaseType<promod3::sidechain::RotamerID>(in_stream, promod3::sidechain::ASP);

  // compare
  check_binary_equal(in_stream, in_stream_ref);
}

void convert_RotamerLib(String file_name) {
  String path_ref = promod3::GetProMod3BinaryPath("sidechain_data", file_name);
  String path_new = file_name;
  // convert it
  promod3::sidechain::RotamerLibPtr p = promod3::sidechain::RotamerLib::Load(path_ref);
  p->Save(path_new);
  // check them
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  // skip header
  promod3::core::CheckMagicNumber(in_stream);
  uint32_t version = promod3::core::GetVersionNumber(in_stream);
  // see Save for required types...
  promod3::core::CheckTypeSize<uint>(in_stream);
  promod3::core::CheckTypeSize<uint64_t>(in_stream);
  promod3::core::CheckTypeSize<Real>(in_stream);
  promod3::core::CheckTypeSize<promod3::sidechain::RotamerID>(in_stream);
  promod3::core::CheckTypeSize<promod3::sidechain::RotamerLibEntry>(in_stream);

  promod3::core::CheckBaseType<uint>(in_stream);
  promod3::core::CheckBaseType<uint64_t>(in_stream);
  promod3::core::CheckBaseType<Real>(in_stream);
  promod3::core::CheckBaseType<promod3::sidechain::RotamerID>(in_stream, promod3::sidechain::ASP);

  // compare
  check_binary_equal(in_stream, in_stream_ref);
}

void portable_torsion_sampler(String file_name, String file_name_portable) {
  // convert it
  uint seed = 0;
  String path_ref = promod3::GetProMod3BinaryPath("loop_data", file_name);
  promod3::loop::TorsionSamplerPtr p = promod3::loop::TorsionSampler::Load(path_ref, seed);
  String path_portable = file_name_portable;
  p->SavePortable(path_portable);
  // convert back
  p = promod3::loop::TorsionSampler::LoadPortable(path_portable, seed);
  String path_new = file_name;
  p->Save(path_new);
  // binary check
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  check_binary_equal(in_stream, in_stream_ref);
  // proper check
  p = promod3::loop::TorsionSampler::Load(path_new, seed);
  promod3::loop::TorsionSamplerPtr p_ref = promod3::loop::TorsionSampler::Load(path_ref, seed);
  if (*p == *p_ref) {
    std::cout << "All good!\n";
  } else {
    std::cout << "Object mismatch!\n";
  }
}

void portable_frag_db(String file_name, String file_name_portable) {
  // convert it
  String path_ref = promod3::GetProMod3BinaryPath("loop_data", file_name);
  String path_portable = file_name_portable;
  String path_new = file_name;
  promod3::loop::FragDBPtr p;

  std::cout << "Loading " << path_ref << std::endl;
  clock_t tS = clock();
  p = promod3::loop::FragDB::Load(path_ref);
  clock_t tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;

  std::cout << "Storing " << path_portable << std::endl;
  tS = clock();
  p->SavePortable(path_portable);
  tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;

  // convert back
  std::cout << "Loading " << path_portable << std::endl;
  tS = clock();
  p = promod3::loop::FragDB::LoadPortable(path_portable);
  tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;

  std::cout << "Storing " << path_new << std::endl;
  tS = clock();
  p->Save(path_new);
  tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;

  // check them
  p = promod3::loop::FragDB::Load(path_new);
  //p = promod3::loop::FragDB::LoadPortable(path_portable);
  promod3::loop::FragDBPtr p_ref = promod3::loop::FragDB::Load(path_ref);
  if (*p == *p_ref) {
    std::cout << "All good!\n";
  } else {
    std::cout << "Object mismatch!\n";
  }
  p->SetReadOnly(false);
  if (*p == *p_ref) {
    std::cout << "Objects shouldn't match :o!\n";
  } else {
    std::cout << "All good!\n";
  }
  p_ref->SetReadOnly(false);
  if (*p == *p_ref) {
    std::cout << "All good!\n";
  } else {
    std::cout << "Object mismatch!\n";
  }

  // sanity check
  //promod3::loop::FragDB f(1,2);
  promod3::loop::FragDB f(p->GetDistBinSize(), p->GetAngularBinSize());
  if (*p == f) {
    std::cout << "Objects shouldn't match :o!\n";
  } else {
    std::cout << "All good!\n";
  }
}

void portable_structure_db(String path_ref, String path_new,
                           String path_portable) {
  // convert it
  promod3::loop::StructureDBPtr p;
  std::cout << "Loading " << path_ref << std::endl;
  clock_t tS = clock();
  p = promod3::loop::StructureDB::Load(path_ref, true);
  clock_t tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;
  std::cout << "Storing " << path_portable << std::endl;
  tS = clock();
  p->SavePortable(path_portable);
  tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;
  // convert back
  std::cout << "Loading " << path_portable << std::endl;
  tS = clock();
  p = promod3::loop::StructureDB::LoadPortable(path_portable);
  tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;
  std::cout << "Storing " << path_new << std::endl;
  tS = clock();
  p->Save(path_new);
  tE = clock();
  std::cout << "Elapsed: " << (tE - tS)/(double)CLOCKS_PER_SEC << std::endl;

  // check them
  p = promod3::loop::StructureDB::Load(path_new, true);
  //p = promod3::loop::StructureDB::LoadPortable(path_portable);
  promod3::loop::StructureDBPtr p_ref = promod3::loop::StructureDB::Load(path_ref, true);
  if (*p == *p_ref) {
    std::cout << "All good!\n";
  } else {
    std::cout << "Object mismatch!\n";
  }
}

void portable_structure_db(String file_name, String file_name_portable) {
  String path_ref = promod3::GetProMod3BinaryPath("loop_data", file_name);
  portable_structure_db(path_ref, file_name, file_name_portable);
}

void portable_BBDepRotamerLib(String file_name, String file_name_portable) {
  // convert it
  String path_ref = promod3::GetProMod3BinaryPath("sidechain_data", file_name);
  promod3::sidechain::BBDepRotamerLibPtr p = promod3::sidechain::BBDepRotamerLib::Load(path_ref);
  String path_portable = file_name_portable;
  p->SavePortable(path_portable);
  // convert back
  p = promod3::sidechain::BBDepRotamerLib::LoadPortable(path_portable);
  String path_new = file_name;
  p->Save(path_new);
  // binary check
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  check_binary_equal(in_stream, in_stream_ref);
}

void portable_RotamerLib(String file_name, String file_name_portable) {
  // convert it
  String path_ref = promod3::GetProMod3BinaryPath("sidechain_data", file_name);
  promod3::sidechain::RotamerLibPtr p = promod3::sidechain::RotamerLib::Load(path_ref);
  String path_portable = file_name_portable;
  p->SavePortable(path_portable);
  // convert back
  p = promod3::sidechain::RotamerLib::LoadPortable(path_portable);
  String path_new = file_name;
  p->Save(path_new);
  // binary check
  std::ifstream in_stream_ref(path_ref.c_str(), std::ios::binary);
  std::ifstream in_stream(path_new.c_str(), std::ios::binary);
  check_binary_equal(in_stream, in_stream_ref);
}

int main() {
  /////////////////////////////////////////////////////////////
  // results are stored in local path
  /////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////
  // ADD HEADERS
  // ASSUME: new Save, old Load
  /////////////////////////////////////////////////////////////
  // convert_torsion_sampler("torsion_sampler.dat");
  // convert_torsion_sampler("torsion_sampler_helical.dat");
  // convert_torsion_sampler("torsion_sampler_extended.dat");
  // convert_torsion_sampler("torsion_sampler_coil.dat");
  // convert_frag_db("frag_db.dat");
  // convert_structure_db("structure_db.dat");
  // convert_structure_db("structure_db_small.dat", "structure_db_new.dat");
  // convert_BBDepRotamerLib("bb_dep_lib.dat");
  // convert_RotamerLib("lib.dat");

  /////////////////////////////////////////////////////////////
  // MAKE PORTABLE VERSIONS
  // ASSUME: raw binaries readable and in stage folder
  /////////////////////////////////////////////////////////////
  portable_torsion_sampler("torsion_sampler.dat",
                           "p_data/portable_torsion_sampler.dat");
  portable_torsion_sampler("torsion_sampler_helical.dat",
                           "p_data/portable_torsion_sampler_helical.dat");
  portable_torsion_sampler("torsion_sampler_extended.dat",
                           "p_data/portable_torsion_sampler_extended.dat");
  portable_torsion_sampler("torsion_sampler_coil.dat",
                           "p_data/portable_torsion_sampler_coil.dat");
  portable_frag_db("frag_db.dat",
                   "p_data/portable_frag_db.dat");
  portable_structure_db("structure_db.dat",
                        "p_data/portable_structure_db.dat");
  portable_structure_db("src_data/structure_db_small.dat",
                        "structure_db_new.dat",
                        "p_data/portable_structure_db_small.dat");
  portable_BBDepRotamerLib("bb_dep_lib.dat",
                           "p_data/portable_bb_dep_lib.dat");
  portable_RotamerLib("lib.dat",
                      "p_data/portable_lib.dat");

  return 0;
}
