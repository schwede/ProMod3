import argparse
from ost import io, seq, PushVerbosityLevel, LogInfo
from ost.bindings import hhblits
from promod3 import loop, modelling

desc = "Build model given an alignment and template structure as it is done "\
       "for the best single template predictor in CAMEO. Using a sequence "\
       "profile is optional but recommended. For the sequence profile you "\
       "can either provide a hhm file that is consistent with the target "\
       "sequence or provide the path to HHblits (2.0.16) as well as the "\
       "required NR20 sequence database."
tpl_help = "Template structure in PDB format. The tpl must contain one chain "\
           "and the residues in the chain must match the template sequence in "\
           "the alignment."
aln_help = "Alignment in FASTA format with first sequence being the target "\
           "sequence and second sequence the template sequence."
outfile_help = "Path to store the model in PDB format (default: \"model.pdb\")"
hhsuite_path_help = "Path to HHblits installation (version 2.0.14)."
nrdb_help = "Path to NR20 sequence database for sequence profile creation."
profile_help = "Path to sequence profile in hhm format. The sequence in the "\
               "profile must be consistent with the target sequence in the "\
               "alignment."

parser = argparse.ArgumentParser(description=desc)
parser.add_argument("template", type = str, help = tpl_help)
parser.add_argument("alignment", type = str, help = aln_help)
parser.add_argument("-o", "--outfile", type = str, help=outfile_help, 
                    default="model.pdb")
parser.add_argument("-r", "--hhsuite_root_dir", type=str, 
                    help = hhsuite_path_help)
parser.add_argument("-n", "--nrdb", type = str, help = nrdb_help)
parser.add_argument("-p", "--profile", type = str, help = profile_help)
parser.add_argument("-v", "--verbose", action="store_true")
args = parser.parse_args()

def _BuildModelTerminiFraggers(mhandle, fragger_handles):
 
    # SETUP
    fragment_db = loop.LoadFragDB()
    structure_db = loop.LoadStructureDB()
    torsion_sampler = loop.LoadTorsionSamplerCoil()
    merge_distance = 4
 
    # CLOSE ALL GAPS
    modelling.CloseGaps(mhandle, merge_distance, fragment_db, structure_db,
                        torsion_sampler, fragger_handles)
 
    # MODEL TERMINI
    modelling.ModelTermini(mhandle, torsion_sampler, fragger_handles)
    modelling.RemoveTerminalGaps(mhandle)  # length=1 ignored above
 
    # BUILD SIDECHAINS
    modelling.BuildSidechains(mhandle, merge_distance, fragment_db,
                              structure_db, torsion_sampler)
 
    # MINIMIZE ENERGY OF FINAL MODEL USING MOLECULAR MECHANICS
    modelling.MinimizeModelEnergy(mhandle)
 
    # SANITY CHECKS
    modelling.CheckFinalModel(mhandle)
 
    # DONE
    return mhandle.model

def _ExecuteProMod3(tpl, aln, profile, psipred_pred):
    aln.AttachView(1, tpl.CreateFullView())
    mhandle = modelling.BuildRawModel(aln)
    if profile:
        modelling.SetSequenceProfiles(mhandle, [profile])
    fragger_handle = modelling.FraggerHandle(aln.GetSequence(0).gapless_string,
                                             profile, psipred_pred, 
                                             rmsd_thresh = 0.02)
    fragger_handles = [fragger_handle]
    return _BuildModelTerminiFraggers(mhandle, fragger_handles)

if args.verbose:
    PushVerbosityLevel(3)

# GET ALIGNMENT
aln = io.LoadAlignment(args.alignment)
if aln.GetCount() != 2:
    raise RuntimeError("Expect exactly two sequences in alignment")

# GET TEMPLATE
tpl = io.LoadPDB(args.template)
if tpl.GetChainCount() != 1:
    raise RuntimeError("Expect exactly one chain in template structure")

# SETUP SEQUENCE PROFILE AND PSIPRED PREDICTION IF PROVIDED
profile_file = None
if args.profile:
    LogInfo("Use provided sequence profile: %s" % (args.profile))
    profile_file = args.profile
elif args.hhsuite_root_dir and args.nrdb:
    LogInfo("Generate sequence profile with hhblits located at %s and nrdb "\
            "located at %s" % (args.hhsuite_root_dir, args.nrdb))
    query = seq.CreateSequence("query", aln.GetSequence(0).gapless_string)
    hh = hhblits.HHblits(query, args.hhsuite_root_dir)
    a3m_file = hh.BuildQueryMSA(nrdb=args.nrdb)
    profile_file = hh.A3MToProfile(a3m_file)
else:
    LogInfo("Run without sequence profile. This is not recommended.")
    
profile = None
psipred_pred = None
if profile_file:
    profile = io.LoadSequenceProfile(profile_file)
    if profile.sequence != aln.GetSequence(0).GetGaplessString():
        raise RuntimeError("Profile sequence must be consistent with target "
                           "sequence!")
    psipred_pred = loop.PsipredPrediction.FromHHM(profile_file)

# GOGOGO
model = _ExecuteProMod3(tpl, aln, profile, psipred_pred)

# DUMP THE MODEL TO DISK
io.SavePDB(model, args.outfile)

