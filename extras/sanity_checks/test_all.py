# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
import unittest
import subprocess
import promod3
from ost import io

# get pm-binary from command line
if len(sys.argv) > 1:
    pm_bin = sys.argv[1]
else:
    pm_bin = 'pm'

# helper
def run_me(cmd):
    """Run cmd (list of tokens). Returns tuple (return-code, stdout, stderr)."""
    job = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sout = job.stdout.decode()
    serr = job.stderr.decode()
    print('RUN', cmd, 'return code', job.returncode)
    if sout:
        print('STDOUT\n%s' % sout.strip())
    if serr:
        print('STDERR\n%s' % serr.strip())
    return job.returncode, sout, serr

# get own path
my_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(my_path)

# quick sanity check
print('-------------- quick sanity check -------------- ')
assert(len(promod3.__version__) > 0)
assert(os.path.exists(promod3.GetProMod3SharedDataPath()))
rcode, sout, serr = run_me([pm_bin, 'test_module.py'])
assert(rcode == 0)

# run a pipeline
print('-------------- run a pipeline -------------- ')
rcode, sout, serr = run_me([pm_bin, 'run_pipeline.py'])
assert(rcode == 0)
# check that result exists and is readable
io.LoadPDB('model.pdb')
# clean up
os.remove('model.pdb')

# run doctests
print('-------------- run doctests -------------- ')
sys.path.insert(0, my_path)
from test_doctests import DocTests
DocTests.setPmBinary(pm_bin)
testnames = unittest.TestLoader().getTestCaseNames(DocTests)
suite = unittest.TestSuite()
for name in testnames:
    # filter tests meant for developpers
    if name not in ['testAction', 'testActionVerbose']:
        suite.addTest(DocTests(name))
res = unittest.TextTestRunner(verbosity=2).run(suite)
assert(res.wasSuccessful())

# check C++ interface
print('-------------- check C++ interface -------------- ')
rcode, sout, serr = run_me(['make', 'clean'])
assert(rcode == 0)
rcode, sout, serr = run_me(['make', 'run'])
assert(rcode == 0)
# check that result exists and is readable
io.LoadPDB('test.pdb')
# clean up
os.remove('test.pdb')
rcode, sout, serr = run_me(['make', 'clean'])
