// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


// Test code for low-level ProMod3 features

#include <ost/base.hh>
#include <ost/io/mol/pdb_writer.hh>
#include <promod3/config.hh>
#include <promod3/loop/backbone.hh>
#include <iostream>

int main() {
  std::cout << "Running ProMod3 testcode" << std::endl;

  std::cout << "ProMod3-version = " << PROMOD3_VERSION_STRING << std::endl;
  std::cout << "ProMod3SharedDataPath = " << promod3::GetProMod3SharedDataPath() << std::endl;

  String sequence = "HELLYEAH";
  promod3::loop::BackboneList bb_list(sequence);
  ost::io::PDBWriter writer(String("test.pdb"), ost::io::IOProfile());
  writer.Write(bb_list.ToEntity());

  return 0;
}