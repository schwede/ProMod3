# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import ost
from ost import io, mol
from promod3 import modelling
import time

# setup logging (level 3 = up to LogInfo)
ost.PushVerbosityLevel(3)

# get data
aln = io.LoadAlignment("aln.fasta")
template = io.LoadPDB("template.pdb")
# add sec. structure
mol.alg.AssignSecStruct(template)
# get raw model
aln.AttachView(1,template.Select('peptide=true'))
mhandle = modelling.BuildRawModel(aln)

# build final model
start = time.time()
model = modelling.BuildFromRawModel(mhandle)
print("\n----\nBuildFromRawModel took: ",time.time()-start)
io.SavePDB(model, "model.pdb")
