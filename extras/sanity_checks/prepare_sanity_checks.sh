#!/bin/bash

# check inputs
PARAM=$#
if [ $PARAM -lt 2 ] || [ $PARAM -gt 2 ]; then
  echo 'Usage: "prepare_sanity_checks.sh GIT_ROOT TRG_PATH", where:'
  echo "  GIT_ROOT = root path of ProMod3 git repository (containing doc/tests folder)"
  echo "  TRG_PATH = target path, which is created and contains tests for installation"
  exit 1
fi

# setup
GIT_ROOT=$1
TRG_PATH=$2
mkdir -p $TRG_PATH

# copy this
rsync -az --no-perms --delete "$GIT_ROOT/extras/sanity_checks/" "$TRG_PATH"
# copy doctests
rsync -az --exclude="CMakeLists.txt" --no-perms \
      "$GIT_ROOT/doc/tests/" "$TRG_PATH"
# done
