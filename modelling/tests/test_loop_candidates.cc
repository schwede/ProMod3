// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <promod3/modelling/loop_candidate.hh>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <ost/conop/heuristic.hh>
#include <ost/io/mol/pdb_reader.hh>
#include <ost/io/seq/hhm_io_handler.hh>
#include <ost/io/seq/load.hh>
#include <ost/io/seq/pssm_io_handler.hh>
#include <ost/mol/builder.hh>
#include <ost/seq/profile_handle.hh>
#include <promod3/core/geom_stems.hh>
#include <promod3/core/message.hh>
#include <promod3/loop/loop_object_loader.hh>
#include <algorithm>

BOOST_AUTO_TEST_SUITE( modelling );

using namespace promod3;
using namespace promod3::modelling;

namespace {

/////////////////////////////////////////////////////////////////////
// FOR DEBUGGING
/////////////////////////////////////////////////////////////////////
/*bool IsWithinBounds(const loop::BackboneList& bb_list,
                    const core::StemPairOrientation& stem_o,
                    Real max_dist_diff, Real max_ang_dist) {
  // get data
  const uint last_idx = bb_list.size() - 1;
  core::StemPairOrientation bb_o(bb_list.GetN(0), bb_list.GetCA(0),
                                 bb_list.GetC(0), bb_list.GetN(last_idx),
                                 bb_list.GetCA(last_idx),
                                 bb_list.GetC(last_idx));
  return (   std::abs(bb_o.distance - stem_o.distance) <= max_dist_diff
          && std::abs(bb_o.angle_one - stem_o.angle_one) <= max_ang_dist
          && std::abs(bb_o.angle_two - stem_o.angle_two) <= max_ang_dist
          && std::abs(bb_o.angle_three - stem_o.angle_three) <= max_ang_dist
          && std::abs(bb_o.angle_four - stem_o.angle_four) <= max_ang_dist);
}

void AreWithinBounds(const LoopCandidates& loop_candidates,
                     const loop::FragDBPtr& frag_db,
                     ost::mol::ResidueHandle n_stem,
                     ost::mol::ResidueHandle c_stem) {
  // SETUP
  Real dist_bin_size = frag_db->GetDistBinSize();
  Real ang_bin_size = (frag_db->GetAngularBinSize()*Real(M_PI)) / Real(180);
  std::cout << "BIN-SIZES: " << dist_bin_size << ", " << ang_bin_size << std::endl;
  core::StemPairOrientation stem_o(n_stem, c_stem);
  for (uint i = 0; i < loop_candidates.size(); ++i) {
    const bool check = IsWithinBounds(loop_candidates[i], stem_o,
                                      dist_bin_size, ang_bin_size);
    std::cout << "CANDIDATE " << i;
    if (check) std::cout << ": IsWithinBounds\n";
    else       std::cout << ": IsNotWithinBounds\n";
  }
}*/

/*void ShowFragments(const LoopCandidates& loop_candidates) {
  std::cout << "FRAGMENTS " << loop_candidates.size() << ":\n";
  for (uint i = 0; i < loop_candidates.size(); ++i) {
    const loop::FragmentInfo& f = loop_candidates.GetFragmentInfo(i);
    std::cout << "- FragmentInfo(" << f.chain_index << ", "
              << f.offset << ", "  << f.length << ")\n";
  }
  std::cout << std::endl;
}*/

/*void ShowIndices(const std::vector<uint>& orig_indices) {
  std::cout << "INDICES " << orig_indices.size() << ": ";
  for (uint i = 0; i < orig_indices.size(); ++i) {
    std::cout << orig_indices[i] << " ";
  }
  std::cout << std::endl;
}*/

/*void ShowClusters(const std::vector< std::vector<uint> >& clusters) {
  std::cout << "CLUSTERS " << clusters.size() << std::endl;
  for (uint i = 0; i < clusters.size(); ++i) {
    std::cout << "- " << i << ": ";
    for (uint j = 0; j < clusters[i].size(); ++j) {
      std::cout << clusters[i][j] << " ";
    }
    std::cout << std::endl;
  }
}*/

/////////////////////////////////////////////////////////////////////
// FOR TESTING
/////////////////////////////////////////////////////////////////////
ost::mol::EntityHandle LoadTestStructure(const String& pdb_name) {
  ost::io::PDBReader reader(pdb_name, ost::io::IOProfile());
  ost::mol::EntityHandle test_ent = ost::mol::CreateEntity();
  reader.Import(test_ent);
  ost::conop::HeuristicProcessor heu_proc;
  heu_proc.Process(test_ent);

  return test_ent;
}

LoopCandidatesPtr
FillFromFragments(const std::vector<loop::FragmentInfo>& fragments,
                  const ost::mol::ResidueHandle& n_stem,
                  const ost::mol::ResidueHandle& c_stem, const String& seq,
                  loop::StructureDBPtr structure_db) {
  // setup candidates
  LoopCandidatesPtr candidates(new LoopCandidates(seq));
  // get BBs and add to LC
  for (uint i = 0; i < fragments.size(); ++i) {
    loop::BackboneList bb_list;
    structure_db->FillBackbone(bb_list, n_stem, c_stem, fragments[i], seq);
    candidates->Add(bb_list);
    candidates->AddFragmentInfo(fragments[i]);
  }
  return candidates;
}

void CheckOrigIndices(const LoopCandidates& lc,
                      const std::vector<uint>& orig_indices,
                      const std::vector<loop::FragmentInfo>& fragments) {
  BOOST_CHECK_EQUAL(orig_indices.size(), lc.size());
  for (uint i = 0; i < orig_indices.size(); ++i) {
    BOOST_CHECK(orig_indices[i] < fragments.size());
    BOOST_CHECK(fragments[orig_indices[i]] == lc.GetFragmentInfo(i));
  }
}

void CheckScore(const std::vector<Real>& scores, uint exp_size,
                Real exp_min_score, Real exp_max_score,
                Real exp_avg_score) {
  // check size
  BOOST_CHECK_EQUAL(scores.size(), exp_size);
  // check stats
  Real min_score = 1000;
  Real max_score = -1000;
  Real avg_score = 0;
  for (uint i = 0; i < scores.size(); ++i) {
    min_score = std::min(min_score, scores[i]);
    max_score = std::max(max_score, scores[i]);
    avg_score += scores[i];
  }
  avg_score /= scores.size();
  // in case of checking stem rmsd, the min_score is super small and the
  // requested accuracy is within floating point errors => make it 1e-2
  BOOST_CHECK_CLOSE(min_score, exp_min_score, Real(1e-2));
  BOOST_CHECK_CLOSE(max_score, exp_max_score, Real(1e-3));
  BOOST_CHECK_CLOSE(avg_score, exp_avg_score, Real(1e-3));
}

void CheckProfScores(const LoopCandidates& lc, 
                     const std::vector<uint>& orig_indices,
                     loop::StructureDBPtr structure_db,
                     ost::seq::ProfileHandlePtr prof,
                     uint start_idx, const std::vector<Real>& seq_prof_scores,
                     const std::vector<Real>& str_prof_scores) {
  // get new scores
  std::vector<Real> new_seq_prof_scores, new_str_prof_scores;
  new_seq_prof_scores = lc.CalculateSequenceProfileScores(
                              structure_db, *prof, start_idx);
  new_str_prof_scores = lc.CalculateStructureProfileScores(
                              structure_db, *prof, start_idx);
  // check them
  BOOST_CHECK_EQUAL(seq_prof_scores.size(), str_prof_scores.size());
  BOOST_CHECK_EQUAL(orig_indices.size(), lc.size());
  BOOST_CHECK_EQUAL(orig_indices.size(), new_seq_prof_scores.size());
  BOOST_CHECK_EQUAL(orig_indices.size(), new_str_prof_scores.size());
  for (uint i = 0; i < orig_indices.size(); ++i) {
    BOOST_CHECK(orig_indices[i] < seq_prof_scores.size());
    BOOST_CHECK(seq_prof_scores[orig_indices[i]] == new_seq_prof_scores[i]);
    BOOST_CHECK(str_prof_scores[orig_indices[i]] == new_str_prof_scores[i]);
  }
}

void CheckClusters(const std::vector< std::vector<uint> >& clusters,
                   uint N_cand) {
  // all included?
  std::vector<bool> is_set(N_cand, false);
  for (uint i = 0; i < clusters.size(); ++i) {
    for (uint j = 0; j < clusters[i].size(); ++j) {
      BOOST_CHECK(!is_set[clusters[i][j]]);
      is_set[clusters[i][j]] = true;
    }
  }
  for (uint i = 0; i < N_cand; ++i) {
    BOOST_CHECK(is_set[i]);
  }
}

std::vector< std::vector<uint> >
FilterClusters(const std::vector< std::vector<uint> >& clusters) {
  std::vector< std::vector<uint> > filtered_clusters;
  for (uint i = 0; i < clusters.size(); ++i) {
    if (clusters[i].size() > 1) filtered_clusters.push_back(clusters[i]);
  }
  return filtered_clusters;
}

void CheckClustered(const LoopCandidatesList& lc_list, const LoopCandidates& lc,
                    const std::vector< std::vector<uint> >& clusters) {
  BOOST_CHECK_EQUAL(lc_list.size(), clusters.size());
  for (uint i = 0; i < clusters.size(); ++i) {
    const LoopCandidates& lc_other = *lc_list[i];
    BOOST_CHECK_EQUAL(lc_other.size(), clusters[i].size());
    for (uint j = 0; j < clusters[i].size(); ++j) {
      const uint lc_idx = clusters[i][j];
      BOOST_CHECK(lc_other[j] == lc[lc_idx]);
      BOOST_CHECK(lc_other.GetFragmentInfo(j) == lc.GetFragmentInfo(lc_idx));
    }
  }
}

} // anon ns

/////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(test_loop_candidates_base) {
  // setup
  const String loop_seq = "HELLYEAH";
  const uint loop_length = loop_seq.length();
  loop::BackboneList bb_list(loop_seq);
  loop::BackboneList long_bb_list(loop_seq + "A");
  loop::BackboneList wrong_bb_list("HELLYESS");
  loop::FragmentInfo frag_info1(1, 1, loop_length);
  loop::FragmentInfo frag_info2(2, 2, loop_length);
  loop::FragmentInfo frag_info3(3, 3, loop_length);

  // fill LoopCandidates
  LoopCandidates lc(loop_seq);
  BOOST_CHECK(lc.empty());
  lc.Add(bb_list);
  lc.AddFragmentInfo(frag_info1);
  lc.Add(bb_list);
  lc.AddFragmentInfo(frag_info2);
  lc.Add(bb_list);
  lc.AddFragmentInfo(frag_info3);

  // check
  BOOST_CHECK(!lc.empty());
  BOOST_CHECK_EQUAL(lc.size(), uint(3));
  BOOST_CHECK(lc.HasFragmentInfos());
  for (uint i = 0; i < 3; ++i) BOOST_CHECK(lc[i] == bb_list);
  BOOST_CHECK(lc.GetFragmentInfo(0) == frag_info1);
  BOOST_CHECK(lc.GetFragmentInfo(1) == frag_info2);
  BOOST_CHECK(lc.GetFragmentInfo(2) == frag_info3);

  // remove an element
  LoopCandidates lc2 = lc; // COPY
  lc2.Remove(1);
  BOOST_CHECK(!lc2.empty());
  BOOST_CHECK_EQUAL(lc2.size(), uint(2));
  BOOST_CHECK(lc2.HasFragmentInfos());
  for (uint i = 0; i < 2; ++i) BOOST_CHECK(lc2[i] == bb_list);
  BOOST_CHECK(lc2.GetFragmentInfo(0) == frag_info1);
  BOOST_CHECK(lc2.GetFragmentInfo(1) == frag_info3);

  // extract elements
  std::vector<uint> indices;
  indices.push_back(2);
  indices.push_back(0);
  LoopCandidatesPtr lc_sub = lc.Extract(indices);
  BOOST_CHECK(!lc_sub->empty());
  BOOST_CHECK_EQUAL(lc_sub->size(), uint(2));
  BOOST_CHECK(lc_sub->HasFragmentInfos());
  for (uint i = 0; i < 2; ++i) BOOST_CHECK((*lc_sub)[i] == bb_list);
  BOOST_CHECK(lc_sub->GetFragmentInfo(0) == frag_info3);
  BOOST_CHECK(lc_sub->GetFragmentInfo(1) == frag_info1);

  // check exceptions
  BOOST_CHECK_THROW(lc[-1], std::exception);
  BOOST_CHECK_THROW(lc[3], std::exception);
  BOOST_CHECK_THROW(lc.GetFragmentInfo(-1), promod3::Error);
  BOOST_CHECK_THROW(lc.GetFragmentInfo(3), promod3::Error);
  BOOST_CHECK_THROW(lc.Add(long_bb_list), promod3::Error);
  BOOST_CHECK_THROW(lc.Add(wrong_bb_list), promod3::Error);
  BOOST_CHECK_EQUAL(lc.size(), uint(3));
  BOOST_CHECK_THROW(lc.Remove(-1), promod3::Error);
  BOOST_CHECK_THROW(lc.Remove(3), promod3::Error);
  BOOST_CHECK_EQUAL(lc.size(), uint(3));
  std::vector<uint> bad_indices = indices;
  bad_indices.push_back(3);
  BOOST_CHECK_THROW(lc.Extract(bad_indices), promod3::Error);

  // break fragments
  lc2 = lc; // COPY
  lc2.Add(bb_list);
  BOOST_CHECK(!lc2.HasFragmentInfos());
  BOOST_CHECK_THROW(lc2.GetFragmentInfo(0), promod3::Error);
  // extract should still work but w/o fragments
  lc_sub = lc2.Extract(indices);
  BOOST_CHECK(!lc_sub->empty());
  BOOST_CHECK_EQUAL(lc_sub->size(), uint(2));
  BOOST_CHECK(!lc_sub->HasFragmentInfos());
  for (uint i = 0; i < 2; ++i) BOOST_CHECK((*lc_sub)[i] == bb_list);
}

BOOST_AUTO_TEST_CASE(test_loop_candidates_from_db) {
  // setup
  ost::mol::EntityHandle ent = LoadTestStructure("data/1CRN.pdb");
  ost::seq::ProfileHandlePtr prof;
  prof = ost::io::LoadSequenceProfile("data/1CRNA.hhm");
  const uint loop_length = 7;
  const uint start_idx = 10;

  // get dbs (small DBs created as in example codes using crambin)
  loop::StructureDBPtr structure_db;
  structure_db = loop::StructureDB::LoadPortable("data/port_str_db.dat");
  // get known fragments to put in here
  std::vector<loop::FragmentInfo> fragments;
  fragments.push_back(loop::FragmentInfo(1, 108, loop_length));
  fragments.push_back(loop::FragmentInfo(1, 24, loop_length));
  fragments.push_back(loop::FragmentInfo(1, 93, loop_length));
  fragments.push_back(loop::FragmentInfo(0, 10, loop_length));
  fragments.push_back(loop::FragmentInfo(0, 9, loop_length));
  fragments.push_back(loop::FragmentInfo(1, 90, loop_length));
  fragments.push_back(loop::FragmentInfo(0, 7, loop_length));
  fragments.push_back(loop::FragmentInfo(1, 28, loop_length));
  fragments.push_back(loop::FragmentInfo(1, 4, loop_length));

  // manually fill loop candidates
  String loop_seq = "";
  ost::mol::ResidueHandleList res_list = ent.GetResidueList();
  for (uint i = 0; i < loop_length; ++i) {
    loop_seq += res_list[start_idx + i].GetOneLetterCode();
  }
  ost::mol::ResidueHandle n_stem = res_list[start_idx];
  ost::mol::ResidueHandle c_stem = res_list[start_idx+loop_length-1];
  LoopCandidatesPtr loop_candidates;
  loop_candidates = FillFromFragments(fragments, n_stem, c_stem, loop_seq,
                                      structure_db);
  const uint N_cand = loop_candidates->size();
  BOOST_CHECK_EQUAL(N_cand, uint(9));

  // get scores
  std::vector<Real> seq_prof_scores, str_prof_scores, stem_rmsds;
  seq_prof_scores = loop_candidates->CalculateSequenceProfileScores(
                        structure_db, *prof, start_idx);
  str_prof_scores = loop_candidates->CalculateStructureProfileScores(
                        structure_db, *prof, start_idx);
  stem_rmsds = loop_candidates->CalculateStemRMSDs(n_stem, c_stem);
  // check scores
  CheckScore(seq_prof_scores, N_cand, -2.73226428, 2.37290597, -1.04137301);
  CheckScore(str_prof_scores, N_cand, 0.0652512088, 0.749628067, 0.391301066);
  CheckScore(stem_rmsds, N_cand, 0.0314558744, 2.9163394, 1.22806919);

  // check ApplyXXX
  // -> expected ApplyCCD: 0, 2, .., 8 converging
  LoopCandidates lc = *loop_candidates; // COPY
  std::vector<uint> orig_indices;
  orig_indices = lc.ApplyCCD(n_stem, c_stem);
  CheckOrigIndices(lc, orig_indices, fragments);
  // ShowIndices(orig_indices);
  BOOST_CHECK_EQUAL(orig_indices.size(), uint(9));
  BOOST_CHECK_EQUAL(orig_indices[0], uint(0));
  BOOST_CHECK_EQUAL(orig_indices[1], uint(1));
  BOOST_CHECK_EQUAL(orig_indices[8], uint(8));
  CheckProfScores(lc, orig_indices, structure_db, prof, start_idx,
                  seq_prof_scores, str_prof_scores);
  // -> expected ApplyKIC: 0 converging with 2 LC
  lc = *loop_candidates; // COPY
  orig_indices = lc.ApplyKIC(n_stem, c_stem, 1, 3, 5);
  CheckOrigIndices(lc, orig_indices, fragments);
  // ShowIndices(orig_indices);
  BOOST_CHECK_EQUAL(orig_indices.size(), uint(50));


  BOOST_CHECK_EQUAL(orig_indices[0], uint(0));
  BOOST_CHECK_EQUAL(orig_indices[1], uint(0));
  BOOST_CHECK_EQUAL(orig_indices[2], uint(0));
  BOOST_CHECK_EQUAL(orig_indices[3], uint(0));
  BOOST_CHECK_EQUAL(orig_indices[4], uint(1));
  BOOST_CHECK_EQUAL(orig_indices[5], uint(1));
  BOOST_CHECK_EQUAL(orig_indices[6], uint(1));
  BOOST_CHECK_EQUAL(orig_indices[49], uint(8));

  CheckProfScores(lc, orig_indices, structure_db, prof, start_idx,
                  seq_prof_scores, str_prof_scores);

  // cluster them
  std::vector< std::vector<uint> > clusters;
  const Real max_dist = 0.35;
  loop_candidates->GetClusters(max_dist, clusters);
  // ShowClusters(clusters);

  BOOST_CHECK_EQUAL(clusters.size(), uint(5));

  std::vector<uint> cluster_sizes;
  for(uint i = 0; i < clusters.size(); ++i){
    cluster_sizes.push_back(clusters[i].size());
  }
  std::sort(cluster_sizes.begin(), cluster_sizes.end());

  BOOST_CHECK_EQUAL(cluster_sizes[0], uint(1));
  BOOST_CHECK_EQUAL(cluster_sizes[1], uint(1));
  BOOST_CHECK_EQUAL(cluster_sizes[2], uint(1));
  BOOST_CHECK_EQUAL(cluster_sizes[3], uint(3));
  BOOST_CHECK_EQUAL(cluster_sizes[4], uint(3));

  CheckClusters(clusters, N_cand);
  // get filtered variant to check neglect_size_one below
  std::vector< std::vector<uint> > filtered_clusters = FilterClusters(clusters);
  BOOST_CHECK_EQUAL(filtered_clusters.size(), uint(2));

  // check GetClusteredCandidates
  LoopCandidatesList lc_list;
  lc_list = loop_candidates->GetClusteredCandidates(max_dist, true);
  CheckClustered(lc_list, *loop_candidates, filtered_clusters);
  lc_list = loop_candidates->GetClusteredCandidates(max_dist, false);
  CheckClustered(lc_list, *loop_candidates, clusters);

  // check exceptions with profiles
  BOOST_CHECK(lc.HasFragmentInfos());
  const uint first_bad_idx = prof->size() - loop_length + 1;
  BOOST_CHECK_THROW(lc.CalculateSequenceProfileScores(
                      structure_db, *prof, first_bad_idx),
                    ost::Error);
  BOOST_CHECK_THROW(lc.CalculateStructureProfileScores(
                      structure_db, *prof, first_bad_idx),
                    ost::Error);
  // invalidate fragments in lc
  lc.Add(lc[0]);
  BOOST_CHECK(!lc.HasFragmentInfos());
  BOOST_CHECK_THROW(lc.CalculateSequenceProfileScores(
                      structure_db, *prof, start_idx),
                    promod3::Error);
  BOOST_CHECK_THROW(lc.CalculateStructureProfileScores(
                      structure_db, *prof, start_idx),
                    promod3::Error);

  // check behavior for empty loop candidate lists (may happen...)
  std::vector<uint> indices;
  LoopCandidatesPtr lc_empty = loop_candidates->Extract(indices);
  BOOST_CHECK(lc_empty->empty());
  BOOST_CHECK_EQUAL(lc_empty->size(), uint(0));
  // closing
  orig_indices = lc_empty->ApplyCCD(n_stem, c_stem);
  BOOST_CHECK(orig_indices.empty());
  orig_indices = lc_empty->ApplyKIC(n_stem, c_stem, 1, 2, 3);
  BOOST_CHECK(orig_indices.empty());
  // clustering
  lc_empty->GetClusters(max_dist, clusters);
  BOOST_CHECK(clusters.empty());
  lc_list = lc_empty->GetClusteredCandidates(max_dist);
  BOOST_CHECK(lc_list.empty());
  // scores
  seq_prof_scores = lc_empty->CalculateSequenceProfileScores(
                        structure_db, *prof, start_idx);
  BOOST_CHECK(seq_prof_scores.empty());
  str_prof_scores = lc_empty->CalculateStructureProfileScores(
                        structure_db, *prof, start_idx);
  BOOST_CHECK(str_prof_scores.empty());
  stem_rmsds = lc_empty->CalculateStemRMSDs(n_stem, c_stem);
  BOOST_CHECK(stem_rmsds.empty());
}

BOOST_AUTO_TEST_CASE(test_loop_candidates_fill_from_db) {
  // setup
  ost::mol::EntityHandle ent = LoadTestStructure("data/1CRN.pdb");
  ost::seq::ProfileHandlePtr prof;
  prof = ost::io::LoadSequenceProfile("data/1CRNA.hhm");
  const uint loop_length = 5;
  const uint start_idx = 10;

  // get dbs (small DBs created as in example codes using crambin)
  loop::FragDBPtr frag_db;
  frag_db = loop::FragDB::LoadPortable("data/port_frag_db.dat");
  loop::StructureDBPtr structure_db;
  structure_db = loop::StructureDB::LoadPortable("data/port_str_db.dat");

  // get loop candidates
  String loop_seq = "";
  ost::mol::ResidueHandleList res_list = ent.GetResidueList();
  for (uint i = 0; i < loop_length; ++i) {
    loop_seq += res_list[start_idx + i].GetOneLetterCode();
  }
  ost::mol::ResidueHandle n_stem = res_list[start_idx];
  ost::mol::ResidueHandle c_stem = res_list[start_idx+loop_length-1];
  LoopCandidatesPtr loop_candidates = LoopCandidates::FillFromDatabase(
                        n_stem, c_stem, loop_seq, frag_db, structure_db, true);
  const uint N_cand = loop_candidates->size();

  // check candidates
  BOOST_CHECK(N_cand > 0);
  BOOST_CHECK(loop_candidates->HasFragmentInfos());
  // ShowFragments(*loop_candidates);
  // AreWithinBounds(*loop_candidates, frag_db, n_stem, c_stem);

  // get dummy ent with one residue with only C and CA (no N)
  ost::mol::EntityHandle dummy_ent = ost::mol::CreateEntity();
  ost::mol::XCSEditor ed = dummy_ent.EditXCS(ost::mol::BUFFERED_EDIT);
  ost::mol::ChainHandle dummy_chain = ed.InsertChain("A"); 
  ost::mol::ResidueHandle bad_res = ed.AppendResidue(dummy_chain, "RES");
  ed.InsertAtom(bad_res, "C", geom::Vec3(0,0,0));
  ed.InsertAtom(bad_res, "CA", geom::Vec3(1,1,1));
  ed.UpdateICS();

  // check exceptions with bad res
  BOOST_CHECK_THROW(LoopCandidates::FillFromDatabase(
                      bad_res, c_stem, loop_seq, frag_db, structure_db, true),
                    promod3::Error);
  BOOST_CHECK_THROW(LoopCandidates::FillFromDatabase(
                      n_stem, bad_res, loop_seq, frag_db, structure_db, true),
                    promod3::Error);
  BOOST_CHECK_THROW(loop_candidates->CalculateStemRMSDs(bad_res, c_stem),
                    promod3::Error);
  BOOST_CHECK_THROW(loop_candidates->CalculateStemRMSDs(n_stem, bad_res),
                    promod3::Error);
}

BOOST_AUTO_TEST_SUITE_END();
