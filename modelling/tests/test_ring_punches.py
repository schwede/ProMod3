# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Unit tests to detect ring punchings.
"""
import unittest
import ost
from promod3 import modelling, loop

class RingPunchTests(unittest.TestCase):
    def testRings(self):
        '''Check that rings are detected.'''
        # prepare test for rings
        sequence = 'PAYFWPPPAH'
        bb_list = loop.BackboneList(sequence)
        model = bb_list.ToEntity()
        rings = modelling.GetRings(model)
        self.assertEqual(len(rings), 4)
        for ring in rings:
            self.assertEqual(type(ring.center), ost.geom.Vec3)
            self.assertEqual(type(ring.plane), ost.geom.Plane)
            # all prolines
            self.assertAlmostEqual(ring.radius, 1.28, places=1)
        self.assertEqual(str(rings[0].residue), "A.PRO1")
        self.assertEqual(str(rings[1].residue), "A.PRO6")
        self.assertEqual(str(rings[2].residue), "A.PRO7")
        self.assertEqual(str(rings[3].residue), "A.PRO8")
        # add sidechains
        modelling.ReconstructSidechains(model, keep_sidechains=False)
        rings = modelling.GetRings(model)
        self.assertEqual(len(rings), 9)
        self.assertAlmostEqual(rings[0].radius, 1.31, places=1)
        self.assertEqual(str(rings[0].residue), "A.PRO1")
        self.assertAlmostEqual(rings[1].radius, 1.4, places=1)
        self.assertEqual(str(rings[1].residue), "A.TYR3")
        self.assertAlmostEqual(rings[2].radius, 1.4, places=1)
        self.assertEqual(str(rings[2].residue), "A.PHE4")
        self.assertAlmostEqual(rings[3].radius, 1.2, places=1)
        self.assertEqual(str(rings[3].residue), "A.TRP5")
        self.assertAlmostEqual(rings[4].radius, 1.42, places=1)
        self.assertEqual(str(rings[4].residue), "A.TRP5")
        self.assertEqual(str(rings[5].residue), "A.PRO6")
        self.assertEqual(str(rings[6].residue), "A.PRO7")
        self.assertEqual(str(rings[7].residue), "A.PRO8")
        self.assertAlmostEqual(rings[8].radius, 1.18, places=1)
        self.assertEqual(str(rings[8].residue), "A.HIS10")
        # ensure no ring punchings
        self.assertEqual(len(modelling.GetRingPunches(rings, model)), 0)
        self.assertFalse(modelling.HasRingPunches(rings, model))
        # make sure we can also use views
        ringss = modelling.GetRings(model.Select("rnum=1:2"))
        models = model.Select("rnum=2:4")
        self.assertEqual(len(ringss), 1)
        self.assertAlmostEqual(ringss[0].radius, 1.31, places=1)
        self.assertEqual(str(ringss[0].residue), "A.PRO1")
        self.assertEqual(len(modelling.GetRingPunches(ringss, model)), 0)
        self.assertFalse(modelling.HasRingPunches(ringss, model))
        self.assertEqual(len(modelling.GetRingPunches(rings, models)), 0)
        self.assertFalse(modelling.HasRingPunches(rings, models))
        self.assertEqual(len(modelling.GetRingPunches(ringss, models)), 0)
        self.assertFalse(modelling.HasRingPunches(ringss, models))
        # check that we can remodel parts and keep non-remodelled rings
        start_resnum = 6
        end_resnum = 8
        myqueryin = "rnum=" + str(start_resnum) + ":" + str(end_resnum)
        myqueryout = "rnum<" + str(start_resnum) + " or rnum>" + str(end_resnum)
        model_out = model.Select(myqueryout)
        rings_out = modelling.GetRings(model_out)
        sub_bb_list = bb_list.Extract(start_resnum-1,end_resnum)
        sub_bb_list.InsertInto(model.chains[0], start_resnum)
        model_new = model.Select(myqueryin)
        rings_new = modelling.GetRings(model_new)
        self.assertFalse(modelling.HasRingPunches(rings_out, model_new))
        self.assertFalse(modelling.HasRingPunches(rings_new, model))
        # check w/o insertion
        model_new = bb_list.Extract(start_resnum-1,end_resnum).ToEntity()
        rings_new = modelling.GetRings(model_new)
        self.assertFalse(modelling.HasRingPunches(rings_out+rings_new, model_new))
        self.assertFalse(modelling.HasRingPunches(rings_new, model_out))

    def testRingPunch(self):
        '''Check that we detect ring punches (selected examples).'''
        # test 1
        model = ost.io.LoadPDB("data/1ITX-A-11_sidechain.pdb")
        rings = modelling.GetRings(model)
        ring_punches = modelling.GetRingPunches(rings, model)
        self.assertTrue(modelling.HasRingPunches(rings, model))
        self.assertEqual(len(ring_punches), 1)
        self.assertEqual(str(ring_punches[0]), "A.TYR321")
        # test 2
        model = ost.io.LoadPDB("data/4R6K-A-13_sidechain.pdb")
        rings = modelling.GetRings(model)
        ring_punches = modelling.GetRingPunches(rings, model)
        self.assertTrue(modelling.HasRingPunches(rings, model))
        self.assertEqual(len(ring_punches), 1)
        self.assertEqual(str(ring_punches[0]), "A.PRO197")
        # hetero oligomer
        model = ost.io.LoadPDB("data/hetero-punched.pdb")
        rings = modelling.GetRings(model)
        ring_punches = modelling.GetRingPunches(rings, model)
        self.assertTrue(modelling.HasRingPunches(rings, model))
        self.assertEqual(len(ring_punches), 2)
        self.assertEqual(str(ring_punches[0]), "A.TRP16")
        self.assertEqual(str(ring_punches[1]), "B.TRP46")

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
