# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import unittest
from promod3 import modelling
from ost import io,seq

class GapExtension(unittest.TestCase):
    #######################################################################
    # HELPERs - same test case for all gap routines
    #######################################################################
    def getRawModel(self):
        tpl = io.LoadPDB('data/2dbs.pdb')
        aln = seq.CreateAlignment(
            seq.CreateSequence('trg', 'AAAATLNGFTVPAGNTLV-LNP--DKGATVTMA'),
            seq.CreateSequence('tpl', 'NGGT---LL--IPNGTYHFLGIQMKSNVHIRV-'))
        aln.AttachView(1, tpl.CreateFullView())
        return modelling.BuildRawModel(aln)

    def checkGap(self, gap, n_stem_resnum, c_stem_resnum, seq):
        self.assertEqual(gap.before.GetNumber(), n_stem_resnum)
        self.assertEqual(gap.after.GetNumber(), c_stem_resnum)
        self.assertEqual(gap.seq, seq)
    #######################################################################

    def testGapExtender(self):
        mhandle = self.getRawModel()
        # use first gap
        gap = mhandle.gaps[0].Copy()
        ext = modelling.GapExtender(gap, mhandle.seqres[0])
        self.checkGap(gap, 4, 8, 'TLN')
        # extend it
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 8, 'ATLN')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 9, 'TLNG')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 8, 'AATLN')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 9, 'ATLNG')
        self.assertFalse(ext.Extend())
        # ensure gap not changing on false
        self.checkGap(gap, 3, 9, 'ATLNG')
        self.assertFalse(ext.Extend())

    def testFullGapExtender(self):
        mhandle = self.getRawModel()
        # run with various settings
        for i in [-2, -1, 2, 7, 12, 120]:
            # use first gap
            gap = mhandle.gaps[0].Copy()
            self.checkGap(gap, 4, 8, 'TLN')
            # setup extender
            if i == -2:
                ext = modelling.FullGapExtender(gap, mhandle.seqres[0])
            else:
                ext = modelling.FullGapExtender(gap, mhandle.seqres[0], i)
            if i < 0 or i > 100:
                # note: cannot include final terminal insertion!
                exp_max_length = mhandle.seqres[0].GetLength()-3
            else:
                exp_max_length = i
            # extend it
            if exp_max_length > 3:
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 3, 8, 'ATLN')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 4, 9, 'TLNG')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 2, 8, 'AATLN')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 3, 9, 'ATLNG')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 1, 8, 'AAATLN')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 2, 9, 'AATLNG')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 1, 9, 'AAATLNG')
                self.assertTrue(ext.Extend())
                self.checkGap(gap, 4, 12, 'TLNGFTV')
                # check rest
                while ext.Extend():
                    self.assertLessEqual(gap.length, exp_max_length)
                self.assertEqual(gap.length, exp_max_length)
            else:
                self.assertFalse(ext.Extend())
                self.checkGap(gap, 4, 8, 'TLN')
                self.assertFalse(ext.Extend())

    def testFullGapExtenderMerge(self):
        mhandle = self.getRawModel()
        modelling.MergeGaps(mhandle, 1)
        # use first gap
        gap = mhandle.gaps[0].Copy()
        self.checkGap(gap, 4, 8, 'TLN')
        # setup extender
        ext = modelling.FullGapExtender(gap, mhandle.seqres[0])
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 8, 'ATLN')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 9, 'TLNG')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 8, 'AATLN')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 9, 'ATLNG')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 8, 'AAATLN')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 9, 'AATLNG')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 9, 'AAATLNG')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 19, 'TLNGFTVPAGNTLV')
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 19, 'ATLNGFTVPAGNTLV')
        # check rest
        exp_max_length = mhandle.seqres[0].GetLength()-3
        while ext.Extend():
            self.assertLessEqual(gap.length, exp_max_length)
        self.assertEqual(gap.length, exp_max_length)

    def testScoringGapExtender(self):
        mhandle = self.getRawModel()
        seq_length = mhandle.seqres[0].GetLength()
        penalties = [1.0]*4 + [0.0]*(seq_length-4)
        # use first gap
        gap = mhandle.gaps[0].Copy()
        ext = modelling.ScoringGapExtender(gap, 0.8, penalties,
                                           mhandle.seqres[0], -2)
        self.checkGap(gap, 4, 8, 'TLN')
        # extend it
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 9, 'TLNG') # 0.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 8, 'ATLN') # 1.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 9, 'ATLNG') # 2.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 8, 'AATLN') # 3.6
        self.assertFalse(ext.Extend())
        # ensure gap not changing on false
        self.checkGap(gap, 2, 8, 'AATLN')
        self.assertFalse(ext.Extend())

    def testScoringGapExtender12(self):
        mhandle = self.getRawModel()
        seq_length = mhandle.seqres[0].GetLength()
        penalties = [1.0]*4 + [0.0]*(seq_length-4)
        # use first gap
        gap = mhandle.gaps[0].Copy()
        ext = modelling.ScoringGapExtender(gap, 0.8, penalties,
                                           mhandle.seqres[0], 12)
        self.checkGap(gap, 4, 8, 'TLN')
        # extend it
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 9, 'TLNG') # 0.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 8, 'ATLN') # 1.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 9, 'ATLNG') # 2.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 12, 'TLNGFTV') # 3.2
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 8, 'AATLN') # 3.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 13, 'TLNGFTVP') # 4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 9, 'AATLNG') # 4.4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 14, 'TLNGFTVPA') # 4.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 12, 'ATLNGFTV') # 5
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 8, 'AAATLN') # 5.4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 15, 'TLNGFTVPAG') # 5.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 13, 'ATLNGFTVP') # 5.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 9, 'AAATLNG') # 6.2
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 16, 'TLNGFTVPAGN') # 6.4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 14, 'ATLNGFTVPA') # 6.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 12, 'AATLNGFTV') # 6.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 17, 'TLNGFTVPAGNT') # 7.2
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 15, 'ATLNGFTVPAG') # 7.4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 13, 'AATLNGFTVP') # 7.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 16, 'ATLNGFTVPAGN') # 8.2
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 14, 'AATLNGFTVPA') # 8.4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 12, 'AAATLNGFTV') # 8.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 2, 15, 'AATLNGFTVPAG') # 9.2
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 13, 'AAATLNGFTVP') # 9.4
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 1, 14, 'AAATLNGFTVPA') # 10.2
        # ensure gap not changing on false
        self.checkGap(gap, 1, 14, 'AAATLNGFTVPA')

    def testScoringGapExtenderAll(self):
        mhandle = self.getRawModel()
        seq_length = mhandle.seqres[0].GetLength()
        penalties = [1.0]*4 + [0.0]*(seq_length-4)
        # use first gap
        gap = mhandle.gaps[0].Copy()
        ext = modelling.ScoringGapExtender(gap, 0.8, penalties,
                                           mhandle.seqres[0], -1)
        self.checkGap(gap, 4, 8, 'TLN')
        # extend it
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 9, 'TLNG') # 0.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 8, 'ATLN') # 1.8
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 3, 9, 'ATLNG') # 2.6
        self.assertTrue(ext.Extend())
        self.checkGap(gap, 4, 12, 'TLNGFTV') # 3.2
        # check rest
        exp_max_length = seq_length-3
        max_length = 0
        while ext.Extend():
            self.assertLessEqual(gap.length, exp_max_length)
            max_length = max(max_length, gap.length)
        self.assertEqual(max_length, exp_max_length)

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
