# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Unit tests for modelling pipeline components.
We go from a fake cut in crambin to reconstruct that same protein.
"""
import unittest
import math
import ost
from ost import io, mol, seq, settings
from ost.mol import mm
from promod3 import loop, modelling, core

################################################################
# Code to generate 1crn_cut and 1crn.fasta:
################################################################
# from ost import io,mol,seq
# # load protein 
# prot = io.LoadPDB('1crn', remote=True)
# # get only amino acids
# prot = mol.CreateEntityFromView(prot.Select("peptide=true"), True)
# # get sequence
# seqres = ''.join([r.one_letter_code for r in prot.residues])
# # cut out stuff for fake pdb file
# prot_cut = mol.CreateEntityFromView(prot.Select("rnum < 25 or rnum > 30"), True)
# io.SavePDB(prot_cut, "data/1crn_cut.pdb")
# seqres_cut = seqres[:24] + '-'*6 + seqres[30:]
# # create alignment and dump
# aln = seq.CreateAlignment(
#     seq.CreateSequence('trg', seqres),
#     seq.CreateSequence('tpl', seqres_cut))
# io.SaveAlignment(aln, "data/1crn.fasta")
################################################################
# Code to generate reference solutions:
# -> these files must be carefully (!!) updated each time
#    parts of the pipeline change!
################################################################
# from ost import io
# from promod3 import loop,modelling
# # create raw model
# tpl = io.LoadPDB('data/1crn_cut.pdb')
# aln = io.LoadAlignment('data/1crn.fasta')
# aln.AttachView(1, tpl.CreateFullView())
# mhandle = modelling.BuildRawModel(aln)
# # do it all
# final_model = modelling.BuildFromRawModel(mhandle)
# io.SavePDB(final_model, 'data/1crn_build.pdb')
# # step-by-step - loop
# mhandle = modelling.BuildRawModel(aln)
# frag_db = loop.LoadFragDB()
# structure_db = loop.LoadStructureDB()
# torsion_sampler = loop.LoadTorsionSamplerCoil()
# modelling.FillLoopsByDatabase(mhandle, frag_db,
#                               structure_db,
#                               torsion_sampler)
# io.SavePDB(mhandle.model, 'data/1crn_rec.pdb')
# # step-by-step - sidechains
# mhandle.model = io.LoadPDB('data/1crn_rec.pdb')
# modelling.BuildSidechains(mhandle)
# io.SavePDB(mhandle.model, 'data/1crn_sc.pdb')
# # step-by-step - energy minimization
# mhandle.model = io.LoadPDB('data/1crn_sc.pdb')
# modelling.MinimizeModelEnergy(mhandle)
# io.SavePDB(mhandle.model, 'data/1crn_final.pdb')
################################################################
# Code to generate oligomers
# REQ: 2aoh-1.pdb and 5d52-1.pdb from SMTL (or here in data)
################################################################
# import os
# from ost import io,mol,seq
# # choose protein
# out_path = "oligos"
# tpl_id = '5d52-1' # hetero
# #tpl_id = '2aoh-1' # homo (with useless chain C)
# # load protein 
# prot = io.LoadPDB(os.path.join(out_path, tpl_id + ".pdb"))
# # get only amino acids
# prot = mol.CreateEntityFromView(prot.Select("peptide=true"), True)
# # get chain names and seqres
# nameA = prot.chains[0].name
# nameB = prot.chains[1].name
# seqresA = ''.join([r.one_letter_code for r in prot.chains[0].residues])
# seqresB = ''.join([r.one_letter_code for r in prot.chains[1].residues])
# # choose what to cut (rnum=X1..X2 removed)
# rnumA1 = 6
# rnumA2 = 11
# rnumB1 = 17
# rnumB2 = 22
# # cut out stuff for fake pdb file
# myquery = "(cname=%s and (rnum < %d or rnum > %d))" \
#           " or (cname= %s and (rnum < %d or rnum > %d))" \
#           % (nameA, rnumA1, rnumA2, nameB, rnumB1, rnumB2)
# prot_cut = mol.CreateEntityFromView(prot.Select(myquery), True)
# io.SavePDB(prot_cut, os.path.join(out_path, tpl_id + "_cut.pdb"))
# # create alignments
# seqresA_cut = seqresA[:rnumA1-1] + '-'*(rnumA2-rnumA1+1) + seqresA[rnumA2:]
# seqresB_cut = seqresB[:rnumB1-1] + '-'*(rnumB2-rnumB1+1) + seqresB[rnumB2:]
# alnA = seq.CreateAlignment(seq.CreateSequence('trg', seqresA),
#                            seq.CreateSequence(nameA, seqresA_cut))
# alnB = seq.CreateAlignment(seq.CreateSequence('trg', seqresB),
#                            seq.CreateSequence(nameB, seqresB_cut))
# # dump and report
# print 'ALIGNMENT-A\n' + str(alnA)
# print 'ALIGNMENT-B\n' + str(alnB)
# io.SaveAlignment(alnA, os.path.join(out_path, tpl_id + "_cut_A.fasta"))
# io.SaveAlignment(alnB, os.path.join(out_path, tpl_id + "_cut_B.fasta"))
################################################################

# setting up an OST LogSink to capture messages
class _FetchLog(ost.LogSink):
    def __init__(self):
        ost.LogSink.__init__(self)
        self.messages = dict()

    def LogMessage(self, message, severity):
        levels = ['ERROR', 'WARNING', 'SCRIPT', 'INFO', 'VERBOSE', 'DEBUG',
                  'TRACE']
        level = levels[severity]
        if not level in list(self.messages.keys()):
            self.messages[level] = list()
        self.messages[level].append(message.strip())

class PipelineTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # load dbs etc here for all tests
        cls.frag_db = loop.LoadFragDB()
        cls.structure_db = loop.LoadStructureDB()
        cls.torsion_sampler = loop.LoadTorsionSamplerCoil()

    @classmethod
    def tearDownClass(cls):
        # show runtimes if this was enabled
        core.StaticRuntimeProfiler.PrintSummary(5)

    #######################################################################
    # HELPERs
    #######################################################################
    def compare(self, model, filename, delta=0.01):
        '''Compare model with whatever is stored in filename.'''
        model_ref = io.LoadPDB(filename)
        self.assertEqual(model.GetAtomCount(), model_ref.GetAtomCount())
        # NOTE: ignore rmsd for now (fails too easily)
        #diff = mol.alg.Superpose(model_ref, model)
        #self.assertLess(diff.rmsd, delta)

    def checkFullModel(self, mhandle):
        '''Compare full modelling output result in mhandle.'''
        self.assertEqual(len(mhandle.gaps), 0)
        self.assertTrue(mhandle.model.IsValid())
        # estimate delta based on newly added atoms
        acf = mhandle.model.GetAtomCount()
        acr = self.raw_model.GetAtomCount()
        avgd = 0.2   # allow for this delta on each newly added atom
        delta = avgd * math.sqrt(float(acf-acr)/float(acf))
        self.compare(mhandle.model, 'data/1crn_build.pdb', delta)

    def getRawModel(self):
        '''Get default raw model. Can overwrite model in there.'''
        tpl = io.LoadPDB('data/1crn_cut.pdb')
        aln = io.LoadAlignment('data/1crn.fasta')
        aln.AttachView(1, tpl.CreateFullView())
        mhandle = modelling.BuildRawModel(aln)
        # keep a copy of the raw model
        self.raw_model = mhandle.model.Copy()
        return mhandle

    def getRawModelOligo(self, file_prefix):
        '''Get raw model for tests with oligomers.'''
        tpl = io.LoadPDB(file_prefix + '.pdb')
        alns = seq.AlignmentList()
        alns.append(io.LoadAlignment(file_prefix + "_A.fasta"))
        alns.append(io.LoadAlignment(file_prefix + "_B.fasta"))
        alns[0].AttachView(1, tpl.Select("cname=A").CreateFullView())
        alns[1].AttachView(1, tpl.Select("cname=B").CreateFullView())
        mhandle = modelling.BuildRawModel(alns)
        return mhandle

    def getMockModel(self, model):
        '''Get ModellingHandle for given model (assume no gaps).'''
        mhandle = modelling.ModellingHandle()
        mhandle.model = model
        for ch in model.chains:
            # we add dummy residues to seqres to be consistent with res. num.
            myseq = 'A' * (ch.residues[0].GetNumber().GetNum() - 1)
            myseq += ''.join([r.one_letter_code for r in ch.residues])
            mhandle.seqres.AddSequence(seq.CreateSequence(ch.name, myseq))
        return mhandle

    def checkFinalModel(self, mhandle, exp_gaps=0, num_chains=0):
        '''Check if:
        - model has exp_gaps and that CheckFinalModel reports it
        - model residues are properly set (peptide_linking, protein, torsion)
        '''
        # check residue properties
        for r in mhandle.model.residues:
            # check types
            self.assertTrue(r.peptide_linking)
            self.assertTrue(r.is_protein)
            # check links
            if r.next.IsValid() and (r.next.number - r.number) == 1:
                self.assertTrue(mol.InSequence(r, r.next))
            # check torsions
            if r.prev.IsValid() and mol.InSequence(r.prev, r):
                self.assertTrue(r.phi_torsion.IsValid())
                self.assertTrue(r.omega_torsion.IsValid())
            if r.next.IsValid() and mol.InSequence(r, r.next):
                self.assertTrue(r.psi_torsion.IsValid())
        # setup gap check
        self.assertEqual(len(mhandle.gaps), exp_gaps)
        log = _FetchLog()
        ost.PushLogSink(log)
        ost.PushVerbosityLevel(1)
        # check
        log.messages['WARNING'] = list()
        modelling.CheckFinalModel(mhandle)
        if exp_gaps == 0:
            self.assertEqual(len(log.messages['WARNING']), 0)
        else:
            self.assertEqual(len(log.messages['WARNING']), 1)
            self.assertEqual(log.messages['WARNING'][-1],
                             "Failed to close %d gap(s). " \
                             "Returning incomplete model!" % exp_gaps)
            self.assertGreaterEqual(len(mhandle.modelling_issues), exp_gaps)
            for i in range(exp_gaps):
                issue = mhandle.modelling_issues[i]
                self.assertTrue(issue.text.startswith("Failed to close"))
                self.assertTrue(issue.is_major())
                self.assertEqual(len(issue.residue_list), 2)
            # fake remove gaps to get sequence mismatch warnings
            mhandle.gaps = modelling.StructuralGapList()
            mhandle.modelling_issues = list()
            log.messages['WARNING'] = list()
            modelling.CheckFinalModel(mhandle)
            # 1 warning/issue per chain expected
            exps = "Sequence mismatch in chain"
            self.assertGreaterEqual(len(log.messages['WARNING']), num_chains)
            self.assertGreaterEqual(len(mhandle.modelling_issues), num_chains)
            for i in range(num_chains):
                self.assertTrue(log.messages['WARNING'][i].startswith(exps))
                issue = mhandle.modelling_issues[i]
                self.assertTrue(issue.text.startswith(exps))
                self.assertTrue(issue.is_major())
                self.assertEqual(len(issue.residue_list), 0)

    #######################################################################

    def testLoopReconstruction(self):
        '''Check loop reconstruction.'''
        mhandle = self.getRawModel()
        modelling.FillLoopsByDatabase(mhandle, self.frag_db,
                                      self.structure_db,
                                      self.torsion_sampler)
        self.assertEqual(len(mhandle.gaps), 0)
        self.compare(mhandle.model, 'data/1crn_rec.pdb')
        # NOTE: this test is slightly unstable as it requires the same gap to
        #       be chosen in FillLoopsByDatabase as in 1crn_rec!

    def testBuildSidechains(self):
        '''Check building of sidechains.'''
        mhandle = self.getMockModel(io.LoadPDB('data/1crn_rec.pdb'))
        modelling.BuildSidechains(mhandle)
        self.compare(mhandle.model, 'data/1crn_sc.pdb')

    def testBuildSidechainsRingPunch(self):
        '''Check building of sidechains with ring punches.'''
        # test 1
        mhandle = self.getMockModel(io.LoadPDB('data/1ITX-A-11_sidechain.pdb'))
        modelling.BuildSidechains(mhandle, fragment_db=self.frag_db,
                                  structure_db=self.structure_db,
                                  torsion_sampler=self.torsion_sampler)
        rings = modelling.GetRings(mhandle.model)
        self.assertFalse(modelling.HasRingPunches(rings, mhandle.model))
        # test 2
        mhandle = self.getMockModel(io.LoadPDB('data/4R6K-A-13_sidechain.pdb'))
        modelling.BuildSidechains(mhandle, fragment_db=self.frag_db,
                                  structure_db=self.structure_db,
                                  torsion_sampler=self.torsion_sampler)
        rings = modelling.GetRings(mhandle.model)
        self.assertFalse(modelling.HasRingPunches(rings, mhandle.model))
        # hetero oligomer
        mhandle = self.getMockModel(io.LoadPDB('data/hetero-punched.pdb'))
        modelling.BuildSidechains(mhandle, fragment_db=self.frag_db,
                                  structure_db=self.structure_db,
                                  torsion_sampler=self.torsion_sampler)
        rings = modelling.GetRings(mhandle.model)
        self.assertFalse(modelling.HasRingPunches(rings, mhandle.model))
        # ring punches in neighboring entries
        mhandle = self.getMockModel(io.LoadPDB('data/neighbor-punched.pdb'))
        rings = modelling.GetRings(mhandle.model)
        ring_punches = modelling.GetRingPunches(rings, mhandle.model)
        self.assertEqual(len(ring_punches), 2)
        self.assertTrue(ring_punches[0].next == ring_punches[1])
        modelling.BuildSidechains(mhandle, fragment_db=self.frag_db,
                                  structure_db=self.structure_db,
                                  torsion_sampler=self.torsion_sampler)
        rings = modelling.GetRings(mhandle.model)
        self.assertFalse(modelling.HasRingPunches(rings, mhandle.model))

    def testMinimizeModelEnergy(self):
        '''Check energy minimization.'''
        mhandle = self.getMockModel(io.LoadPDB('data/1crn_sc.pdb'))
        modelling.MinimizeModelEnergy(mhandle)
        self.compare(mhandle.model, 'data/1crn_final.pdb')

    def testMinimizeModelEnergyFails(self):
        '''Check energy minimization fails.'''
        # setup log
        log = _FetchLog()
        ost.PushLogSink(log)
        ost.PushVerbosityLevel(1)
        # catch atoms on top
        mhandle = self.getMockModel(io.LoadPDB('data/gly_on_top.pdb'))
        log.messages['ERROR'] = list()
        modelling.MinimizeModelEnergy(mhandle)
        self.assertEqual(len(log.messages['ERROR']), 1)
        self.assertEqual(len(mhandle.modelling_issues), 1)
        exp_msg = "OpenMM could not minimize energy as atoms are on "\
                  "top of each other!"
        self.assertEqual(log.messages['ERROR'][0], exp_msg)
        issue = mhandle.modelling_issues[0]
        self.assertEqual(issue.text, exp_msg)
        self.assertTrue(issue.is_major())
        self.assertEqual(len(issue.residue_list), 0)
        # catch atoms almost on top
        # -> this is known to only fail with the CPU platform
        # -> tested with OpenMM 7.1.1 (unknown for others)
        mm_settings = mm.Settings()
        mm_settings.platform = mm.Platform.CPU
        if mm.Simulation.IsPlatformAvailable(mm_settings):
            model = io.LoadPDB('data/gly_almost_on_top.pdb')
            mhandle = self.getMockModel(model)
            log.messages['ERROR'] = list()
            modelling.MinimizeModelEnergy(mhandle)
            self.assertEqual(len(log.messages['ERROR']), 1)
            self.assertEqual(len(mhandle.modelling_issues), 1)
            exp_msg = "OpenMM could not minimize energy! Usually this is "\
                      "caused by atoms which are almost on top of each other."
            self.assertTrue(log.messages['ERROR'][0].startswith(exp_msg))
            issue = mhandle.modelling_issues[0]
            self.assertTrue(issue.text.startswith(exp_msg))
            self.assertTrue(issue.is_major())
            self.assertEqual(len(issue.residue_list), 0)
        else:
            print("OpenMM CPU platform not available. " \
                  "Skipping almost_on_top check.")

    def testBuildFromRawModel(self):
        '''Check that BuildFromRawModel works.'''
        # get raw model
        mhandle = self.getRawModel()
        # build final model
        final_model = modelling.BuildFromRawModel(mhandle)
        # check
        self.assertEqual(mhandle.model, final_model)
        self.checkFullModel(mhandle)
        self.checkFinalModel(mhandle)

    def testHomoOligomer(self):
        '''Check that BuildFromRawModel works for a homo oligomer.'''
        # get raw model
        mhandle = self.getRawModelOligo("data/2aoh-1_cut")
        # build final model
        final_model = modelling.BuildFromRawModel(mhandle)
        # check
        self.assertEqual(mhandle.model, final_model)
        self.assertEqual(mhandle.model.residue_count, 198)
        self.assertEqual(mhandle.model.atom_count, 1508)
        self.checkFinalModel(mhandle)

    def testHeteroOligomer(self):
        '''Check that BuildFromRawModel works for a hetero oligomer.'''
        # get raw model
        mhandle = self.getRawModelOligo("data/5d52-1_cut")
        # build final model
        final_model = modelling.BuildFromRawModel(mhandle)
        # check
        self.assertEqual(mhandle.model, final_model)
        self.assertEqual(mhandle.model.residue_count, 51)
        self.assertEqual(mhandle.model.atom_count, 403)
        self.checkFinalModel(mhandle)

    def testCheckFinalModel(self):
        '''Check that we report unclosed models.'''
        # single chain
        mhandle = self.getRawModel()
        self.checkFinalModel(mhandle, exp_gaps=1, num_chains=1)
        # repeat after sidechain reconstruction and MM
        mhandle = self.getRawModel()
        modelling.BuildSidechains(mhandle)
        modelling.MinimizeModelEnergy(mhandle)
        self.checkFinalModel(mhandle, exp_gaps=1, num_chains=1)
        # homo-dimer
        mhandle = self.getRawModelOligo("data/2aoh-1_cut")
        self.checkFinalModel(mhandle, exp_gaps=2, num_chains=2)
        # repeat after sidechain reconstruction and MM
        mhandle = self.getRawModelOligo("data/2aoh-1_cut")
        modelling.BuildSidechains(mhandle)
        modelling.MinimizeModelEnergy(mhandle)
        self.checkFinalModel(mhandle, exp_gaps=2, num_chains=2)
        # hetero-dimer
        mhandle = self.getRawModelOligo("data/5d52-1_cut")
        self.checkFinalModel(mhandle, exp_gaps=2, num_chains=2)
        # repeat after sidechain reconstruction and MM
        mhandle = self.getRawModelOligo("data/5d52-1_cut")
        modelling.BuildSidechains(mhandle)
        modelling.MinimizeModelEnergy(mhandle)
        self.checkFinalModel(mhandle, exp_gaps=2, num_chains=2)

    def testCheckFinalModelRingPunches(self):
        '''Check that we report models with ring punches.'''
        # setup log
        log = _FetchLog()
        ost.PushLogSink(log)
        ost.PushVerbosityLevel(1)
        # setup raw model
        mhandle = self.getMockModel(io.LoadPDB('data/1ITX-A-11_sidechain.pdb'))
        # check
        log.messages['WARNING'] = list()
        modelling.CheckFinalModel(mhandle)
        self.assertEqual(len(log.messages['WARNING']), 1)
        self.assertGreaterEqual(len(mhandle.modelling_issues), 1)
        exp_msg = "Ring of A.TYR321 has been punched!"
        self.assertEqual(log.messages['WARNING'][0], exp_msg)
        issue = mhandle.modelling_issues[0]
        self.assertEqual(issue.text, exp_msg)
        self.assertTrue(issue.is_major())
        self.assertEqual(len(issue.residue_list), 1)
        self.assertEqual(str(issue.residue_list[0]), "A.TYR321")

    def testCheckFinalModelClash(self):
        '''Check that we report models with stereo-chemical problems.'''
        # setup log
        log = _FetchLog()
        ost.PushLogSink(log)
        ost.PushVerbosityLevel(3)
        # setup raw model
        mhandle = self.getMockModel(io.LoadPDB('data/1K5C-A-16_clash.pdb'))
        # check
        log.messages['INFO'] = list()
        modelling.CheckFinalModel(mhandle)
        self.assertEqual(len(log.messages['INFO']), 2)
        self.assertEqual(len(mhandle.modelling_issues), 2)
        exp_msg = "Stereo-chemical problem in backbone of residue A.GLY79"
        self.assertEqual(log.messages['INFO'][0], exp_msg)
        issue = mhandle.modelling_issues[0]
        self.assertEqual(issue.text, exp_msg)
        self.assertFalse(issue.is_major())
        self.assertEqual(len(issue.residue_list), 1)
        self.assertEqual(str(issue.residue_list[0]), "A.GLY79")
        exp_msg = "Stereo-chemical problem in sidechain of residue A.HIS98"
        self.assertEqual(log.messages['INFO'][1], exp_msg)
        issue = mhandle.modelling_issues[1]
        self.assertEqual(issue.text, exp_msg)
        self.assertFalse(issue.is_major())
        self.assertEqual(len(issue.residue_list), 1)
        self.assertEqual(str(issue.residue_list[0]), "A.HIS98")

    def testMolProbity(self):
        '''Check if binding to molprobity works.'''
        # do we have it?
        try:
            settings.Locate("phenix.molprobity",
                            env_name='MOLPROBITY_EXECUTABLE')
        except settings.FileNotFound:
            print('phenix.molprobity is missing. Please install it and make '+\
                  'it available in your PATH to execute test testMolProbity.')
            return
        # load 1crn and evaluate it
        prot = io.LoadPDB('1crn', remote=True)
        results = modelling.RunMolProbityEntity(prot)
        # check output
        self.assertAlmostEqual(results['Ramachandran favored'], 97.73, places=2)
        self.assertAlmostEqual(results['MolProbity score'], 0.56, places=2)
        self.assertEqual(results['C-beta deviations'], 0)
        self.assertAlmostEqual(results['Clashscore'], 0.0, places=2)
        self.assertAlmostEqual(results['Ramachandran outliers'], 0.0, places=2)
        self.assertAlmostEqual(results['Rotamer outliers'], 0.0, places=2)

    def testBuildModelWithLigands(self):
        '''Check that we can run pipeline with a ligand.'''
        # we get template with 5 ligands and model 1 chain
        tpl = io.LoadPDB("data/smtl2bl4.1.pdb")
        model_view = tpl.Select("cname=A")
        seqres = ''.join([r.one_letter_code for r in model_view.residues])
        aln = seq.CreateAlignment(seq.CreateSequence('trg', seqres),
                                  seq.CreateSequence('tpl', seqres))
        aln.AttachView(1, model_view)
        mhandle = modelling.BuildRawModel(aln, include_ligands=True)
        # 2 ligands (NAD, FE2) touching chain A
        ch = mhandle.model.FindChain('_')
        self.assertTrue(ch.IsValid())
        self.assertEqual(ch.residue_count, 2)
        # model it without extra force fields
        # -> should see ligands for sidechains and remove them in energy min.
        # -> we excpect two error logs for skipped ligands
        log = _FetchLog()
        ost.PushLogSink(log)
        log.messages['ERROR'] = list()
        final_model = modelling.BuildFromRawModel(mhandle)
        self.assertFalse(final_model.FindChain('_').IsValid())
        self.assertEqual(len(log.messages['ERROR']), 2)
        # model it with extra force field
        # -> only NAD ligand in ff file, should be kept
        ff = mol.mm.Forcefield.Load("data/ff_NAD.dat")
        mhandle = modelling.BuildRawModel(aln, include_ligands=True)

        final_model = modelling.BuildFromRawModel(mhandle, use_amber_ff = True, 
                                                  extra_force_fields = [ff])
        ch = final_model.FindChain('_')
        self.assertTrue(ch.IsValid())
        self.assertEqual(ch.residue_count, 1)
        self.assertEqual(ch.residues[0].name, "NAD")

    def testBuildModelCalpha(self):
        '''Check treatment of CA traces.'''
        # setup log
        log = _FetchLog()
        ost.PushLogSink(log)
        ost.PushVerbosityLevel(1)
        log.messages['ERROR'] = list()
        # setup tpl and aln
        tpl = io.LoadPDB('data/CA-3cm91E.pdb')
        aln = io.LoadAlignment('data/CA-3cm91E.fasta')
        aln.AttachView(1, tpl.CreateFullView())
        # check that we get error and empty model
        mhandle = modelling.BuildRawModel(aln)
        self.assertEqual(len(log.messages['ERROR']), 1)
        self.assertEqual(log.messages['ERROR'][-1],
                         "No residues added for chain A! This typically "\
                         "indicates that the template is a Calpha trace "\
                         "or only contains D-peptides.")
        self.assertEqual(mhandle.model.residue_count, 0)
        # check that we produce an error when trying to build model
        final_model = modelling.BuildFromRawModel(mhandle)
        self.assertEqual(len(log.messages['ERROR']), 2)
        self.assertEqual(log.messages['ERROR'][-1],
                         "Cannot perform modelling with an empty raw model.")
        self.assertEqual(final_model.residue_count, 0)

    def testCheckFinalModelShort(self):
        '''Check that we report chains with only 2 residues.'''
        # setup log
        log = _FetchLog()
        ost.PushLogSink(log)
        ost.PushVerbosityLevel(1)
        # setup raw model
        mhandle = self.getMockModel(io.LoadPDB('data/modelCApartial-5tgl1A.pdb'))
        # check
        log.messages['WARNING'] = list()
        modelling.CheckFinalModel(mhandle)
        self.assertEqual(len(log.messages['WARNING']), 1)
        self.assertEqual(len(mhandle.modelling_issues), 1)
        exp_msg = "Chain A of returned model contains only 2 residues! "\
                  "This typically indicates that the template is mostly a "\
                  "Calpha trace or contains too many D-peptides."
        self.assertEqual(log.messages['WARNING'][-1], exp_msg)
        issue = mhandle.modelling_issues[0]
        self.assertEqual(issue.text, exp_msg)
        self.assertTrue(issue.is_major())
        self.assertEqual(len(issue.residue_list), 0)

if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()
