# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Unit tests for alignment fiddling.
"""
import unittest
from promod3 import modelling
from ost import seq, io

class ModellingTests(unittest.TestCase):

    def testDeleteGapCols(self):
        aln = io.LoadAlignment('data/double_gap_aln.fasta')
        s0_orig = aln.GetSequence(0)
        s1_orig = aln.GetSequence(1)
        # recreate aln with added offsets and attach some random structure to 
        # both sequences
        s0 = seq.CreateSequence(s0_orig.GetName(), str(s0_orig))
        s1 = seq.CreateSequence(s1_orig.GetName(), str(s1_orig))
        s0.SetOffset(1)
        s1.SetOffset(2)
        random_structure = io.LoadPDB('data/1CRN.pdb')
        s0.AttachView(random_structure.CreateFullView())
        s1.AttachView(random_structure.CreateFullView())
        pimped_aln = seq.CreateAlignment()
        pimped_aln.AddSequence(s0)
        pimped_aln.AddSequence(s1)
        processed_aln = modelling.DeleteGapCols(pimped_aln)
        processed_s0 = processed_aln.GetSequence(0)
        processed_s1 = processed_aln.GetSequence(1)
       
        # the following things should be equal between sx and processed_sx:
        # - name
        # - offset
        # - view attached
        self.assertEqual(s0.GetName(), processed_s0.GetName())
        self.assertEqual(s1.GetName(), processed_s1.GetName())
        self.assertEqual(s0.GetOffset(), processed_s0.GetOffset())
        self.assertEqual(s1.GetOffset(), processed_s1.GetOffset())
        self.assertTrue(processed_s0.HasAttachedView())
        self.assertTrue(processed_s1.HasAttachedView())

        # the actual sequences changed and are checked with the expected outcome
        self.assertEqual(str(processed_s0), 'ABCDEFG-H--I')
        self.assertEqual(str(processed_s1), 'ABCDEFGHIJKL')


    def testPullTerminalDeletions(self):

        aln = io.LoadAlignment('data/terminal_deletion_aln.fasta')

        # min_terminal_anchor_sizes which are <= 0 are disallowed
        self.assertRaises(RuntimeError, modelling.PullTerminalDeletions, aln, -10)
        self.assertRaises(RuntimeError, modelling.PullTerminalDeletions, aln, 0)

        # check expected results for different min_terminal_anchor_sizes

        # nothing should happen for anchor size 1
        processed_aln = modelling.PullTerminalDeletions(aln, 1)
        self.assertEqual(str(aln.GetSequence(0)), str(processed_aln.GetSequence(0)))
        self.assertEqual(str(aln.GetSequence(1)), str(processed_aln.GetSequence(1)))

        processed_aln = modelling.PullTerminalDeletions(aln, 2)
        exp_s0 = 'GSHMG----DLK--------------YSLERLREILERLEENPSEKQIVEAIRAIVENNAQIVE--AAIE-NNAQIVENNRAIIEALEAIGVDQKILEEMKKQLKDLKRSLERG'
        exp_s1 = '-----DAQDKLKYLVKQLERALRELKKSLDELERSLEELEKNPSEDALVENNRLNVENNKIIVEVLRIILE-------------------------------------------'
        self.assertEqual(str(processed_aln.GetSequence(0)), exp_s0)
        self.assertEqual(str(processed_aln.GetSequence(1)), exp_s1)

        processed_aln = modelling.PullTerminalDeletions(aln, 3)
        exp_s0 = 'GSHMG----DLK--------------YSLERLREILERLEENPSEKQIVEAIRAIVENNAQIVE--AAIE-NNAQIVENNRAIIEALEAIGVDQKILEEMKKQLKDLKRSLERG'
        exp_s1 = '-----DAQDKLKYLVKQLERALRELKKSLDELERSLEELEKNPSEDALVENNRLNVENNKIIVEVLRIILE-------------------------------------------'
        self.assertEqual(str(processed_aln.GetSequence(0)), exp_s0)
        self.assertEqual(str(processed_aln.GetSequence(1)), exp_s1)

        processed_aln = modelling.PullTerminalDeletions(aln, 4)
        exp_s0 = 'GSHMG------------------DLKYSLERLREILERLEENPSEKQIVEAIRAIVENNAQIVE--AAIE-NNAQIVENNRAIIEALEAIGVDQKILEEMKKQLKDLKRSLERG'
        exp_s1 = '-----DAQDKLKYLVKQLERALRELKKSLDELERSLEELEKNPSEDALVENNRLNVENNKIIVEVLRIILE-------------------------------------------'
        self.assertEqual(str(processed_aln.GetSequence(0)), exp_s0)
        self.assertEqual(str(processed_aln.GetSequence(1)), exp_s1)

        processed_aln = modelling.PullTerminalDeletions(aln, 5)
        exp_s0 = 'GSHMG------------------DLKYSLERLREILERLEENPSEKQIVEAIRAIVENNAQIVEAAIE---NNAQIVENNRAIIEALEAIGVDQKILEEMKKQLKDLKRSLERG'
        exp_s1 = '-----DAQDKLKYLVKQLERALRELKKSLDELERSLEELEKNPSEDALVENNRLNVENNKIIVEVLRIILE-------------------------------------------'
        self.assertEqual(str(processed_aln.GetSequence(0)), exp_s0)
        self.assertEqual(str(processed_aln.GetSequence(1)), exp_s1)

        # also check, whether sequence name, offsets and attached views are preserved
        random_structure = io.LoadPDB('data/1CRN.pdb')
        s0 = seq.CreateSequence(aln.GetSequence(0).GetName(), str(aln.GetSequence(0)))
        s1 = seq.CreateSequence(aln.GetSequence(1).GetName(), str(aln.GetSequence(1)))
        s0.AttachView(random_structure.CreateFullView())
        s1.AttachView(random_structure.CreateFullView())
        pimped_aln = seq.CreateAlignment()
        pimped_aln.AddSequence(s0)
        pimped_aln.AddSequence(s1)
        processed_aln = modelling.PullTerminalDeletions(pimped_aln, 5)
        processed_s0 = processed_aln.GetSequence(0)
        processed_s1 = processed_aln.GetSequence(1)
       
        # the following things should be equal between sx and processed_sx:
        # - name
        # - offset
        # - view attached
        self.assertEqual(s0.GetName(), processed_s0.GetName())
        self.assertEqual(s1.GetName(), processed_s1.GetName())
        self.assertEqual(s0.GetOffset(), processed_s0.GetOffset())
        self.assertEqual(s1.GetOffset(), processed_s1.GetOffset())
        self.assertTrue(processed_s0.HasAttachedView())
        self.assertTrue(processed_s1.HasAttachedView())


if __name__ == "__main__":
    from ost import testutils
    testutils.RunTests()

