// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <promod3/modelling/model.hh>
#include <promod3/core/export_helper.hh>

using namespace boost::python;
using namespace promod3::modelling;


namespace {

ModellingHandle WrapMHandleFromAln(const ost::seq::AlignmentList& aln,
                                   const boost::python::list& chain_names,
                                   bool include_ligands,
                                   bool spdbv_style) {
  std::vector<String> v_chain_names;
  promod3::core::ConvertListToVector(chain_names, v_chain_names);
  return MHandleFromAln(aln, v_chain_names, include_ligands, spdbv_style);
}

int WrapCountEnclosedGaps(ModellingHandle& mhandle, const StructuralGap& gap) {
  return CountEnclosedGaps(mhandle, gap, false);
}

int WrapCountEnclosedIns(ModellingHandle& mhandle, const StructuralGap& gap) {
  return CountEnclosedGaps(mhandle, gap, true);
}

promod3::scoring::BackboneOverallScorerPtr
WrapGetBackboneScorer(ModellingHandle& mhandle) {
  if (!mhandle.backbone_scorer) {
    throw promod3::Error("The backbone_scorer must be properly initialized "
                         "before it can be used!");
  }
  return mhandle.backbone_scorer;
}

void WrapSetBackboneScorer(ModellingHandle& mhandle,
                           promod3::scoring::BackboneOverallScorerPtr scorer) {
  mhandle.backbone_scorer = scorer;
}

promod3::scoring::BackboneScoreEnvPtr
WrapGetBackboneScorerEnv(ModellingHandle& mhandle) {
  if (!mhandle.backbone_scorer_env) {
    throw promod3::Error("The backbone_scorer_env must be properly initialized "
                         "before it can be used!");
  }
  return mhandle.backbone_scorer_env;
}

void WrapSetBackboneScorerEnv(ModellingHandle& mhandle,
                              promod3::scoring::BackboneScoreEnvPtr env) {
  mhandle.backbone_scorer_env = env;
}

promod3::scoring::AllAtomOverallScorerPtr
WrapGetAllAtomScorer(ModellingHandle& mhandle) {
  if (!mhandle.all_atom_scorer) {
    throw promod3::Error("The all_atom_scorer must be properly initialized "
                         "before it can be used!");
  }
  return mhandle.all_atom_scorer;
}

void WrapSetAllAtomScorer(ModellingHandle& mhandle,
                          promod3::scoring::AllAtomOverallScorerPtr scorer) {
  mhandle.all_atom_scorer = scorer;
}

promod3::loop::AllAtomEnvPtr
WrapGetAllAtomScorerEnv(ModellingHandle& mhandle) {
  if (!mhandle.all_atom_scorer_env) {
    throw promod3::Error("The all_atom_scorer_env must be properly initialized "
                         "before it can be used!");
  }
  return mhandle.all_atom_scorer_env;
}

void WrapSetAllAtomScorerEnv(ModellingHandle& mhandle,
                             promod3::loop::AllAtomEnvPtr env) {
  mhandle.all_atom_scorer_env = env;
}

promod3::loop::AllAtomEnvPtr
WrapGetAllAtomSidechainEnv(ModellingHandle& mhandle) {
  if (!mhandle.all_atom_sidechain_env) {
    throw promod3::Error("The all_atom_sidechain_env must be properly "
                         "initialized before it can be used!");
  }
  return mhandle.all_atom_sidechain_env;
}

void WrapSetAllAtomSidechainEnv(ModellingHandle& mhandle,
                                promod3::loop::AllAtomEnvPtr env) {
  mhandle.all_atom_sidechain_env = env;
}

promod3::modelling::SidechainReconstructorPtr
WrapGetSidechainRec(ModellingHandle& mhandle) {
  if (!mhandle.sidechain_reconstructor) {
    throw promod3::Error("The sidechain_reconstructor must be properly "
                         "initialized before it can be used!");
  }
  return mhandle.sidechain_reconstructor;
}

void WrapSetSidechainRec(ModellingHandle& mhandle,
                         promod3::modelling::SidechainReconstructorPtr sc_rec) {
  mhandle.sidechain_reconstructor = sc_rec;
}

void WrapSetSequenceProfiles(ModellingHandle& mhandle,
                             const boost::python::list& profiles){
  ost::seq::ProfileHandleList v_profiles;
  promod3::core::ConvertListToVector(profiles, v_profiles);
  SetSequenceProfiles(mhandle, v_profiles);
}

void WrapSetPsipredPredictions(ModellingHandle& mhandle,
                               const boost::python::list& predictions){
  promod3::loop::PsipredPredictionList v_predictions;
  promod3::core::ConvertListToVector(predictions, v_predictions);
  SetPsipredPredictions(mhandle, v_predictions);
}

boost::python::list WrapPsipredPredictionAccess(ModellingHandle& mhandle){
  boost::python::list return_list;
  promod3::core::AppendVectorToList(mhandle.psipred_predictions, return_list);
  return return_list;
}

boost::python::list WrapProfileAccess(ModellingHandle& mhandle){
  boost::python::list return_list;
  promod3::core::AppendVectorToList(mhandle.profiles, return_list);
  return return_list;
}

} // anon ns

void export_model()
{
  class_<ModellingHandle>("ModellingHandle", init<>())
    .def("Copy", &ModellingHandle::Copy)
    .def_readwrite("model", &ModellingHandle::model)
    .def_readwrite("gaps", &ModellingHandle::gaps)
    .add_property("seqres", &ModellingHandle::seqres)
    .add_property("psipred_predictions", &WrapPsipredPredictionAccess)
    .add_property("profiles", &WrapProfileAccess)
    .add_property("backbone_scorer", &WrapGetBackboneScorer,
                                     &WrapSetBackboneScorer)
    .add_property("backbone_scorer_env", &WrapGetBackboneScorerEnv,
                                         &WrapSetBackboneScorerEnv)
    .add_property("all_atom_scorer", &WrapGetAllAtomScorer,
                                     &WrapSetAllAtomScorer)
    .add_property("all_atom_scorer_env", &WrapGetAllAtomScorerEnv,
                                         &WrapSetAllAtomScorerEnv)
    .add_property("all_atom_sidechain_env", &WrapGetAllAtomSidechainEnv,
                                            &WrapSetAllAtomSidechainEnv)
    .add_property("sidechain_reconstructor", &WrapGetSidechainRec,
                                             &WrapSetSidechainRec)
  ;

  def("ClearGaps", ClearGaps, (arg("mhandle"), arg("gap")));
  def("CountEnclosedGaps", WrapCountEnclosedGaps, (arg("mhandle"),
                                                   arg("gap")));
  def("CountEnclosedInsertions", WrapCountEnclosedIns, (arg("mhandle"),
                                                        arg("gap")));
  def("MergeGaps", MergeGaps, (arg("mhandle"),arg("index")));
  def("RemoveTerminalGaps", RemoveTerminalGaps, (arg("mhandle")));
  def("ReorderGaps", &ReorderGaps, (arg("mhandle")));
  def("SetupDefaultBackboneScoring", &SetupDefaultBackboneScoring,
      (arg("mhandle")));
  def("IsBackboneScoringSetUp", &IsBackboneScoringSetUp, (arg("mhandle")));
  def("SetupDefaultAllAtomScoring", &SetupDefaultAllAtomScoring,
      (arg("mhandle")));
  def("IsAllAtomScoringSetUp", &IsAllAtomScoringSetUp, (arg("mhandle")));
  def("SetSequenceProfiles", &WrapSetSequenceProfiles,
      (arg("mhandle"), arg("profiles")));
  def("SetPsipredPredictions", &WrapSetPsipredPredictions,
      (arg("mhandle"), arg("psipred_predictions")));
  def("MergeMHandle", &MergeMHandle, (arg("source_mhandle"),
                                      arg("target_mhandle"),
                                      arg("source_chain_idx"),
                                      arg("target_chain_idx"),
                                      arg("start_resnum"),
                                      arg("end_resnum"),
                                      arg("transform")));
  def("InsertLoop", InsertLoop,
      (arg("mhandle"), arg("bb_list"), arg("start_resnum"), arg("chain_idx")));
  def("InsertLoopClearGaps", InsertLoopClearGaps,
      (arg("mhandle"), arg("bb_list"), arg("gap")));
  def("MHandleFromAln", &WrapMHandleFromAln, (arg("aln"), arg("chain_names"), 
                                              arg("include_ligands")=false, 
                                              arg("spdbv_style")=false));
}
