// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/shared_ptr.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/modelling/motif_finder.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;


boost::shared_ptr<MotifQuery> WrapInitPositions(const geom::Vec3List& positions, 
                                                const String& identifier,
                                                Real min_triangle_edge_length,
                                                Real max_triangle_edge_length,
                                                Real bin_size) {
  
  boost::shared_ptr<MotifQuery> ptr(new MotifQuery(positions, identifier,
                                                   min_triangle_edge_length,
                                                   max_triangle_edge_length,
                                                   bin_size));
  return ptr;
}

boost::shared_ptr<MotifQuery> WrapInitPositionsFlags(const geom::Vec3List& positions, 
                                                     const String& identifier,
                                                     Real min_triangle_edge_length,
                                                     Real max_triangle_edge_length,
                                                     Real bin_size,
                                                     const list& flags) {
  std::vector<int> v_flags;
  promod3::core::ConvertListToVector(flags, v_flags);
  boost::shared_ptr<MotifQuery> ptr(new MotifQuery(positions, identifier,
                                                   min_triangle_edge_length,
                                                   max_triangle_edge_length,
                                                   bin_size, v_flags));
  return ptr;
}

boost::shared_ptr<MotifQuery> WrapInitQueries(const list& queries) {

  std::vector<MotifQuery> v_queries;
  for(uint i = 0; i < len(queries); ++i) {
    v_queries.push_back(extract<MotifQuery>(queries[i]));
  }
  boost::shared_ptr<MotifQuery> ptr(new MotifQuery(v_queries));
  return ptr;
}


boost::python::list WrapFindMotifs(const MotifQuery& query,
                                   const geom::Vec3List& positions,
                                   Real hash_thresh,
                                   Real distance_thresh,
                                   Real refine_thresh,
                                   const boost::python::list& flags,
                                   bool swap_thresh) {

  std::vector<int> v_flags;
  promod3::core::ConvertListToVector(flags, v_flags);

  std::vector<MotifMatch> v_result = FindMotifs(query, positions, 
                                                hash_thresh,
                                                distance_thresh,
                                                refine_thresh,
                                                v_flags,
                                                swap_thresh);
  list return_list;
  for(std::vector<MotifMatch>::iterator it = v_result.begin();
      it != v_result.end(); ++it) {
    return_list.append(*it);
  }
  return return_list;
}


boost::python::list WrapGetAlignment(const MotifMatch& match) {

  std::vector<std::pair<int,int> > aln = match.aln;
  list return_list;
  for(std::vector<std::pair<int,int> >::iterator it = aln.begin();
      it != aln.end(); ++it) {
    return_list.append(boost::python::make_tuple(it->first, it->second));
  }
  return return_list;
}


size_t GetNTrianglesSingleQuery(const MotifQuery& query, uint idx) {
  return query.GetNTriangles(idx);
}


size_t GetNTrianglesFull(const MotifQuery& query) {
  return query.GetNTriangles();
}


void export_motif_finder() {

  class_<Triangle>("Triangle", no_init)
    .def_readonly("p1", &Triangle::p1)
    .def_readonly("p2", &Triangle::p2)
    .def_readonly("p3", &Triangle::p3)
  ;


  class_<MotifQuery>("MotifQuery", no_init)
    .def("__init__", make_constructor(&WrapInitPositions))
    .def("__init__", make_constructor(&WrapInitPositionsFlags))
    .def("__init__", make_constructor(&WrapInitQueries))
    .def("Load", &MotifQuery::Load, (arg("filename"))).staticmethod("Load")
    .def("Save", &MotifQuery::Save, (arg("filename")))
    .def("GetPositions", &MotifQuery::GetPositions, 
         return_value_policy<copy_const_reference>(), (arg("query_idx")))
    .def("GetIdentifiers", &MotifQuery::GetIdentifiers, 
         return_value_policy<copy_const_reference>())
    .def("GetN", &MotifQuery::GetN)
    .def("GetQuerySize", &MotifQuery::GetQuerySize, (arg("query_idx")))
    .def("GetNTriangles", &GetNTrianglesSingleQuery, (arg("query_idx")))
    .def("GetNTriangles", &GetNTrianglesFull)
    .def("GetMaxTriangleEdgeLength", &MotifQuery::GetMaxTriangleEdgeLength)
    .def("GetTriangle", &MotifQuery::GetTriangle,(arg("query_idx"),
                                                  arg("triangle_idx")))
    .def("Prune", &MotifQuery::Prune, (arg("factor")))
    .def("PrintBinSizes", &MotifQuery::PrintBinSizes)
  ;


  class_<MotifMatch>("MotifMatch", no_init)
    .def_readonly("query_idx", &MotifMatch::query_idx)
    .def_readonly("triangle_idx", &MotifMatch::triangle_idx)
    .def_readonly("mat", &MotifMatch::mat)
    .add_property("alignment", &WrapGetAlignment)
  ;


  def("FindMotifs", &WrapFindMotifs, (arg("query"), arg("target_positions"), 
                                      arg("hash_thresh")=0.4,
                                      arg("distance_thresh")=1.0,
                                      arg("refine_thresh")=0.7,
                                      arg("flags")=boost::python::list(),
                                      arg("swap_thresh")=false));
}
