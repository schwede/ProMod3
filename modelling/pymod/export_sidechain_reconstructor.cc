// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>
#include <promod3/modelling/sidechain_reconstructor.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;

namespace {

boost::python::list
WrapGetLoopStartIndices(const SidechainReconstructionData& sc_rec_data) {
  boost::python::list return_list;
  core::AppendVectorToList(sc_rec_data.loop_start_indices, return_list);
  return return_list;
}

boost::python::list
WrapGetLoopLengths(const SidechainReconstructionData& sc_rec_data) {
  boost::python::list return_list;
  core::AppendVectorToList(sc_rec_data.loop_lengths, return_list);
  return return_list;
}

boost::python::list
WrapGetRotamerResIndices(const SidechainReconstructionData& sc_rec_data) {
  boost::python::list return_list;
  core::AppendVectorToList(sc_rec_data.rotamer_res_indices, return_list);
  return return_list;
}

boost::python::list
WrapGetDisulfidBridges(const SidechainReconstructionData& sc_rec_data) {
  boost::python::list return_list;
  core::AppendVectorToList(sc_rec_data.disulfid_bridges, return_list);
  return return_list;
}

boost::python::list
WrapGetIsNTer(const SidechainReconstructionData& sc_rec_data) {
  boost::python::list return_list;
  core::AppendVectorToList(sc_rec_data.is_n_ter, return_list);
  return return_list;
}

boost::python::list
WrapGetIsCTer(const SidechainReconstructionData& sc_rec_data) {
  boost::python::list return_list;
  core::AppendVectorToList(sc_rec_data.is_c_ter, return_list);
  return return_list;
}

SidechainReconstructionDataPtr
WrapReconstruct(SidechainReconstructor& sc_rec, uint start_resnum,
                uint num_residues, uint chain_idx) {
  return sc_rec.Reconstruct(start_resnum, num_residues, chain_idx);
}

SidechainReconstructionDataPtr
WrapReconstructRN(SidechainReconstructor& sc_rec,
                  const ost::mol::ResNum& start_resnum,
                  uint num_residues, uint chain_idx) {
  uint num = start_resnum.GetNum();
  return sc_rec.Reconstruct(num, num_residues, chain_idx);
}

SidechainReconstructionDataPtr
WrapReconstructInputList(SidechainReconstructor& sc_rec,
                         const boost::python::list& start_resnums,
                         const boost::python::list& num_residues,
                         const boost::python::list& chain_indices){

  std::vector<uint> v_start_resnums;
  std::vector<uint> v_num_residues;
  std::vector<uint> v_chain_indices;

  core::ConvertListToVector(start_resnums, v_start_resnums);
  core::ConvertListToVector(num_residues, v_num_residues);
  core::ConvertListToVector(chain_indices, v_chain_indices);

  return sc_rec.Reconstruct(v_start_resnums, v_num_residues, v_chain_indices);
}

template<typename LibType> void
WrapAttachEnvironment(SidechainReconstructor& sc_rec, loop::AllAtomEnv& env,
                      bool use_frm, LibType lib_param) {
  sc_rec.AttachEnvironment(env, use_frm, lib_param);
}

} // anon wrapper ns

void export_SidechainReconstructor() {

  class_<SidechainReconstructionData, SidechainReconstructionDataPtr>
    ("SidechainReconstructionData", init<>())
    .def_readonly("env_pos", &SidechainReconstructionData::env_pos)
    .add_property("loop_start_indices", &WrapGetLoopStartIndices)
    .add_property("loop_lengths", &WrapGetLoopLengths)
    .add_property("rotamer_res_indices", &WrapGetRotamerResIndices)
    .add_property("disulfid_bridges", &WrapGetDisulfidBridges)
    .add_property("is_n_ter", &WrapGetIsNTer)
    .add_property("is_c_ter", &WrapGetIsCTer)
  ;

  class_<SidechainReconstructor, SidechainReconstructorPtr>
    ("SidechainReconstructor", no_init)
    .def(init<bool, bool, bool, Real, Real, uint64_t, Real, Real>(
         (arg("keep_sidechains")=true, 
          arg("build_disulfids")=true,
          arg("optimize_subrotamers")=false,
          arg("remodel_cutoff")=20, 
          arg("rigid_frame_cutoff")=0,
          arg("graph_max_complexity")=100000000,
          arg("graph_intial_epsilon")=0.02,
          arg("disulfid_score_thresh")=45)))
    .def("Reconstruct", WrapReconstruct,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx")=0))
    .def("Reconstruct", WrapReconstructRN,
         (arg("start_resnum"), arg("num_residues"), arg("chain_idx")=0))
    .def("Reconstruct", WrapReconstructInputList,
         (arg("start_resnum_list"), arg("num_residues_list"), arg("chain_idx_list")))
    .def("AttachEnvironment", WrapAttachEnvironment<bool>,
         (arg("env"), arg("use_frm")=true, arg("use_bbdep_lib")=true))
    .def("AttachEnvironment", WrapAttachEnvironment<sidechain::BBDepRotamerLibPtr>,
         (arg("env"), arg("use_frm"), arg("rotamer_library")))
    .def("AttachEnvironment", WrapAttachEnvironment<sidechain::RotamerLibPtr>,
         (arg("env"), arg("use_frm"), arg("rotamer_library")))
  ;
}
