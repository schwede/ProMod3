// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/modelling/ccd.hh>
#include <promod3/modelling/kic.hh>
#include <promod3/modelling/backbone_relaxer.hh>
#include <promod3/modelling/all_atom_relaxer.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;

namespace {

// CCD wrappers

CCDPtr ccd_constructor_one(const String& sequence,
                           const ost::mol::ResidueHandle& before,
                           const ost::mol::ResidueHandle& after, 
                           loop::TorsionSamplerPtr torsion_sampler,
                           uint max_steps, Real rmsd_cutoff, int seed) {
  CCDPtr p(new CCD(sequence, before, after, torsion_sampler, max_steps,
                   rmsd_cutoff, seed));
  return p;
}

CCDPtr ccd_constructor_two(const String& sequence,
                           const ost::mol::ResidueHandle& before,
                           const ost::mol::ResidueHandle& after,
                           const boost::python::list& torsion_sampler,
                           uint max_steps, Real rmsd_cutoff, int seed) {
  loop::TorsionSamplerList v_samplers;
  core::ConvertListToVector(torsion_sampler, v_samplers);
  CCDPtr p(new CCD(sequence, before, after, v_samplers, max_steps, rmsd_cutoff,
                   seed));
  return p;
}

CCDPtr ccd_constructor_three(const ost::mol::ResidueHandle& before, 
                             const ost::mol::ResidueHandle& after, 
                             uint max_steps, Real rmsd_cutoff) {
  CCDPtr p(new CCD(before, after, max_steps, rmsd_cutoff));
  return p;
}

// KIC wrappers

boost::python::list WrapKICClose(const KIC& closer,
                                 const loop::BackboneList& bb_list, 
                                 uint pivot_one, uint pivot_two, uint pivot_three){
  std::vector<loop::BackboneList> closed_loops;
  closer.Close(bb_list, pivot_one,pivot_two,pivot_three,closed_loops);
  boost::python::list return_list;
  core::AppendVectorToList(closed_loops, return_list);
  return return_list;
}

}

void export_loop_closure() {
  
  class_<CCD, CCDPtr>("CCD", no_init)
    .def("__init__", make_constructor(&ccd_constructor_one))
    .def("__init__", make_constructor(&ccd_constructor_two))
    .def("__init__", make_constructor(&ccd_constructor_three))
    .def("Close", &CCD::Close, (arg("bb_list")))
  ;

  class_<KIC, KICPtr>("KIC", no_init)
    .def(init<const ost::mol::ResidueHandle&, const ost::mol::ResidueHandle>(
         (arg("n_stem"), arg("c_stem"))))
    .def("Close", &WrapKICClose,
         (arg("bb_list"), arg("pivot_one"), arg("pivot_two"),
          arg("pivot_three")))
  ;

  class_<BackboneRelaxer, BackboneRelaxerPtr>("BackboneRelaxer", no_init)
    .def(init<const loop::BackboneList&, promod3::loop::ForcefieldLookupPtr, 
              bool, bool>(
         (arg("bb_list"), arg("ff_lookup"), arg("fix_nterm")=true, 
          arg("fix_cterm")=true)))
    .def("AddNRestraint", &BackboneRelaxer::AddNRestraint,
         (arg("idx"), arg("pos"), arg("force_constaint")=100000.0))
    .def("AddCARestraint", &BackboneRelaxer::AddCARestraint,
         (arg("idx"), arg("pos"), arg("force_constaint")=100000.0))
    .def("AddCBRestraint", &BackboneRelaxer::AddCBRestraint,
         (arg("idx"), arg("pos"), arg("force_constaint")=100000.0))
    .def("AddCRestraint", &BackboneRelaxer::AddCRestraint,
         (arg("idx"), arg("pos"), arg("force_constaint")=100000.0))
    .def("AddORestraint", &BackboneRelaxer::AddORestraint,
         (arg("idx"), arg("pos"), arg("force_constaint")=100000.0))
    .def("SetNonBondedCutoff", &BackboneRelaxer::SetNonBondedCutoff,
         (arg("nonbonded_cutoff")))
    .def("GetNonBondedCutoff", &BackboneRelaxer::GetNonBondedCutoff)
    .def("Run", &BackboneRelaxer::Run,
         (arg("bb_list"), arg("steps")=100, arg("stop_criterion")=0.01))
  ;

  class_<AllAtomRelaxer, AllAtomRelaxerPtr>("AllAtomRelaxer", no_init)
    .def(init<modelling::SidechainReconstructionDataPtr,
              loop::MmSystemCreatorPtr>(
         (arg("sc_data"), arg("mm_system_creator"))))
    .def("UpdatePositions", &AllAtomRelaxer::UpdatePositions, (arg("sc_data")))
    .def("Run", &AllAtomRelaxer::Run,
         (arg("sc_data"), arg("steps")=100, arg("stop_criterion")=0.01))
    .def("GetSystemCreator", &AllAtomRelaxer::GetSystemCreator)
  ;

}
