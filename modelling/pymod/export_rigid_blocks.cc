// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/export_helper.hh>

#include <promod3/modelling/rigid_blocks.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;

// WRAPPERS
namespace {

boost::python::tuple WrapRigidBlocksBBList(const promod3::loop::BackboneList& one,
                                          const promod3::loop::BackboneList& two,
                                          uint window_length,
                                          uint max_iterations,
                                          Real distance_thresh,
                                          Real cluster_thresh){
  
  std::vector<std::vector<uint> > indices;
  std::vector<geom::Mat4> transformations;
  promod3::modelling::RigidBlocks(one, two, window_length,
                                  max_iterations, distance_thresh,
                                  cluster_thresh, indices, transformations);
  boost::python::list idx_return_list;
  boost::python::list t_return_list;
  for(uint i = 0; i < indices.size(); ++i){
    boost::python::list idx_list;
    core::AppendVectorToList(indices[i], idx_list);
    idx_return_list.append(idx_list);
  }
  core::AppendVectorToList(transformations, t_return_list);

  return boost::python::make_tuple(idx_return_list, t_return_list);
}

boost::python::tuple WrapRigidBlocksAln(const ost::seq::AlignmentHandle& aln,
                                       uint seq_one_idx, uint seq_two_idx,
                                       uint window_length,
                                       uint max_iterations,
                                       Real distance_thresh,
                                       Real cluster_thresh){
  
  std::vector<std::vector<uint> > indices;
  std::vector<geom::Mat4> transformations;
  promod3::modelling::RigidBlocks(aln, seq_one_idx, seq_two_idx, window_length,
                                  max_iterations, distance_thresh, 
                                  cluster_thresh, indices, transformations);
  boost::python::list idx_return_list;
  boost::python::list t_return_list;
  for(uint i = 0; i < indices.size(); ++i){
    boost::python::list idx_list;
    core::AppendVectorToList(indices[i],idx_list);
    idx_return_list.append(idx_list);
  }
  core::AppendVectorToList(transformations, t_return_list);

  return boost::python::make_tuple(idx_return_list, t_return_list);
}

boost::python::tuple WrapRigidBlocksVecList(const geom::Vec3List& pos_one,
                                            const geom::Vec3List& pos_two,
                                            uint window_length,
                                            uint max_iterations,
                                            Real distance_thresh,
                                            Real cluster_thresh){
  
  std::vector<std::vector<uint> > indices;
  std::vector<geom::Mat4> transformations;
  promod3::modelling::RigidBlocks(pos_one, pos_two, window_length,
                                  max_iterations, distance_thresh, 
                                  cluster_thresh, indices, transformations);
  boost::python::list idx_return_list;
  boost::python::list t_return_list;
  for(uint i = 0; i < indices.size(); ++i){
    boost::python::list idx_list;
    core::AppendVectorToList(indices[i],idx_list);
    idx_return_list.append(idx_list);
  }
  core::AppendVectorToList(transformations, t_return_list);

  return boost::python::make_tuple(idx_return_list, t_return_list);
}


}//ns

void export_rigid_blocks() {

  def("RigidBlocks",&WrapRigidBlocksBBList,(arg("bb_list_one"),arg("bb_list_two"),arg("window_length")=12,arg("max_iterations")=20,arg("distance_thresh")=3.0,arg("cluster_thresh")=0.9));

  def("RigidBlocks",&WrapRigidBlocksAln,(arg("aln"),arg("seq_one_idx")=0,arg("seq_two_idx")=1,arg("window_length")=12,arg("max_iterations")=20,arg("distance_thresh")=3.0,arg("cluster_thresh")=0.9));

  def("RigidBlocks",&WrapRigidBlocksVecList,(arg("pos_one"), arg("pos_two"), arg("window_length")=12,arg("max_iterations")=20,arg("distance_thresh")=3.0,arg("cluster_thresh")=0.9));

}
