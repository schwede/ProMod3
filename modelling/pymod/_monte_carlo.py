# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import random
import math


def SampleMonteCarlo(sampler, closer, scorer, cooler, steps, bb_list, 
                     initialize, seed = 0, lowest_energy_conformation = True):
    '''  A convenient function to perform Monte Carlo sampling using a simulated
    annealing scheme. In every iteration, a new loop conformation gets proposed 
    by the provided *sampler* and closed by the *closer*. Upon scoring, this new
    conformation gets accepted/rejected using a metropolis criterion based on 
    the temperature given by the *cooler* 
    => acceptance probability: exp(-delta_score/T).
    The result is stored in *bb_list* (passed by reference, so NO return value) 
    and is either the lowest energy conformation ever encountered or the last 
    accepted proposal.

    :param sampler:       Sampler object capable of initializing and altering
                          conformations.
    :param closer:        Closer object to adapt a new conformation to
                          the environment.
    :param scorer:        Scorer object to score new loop conformations.
    :param cooler:        Cooler object to control the temperature of the 
                          Monte Carlo trajectory.
    :param steps:         Number of Monte Carlo iterations to be performed.
    :param bb_list:       The chosen conformation gets stored here.
    :param initialize:    Whether a new bb_list should be generated as starting
                          point, based on the samplers Initialize function.
                          The input *bb_list* gets used otherwise.
    :param seed:          Seed for internal random number generator.
    :param lowest_energy_conformation:  If True, we choose the lowest scoring
                                        conformation of the trajectory. 
                                        Otherwise, the last accepted proposal.

    :type sampler:        :ref:`mc-sampler-object`
    :type closer:         :ref:`mc-closer-object`
    :type scorer:         :ref:`mc-scorer-object`
    :type cooler:         :ref:`mc-cooler-object`
    :type steps:          :class:`int`
    :type bb_list:        :class:`~promod3.loop.BackboneList`
    :type initialize:     :class:`bool`
    :type seed:           :class:`int`
    :type lowest_energy_conformation: :class:`bool`
    '''

    # Initialize from scratch if necessary
    if initialize:
        initialized = False
        for i in range(100):
            sampler.Initialize(bb_list)
            if closer.Close(bb_list, bb_list):
                initialized = True
                break
        if not initialized:
            raise RuntimeError("Failed to initialize monte carlo protocol!")

    # setup
    random.seed(seed)
    score = scorer.GetScore(bb_list)
    closed = False
    min_score = score
    min_score_bb_list = bb_list.Copy()
    proposed_bb_list = bb_list.Copy()

    for i in range(steps):

        # try several proposals (we might not be able to close at first attempt)
        closed = False
        for j in range(3):
            sampler.ProposeStep(bb_list, proposed_bb_list)
            if closer.Close(proposed_bb_list, proposed_bb_list):
                closed = True
                break

        # check for success, try again in next step in case of failure
        if not closed:
            continue

        # accept / reject based on Metropolis criterion
        temperature = cooler.GetTemperature()
        new_score = scorer.GetScore(proposed_bb_list)

        if math.exp(-(new_score-score)/temperature) > random.random():
            positions = proposed_bb_list.Copy()
            score = new_score
            if lowest_energy_conformation and score < min_score:
                min_score = score
                min_score_bb_list = positions

    if lowest_energy_conformation:
        bb_list = min_score_bb_list
