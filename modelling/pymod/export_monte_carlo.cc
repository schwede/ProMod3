// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/raw_function.hpp>

#include <promod3/core/export_helper.hh>
#include <promod3/modelling/monte_carlo.hh>
#include <promod3/modelling/monte_carlo_cooler.hh>
#include <promod3/modelling/monte_carlo_scorer.hh>
#include <promod3/modelling/monte_carlo_sampler.hh>
#include <promod3/modelling/monte_carlo_closer.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;


namespace {

object PhiPsiInitWrapper(tuple args, dict kwargs){

  object self = args[0];
  args = tuple(args.slice(1,_));

  if(len(args) != 2){
    std::stringstream ss;
    ss << "Need to define sequence and torsion_sampler as non keyword arguments, ";
    ss << "all other arguments must be keyword arguments!";
    throw promod3::Error(ss.str());
  }

  String seq = extract<String>(args[0]);

  Real n_stem_phi = -1.0472;
  if(kwargs.contains("n_stem_phi")){
    n_stem_phi = extract<Real>(kwargs["n_stem_phi"]);
    kwargs["n_stem_phi"].del();
  }

  Real c_stem_psi = -0.78540;
  if(kwargs.contains("c_stem_psi")){
    c_stem_psi = extract<Real>(kwargs["c_stem_psi"]);
    kwargs["c_stem_psi"].del();
  }

  char prev_aa = 'A';
  if(kwargs.contains("prev_aa")){
    prev_aa = extract<char>(kwargs["prev_aa"]);
    kwargs["prev_aa"].del();
  }

  char next_aa = 'A';
  if(kwargs.contains("next_aa")){
    next_aa = extract<char>(kwargs["next_aa"]);
    kwargs["next_aa"].del();
  }

  uint seed = 0;
  if(kwargs.contains("seed")){
    seed = extract<uint>(kwargs["seed"]);
    kwargs["seed"].del();
  }

  if(len(kwargs) > 0){
    std::stringstream ss;
    ss << "Invalid keywords observed when setting up PhiPsiSampler! ";
    ss << "Or did you pass the same keyword twice? ";
    ss << "Valid keywords are: n_stem_phi, c_stem_psi, prev_aa, next_aa and seed!";
    throw promod3::Error(ss.str());
  }

  extract<loop::TorsionSamplerPtr> get_sampler(args[1]);
  if (get_sampler.check()){
    return self.attr("__init__")(seq,get_sampler(),n_stem_phi,c_stem_psi,prev_aa,next_aa,seed);
  }
  else{
    boost::python::list samplers = extract<boost::python::list>(args[1]);
    loop::TorsionSamplerList v_samplers;
    core::ConvertListToVector(samplers, v_samplers);
    return self.attr("__init__")(seq, v_samplers, n_stem_phi, c_stem_psi,
                                 prev_aa, next_aa, seed);
  }
}

object SoftInitWrapper(tuple args, dict kwargs){

  object self = args[0];
  args = tuple(args.slice(1,_));

  if(len(args) != 3){
    std::stringstream ss;
    ss << "Need to define sequence, torsion_sampler and max_dev as non keyword arguments, ";
    ss << "all other arguments must be keyword arguments!";
    throw promod3::Error(ss.str());
  }

  String seq = extract<String>(args[0]);
  Real max_dev = extract<Real>(args[2]);

  Real n_stem_phi = -1.0472;
  if(kwargs.contains("n_stem_phi")){
    n_stem_phi = extract<Real>(kwargs["n_stem_phi"]);
    kwargs["n_stem_phi"].del();
  }

  Real c_stem_psi = -0.78540;
  if(kwargs.contains("c_stem_psi")){
    c_stem_psi = extract<Real>(kwargs["c_stem_psi"]);
    kwargs["c_stem_psi"].del();
  }

  char prev_aa = 'A';
  if(kwargs.contains("prev_aa")){
    prev_aa = extract<char>(kwargs["prev_aa"]);
    kwargs["prev_aa"].del();
  }

  char next_aa = 'A';
  if(kwargs.contains("next_aa")){
    next_aa = extract<char>(kwargs["next_aa"]);
    kwargs["next_aa"].del();
  }

  uint seed = 0;
  if(kwargs.contains("seed")){
    seed = extract<uint>(kwargs["seed"]);
    kwargs["seed"].del();
  }

  if(len(kwargs) > 0){
    std::stringstream ss;
    ss << "Invalid keywords observed when setting up SoftSampler! ";
    ss << "Or did you pass the same keyword twice? ";
    ss << "Valid keywords are: n_stem_phi, c_stem_psi, prev_aa, next_aa and seed!";
    throw promod3::Error(ss.str());
  }

  extract<loop::TorsionSamplerPtr> get_sampler(args[1]);
  if (get_sampler.check()){
    return self.attr("__init__")(seq,get_sampler(),max_dev,n_stem_phi,c_stem_psi,prev_aa,next_aa,seed);
  }
  else{
    boost::python::list samplers = extract<boost::python::list>(args[1]);
    loop::TorsionSamplerList v_samplers;
    core::ConvertListToVector(samplers, v_samplers);
    return self.attr("__init__")(seq, v_samplers, max_dev, n_stem_phi,
                                 c_stem_psi, prev_aa, next_aa, seed);
  }
}


object FragmentInitWrapper(tuple args, dict kwargs){

  object self = args[0];
  args = tuple(args.slice(1,_));

  if(len(args) != 2){
    std::stringstream ss;
    ss << "Need to define sequence and fragger_list as non keyword arguments, ";
    ss << "all other arguments must be keyword arguments!";
    throw promod3::Error(ss.str());
  }

  String seq = extract<String>(args[0]);

  loop::BackboneList init_bb_list(seq);
  if(kwargs.contains("init_bb_list")){
    init_bb_list = extract<loop::BackboneList>(kwargs["init_bb_list"]);
    kwargs["init_bb_list"].del();
  }

  uint sampling_start_index = 0;
  if(kwargs.contains("sampling_start_index")){
    sampling_start_index = extract<uint>(kwargs["sampling_start_index"]);
    kwargs["sampling_start_index"].del();
  }

  uint init_fragments = 3;
  if(kwargs.contains("init_fragments")){
    init_fragments = extract<uint>(kwargs["init_fragments"]);
    kwargs["init_fragments"].del();
  }

  uint seed = 0;
  if(kwargs.contains("seed")){
    seed = extract<uint>(kwargs["seed"]);
    kwargs["seed"].del();
  }

  if(len(kwargs) > 0){
    std::stringstream ss;
    ss << "Invalid keywords observed when setting up FragmentSampler! ";
    ss << "Or did you pass the same keyword twice? ";
    ss << "Valid keywords are: init_bb_list, sampling_start_index, ";
    ss << "init_fragments and seed!";
    throw promod3::Error(ss.str());
  }

  boost::python::list fraggers = extract<boost::python::list>(args[1]);
  loop::FraggerList v_fraggers;
  core::ConvertListToVector(fraggers, v_fraggers);
  return self.attr("__init__")(seq, v_fraggers, init_bb_list,
                               sampling_start_index, init_fragments, seed);
}


CCDCloserPtr CCDCloserWrapperSingle(const ost::mol::ResidueHandle& n_stem,
                                    const ost::mol::ResidueHandle& c_stem,
                                    const String& sequence,
                                    loop::TorsionSamplerPtr torsion_sampler,
                                    uint seed) {
  CCDCloserPtr p(new CCDCloser(n_stem,c_stem,sequence,torsion_sampler,seed));
  return p;
}

CCDCloserPtr CCDCloserWrapperList(const ost::mol::ResidueHandle& n_stem,
                                  const ost::mol::ResidueHandle& c_stem,
                                  const String& sequence,
                                  boost::python::list torsion_sampler,
                                  uint seed) {
  loop::TorsionSamplerList v_torsion_sampler;
  core::ConvertListToVector(torsion_sampler, v_torsion_sampler);
  CCDCloserPtr p(new CCDCloser(n_stem, c_stem, sequence, v_torsion_sampler,
                               seed));
  return p;
}

LinearScorerPtr LinearScorerInitWrapper(scoring::BackboneOverallScorerPtr scorer,
                                        scoring::BackboneScoreEnvPtr scorer_env,
                                        uint start_resnum, uint num_residues,
                                        uint chain_index,
                                        const boost::python::dict& weights) {
  std::map<String,Real> m_weights;
  core::ConvertDictToMap(weights, m_weights);
  LinearScorerPtr p(new LinearScorer(scorer, scorer_env, start_resnum, 
                                     num_residues, chain_index, m_weights));
  return p;
}

}

void export_monte_carlo() {

  //export coolers
  class_<MonteCarloCooler, boost::noncopyable>("MonteCarloCooler", no_init);

  class_<ExponentialCooler,bases<MonteCarloCooler> >("ExponentialCooler", init<int,Real,Real>())
    .def("GetTemperature", &ExponentialCooler::GetTemperature)
    .def("Reset", &ExponentialCooler::Reset)
  ;

  register_ptr_to_python<MonteCarloCoolerPtr>();
  register_ptr_to_python<ExponentialCoolerPtr>();


  //export scorers
  class_<MonteCarloScorer, boost::noncopyable>("MonteCarloScorer", no_init);

  class_<LinearScorer,bases<MonteCarloScorer> >("LinearScorer",no_init)
    .def("__init__", make_constructor(&LinearScorerInitWrapper))
    .def("GetScore", &LinearScorer::GetScore, (arg("positions")))
  ;

  register_ptr_to_python<MonteCarloScorerPtr>();
  register_ptr_to_python<LinearScorerPtr>();


  //export samplers
  class_<MonteCarloSampler, boost::noncopyable>("MonteCarloSampler", no_init);

  class_<PhiPsiSampler,bases<MonteCarloSampler> >("PhiPsiSampler",no_init)
    .def("__init__", raw_function(PhiPsiInitWrapper))
    .def(init<String,loop::TorsionSamplerPtr, Real, Real, char,char,uint>())
    .def(init<String,loop::TorsionSamplerList, Real, Real, char,char,uint>())
    .def("Initialize",&MonteCarloSampler::Initialize)
    .def("ProposeStep",&MonteCarloSampler::ProposeStep)
  ;

  class_<SoftSampler,bases<MonteCarloSampler> >("SoftSampler",no_init)
    .def("__init__", raw_function(SoftInitWrapper))
    .def(init<String,loop::TorsionSamplerPtr,Real,Real, Real, char,char,uint>())
    .def(init<String,loop::TorsionSamplerList,Real,Real, Real, char,char,uint>())
    .def("Initialize",&MonteCarloSampler::Initialize)
    .def("ProposeStep",&MonteCarloSampler::ProposeStep)
  ;

  class_<FragmentSampler,bases<MonteCarloSampler> >("FragmentSampler",no_init)
    .def("__init__", raw_function(FragmentInitWrapper))
    .def(init<const String&,const loop::FraggerList&,const loop::BackboneList&,uint,uint,uint>())
    .def("Initialize",&MonteCarloSampler::Initialize)
    .def("ProposeStep",&MonteCarloSampler::ProposeStep)
  ;

  register_ptr_to_python<PhiPsiSamplerPtr>();
  register_ptr_to_python<SoftSamplerPtr>();
  register_ptr_to_python<MonteCarloSamplerPtr>();
  register_ptr_to_python<FragmentSamplerPtr>();

  //export closer
  class_<MonteCarloCloser, boost::noncopyable>("MonteCarloCloser", no_init);

  class_<KICCloser,bases<MonteCarloCloser> >
    ("KICCloser", init<ost::mol::ResidueHandle&, ost::mol::ResidueHandle&, uint>())
    .def("Close",&KICCloser::Close,(arg("actual_positions"),arg("closed_positions")))
  ;

  class_<DirtyCCDCloser,bases<MonteCarloCloser> >
    ("DirtyCCDCloser", init<ost::mol::ResidueHandle&, ost::mol::ResidueHandle&>())
    .def("Close",&DirtyCCDCloser::Close,(arg("actual_positions"),arg("closed_positions")))
  ;

  class_<CCDCloser,bases<MonteCarloCloser> >("CCDCloser",no_init)
    .def("__init__",make_constructor(&CCDCloserWrapperSingle))
    .def("__init__",make_constructor(&CCDCloserWrapperList))
    .def("Close",&CCDCloser::Close,(arg("actual_positions"),arg("closed_positions")))
  ;

  class_<NTerminalCloser,bases<MonteCarloCloser> >("NTerminalCloser",init<const ost::mol::ResidueHandle&>())
    .def("Close",&NTerminalCloser::Close,(arg("actual_positions"),arg("closed_positions")))
  ;

  class_<CTerminalCloser,bases<MonteCarloCloser> >("CTerminalCloser",init<const ost::mol::ResidueHandle&>())
    .def("Close",&CTerminalCloser::Close,(arg("actual_positions"),arg("closed_positions")))
  ;

  class_<DeNovoCloser,bases<MonteCarloCloser> >("DeNovoCloser",init<>())
    .def("Close",&DeNovoCloser::Close,(arg("actual_positions"),arg("closed_positions")))
  ;

  register_ptr_to_python<KICCloserPtr>();
  register_ptr_to_python<DirtyCCDCloserPtr>();
  register_ptr_to_python<CCDCloserPtr>();
  register_ptr_to_python<NTerminalCloserPtr>();
  register_ptr_to_python<CTerminalCloserPtr>();
  register_ptr_to_python<MonteCarloCloserPtr>();
}
