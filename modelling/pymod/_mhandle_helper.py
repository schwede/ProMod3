# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class ModellingIssue:
    """

    :param text:         Sets :attr:`text`.
    :param severity:     Sets :attr:`severity`.
    :param residue_list: Sets :attr:`residue_list`.

    .. attribute:: text

      Description of issue.

      :type: :class:`str`

    .. attribute:: severity

      Severity of issue.

      :type: :class:`Severity`

    .. attribute:: residue_list

      List of residues affected by issue (or empty list if global issue).

      :type: :class:`list` of :class:`~ost.mol.ResidueHandle` /
             :class:`~ost.mol.ResidueView`
    """

    class Severity:
        """Enumerates severities."""
        #: Minor issues like remaining stereo-chemistry problems after MM.
        MINOR = 0
        #: Major issues like MM-failures, incomplete models and ring punches.
        MAJOR = 10

    def __init__(self, text, severity, residue_list=[]):
        self.text = text
        self.severity = severity
        self.residue_list = residue_list

    def is_major(self):
        """
        :return: True if this is a major issue.
        :rtype:  :class:`bool`
        """
        return self.severity == ModellingIssue.Severity.MAJOR

def AddModellingIssue(mhandle, text, severity, residue_list=[]):
    """Adds a new :class:`ModellingIssue` to
    :attr:`~ModellingHandle.modelling_issues` in *mhandle*.

    If *mhandle* doesn't contain the :attr:`~ModellingHandle.modelling_issues`
    attribute yet, it is added.

    :param mhandle:      Will have the issue added to.
    :type mhandle:       :class:`ModellingHandle`
    :param text:         Sets :attr:`ModellingIssue.text`.
    :param severity:     Sets :attr:`ModellingIssue.severity`.
    :param residue_list: Sets :attr:`ModellingIssue.residue_list`.
    """
    modelling_issue = ModellingIssue(text, severity, residue_list)
    if hasattr(mhandle, "modelling_issues"):
        mhandle.modelling_issues.append(modelling_issue)
    else:
        mhandle.modelling_issues = [modelling_issue]


def SetFraggerHandles(mhandle, fragger_handles):
    """Sets :attr:`~ModellingHandle.fragger_handles` in *mhandle* while
    ensuring consistency with the :attr:`ModellingHandle.seqres`.

    :param mhandle:     Will have the fragger handles attached afterwards
    :param fragger_handles: The fragger handles to attach

    :type mhandle:      :class:`ModellingHandle`
    :type fragger_handles:  :class:`list` of :class:`FraggerHandle`

    :raises: :class:`ValueError` when the given *fragger_handles* are not 
             consistent with seqres in *mhandle*
    """
    
    if len(mhandle.seqres) != len(fragger_handles):
        raise RuntimeError("Must have one FraggerHandle per chain!")
    for a,b in zip(mhandle.seqres, fragger_handles):
        if str(a) != str(b.sequence):
            raise RuntimeError("Sequence in FraggerHandle must match sequence "+
                               "in SEQRES!")
    mhandle.fragger_handles = fragger_handles

# these classes/methods will be exported into module
__all__ = ('ModellingIssue', 'AddModellingIssue', 'SetFraggerHandles')
