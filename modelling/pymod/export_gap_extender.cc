// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/modelling/gap_extender.hh>
#include <promod3/core/export_helper.hh>

using namespace ost;
using namespace boost::python;
using namespace promod3;
using namespace promod3::modelling;

namespace {

  GapExtenderPtr wrap_ExtenderInitOne(StructuralGap& gap, 
                                      const String& seqres)
  {
    GapExtenderPtr extender(new GapExtender(gap, seqres));
    return extender;
  }

  GapExtenderPtr wrap_ExtenderInitTwo(StructuralGap& gap,
                                      const ost::seq::SequenceHandle& seqres)
  {
    GapExtenderPtr extender(new GapExtender(gap, seqres));
    return extender;
  }
  
  ScoringGapExtenderPtr wrap_ScoringExtenderInitOneB(StructuralGap& gap,
                                                     Real extension_penalty,
                                                     list& penalties,
                                                     const String& seqres,
                                                     const int max_length=-2)
  {
    std::vector<Real> v_penalties;
    core::ConvertListToVector(penalties, v_penalties);
    ScoringGapExtenderPtr extender(
      new ScoringGapExtender(gap, extension_penalty, v_penalties,
                             seqres, max_length));
    return extender;
  }

  ScoringGapExtenderPtr wrap_ScoringExtenderInitOne(StructuralGap& gap,
                                                    Real extension_penalty,
                                                    list& penalties,
                                                    const String& seqres)
  {
    return wrap_ScoringExtenderInitOneB(gap, extension_penalty, penalties, seqres);
  }

  ScoringGapExtenderPtr wrap_ScoringExtenderInitTwoB(
                              StructuralGap& gap,
                              Real extension_penalty,
                              list& penalties,
                              const ost::seq::SequenceHandle& seqres,
                              const int max_length=-2)
  {
    std::vector<Real> v_penalties;
    core::ConvertListToVector(penalties, v_penalties);
    ScoringGapExtenderPtr extender(
      new ScoringGapExtender(gap, extension_penalty, v_penalties,
                             seqres, max_length));
    return extender;
  }

  ScoringGapExtenderPtr wrap_ScoringExtenderInitTwo(
                              StructuralGap& gap,
                              Real extension_penalty,
                              list& penalties,
                              const ost::seq::SequenceHandle& seqres)
  {
    return wrap_ScoringExtenderInitTwoB(gap, extension_penalty, penalties, seqres);
  }
}


void export_gap_extender()
{

  class_<GapExtender>("GapExtender", no_init)
    .def("__init__",make_constructor(wrap_ExtenderInitOne))
    .def("__init__",make_constructor(wrap_ExtenderInitTwo))
    .def("Extend", &GapExtender::Extend)
  ;
  register_ptr_to_python<GapExtenderPtr>();

  class_<FullGapExtender>("FullGapExtender", no_init)
    .def(init<StructuralGap&, const String&, const int>(
         (arg("gap"), arg("seqres"), arg("max_length")=-1)))
    .def(init<StructuralGap&, const ost::seq::SequenceHandle&, const int>(
         (arg("gap"), arg("seqres"), arg("max_length")=-1)))
    .def("Extend", &FullGapExtender::Extend)
  ;
  register_ptr_to_python<FullGapExtenderPtr>();
  
  class_<ScoringGapExtender>("ScoringGapExtender", no_init)
     .def("__init__",make_constructor(wrap_ScoringExtenderInitOne))
     .def("__init__",make_constructor(wrap_ScoringExtenderInitTwo))
     .def("__init__",make_constructor(wrap_ScoringExtenderInitOneB))
     .def("__init__",make_constructor(wrap_ScoringExtenderInitTwoB))
     .def("Extend", &ScoringGapExtender::Extend)
  ;
  register_ptr_to_python<ScoringGapExtenderPtr>();
}
