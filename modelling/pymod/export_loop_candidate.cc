// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <promod3/core/export_helper.hh>
#include <ost/export_helper/pair_to_tuple_conv.hh>
#include <promod3/modelling/loop_candidate.hh>

using namespace promod3;
using namespace boost::python;
using namespace promod3::modelling;

namespace {

loop::BackboneList& lc_getitem(LoopCandidatesPtr lcs, size_t i) {
  return (*lcs)[i];
}

boost::python::list ApplyCCDWithAllSamplers(LoopCandidatesPtr p,
                             const ost::mol::ResidueHandle& n_stem,
                             const ost::mol::ResidueHandle& c_stem,
                             boost::python::list l, 
                             int max_iterations, Real rmsd_cutoff, 
                             bool keep_non_converged, int random_seed) {
  // do work
  loop::TorsionSamplerList v;
  core::ConvertListToVector(l, v);
  std::vector<uint> indices;
  indices = p->ApplyCCD(n_stem, c_stem, v, max_iterations, rmsd_cutoff,
                        keep_non_converged, random_seed);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(indices, return_list);
  return return_list;
}

boost::python::list ApplyCCDWithSampler(LoopCandidatesPtr p,
                         const ost::mol::ResidueHandle& n_stem,
                         const ost::mol::ResidueHandle& c_stem,
                         loop::TorsionSamplerPtr torsion_sampler, 
                         int max_iterations, Real rmsd_cutoff, 
                         bool keep_non_converged, int random_seed) {
  // do work
  std::vector<uint> indices;
  indices = p->ApplyCCD(n_stem, c_stem, torsion_sampler, max_iterations,
                        rmsd_cutoff, keep_non_converged, random_seed);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(indices, return_list);
  return return_list;
}

boost::python::list ApplyCCDWithoutSampler(LoopCandidatesPtr p,
                            const ost::mol::ResidueHandle& n_stem,
                            const ost::mol::ResidueHandle& c_stem,
                            int max_iterations, 
                            Real rmsd_cutoff, bool keep_non_converged) {
  // do work
  std::vector<uint> indices;
  indices = p->ApplyCCD(n_stem, c_stem, max_iterations, rmsd_cutoff,
                        keep_non_converged);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(indices, return_list);
  return return_list;
}

boost::python::list WrapApplyKIC(LoopCandidatesPtr p,
                                 const ost::mol::ResidueHandle& n_stem,
                                 const ost::mol::ResidueHandle& c_stem,
                                 uint pivot_one, uint pivot_two,
                                 uint pivot_three) {
  // do work
  std::vector<uint> indices;
  indices = p->ApplyKIC(n_stem, c_stem, pivot_one, pivot_two, pivot_three);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(indices, return_list);
  return return_list;
}

LoopCandidatesPtr FillFromMC(const String& seq, 
                             uint num_loops, uint steps, 
                             MonteCarloSamplerPtr sampler,
                             MonteCarloCloserPtr closer,
                             MonteCarloScorerPtr scorer,
                             MonteCarloCoolerPtr cooler,
                             int random_seed) {
  return LoopCandidates::FillFromMonteCarloSampler(seq,num_loops,steps,
                                                   sampler,closer,scorer,cooler,
                                                   random_seed);
}

LoopCandidatesPtr FillFromMCInit(const loop::BackboneList& initial_bb,
                                 const String& seq, 
                                 uint num_loops, uint steps, 
                                 MonteCarloSamplerPtr sampler,
                                 MonteCarloCloserPtr closer,
                                 MonteCarloScorerPtr scorer,
                                 MonteCarloCoolerPtr cooler,
                                 int random_seed) {
  return LoopCandidates::FillFromMonteCarloSampler(initial_bb,seq, num_loops,
                                                   steps, sampler, closer,
                                                   scorer, cooler,
                                                   random_seed);
}

boost::python::list WrapGetClusters(LoopCandidatesPtr p, Real max_dist, 
                                    bool superposed_rmsd) {
  // get them
  std::vector< std::vector<uint> > clusters;
  p->GetClusters(max_dist, clusters, superposed_rmsd);
  // convert and return
  boost::python::list return_list;
  for (uint i = 0; i < clusters.size(); ++i) {
    boost::python::list lc_list;
    core::AppendVectorToList(clusters[i], lc_list);
    return_list.append(lc_list);
  }
  return return_list;
}

boost::python::list WrapGetClusteredCandidates(LoopCandidatesPtr p,
                                               Real max_dist,
                                               bool neglect_size_one,
                                               bool superposed_rmsd) {
  // get them
  LoopCandidatesList clustered_lc
   = p->GetClusteredCandidates(max_dist, neglect_size_one, superposed_rmsd);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(clustered_lc, return_list);
  return return_list;
}

void WrapCalculateBackboneScoresMH(LoopCandidatesPtr p,
                                   ScoreContainer& score_container,
                                   const ModellingHandle& mhandle,
                                   uint start_resnum, uint chain_idx) {
  p->CalculateBackboneScores(score_container, mhandle, start_resnum, chain_idx);
}
void WrapCalculateBackboneScoresMH_K(LoopCandidatesPtr p,
                                     ScoreContainer& score_container,
                                     const ModellingHandle& mhandle,
                                     const boost::python::list& keys,
                                     uint start_resnum, uint chain_idx) {
  std::vector<String> v_keys;
  core::ConvertListToVector(keys, v_keys);
  p->CalculateBackboneScores(score_container, mhandle, v_keys, start_resnum,
                             chain_idx);
}
void WrapCalculateBackboneScores(LoopCandidatesPtr p,
                                 ScoreContainer& score_container,
                                 scoring::BackboneOverallScorerPtr scorer,
                                 scoring::BackboneScoreEnvPtr scorer_env,
                                 uint start_resnum, uint chain_idx) {
  p->CalculateBackboneScores(score_container, scorer, scorer_env, start_resnum, 
                             chain_idx);
}
void WrapCalculateBackboneScoresK(LoopCandidatesPtr p,
                                  ScoreContainer& score_container,
                                  scoring::BackboneOverallScorerPtr scorer,
                                  scoring::BackboneScoreEnvPtr scorer_env,
                                  const boost::python::list& keys,
                                  uint start_resnum, uint chain_idx) {
  std::vector<String> v_keys;
  core::ConvertListToVector(keys, v_keys);
  p->CalculateBackboneScores(score_container, scorer, scorer_env, v_keys, 
                             start_resnum, chain_idx);
}
void WrapCalculateAllAtomScores(LoopCandidatesPtr p,
                                ScoreContainer& score_container,
                                const ModellingHandle& mhandle,
                                uint start_resnum, uint chain_idx) {
  p->CalculateAllAtomScores(score_container, mhandle, start_resnum, chain_idx);
}
void WrapCalculateAllAtomScoresK(LoopCandidatesPtr p,
                                 ScoreContainer& score_container,
                                 const ModellingHandle& mhandle,
                                 const boost::python::list& keys,
                                 uint start_resnum, uint chain_idx) {
  std::vector<String> v_keys;
  core::ConvertListToVector(keys, v_keys);
  p->CalculateAllAtomScores(score_container, mhandle, v_keys, start_resnum,
                            chain_idx);
}

boost::python::list WrapCalcSequenceProfScores(LoopCandidatesPtr p,
                               loop::StructureDBPtr structure_db,
                               const ost::seq::ProfileHandle& prof,
                               uint offset) {
  // get scores
  std::vector<Real> scores = p->CalculateSequenceProfileScores(structure_db,
                                                               prof, offset);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(scores, return_list);
  return return_list;
}
void WrapCalcSequenceProfScoresSC(LoopCandidatesPtr p,
                                  ScoreContainer& score_container,
                                  loop::StructureDBPtr structure_db,
                                  const ost::seq::ProfileHandle& prof,
                                  uint offset) {
  p->CalculateSequenceProfileScores(score_container, structure_db, prof,
                                    offset);
}

boost::python::list WrapCalcStructureProfScores(LoopCandidatesPtr p,
                               loop::StructureDBPtr structure_db,
                               const ost::seq::ProfileHandle& prof,
                               uint offset) {
  // get scores
  std::vector<Real> scores = p->CalculateStructureProfileScores(structure_db,
                                                                prof, offset);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(scores, return_list);
  return return_list;
}
void WrapCalcStructureProfScoresSC(LoopCandidatesPtr p,
                                   ScoreContainer& score_container,
                                   loop::StructureDBPtr structure_db,
                                   const ost::seq::ProfileHandle& prof,
                                   uint offset) {
  p->CalculateStructureProfileScores(score_container, structure_db, prof,
                                     offset);
}

boost::python::list WrapCalculateStemRMSDs(LoopCandidatesPtr p,
                               const ost::mol::ResidueHandle& n_stem,
                               const ost::mol::ResidueHandle& c_stem) {
  // get scores
  std::vector<Real> scores = p->CalculateStemRMSDs(n_stem, c_stem);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(scores, return_list);
  return return_list;
}
void WrapCalculateStemRMSDsSC(LoopCandidatesPtr p,
                              ScoreContainer& score_container,
                              const ost::mol::ResidueHandle& n_stem,
                              const ost::mol::ResidueHandle& c_stem) {
  p->CalculateStemRMSDs(score_container, n_stem, c_stem);
}

LoopCandidatesPtr WrapExtractList(LoopCandidatesPtr p,
                                  const boost::python::list& idx_list) {
  std::vector<uint> indices;
  core::ConvertListToVector(idx_list, indices);
  return p->Extract(indices);
}

} //ns


void export_loop_candidate() {

  class_<LoopCandidates, LoopCandidatesPtr>
    ("LoopCandidates", init<const String&>())
    .def("FillFromDatabase", &LoopCandidates::FillFromDatabase,
         (arg("n_stem"), arg("c_stem"), arg("seq"), arg("frag_db"),
          arg("structure_db"), arg("extended_search")=false))
    .staticmethod("FillFromDatabase")
    .def("FillFromMonteCarloSampler", &FillFromMCInit,
         (arg("initial_bb"), arg("seq"), arg("num_loops"), arg("steps"),
          arg("sampler"), arg("closer"), arg("scorer"), arg("cooler"),
          arg("random_seed")=0))
    .def("FillFromMonteCarloSampler", &FillFromMC,
         (arg("seq"), arg("num_loops"), arg("steps"), arg("sampler"),
          arg("closer"), arg("scorer"), arg("cooler"), arg("random_seed")=0))
    .staticmethod("FillFromMonteCarloSampler")
    .def("GetSequence",&LoopCandidates::GetSequence, 
         return_value_policy<copy_const_reference>())
    .def("ApplyCCD", &ApplyCCDWithSampler,
         (arg("n_stem"), arg("c_stem"), arg("torsion_sampler"),
          arg("max_iterations")=1000, arg("rmsd_cutoff")=0.1,
          arg("keep_non_converged")=false, arg("random_seed")=0))
    .def("ApplyCCD", &ApplyCCDWithAllSamplers,
         (arg("n_stem"), arg("c_stem"), arg("torsion_samplers"),
          arg("max_iterations")=1000, arg("rmsd_cutoff")=0.1,
          arg("keep_non_converged")=false, arg("random_seed")=0))
    .def("ApplyCCD", &ApplyCCDWithoutSampler,
         (arg("n_stem"), arg("c_stem"), arg("max_iterations")=1000,
          arg("rmsd_cutoff")=0.1, arg("keep_non_converged")=false))
    .def("ApplyKIC", &WrapApplyKIC,
         (arg("n_stem"), arg("c_stem"), arg("pivot_one"), arg("pivot_two"),
          arg("pivot_three")))
    .def("CalculateBackboneScores", &WrapCalculateBackboneScoresMH,
         (arg("score_container"), arg("mhandle"),
          arg("start_resnum"), arg("chain_idx")=0))
    .def("CalculateBackboneScores", &WrapCalculateBackboneScoresMH_K,
         (arg("score_container"), arg("mhandle"), arg("keys"),
          arg("start_resnum"), arg("chain_idx")=0))
    .def("CalculateBackboneScores", &WrapCalculateBackboneScores,
         (arg("score_container"), arg("scorer"), arg("scorer_env"),
          arg("start_resnum"), arg("chain_idx")=0))
    .def("CalculateBackboneScores", &WrapCalculateBackboneScoresK,
         (arg("score_container"), arg("scorer"), arg("scorer_env"), 
          arg("keys"), arg("start_resnum"), arg("chain_idx")=0))
    .def("CalculateAllAtomScores", &WrapCalculateAllAtomScores,
         (arg("score_container"), arg("mhandle"),
          arg("start_resnum"), arg("chain_idx")=0))
    .def("CalculateAllAtomScores", &WrapCalculateAllAtomScoresK,
         (arg("score_container"), arg("mhandle"), arg("keys"),
          arg("start_resnum"), arg("chain_idx")=0))
    .def("CalculateSequenceProfileScores", &WrapCalcSequenceProfScores,
         (arg("structure_db"), arg("prof"), arg("offset")=0))
    .def("CalculateSequenceProfileScores", &WrapCalcSequenceProfScoresSC,
         (arg("score_container"), arg("structure_db"), arg("prof"),
          arg("offset")=0))
    .def("CalculateStructureProfileScores", &WrapCalcStructureProfScores,
         (arg("structure_db"), arg("prof"), arg("offset")=0))
    .def("CalculateStructureProfileScores", &WrapCalcStructureProfScoresSC,
         (arg("score_container"), arg("structure_db"), arg("prof"),
          arg("offset")=0))
    .def("CalculateStemRMSDs", &WrapCalculateStemRMSDs,
         (arg("n_stem"), arg("c_stem")))
    .def("CalculateStemRMSDs", &WrapCalculateStemRMSDsSC,
         (arg("score_container"), arg("n_stem"), arg("c_stem")))
    .def("__iter__", iterator<LoopCandidates>())
    .def("__len__", &LoopCandidates::size)
    .def("__getitem__", lc_getitem, 
         return_value_policy<reference_existing_object>())
    .def("Add", &LoopCandidates::Add, (arg("bb_list")))
    .def("AddFragmentInfo", &LoopCandidates::AddFragmentInfo, (arg("fragment")))
    .def("Remove", &LoopCandidates::Remove, (arg("index")))
    .def("HasFragmentInfos", &LoopCandidates::HasFragmentInfos)
    .def("GetFragmentInfo", &LoopCandidates::GetFragmentInfo, (arg("index")),
         return_value_policy<copy_const_reference>())
    .def("Extract", &WrapExtractList, (arg("indices")))
    .def("GetClusters", &WrapGetClusters, (arg("max_dist"), 
                                           arg("superposed_rmsd")=false))
    .def("GetClusteredCandidates", &WrapGetClusteredCandidates,
         (arg("max_dist"), arg("neglect_size_one")=true, 
          arg("superposed_rmsd")=false))
    .def("GetLargestCluster", &LoopCandidates::GetLargestCluster, 
         (arg("max_dist"), arg("superposed_rmsd")=false))
  ;

}
