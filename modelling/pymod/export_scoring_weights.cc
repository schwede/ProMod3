// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/message.hh>
#include <promod3/core/export_helper.hh>

#include <promod3/modelling/scoring_weights.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;

// WRAPPERS
namespace {

boost::python::dict WrapGetWeights(bool with_db, bool with_aa,
                                   bool length_dependent, int loop_length) {
  boost::python::dict result;
  core::ConvertMapToDict(
    ScoringWeights::GetInstance().GetWeights(with_db, with_aa, 
                                             length_dependent, 
                                             loop_length), result);
  return result;
}
void WrapSetWeights(bool with_db, bool with_aa, const boost::python::dict& wd,
                    bool length_dependent, int loop_length) {
  std::map<String, Real> weights;
  core::ConvertDictToMap(wd, weights);
  ScoringWeights::GetInstance().SetWeights(with_db, with_aa, weights,
                                           length_dependent, loop_length);
}
boost::python::dict WrapGetBackboneWeights(bool with_db, bool with_aa,
                                           bool length_dependent,
                                           int loop_length) {
  boost::python::dict result;
  core::ConvertMapToDict(
          ScoringWeights::GetInstance().GetBackboneWeights(with_db, with_aa,
                                                           length_dependent, 
                                                           loop_length),
          result);
  return result;
}
boost::python::dict WrapGetAllAtomWeights(bool with_db, bool length_dependent,
                                          int loop_length) {
  boost::python::dict result;
  core::ConvertMapToDict(
          ScoringWeights::GetInstance().GetAllAtomWeights(with_db,
                                                          length_dependent,
                                                          loop_length), result);
  return result;
}

String WrapGetStemRMSDsKey() {
  return ScoringWeights::GetInstance().GetStemRMSDsKey();
}
void WrapSetStemRMSDsKey(const String& key) {
  return ScoringWeights::GetInstance().SetStemRMSDsKey(key);
}
String WrapGetSequenceProfileScoresKey() {
  return ScoringWeights::GetInstance().GetSequenceProfileScoresKey();
}
void WrapSetSequenceProfileScoresKey(const String& key) {
  return ScoringWeights::GetInstance().SetSequenceProfileScoresKey(key);
}
String WrapGetStructureProfileScoresKey() {
  return ScoringWeights::GetInstance().GetStructureProfileScoresKey();
}
void WrapSetStructureProfileScoresKey(const String& key) {
  return ScoringWeights::GetInstance().SetStructureProfileScoresKey(key);
}

boost::python::list WrapGetBackboneScoringKeys() {
  boost::python::list result;
  core::AppendVectorToList(
          ScoringWeights::GetInstance().GetBackboneScoringKeys(), result);
  return result; 
}
void WrapSetBackboneScoringKeys(const boost::python::list& key_list) {
  std::vector<String> keys;
  core::ConvertListToVector(key_list, keys);
  ScoringWeights::GetInstance().SetBackboneScoringKeys(keys);
}

boost::python::list WrapGetAllAtomScoringKeys() {
  boost::python::list result;
  core::AppendVectorToList(
          ScoringWeights::GetInstance().GetAllAtomScoringKeys(), result);
  return result; 
}
void WrapSetAllAtomScoringKeys(const boost::python::list& key_list) {
  std::vector<String> keys;
  core::ConvertListToVector(key_list, keys);
  ScoringWeights::GetInstance().SetAllAtomScoringKeys(keys);
}

} // anon wrapper ns

void export_scoring_weights() {

  class_<ScoringWeights>("ScoringWeights", no_init)
    .def("GetWeights", WrapGetWeights,
         (arg("with_db")=false, arg("with_aa")=false,
          arg("length_dependent")=false, arg("loop_length")=-1))
    .staticmethod("GetWeights")
    .def("SetWeights", WrapSetWeights,
         (arg("with_db"), arg("with_aa"), arg("weights"),
          arg("length_dependent")=false, arg("loop_length")=-1))
    .staticmethod("SetWeights")
    .def("GetBackboneWeights", WrapGetBackboneWeights,
         (arg("with_db")=false, arg("with_aa")=false,
          arg("length_dependent")=false, arg("loop_length")=-1))
    .staticmethod("GetBackboneWeights")
    .def("GetAllAtomWeights", WrapGetAllAtomWeights, 
         (arg("with_db")=false, arg("length_dependent")=false, 
          arg("loop_length")=-1))
    .staticmethod("GetAllAtomWeights")
    .def("GetStemRMSDsKey", WrapGetStemRMSDsKey)
    .staticmethod("GetStemRMSDsKey")
    .def("SetStemRMSDsKey", WrapSetStemRMSDsKey, (arg("key")))
    .staticmethod("SetStemRMSDsKey")
    .def("GetSequenceProfileScoresKey", WrapGetSequenceProfileScoresKey)
    .staticmethod("GetSequenceProfileScoresKey")
    .def("SetSequenceProfileScoresKey", WrapSetSequenceProfileScoresKey,
         (arg("key")))
    .staticmethod("SetSequenceProfileScoresKey")
    .def("GetStructureProfileScoresKey", WrapGetStructureProfileScoresKey)
    .staticmethod("GetStructureProfileScoresKey")
    .def("SetStructureProfileScoresKey", WrapSetStructureProfileScoresKey,
         (arg("key")))
    .staticmethod("SetStructureProfileScoresKey")
    .def("GetBackboneScoringKeys", WrapGetBackboneScoringKeys)
    .staticmethod("GetBackboneScoringKeys")
    .def("SetBackboneScoringKeys", WrapSetBackboneScoringKeys, (arg("keys")))
    .staticmethod("SetBackboneScoringKeys")
    .def("GetAllAtomScoringKeys", WrapGetAllAtomScoringKeys)
    .staticmethod("GetAllAtomScoringKeys")
    .def("SetAllAtomScoringKeys", WrapSetAllAtomScoringKeys, (arg("keys")))
    .staticmethod("SetAllAtomScoringKeys")
  ;

}
