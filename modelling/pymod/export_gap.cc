// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <promod3/modelling/gap.hh>

using namespace ost;
using namespace boost::python;
using namespace promod3::modelling;

void export_gap()
{
  class_<StructuralGap>("StructuralGap", init<mol::ResidueHandle, 
                                              mol::ResidueHandle, 
                                              String>((arg("before"), 
                                                       arg("after"), "seq")))
    .def(init<const StructuralGap&>())
    .def("GetChainIndex", &StructuralGap::GetChainIndex)
    .def("GetChainName", &StructuralGap::GetChainName)
    .def("GetChain",&StructuralGap::GetChain)
    .def("IsCTerminal", &StructuralGap::IsCTerminal)
    .def("IsNTerminal", &StructuralGap::IsNTerminal)
    .def("IsTerminal", &StructuralGap::IsTerminal)
    .def("ShiftCTerminal", &StructuralGap::ShiftCTerminal)
    .def("ExtendAtNTerm", &StructuralGap::ExtendAtNTerm)
    .def("ExtendAtCTerm", &StructuralGap::ExtendAtCTerm)
    .def("GetLength", &StructuralGap::GetLength)
    .def("Copy", &StructuralGap::Copy)
    .def("Transfer", &StructuralGap::Transfer)
    .add_property("length", &StructuralGap::GetLength)
    .def_readonly("seq", &StructuralGap::sequence)
    .def_readonly("before", &StructuralGap::before)
    .def_readonly("after", &StructuralGap::after)
    .add_property("full_seq", &StructuralGap::GetFullSeq)
    .def(self_ns::str(self))
  ;

  class_<StructuralGapList>("StructuralGapList", init<>())
    .def(vector_indexing_suite<StructuralGapList>())
  ;
}
