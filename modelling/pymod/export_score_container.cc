// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/core/message.hh>
#include <promod3/core/export_helper.hh>

#include <promod3/modelling/score_container.hh>

using namespace promod3;
using namespace promod3::modelling;
using namespace boost::python;

// WRAPPERS
namespace {

boost::python::list WrapGet(const ScoreContainer& sc, const String& key) {
  boost::python::list result;
  core::AppendVectorToList(sc.Get(key), result);
  return result; 
}
void WrapSet(ScoreContainer& sc, const String& key,
             const boost::python::list& scores) {
  ScoreContainer::ScoreVector v;
  core::ConvertListToVector(scores, v);
  sc.Set(key, v);
}

boost::python::list WrapLinearCombine(const ScoreContainer& sc,
                                      const boost::python::dict& weights) {
  // get scores
  std::map<String, Real> w;
  core::ConvertDictToMap(weights, w);
  ScoreContainer::ScoreVector scores = sc.LinearCombine(w);
  // convert and return
  boost::python::list return_list;
  core::AppendVectorToList(scores, return_list);
  return return_list;
}

ScoreContainerPtr WrapExtract(const ScoreContainer& sc,
                              const boost::python::list indices) {
  std::vector<uint> v;
  core::ConvertListToVector(indices, v);
  return sc.Extract(v);
}

} // anon wrapper ns

void export_score_container() {

  class_<ScoreContainer, ScoreContainerPtr>("ScoreContainer", init<>())
    .def("IsEmpty", &ScoreContainer::IsEmpty)
    .def("Contains", &ScoreContainer::Contains, (arg("key")))
    .def("Get", WrapGet, (arg("key")))
    .def("Set", WrapSet, (arg("key"), arg("scores")))
    .def("GetNumCandidates", &ScoreContainer::GetNumCandidates)
    .def("LinearCombine", WrapLinearCombine, (arg("linear_weights")))
    .def("Copy", &ScoreContainer::Copy)
    .def("Extract", WrapExtract, (arg("indices")))
    .def("Extend", &ScoreContainer::Extend, (arg("other")))
  ;

}
