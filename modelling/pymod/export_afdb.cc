// Copyright (c) 2013-2023, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <promod3/modelling/afdb.hh>

using namespace ost;
using namespace boost::python;
using namespace promod3::modelling;

namespace {
  void WrapSeqToPentamerIndices(const String& sequence, bool unique,
                                boost::python::list& l) {
    std::vector<int> indices;
    SeqToPentamerIndices(sequence, unique, indices);
    for(size_t i = 0; i < indices.size(); ++i) {
      l.append(indices[i]);
    }
  }
}

void export_afdb()
{
  def("SeqToPentamerIndices", &WrapSeqToPentamerIndices, (arg("sequence"),
                                                          arg("unique"),
                                                          arg("result_list")));
  def("CreateAFDBIdx", &CreateAFDBIdx, (arg("uniprot_ac"), arg("fragment"),
                                        arg("version")));

  def("CreatePentaMatch", &CreatePentaMatch, (arg("seq_list"),
                                              arg("db_path"),
                                              arg("entries_from_seqnames")));
}
