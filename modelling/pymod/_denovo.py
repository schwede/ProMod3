# Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
#                          Biozentrum - University of Basel
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from promod3 import scoring, loop
from ._fragger_handle import FraggerHandle
from ._modelling import *

def GenerateDeNovoTrajectories(sequence, 
                               num_trajectories = 200,
                               avg_sampling_per_position = 600,
                               profile = None,
                               psipred_prediction = None,
                               fragment_handler = None,
                               scorer = None,
                               scorer_env = None,
                               scoring_weights = None):
    '''Example de novo modelling pipeline based on Fragment sampling and
    backbone scoring. Take this as a starting point for more advanced
    de novo procedures.

    :param sequence:    The sequence you want to sample
    :type sequence:     :class:`str`

    :param num_trajectories:  The number of sampling trajectories you
                              want to generate 
    :type num_trajectories:   :class:`int`

    
    :param avg_sampling_per_position: Number of Monte Carlo sampling steps
                                      the total number is: 
                                      len(**sequence**) * **avg_sampling_per_position** 

    :param profile:     The sequence profile for **sequence**. This increases the 
                        fragment search performance.
    :type profile:      :class:`ost.seq.ProfileHandle`

    :param psipred_prediction: The psipred prediction for **sequence**. This
                               increases the fragment search performance
    :type psipred_prediction:  :class:`promod3.loop.PsipredPrediction`

    :param fragment_handler: You can provide already initialized fragments.
                             If you pass this parameter, **profile** and
                             **psipred_prediction** get neglected and do
                             not influence the fragment search, the
                             ones you initialized **fragment_handler** with
                             get used instead.
    :type fragment_handler:  :class:`promod3.modelling.FraggerHandle`

    :param scorer:      Scorer doing the backbone scoring. If not provided, a 
                        default one gets loaded with default objects with
                        following keys: clash, reduced, cb_packing, hbond, cbeta, 
                        torsion and pairwise 
    :type scorer:       :class:`promod3.scoring.BackboneOverallScorer`

    :param scorer_env:  The scoring env that relates to **scorer**
                        This environment will be changed! 
    :type scorer_env:   :class:`promod3.scoring.BackboneScoreEnv`

    :param scoring_weights: Linear weights for different scores. If not provided,
                            the output of ScoringWeights.GetWeights() is used.
                            Please note, that the weights must be consistent
                            with the keys of the scores in **scorer**
    :type scoring_weights:  :class:`dict`

    :returns:           A :class:`promod3.loop.LoopCandidates` object containing
                        **num_trajectories** elements for further processing
    '''

    if len(sequence) < 9:
        raise RuntimeError("Seq too short for Denovo sampling (min length 9)")

    # check whether profile / psipred_prediction are consistent if present
    if profile != None:
      if profile.sequence != sequence:
          err = "Sequence of profile must match input sequence!"
          raise RuntimeError(err)

    if psipred_prediction != None:
        if len(sequence) != len(psipred_prediction):
            err = "psipred_prediction must be consistent with the input sequence!"
            raise RuntimeError(err)

    if scorer != None and scorer_env == None:
      raise RuntimeError("Must provide an environment when you provide a scorer!")

    if fragment_handler == None:
        # we first have to build our own handler
        fragment_handler = FraggerHandle(sequence, 
                                         profile = profile, 
                                         psipred_pred = psipred_prediction)
    fragger_list = fragment_handler.GetList()

    if scorer == None:
        # Lets setup a scorer with empty environment
        scorer = scoring.BackboneOverallScorer()
        scorer_env = scoring.BackboneScoreEnv(sequence)
        scorer["clash"] = scoring.ClashScorer()
        scorer["reduced"] = scoring.LoadReducedScorer()
        scorer["cb_packing"] = scoring.LoadCBPackingScorer()
        scorer["hbond"] = scoring.LoadHBondScorer()
        scorer["cbeta"] = scoring.LoadCBetaScorer()
        scorer["torsion"] = scoring.LoadTorsionScorer()
        scorer["pairwise"] = scoring.PairwiseScorer()
        scorer.AttachEnvironment(scorer_env)

    if scoring_weights == None:
        scoring_weights = ScoringWeights.GetWeights()

    # the number of 109 is roughly the number of times we have to apply
    # a factor of 0.9 to 100 until it reaches a value of 0.001
    start_temperature = 100
    cooling_factor = 0.9
    sampling_steps = avg_sampling_per_position * len(fragger_list)
    cooling_interval = sampling_steps / 109

    mc_sampler = FragmentSampler(sequence, fragger_list, 
                                 init_fragments = 5)
    mc_closer = DeNovoCloser()
    mc_scorer = LinearScorer(scorer, scorer_env, 1, len(sequence), 0, 
                             scoring_weights)
    mc_cooler = ExponentialCooler(start_temperature,
                                  cooling_interval,
                                  cooling_factor)

    candidates = LoopCandidates.FillFromMonteCarloSampler(sequence,
                                                          num_trajectories,
                                                          sampling_steps,
                                                          mc_sampler,
                                                          mc_closer,
                                                          mc_scorer,
                                                          mc_cooler)  
    return candidates
