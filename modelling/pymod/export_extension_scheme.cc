// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <promod3/modelling/extension_scheme.hh>

using namespace boost::python;
using namespace promod3::modelling;

namespace{
  boost::python::tuple WrapShiftExtend(ShiftExtension& ext){
    std::pair<int,int> e = ext.Extend();
    boost::python::tuple tuple_e = boost::python::make_tuple(e.first,
                                                             e.second);
    return tuple_e;
  }
}


void export_extension_scheme()
{
  class_<ShiftExtension>("ShiftExtension", init<int,int>())
    .def("Extend", &WrapShiftExtend)
  ;
}
