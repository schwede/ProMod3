// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/monte_carlo.hh>

namespace promod3 { namespace modelling {

void SampleMonteCarlo(MonteCarloSamplerPtr sampler, MonteCarloCloserPtr closer,
                      MonteCarloScorerPtr scorer, MonteCarloCoolerPtr cooler,
                      uint steps, loop::BackboneList& positions,
                      bool initialize, uint seed,
                      bool lowest_energy_conformation) {

  loop::BackboneList proposed_positions;

  boost::mt19937 rgen(seed);
  boost::uniform_01<boost::mt19937> zeroone(rgen);

  Real score, new_score, temperature;

  if (initialize) {
    //try to initialize 
    bool initialized = false;
    for (int i = 0; i < 100; ++i) {
      sampler->Initialize(positions);
      if (closer->Close(positions, positions)) {
        initialized = true;
        break;
      }
    }
    if (!initialized) {
      throw promod3::Error("Failed to initialize monte carlo sampling protocol!");
    }
  }

  score = scorer->GetScore(positions);
  bool closed = false;

  Real min_score = score;
  loop::BackboneList min_score_bb_list = positions;

  for (uint i = 0; i < steps; ++i) {
    //try several proposals!
    closed = false;
    for (int j = 0; j < 3; ++j) {
      sampler->ProposeStep(positions, proposed_positions);
      if (closer->Close(proposed_positions, proposed_positions)) {
        closed = true;
        break;
      }
    }
    if (!closed) continue;
    temperature = cooler->GetTemperature();
    new_score = scorer->GetScore(proposed_positions);
    if (std::exp(-(new_score-score)/temperature) > zeroone()) {
      positions.swap(proposed_positions);
      score = new_score;
      if (lowest_energy_conformation && score < min_score) {
        min_score = score;
        min_score_bb_list = positions;
      }
    }
  }

  // update return value if needed
  if (lowest_energy_conformation) positions = min_score_bb_list;
}

}}//ns

