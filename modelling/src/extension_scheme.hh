// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_EXTENSION_SCHEME_HH
#define PROMOD_MODELLING_EXTENSION_SCHEME_HH

#include <promod3/core/message.hh>

namespace promod3{ namespace modelling{

class ExtensionScheme{
public:
  virtual std::pair<int,int> Extend() = 0;
};


class ShiftExtension : public ExtensionScheme{

public:

  ShiftExtension(int start_n, int start_c): start_n_(start_n),
                                            start_c_(start_c),
                                            current_n_(start_n),
                                            current_c_(start_c),
                                            current_extension_(0) { 
    if(start_n > start_c){
      throw promod3::Error("Start n_num must be smaller or equal start c_num!");
    }

  }

  virtual std::pair<int,int> Extend();

private:

  int start_n_;
  int start_c_;
  int current_n_;
  int current_c_;
  int current_extension_;
};

}} // ns

#endif
