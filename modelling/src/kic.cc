// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/kic.hh>
#include <promod3/core/eigen_types.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

namespace {

struct KICParameters {
  Real dist[3];
  Real theta[3];
  Real delta[3];
  Real xi[3];
  Real eta[3];
  Real alpha[3];
  uint p1;
  uint p2;
  uint p3;
  geom::Mat3 F3_base;
  geom::Mat4 n_stem_transform;
  geom::Mat4 c_stem_transform;
  geom::Mat4 F1_initial_transform;
  geom::Mat4 F2_initial_transform;
};

void BuildFragmentBase(const geom::Vec3& pos1, const geom::Vec3& pos2, const geom::Vec3& pos3, geom::Mat3& base){

  geom::Vec3 a,b,c;
  a = geom::Normalize(pos2-pos1);
  c = geom::Normalize(geom::Cross(a,pos3-pos2));
  b = geom::Cross(c,a); 
  
  for(uint i = 0; i < 3; ++i){
    base(0,i) = a[i];
    base(1,i) = b[i];
    base(2,i) = c[i];
  }
} 

bool FillKICParameters(const promod3::loop::BackboneList& bb_list,
                       const ost::mol::ResidueHandle& n_stem,
                       const ost::mol::ResidueHandle& c_stem,
                       uint pivot_one, uint pivot_two, uint pivot_three,
                       KICParameters& parameters) {

  parameters.n_stem_transform = bb_list.GetTransform(0, n_stem);
  parameters.c_stem_transform = bb_list.GetTransform(bb_list.size()-1, c_stem);

  geom::Vec3List F1, F2, F3;

  F1.push_back(bb_list.GetCA(pivot_one));
  F1.push_back(bb_list.GetC(pivot_one));
  F1.push_back(bb_list.GetN(pivot_two));
  F1.push_back(bb_list.GetCA(pivot_two));

  F2.push_back(bb_list.GetCA(pivot_two));
  F2.push_back(bb_list.GetC(pivot_two));
  F2.push_back(bb_list.GetN(pivot_three));
  F2.push_back(bb_list.GetCA(pivot_three));

  F3.push_back(geom::Vec3(parameters.c_stem_transform * bb_list.GetCA(pivot_three)));
  F3.push_back(geom::Vec3(parameters.c_stem_transform * bb_list.GetC(pivot_three)));
  F3.push_back(geom::Vec3(parameters.n_stem_transform * bb_list.GetN(pivot_one)));
  F3.push_back(geom::Vec3(parameters.n_stem_transform * bb_list.GetCA(pivot_one)));

  parameters.dist[0] = geom::Distance(F1[0],F1[3]);
  parameters.dist[1] = geom::Distance(F2[0],F2[3]);
  parameters.dist[2] = geom::Distance(F3[0],F3[3]);

  //check, whether we have a valid triangle
  if(parameters.dist[0] + parameters.dist[1] <= parameters.dist[2] ||
     parameters.dist[1] + parameters.dist[2] <= parameters.dist[0] ||
     parameters.dist[2] + parameters.dist[0] <= parameters.dist[1]) return false;

  //theta is not directly associated to single fragments, but rather to angles between fragments...
  parameters.theta[0] = geom::Angle(bb_list.GetN(pivot_one)-bb_list.GetCA(pivot_one), 
                                    bb_list.GetC(pivot_one)-bb_list.GetCA(pivot_one));
  parameters.theta[1] = geom::Angle(bb_list.GetN(pivot_two)-bb_list.GetCA(pivot_two), 
                                    bb_list.GetC(pivot_two)-bb_list.GetCA(pivot_two));
  parameters.theta[2] = geom::Angle(bb_list.GetN(pivot_three)-bb_list.GetCA(pivot_three), 
                                    bb_list.GetC(pivot_three)-bb_list.GetCA(pivot_three));

  parameters.delta[0] = geom::DihedralAngle(F1[1],F1[0],F1[3],F1[2]);
  parameters.delta[1] = geom::DihedralAngle(F2[1],F2[0],F2[3],F2[2]);
  parameters.delta[2] = geom::DihedralAngle(F3[1],F3[0],F3[3],F3[2]);

  parameters.eta[0] = geom::Angle(F1[1]-F1[0],F1[3]-F1[0]);
  parameters.eta[1] = geom::Angle(F2[1]-F2[0],F2[3]-F2[0]);
  parameters.eta[2] = geom::Angle(F3[1]-F3[0],F3[3]-F3[0]);

  parameters.xi[0] = geom::Angle(F1[0]-F1[3], F1[2]-F1[3]);
  parameters.xi[1] = geom::Angle(F2[0]-F2[3], F2[2]-F2[3]);
  parameters.xi[2] = geom::Angle(F3[0]-F3[3], F3[2]-F3[3]);

  Real d11 = parameters.dist[0] * parameters.dist[0];
  Real d22 = parameters.dist[1] * parameters.dist[1];
  Real d33 = parameters.dist[2] * parameters.dist[2];
  parameters.alpha[0] = std::acos((d22-d33-d11)/(2*parameters.dist[0]*parameters.dist[2]));
  parameters.alpha[1] = std::acos((d33-d11-d22)/(2*parameters.dist[0]*parameters.dist[1]));
  parameters.alpha[2] = std::acos((d11-d22-d33)/(2*parameters.dist[1]*parameters.dist[2]));

  parameters.p1 = pivot_one;
  parameters.p2 = pivot_two;
  parameters.p3 = pivot_three;

  geom::Mat3 F1_base;
  geom::Mat3 F2_base;

  BuildFragmentBase(F1[0], F1[3], F1[1], F1_base);
  BuildFragmentBase(F2[0], F2[3], F2[1], F2_base);
  BuildFragmentBase(F3[0], F3[3], F3[1], parameters.F3_base);

  //translation of first pos to origin
  geom::Mat4 f1_A; 
  f1_A.PasteTranslation(-bb_list.GetCA(pivot_one));
  //application of base
  geom::Mat4 f1_B;
  f1_B.PasteRotation(F1_base);
  parameters.F1_initial_transform = f1_B * f1_A;

  //translation of first pos to origin
  geom::Mat4 f2_A;
  f2_A.PasteTranslation(-bb_list.GetCA(pivot_two));
  //application of base
  geom::Mat4 f2_B;
  f2_B.PasteRotation(F2_base);
  parameters.F2_initial_transform = f2_B * f2_A;

  return true;
}

void FillDixonMatrices(KICParameters& parameters, promod3::core::EMat8& R0, 
                       promod3::core::EMat8& R1, promod3::core::EMat8& R2){
  Real p[3][3][3];
  Real c_theta[3];   //c:cosine, s:sine, naming more or less as in the Coutsias paper
  Real s_delta[3];
  Real c_delta[3];
  Real s_xi[3];
  Real c_xi[3];
  Real s_eta[3];
  Real s_alpha_min_eta[3];
  Real s_alpha_plus_eta[3];
  Real c_alpha_min_eta[3];
  Real c_alpha_plus_eta[3];

  int i_min_one;
  for(int i = 0; i < 3; ++i){
    i_min_one = (i+2)%3; //circular...
    c_theta[i] = std::cos(parameters.theta[i]);
    s_delta[i] = std::sin(parameters.delta[i_min_one]);
    c_delta[i] = std::cos(parameters.delta[i_min_one]);
    s_xi[i] = std::sin(parameters.xi[i_min_one]);
    c_xi[i] = std::cos(parameters.xi[i_min_one]);
    s_eta[i] = std::sin(parameters.eta[i]);
    s_alpha_min_eta[i] = std::sin(parameters.alpha[i]-parameters.eta[i]);
    s_alpha_plus_eta[i] = std::sin(parameters.alpha[i]+parameters.eta[i]);
    c_alpha_min_eta[i] = std::cos(parameters.alpha[i]-parameters.eta[i]);
    c_alpha_plus_eta[i] = std::cos(parameters.alpha[i]+parameters.eta[i]);
  }

  //certain products, that occur more than once and thus can be precalculated
  Real s_delta_s_xi[3];
  Real c_delta_s_xi[3];
  Real c_xi_c_alpha_min_eta[3];
  Real c_xi_c_alpha_plus_eta[3];
  
  for(int i = 0; i < 3; ++i){
    s_delta_s_xi[i] = s_delta[i] * s_xi[i];
    c_delta_s_xi[i] = c_delta[i] * s_xi[i];
    c_xi_c_alpha_min_eta[i] = c_xi[i] * c_alpha_min_eta[i];
    c_xi_c_alpha_plus_eta[i] = c_xi[i] * c_alpha_plus_eta[i];
  }

  for(int i = 0; i < 3; ++i){
    p[0][0][i] = -c_theta[i]-c_xi_c_alpha_plus_eta[i]+c_delta_s_xi[i]*s_alpha_plus_eta[i];
    p[0][1][i] = 2*s_delta_s_xi[i]*s_eta[i];
    p[0][2][i] = -c_theta[i]-c_xi_c_alpha_min_eta[i]+c_delta_s_xi[i]*s_alpha_min_eta[i];

    p[1][0][i] = -2*s_delta_s_xi[i]*s_alpha_plus_eta[i];
    p[1][1][i] = 4*c_delta_s_xi[i]*s_eta[i];
    p[1][2][i] = -2*s_delta_s_xi[i]*s_alpha_min_eta[i];

    p[2][0][i] = -c_theta[i]-c_xi_c_alpha_plus_eta[i]-c_delta_s_xi[i]*s_alpha_plus_eta[i];
    p[2][1][i] = -2*s_delta_s_xi[i]*s_eta[i];
    p[2][2][i] = -c_theta[i]-c_xi_c_alpha_min_eta[i]-c_delta_s_xi[i]*s_alpha_min_eta[i];
  }

  Real A[3][3];
  Real B[3][3];
  Real C[3][3];
  Real D[3][3];

  for(int i = 0; i < 3; ++i){
    for(int j = 0; j < 3; ++j){
      A[i][j] = p[i][1][1]*p[0][j][2] - p[i][0][1]*p[1][j][2];
      B[i][j] = p[i][2][1]*p[0][j][2] - p[i][0][1]*p[2][j][2];
      C[i][j] = p[i][2][1]*p[1][j][2] - p[i][1][1]*p[2][j][2];
      D[i][j] = p[j][i][0];
    }
  }

  promod3::core::EMat8* RArray[3] = {&R0, &R1, &R2};
  for(int i = 0; i < 3; ++i){
    promod3::core::EMat8& Ri = *RArray[i];

    Ri(0,1) = A[0][i]; Ri(0,2) = A[1][i]; Ri(0,3) = A[2][i];
    Ri(0,5) = B[0][i]; Ri(0,6) = B[1][i]; Ri(0,7) = B[2][i];

    Ri(1,0) = A[0][i]; Ri(1,1) = A[1][i]; Ri(1,2) = A[2][i];
    Ri(1,4) = B[0][i]; Ri(1,5) = B[1][i]; Ri(1,6) = B[2][i];

    Ri(2,1) = B[0][i]; Ri(2,2) = B[1][i]; Ri(2,3) = B[2][i];
    Ri(2,5) = C[0][i]; Ri(2,6) = C[1][i]; Ri(2,7) = C[2][i];

    Ri(3,0) = B[0][i]; Ri(3,1) = B[1][i]; Ri(3,2) = B[2][i];
    Ri(3,4) = C[0][i]; Ri(3,5) = C[1][i]; Ri(3,6) = C[2][i];

    Ri(4,5) = D[0][i]; Ri(4,6) = D[1][i]; Ri(4,7) = D[2][i];

    Ri(5,4) = D[0][i]; Ri(5,5) = D[1][i]; Ri(5,6) = D[2][i];

    Ri(6,1) = D[0][i]; Ri(6,2) = D[1][i]; Ri(6,3) = D[2][i];

    Ri(7,0) = D[0][i]; Ri(7,1) = D[1][i]; Ri(7,2) = D[2][i];
  }
}

void ResolveEigenProblem(promod3::core::EMat8& R0, promod3::core::EMat8& R1, 
                         promod3::core::EMat8& R2, 
                         std::vector<std::vector<Real> >& taus){
  promod3::core::EMat16 E0 = promod3::core::EMat16::Zero();
  promod3::core::EMat16 E1 = promod3::core::EMat16::Zero();

  E0.topRightCorner(8,8).setIdentity();  
  E0.bottomLeftCorner(8,8) = -R0;
  E0.bottomRightCorner(8,8) = -R1;

  E1.topLeftCorner(8,8).setIdentity();
  E1.bottomRightCorner(8,8) = R2;

  typedef Eigen::GeneralizedEigenSolver<promod3::core::EMat16> EigenSolver;
  EigenSolver solver(E0, E1, true);
  const EigenSolver::ComplexVectorType& alpha = solver.alphas();
  const EigenSolver::VectorType& beta = solver.betas();
  const EigenSolver::EigenvectorsType& eigenvectors = solver.eigenvectors();

  for(uint i = 0; i < 16; ++i){
    if(std::abs(alpha[i].imag()) > 0) continue; //imaginary component
    std::vector<Real> solution;
    if(eigenvectors(0,i).real() > 1e-6){
      //note, that we have to normalize, since the length of an 
      //eigenvector can vary...
      solution.push_back(2*std::atan(eigenvectors(1,i).real()/
                                     eigenvectors(0,i).real()));
      solution.push_back(2*std::atan(eigenvectors(4,i).real()/
                                     eigenvectors(0,i).real()));
    }else{
      solution.push_back(2*std::atan(eigenvectors(3,i).real()/
                                     eigenvectors(2,i).real()));
      solution.push_back(2*std::atan(eigenvectors(6,i).real()/
                                     eigenvectors(2,i).real()));
    }
    solution.push_back(2*std::atan(alpha[i].real()/beta(i)));
    taus.push_back(solution);
  }
}

void ApplySolution(const KICParameters& parameters, Real tau1, Real tau2,
                   Real tau3, promod3::loop::BackboneList& bb_list) {
  Real sin_tau1 = std::sin(tau1);
  Real cos_tau1 = std::cos(tau1);
  Real sin_tau2 = std::sin(tau2);
  Real cos_tau2 = std::cos(tau2);
  Real sin_tau3 = std::sin(tau3);
  Real cos_tau3 = std::cos(tau3);
  Real sin_alpha1 = std::sin(parameters.alpha[0]);
  Real cos_alpha1 = std::cos(parameters.alpha[0]);
  Real sin_alpha3 = std::sin(parameters.alpha[2]);
  Real cos_alpha3 = std::cos(parameters.alpha[2]);

  geom::Mat3 transposed_base_3 = geom::Transpose(parameters.F3_base);

  // do lot of transformations to finaly place atoms from F1 and F2 onto their
  // correct positions...
  // note, that the first two transformations (A and B) have already been combined 
  // in FillKICParameters

  // do transformations matrices for F1
  // rotate around x
  geom::Mat4 f1_C;
  f1_C(1,1) = cos_tau1;
  f1_C(2,2) = cos_tau1;
  f1_C(1,2) = -sin_tau1;
  f1_C(2,1) = sin_tau1;

  // rotate around z
  geom::Mat4 f1_D;
  f1_D(0,0) = cos_alpha1;
  f1_D(0,1) = -sin_alpha1;
  f1_D(1,0) = sin_alpha1;
  f1_D(1,1) = cos_alpha1; 

  // magic_translation
  geom::Mat4 f1_E;
  f1_E(0,3) = parameters.dist[2];

  // again rotation around x
  geom::Mat4 f1_F;
  f1_F(1,1) = cos_tau3;
  f1_F(2,2) = cos_tau3;
  f1_F(1,2) = sin_tau3;
  f1_F(2,1) = -sin_tau3;

  // apply transposed F3 base
  geom::Mat4 f1_G;
  f1_G.PasteRotation(transposed_base_3);

  // final translation to correct positions
  geom::Mat4 f1_H;
  f1_H.PasteTranslation(geom::Vec3(parameters.c_stem_transform * bb_list.GetCA(parameters.p3)));

  // do transformation matrices for F2
  // rotate around x
  geom::Mat4 f2_C;
  f2_C(1,1) = cos_tau2;
  f2_C(2,2) = cos_tau2;
  f2_C(1,2) = -sin_tau2;
  f2_C(2,1) = sin_tau2;

  // rotate around z
  geom::Mat4 f2_D;
  f2_D(0,0) = cos_alpha3;
  f2_D(0,1) = sin_alpha3;
  f2_D(1,0) = -sin_alpha3;
  f2_D(1,1) = cos_alpha3; 

  // we need one of the transformed position for a translation...
  geom::Mat4 current_f2_transform = f2_D * f2_C * parameters.F2_initial_transform;
  geom::Vec3 translation_vec = geom::Vec3(current_f2_transform * bb_list.GetCA(parameters.p3));
  geom::Mat4 f2_E;
  f2_E(0,3) = -translation_vec[0];
  f2_E(1,3) = -translation_vec[1]; 

  // the next two matrices are equivalent to f1...

  // the following transformations are the same for both...
  geom::Mat4 pre_transform = f1_H * f1_G * f1_F; 

  geom::Mat4 f1_transform = pre_transform * f1_E * f1_D * f1_C * parameters.F1_initial_transform;
  geom::Mat4 f2_transform = pre_transform * f2_E * current_f2_transform;

  // transform pivots
  geom::Vec3 n_pos, ca_pos, cb_pos, c_pos, o_pos;
  // p1
  n_pos  = geom::Vec3(parameters.n_stem_transform * bb_list.GetN(parameters.p1));
  ca_pos = geom::Vec3(parameters.n_stem_transform * bb_list.GetCA(parameters.p1));
  c_pos  = geom::Vec3(f1_transform * bb_list.GetC(parameters.p1));
  o_pos  = geom::Vec3(f1_transform * bb_list.GetO(parameters.p1));
  promod3::core::ConstructCBetaPos(n_pos, ca_pos, c_pos, cb_pos);
  bb_list.SetN(parameters.p1, n_pos);
  bb_list.SetCA(parameters.p1, ca_pos);
  bb_list.SetC(parameters.p1, c_pos);
  bb_list.SetO(parameters.p1, o_pos);
  bb_list.SetCB(parameters.p1, cb_pos);
  // p2
  n_pos  = geom::Vec3(f1_transform * bb_list.GetN(parameters.p2));
  ca_pos = geom::Vec3(f1_transform * bb_list.GetCA(parameters.p2));
  c_pos  = geom::Vec3(f2_transform * bb_list.GetC(parameters.p2));
  o_pos  = geom::Vec3(f2_transform * bb_list.GetO(parameters.p2));
  promod3::core::ConstructCBetaPos(n_pos, ca_pos, c_pos, cb_pos);
  bb_list.SetN(parameters.p2, n_pos);
  bb_list.SetCA(parameters.p2, ca_pos);
  bb_list.SetC(parameters.p2, c_pos);
  bb_list.SetO(parameters.p2, o_pos);
  bb_list.SetCB(parameters.p2, cb_pos);
  // p3
  n_pos  = geom::Vec3(f2_transform * bb_list.GetN(parameters.p3));
  ca_pos = geom::Vec3(parameters.c_stem_transform * bb_list.GetCA(parameters.p3));
  c_pos  = geom::Vec3(parameters.c_stem_transform * bb_list.GetC(parameters.p3));
  o_pos  = geom::Vec3(parameters.c_stem_transform * bb_list.GetO(parameters.p3));
  promod3::core::ConstructCBetaPos(n_pos, ca_pos, c_pos, cb_pos);
  bb_list.SetN(parameters.p3, n_pos);
  bb_list.SetCA(parameters.p3, ca_pos);
  bb_list.SetC(parameters.p3, c_pos);
  bb_list.SetO(parameters.p3, o_pos);
  bb_list.SetCB(parameters.p3, cb_pos);

  // transform ranges
  bb_list.ApplyTransform(parameters.p1+1, parameters.p2, f1_transform);
  bb_list.ApplyTransform(parameters.p2+1, parameters.p3, f2_transform);
  bb_list.ApplyTransform(0, parameters.p1, parameters.n_stem_transform);
  bb_list.ApplyTransform(parameters.p3+1, bb_list.size(),
                         parameters.c_stem_transform);
}
} // anon ns

namespace promod3 { namespace modelling {

KIC::KIC(const ost::mol::ResidueHandle& n_stem, 
         const ost::mol::ResidueHandle& c_stem): n_stem_(n_stem),
                                                 c_stem_(c_stem) { }

void KIC::Close(const loop::BackboneList& bb_list, uint pivot_one,
                uint pivot_two, uint pivot_three,
                std::vector<loop::BackboneList>& solutions) const {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "KIC::Close", 2);

  // REQ: 0 < p1 < p2 < p3 < bb_list.size()-1
  if (pivot_one < 1 || pivot_three >= bb_list.size()-1
      || pivot_one >= pivot_two || pivot_two >= pivot_three) {
    throw promod3::Error("Observed invalid pivot residues when running KIC "
                         "loop closing algorithm!");
  }

  KICParameters parameters;
  // initialize Dixon Matrices directly with zeros
  promod3::core::EMat8 R0 = promod3::core::EMat8::Zero();
  promod3::core::EMat8 R1 = promod3::core::EMat8::Zero();
  promod3::core::EMat8 R2 = promod3::core::EMat8::Zero();
  // solutions will be stored in here
  std::vector<std::vector<Real> > taus;
  if (!FillKICParameters(bb_list, n_stem_, c_stem_, pivot_one, pivot_two,
                         pivot_three, parameters)) {
    return; //something went wrong... e.g. triangle inequality..
  }
  FillDixonMatrices(parameters,R0,R1,R2);
  ResolveEigenProblem(R0,R1,R2, taus);

  // check res. after c stem for oxygen reconstruction
  ost::mol::ResidueHandle after_c_stem;  // invalid by def.
  ost::mol::ResidueHandle c_stem_next = c_stem_.GetNext();
  if (c_stem_next.IsValid()) {
    // is it really the next residue?
    if (ost::mol::InSequence(c_stem_, c_stem_next)) {
      after_c_stem = c_stem_next;
    }
  }

  // add all solutions
  for (uint i = 0; i < taus.size(); ++i) {
    solutions.push_back(bb_list);
    ApplySolution(parameters, taus[i][0], taus[i][1], taus[i][2],
                  solutions.back());
    solutions.back().ReconstructCStemOxygen(after_c_stem);
  }
}

}} //ns
