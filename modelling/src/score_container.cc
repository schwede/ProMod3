// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/score_container.hh>

namespace promod3 { namespace modelling {

ScoreContainer::ScoreVector ScoreContainer::LinearCombine(
                    const std::map<String, Real>& linear_weights) const {
  ScoreVector result(num_candidates_, 0);
  for (std::map<String, Real>::const_iterator i = linear_weights.begin();
       i != linear_weights.end(); ++i) {
    const ScoreVector& scores = Get(i->first);
    const Real weight = i->second;
    for (uint j = 0; j < num_candidates_; ++j) result[j] += weight * scores[j];
  }
  return result;
}

ScoreContainerPtr
ScoreContainer::Extract(const std::vector<uint>& indices) const {
  // check indices
  for (uint j = 0; j < indices.size(); ++j) {
    if (indices[j] >= num_candidates_) {
      throw promod3::Error("Index out-of-bounds in ScoreContainer::Extract!");
    }
  }
  // get results
  ScoreContainerPtr result(new ScoreContainer);
  for (std::map<String, ScoreVector>::const_iterator i = map_.begin();
       i != map_.end(); ++i) {
    // setup scores directly in map of result
    ScoreVector& target_scores = result->map_[i->first];
    target_scores.resize(indices.size());
    for (uint j = 0; j < indices.size(); ++j) {
      target_scores[j] = i->second[indices[j]];
    }
  }
  result->num_candidates_ = indices.size();
  return result;
}

void ScoreContainer::Extend(const ScoreContainer& other) {
  // check consistency
  if (!IsEmpty()) {
    if (map_.size() != other.map_.size()) {
      throw promod3::Error("Inconsistent keys in ScoreContainer::Extend!");
    }
    for (std::map<String, ScoreVector>::const_iterator i = map_.begin();
         i != map_.end(); ++i) {
      if (!other.Contains(i->first)) {
        throw promod3::Error("Inconsistent keys in ScoreContainer::Extend!");
      }
    }
  }
  // extend this
  for (std::map<String, ScoreVector>::const_iterator i = other.map_.begin();
       i != other.map_.end(); ++i) {
    ScoreVector& scores = map_[i->first];
    const ScoreVector& scores_to_add = i->second;
    scores.insert(scores.end(), scores_to_add.begin(), scores_to_add.end());
  }
  num_candidates_ += other.num_candidates_;
}

}} // ns
