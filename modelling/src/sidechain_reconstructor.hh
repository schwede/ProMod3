// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SIDECHAIN_RECONSTRUCTOR_HH
#define PM3_SIDECHAIN_RECONSTRUCTOR_HH

#include <promod3/loop/all_atom_env.hh>
#include <promod3/modelling/sidechain_env_listener.hh>

namespace promod3 { namespace modelling {

class SidechainReconstructionData;
class SidechainReconstructor;
typedef boost::shared_ptr<SidechainReconstructionData>
        SidechainReconstructionDataPtr;
typedef boost::shared_ptr<SidechainReconstructor> SidechainReconstructorPtr;

/// \brief Helper object to pass around SidechainReconstructor results.
class SidechainReconstructionData {
public:
  // atoms and res. indices
  loop::AllAtomEnvPositionsPtr env_pos;
  // loop residues of env_pos are the ones actually being reconstructed
  // -> loop i_loop has res. indices "env_pos->res_indices[i]" for i in
  //    (loop_start_indices[i_loop] + [0, loop_lengths[i_loop] - 1])
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  // indices (into env_pos) of residues where we added rotamer sidechains
  std::vector<uint> rotamer_res_indices;
  // pairs of indices (into env_pos) of residues where we added disulfid bridge
  std::vector< std::pair<uint,uint> > disulfid_bridges;
  // for each residue in env_pos, we define if it is a C or N terminal residue
  std::vector<bool> is_n_ter;
  std::vector<bool> is_c_ter;
  // indices of frame residues (into all_pos) that are additionally considered 
  // in sidechain reconstruction
  std::vector<uint> rigid_frame_indices;
  // link to all env. positions used to get result (BB pos. should match)
  loop::ConstAllAtomPositionsPtr all_pos;
};

/// \brief Reconstruct loops using AllAtomEnv and SidechainEnvListener
class SidechainReconstructor {
public:

  SidechainReconstructor(bool keep_sidechains = true,
                         bool build_disulfids = true,
                         bool optimize_subrotamers = false,
                         Real remodel_cutoff = 20,
                         Real rigid_frame_cutoff = 0,
                         uint64_t graph_max_complexity = 100000000,
                         Real graph_intial_epsilon = 0.02,
                         Real disulfid_score_thresh = 45)
                         : keep_sidechains_(keep_sidechains)
                         , build_disulfids_(build_disulfids)
                         , optimize_subrotamers_(optimize_subrotamers)
                         , remodel_cutoff_(remodel_cutoff)
                         , rigid_frame_cutoff_(rigid_frame_cutoff)
                         , graph_max_complexity_(graph_max_complexity)
                         , graph_intial_epsilon_(graph_intial_epsilon)
                         , disulfid_score_thresh_(disulfid_score_thresh)
                         , attached_environment_(false) { }

  // reconstruct sidechains for a loop (must be in environment!)
  SidechainReconstructionDataPtr
  Reconstruct(uint start_resnum, uint num_residues, uint chain_idx = 0) const;

  SidechainReconstructionDataPtr
  Reconstruct(const std::vector<uint>& start_resnums,
              const std::vector<uint>& num_residues,
              const std::vector<uint>& chain_indices) const;

  // options as in SidechainEnvListener (only used if no listener there yet!)
  void AttachEnvironment(loop::AllAtomEnv& env, bool use_frm = true,
                         bool use_bbdep_lib = true);
  void AttachEnvironment(loop::AllAtomEnv& env, bool use_frm,
                         sidechain::BBDepRotamerLibPtr bbdep_library);
  void AttachEnvironment(loop::AllAtomEnv& env, bool use_frm,
                         sidechain::RotamerLibPtr library);

private:

  // indices = res. indices of loops to reconstruct
  // -> loop i_loop has res. indices "indices[i]" for i in 
  //    (loop_start_indices[i_loop] + [0, loop_lengths[i_loop] - 1])
  SidechainReconstructionDataPtr 
  ReconstructFromIndices(const std::vector<uint>& indices,
                         const std::vector<uint>& loop_start_indices,
                         const std::vector<uint>& loop_lengths) const;

  // HELPERS
  void CheckEnvListener_(bool use_frm) const;
  void SetEnvData_(loop::AllAtomEnv& env);

  // handles disulfid bridges for cys_indices
  // -> cys_indices[i] can index into has_sidechain / res->env_pos->all_pos
  // -> pairs of cys_indices[i] for bridges added to res->disulfid_bridges
  // -> unhandled CYS with known SC are added to frame_residues
  // -> has_sidechain is updated as needed (i.e. when SC has been dealt with)
  void BuildDisulfids_(SidechainReconstructionDataPtr res,
                       std::vector<uint>& cys_indices,
                       std::vector<sidechain::FrameResiduePtr>& frame_residues,
                       std::vector<bool>& has_sidechain) const;

  // collects rotamers for residues with has_sidechain[i] = false
  // -> adds to rotamer_groups and res->rotamer_res_indices (same size)
  // -> rot. groups are copied to allow for reuse...
  template<typename RotamerGroupPtr>
  void CollectRotamerGroups_(SidechainReconstructionDataPtr res,
                             std::vector<RotamerGroupPtr>& rotamer_groups,
                             std::vector<bool>& has_sidechain) const;

  // solve system: templatized on rotamer type used
  // -> IN: res->env_pos->res_indices, res->num_reconstruct
  // -> OUT: res->env_pos->all_pos, res->rotamer_res_indices
  template<typename RotamerGroup>
  void SolveSystem_(SidechainReconstructionDataPtr res) const;

  // reconstruction parameters
  bool keep_sidechains_;
  bool build_disulfids_;
  bool optimize_subrotamers_;
  Real remodel_cutoff_;
  Real rigid_frame_cutoff_;
  uint64_t graph_max_complexity_;
  Real graph_intial_epsilon_;
  Real disulfid_score_thresh_;

  // environment specific information, set upon calling AttachEnvironment
  bool attached_environment_;
  SidechainEnvListenerPtr env_;
  loop::ConstIdxHandlerPtr idx_handler_;
  loop::ConstAllAtomPositionsPtr all_pos_;
};

}} // ns

#endif
