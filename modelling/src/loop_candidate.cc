// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/loop_candidate.hh>
#include <promod3/modelling/kic.hh>
#include <promod3/modelling/scoring_weights.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/geom_stems.hh>
#include <promod3/core/cluster.hh>
#include <algorithm>

namespace promod3 { namespace modelling {

namespace {

// check if BB and N/C orientation match well enough
// motivation:
// - SearchDB with extra_bins = 1 has width of 2 bins but not centered
// - we could use extra_bins = 2 and check for +- bin_size here...
bool IsWithinBounds(const loop::BackboneList& bb_list,
                    const core::StemPairOrientation& stem_o,
                    Real max_dist_diff, Real max_ang_dist) {
  // get data
  const uint last_idx = bb_list.size() - 1;
  core::StemPairOrientation bb_o(bb_list.GetN(0), bb_list.GetCA(0),
                                 bb_list.GetC(0), bb_list.GetN(last_idx),
                                 bb_list.GetCA(last_idx),
                                 bb_list.GetC(last_idx));
  return (   std::abs(bb_o.distance - stem_o.distance) <= max_dist_diff
          && std::abs(bb_o.angle_one - stem_o.angle_one) <= max_ang_dist
          && std::abs(bb_o.angle_two - stem_o.angle_two) <= max_ang_dist
          && std::abs(bb_o.angle_three - stem_o.angle_three) <= max_ang_dist
          && std::abs(bb_o.angle_four - stem_o.angle_four) <= max_ang_dist);
}

} // anon ns

LoopCandidates::LoopCandidates(const String& seq): sequence_(seq) { }

void LoopCandidates::Add(const loop::BackboneList& candidate) {
  // we check, whether the length and the sequence are consistent
  if (candidate.size() != sequence_.size()) {
    throw promod3::Error("Loop candidate to add does not have the required size!");
  }
  if (sequence_ != candidate.GetSequence()) {
    throw promod3::Error("Loop candidate does not have the required sequence!");
  }
  candidates_.push_back(candidate);
}

void LoopCandidates::AddFragmentInfo(const loop::FragmentInfo& fragment) {
  fragments_.push_back(fragment);
}

void LoopCandidates::Remove(uint index){
  if (index >= this->size()) {
    throw promod3::Error("Index is out of bounds!");
  }
  // update fragments_ if there
  if (HasFragmentInfos()) fragments_.erase(fragments_.begin()+index);
  // remove candidate
  candidates_.erase(candidates_.begin()+index);
}

bool LoopCandidates::HasFragmentInfos() const {
  // sizes must match
  return (fragments_.size() == candidates_.size());
}

const loop::FragmentInfo& LoopCandidates::GetFragmentInfo(uint index) const {
  if (!HasFragmentInfos()) {
    throw promod3::Error("FragmentInfos are invalid in LoopCandidates!");
  }
  if (index >= this->size()) {
    throw promod3::Error("Index is out of bounds!");
  }
  return fragments_[index];
}

LoopCandidatesPtr
LoopCandidates::Extract(const std::vector<uint>& indices) const {
  // setup and check indices
  const bool had_fragments = HasFragmentInfos();
  for (uint i = 0; i < indices.size(); ++i) {
    if (indices[i] >= this->size()) {
      throw promod3::Error("Index is out of bounds!");
    }
  }
  // create new set of candidates
  LoopCandidatesPtr new_candidates(new LoopCandidates(sequence_));
  for (uint i = 0; i < indices.size(); ++i) {
    new_candidates->Add(candidates_[indices[i]]);
    if (had_fragments) new_candidates->AddFragmentInfo(fragments_[indices[i]]);
  }
  return new_candidates;
}

LoopCandidatesPtr LoopCandidates::FillFromDatabase(
                            const ost::mol::ResidueHandle& n_stem,
                            const ost::mol::ResidueHandle& c_stem,
                            const String& seq, loop::FragDBPtr frag_db,
                            loop::StructureDBPtr structure_db,
                            bool extended_search) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "LoopCandidates::FillFromDatabase", 2);

  LoopCandidatesPtr candidates(new LoopCandidates(seq));
  // look for fragments: with extended_search we do a centered +- bin size
  std::vector<loop::FragmentInfo> fragments;
  const uint extra_bins = (extended_search) ? 2 : 0;
  frag_db->SearchDB(n_stem, c_stem, seq.size(), fragments, extra_bins);
  // setup data for IsWithinBounds
  const core::StemCoords n_stem_c(n_stem);
  const core::StemCoords c_stem_c(c_stem);
  const core::StemPairOrientation stem_o(n_stem_c, c_stem_c);
  const Real dist_bin_size = frag_db->GetDistBinSize();
  const Real ang_bin_size = (frag_db->GetAngularBinSize()*Real(M_PI)) / 180;
  // add fragments
  for (uint i = 0; i < fragments.size(); ++i) {
    loop::BackboneList bb_list;
    structure_db->FillBackbone(bb_list, n_stem, c_stem, fragments[i], seq);
    if (!extended_search || IsWithinBounds(bb_list, stem_o, dist_bin_size,
                                           ang_bin_size)) {
      candidates->Add(bb_list);
      candidates->AddFragmentInfo(fragments[i]);
    }
  }

  return candidates;
}

LoopCandidatesPtr LoopCandidates::FillFromMonteCarloSampler(
                            const String& seq, uint num_loops, uint steps,
                            MonteCarloSamplerPtr sampler,
                            MonteCarloCloserPtr closer,
                            MonteCarloScorerPtr scorer,
                            MonteCarloCoolerPtr cooler, int random_seed) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "LoopCandidates::FillFromMonteCarloSampler", 2);

  LoopCandidatesPtr candidates(new LoopCandidates(seq));

  for (uint i = 0; i < num_loops; ++i) {
    loop::BackboneList new_backbone;
    cooler->Reset();
    SampleMonteCarlo(sampler, closer, scorer, cooler, steps, new_backbone,
                     true, random_seed + i);
    candidates->Add(new_backbone);
  }
  return candidates;
}

LoopCandidatesPtr LoopCandidates::FillFromMonteCarloSampler(
                            const loop::BackboneList& initial_bb,
                            const String& seq, uint num_loops, uint steps,
                            MonteCarloSamplerPtr sampler,
                            MonteCarloCloserPtr closer,
                            MonteCarloScorerPtr scorer,
                            MonteCarloCoolerPtr cooler, int random_seed) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "LoopCandidates::FillFromMonteCarloSampler", 2);

  //check seq and initial conformation for consistency
  if (seq.size() != initial_bb.size()) {
    throw promod3::Error("Sequence and initial conformation are inconsistent "
                         "in size!");
  }

  for (uint i = 0; i < seq.size(); ++i) {
    if (initial_bb.GetAA(i) != ost::conop::OneLetterCodeToAminoAcid(seq[i])) {
      throw promod3::Error("Sequence and initial conformation are inconsistent "
                           "in aa identity!");
    }
  }

  LoopCandidatesPtr candidates(new LoopCandidates(seq));

  for(uint i = 0; i < num_loops; ++i){
    loop::BackboneList new_backbone = initial_bb;
    cooler->Reset();
    SampleMonteCarlo(sampler, closer, scorer, cooler, steps, new_backbone,
                     false, random_seed + i);
    candidates->Add(new_backbone);
  }
  return candidates;
}

std::vector<uint>
LoopCandidates::ApplyCCD(CCD& closer, bool keep_non_converged) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "LoopCandidates::ApplyCCD", 2);
  // keep track of original indices
  std::vector<uint> original_indices;
  // keep fixed original size and count deleted ones
  const uint num_candidates = candidates_.size();
  uint deleted_candidates = 0;
  for (uint i = 0; i < num_candidates; ++i) {
    const uint cur_idx = i - deleted_candidates;
    const bool closed = closer.Close(candidates_[cur_idx]);
    if (!closed && !keep_non_converged) {
      this->Remove(cur_idx);
      ++deleted_candidates;
    } else {
      original_indices.push_back(i);
    }
  }
  return original_indices;
}

std::vector<uint>
LoopCandidates::ApplyCCD(const ost::mol::ResidueHandle& n_stem,
                         const ost::mol::ResidueHandle& c_stem,
                         loop::TorsionSamplerList torsion_sampler, 
                         int max_iterations, Real rmsd_cutoff, 
                         bool keep_non_converged, int random_seed) {
  CCD closer(sequence_, n_stem, c_stem, torsion_sampler, max_iterations,
             rmsd_cutoff, random_seed);
  return this->ApplyCCD(closer, keep_non_converged);
}

std::vector<uint>
LoopCandidates::ApplyCCD(const ost::mol::ResidueHandle& n_stem,
                         const ost::mol::ResidueHandle& c_stem,
                         loop::TorsionSamplerPtr torsion_sampler, 
                         int max_iterations, Real rmsd_cutoff, 
                         bool keep_non_converged, int random_seed) {
  CCD closer(sequence_, n_stem, c_stem, torsion_sampler, max_iterations,
             rmsd_cutoff, random_seed);
  return this->ApplyCCD(closer, keep_non_converged);
}

std::vector<uint>
LoopCandidates::ApplyCCD(const ost::mol::ResidueHandle& n_stem,
                         const ost::mol::ResidueHandle& c_stem,
                         int max_iterations, Real rmsd_cutoff, 
                         bool keep_non_converged) {
  CCD closer(n_stem, c_stem, max_iterations, rmsd_cutoff);
  return this->ApplyCCD(closer, keep_non_converged);
}

std::vector<uint>
LoopCandidates::ApplyKIC(const ost::mol::ResidueHandle& n_stem,
                         const ost::mol::ResidueHandle& c_stem,
                         uint pivot_one, uint pivot_two, uint pivot_three) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "LoopCandidates::ApplyKIC", 2);
  // setup
  LoopCandidateList new_candidates;
  std::vector<loop::FragmentInfo> new_fragments;
  const bool had_fragments = HasFragmentInfos();
  KIC closer(n_stem, c_stem);
  // keep track of original indices of converged ones
  std::vector<uint> original_indices;
  // apply KIC to each candidate
  for (uint i = 0; i < candidates_.size(); ++i) {
    // find possible closed conformations and add all to new set of candidates
    std::vector<loop::BackboneList> bb_lists;
    closer.Close(candidates_[i], pivot_one, pivot_two, pivot_three, bb_lists);
    new_candidates.insert(new_candidates.end(), bb_lists.begin(), bb_lists.end());
    if (had_fragments) {
      new_fragments.insert(new_fragments.end(), bb_lists.size(), fragments_[i]);
    }
    original_indices.insert(original_indices.end(), bb_lists.size(), i);
  }
  // store new values
  candidates_ = new_candidates;
  fragments_ = new_fragments;
  return original_indices;
}

void
LoopCandidates::GetClusters(Real max_dist,
                            std::vector< std::vector<uint> >& clusters,
                            bool superposed_rmsd) const {
  // check input
  if (max_dist < 0.0) {
    throw promod3::Error("Have you ever seen a negative RMSD?");
  }
  // note: empty list of candidates returns an empty list (i.e. 0 clusters)

  // setup
  clusters.clear();
  const uint num_loops = candidates_.size();
  // handle trivial case
  if (num_loops == 1) {
    clusters.resize(1);
    clusters[0].push_back(0);
  } else if (num_loops > 0) {

    ost::TriMatrix<Real> rmsd_matrix(num_loops, Real(0.0));
    
    if(superposed_rmsd){
      // prepare the Eigen positions
      promod3::core::EMatX3List position_list;
      position_list.reserve(num_loops);
      uint loop_size = sequence_.size();
      for(uint i = 0; i < num_loops; ++i){
        promod3::core::EMatX3 bb_list_positions = 
        promod3::core::EMatX3::Zero(loop_size, 3);
        for(uint j = 0; j < loop_size; ++j){
          promod3::core::EMatFillRow(bb_list_positions, j, 
                                     candidates_[i].GetCA(j));
        }
        position_list.push_back(bb_list_positions);
      }
      promod3::core::FillRMSDMatrix(position_list, rmsd_matrix);
    }
    else{
      for(uint i = 0; i < this->size(); ++i){
        for(uint j = i + 1; j < this->size(); ++j){
          Real ca_rmsd = candidates_[i].CARMSD(candidates_[j], false);
          rmsd_matrix.Set(i, j, ca_rmsd);
        }
      }
    }

    // get clusters
    promod3::core::DoClustering(rmsd_matrix, promod3::core::AVG_DIST_METRIC,
                                max_dist, '-', clusters);
  }
}

LoopCandidatesList
LoopCandidates::GetClusteredCandidates(Real max_dist,
                                       bool neglect_size_one,
                                       bool superposed_rmsd) const {

  // get clusters
  std::vector< std::vector<uint> > clusters;
  GetClusters(max_dist, clusters, superposed_rmsd);
  // get all candidates
  LoopCandidatesList return_list;
  for (uint i = 0; i < clusters.size(); ++i) {
    if (clusters[i].size() > 1 || !neglect_size_one) {
      return_list.push_back(Extract(clusters[i]));
    }
  }
  return return_list;
}

LoopCandidatesPtr LoopCandidates::GetLargestCluster(Real max_dist,
                                                    bool superposed_rmsd) const{
  // check input
  if (candidates_.empty()) {
    throw promod3::Error("No clusters for empty LoopCandidates!");
  }
  if (max_dist < 0.0) {
    throw promod3::Error("Have you ever seen a negative RMSD?");
  }

  const uint num_loops = candidates_.size();
  // handle trivial case
  if (num_loops == 1) {
    std::vector<uint> indices;
    indices.push_back(0);
    return Extract(indices);
  } else {

    ost::TriMatrix<Real> rmsd_matrix(num_loops, Real(0.0));

    if(superposed_rmsd){
      // prepare the Eigen positions
      promod3::core::EMatX3List position_list;
      position_list.reserve(num_loops);
      uint loop_size = sequence_.size();
      for(uint i = 0; i < num_loops; ++i){
        promod3::core::EMatX3 bb_list_positions = 
        promod3::core::EMatX3::Zero(loop_size, 3);
        for(uint j = 0; j < loop_size; ++j){
          promod3::core::EMatFillRow(bb_list_positions, j, 
                                     candidates_[i].GetCA(j));
        }
        position_list.push_back(bb_list_positions);
      }
      promod3::core::FillRMSDMatrix(position_list, rmsd_matrix);
    }
    else{
      for(uint i = 0; i < this->size(); ++i){
        for(uint j = i + 1; j < this->size(); ++j){
          Real ca_rmsd = candidates_[i].CARMSD(candidates_[j], false);
          rmsd_matrix.Set(i, j, ca_rmsd);
        }
      }
    }

    // search for the candidate with maximal neighbours
    int max_num = 0;
    int max_idx = 0;

    for(uint i = 0; i < num_loops; ++i){
      int num = 0;
      for(uint j = 0; j < num_loops; ++j){
        if(rmsd_matrix.Get(i, j) < max_dist) {
          ++num;
        }
      }
      if(num > max_num){
        max_num = num;
        max_idx = i;
      }
    }

    std::vector<uint> indices;
    for(uint i = 0; i < num_loops; ++i){
      if(rmsd_matrix.Get(max_idx, i) < max_dist) {
        indices.push_back(i);
      }
    }

    return Extract(indices);
  }
}

void LoopCandidates::CalculateBackboneScores(
                                      ScoreContainer& score_container,
                                      scoring::BackboneOverallScorerPtr scorer,
                                      scoring::BackboneScoreEnvPtr scorer_env,
                                      const std::vector<String>& keys,
                                      uint start_resnum, uint chain_idx) const {

  uint N = candidates_.size();
  uint L = sequence_.size();

  scorer_env->Stash(start_resnum, L, chain_idx);
  
  for (uint i = 0; i < keys.size(); ++i) {
    std::vector<Real> scores(N);
    for(uint j = 0; j < N; ++j) {
      scorer_env->SetEnvironment(candidates_[j], start_resnum, chain_idx);
      scores[j] = scorer->Calculate(keys[i], start_resnum, L, chain_idx);
    }
    score_container.Set(keys[i], scores);
  }
  scorer_env->Pop();
}

void LoopCandidates::CalculateBackboneScores(
                                      ScoreContainer& score_container,
                                      scoring::BackboneOverallScorerPtr scorer,
                                      scoring::BackboneScoreEnvPtr scorer_env,
                                      uint start_resnum, uint chain_idx) const {

  const std::vector<String>&
  keys = ScoringWeights::GetInstance().GetBackboneScoringKeys();
  CalculateBackboneScores(score_container, scorer, scorer_env, keys, 
                          start_resnum, chain_idx);
}

void LoopCandidates::CalculateBackboneScores(
                    ScoreContainer& score_container,
                    const ModellingHandle& mhandle,
                    uint start_resnum, uint chain_idx) const {

  const std::vector<String>&
  keys = ScoringWeights::GetInstance().GetBackboneScoringKeys();
  scoring::BackboneOverallScorerPtr scorer = mhandle.backbone_scorer;
  scoring::BackboneScoreEnvPtr env = mhandle.backbone_scorer_env;
  CalculateBackboneScores(score_container, scorer, env, keys, start_resnum,
                          chain_idx);
}

void LoopCandidates::CalculateBackboneScores(
                    ScoreContainer& score_container,
                    const ModellingHandle& mhandle,
                    const std::vector<String>& keys,
                    uint start_resnum, uint chain_idx) const {

  scoring::BackboneOverallScorerPtr scorer = mhandle.backbone_scorer;
  scoring::BackboneScoreEnvPtr env = mhandle.backbone_scorer_env;
  CalculateBackboneScores(score_container, scorer, env, keys, start_resnum,
                          chain_idx);
}

void LoopCandidates::CalculateAllAtomScores(
                    ScoreContainer& score_container,
                    const ModellingHandle& mhandle,
                    uint start_resnum, uint chain_idx) const {
  const std::vector<String>&
  keys = ScoringWeights::GetInstance().GetAllAtomScoringKeys();
  CalculateAllAtomScores(score_container, mhandle, keys, start_resnum,
                         chain_idx);
}
void LoopCandidates::CalculateAllAtomScores(
                    ScoreContainer& score_container,
                    const ModellingHandle& mhandle,
                    const std::vector<String>& keys,
                    uint start_resnum, uint chain_idx) const {

  core::ScopedTimerPtr timer = core::StaticRuntimeProfiler::StartScoped(
                          "LoopCandidates::CalculateAllAtomScores", 2);

  // check modelling handle
  if (!IsAllAtomScoringSetUp(mhandle)) {
    throw promod3::Error("All atom scoring must be set up before calculating "
                         "all atom scores!");
  }

  // get stuff from mhandle (short names for convenience)
  loop::AllAtomEnvPtr aa_score_env = mhandle.all_atom_scorer_env;
  scoring::AllAtomOverallScorerPtr aa_scorer = mhandle.all_atom_scorer;
  loop::AllAtomEnvPtr aa_sc_env = mhandle.all_atom_sidechain_env;
  SidechainReconstructorPtr sc_rec = mhandle.sidechain_reconstructor;
  SidechainReconstructionDataPtr sc_result;

  // prepare storage for all scores
  std::vector< std::vector<Real> > aa_scores(keys.size());
  for (uint i_k = 0; i_k < keys.size(); ++i_k) {
    aa_scores[i_k].resize(size());
  }

  // keep backup of loop of interest
  const uint loop_length = sequence_.size();
  loop::AllAtomEnvPositionsPtr loop_backup;
  loop_backup = aa_sc_env->GetEnvironment(start_resnum, loop_length, chain_idx);

  // for each loop: reconstruct sidechains and get all scores
  for (uint i_lc = 0; i_lc < size(); ++i_lc) {
    // setup sidechain env. and reconstruct
    aa_sc_env->SetEnvironment(candidates_[i_lc], start_resnum, chain_idx);
    sc_result = sc_rec->Reconstruct(start_resnum, loop_length, chain_idx);
    // fix env. for scoring (always contains loop and relevant surrounding!)
    aa_score_env->SetEnvironment(*(sc_result->env_pos));
    for (uint i_k = 0; i_k < keys.size(); ++i_k) {
      scoring::AllAtomScorerPtr scorer = aa_scorer->Get(keys[i_k]);
      aa_scores[i_k][i_lc] = scorer->CalculateScore(start_resnum, loop_length,
                                                    chain_idx);
    }
  }

  // update score container
  for (uint i_k = 0; i_k < keys.size(); ++i_k) {
    score_container.Set(keys[i_k], aa_scores[i_k]);
  }

  // reset aa_sc_env
  aa_sc_env->SetEnvironment(*loop_backup);
  // NOTE: we leave the aa_score_env in a modified state
  // -> this is ok for aa_score_env as we always set proper surroundings
  // -> this is not ok for SC (due to frame approach), so we back it up
}

std::vector<Real> LoopCandidates::CalculateSequenceProfileScores(
                    loop::StructureDBPtr structure_db,
                    const ost::seq::ProfileHandle& prof, uint offset) const {

  core::ScopedTimerPtr timer = core::StaticRuntimeProfiler::StartScoped(
                          "LoopCandidates::CalculateSequenceProfileScores", 2);
  // check
  if (!HasFragmentInfos()) {
    throw promod3::Error("FragmentInfos are invalid in LoopCandidates!");
  }

  // get scores
  std::vector<Real> scores(this->size());
  ost::seq::ProfileHandlePtr lc_prof;
  for (uint i = 0; i < this->size(); ++i) {
    lc_prof = structure_db->GetSequenceProfile(fragments_[i]);
    scores[i] = prof.GetAverageScore(*lc_prof, offset);
  }
  return scores;

}
void LoopCandidates::CalculateSequenceProfileScores(
                    ScoreContainer& score_container,
                    loop::StructureDBPtr structure_db,
                    const ost::seq::ProfileHandle& prof, uint offset) const {
  // use default key
  score_container.Set(
            ScoringWeights::GetInstance().GetSequenceProfileScoresKey(),
            CalculateSequenceProfileScores(structure_db, prof, offset));
}

std::vector<Real> LoopCandidates::CalculateStructureProfileScores(
                    loop::StructureDBPtr structure_db,
                    const ost::seq::ProfileHandle& prof, uint offset) const {

  core::ScopedTimerPtr timer = core::StaticRuntimeProfiler::StartScoped(
                          "LoopCandidates::CalculateStructureProfileScores", 2);
  // check
  if (!HasFragmentInfos()) {
    throw promod3::Error("FragmentInfos are invalid in LoopCandidates!");
  }

  // get scores
  std::vector<Real> scores(this->size());
  ost::seq::ProfileHandlePtr lc_prof;
  for (uint i = 0; i < this->size(); ++i) {
    lc_prof = structure_db->GetStructureProfile(fragments_[i]);
    scores[i] = prof.GetAverageScore(*lc_prof, offset);
  }
  return scores;
}
void LoopCandidates::CalculateStructureProfileScores(
                    ScoreContainer& score_container,
                    loop::StructureDBPtr structure_db,
                    const ost::seq::ProfileHandle& prof, uint offset) const {
  // use default key
  score_container.Set(
            ScoringWeights::GetInstance().GetStructureProfileScoresKey(),
            CalculateStructureProfileScores(structure_db, prof, offset));
}

std::vector<Real> LoopCandidates::CalculateStemRMSDs(
                    const ost::mol::ResidueHandle& n_stem, 
                    const ost::mol::ResidueHandle& c_stem) const {

  core::ScopedTimerPtr timer = core::StaticRuntimeProfiler::StartScoped(
                                "LoopCandidates::CalculateStemRMSDs", 2);
  
  // get/check target coordinates
  ost::mol::AtomHandle n = c_stem.FindAtom("N");
  ost::mol::AtomHandle ca = c_stem.FindAtom("CA");
  ost::mol::AtomHandle c = c_stem.FindAtom("C");
  if (!n.IsValid() || !ca.IsValid() || !c.IsValid()) {
    throw promod3::Error("Could not find N, CA or C atom in c_stem!");
  }

  // get scores
  std::vector<Real> scores(this->size());
  for (uint i = 0; i < this->size(); ++i) {
    // get copy of candidate
    loop::BackboneList bb_list = candidates_[i].Extract(0, sequence_.size());
    // superpose N terminus of loop
    geom::Mat4 t = bb_list.GetTransform(0, n_stem);
    bb_list.ApplyTransform(t);
    // get RMSD of C terminus w.r.t. C stem
    Real msd = 0.0;
    const uint last_idx = bb_list.size() - 1;
    msd += geom::Length2(bb_list.GetN(last_idx) - n.GetPos());
    msd += geom::Length2(bb_list.GetCA(last_idx) - ca.GetPos());
    msd += geom::Length2(bb_list.GetC(last_idx) - c.GetPos());
    scores[i] = std::sqrt(msd/3);
  }
  return scores;
}
void LoopCandidates::CalculateStemRMSDs(
                    ScoreContainer& score_container,
                    const ost::mol::ResidueHandle& n_stem, 
                    const ost::mol::ResidueHandle& c_stem) const {
  // use default key
  score_container.Set(ScoringWeights::GetInstance().GetStemRMSDsKey(),
                      CalculateStemRMSDs(n_stem, c_stem));
}

}}
