// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_CCD_HH
#define PROMOD_MODELLING_CCD_HH

#include <vector>
#include <math.h>

#include <ost/mol/mol.hh>
#include <ost/geom/vecmat3_op.hh>

#include <promod3/loop/torsion_sampler.hh>
#include <promod3/core/message.hh>
#include <promod3/loop/backbone.hh>

namespace promod3 { namespace modelling {

namespace {
// helper to get angle bin for torsion samplers
inline int GetAngleBin(int bins_per_dimension, Real angle) {
  // bins_per_dimension = 2*pi/bin_size => 1/bin_size = sc
  const Real sc = bins_per_dimension / (Real(2 * M_PI));
  angle += Real(M_PI);
  while (angle < 0) angle += 2*Real(M_PI);
  return int(angle * sc) % bins_per_dimension;
}
}

class CCD;
typedef boost::shared_ptr<CCD> CCDPtr;

class CCD {

public:
  CCD(const String& sequence, const ost::mol::ResidueHandle& n_stem,
      const ost::mol::ResidueHandle& c_stem,
      loop::TorsionSamplerPtr torsion_sampler,
      uint max_steps = 1000, Real rmsd_cutoff = 0.1, int random_seed = 0);

  CCD(const String& sequence, const ost::mol::ResidueHandle& n_stem, 
      const ost::mol::ResidueHandle& c_stem,
      const loop::TorsionSamplerList& torsion_sampler, 
      uint max_steps = 1000, Real rmsd_cutoff = 0.1, int random_seed = 0);

  CCD(const ost::mol::ResidueHandle& n_stem, const ost::mol::ResidueHandle& c_stem, 
      uint max_steps = 1000, Real rmsd_cutoff = 0.1);

  bool Close(loop::BackboneList& bb_list);

private:

  // data
  ost::mol::ResidueHandle n_stem_;
  ost::mol::ResidueHandle after_c_stem_;
  geom::Vec3 target_positions_[3];
  std::vector<uint> dihedral_indices_;
  loop::TorsionSamplerList torsion_sampler_;
  Real n_stem_phi_;
  Real c_stem_psi_;
  uint max_steps_;
  Real rmsd_cutoff_;
  bool constrained_;
  int random_seed_; // only for constrained_ = true

  // extract prob. from torsion sampler (using inlined functions)
  Real GetProbability(const std::pair<Real,Real>& dihedrals,
                      const uint res_index) {
    int bins_per_dimension = torsion_sampler_[res_index]->GetBinsPerDimension();
    int phi_bin = GetAngleBin(bins_per_dimension, dihedrals.first);
    int psi_bin = GetAngleBin(bins_per_dimension, dihedrals.second);
    return torsion_sampler_[res_index]->GetProbability(dihedral_indices_[res_index], phi_bin, psi_bin);
  }

  // helpers for closers -> phi/psi = rotations to perform at each index
  bool GetSimpleCloseRotations_(geom::Vec3* positions, Real* rot_phi,
                                Real* rot_psi, const uint num_residues);
  bool GetConstrainedCloseRotations_(geom::Vec3* positions, Real* rot_phi,
                                     Real* rot_psi, const uint num_residues);
  
  // working on internal data
  void SetStems_(const ost::mol::ResidueHandle& n_stem,
                 const ost::mol::ResidueHandle& c_stem);
  void SetTargetPositions_(const ost::mol::ResidueHandle& c_stem);
  void SetTorsionParametrization_(const ost::mol::ResidueHandle& before,
                                  const ost::mol::ResidueHandle& after,
                                  const String& sequence);
};

}} //ns
#endif
