// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_MODELLING_GAP_HH
#define PM3_MODELLING_GAP_HH

#include <ost/mol/mol.hh>
#include <promod3/core/message.hh>

namespace promod3 { namespace modelling {
  
/// Describes a structural gap, i.e. a loop to be modeled. The gap may either be
/// terminal or between two defined regions. The gap stores information of the
/// last residue before and the first residue after the gap as well as the
/// sequence of gap.
struct StructuralGap {
  
  StructuralGap(ost::mol::ResidueHandle b, ost::mol::ResidueHandle a, String s):
    before(b), after(a), sequence(s)
  { 
    if(a.IsValid() && b.IsValid()){
      if(a.GetChain() != b.GetChain()){
        std::stringstream ss;
        ss << "The two gap stem residues must be from the same chain!";
        throw promod3::Error(ss.str());
      }
      if(b.GetNumber() >= a.GetNumber()){
        std::stringstream ss;
        ss << "Resnum of c_stem must be bigger than resnum from n_stem!";
        throw promod3::Error(ss.str());
      }
      if(s.size() != a.GetNumber()-b.GetNumber()-1){
        std::stringstream ss;
        ss << "Size of provided sequence is inconsistent with the ";
        ss << "residue numbers of the stem residues!";
        throw promod3::Error(ss.str());
      }
    }
    if(!a.IsValid() && !b.IsValid()){
      throw promod3::Error("At least one residue must be valid when initializing gap!");
    }
  }
  
  /// \brief get full sequence, including stem residues
  String GetFullSeq() const
  {
    std::stringstream s;
    if (this->before.IsValid()) {
      s << before.GetOneLetterCode();
    }
    s << sequence;
    if (after.IsValid()) {
      s << after.GetOneLetterCode();
    }
    return s.str();
  }

  /// \brief returns name of chain, the gap is belonging to
  String GetChainName() const;

  /// \briaf returns chain, the gap is belonging to
  ost::mol::ChainHandle GetChain() const;

  /// \brief returns index of chain, the gap is belonging to
  uint GetChainIndex() const;

  /// Whether the gap is N-terminal. If true, only 'after' points to a valid 
  /// residue.  
  bool IsNTerminal() const { return !before.IsValid() && after.IsValid(); }
  /// Whether the gap is C-terminal. If true, only 'before' points to a valid 
  /// residue.
  bool IsCTerminal() const { return before.IsValid() && !after.IsValid(); }

  /// \brief Whether the gap is N- or C-terminal.
  bool IsTerminal() const { return !(before.IsValid() && after.IsValid()); }
  
  
  /// \brief get length of the gap
  int GetLength() const { return sequence.length(); }
  
  /// \brief Try to extend gap at C-terminal end of gap.
  /// 
  /// Only possible if the gap is not at C-terminal and it doesn't try to
  /// extend the gap past another gap.
  ///
  /// \returns True, iff extend succeeded (gap is only updated in that case)
  bool ExtendAtCTerm();

  /// \brief Try to extend gap at N-terminal end of gap.
  /// 
  /// Only possible if the gap is not at N-terminal and it doesn't try to
  /// extend the gap past another gap.
  ///
  /// \returns True, iff extend succeeded (gap is only updated in that case)
  bool ExtendAtNTerm();
  
  /// \brief Try to shift gap by one position towards C-terminal.
  ///
  /// Only possible if new gap is not terminal and it doesn't try to shift the
  /// gap past another gap.
  ///
  /// \returns True, iff shift succeeded (gap is only updated in that case)
  bool ShiftCTerminal();
  
  String AsString() const;

  bool operator==(const StructuralGap& rhs) const
  {
    return before==rhs.before && after==rhs.after && sequence==rhs.sequence;
  }
  
  bool operator!=(const StructuralGap& rhs) const
  {
    return !this->operator==(rhs);
  }
  /// very simple hashing function
  uint GetHashCode() const 
  {
    if (this->IsCTerminal()) {
      return before.GetNumber().GetNum();
    } else if (this->IsNTerminal()) {
      return after.GetNumber().GetNum()*117;
    }
    return before.GetNumber().GetNum()+after.GetNumber().GetNum()*117;
  }
  
  ost::mol::EntityHandle GetEntity() const
  {
    if (before) { return before.GetEntity(); }
    if (after) { return after.GetEntity(); }
    return ost::mol::EntityHandle();
  }

  StructuralGap Copy() const { return StructuralGap(before, after, sequence); }
  
  void Transfer(const ost::mol::ChainHandle& chain);

  ost::mol::ResidueHandle before;
  ost::mol::ResidueHandle after;
  String                  sequence;
};

inline std::ostream&
operator<<(std::ostream& o, const StructuralGap& g)
{
  o << g.AsString();
  return o;
}

typedef std::vector<StructuralGap> StructuralGapList;

}}

#endif
