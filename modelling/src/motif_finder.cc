// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/motif_finder.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/modelling/robin_hood.h>
#include <promod3/core/portable_binary_serializer.hh>
#include <promod3/core/check_io.hh>

#include <promod3/core/eigen_types.hh>
#include <promod3/core/superpose.hh>

#if PM3_ENABLE_SSE && OST_DOUBLE_PRECISION == 0
#include <xmmintrin.h>
#endif
#include <limits>

namespace {
  
struct MotifHasherKey {
  
  MotifHasherKey(): key(0) { }
  
  inline void SetF(uint32_t f) {
    key = (key & (~f_mask)) | (static_cast<uint64_t>(f) << 57);    
  }
  inline void SetG(uint32_t g) {
    key = (key & (~g_mask)) | (static_cast<uint64_t>(g) << 51);    
  }
  inline void SetH(uint32_t h) {
    key = (key & (~h_mask)) | (static_cast<uint64_t>(h) << 45);    
  }
  inline void SetI(uint32_t i) {
    key = (key & (~i_mask)) | (static_cast<uint64_t>(i) << 39);    
  }
  inline void SetA(uint32_t a) {
    key = (key & (~a_mask)) | (static_cast<uint64_t>(a) << 34);
  }
  inline void SetB(uint32_t b) {
    key = (key & (~b_mask)) | (static_cast<uint64_t>(b) << 29);
  }
  inline void SetC(uint32_t c) {
    key = (key & (~c_mask)) | (static_cast<uint64_t>(c) << 24);
  }
  inline void SetX(int32_t x) {
    key = (key & (~x_mask)) | ((static_cast<uint64_t>(x+127)) << 16);
  }
  inline void SetY(int32_t y) {
    key = (key & (~y_mask)) | ((static_cast<uint64_t>(y+127)) << 8);
  }
  inline void SetZ(int32_t z) {
    key = (key & (~z_mask)) | (static_cast<uint64_t>(z+127));
  }
    
  bool operator==(const MotifHasherKey& other) const {
    return key == other.key;
  }

  bool operator!=(const MotifHasherKey& other) const {
    return key != other.key;
  }

  // data layout
  // 0ffffffgggggghhhhhhiiiiiiaaaaabbbbbcccccxxxxxxxxyyyyyyyyzzzzzzzz
  // f,g,h,i are flags that describe arbitrary properties of the positions
  // building the reference triangle and the actually described position.
  // a,b,c describe the edge lengths of the reference triangle and x,y,z 
  // describe the coordinates of the position relative the the reference 
  // triangle.
  // f,g,h,i can represent values in [0,63]
  // a,b,c can represent values in [0,31]
  // x,y,z can represent values in [-127,127]
  // THERE ARE NO CHECKS FOR OVERFLOWS!
  static const uint64_t f_mask = 0x7E00000000000000;
  static const uint64_t g_mask = 0x01F8000000000000;
  static const uint64_t h_mask = 0x0007E00000000000;
  static const uint64_t i_mask = 0x00001F8000000000;
  static const uint64_t a_mask = 0x0000007C00000000;
  static const uint64_t b_mask = 0x00000003E0000000;
  static const uint64_t c_mask = 0x000000001F000000;
  static const uint64_t x_mask = 0x0000000000FF0000;
  static const uint64_t y_mask = 0x000000000000FF00;
  static const uint64_t z_mask = 0x00000000000000FF;

  uint64_t key;
};


struct MotifHasherKeyHasher {
  std::size_t operator()(const MotifHasherKey& k) const {
    static robin_hood::hash<uint64_t> hasher;
    return hasher(k.key);
  }      
};

 
typedef uint32_t MotifHasherValueItem;
  
typedef std::vector<MotifHasherValueItem> MotifHasherValue;
typedef robin_hood::unordered_map<MotifHasherKey, MotifHasherValue, 
                                  MotifHasherKeyHasher> MotifHasherMap;


struct InitialHit{
  InitialHit(int triangle_idx, int target_p1, int target_p2,
             int target_p3, int counts): triangle_idx(triangle_idx),
                                         target_p1(target_p1),
                                         target_p2(target_p2),
                                         target_p3(target_p3),
                                         counts(counts) { }

  int triangle_idx;
  int target_p1;
  int target_p2;
  int target_p3;
  int counts;
};


struct InitialHits{

  InitialHits(const promod3::modelling::MotifQuery& query,
              uint max_hits): max_hits(max_hits) {
    initial_hits.resize(query.GetN());
  }

  void AddHits(const std::vector<int>& query_indices,
               const std::vector<int>& triangle_indices,
               const std::vector<int>& counts,
               int target_p1, int target_p2, int target_p3) {

    for(uint hit_idx = 0; hit_idx < query_indices.size(); ++hit_idx) {

      InitialHit hit(triangle_indices[hit_idx], target_p1, target_p2, target_p3,
                     counts[hit_idx]);
      std::vector<InitialHit>& hits = initial_hits[query_indices[hit_idx]];
      bool added = false;
      for(auto it = hits.begin(); it != hits.end(); ++it) {
        if(hit.counts > it->counts) {
          hits.insert(it, hit);
          added = true;
          break;
        }
      }
      if(!added) {
        hits.push_back(hit);
      }
      while(hits.size() > max_hits) {
        hits.pop_back();
      }
    }
  }

  uint max_hits;
  std::vector<std::vector<InitialHit> > initial_hits;
};


struct Accumulator{

  Accumulator(const promod3::modelling::MotifQuery& query, 
              Real coverage_thresh, int fix_thresh) {

    int n = 0;
    for(uint i = 0; i < query.GetN(); ++i) {
      range_start.push_back(n);
      n += query.GetNTriangles(i);
    }
    accumulator.assign(n, 0);

    if(fix_thresh < 0) {
      // default: thresholds depend on sizes of query
      for(uint i = 0; i < query.GetN(); ++i) {
        uint32_t query_size = query.GetQuerySize(i);
        uint32_t n_triangles = query.GetNTriangles(i);
        uint16_t thresh = std::ceil((query_size-3) * coverage_thresh);
        for(uint j = 0; j < n_triangles; ++j) {
          thresholds.push_back(thresh);
        }
      }
    } else {
      // assign fix thresh for each triangle
      thresholds.assign(n, fix_thresh);
    }
  }


  void Eval() {

    std::vector<int> indices;
    
    #if PM3_ENABLE_SSE && OST_DOUBLE_PRECISION == 0

    uint pos = 0;
    __m128i a,b;
    int mask;
    for(uint sse_loop = 0; sse_loop < accumulator.size() / 8; 
        ++sse_loop, pos += 8) {
      a = _mm_load_si128(reinterpret_cast<const __m128i*>(&accumulator[pos]));
      b = _mm_load_si128(reinterpret_cast<const __m128i*>(&thresholds[pos]));
      // there is no comparison intrinsic dealing with unsigned integers
      // we have to take some detour...
      a = _mm_cmpeq_epi16(_mm_max_epu16(a, b), a);
      // there is no movemask for 16 bit integers, we use the one for 8 bit 
      // instead. Every positive hit will be marked by two neighbouring bits.
      mask = _mm_movemask_epi8(a);
      if(mask != 0) {
        for(int i = 0; i < 8; ++i) {
          if(mask & 1) {
            indices.push_back(pos + i);
          }
          mask = mask>>2;
        }
      }
    }

    // do the leftovers
    for(; pos < accumulator.size(); ++pos) {
      if(accumulator[pos] >= thresholds[pos]) {
        indices.push_back(pos);
      }
    }

    #else

    for(uint i = 0; i < accumulator.size(); ++i) {
      if(accumulator[i] >= thresholds[i]) {
        indices.push_back(i);
      }
    }
    
    #endif

    query_indices.clear();
    triangle_indices.clear();
    counts.clear();
    uint query_idx_plus_one = 1;
    uint n_ranges = range_start.size();
    for(uint i = 0; i < indices.size(); ++i) {
      uint idx = indices[i];
      while(query_idx_plus_one < n_ranges) {
        if(range_start[query_idx_plus_one] > idx) {
          break;
        }
        ++query_idx_plus_one;
      }
      query_indices.push_back(query_idx_plus_one-1);
      triangle_indices.push_back(idx-range_start[query_indices.back()]);
      counts.push_back(accumulator[idx]);
    }
  }

  inline void AddCount(uint idx) {
    ++accumulator[idx];
  }

  inline void SetZero() {
    memset(&accumulator[0], 0, accumulator.size() * sizeof(uint16_t));
  }

  void Process(int p1, int p2, int p3, InitialHits& initial_hits) {
    this->Eval();
    initial_hits.AddHits(query_indices, triangle_indices, counts, p1, p2, p3);
    this->SetZero();
  }

  // stuff that holds actual data for the accumulator
  std::vector<uint32_t> range_start;
  std::vector<uint16_t> accumulator;
  std::vector<uint16_t> thresholds;
  // That's mutable stuff to work on
  std::vector<int> query_indices;
  std::vector<int> triangle_indices;
  std::vector<int> counts;
};


void BaseTransform(const promod3::core::EMat3X& pos, Real bin_size,
                   int p1, int p2, int p3,
                   promod3::core::EMat3X& transformed_pos) {

  // translate to new origin
  transformed_pos = pos.colwise() - pos.col(p1);

  // define new coordinate system
  promod3::core::EMat3 base;
  base.col(0) = transformed_pos.col(p2);
  base.col(2) = base.col(0).cross(transformed_pos.col(p3));
  base.col(1) = base.col(0).cross(base.col(2));
  base.colwise().normalize();

  // apply bin size
  base = bin_size * base;

  // change base
  transformed_pos = base.inverse() * transformed_pos;
}


void SetupEigenMatrices(const geom::Vec3List& positions,
                        promod3::core::EMat3X& eigen_positions,
                        promod3::core::EMatXX& eigen_distances) {

  int n = positions.size();  
  eigen_positions = promod3::core::EMat3X::Zero(3,n);
  for(int i = 0; i < n; ++i) {
    promod3::core::EMatFillCol(eigen_positions, i, positions[i]);
  }

  eigen_distances = promod3::core::EMatXX::Zero(n, n);
  for(int i = 0; i < n; ++i) {
    for(int j = i+1; j < n; ++j) {
      Real d = (eigen_positions.col(i) - eigen_positions.col(j)).norm();
      eigen_distances(i,j) = d;
      eigen_distances(j,i) = d;
    }
  }
}


#if 0 // more elaborate version to get an initial alignment...
void GetInitialAlignment(const promod3::core::EMat3X& query_pos,
                         int query_p1, int query_p2, int query_p3,
                         const promod3::core::EMat3X& target_pos,
                         int target_p1, int target_p2, int target_p3,
                         std::vector<std::pair<int,int> >& alignment) {

  // This function creates a hash map containing the query positions
  // with the found hit-triangle as a base. Normally, one would store the
  // according query and triangle index as a value in the hash map. However,
  // we already know that. We store the index of the according position in
  // the query instead.
  // Having that map constructed, we iterate over the target positions
  // that have also been transformed with the found hit-triangle as a base.
  // By doing that, we can map the matching query and target positions.

  // first add the triangle indices, they are considered as match anyway
  alignment.clear();
  alignment.push_back(std::make_pair(query_p1, target_p1));
  alignment.push_back(std::make_pair(query_p2, target_p2));
  alignment.push_back(std::make_pair(query_p3, target_p3));

  promod3::core::EMat3X transformed_query_pos;
  promod3::core::EMat3X transformed_target_pos;
  BaseTransform(query_pos, query_p1, query_p2, query_p3, 
                transformed_query_pos);
  BaseTransform(target_pos, target_p1, target_p2, target_p3, 
                transformed_target_pos);

  MotifHasherMap map;
  for(int i = 0; i < transformed_query_pos.cols(); ++i) {
    if(i != query_p1 && i != query_p2 && i != query_p3) {
      Real x = std::floor(transformed_query_pos(0,i));
      Real y = std::floor(transformed_query_pos(1,i));
      Real z = std::floor(transformed_query_pos(2,i));
      if(x < 32 && y < 32 && z < 32 && x > -32 && y > -32 && z > -32) {
        MotifHasherKey key;
        // only set X,Y and Z component, leave A, B and C zero, as the two
        // triangles match anyway at this stage
        key.SetX(static_cast<int32_t>(transformed_query_pos(0,i)));
        key.SetY(static_cast<int32_t>(transformed_query_pos(1,i)));
        key.SetZ(static_cast<int32_t>(transformed_query_pos(2,i)));
        MotifHasherMap::iterator it = map.find(key);
        if(it == map.end()) {
          map[key] = MotifHasherValue();
          it = map.find(key);
        }
        it->second.push_back(i);
      }
    }
  }

  for(int i = 0; i < transformed_target_pos.cols(); ++i) {
    if(i != target_p1 && i != target_p2 && i != target_p3) {
      Real x = transformed_target_pos(0,i);
      Real y = transformed_target_pos(1,i);
      Real z = transformed_target_pos(2,i);
      if(x > Real(-31.5) && x < Real(31.5) && y > Real(-31.5) && 
         y < Real(31.5) && z > Real(-31.5) && z < Real(31.5)) {

        int32_t x_min = std::floor(x);
        int32_t y_min = std::floor(y);
        int32_t z_min = std::floor(z);
        if(x - x_min < Real(0.5)) --x_min;
        if(y - y_min < Real(0.5)) --y_min;
        if(z - z_min < Real(0.5)) --z_min;

        MotifHasherKey key;
        for(int32_t j = x_min; j <= x_min+1; ++j) {
          key.SetX(j);
          for(int32_t k = y_min; k <= y_min+1; ++k) {
            key.SetY(k);
            for(int32_t l = z_min; l <= z_min+1; ++l) {
              key.SetZ(l);
              MotifHasherMap::iterator it = map.find(key);
              if(it != map.end()) {
                for(MotifHasherValue::const_iterator v_it = it->second.begin(); 
                    v_it != it->second.end(); ++v_it) {
                  alignment.push_back(std::make_pair(*v_it, i));
                }
              }
            }
          }
        }
      }
    }
  }
}
#endif


bool RefineInitialHit(const promod3::modelling::MotifQuery& query,  
                      const promod3::core::EMat3X& target_pos, 
                      const std::vector<int>& target_flags,
                      Real dist_thresh, Real refine_thresh, bool swap_thresh,
                      int query_idx, int query_triangle_idx, int target_p1,
                      int target_p2, int target_p3, geom::Mat4& mat, 
                      std::vector<std::pair<int, int> >& alignment) {

  // the query is only available as geom::Vec3List. This makes sense from a 
  // usability perspective, but we transfer the thing to an Eigen matrix
  // for processing here...
  int query_n = query.GetQuerySize(query_idx);
  const geom::Vec3List& query_vec3_pos = query.GetPositions(query_idx);
  promod3::core::EMat3X query_pos = promod3::core::EMat3X::Zero(3, query_n);
  for(int i = 0; i < query_n; ++i) {
    promod3::core::EMatFillCol(query_pos, i, query_vec3_pos[i]);
  }

  // we superpose iteratively, so we need to keep track of several 
  // transformations
  std::vector<geom::Mat4> transformations;

  // setup initial alignment to start iterative superposition
  promod3::modelling::Triangle query_triangle = 
  query.GetTriangle(query_idx, query_triangle_idx);
  alignment.clear();
  alignment.push_back(std::make_pair(query_triangle.p1, target_p1));
  alignment.push_back(std::make_pair(query_triangle.p2, target_p2));
  alignment.push_back(std::make_pair(query_triangle.p3, target_p3));

  // first iteration has relaxed distance threshold, as the initial 
  // superposition might be less accurate (only three positions from triangle)
  Real squared_dist_thresh = dist_thresh * dist_thresh * 4;

  for(int iteration = 0; iteration < 5; ++iteration) {

    if(alignment.size() < 3) {
      return false;
    }

    promod3::core::EMatX3 m1 = promod3::core::EMatX3::Zero(alignment.size(),3); 
    promod3::core::EMatX3 m2 = promod3::core::EMatX3::Zero(alignment.size(),3); 

    for(uint i = 0; i < alignment.size(); ++i) {
      m1.row(i) = query_pos.col(alignment[i].first).transpose();
      m2.row(i) = target_pos.col(alignment[i].second).transpose();
    }

    geom::Mat4 t = promod3::core::MinRMSDSuperposition(m1, m2);

    // apply the transform to the query positions
    promod3::core::EMat3 rot;
    rot(0,0) = t(0,0); rot(0,1) = t(0,1); rot(0,2) = t(0,2);
    rot(1,0) = t(1,0); rot(1,1) = t(1,1); rot(1,2) = t(1,2);
    rot(2,0) = t(2,0); rot(2,1) = t(2,1); rot(2,2) = t(2,2);
  
    promod3::core::EVec3 trans;
    trans(0,0) = t(0,3);
    trans(1,0) = t(1,3);
    trans(2,0) = t(2,3);

    query_pos = rot * query_pos;
    query_pos = query_pos.colwise() + trans;

    std::vector<std::pair<int,int> > new_alignment;

    for(int i = 0; i < query_pos.cols(); ++i) {
      promod3::core::ERVecX squared_distances = 
      (target_pos.colwise() - query_pos.col(i)).colwise().squaredNorm();

      // in case of empty flags, we use the in built eigen function to
      // find the minimum. We have to do the full iteration and check for 
      // matching flags otherwise
      if(target_flags.empty()) {
        Real min = squared_distances.minCoeff();
        if(min <= squared_dist_thresh) {
          int min_idx = -1;
          for(int j = 0; j < squared_distances.cols(); ++j) {
            if(squared_distances(0,j) == min) {
              min_idx = j;
              break;
            }
          }
          new_alignment.push_back(std::make_pair(i,min_idx));
        }
      } else {
        const std::vector<uint8_t>& query_flags = query.GetFlags(query_idx);
        int query_flag = query_flags[i];
        Real min = std::numeric_limits<Real>::max();
        int min_idx = -1;
        for(uint j = 0; j < squared_distances.cols(); ++j) {
          if(squared_distances(0,j) < min && query_flag == target_flags[j]) {
            min = squared_distances(0,j);
            min_idx = j;
          }
        }
        if(min <= squared_dist_thresh) {
          new_alignment.push_back(std::make_pair(i,min_idx));
        }
      }
    }

    transformations.push_back(t);
    if(new_alignment == alignment) {
      break;
    } 
    alignment = new_alignment;

    if(iteration == 0) {
      squared_dist_thresh = dist_thresh * dist_thresh;
    }
  }

  // check whether enough positions are superposed
  if(swap_thresh) {
    if(static_cast<Real>(alignment.size()) / target_pos.cols() < refine_thresh) {
      return false;
    }
  } else {
    if(static_cast<Real>(alignment.size()) / query_n < refine_thresh) {
      return false;
    }
  }

  // chain together the final transformation matrix
  int t_idx = transformations.size() - 1;
  mat = transformations[t_idx];
  while(t_idx > 0) {
    mat = mat * transformations[--t_idx];
  }

  return true;
}


void RefineInitialHits(const InitialHits& initial_hits,
                       const promod3::modelling::MotifQuery& query,
                       const promod3::core::EMat3X& target_pos, 
                       const std::vector<int>& flags, Real dist_thresh,
                       Real refine_thresh, bool swap_thresh,
                       std::vector<promod3::modelling::MotifMatch>& results) {


  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "FindMotifs::RefineInitialHits", 2);

  geom::Mat4 mat;
  std::vector<std::pair<int,int> > aln;

  for(uint query_idx = 0; query_idx < query.GetN(); ++query_idx) {
    std::vector<promod3::modelling::MotifMatch> query_results;

    for(auto it = initial_hits.initial_hits[query_idx].begin();
        it != initial_hits.initial_hits[query_idx].end(); ++it) {
      if(RefineInitialHit(query, target_pos, flags, dist_thresh, 
                          refine_thresh, swap_thresh, query_idx,
                          it->triangle_idx,  it->target_p1, it->target_p2,
                          it->target_p3, mat, aln)) {
        // only add the result if its unique
        bool already_there = false;
        for(uint res_idx = 0; res_idx < query_results.size(); ++res_idx) {
          // we do not check for the transformation matrix
          // if the alignment is the same, the matrix will also be the same
          if(query_results[res_idx].aln == aln) {
            already_there = true;
            break;
          }
        }
        if(!already_there) {
          query_results.push_back(promod3::modelling::MotifMatch(query_idx,
                                                                 it->triangle_idx,
                                                                 mat, aln));
        }
      }
    }
    results.insert(results.end(), query_results.begin(), query_results.end());
  }
}


template<typename T>
void WriteVec(std::ofstream& stream, const std::vector<T>& vec) {
  uint32_t size = vec.size();
  stream.write(reinterpret_cast<char*>(&size), sizeof(uint32_t));
  if(size > 0) {
    stream.write(reinterpret_cast<const char*>(&vec[0]), size*sizeof(T));
  }
}


template<typename T>
void ReadVec(std::ifstream& stream, std::vector<T>& vec) {
  uint32_t size;
  stream.read(reinterpret_cast<char*>(&size), sizeof(uint32_t));
  if(size > 0) {
    vec.resize(size);
    stream.read(reinterpret_cast<char*>(&vec[0]), size*sizeof(T));
  }
}


void Accumulate(const promod3::core::EMat3X& transformed_pos,
                Real a, Real b, Real c, int p1, int p2, int p3,
                Real min_pos_bound, Real max_pos_bound,
                const std::vector<int>& flags,
                const MotifHasherMap& map, Accumulator& acc) {

  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "FindMotifs::Accumulate", 2);

  int32_t x_min, y_min, z_min;
  uint32_t a_min = static_cast<uint32_t>(a);
  uint32_t b_min = static_cast<uint32_t>(b);
  uint32_t c_min = static_cast<uint32_t>(c);
  if(a - a_min < Real(0.5)) --a_min;
  if(b - b_min < Real(0.5)) --b_min;
  if(c - c_min < Real(0.5)) --c_min;

  MotifHasherKey key;
  if(!flags.empty()) {
    key.SetF(flags[p1]);
    key.SetG(flags[p2]);
    key.SetH(flags[p3]);
  }

  Real x,y,z;
  for(int p_idx = 0; p_idx < transformed_pos.cols(); ++p_idx) {
    if(p_idx != p1 && p_idx != p2 && p_idx != p3) {
      x = transformed_pos(0, p_idx);
      y = transformed_pos(1, p_idx);
      z = transformed_pos(2, p_idx);
      if(x > min_pos_bound && x < max_pos_bound && y > min_pos_bound && 
         y < max_pos_bound && z > min_pos_bound && z < max_pos_bound) {
        x_min = std::floor(x);
        y_min = std::floor(y);
        z_min = std::floor(z);
        if(x - x_min < Real(0.5)) --x_min;
        if(y - y_min < Real(0.5)) --y_min;
        if(z - z_min < Real(0.5)) --z_min;
        if(!flags.empty()) {
          key.SetI(flags[p_idx]);
        }
        for(uint32_t i = a_min; i <= a_min+1; ++i) {
          key.SetA(i);
          for(uint32_t j = b_min; j <= b_min+1; ++j) {
            key.SetB(j);
            for(uint32_t k = c_min; k <= c_min+1; ++k) {
              key.SetC(k);
              for(int32_t l = x_min; l <= x_min+1; ++l) {
                key.SetX(l);
                for(int32_t m = y_min; m <= y_min+1; ++m) {
                  key.SetY(m);
                  for(int32_t n = z_min; n <= z_min+1; ++n) {
                    key.SetZ(n);
                    MotifHasherMap::const_iterator it = map.find(key);
                    if(it != map.end()) {
                      for(auto v_it = it->second.begin(); 
                          v_it != it->second.end(); ++v_it) {
                        acc.AddCount(*v_it);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}


void SortTriangleEdges(Real a, Real b, Real c, int p1, int p2, int p3,
                       Real& sorted_a, Real& sorted_b, Real& sorted_c,
                       int& sorted_p1, int& sorted_p2, int& sorted_p3) {

  // reassigns a,b,c,p1,p2,p3 so that sorted_a<=sorted_b<=sorted_c. 
  if(a <= b && b <= c) {
    sorted_a = a;
    sorted_b = b;
    sorted_c = c;
    sorted_p1 = p1;
    sorted_p2 = p2;
    sorted_p3 = p3;
  } else if(a <= c && c <= b) {
    sorted_a = a;
    sorted_b = c;
    sorted_c = b;
    sorted_p1 = p2;
    sorted_p2 = p1;
    sorted_p3 = p3;
  } else if(b <= a && a <= c) {
    sorted_a = b;
    sorted_b = a;
    sorted_c = c;
    sorted_p1 = p3;
    sorted_p2 = p2;
    sorted_p3 = p1;
  } else if(b <= c && c <= a) {
    sorted_a = b;
    sorted_b = c;
    sorted_c = a;
    sorted_p1 = p2;
    sorted_p2 = p3;
    sorted_p3 = p1;
  } else if(c <= a && a <= b) {
    sorted_a = c;
    sorted_b = a;
    sorted_c = b;
    sorted_p1 = p3;
    sorted_p2 = p1;
    sorted_p3 = p2;
  } else { //(c <= b && b <= a) 
    sorted_a = c;
    sorted_b = b;
    sorted_c = a;
    sorted_p1 = p1;
    sorted_p2 = p3;
    sorted_p3 = p2;
  }
}

} // anon ns


namespace promod3 { namespace modelling {


struct MotifQueryData {
  MotifHasherMap map;
  std::vector<geom::Vec3List> positions;
  std::vector<String> identifiers;
  std::vector<std::vector<Triangle> > triangles;
  std::vector<std::vector<uint8_t> > flags;
  Real min_triangle_edge_length;
  Real max_triangle_edge_length;
  Real min_pos_bound;
  Real max_pos_bound;
  Real bin_size;
};


MotifQuery::MotifQuery() { }


MotifQuery::MotifQuery(const geom::Vec3List& positions, 
                       const String& identifier,
                       Real min_triangle_edge_length,
                       Real max_triangle_edge_length,
                       Real bin_size,
                       const std::vector<int>& flags) {

  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "MotifQuery::MotifQuery", 2);

  if(positions.size() < 4) {
    throw promod3::Error("Require at least 4 positions to construct "
                         "MotifQuery");
  }

  if(min_triangle_edge_length <= 0.0 || max_triangle_edge_length <= 0.0) {
    throw promod3::Error("min/max triangle edge lengths must be positive!");
  }

  if(min_triangle_edge_length >= max_triangle_edge_length) {
    throw promod3::Error("min_triangle_edge_length must be < "
                         "max_triangle_edge_length");
  }

  if(bin_size <= 0.0) {
    throw promod3::Error("Bin size must be positive!");
  }

  // maximum triangle bin would be 31 but we allow one less...
  // this avoids some range checks when Accumulating in FindMotifs
  int max_triangle_bin = static_cast<uint32_t>(max_triangle_edge_length / bin_size);
  if(max_triangle_bin > 30) {
    throw promod3::Error("Cannot represent triangle in internal data structure. "
                         "Increase bin_size or decrease max_edge_length");
  }

  if(!flags.empty()) {
    if(flags.size() != positions.size()) {
      throw promod3::Error("Number of flags must be consistent with positions!");
    }
    for(auto it = flags.begin(); it != flags.end(); ++it) {
      if(*it < 0 || *it > 63) {
        throw promod3::Error("All flags must be in range [0, 63]!");
      }
    }
  }

  data_ = std::shared_ptr<MotifQueryData>(new MotifQueryData);
  data_->positions.push_back(positions);
  data_->identifiers.push_back(identifier);
  data_->triangles.push_back(std::vector<Triangle>());
  data_->min_triangle_edge_length = min_triangle_edge_length;
  data_->max_triangle_edge_length = max_triangle_edge_length;
  data_->bin_size = bin_size;

  if(!flags.empty()) {
    data_->flags.push_back(std::vector<uint8_t>(flags.begin(), flags.end()));
  } else {
    data_->flags.push_back(std::vector<uint8_t>(positions.size(), 0));
  }

  promod3::core::EMat3X eigen_positions;
  promod3::core::EMatXX eigen_distances;
  SetupEigenMatrices(positions, eigen_positions, eigen_distances);

  MotifHasherKey key;
  promod3::core::EMat3X transformed_pos;
  Real a, b, c,sorted_a, sorted_b, sorted_c, diff_ab, diff_bc;
  int sorted_p1, sorted_p2, sorted_p3;
  int32_t d,e,f;
  int n = eigen_positions.cols();
  uint32_t triangle_idx = 0;
  Real min_pos_bound = std::numeric_limits<Real>::max();
  Real max_pos_bound = -std::numeric_limits<Real>::max();

  for(int p1 = 0; p1 < n; ++p1) {
    for(int p2 = p1+1; p2 < n; ++p2) {
      a = eigen_distances(p1,p2);
      if(a > max_triangle_edge_length || a < min_triangle_edge_length) {
        continue; 
      }
      for(int p3 = p2+1; p3 < n; ++p3) {
        b = eigen_distances(p2,p3);
        c = eigen_distances(p1,p3);
        if(b > max_triangle_edge_length || c > max_triangle_edge_length ||
           b < min_triangle_edge_length || c < min_triangle_edge_length) {
          continue; 
        }

        // we define a<=b<=c. a connects p1 and p2, c connects p1 and p3. 
        // This is not necessarily the case for the current values of 
        // a,b,c,p1,p2,p3, so let's do some shuffling.
        SortTriangleEdges(a, b, c, p1, p2, p3, sorted_a, sorted_b, sorted_c,
                          sorted_p1, sorted_p2, sorted_p3);

        // normalize the sorted edge_lengths
        sorted_a /= bin_size;
        sorted_b /= bin_size;
        sorted_c /= bin_size;

        diff_ab = sorted_b - sorted_a;
        diff_bc = sorted_c - sorted_b;
        if(diff_ab < Real(1.0) && diff_bc < (1.0)) {
          // we sample the close neighbours in the hash map when performing 
          // a search. As soon as edges of the triangle are similar, we get
          // ambiguities with the base. If this if-statement is true, those
          // ambiguities become really bad. So let's just ignore this triangle
          continue;
        }

        // transform all positions into the vector basis defined by the 
        // triangle with edges at p1, p2, p3 and add the stuff to the hash 
        // map
        BaseTransform(eigen_positions, bin_size,
                      sorted_p1, sorted_p2, sorted_p3,
                      transformed_pos);
        key.SetA(static_cast<uint32_t>(sorted_a));
        key.SetB(static_cast<uint32_t>(sorted_b));
        key.SetC(static_cast<uint32_t>(sorted_c));
        key.SetF(static_cast<uint32_t>(data_->flags.back()[sorted_p1]));
        key.SetG(static_cast<uint32_t>(data_->flags.back()[sorted_p2]));
        key.SetH(static_cast<uint32_t>(data_->flags.back()[sorted_p3]));
        for(int i = 0; i < n; ++i) {
          if(i != sorted_p1 && i != sorted_p2 && i != sorted_p3) {
            d = std::floor(transformed_pos(0,i));
            e = std::floor(transformed_pos(1,i));
            f = std::floor(transformed_pos(2,i));

            // allowed range would be [-127, 127] but we decrease that range to
            // [-126, 126] to avoid some checks when Accumulating in FindMotifs
            if(d < 126 && e < 126 && f < 126 &&
               d > -126 && e > -126 && f > -126) {
              min_pos_bound = std::min(transformed_pos(0,i), min_pos_bound);
              min_pos_bound = std::min(transformed_pos(1,i), min_pos_bound);
              min_pos_bound = std::min(transformed_pos(2,i), min_pos_bound);
              max_pos_bound = std::max(transformed_pos(0,i), max_pos_bound);
              max_pos_bound = std::max(transformed_pos(1,i), max_pos_bound);
              max_pos_bound = std::max(transformed_pos(2,i), max_pos_bound);
              key.SetX(d);
              key.SetY(e);
              key.SetZ(f);
              key.SetI(static_cast<uint32_t>(data_->flags.back()[i]));
              MotifHasherMap::iterator it = data_->map.find(key);
              if(it == data_->map.end()) {
                data_->map[key] = MotifHasherValue();
                it = data_->map.find(key);
              }
              it->second.push_back(triangle_idx);
            }
          }
        }

        data_->triangles.back().push_back(Triangle(sorted_p1, sorted_p2, 
                                                   sorted_p3));
        ++triangle_idx;
      }
    }
  }

  data_->min_pos_bound = min_pos_bound;
  data_->max_pos_bound = max_pos_bound;
}


MotifQuery::MotifQuery(const std::vector<MotifQuery>& queries) {

  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "MotifQuery::MotifQuery", 2);

  if(queries.empty()) {
    throw promod3::Error("Cannot initialize MotifQuery from empty list");
  }


  data_ = std::shared_ptr<MotifQueryData>(new MotifQueryData);
  data_->min_triangle_edge_length = std::numeric_limits<Real>::max();
  data_->max_triangle_edge_length = -std::numeric_limits<Real>::max();
  data_->min_pos_bound = std::numeric_limits<Real>::max();
  data_->max_pos_bound = -std::numeric_limits<Real>::max();
  data_->bin_size = queries[0].GetBinSize();

  MotifHasherMap& map = data_->map;

  // iterate over all queries and update data_ along the way
  uint32_t total_num_triangles = 0;
  for(auto query_it = queries.begin(); query_it != queries.end(); ++query_it) {

    if(std::numeric_limits<uint32_t>::max() - query_it->GetNTriangles() <=
       total_num_triangles) {
      std::stringstream ss;
      ss << "Query can maximally contain ";
      ss << std::numeric_limits<uint32_t>::max() << " triangles. ";
      ss << "Reduce number of queries or size of single queries.";
      throw promod3::Error(ss.str());
    }

    if(query_it->GetBinSize() != data_->bin_size) {
      throw promod3::Error("All queries must have consistent bin sizes!");
    }

    // update the map in data_
    for(auto other_map_it = query_it->data_->map.begin(); 
        other_map_it != query_it->data_->map.end(); ++other_map_it) {
      MotifHasherMap::iterator map_it =  map.find(other_map_it->first);
      if(map_it == map.end()) {
        map[other_map_it->first] = MotifHasherValue();
        map_it = map.find(other_map_it->first);
      }
      for(auto item_it = other_map_it->second.begin(); 
          item_it != other_map_it->second.end(); ++item_it) {
        map_it->second.push_back((*item_it) + total_num_triangles);
      }
    }

    // update everything else in data_
    data_->positions.insert(data_->positions.end(), 
                            query_it->data_->positions.begin(),
                            query_it->data_->positions.end());
    data_->identifiers.insert(data_->identifiers.end(), 
                              query_it->data_->identifiers.begin(),
                              query_it->data_->identifiers.end());
    data_->triangles.insert(data_->triangles.end(),
                            query_it->data_->triangles.begin(),
                            query_it->data_->triangles.end()); 
    data_->flags.insert(data_->flags.end(),
                        query_it->data_->flags.begin(),
                        query_it->data_->flags.end());
    data_->min_triangle_edge_length = std::min(data_->min_triangle_edge_length,
                                    query_it->data_->min_triangle_edge_length);
    data_->max_triangle_edge_length = std::max(data_->max_triangle_edge_length,
                                    query_it->data_->max_triangle_edge_length);
    data_->min_pos_bound = std::min(data_->min_pos_bound,
                                    query_it->data_->min_pos_bound);
    data_->max_pos_bound = std::max(data_->max_pos_bound,
                                    query_it->data_->max_pos_bound);
    total_num_triangles += query_it->GetNTriangles();
  }
}


MotifQuery MotifQuery::Load(const String& filename) {
  
  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "MotifQuery::Load", 2);

  std::ifstream in_stream_(filename.c_str(), std::ios::binary);
  if (!in_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' does not exist.";
    throw promod3::Error(ss.str());
  } 

  core::PortableBinaryDataSource in_stream(in_stream_);
  core::CheckMagicNumber(in_stream);
  uint32_t version = core::GetVersionNumber(in_stream_);
  if(version != 1) {
    std::stringstream ss;
    ss << "Unsupported file version '" << version << "' in '" << filename;
    throw promod3::Error(ss.str());
  }
  core::CheckTypeSize<uint64_t>(in_stream);
  core::CheckTypeSize<uint32_t>(in_stream);
  core::CheckTypeSize<uint16_t>(in_stream);
  core::CheckTypeSize<uint8_t>(in_stream);
  core::CheckTypeSize<float>(in_stream);

  MotifQuery query;
  query.data_ = std::shared_ptr<MotifQueryData>(new MotifQueryData);
  std::vector<uint32_t> query_sizes;
  std::vector<float> position_vec;
  std::vector<uint64_t> keys;
  std::vector<uint32_t> n_items;
  std::vector<uint32_t> items;
  std::vector<uint32_t> n_triangles;
  std::vector<uint16_t> triangle_vec;
  std::vector<uint8_t> flag_vec;

  in_stream & query.data_->identifiers;
  ReadVec(in_stream.Stream(), query_sizes);
  ReadVec(in_stream.Stream(), position_vec);
  ReadVec(in_stream.Stream(), keys);
  ReadVec(in_stream.Stream(), n_items);
  ReadVec(in_stream.Stream(), items);
  ReadVec(in_stream.Stream(), n_triangles);
  ReadVec(in_stream.Stream(), triangle_vec);
  ReadVec(in_stream.Stream(), flag_vec);
  in_stream & query.data_->min_triangle_edge_length;
  in_stream & query.data_->max_triangle_edge_length;  
  in_stream & query.data_->min_pos_bound;
  in_stream & query.data_->max_pos_bound;
  in_stream & query.data_->bin_size;

  int position_idx = 0;
  int triangle_idx = 0;
  int flag_idx = 0;
  for(uint i = 0; i < query.data_->identifiers.size(); ++i) {
    geom::Vec3List positions;
    for(uint j = 0; j < query_sizes[i]; ++j) {
      positions.push_back(geom::Vec3(position_vec[position_idx],
                                     position_vec[position_idx+1],
                                     position_vec[position_idx+2]));
      position_idx += 3;
    }
    query.data_->positions.push_back(positions);

    std::vector<Triangle> triangles;
    for(uint j = 0; j < n_triangles[i]; ++j) {
      triangles.push_back(Triangle(triangle_vec[triangle_idx], 
                                   triangle_vec[triangle_idx+1], 
                                   triangle_vec[triangle_idx+2]));
      triangle_idx += 3;
    } 
    query.data_->triangles.push_back(triangles);

    std::vector<uint8_t> flags;
    for(uint j = 0; j < query_sizes[i]; ++j) {
      flags.push_back(flag_vec[flag_idx++]);
    } 
    query.data_->flags.push_back(flags);
  }

  int item_idx = 0;
  for(uint i = 0; i < keys.size(); ++i) {
    MotifHasherKey key;
    key.key = keys[i];
    MotifHasherValue value;
    for(uint j = 0; j < n_items[i]; ++j) {
      value.push_back(items[item_idx++]);
    }
    query.data_->map[key] = value;
  }

  return query;
}


void MotifQuery::Save(const String& filename) const {

  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "MotifQuery::Save", 2);

  std::ofstream out_stream_(filename.c_str(), std::ios::binary);
  if (!out_stream_) {
    std::stringstream ss;
    ss << "The file '" << filename << "' cannot be opened.";
    throw promod3::Error(ss.str());
  }

  std::vector<uint32_t> query_sizes;
  std::vector<float> position_vec;
  std::vector<uint64_t> keys;
  std::vector<uint32_t> n_items;
  std::vector<uint32_t> items;
  std::vector<uint32_t> n_triangles;
  std::vector<uint16_t> triangle_vec;
  std::vector<uint8_t> flag_vec;

  for(uint i = 0; i < data_->positions.size(); ++i) {
    int n = data_->positions[i].size();
    query_sizes.push_back(n);
    for(int j = 0; j < n; ++j) {
      position_vec.push_back(data_->positions[i][j][0]);
      position_vec.push_back(data_->positions[i][j][1]);
      position_vec.push_back(data_->positions[i][j][2]);
    }
    for(int j = 0; j < n; ++j) {
      flag_vec.push_back(data_->flags[i][j]);
    }
    n_triangles.push_back(data_->triangles[i].size());
    for(uint j = 0; j < n_triangles.back(); ++j) {
      triangle_vec.push_back(data_->triangles[i][j].p1);
      triangle_vec.push_back(data_->triangles[i][j].p2);
      triangle_vec.push_back(data_->triangles[i][j].p3);
    }
  }

  for(MotifHasherMap::const_iterator it = data_->map.begin();
      it != data_->map.end(); ++it) {
    keys.push_back(it->first.key);
    n_items.push_back(it->second.size());
    for(uint i = 0; i < n_items.back(); ++i) {
      items.push_back(it->second[i]);
    }
  }

  core::PortableBinaryDataSink out_stream(out_stream_);
  core::WriteMagicNumber(out_stream);
  core::WriteVersionNumber(out_stream, 1);
  core::WriteTypeSize<uint64_t>(out_stream);
  core::WriteTypeSize<uint32_t>(out_stream);
  core::WriteTypeSize<uint16_t>(out_stream);
  core::WriteTypeSize<uint8_t>(out_stream);
  core::WriteTypeSize<float>(out_stream);

  out_stream & data_->identifiers;
  WriteVec(out_stream.Stream(), query_sizes);
  WriteVec(out_stream.Stream(), position_vec);
  WriteVec(out_stream.Stream(), keys);
  WriteVec(out_stream.Stream(), n_items);
  WriteVec(out_stream.Stream(), items);
  WriteVec(out_stream.Stream(), n_triangles);
  WriteVec(out_stream.Stream(), triangle_vec);
  WriteVec(out_stream.Stream(), flag_vec);
  out_stream & data_->min_triangle_edge_length;
  out_stream & data_->max_triangle_edge_length;
  out_stream & data_->min_pos_bound;
  out_stream & data_->max_pos_bound;
  out_stream & data_->bin_size;
}


const geom::Vec3List& MotifQuery::GetPositions(uint idx) const{
  return data_->positions[idx];
}


const std::vector<uint8_t>& MotifQuery::GetFlags(uint idx) const{
  return data_->flags[idx];
}


const std::vector<String>& MotifQuery::GetIdentifiers() const{
  return data_->identifiers;
}


size_t MotifQuery::GetN() const {
  return data_->identifiers.size(); 
} 


size_t MotifQuery::GetQuerySize(uint idx) const {
  return data_->positions[idx].size();
}


size_t MotifQuery::GetNTriangles(uint idx) const {
  return data_->triangles[idx].size();
}


size_t MotifQuery::GetNTriangles() const {
  size_t s = 0;
  for(auto it = data_->triangles.begin(); it != data_->triangles.end(); ++it) {
    s += it->size();
  }
  return s;
}


Real MotifQuery::GetMinTriangleEdgeLength() const {
  return data_->min_triangle_edge_length;
}


Real MotifQuery::GetMaxTriangleEdgeLength() const {
  return data_->max_triangle_edge_length;
}


Real MotifQuery::GetMinPosBound() const {
  return data_->min_pos_bound;
}


Real MotifQuery::GetMaxPosBound() const {
  return data_->max_pos_bound;
}


Real MotifQuery::GetBinSize() const {
  return data_->bin_size;
}


Triangle MotifQuery::GetTriangle(uint query_idx, uint triangle_idx) const {
  return data_->triangles[query_idx][triangle_idx];
}


void MotifQuery::PrintBinSizes() const {

  std::vector<size_t> bin_sizes;
  size_t max_size = 0;

  for(auto it = data_->map.begin(); it!=data_->map.end(); ++it) {
    bin_sizes.push_back(it->second.size());
    max_size = std::max(bin_sizes.back(), max_size);
  }

  std::vector<int> histogram(max_size+1, 0);
  for(auto it = bin_sizes.begin(); it != bin_sizes.end(); ++it) {
    histogram[*it]+=1;
  }

  for(uint idx = 0; idx < histogram.size(); ++idx) {
    std::cout<<idx<<' '<<histogram[idx]<<std::endl;
  }
}


void MotifQuery::Prune(Real factor) {

  std::vector<int> counts_per_triangle(this->GetNTriangles(), 0);
  std::vector<int> min_counts_per_triangle;
  for(uint i = 0; i < this->GetN(); ++i) {
    int thresh = std::ceil((this->GetQuerySize(i)-3) * factor);
    for(uint j = 0; j < this->GetNTriangles(i); ++j) {
      min_counts_per_triangle.push_back(thresh);
    }
  }

  MotifHasherMap new_map;
  const MotifHasherMap& map = data_->map;

  std::set<size_t> bin_size_set;
  for(auto it = map.begin(); it!=map.end(); ++it) {
    bin_size_set.insert(it->second.size());
  }
  std::vector<size_t> bin_sizes(bin_size_set.begin(), bin_size_set.end()); 
  std::sort(bin_sizes.begin(), bin_sizes.end());

  for(auto bin_size = bin_sizes.begin(); bin_size != bin_sizes.end(); ++bin_size) {
    for(auto map_it = map.begin(); map_it != map.end(); ++map_it) {
      if(map_it->second.size() == *bin_size) {
        const MotifHasherValue& triangle_indices = map_it->second;
        MotifHasherValue new_triangle_indices;
        for(auto triangle = triangle_indices.begin(); triangle != triangle_indices.end(); ++triangle) {
          if(counts_per_triangle[*triangle] <= min_counts_per_triangle[*triangle]) {
            ++counts_per_triangle[*triangle];
            new_triangle_indices.push_back(*triangle);
          }
        }
        if(!new_triangle_indices.empty()) {
          new_map[map_it->first] = new_triangle_indices;
        }
      }
    }
  }

  data_->map = new_map;
}


std::vector<MotifMatch> FindMotifs(const MotifQuery& query, 
                                   const geom::Vec3List& positions,
                                   Real hash_thresh, 
                                   Real distance_thresh,
                                   Real refine_thresh,
                                   const std::vector<int>& flags,
                                   bool swap_thresh) {

  promod3::core::ScopedTimerPtr prof = promod3::core::StaticRuntimeProfiler::StartScoped(
                                "FindMotifs::FindMotifs", 2);

  // initial and final hits
  InitialHits initial_hits(query, 50);
  std::vector<MotifMatch> results;

  // general setup
  promod3::core::EMat3X eigen_positions;
  promod3::core::EMatXX eigen_distances;
  promod3::core::EMat3X transformed_pos;
  Real a,b,c,sorted_a,sorted_b,sorted_c, diff_ab, diff_bc;  
  int sorted_p1, sorted_p2, sorted_p3;
  SetupEigenMatrices(positions, eigen_positions, eigen_distances);
  int n_target = eigen_positions.cols();
  Real bin_size = query.GetBinSize();
  Real half_bin_size = bin_size / 2;

  // check whether any flags for the single positions are set
  // if yes, check their validity
  if(!flags.empty()) {
    if(flags.size() != positions.size()) {
      throw promod3::Error("Number of flags must be consistent with positions!");
    }
    for(auto it = flags.begin(); it != flags.end(); ++it) {
      if(*it < 0 || *it > 63) {
        throw promod3::Error("All flags must be in range [0, 63]!");
      }
    }
  }

  // add/subtract constant to allow for some variation
  // min/max edge lengths are absolute distances
  Real min_edge_length = query.GetMinTriangleEdgeLength() - half_bin_size;
  Real max_edge_length = query.GetMaxTriangleEdgeLength() + half_bin_size;
  // pos bounds are already relative to bins, therefore +-0.5
  Real min_pos_bound = query.GetMinPosBound() - 0.5;
  Real max_pos_bound = query.GetMaxPosBound() + 0.5;

  // fetch hash map and setup accumulator
  const MotifHasherMap& map = query.data_->map;
  int acc_thresh = -1;
  if(swap_thresh) {
    acc_thresh = std::ceil((n_target-3) * hash_thresh);
  }
  Accumulator accumulator(query, hash_thresh, acc_thresh);
    
  for(int p1 = 0; p1 < n_target; ++p1) {
    for(int p2 = p1+1; p2 < n_target; ++p2) {
      a = eigen_distances(p1,p2);
      if(a > max_edge_length || a < min_edge_length) {
        continue;
      }
      for(int p3 = p2+1; p3 < n_target; ++p3) {
        b = eigen_distances(p2,p3);
        c = eigen_distances(p1,p3);
        if(b > max_edge_length || c > max_edge_length || 
           b < min_edge_length || c < min_edge_length) {
          continue; 
        }

        // we define a<=b<=c. a connects p1 and p2, c connects p1 and p3. 
        // This is not necessarily the case for the current values of 
        // a,b,c,p1,p2,p3, so let's do some shuffling.
        SortTriangleEdges(a, b, c, p1, p2, p3, sorted_a, sorted_b, sorted_c,
                          sorted_p1, sorted_p2, sorted_p3);

        // normalize the sorted edge_lengths
        sorted_a /= bin_size;
        sorted_b /= bin_size;
        sorted_c /= bin_size;

        diff_ab = sorted_b - sorted_a;
        diff_bc = sorted_c - sorted_b;

        if(diff_ab <= 1.0 && diff_bc <= 1.0) {
          // Small changes in any of the edge lengths might lead to a flip in 
          // the basis. In total we have 6 possible bases...
          //
          // However: when constructing the query I introduced a hack that disallows 
          // triangles where all three edges are very similar. So we can skip...
          // the case for only two edges being similar is treated below.
          continue;
        }

        BaseTransform(eigen_positions, bin_size,
                      sorted_p1, sorted_p2, sorted_p3,
                      transformed_pos);
        Accumulate(transformed_pos, sorted_a, sorted_b, sorted_c,
                   p1, p2, p3, min_pos_bound, max_pos_bound,
                   flags, map, accumulator);

        if(diff_ab > bin_size && diff_bc < bin_size) {
          // The edges b and c have similar length. We thus evaluate a second 
          // basis that represents b>c
          BaseTransform(eigen_positions, bin_size,
                        sorted_p2, sorted_p1, sorted_p3,
                        transformed_pos);
          Accumulate(transformed_pos, sorted_a, sorted_b, sorted_c, p1, p2, p3, 
                     min_pos_bound, max_pos_bound, flags, map, accumulator);
        }

        if(diff_bc > bin_size && diff_ab < bin_size) {
          // The edges a and b have similar length. We thus evaluate a second
          // basis that represents a>b
          BaseTransform(eigen_positions, bin_size,
                        sorted_p3, sorted_p2, sorted_p1,
                        transformed_pos);
          Accumulate(transformed_pos, sorted_a, sorted_b, sorted_c, p1, p2, p3, 
                     min_pos_bound, max_pos_bound, flags, map, accumulator);
        } 

        // Iterate the accumulator and transfer initial solutions that fulfill
        // the hashing thresholds. Once done, the accumulator is cleared and 
        // ready for another round of counting
        accumulator.Process(sorted_p1, sorted_p2, sorted_p3, initial_hits);
      }
    }
  }

  RefineInitialHits(initial_hits, query, eigen_positions, flags, 
                    distance_thresh, refine_thresh, swap_thresh, results);

  return results;
}

}} // ns
