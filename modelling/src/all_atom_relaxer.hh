// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_MODELLING_ALL_ATOM_RELAXER_HH
#define PROMOD3_MODELLING_ALL_ATOM_RELAXER_HH

#include <promod3/loop/mm_system_creator.hh>
#include <promod3/modelling/sidechain_reconstructor.hh>

namespace promod3 { namespace modelling {

class AllAtomRelaxer;
typedef boost::shared_ptr<AllAtomRelaxer> AllAtomRelaxerPtr;

/// \brief Relax loops returned by SidechainReconstructor
class AllAtomRelaxer {
  // for convenience
  typedef SidechainReconstructionData ScRecData;
  typedef SidechainReconstructionDataPtr ScRecDataPtr;
public:
  // create relaxer for this data set (will be ready for Run)
  AllAtomRelaxer(ScRecDataPtr sc_data, loop::MmSystemCreatorPtr mm_sys_creator);

  // update positions
  void UpdatePositions(ScRecDataPtr sc_data);

  // relax (updates loop atom positions in sc_data, returns pot. energy from mm)
  Real Run(ScRecDataPtr sc_data, int steps = 100, Real stop_criterion = 0.01);

  // getters
  ScRecDataPtr GetScData() const { return sc_data_; }
  bool IsFixSurroundingHydrogens() const { return fix_surrounding_hydrogens_; }
  bool IsKillElectrostatics() const { return kill_electrostatics_; }
  Real GetNonbondedCutoff() const { return nonbonded_cutoff_; }
  loop::MmSystemCreatorPtr GetSystemCreator() const {
    return mm_system_creator_;
  }

private:

  // SETTINGS
  ScRecDataPtr sc_data_;
  bool fix_surrounding_hydrogens_;
  bool kill_electrostatics_;
  Real nonbonded_cutoff_;

  // SIMULATION DATA
  loop::AllAtomPositionsPtr all_pos_;
  loop::MmSystemCreatorPtr mm_system_creator_;
};

}} //ns

#endif
