// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/all_atom_relaxer.hh>
#include <promod3/loop/hydrogen_constructor.hh>
#include <promod3/core/runtime_profiling.hh>
#include <exception>
#include <limits>

namespace promod3 { namespace modelling {

AllAtomRelaxer::AllAtomRelaxer(ScRecDataPtr sc_data,
                               loop::MmSystemCreatorPtr mm_sys_creator)
{
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomRelaxer::AllAtomRelaxer", 2);

  // keep data pointer and creator
  sc_data_ = sc_data;
  mm_system_creator_ = mm_sys_creator;

  // extend full data with sidechains
  all_pos_ = sc_data->all_pos->Copy();
  std::vector<uint>& res_indices = sc_data->env_pos->res_indices;
  for (uint i = 0; i < res_indices.size(); ++i) {
    const uint res_idx = res_indices[i];
    all_pos_->SetResidue(res_idx, *(sc_data->env_pos->all_pos), i);
  }

  // setup system
  mm_system_creator_->SetupSystem(*all_pos_, res_indices,
                                  sc_data->loop_start_indices,
                                  sc_data->loop_lengths,
                                  sc_data->is_n_ter, sc_data->is_c_ter,
                                  sc_data->disulfid_bridges);
}

void AllAtomRelaxer::UpdatePositions(ScRecDataPtr sc_data) {
  
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomRelaxer::UpdateLoopPositions", 2);

  // prepare data
  std::vector<uint>& res_indices = sc_data->env_pos->res_indices;
  for (uint i = 0; i < res_indices.size(); ++i) {
    const uint res_idx = res_indices[i];
    all_pos_->SetResidue(res_idx, *(sc_data_->env_pos->all_pos), i);
  }
  // update mm_system_creator_
  mm_system_creator_->UpdatePositions(*all_pos_, res_indices);
}

Real AllAtomRelaxer::Run(ScRecDataPtr sc_data, int steps, Real stop_criterion) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "AllAtomRelaxer::Run", 2);

  // run it
  ost::mol::mm::SimulationPtr sim = mm_system_creator_->GetSimulation();

  // guard against OpenMM error that is thrown when positions become NaN
  // => typically when you particles are (almost) on top of each other
  try {
    sim->ApplySD(stop_criterion, steps);
  } catch(std::exception& e) {
    return std::numeric_limits<Real>::infinity();
  }

  // update positions in sc_data
  loop::AllAtomPositionsPtr out_pos = sc_data_->env_pos->all_pos;
  mm_system_creator_->ExtractLoopPositions(*out_pos);

  return sim->GetPotentialEnergy();
}

}} //ns
