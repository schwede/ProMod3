// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_SIDECHAIN_ENV_LISTENER_HH
#define PM3_SIDECHAIN_ENV_LISTENER_HH

#include <promod3/core/message.hh>
#include <promod3/core/dynamic_spatial_organizer.hh>
#include <promod3/loop/all_atom_env.hh>
#include <promod3/sidechain/frame.hh>
#include <promod3/sidechain/rotamer_id.hh>
#include <promod3/sidechain/rotamer_lib.hh>
#include <promod3/sidechain/bb_dep_rotamer_lib.hh>
#include <promod3/sidechain/scwrl4_rotamer_constructor.hh>

namespace promod3 { namespace modelling {

class SidechainEnvListener;
typedef boost::shared_ptr<SidechainEnvListener> SidechainEnvListenerPtr;

struct ScCBetaSpatialOrganizerItem {
  ScCBetaSpatialOrganizerItem() { }
  geom::Vec3 pos;
  uint res_idx;
};

typedef core::DynamicSpatialOrganizer<ScCBetaSpatialOrganizerItem>
        ScCBetaSpatialOrganizer;

class SidechainEnvListener : public loop::AllAtomEnvListener {

public:

  // construct: global options can only be set here!
  SidechainEnvListener(bool use_frm = true, bool use_bbdep_lib = true,
                       bool add_cyd_rotamers = true, bool all_rotamers = false);
  SidechainEnvListener(bool use_frm, sidechain::BBDepRotamerLibPtr bbdep_library,
                       bool add_cyd_rotamers = true, bool all_rotamers = false);
  SidechainEnvListener(bool use_frm, sidechain::RotamerLibPtr library,
                       bool add_cyd_rotamers = true, bool all_rotamers = false);

  // AllAtomEnvListener interface
  virtual ~SidechainEnvListener();
  virtual void Init(const loop::AllAtomEnv& base_env);
  virtual void ResetEnvironment(const loop::AllAtomEnv& base_env);
  virtual void UpdateEnvironment(const loop::AllAtomEnv& base_env, 
                                 const std::vector<uint>& res_indices);
  virtual String WhoAmI() const { return "SidechainEnvListener"; }
  
  // get all CB atoms within cutoff
  // - result.first: std::pair<ScCBetaSpatialOrganizerItem*,Real>*
  // - result.second: uint = size of partners found
  ScCBetaSpatialOrganizer::WithinResult FindWithin(
                                  const geom::Vec3& pos, Real cutoff) const {
    return env_.FindWithin(pos, cutoff);
  }
  
  // get settings
  bool HasAllRotamers() const { return all_rotamers_; }
  bool HasFRMRotamers() const { return use_frm_; }
  bool HasCYDRotamers() const { return add_cyd_rotamers_; }

  // get data
  uint GetNumResidues() const { return phi_angle_.size(); }
  bool IsNTerminal(uint res_idx) const { return n_ter_[res_idx]; }
  bool IsCTerminal(uint res_idx) const { return c_ter_[res_idx]; }
  sidechain::RotamerID GetRotamerID(uint res_idx) const { return r_id_[res_idx]; }
  Real GetPhiAngle(uint res_idx) const { return phi_angle_[res_idx]; }
  Real GetPsiAngle(uint res_idx) const { return psi_angle_[res_idx]; }
  sidechain::FrameResiduePtr GetBbFrameResidue(uint res_idx) const {
    return bb_frame_residue_[res_idx];
  }
  sidechain::FrameResiduePtr GetScFrameResidue(uint res_idx) const {
    return sc_frame_residue_[res_idx];
  }
  sidechain::FRMRotamerGroupPtr GetFRMRotamerGroup(uint res_idx) const {
    if (!use_frm_) {
      throw promod3::Error("SidechainEnvListener doesn't have FRMRotamers!");
    }
    return frm_rotamer_group_[res_idx];
  }
  sidechain::RRMRotamerGroupPtr GetRRMRotamerGroup(uint res_idx) const {
    if (use_frm_) {
      throw promod3::Error("SidechainEnvListener doesn't have RRMRotamers!");
    }
    return rrm_rotamer_group_[res_idx];
  }
  sidechain::FRMRotamerGroupPtr GetCydFRMRotamerGroup(uint res_idx) const {
    return cyd_frm_rotamer_group_[res_idx];
  }
  sidechain::RRMRotamerGroupPtr GetCydRRMRotamerGroup(uint res_idx) const {
    // typically all disulfid evaluations are done with FRM disulfids
    // (even when use_frm is false), so this is just a nasty fallback
    uint num_rotamers = cyd_frm_rotamer_group_[res_idx]->size();
    std::vector<sidechain::RRMRotamerPtr> rrm_rotamers(num_rotamers);
    for(uint i = 0; i < num_rotamers; ++i){
      rrm_rotamers[i] = (*cyd_frm_rotamer_group_[res_idx])[i]->ToRRMRotamer(); 
    }
    sidechain::RRMRotamerGroupPtr 
    p(new sidechain::RRMRotamerGroup(rrm_rotamers, res_idx));
    
    return p;
  }
  
  // for convenience: use overload to set rot_group based on type
  void GetRotamerGroup(sidechain::FRMRotamerGroupPtr& rot_group, 
                       uint res_idx) const {
    rot_group = GetFRMRotamerGroup(res_idx);
  }
  void GetRotamerGroup(sidechain::RRMRotamerGroupPtr& rot_group, 
                       uint res_idx) const {
    rot_group = GetRRMRotamerGroup(res_idx);
  }
  void GetCydRotamerGroup(sidechain::FRMRotamerGroupPtr& rot_group, 
                          uint res_idx) const {
    rot_group = GetCydFRMRotamerGroup(res_idx);
  }
  void GetCydRotamerGroup(sidechain::RRMRotamerGroupPtr& rot_group, 
                          uint res_idx) const {
    rot_group = GetCydRRMRotamerGroup(res_idx);
  }

  // create a rotamer group based on internal settings (no use_frm_ check!)
  void CreateRotamerGroup(sidechain::FRMRotamerGroupPtr& rot_group,
                          loop::ConstAllAtomPositionsPtr all_pos, 
                          const sidechain::RotamerID r_id, const uint res_idx) {
    if (use_bbdep_lib_) {
      rot_group = rot_constructor_.ConstructFRMRotamerGroup(*all_pos, res_idx, 
                                                            r_id, res_idx,
                                                            bbdep_library_,
                                                            phi_angle_[res_idx],
                                                            psi_angle_[res_idx],
                                                            n_ter_[res_idx],
                                                            c_ter_[res_idx]);
    } else {
      rot_group = rot_constructor_.ConstructFRMRotamerGroup(*all_pos, res_idx, 
                                                            r_id, res_idx,
                                                            library_,
                                                            phi_angle_[res_idx],
                                                            psi_angle_[res_idx],
                                                            n_ter_[res_idx],
                                                            c_ter_[res_idx]);
    }
  }
  void CreateRotamerGroup(sidechain::RRMRotamerGroupPtr& rot_group,
                          loop::ConstAllAtomPositionsPtr all_pos, 
                          const sidechain::RotamerID r_id, const uint res_idx) {
    if (use_bbdep_lib_) {
      rot_group = rot_constructor_.ConstructRRMRotamerGroup(*all_pos, res_idx,
                                                            r_id, res_idx,
                                                            bbdep_library_,
                                                            phi_angle_[res_idx],
                                                            psi_angle_[res_idx],
                                                            n_ter_[res_idx],
                                                            c_ter_[res_idx]);
    } else {
      rot_group = rot_constructor_.ConstructRRMRotamerGroup(*all_pos, res_idx,
                                                            r_id, res_idx,
                                                            library_,
                                                            phi_angle_[res_idx],
                                                            psi_angle_[res_idx],
                                                            n_ter_[res_idx],
                                                            c_ter_[res_idx]);
    }
  }

private:

  // set stuff for one residue
  void SetResidue_(loop::ConstAllAtomPositionsPtr all_pos, const uint res_idx);
  void SetRotamer_(loop::ConstAllAtomPositionsPtr all_pos,
                   const sidechain::RotamerID r_id, const uint res_idx);

  // global data
  bool all_rotamers_;  // construct rotamers for all res. with BB instead of
                       // only those where sidechain atoms are missing
  bool use_frm_;       // use FRMRotamerGroup instead of RRMRotamerGroup
  bool use_bbdep_lib_; // use BBDepRotamerLib instead of RotamerLib
  bool add_cyd_rotamers_;  // build extra rotamers with r_id = CYD for cysteins
  sidechain::RotamerLibPtr library_;
  sidechain::BBDepRotamerLibPtr bbdep_library_;
  sidechain::SCWRL4RotamerConstructor rot_constructor_;
  ScCBetaSpatialOrganizer env_;

  // fixed data per residue (same numbering as base_env.GetAllPosData())
  std::vector<bool> n_ter_;
  std::vector<bool> c_ter_;
  std::vector<sidechain::RotamerID> r_id_;
  
  // dynamic data per residue
  // -> any data only set if BB set ("if (bb_frame_residue_[res_idx]) ...")
  // -> sc_frame_residue set if not GLY and all pos. available
  // -> xxx_rotamer_group set if no atoms are missing or all_rotamers_
  //    -> if use_frm_, only xxx = frm valid, else, only xxx = rrm
  //    -> CalculateInternalEnergies done for all rotamer groups
  ScCBetaSpatialOrganizerItem* env_data_;
  std::vector<Real> phi_angle_;
  std::vector<Real> psi_angle_;
  std::vector<sidechain::FrameResiduePtr> bb_frame_residue_;
  std::vector<sidechain::FrameResiduePtr> sc_frame_residue_;
  std::vector<sidechain::FRMRotamerGroupPtr> frm_rotamer_group_;
  std::vector<sidechain::RRMRotamerGroupPtr> rrm_rotamer_group_;

  // extra rotamer groups (r_id == CYD) for cysteins to handle disulfid bridges
  // -> constructed for res. with r_id == CYS indep. of all_rotamers_ setting!
  std::vector<sidechain::FRMRotamerGroupPtr> cyd_frm_rotamer_group_;
};

}} // ns

#endif
