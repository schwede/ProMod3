// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>
#include <fstream>

#include <ost/log.hh>
#include <ost/string_ref.hh>

#include <promod3/modelling/afdb.hh>

namespace{
  inline int CharToIdx(char ch) {
    switch(ch){
      case 'A': return 0;
      case 'C': return 1;
      case 'D': return 2;
      case 'E': return 3;
      case 'F': return 4;
      case 'G': return 5;
      case 'H': return 6;
      case 'I': return 7;
      case 'K': return 8;
      case 'L': return 9;
      case 'M': return 10;
      case 'N': return 11;
      case 'P': return 12;
      case 'Q': return 13;
      case 'R': return 14;
      case 'S': return 15;
      case 'T': return 16;
      case 'V': return 17;
      case 'W': return 18;
      case 'Y': return 19;
    }
    std::stringstream ss;
    ss << "nonstandard olc observed: " << ch;
    throw ost::Error(ss.str());
  }

  inline int PentamerToIdx(const char* ptr) {
    return CharToIdx(ptr[0])*160000 + CharToIdx(ptr[1])*8000 +
           CharToIdx(ptr[2])*400 + CharToIdx(ptr[3])*20 + CharToIdx(ptr[4]);
  }
  
  inline int PentamerToIdx(const int* ptr) {
    return ptr[0]*160000 + ptr[1]*8000 + ptr[2]*400 + ptr[3]*20 + ptr[4];
  }

  inline uint64_t AlphaToIdx(char ch) {
    if(ch == ' ') {
      return 0;
    } else {
      return static_cast<uint64_t>(ch - 'A' + 1);
    }
  }

  inline uint64_t NumericToIdx(char ch) {
    if(ch == ' ') {
      return 0;
    } else {
      return static_cast<uint64_t>(ch - '0' + 1);
    }
  }

  inline uint64_t AlphaNumericToIdx(char ch) {
    if(ch == ' ') {
      return 0;
    } else if(ch >= '0' && ch <= '9') {
      return static_cast<uint64_t>(ch-'0' + 1);
    } else {
      return static_cast<uint64_t>(ch-'A' + 1 + 10);
    }
  }

  inline bool CheckAlpha(char ch, bool allow_whitespace) {
    return (ch>='A' and ch<='Z') || (allow_whitespace && ch == ' ');
  }

  inline bool CheckNumeric(char ch, bool allow_whitespace) {
    return (ch>='0' and ch<='9') || (allow_whitespace && ch == ' ');
  }

  inline bool CheckAlphaNumeric(char ch, bool allow_whitespace) {
    return CheckAlpha(ch, allow_whitespace) || 
           CheckNumeric(ch, allow_whitespace);
  }
}

namespace promod3 { namespace modelling {


void SeqToPentamerIndices(const String& seq, bool unique, std::vector<int>& indices) {
  int N = seq.size();
  if(N < 5) {
    std::stringstream ss;
    ss << "Sequence must have at least length 5, got: " << seq;
    throw ost::Error(ss.str());
  }
  indices.resize(N);
  for(int i = 0; i < N; ++i) {
    indices[i] = CharToIdx(seq[i]);
  }
  for(int i = 0; i < N-4; ++i) {
    indices[i] = PentamerToIdx(&indices[i]);
  }
  indices.resize(N-4);
  if(unique) {
    std::sort(indices.begin(), indices.end());
    auto last = std::unique(indices.begin(), indices.end());
    indices.erase(last, indices.end());
  }
}

uint64_t CreateAFDBIdx(const String& uniprot_ac, int fragment, int version) {

  // check if uniprot AC has expected size of 6 or 10
  // https://www.uniprot.org/help/accession_numbers
  size_t ac_size = uniprot_ac.size();
  if(ac_size != 6 && ac_size != 10) {
    std::stringstream ss;
    ss << "Expect uniprot AC to be of size 6 or 10, got: " << uniprot_ac; 
    throw ost::Error(ss.str());
  }

  if(!CheckAlpha(uniprot_ac[0], false)) {
    throw ost::Error("Exp capital alphabetic character at idx 0 of uniprot AC");
  }

  if(!CheckNumeric(uniprot_ac[1], false)) {
    throw ost::Error("Exp capital alphabetic character at idx 1 of uniprot AC");
  }

  if(!CheckAlphaNumeric(uniprot_ac[2], false)) {
    throw ost::Error("Exp capital alphabetic character at idx 2 of uniprot AC");
  }

  if(!CheckAlphaNumeric(uniprot_ac[3], false)) {
    throw ost::Error("Exp capital alphabetic character at idx 3 of uniprot AC");
  }

  if(!CheckAlphaNumeric(uniprot_ac[4], false)) {
    throw ost::Error("Exp capital alphabetic character at idx 4 of uniprot AC");
  }

  if(!CheckNumeric(uniprot_ac[5], false)) {
    throw ost::Error("Exp capital alphabetic character at idx 5 of uniprot AC");
  }

  if(ac_size > 6) {
    if(!CheckAlpha(uniprot_ac[6], true)) {
      throw ost::Error("Exp capital alphabetic character at idx 6 of uniprot AC");
    }
  
    if(!CheckAlphaNumeric(uniprot_ac[7], true)) {
      throw ost::Error("Exp capital alphabetic character at idx 7 of uniprot AC");
    }
  
    if(!CheckAlphaNumeric(uniprot_ac[8], true)) {
      throw ost::Error("Exp capital alphabetic character at idx 8 of uniprot AC");
    }
  
    if(!CheckNumeric(uniprot_ac[9], true)) {
      throw ost::Error("Exp capital alphabetic character at idx 9 of uniprot AC");
    }
  }

  if(fragment < 0 || fragment > 127) {
    std::stringstream ss;
    ss << "Expect fragment to be in range [0, 127], got: " << fragment; 
  }
  if(version < 0 || version > 31) {
    std::stringstream ss;
    ss << "Expect version to be in range [0, 31], got: " << version;
  }

  uint64_t idx = 0;
  idx += AlphaToIdx(uniprot_ac[0]);
  idx += NumericToIdx(uniprot_ac[1]) << 5;
  idx += AlphaNumericToIdx(uniprot_ac[2]) << 9;
  idx += AlphaNumericToIdx(uniprot_ac[3]) << 15;
  idx += AlphaNumericToIdx(uniprot_ac[4]) << 21;
  idx += NumericToIdx(uniprot_ac[5]) << 27;
  if(ac_size > 6) {
    idx += AlphaToIdx(uniprot_ac[6]) << 31;
    idx += AlphaNumericToIdx(uniprot_ac[7]) << 36;
    idx += AlphaNumericToIdx(uniprot_ac[8]) << 42;
    idx += NumericToIdx(uniprot_ac[9]) << 48;
  }
  idx += static_cast<uint64_t>(fragment) << 52;
  idx += static_cast<uint64_t>(version) << 59;
  return idx;
}

void CreatePentaMatch(const ost::seq::SequenceList& seq_list,
                      const String& db_path,
                      bool entries_from_seqnames) {

  String indexer_path = db_path + "/" + "indexer.dat";
  String pos_path = db_path + "/" + "pos.dat";
  String length_path = db_path + "/" + "length.dat";
  String meta_path = db_path + "/" + "meta.dat";

  std::vector<int> entry_indices;
  entry_indices.reserve(seq_list.GetCount());
  if(entries_from_seqnames) {
    for(int i = 0; i < seq_list.GetCount(); ++i) {
      const String& sname = seq_list[i].GetName();
      ost::StringRef s(&sname[0], sname.size());
      std::pair<bool, int> idx = s.trim().to_int();
      if(idx.first == false) {
        std::stringstream ss;
        ss << "Cannot cast seq name to integer: " << sname;
        throw ost::Error(ss.str());
      }
      entry_indices.push_back(idx.second);
    }
  } else {
    for(int i = 0; i < seq_list.GetCount(); ++i) {
      entry_indices.push_back(i);
    }
  }

  std::vector<std::vector<int32_t> > indexer(3200000);
  for(int i = 0; i < seq_list.GetCount(); ++i) {
    int entry_idx = entry_indices[i];
    std::vector<int> penta_indices;
    SeqToPentamerIndices(seq_list[i].GetString(), true, penta_indices);
    for(auto j = penta_indices.begin(); j != penta_indices.end(); ++j) {
      indexer[*j].push_back(entry_idx);
    }
  }

  for(auto i = indexer.begin(); i != indexer.end(); ++i) {
    std::sort(i->begin(), i->end());
  }

  std::ofstream indexer_stream(indexer_path, std::ios::binary);
  if(!indexer_stream) {
    throw ost::Error("Could not open indexer file: " + indexer_path);
  }
  std::vector<int64_t> positions;
  std::vector<int32_t> lengths;
  int64_t current_pos = 0;
  for(auto i = indexer.begin(); i != indexer.end(); ++i) {
    int size = i->size();
    if(size > 0) {
      indexer_stream.write(reinterpret_cast<char*>(&(*i)[0]),
                           size*sizeof(int32_t));
    }
    positions.push_back(current_pos);
    lengths.push_back(size);
    current_pos += size;
  }
  indexer_stream.close();

  std::ofstream pos_stream(pos_path, std::ios::binary);
  std::ofstream length_stream(length_path, std::ios::binary);
  std::ofstream meta_stream(meta_path, std::ofstream::out);
  if(!pos_stream) {
    throw ost::Error("Could not open pos file: " + pos_path);
  }
  if(!length_stream) {
    throw ost::Error("Could not open length file: " + length_path);
  }
  if(!meta_stream) {
    throw ost::Error("Could not open meta file: " + meta_path);
  }
  pos_stream.write(reinterpret_cast<char*>(&positions[0]),
                   positions.size()*sizeof(int64_t));
  length_stream.write(reinterpret_cast<char*>(&lengths[0]),
                      lengths.size()*sizeof(int32_t));
  int32_t max_entry_idx = *std::max_element(entry_indices.begin(),
                                            entry_indices.end());
  meta_stream << max_entry_idx;
  pos_stream.close();
  length_stream.close();
  meta_stream.close();
}



}} //ns
