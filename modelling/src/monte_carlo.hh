// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_MONTE_CARLO_HH
#define PROMOD_MODELLING_MONTE_CARLO_HH

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>

#include <promod3/loop/backbone.hh>
#include <promod3/modelling/backbone_relaxer.hh>
#include <promod3/modelling/monte_carlo_cooler.hh>
#include <promod3/modelling/monte_carlo_scorer.hh>
#include <promod3/modelling/monte_carlo_sampler.hh>
#include <promod3/modelling/monte_carlo_closer.hh>

namespace promod3 { namespace modelling {

  void SampleMonteCarlo(MonteCarloSamplerPtr sampler, MonteCarloCloserPtr closer,
                        MonteCarloScorerPtr scorer, MonteCarloCoolerPtr cooler,
                        uint steps, loop::BackboneList& backbone_list,
                        bool initialize = true, uint seed = 0,
                        bool lowest_energy_conformation = true);
}}

#endif

