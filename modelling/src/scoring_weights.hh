// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_MODELLING_SCORING_WEIGHTS_HH
#define PROMOD3_MODELLING_SCORING_WEIGHTS_HH

#include <map>
#include <vector>
#include <ost/base.hh>

namespace promod3 { namespace modelling {

// Defines lookups for (modifiable) scorer weights and keys.
class ScoringWeights {
public:
  // Singleton access to one modifiable instance (as in AminoAcidLookup etc)
  static ScoringWeights& GetInstance() {
    static ScoringWeights instance;
    return instance;
  }
  // Weight access
  const std::map<String, Real>& GetWeights(bool with_db = false,
                                           bool with_aa = false,
                                           bool length_dependent=false,
                                           int loop_length = -1) const;
  void SetWeights(bool with_db, bool with_aa,
                  const std::map<String, Real>& weights,
                  bool length_dependent = false, 
                  int loop_length = -1);
  // get subsets (as copy for further editing/use)
  std::map<String, Real> GetBackboneWeights(bool with_db = false,
                                            bool with_aa = false,
                                            bool length_dependent=false,
                                            int loop_length = -1) const;
  std::map<String, Real> GetAllAtomWeights(bool with_db = false,
                                           bool length_dependent=false,
                                           int loop_length = -1) const;
  // Key access
  const String& GetStemRMSDsKey() const { return stem_rmsd_key_; }
  void SetStemRMSDsKey(const String& key) { stem_rmsd_key_ = key; }
  const String& GetSequenceProfileScoresKey() const {
    return seq_prof_score_key_;
  }
  void SetSequenceProfileScoresKey(const String& key) {
    seq_prof_score_key_ = key;
  }
  const String& GetStructureProfileScoresKey() const {
    return str_prof_score_key_;
  }
  void SetStructureProfileScoresKey(const String& key) {
    str_prof_score_key_ = key;
  }
  const std::vector<String>& GetBackboneScoringKeys() const {
    return bb_scoring_keys_;
  }
  void SetBackboneScoringKeys(const std::vector<String>& keys) {
    bb_scoring_keys_ = keys;
  }
  const std::vector<String>& GetAllAtomScoringKeys() const {
    return aa_scoring_keys_;
  }
  void SetAllAtomScoringKeys(const std::vector<String>& keys) {
    aa_scoring_keys_ = keys;
  }
private:
  // construction only inside here
  ScoringWeights();

  // weights for all combinations, length agnostic
  std::map<String, Real> weights_bb_;
  std::map<String, Real> weights_bb_db_;
  std::map<String, Real> weights_bb_aa_;
  std::map<String, Real> weights_bb_db_aa_;

  // weights for all combinations, length dependent. The length key of -1 
  // represents the fallback for not covered input lengths (e.g. long loops)
  std::map<int, std::map<String, Real> > length_dependent_weights_bb_;
  std::map<int, std::map<String, Real> > length_dependent_weights_bb_db_;
  std::map<int, std::map<String, Real> > length_dependent_weights_bb_aa_;
  std::map<int, std::map<String, Real> > length_dependent_weights_bb_db_aa_;

  // key names
  String stem_rmsd_key_;
  String seq_prof_score_key_;
  String str_prof_score_key_;
  std::vector<String> bb_scoring_keys_;
  std::vector<String> aa_scoring_keys_;
};

}} //ns

#endif
