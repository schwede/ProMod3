// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/sidechain_reconstructor.hh>
#include <promod3/sidechain/disulfid.hh>
#include <promod3/sidechain/rotamer_graph.hh>
#include <promod3/sidechain/subrotamer_optimizer.hh>
#include <promod3/core/message.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace modelling {

namespace {

// HELPERS
const geom::Vec3&
GetCxPos_(const loop::AllAtomPositions& new_pos, uint res_idx) {
  // use CA for GLY, CB otherwise
  if (new_pos.GetAA(res_idx) == ost::conop::GLY) {
    if (!new_pos.IsSet(res_idx, loop::BB_CA_INDEX)) {
      throw promod3::Error("CA must be set for all residues in Reconstruct!");
    }
    return new_pos.GetPos(res_idx, loop::BB_CA_INDEX);
  } else {
    if (!new_pos.IsSet(res_idx, loop::BB_CB_INDEX)) {
      throw promod3::Error("CB must be set for all residues in Reconstruct!");
    }
    return new_pos.GetPos(res_idx, loop::BB_CB_INDEX);
  }
}

} // anon ns

SidechainReconstructionDataPtr
SidechainReconstructor::Reconstruct(uint start_resnum, uint num_residues,
                                    uint chain_idx) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "SidechainReconstructor::Reconstruct", 2);

  if (!attached_environment_) {
    throw promod3::Error("Can only reconstruct sidechains when environment is "
                         "attached!");
  }

  // get loop indices
  std::vector<uint> indices = idx_handler_->ToIdxVector(start_resnum,
                                                        num_residues,
                                                        chain_idx);
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  if (num_residues > 0) {
    loop_start_indices.push_back(0);
    loop_lengths.push_back(num_residues);
  }
  return ReconstructFromIndices(indices, loop_start_indices, loop_lengths);

}

SidechainReconstructionDataPtr
SidechainReconstructor::Reconstruct(
                       const std::vector<uint>& start_resnums,
                       const std::vector<uint>& num_residues,
                       const std::vector<uint>& chain_indices) const {
  
  promod3::core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                            "SidechainReconstructor::Reconstruct", 2);

  if (!attached_environment_) {
    throw promod3::Error("Can only reconstruct sidechains when environment is "
                         "attached!");
  }

  // get loop indices
  std::vector<uint> loop_start_indices;
  std::vector<uint> loop_lengths;
  std::vector<uint> indices = idx_handler_->ToIdxVector(start_resnums,
                                                        num_residues,
                                                        chain_indices,
                                                        loop_start_indices,
                                                        loop_lengths, true);
  return ReconstructFromIndices(indices, loop_start_indices, loop_lengths);
}

void SidechainReconstructor::AttachEnvironment(loop::AllAtomEnv& env,
                                               bool use_frm,
                                               bool use_bbdep_lib) {
  loop::AllAtomEnvListenerPtr tst = env.GetListener("SidechainEnvListener");
  if (tst) {
    env_ = boost::dynamic_pointer_cast<SidechainEnvListener>(tst);
    CheckEnvListener_(use_frm);
  } else {
    // new one needed
    env_.reset(new SidechainEnvListener(use_frm, use_bbdep_lib,
                                        build_disulfids_, !keep_sidechains_));
    env.AttachListener(env_);
  }
  SetEnvData_(env);
}
void SidechainReconstructor::AttachEnvironment(loop::AllAtomEnv& env,
                                               bool use_frm,
                                               sidechain::BBDepRotamerLibPtr 
                                               bbdep_library) {
  loop::AllAtomEnvListenerPtr tst = env.GetListener("SidechainEnvListener");
  if (tst) {
    env_ = boost::dynamic_pointer_cast<SidechainEnvListener>(tst);
    CheckEnvListener_(use_frm);
  } else {
    // new one needed
    env_.reset(new SidechainEnvListener(use_frm, bbdep_library,
                                        build_disulfids_, !keep_sidechains_));
    env.AttachListener(env_);
  }
  SetEnvData_(env);
}
void SidechainReconstructor::AttachEnvironment(loop::AllAtomEnv& env,
                                               bool use_frm,
                                               sidechain::RotamerLibPtr 
                                               library) {
  loop::AllAtomEnvListenerPtr tst = env.GetListener("SidechainEnvListener");
  if (tst) {
    env_ = boost::dynamic_pointer_cast<SidechainEnvListener>(tst);
    CheckEnvListener_(use_frm);
  } else {
    // new one needed
    env_.reset(new SidechainEnvListener(use_frm, library, build_disulfids_, 
                                        !keep_sidechains_));
    env.AttachListener(env_);
  }
  SetEnvData_(env);
}

SidechainReconstructionDataPtr
SidechainReconstructor::ReconstructFromIndices(
                       const std::vector<uint>& indices,
                       const std::vector<uint>& loop_start_indices,
                       const std::vector<uint>& loop_lengths) const {

  // prepare result
  SidechainReconstructionDataPtr res(new SidechainReconstructionData());
  res->env_pos.reset(new loop::AllAtomEnvPositions);
  std::vector<uint>& res_indices = res->env_pos->res_indices;
  res_indices = indices;
  res->loop_start_indices = loop_start_indices;
  res->loop_lengths = loop_lengths;
  res->all_pos = all_pos_;

  std::set<uint> nb_frame_indices;
  Real overall_cutoff = remodel_cutoff_ + rigid_frame_cutoff_;

  // get surrounding
  std::set<uint> nb_indices;
  ScCBetaSpatialOrganizer::WithinResult within_result;
  std::pair<ScCBetaSpatialOrganizerItem*,Real>* a;
  for (uint i = 0; i < indices.size(); ++i) {
    const uint res_idx = res_indices[i];
    within_result = env_->FindWithin(GetCxPos_(*all_pos_, res_idx), 
                                     overall_cutoff);
    a = within_result.first;
    for (uint j = 0; j < within_result.second; ++j) {
      if(a[j].second <= remodel_cutoff_) nb_indices.insert(a[j].first->res_idx);
      else nb_frame_indices.insert(a[j].first->res_idx);
    }
  }

  // remove the indices form nb_indices
  for (std::vector<uint>::iterator i = res_indices.begin();
       i != res_indices.end(); ++i) {
    nb_indices.erase(*i);
  }

  // remove indices from nb_frame_indices
  for (std::vector<uint>::iterator i = res_indices.begin();
       i != res_indices.end(); ++i) {
    nb_frame_indices.erase(*i);
  }

  // and also the nb_indices
  for (std::set<uint>::iterator i = nb_indices.begin();
       i != nb_indices.end(); ++i) {
    nb_frame_indices.erase(*i);
  }

  // append unique surrounding residue indices (first loop part, then rest)
  res_indices.insert(res_indices.end(), nb_indices.begin(), nb_indices.end());
  // assign all frame residues close to the unique surrounding residues
  res->rigid_frame_indices.assign(nb_frame_indices.begin(),
                                  nb_frame_indices.end());

  // add information on termini
  res->is_n_ter.resize(res_indices.size());
  res->is_c_ter.resize(res_indices.size());
  for (uint i = 0; i < res_indices.size(); ++i) {
    const uint res_idx = res_indices[i];
    res->is_n_ter[i] = env_->IsNTerminal(res_idx);
    res->is_c_ter[i] = env_->IsCTerminal(res_idx);
  }

  // solve system
  if (env_->HasFRMRotamers()) SolveSystem_<sidechain::FRMRotamerGroup>(res);
  else                        SolveSystem_<sidechain::RRMRotamerGroup>(res);
  return res;
}

void SidechainReconstructor::CheckEnvListener_(bool use_frm) const {
  // check consistency of relevant features
  if (use_frm != env_->HasFRMRotamers()) {
    throw promod3::Error("Inconsistency in FRM Rotamer wishes!");
  }
  if (!keep_sidechains_ && !env_->HasAllRotamers()) {
    throw promod3::Error("Need SidechainEnvListener with all rotamers when "
                         "not keeping existing sidechains!");
  }
  if (build_disulfids_ && !env_->HasCYDRotamers()) {
    throw promod3::Error("Need SidechainEnvListener with CYD rotamers when "
                         "trying to build disulfid bridges!");
  }
}

void SidechainReconstructor::SetEnvData_(loop::AllAtomEnv& env) {
  idx_handler_ = env.GetIdxHandlerData();
  all_pos_ = env.GetAllPosData();
  attached_environment_ = true;
}


void SidechainReconstructor::BuildDisulfids_(
                       SidechainReconstructionDataPtr res,
                       std::vector<uint>& cys_indices,
                       std::vector<sidechain::FrameResiduePtr>& frame_residues,
                       std::vector<bool>& has_sidechain) const {
  // setup
  std::vector<uint>& res_indices = res->env_pos->res_indices;
  typedef sidechain::FRMRotamer Rotamer;
  typedef sidechain::FRMRotamerPtr RotamerPtr;
  typedef sidechain::FRMRotamerGroup RotamerGroup;
  typedef sidechain::FRMRotamerGroupPtr RotamerGroupPtr;

  loop::AllAtomPositions& out_pos = *(res->env_pos->all_pos);

  // collect rotamers and data required to resolve the disulfid bridges
  sidechain::FramePtr frame(new sidechain::Frame(frame_residues));
  std::vector<RotamerGroupPtr> cys_rotamer_groups;
  std::vector<geom::Vec3> ca_pos;
  std::vector<geom::Vec3> cb_pos;
  std::vector<uint> cys_with_rot_groups;

  uint num_cys_indices = cys_indices.size();
  cys_rotamer_groups.reserve(num_cys_indices);
  ca_pos.reserve(num_cys_indices);
  cb_pos.reserve(num_cys_indices);
  cys_with_rot_groups.reserve(num_cys_indices);

  for (uint i = 0; i < cys_indices.size(); ++i) {
    const uint global_idx = res_indices[cys_indices[i]];
    RotamerGroupPtr p;
    env_->GetCydRotamerGroup(p, global_idx);
    if (p) {
      if(keep_sidechains_) {
        sidechain::FrameResiduePtr f_res = env_->GetScFrameResidue(global_idx);
        if(f_res) {
          // We want to keep existing sidechains (keep_sidechains_ == true) 
          // and everything seems to be present since the sidechain frame 
          // residue exists. We therefore replace the extracted FRMRotamerGroup 
          // (p) with the SG position from the frame. 
          // The reason for still creating a FRMRotamerGroup is that it can 
          // enter the ResolveCystein evaluation.
          std::vector<sidechain::Particle> particles;
          for(uint j = 0; j < f_res->size(); ++j) {
            if((*f_res)[j].GetName() == "SG") {
              particles.push_back((*f_res)[j]);
              break;
            }
          }
          if(particles.empty()) {
            throw promod3::Error("Could not find SG atom in CYS frame residue");
          }
          // The temperature and self internal_e_prefactor parameter have been 
          // copied from the SCWRL4RotamerConstructor. Hacky...
          RotamerPtr r(new Rotamer(particles, (*p)[0]->GetTemperature(), 1.0,
                                   (*p)[0]->GetInternalEnergyPrefactor()));
          std::vector<int> subrotamer_definition(1,0);
          r->AddSubrotamerDefinition(subrotamer_definition);
          r->SetInternalEnergy(0.0);
          std::vector<RotamerPtr> rotamers;
          rotamers.push_back(r);
          RotamerGroupPtr new_rot_group(new RotamerGroup(rotamers, global_idx));  
          p = new_rot_group;                                            
        }
      }
      
      p->SetFrameEnergy(frame);
      cys_rotamer_groups.push_back(p);
      ca_pos.push_back(all_pos_->GetPos(global_idx, loop::BB_CA_INDEX));
      cb_pos.push_back(all_pos_->GetPos(global_idx, loop::BB_CB_INDEX));
      cys_with_rot_groups.push_back(cys_indices[i]);
    }
  }

  std::vector<std::pair<uint, uint> > bond_solution;
  std::vector<std::pair<uint, uint> > rot_solution;

  ResolveCysteins(cys_rotamer_groups, ca_pos, cb_pos,
                  disulfid_score_thresh_, true,
                  bond_solution, rot_solution);

  std::vector<int> is_bonded(cys_rotamer_groups.size(), 0);

  for(uint solution_idx = 0; solution_idx < bond_solution.size(); 
      ++solution_idx) {

    const std::pair<uint, uint>& bond_pair = bond_solution[solution_idx];
    const std::pair<uint, uint>& rot_pair = rot_solution[solution_idx];

    // fetch required data
    RotamerPtr r_one = (*(cys_rotamer_groups[bond_pair.first]))[rot_pair.first];
    RotamerPtr r_two = (*(cys_rotamer_groups[bond_pair.second]))[rot_pair.second];
    uint idx_one = cys_with_rot_groups[bond_pair.first];
    uint idx_two = cys_with_rot_groups[bond_pair.second];
    uint global_idx_one = res_indices[idx_one];
    uint global_idx_two = res_indices[idx_two];

    // update the rotamer groups participating in disulfid bonds
    is_bonded[bond_pair.first] = 1;
    is_bonded[bond_pair.second] = 1; 

    // do first disulfid bond partner
    frame_residues.push_back(r_one->ToFrameResidue(global_idx_one));
    r_one->ApplyOnResidue(out_pos, idx_one);
    has_sidechain[idx_one] = true;   

    // do second disulfid bond partner
    frame_residues.push_back(r_two->ToFrameResidue(global_idx_two));
    r_two->ApplyOnResidue(out_pos, idx_two);
    has_sidechain[idx_two] = true;  

    res->disulfid_bridges.push_back(std::make_pair(idx_one, idx_two)); 
  }

  // in case of keep_sidechains_, we add all complete cysteins not participating
  // to any disulfid bond also to the frame
  if(keep_sidechains_) {
    for(uint i = 0; i < is_bonded.size(); ++i){
      if(is_bonded[i] == 0){
        const uint idx = cys_with_rot_groups[i];
        const uint global_idx = res_indices[idx];
        sidechain::FrameResiduePtr frame_res = 
        env_->GetScFrameResidue(global_idx);
        if(frame_res) {
          frame_residues.push_back(frame_res);
          has_sidechain[idx] = true;
        }
      }
    }
  }

}

template<typename RotamerGroupPtr>
void SidechainReconstructor::CollectRotamerGroups_(
                       SidechainReconstructionDataPtr res,
                       std::vector<RotamerGroupPtr>& rotamer_groups,
                       std::vector<bool>& has_sidechain) const {
  // setup
  std::vector<uint>& res_indices = res->env_pos->res_indices;
  typedef typename RotamerGroupPtr::element_type RotamerGroup;
  // do it
  for (uint i = 0; i < res_indices.size(); ++i) {
    const uint res_idx = res_indices[i];
    // check rotamer
    if (!has_sidechain[i]) {
      // add all available rotamers
      RotamerGroupPtr rot_group;
      env_->GetRotamerGroup(rot_group, res_idx);
      if (rot_group) {
        // add a copy (we modify rotamer in ApplySelfEnergyThresh!)
        RotamerGroupPtr new_group(new RotamerGroup(*rot_group));
        rotamer_groups.push_back(new_group);
        res->rotamer_res_indices.push_back(i);
      }
    }
  }
}

void ApplySubrotamerOptimization(std::vector<sidechain::FRMRotamerGroupPtr>& 
                                 rotamer_groups,
                                 const std::vector<int>& solution) {

  uint num_rotamer_groups = rotamer_groups.size();

  std::vector<sidechain::FRMRotamerPtr> rotamers(rotamer_groups.size());
  for(uint i = 0; i < num_rotamer_groups; ++i){
    rotamers[i] = (*rotamer_groups[i])[solution[i]];
  }
  sidechain::SubrotamerOptimizer(rotamers);
}

void ApplySubrotamerOptimization(std::vector<sidechain::RRMRotamerGroupPtr>& 
                                 rotamer_groups,
                                 const std::vector<int>& solution) {
  //there's nothing to do... 
}


template<typename RotamerGroup>
void SidechainReconstructor::SolveSystem_(
                       SidechainReconstructionDataPtr res) const {
  // setup
  std::vector<uint>& res_indices = res->env_pos->res_indices;
  std::vector<uint>& rigid_frame_indices = res->rigid_frame_indices;
  typedef boost::shared_ptr<RotamerGroup> RotamerGroupPtr;

  // prepare solution
  res->env_pos->all_pos = all_pos_->Extract(res_indices);
  loop::AllAtomPositions& out_pos = *(res->env_pos->all_pos);

  // nothing to be done if empty
  if (res_indices.empty()) return;
  
  // collect frame residues and cysteins (if needed)
  std::vector<sidechain::FrameResiduePtr> frame_residues;
  std::vector<bool> has_sidechain(res_indices.size(), false);
  std::vector<uint> cys_indices;
  for (uint i = 0; i < res_indices.size(); ++i) {
    const uint res_idx = res_indices[i];
    // add backbone frame residue if available
    sidechain::FrameResiduePtr frame_res = env_->GetBbFrameResidue(res_idx);
    if (frame_res) frame_residues.push_back(frame_res);
    // check sidechain
    if (build_disulfids_ && env_->GetRotamerID(res_idx) == sidechain::CYS) {
      cys_indices.push_back(i);
    } else if (keep_sidechains_) {
      frame_res = env_->GetScFrameResidue(res_idx);
      if (frame_res) {
        frame_residues.push_back(frame_res);
        has_sidechain[i] = true;
      }
    }
  }

  // collect frame residues not associated to the remodelled residues, but
  // are nevertheless close enough to be considered
  for(uint i = 0; i < rigid_frame_indices.size(); ++i) {
    const uint res_idx = rigid_frame_indices[i];
    sidechain::FrameResiduePtr bb_frame_res = env_->GetBbFrameResidue(res_idx);
    if(bb_frame_res) frame_residues.push_back(bb_frame_res);
    sidechain::FrameResiduePtr sc_frame_res = env_->GetScFrameResidue(res_idx);
    if(sc_frame_res) frame_residues.push_back(sc_frame_res);
  }

  // handle cysteins
  res->disulfid_bridges.clear();
  if (build_disulfids_) {
    BuildDisulfids_(res, cys_indices, frame_residues, has_sidechain);
  }
  
  // collect rotamers
  std::vector<RotamerGroupPtr> rotamer_groups;
  res->rotamer_res_indices.clear();  // same size as rotamer_groups
  CollectRotamerGroups_(res, rotamer_groups, has_sidechain);

  // set frame energies in rotamers (note: internal energies are precomputed)
  sidechain::FramePtr frame(new sidechain::Frame(frame_residues));
  for (uint i = 0; i < rotamer_groups.size(); ++i) {
    rotamer_groups[i]->SetFrameEnergy(frame);
    rotamer_groups[i]->ApplySelfEnergyThresh();
  }

  // solve graph
  sidechain::RotamerGraphPtr graph = 
  sidechain::RotamerGraph::CreateFromList(rotamer_groups);
  std::pair<std::vector<int>,Real> solution = 
    graph->TreeSolve(graph_max_complexity_, graph_intial_epsilon_);

  // do subrotamer optimization if required
  if(optimize_subrotamers_){
    ApplySubrotamerOptimization(rotamer_groups, solution.first);
  }

  // apply solution to subset of data
  for (uint i = 0; i < res->rotamer_res_indices.size(); ++i) {
    const uint res_idx = res->rotamer_res_indices[i];
    rotamer_groups[i]->ApplyOnResidue(solution.first[i], out_pos, res_idx);
  }
}

}} // ns
