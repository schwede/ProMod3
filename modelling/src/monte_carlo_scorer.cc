// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/monte_carlo_scorer.hh>

namespace promod3 { namespace modelling {

MonteCarloScorer::~MonteCarloScorer() { }

LinearScorer::LinearScorer(scoring::BackboneOverallScorerPtr scorer,
                           scoring::BackboneScoreEnvPtr scorer_env,
                           uint start_resnum, uint num_residues, 
                           uint chain_index, 
                           const std::map<String,Real>& weights): 
                           scorer_(scorer),
                           scorer_env_(scorer_env),
                           start_resnum_(start_resnum),
                           num_residues_(num_residues),
                           chain_index_(chain_index)
{
  w_scorers_ = scorer->GetWeightedScorers(weights);
}

Real LinearScorer::GetScore(const loop::BackboneList& positions) {

  if(positions.size() != num_residues_) {
    throw promod3::Error("Input BackboneList is inconsistent with loop length "
                         "that you defined at initialization of the "
                         "LinearScorer");
  }

  scorer_env_->SetEnvironment(positions, start_resnum_, chain_index_);
  Real score = 0.0;
  for (uint i = 0; i < w_scorers_.size(); ++i) {
    const Real weight = w_scorers_[i].first;
    scoring::BackboneScorerPtr one_scorer = w_scorers_[i].second;
    score += weight * one_scorer->CalculateScore(start_resnum_,
                                                 num_residues_,
                                                 chain_index_);
  }
  return score;
}

}}//ns
