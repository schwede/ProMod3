// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/monte_carlo_sampler.hh>


namespace promod3 { namespace modelling {

MonteCarloSampler::~MonteCarloSampler() { }

PhiPsiSampler::PhiPsiSampler(const String& sequence,
                             loop::TorsionSamplerPtr torsion_sampler,
                             Real n_stem_phi, Real c_stem_psi, char prev_aa,
                             char next_aa, uint seed)
                             : sequence_(sequence), rgen_(seed)
                             , n_stem_phi_(n_stem_phi), c_stem_psi_(c_stem_psi)
                             , angle_selector_(0, sequence_.size()-1)
{
  torsion_sampler_.assign(sequence_.size(), torsion_sampler);
  std::stringstream ss;
  ss << prev_aa;
  ss << sequence_;
  ss << next_aa;
  dihedral_indices_ = torsion_sampler->GetHistogramIndices(ss.str());
}

PhiPsiSampler::PhiPsiSampler(const String& sequence,
                             loop::TorsionSamplerList torsion_sampler,
                             Real n_stem_phi, Real c_stem_psi, char prev_aa,
                             char next_aa, uint seed)
                             : sequence_(sequence)
                             , torsion_sampler_(torsion_sampler), rgen_(seed)
                             , n_stem_phi_(n_stem_phi), c_stem_psi_(c_stem_psi)
                             , angle_selector_(0, sequence_.size()-1)
{
  if (sequence.size() != torsion_sampler.size()) {
    throw promod3::Error("Sequence must have same size as provided "
                         "torsionsamplers when initializing PhiPsiSampler!");
  }                            

  std::vector<ost::conop::AminoAcid> aa;
  aa.push_back(ost::conop::OneLetterCodeToAminoAcid(prev_aa));
  if (aa.back() == ost::conop::XXX) {
    throw promod3::Error("Only standard amino acids are allowed for setting up PhiPsiSampler!");
  }
  for (uint i = 0; i < sequence_.size(); ++i) {
    aa.push_back(ost::conop::OneLetterCodeToAminoAcid(sequence_[i]));
    if (aa.back() == ost::conop::XXX) {
      throw promod3::Error("Only standard amino acids are allowed for setting up PhiPsiSampler!");
    }
  }
  aa.push_back(ost::conop::OneLetterCodeToAminoAcid(next_aa));
  if (aa.back() == ost::conop::XXX) {
    throw promod3::Error("Only standard amino acids are allowed for setting up PhiPsiSampler!");
  }

  for(uint i = 1; i < sequence_.size()+1; ++i){
    dihedral_indices_.push_back(
      torsion_sampler_[i-1]->GetHistogramIndex(aa[i-1], aa[i], aa[i+1]));
  }
}

void PhiPsiSampler::Initialize(loop::BackboneList& positions) {

  positions.clear();
  std::vector<Real> phi_angles;
  std::vector<Real> psi_angles;
  std::pair<Real,Real> actual_draw;
  for(uint i = 0; i < dihedral_indices_.size(); ++i){
    actual_draw = torsion_sampler_[i]->Draw(dihedral_indices_[i]);
    phi_angles.push_back(actual_draw.first);
    psi_angles.push_back(actual_draw.second);
  }
  loop::BackboneList bb_list(sequence_, phi_angles, psi_angles);
  positions = bb_list;
}

void PhiPsiSampler::ProposeStep(const loop::BackboneList& actual_positions,
                                loop::BackboneList& proposal) {

  if (actual_positions.size() != sequence_.size()) {
    std::stringstream ss;
    ss << "Given positions must be consistent in size with the samplers ";
    ss << "sequence!";
    throw promod3::Error(ss.str());
  }

  proposal = actual_positions; 

  if(proposal.size() <= 1){
    return; // theres nothing to sample...
  }

  int angle_to_modify = angle_selector_(rgen_);

  if (angle_to_modify == 0) {
    Real proposed_angle = torsion_sampler_[angle_to_modify]->DrawPsiGivenPhi(dihedral_indices_[angle_to_modify],n_stem_phi_);
    proposal.SetPsiTorsion(0, proposed_angle);
  } else if (angle_to_modify == static_cast<int>(dihedral_indices_.size())-1) {
    Real proposed_angle = torsion_sampler_[angle_to_modify]->DrawPhiGivenPsi(dihedral_indices_[angle_to_modify],c_stem_psi_);
    proposal.SetPhiTorsion(dihedral_indices_.size()-1, proposed_angle);
  } else {
    std::pair<Real,Real> dihedral_angles = torsion_sampler_[angle_to_modify]->Draw(dihedral_indices_[angle_to_modify]);
    proposal.SetPhiPsiTorsion(angle_to_modify, dihedral_angles.first,
                              dihedral_angles.second);
  }
}

SoftSampler::SoftSampler(const String& sequence,
                         loop::TorsionSamplerPtr torsion_sampler, Real max_dev,
                         Real n_stem_phi, Real c_stem_psi, char prev_aa,
                         char next_aa, uint seed)
                         : sequence_(sequence), max_dev_(max_dev), rgen_(seed)
                         , n_stem_phi_(n_stem_phi), c_stem_psi_(c_stem_psi)
                         , angle_selector_(0,2*sequence_.size()-3)
                         , zero_one_(rgen_) {

  torsion_sampler_.assign(sequence_.size(), torsion_sampler);
  std::stringstream ss;
  ss << prev_aa;
  ss << sequence_;
  ss << next_aa;
  dihedral_indices_ = torsion_sampler->GetHistogramIndices(ss.str());
}

SoftSampler::SoftSampler(const String& sequence,
                         loop::TorsionSamplerList torsion_sampler,
                         Real max_dev, Real n_stem_phi, Real c_stem_psi,
                         char prev_aa, char next_aa, uint seed)
                         : sequence_(sequence)
                         , torsion_sampler_(torsion_sampler)
                         , max_dev_(max_dev), rgen_(seed)
                         , n_stem_phi_(n_stem_phi), c_stem_psi_(c_stem_psi)
                         , angle_selector_(0,2*sequence_.size()-3)
                         , zero_one_(rgen_) {

  if (sequence.size() != torsion_sampler.size()) {
    throw promod3::Error("Sequence must have same size as provided "
                         "torsionsamplers when initializing SoftSampler!");
  }                            

  std::vector<ost::conop::AminoAcid> aa;
  aa.push_back(ost::conop::OneLetterCodeToAminoAcid(prev_aa));
  if(aa.back() == ost::conop::XXX){
    throw promod3::Error("Only standard amino acids are allowed for setting up SoftSampler!");
  }
  for (uint i = 0; i < sequence_.size(); ++i) {
    aa.push_back(ost::conop::OneLetterCodeToAminoAcid(sequence_[i]));
    if (aa.back() == ost::conop::XXX) {
      throw promod3::Error("Only standard amino acids are allowed for setting up SoftSampler!");
    }
  }
  aa.push_back(ost::conop::OneLetterCodeToAminoAcid(next_aa));
  if (aa.back() == ost::conop::XXX) {
    throw promod3::Error("Only standard amino acids are allowed for setting up SoftSampler!");
  }

  for (uint i = 1; i < sequence_.size()+1; ++i) {
    dihedral_indices_.push_back(
      torsion_sampler_[i-1]->GetHistogramIndex(aa[i-1], aa[i], aa[i+1]));
  }
}


void SoftSampler::Initialize(loop::BackboneList& positions) {

  positions.clear();
  std::vector<Real> phi_angles;
  std::vector<Real> psi_angles;
  std::pair<Real,Real> actual_draw;
  for(uint i = 0; i < dihedral_indices_.size(); ++i){
    actual_draw = torsion_sampler_[i]->Draw(dihedral_indices_[i]);
    phi_angles.push_back(actual_draw.first);
    psi_angles.push_back(actual_draw.second);
  }
  loop::BackboneList bb_list(sequence_, phi_angles, psi_angles);
  positions = bb_list;
}

void SoftSampler::ProposeStep(const loop::BackboneList& actual_positions,
                              loop::BackboneList& proposal) {


  if (actual_positions.size() != sequence_.size()) {
    std::stringstream ss;
    ss << "Given positions must be consistent in size with the samplers ";
    ss << "sequence!";
    throw promod3::Error(ss.str());
  }

  proposal = actual_positions; 

  if(proposal.size() <= 1){
    return; // theres nothing to sample...
  }

  std::pair<Real,Real> old_dihedrals, new_dihedrals;
  Real old_prob, new_prob;
  int max_angle_index = 2 * sequence_.size() - 3;

  for (uint i = 0; i < 100; ++i) {

    int angle_to_modify = angle_selector_(rgen_);
    Real proposed_dev = zero_one_()*2*max_dev_-max_dev_;

    if (angle_to_modify == 0) {
      old_dihedrals.first = n_stem_phi_;
      old_dihedrals.second = proposal.GetPsiTorsion(0);

      new_dihedrals.first = n_stem_phi_;
      new_dihedrals.second = old_dihedrals.second + proposed_dev;

      old_prob = torsion_sampler_[0]->GetProbability(dihedral_indices_[0],old_dihedrals);
      new_prob = torsion_sampler_[0]->GetProbability(dihedral_indices_[0],new_dihedrals);

      if (new_prob/old_prob > zero_one_()) {
        proposal.SetPsiTorsion(0, new_dihedrals.second);
        break;
      }

    } else if (angle_to_modify == max_angle_index) {

      old_dihedrals.first = proposal.GetPhiTorsion(proposal.size()-1);
      old_dihedrals.second = c_stem_psi_;

      new_dihedrals.first = old_dihedrals.first + proposed_dev;
      new_dihedrals.second = c_stem_psi_;

      old_prob = torsion_sampler_.back()->GetProbability(dihedral_indices_.back(),old_dihedrals);
      new_prob = torsion_sampler_.back()->GetProbability(dihedral_indices_.back(),new_dihedrals);

      if (new_prob/old_prob > zero_one_()) {
        proposal.SetPhiTorsion(proposal.size()-1, new_dihedrals.first);
        break;      
      }

    } else {

      int bb_index = (angle_to_modify - 1) / 2 + 1;
      old_dihedrals.first = proposal.GetPhiTorsion(bb_index);
      old_dihedrals.second = proposal.GetPsiTorsion(bb_index);
      old_prob = torsion_sampler_[bb_index]->GetProbability(dihedral_indices_[bb_index],old_dihedrals);

      if ((angle_to_modify - 1) % 2 == 0) {
        //it's the phi angle of residue bb_index
        new_dihedrals.first = old_dihedrals.first + proposed_dev;
        new_dihedrals.second = old_dihedrals.second;

        new_prob = torsion_sampler_[bb_index]->GetProbability(dihedral_indices_[bb_index],new_dihedrals);

        if(new_prob/old_prob > zero_one_()){
          proposal.SetPhiTorsion(bb_index, new_dihedrals.first);
          break;      
        }
      } else {
        //it's the psi angle of residue bb_index
        new_dihedrals.first = old_dihedrals.first;
        new_dihedrals.second = old_dihedrals.second + proposed_dev;

        new_prob = torsion_sampler_[bb_index]->GetProbability(dihedral_indices_[bb_index],new_dihedrals);

        if (new_prob/old_prob > zero_one_()) {
          proposal.SetPsiTorsion(bb_index, new_dihedrals.second);
          break;      
        }
      }
    }
  }
}

FragmentSampler::FragmentSampler(const String& sequence,
                                 const loop::FraggerList& fraggers,
                                 const loop::BackboneList& init_bb_list,
                                 uint sampling_start_index,
                                 uint init_fragments, uint seed)
                                 : sequence_(sequence)
                                 , init_bb_list_(init_bb_list)
                                 , sampling_start_index_(sampling_start_index)
                                 , init_fragments_(init_fragments)
                                 , rgen_(seed) {

  // check for consistency of sequence and init_bb_list
  if (sequence_ != init_bb_list_.GetSequence()) {
    throw promod3::Error("Sequence and initial bb_list must be consistent!");
  }  

  // check, whether the fraggers are alright
  if (fraggers.empty()) {
    throw promod3::Error("Do you really think fragment sampling with empty fragment list works?");
  }

  for (loop::FraggerList::const_iterator i = fraggers.begin(); 
       i != fraggers.end(); ++i) {
    if ((*i)->empty()) {
      throw promod3::Error("Every fragger needs to contain at least some fragments!");
    }
  }

  uint frag_length = fraggers[0]->fragment_size();
  for (loop::FraggerList::const_iterator i = fraggers.begin(); 
       i != fraggers.end(); ++i) {
    if ((*i)->fragment_size() != frag_length) {
      throw promod3::Error("All fragments must be of equal size!");
    }
  }

  if (sampling_start_index + fraggers.size() - 1 + frag_length > sequence_.size())
  {
    throw promod3::Error("Fragments are not fully covered by sequence!");
  }

  for (uint i = 0; i < fraggers.size(); ++i) {
    if(fraggers[i]->fragment_seq() != sequence_.substr(sampling_start_index + i,frag_length)){
      throw promod3::Error("Fragment sequences must match the overall sequence!");
    }
  }

  // fill the random number generators
  position_selector_ = boost::random::uniform_int_distribution<>(0, fraggers.size()-1);
  for (loop::FraggerList::const_iterator i = fraggers.begin();
       i != fraggers.end(); ++i) {
    fragment_selector_.push_back(boost::random::uniform_int_distribution<>(0, (*i)->size()-1));
  }

  // finally fill the fraggers
  for (loop::FraggerList::const_iterator i = fraggers.begin();
       i != fraggers.end(); ++i){
    fraggers_.push_back(*i);
  }
}

void FragmentSampler::Initialize(loop::BackboneList& positions) {

  positions = init_bb_list_;

  for (uint i = 0; i < init_fragments_; ++i) {
    int pos = position_selector_(rgen_);
    int fragment_idx = fragment_selector_[pos](rgen_);
    positions.ReplaceFragment((*(fraggers_[pos]))[fragment_idx],
                              sampling_start_index_ + pos, true);
  }
}

void FragmentSampler::ProposeStep(const loop::BackboneList& actual_positions,
                                  loop::BackboneList& proposal) {
  proposal = actual_positions;
  int pos = position_selector_(rgen_);
  int fragment_idx = fragment_selector_[pos](rgen_);
  proposal.ReplaceFragment((*(fraggers_[pos]))[fragment_idx],
                           sampling_start_index_ + pos, true);
}

}}//ns
