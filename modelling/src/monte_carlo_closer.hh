// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_MONTE_CARLO_CLOSER_HH
#define PROMOD_MODELLING_MONTE_CARLO_CLOSER_HH

#include <vector>
#include <limits>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/shared_ptr.hpp>

#include <ost/mol/residue_handle.hh>
#include <ost/mol/atom_handle.hh>

#include <promod3/loop/backbone.hh>
#include <promod3/loop/torsion_sampler.hh>
#include <promod3/core/message.hh>
#include <promod3/modelling/ccd.hh>
#include <promod3/modelling/kic.hh>


namespace promod3 { namespace modelling {

class MonteCarloCloser;
class KICCloser;
class DirtyCCDCloser;
class CCDCloser;
class NTerminalCloser;
class CTerminalCloser;
class DeNovoCloser;

typedef boost::shared_ptr<MonteCarloCloser> MonteCarloCloserPtr;
typedef boost::shared_ptr<KICCloser> KICCloserPtr;
typedef boost::shared_ptr<DirtyCCDCloser> DirtyCCDCloserPtr;
typedef boost::shared_ptr<CCDCloser> CCDCloserPtr;
typedef boost::shared_ptr<NTerminalCloser> NTerminalCloserPtr;
typedef boost::shared_ptr<CTerminalCloser> CTerminalCloserPtr;
typedef boost::shared_ptr<DeNovoCloser> DeNovoCloserPtr;

class MonteCarloCloser {

public:
  virtual bool Close(const loop::BackboneList& actual_positions,
                     loop::BackboneList& closed_positions) = 0;

  virtual ~MonteCarloCloser() = 0;
};


class KICCloser : public MonteCarloCloser {

public:
  KICCloser(const ost::mol::ResidueHandle& n_stem,
            const ost::mol::ResidueHandle& c_stem, 
            uint seed);

  bool Close(const loop::BackboneList& actual_positions,
             loop::BackboneList& closed_positions);

private:

  void ShufflePivotResidues();

  uint backbone_size_;
  KIC closer_;

  //source of randomness
  boost::mt19937 rgen_;

  //stuff required to shuffle the pivot residues
  uint pivot_one_, pivot_two_, pivot_three_;
  std::vector<uint> possible_pivot_residues_;
  boost::uniform_int<> shuffle_generator_;
  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > shuffler_;
};


class DirtyCCDCloser : public MonteCarloCloser {
public:
  DirtyCCDCloser(const ost::mol::ResidueHandle& n_stem,
                 const ost::mol::ResidueHandle& c_stem);

  bool Close(const loop::BackboneList& actual_positions,
             loop::BackboneList& closed_positions);

private:
  int seed_;
  CCD closer_;
};


class CCDCloser : public MonteCarloCloser {
public:

  CCDCloser(const ost::mol::ResidueHandle& n_stem,
            const ost::mol::ResidueHandle& c_stem, const String& sequence,
            loop::TorsionSamplerPtr torsion_sampler, int seed);

  CCDCloser(const ost::mol::ResidueHandle& n_stem,
            const ost::mol::ResidueHandle& c_stem, const String& sequence,
            const loop::TorsionSamplerList& torsion_sampler, int seed);

  bool Close(const loop::BackboneList& actual_positions,
             loop::BackboneList& closed_positions);

private:
  CCD closer_;
};


class NTerminalCloser : public MonteCarloCloser {
public:
  NTerminalCloser(const ost::mol::ResidueHandle& c_stem);

  bool Close(const loop::BackboneList& actual_positions,
             loop::BackboneList& closed_positions);

private:
  loop::BackboneList c_stem_;
  ost::mol::ResidueHandle after_c_stem_;
};


class CTerminalCloser : public MonteCarloCloser {
public:
  CTerminalCloser(const ost::mol::ResidueHandle& n_stem);

  bool Close(const loop::BackboneList& actual_positions,
             loop::BackboneList& closed_positions);

private:
  loop::BackboneList n_stem_;
};


class DeNovoCloser : public MonteCarloCloser {
public:
  DeNovoCloser() { }

  bool Close(const loop::BackboneList& actual_positions,
             loop::BackboneList& closed_positions) {
    closed_positions = actual_positions;
    return true;
  }
};

}}//ns
#endif
