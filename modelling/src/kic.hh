// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_KIC_HH
#define PROMOD_MODELLING_KIC_HH

#include <ost/mol/residue_handle.hh>
#include <promod3/loop/backbone.hh>

namespace promod3 { namespace modelling {

class KIC;
typedef boost::shared_ptr<KIC> KICPtr;

class KIC{
public:
 KIC(const ost::mol::ResidueHandle& n_stem, const ost::mol::ResidueHandle& c_stem);

 void Close(const loop::BackboneList& bb_list, uint pivot_one, uint pivot_two,
            uint pivot_three, std::vector<loop::BackboneList>& solutions) const;

private:

  ost::mol::ResidueHandle n_stem_;
  ost::mol::ResidueHandle c_stem_;

};

}} //ns
#endif
