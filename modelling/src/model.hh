// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_MODELLING_MODEL_HH
#define PM3_MODELLING_MODEL_HH

#include <ost/seq/alignment_handle.hh>
#include <ost/seq/sequence_list.hh>

#include <ost/seq/profile_handle.hh>
#include <promod3/core/message.hh>
#include <promod3/loop/all_atom_env.hh>
#include <promod3/loop/psipred_prediction.hh>
#include <promod3/modelling/gap.hh>
#include <promod3/scoring/all_atom_overall_scorer.hh>
#include <promod3/scoring/backbone_overall_scorer.hh>
#include <promod3/scoring/backbone_score_base.hh>
#include <promod3/modelling/sidechain_reconstructor.hh>

namespace promod3 { namespace modelling {


struct ModellingHandle {

  ModellingHandle() {
    seqres = ost::seq::CreateSequenceList();
  }
  
  bool operator==(const ModellingHandle& rhs) const 
  {
    return model==rhs.model && gaps==rhs.gaps;
  }
  
  bool operator!=(const ModellingHandle& rhs) const
  {
    return !this->operator==(rhs);
  }

  ModellingHandle Copy() const;

  ost::mol::EntityHandle model;
  StructuralGapList      gaps;
  ost::seq::SequenceList seqres;
  promod3::loop::PsipredPredictionList psipred_predictions;
  ost::seq::ProfileHandleList profiles;
  promod3::scoring::BackboneScoreEnvPtr backbone_scorer_env;
  promod3::scoring::BackboneOverallScorerPtr backbone_scorer;
  promod3::loop::AllAtomEnvPtr all_atom_scorer_env; // tmp only for scoring
  promod3::scoring::AllAtomOverallScorerPtr all_atom_scorer;
  promod3::loop::AllAtomEnvPtr all_atom_sidechain_env;
  SidechainReconstructorPtr sidechain_reconstructor;
};

// see Python doc
int CountEnclosedGaps(const ModellingHandle& mhandle, const StructuralGap& gap,
                      bool insertions_only=false);

// see Python doc
int ClearGaps(ModellingHandle& mhandle, const StructuralGap& gap);

// see Python doc
void MergeGaps(ModellingHandle& mhandle, uint index);

// see Python doc
int RemoveTerminalGaps(ModellingHandle& mhandle);

// see Python doc
void ReorderGaps(ModellingHandle& mhandle);

// see Python doc
void SetupDefaultBackboneScoring(ModellingHandle& mhandle);
// see Python doc
inline bool IsBackboneScoringSetUp(const ModellingHandle& mhandle) {
  return bool(mhandle.backbone_scorer) && bool(mhandle.backbone_scorer_env);
}

// see Python doc
void SetupDefaultAllAtomScoring(ModellingHandle& mhandle);
// see Python doc
inline bool IsAllAtomScoringSetUp(const ModellingHandle& mhandle) {
  return    bool(mhandle.all_atom_scorer_env)
         && bool(mhandle.all_atom_scorer)
         && bool(mhandle.all_atom_sidechain_env)
         && bool(mhandle.sidechain_reconstructor);
}

// see Python doc
void SetSequenceProfiles(ModellingHandle& mhandle, 
                         const ost::seq::ProfileHandleList& profiles);

// see Python doc
void SetPsipredPredictions(ModellingHandle& mhandle,
                           const promod3::loop::PsipredPredictionList& 
                           psipred_predictions);

// see Python doc
void MergeMHandle(const ModellingHandle& source_mhandle,
                  ModellingHandle& target_mhandle,
                  uint source_chain_idx, uint target_chain_idx,
                  uint start_resnum, uint end_resnum,
                  const geom::Mat4& transform);

// see Python doc
void InsertLoop(ModellingHandle& mhandle, const loop::BackboneList& bb_list,
                uint start_resnum, uint chain_idx);
int InsertLoopClearGaps(ModellingHandle& mhandle,
                        const loop::BackboneList& bb_list,
                        const StructuralGap& gap);

/// \brief copies all atom of src_res to dst_res
/// \param has_cbeta will be set to true if the src_res has a cbeta and the 
//      dst_residue is not a glycine
bool CopyIdentical(ost::mol::ResidueView src_res,
                   ost::mol::ResidueHandle dst_res,
                   ost::mol::XCSEditor& edi,
                   bool& has_cbeta);

/// \brief copies atoms of src_res to dst_res
///
/// src_res and dst_res are thought to be conserved, e.g. the parent standard 
/// amino acid of both residues is the same. This includes cases where e.g. the 
/// src_rs is and MSE and the dst_res is a MET. This function automatically 
/// tries to do the right thing an keep as many atoms as possible from src_res
bool CopyConserved(ost::mol::ResidueView src_res,
                   ost::mol::ResidueHandle dst_res,
                   ost::mol::XCSEditor& edi,
                   bool& has_cbeta);

/// \brief construct dst_res in case src_res and dst_res are not conserved.
/// 
/// This essentially copies the backbone of src_res to dst_res. The CB atom is 
/// only copied if dst_res is not equal to glycine.
bool CopyNonConserved(ost::mol::ResidueView src_res,
                      ost::mol::ResidueHandle dst_res,
                      ost::mol::XCSEditor& edi,
                      bool& has_cbeta);

/// \brief construct dst_res from src_res when src_res is an MSE
bool CopyMSE(ost::mol::ResidueView src_res,
             ost::mol::ResidueHandle dst_res,
             ost::mol::XCSEditor& edi,
             bool& has_cbeta);

bool CopyModified(ost::mol::ResidueView src_res,
                  ost::mol::ResidueHandle dst_res,
                  ost::mol::XCSEditor& edi,
                  bool& has_cbeta);


/// \brief Build raw model from list of alignments.
/// 
/// Every item in the list represents a target chain and the second sequence of 
/// every aln must have a view attached.
/// 
/// Basic protein core modeling algorithm that copies backbone coordinates based
/// on the sequence alignment. For matching residues, the sidechain coordinates 
/// are also copied. Gaps are ignored.
/// 
/// Residue numbers are set such that missing residue in gaps are honored and 
/// subsequent loop modeling can insert new residues without having to 
/// renumber.
///
/// The returned ModellingHandle stores information about insertions and 
/// deletions in the gaps list.
///
/// Hydrogen an deuterium atoms are not copied into the model
///
/// Notes:
/// - this is used in SWISS-MODEL and hence, legacy code must be preserved.
/// - this assumes that a default OST compounds library is set (with
///   Conopology::Instance().SetDefaultLib) as is done by default for
///   Python imports (see core __init__.py)
ModellingHandle
MHandleFromAln(const ost::seq::AlignmentList& aln, 
               const std::vector<String>& chain_names,
               bool include_ligands=false,
               bool spdbv_style=false);

/// \brief get called by MHandleFromAln, adds one single chain.
void AddRawChain(const ost::seq::AlignmentHandle& aln,
                 ost::mol::XCSEditor& edi,
                 StructuralGapList& gap_list,
                 const String& chain_name,
                 bool spdbv_style=false);

/// \brief handles ligands and gets called by MHandleFromAln if add_ligand flag 
///    is true.
void AddLigands(const ost::seq::AlignmentList& aln,
                ost::mol::XCSEditor& edi,
                ost::mol::EntityHandle& model);

}}

#endif
