// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_MODELLING_GAP_EXTENDER_HH
#define PM3_MODELLING_GAP_EXTENDER_HH

#include <ost/mol/mol.hh>
#include <promod3/core/message.hh>
#include <promod3/modelling/gap.hh>
#include <promod3/modelling/extension_scheme.hh>
#include <ost/seq/sequence_handle.hh>
#include <algorithm>

namespace promod3 { namespace modelling {

class BaseGapExtender;
class GapExtender;
class FullGapExtender;
class ScoringGapExtender;

typedef boost::shared_ptr<BaseGapExtender> BaseGapExtenderPtr;
typedef boost::shared_ptr<GapExtender> GapExtenderPtr;
typedef boost::shared_ptr<FullGapExtender> FullGapExtenderPtr;
typedef boost::shared_ptr<ScoringGapExtender> ScoringGapExtenderPtr;

/// \brief Base class for all gap extenders.
class BaseGapExtender {
public:
  // ensure the right destructor is called
  virtual ~BaseGapExtender() { }
  /// \returns False, if the *gap* cannot be extended any further.
  virtual bool Extend() = 0;
};

/// \brief Helper class for gap extension. 
/// 
/// The extender cycles through the following 
/// steps:
///       -
///      --
///       --
///     ---
///      ---
///       ---
///    ----
///     ----
///      ----
///       ----
///
/// An exception will be raised if a terminal gap is used to construct this.
class GapExtender: public BaseGapExtender {
public:
  GapExtender(StructuralGap& g,
              const String& seqres)
              : gap(g)
              , seqres_(seqres)
              , scheme_(gap.before.GetNumber().GetNum(),
                        gap.after.GetNumber().GetNum()) { }

  GapExtender(StructuralGap& g,
              const ost::seq::SequenceHandle seqres)
              : gap(g)
              , seqres_(seqres.GetString())
              , scheme_(gap.before.GetNumber().GetNum(),
                        gap.after.GetNumber().GetNum()) { }

  /// \brief Tries to extend *gap*.
  /// \returns False, if the *gap* cannot be extended any further. This happens
  ///          if it reaches a terminal or another insertion gap.
  ///          Otherwise, the *gap* passed to the constructor is changed.
  ///          The gaps are extended with ascending length and will always have
  ///          valid termini.
  bool Extend();
  StructuralGap& gap;
private:
  String seqres_;
  ShiftExtension scheme_;
};


/// \brief Cycles as GapExtender, but continues even if another gap was encountered.
///
/// The max. gap-length (w/o termini) can be controlled with the optional
/// max_length parameter.
///
/// An exception will be raised if a terminal gap is used to construct this.
class FullGapExtender: public BaseGapExtender {
public:
  // max_length = -1: get max. gap length from seqres
  FullGapExtender(StructuralGap& g,
                  const String& seqres,
                  const int max_length=-1);
  FullGapExtender(StructuralGap& g,
                  const ost::seq::SequenceHandle seqres,
                  const int max_length=-1);

  /// \brief Tries to extend *gap*.
  /// \returns False, if the *gap* cannot be extended without exceeding *max_length*.
  ///          Otherwise, the *gap* passed to the constructor is changed.
  ///          The gaps are extended with ascending length and will always have
  ///          valid termini.
  bool Extend();
  StructuralGap& gap;
private:
  uint max_length_;
  String seqres_;
  ShiftExtension scheme_;
  void SetMaxLength_(const int max_length);
};

/// \brief Gap extender with penalties for (e.g. conserved) regions of sequence.
///
/// The extender scores possible gap extensions and returns them in order of
/// their score when :meth:`Extend` is called.
/// The score is penalized according to length and according to certain (well
/// conserved) regions in the structure as defined by *penalties*.
/// score = num_gap_extensions * `extension_penalty` + sum( `penalties` [i] )
/// (i = resnum - 1 of residues in extension)
///
/// An exception will be raised if a terminal gap is used to construct this.
class ScoringGapExtender: public BaseGapExtender {
public:
  // max_length = -2: use GapExtender, else use FullGapExtender and pass max_length
  ScoringGapExtender(StructuralGap& g, Real extension_penalty,
                     const std::vector<Real>& penalties,
                     const String& seqres,
                     const int max_length=-2);

  ScoringGapExtender(StructuralGap& g, Real extension_penalty,
                     const std::vector<Real>& penalties,
                     const ost::seq::SequenceHandle& seqres,
                     const int max_length=-2);

  /// \brief Tries to extend *gap*.
  /// \returns False, if the *gap* cannot be extended any further.
  ///          Otherwise, the *gap* passed to the constructor is changed.
  ///          The gaps are extended with ascending score and will always have
  ///          valid termini.
  bool Extend();
  StructuralGap& gap;
private:

  void Init(Real extension_penalty, const std::vector<Real>& penalties,
            const String& seqres, const int max_length);

  uint actual_gap_index_;
  std::vector<StructuralGap> gaps_;
};


}}//ns

#endif
