// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_MONTE_CARLO_SCORER_HH
#define PROMOD_MODELLING_MONTE_CARLO_SCORER_HH

#include <vector>
#include <map>

#include <ost/mol/mol.hh>

#include <promod3/scoring/backbone_overall_scorer.hh>

namespace promod3 { namespace modelling {

class MonteCarloScorer;
class LinearScorer;

typedef boost::shared_ptr<MonteCarloScorer> MonteCarloScorerPtr;
typedef boost::shared_ptr<LinearScorer> LinearScorerPtr;

class MonteCarloScorer {
public:
  virtual Real GetScore(const loop::BackboneList& positions) = 0;
  virtual ~MonteCarloScorer() = 0;
};

class LinearScorer : public MonteCarloScorer {

public:
  LinearScorer(scoring::BackboneOverallScorerPtr scorer, 
               scoring::BackboneScoreEnvPtr scorer_env,
               uint start_resnum, uint num_residues,
               uint chain_index, const std::map<String,Real>& weights);

  Real GetScore(const loop::BackboneList& positions);

  void StashScoringEnv() { scorer_env_->Stash(start_resnum_, num_residues_, 
                                              chain_index_); }

  void PopScoringEnv() { scorer_env_->Pop(); }

private:
  scoring::BackboneOverallScorerPtr scorer_;
  scoring::BackboneScoreEnvPtr scorer_env_;
  uint start_resnum_;
  uint num_residues_;
  uint chain_index_;
  std::vector<std::pair<Real, scoring::BackboneScorerPtr> > w_scorers_;
};

}}

#endif

