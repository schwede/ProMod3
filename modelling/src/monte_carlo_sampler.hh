// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_MONTE_CARLO_SAMPLER_HH
#define PROMOD_MODELLING_MONTE_CARLO_SAMPLER_HH

#include <vector>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/shared_ptr.hpp>

#include <ost/mol/residue_handle.hh>
#include <ost/mol/atom_handle.hh>

#include <promod3/core/message.hh>
#include <promod3/loop/backbone.hh>
#include <promod3/loop/torsion_sampler.hh>
#include <promod3/loop/fragger.hh>

namespace promod3 { namespace modelling {

class MonteCarloSampler;
class PhiPsiSampler;
class SoftSampler;
class FragmentSampler;

typedef boost::shared_ptr<MonteCarloSampler> MonteCarloSamplerPtr;
typedef boost::shared_ptr<PhiPsiSampler> PhiPsiSamplerPtr;
typedef boost::shared_ptr<SoftSampler> SoftSamplerPtr;
typedef boost::shared_ptr<FragmentSampler> FragmentSamplerPtr;

class MonteCarloSampler{

public:

  virtual void Initialize(loop::BackboneList& positions) = 0;
  
  virtual void ProposeStep(const loop::BackboneList& actual_positions,
                           loop::BackboneList& proposal) = 0;
  virtual ~MonteCarloSampler() = 0;

};

class PhiPsiSampler : public MonteCarloSampler{

public:
  PhiPsiSampler(const String& sequence, loop::TorsionSamplerPtr torsion_sampler, 
                Real n_stem_phi, Real c_stem_psi, 
                char prev_aa, char next_aa, uint seed);

  PhiPsiSampler(const String& sequence, loop::TorsionSamplerList torsion_sampler,
                Real n_stem_phi, Real c_stem_psi, 
                char prev_aa, char next_aa, uint seed);
             
  void Initialize(loop::BackboneList& positions);

  void ProposeStep(const loop::BackboneList& actual_positions,
                   loop::BackboneList& proposal);

private:

  String sequence_;
  loop::TorsionSamplerList torsion_sampler_;
  std::vector<uint> dihedral_indices_;

  //source of randomness
  boost::mt19937 rgen_;

  //non modifiable torsion angles of stem residues
  Real n_stem_phi_, c_stem_psi_;

  //distribution to select the angle to modify
  boost::random::uniform_int_distribution<> angle_selector_;
};

class SoftSampler : public MonteCarloSampler{
public:

  SoftSampler(const String& sequence, loop::TorsionSamplerPtr torsion_sampler,
              Real max_dev, Real n_stem_phi, Real c_stem_psi, 
              char prev_aa, char next_aa, uint seed);

  SoftSampler(const String& sequence, loop::TorsionSamplerList torsion_sampler,
              Real max_dev, Real n_stem_phi, Real c_stem_psi, 
              char prev_aa, char next_aa, uint seed);

  void Initialize(loop::BackboneList& positions);

  void ProposeStep(const loop::BackboneList& actual_positions,
                   loop::BackboneList& proposal);

private:

  String sequence_;
  loop::TorsionSamplerList torsion_sampler_;
  std::vector<uint> dihedral_indices_;
  Real max_dev_;

  // source of randomness
  boost::mt19937 rgen_;
  // non modifiable torsion angles of stem residues
  Real n_stem_phi_, c_stem_psi_;
  // distribution to select the angle to modify
  boost::random::uniform_int_distribution<> angle_selector_;
  // distribution to accept/reject new angle proposal
  boost::uniform_01<boost::mt19937&> zero_one_;
};


class FragmentSampler : public MonteCarloSampler{

public:

  FragmentSampler(const String& sequence, const loop::FraggerList& fraggers,
                  const loop::BackboneList& init_bb_list,
                  uint sampling_start_index, uint init_fragments, uint seed);

  void Initialize(loop::BackboneList& positions);

  void ProposeStep(const loop::BackboneList& actual_positions,
                   loop::BackboneList& proposal);


private:

  String sequence_;
  loop::BackboneList init_bb_list_;
  uint sampling_start_index_;
  uint init_fragments_;
  boost::mt19937 rgen_;
  boost::random::uniform_int_distribution<> position_selector_;
  std::vector<boost::random::uniform_int_distribution<> > fragment_selector_;
  loop::FraggerList fraggers_;
};


}}//ns
#endif

