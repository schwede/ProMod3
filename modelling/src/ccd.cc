// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/ccd.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/core/geom_base.hh>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/uniform_int.hpp>

namespace promod3 { namespace modelling {

///////////////////////////////////////////////////////////////////////////////
// HELPERS (here to be inlinable)
namespace {

void SuperposeOntoNStem(loop::BackboneList& bb_list, 
                        const ost::mol::ResidueHandle& n_stem){
  geom::Mat4 t = bb_list.GetTransform(0, n_stem);
  bb_list.ApplyTransform(t);
}

void FitCStem(const geom::Vec3* target_positions, loop::BackboneList& bb_list) {

  Real after_n_ca_dist = geom::Length(target_positions[1]-target_positions[0]);
  Real after_ca_c_dist = geom::Length(target_positions[2]-target_positions[1]);
  Real after_n_ca_c_angle = geom::Angle(target_positions[0]-target_positions[1],
                                        target_positions[2]-target_positions[1]);
  const uint last_idx = bb_list.size() - 1;
  const geom::Vec3 n_pos = bb_list.GetN(last_idx);
  geom::Vec3 bb_list_n_ca_vec = geom::Normalize(bb_list.GetCA(last_idx)-n_pos);
  Real bb_list_last_dihedral = bb_list.GetPhiTorsion(last_idx);
  const geom::Vec3 new_ca_pos = n_pos + after_n_ca_dist * bb_list_n_ca_vec;
  bb_list.SetCA(last_idx, new_ca_pos);
  geom::Vec3 new_c_pos;
  core::ConstructAtomPos(bb_list.GetC(last_idx - 1), n_pos, new_ca_pos,
                         after_ca_c_dist, after_n_ca_c_angle,
                         bb_list_last_dihedral, new_c_pos);
  bb_list.SetC(last_idx, new_c_pos);
}

inline Real GetTargetMSD(const loop::BackboneList& bb_list,
                         const geom::Vec3* target) {
  Real msd = 0.0;
  const uint last_idx = bb_list.size() - 1;
  msd += geom::Length2(bb_list.GetN(last_idx) - target[0]);
  msd += geom::Length2(bb_list.GetCA(last_idx) - target[1]);
  msd += geom::Length2(bb_list.GetC(last_idx) - target[2]);
  return msd/3.0;
}

Real GetRotationAngle(const geom::Vec3* actual_pos,
                      const geom::Vec3& anchor, const geom::Vec3& rot_bond,
                      const geom::Vec3* target_pos) {
  // readable version:
  // geom::Vec3 normalized_rot_bond = geom::Normalize(rot_bond);
  // geom::Vec3 o1 = geom::Dot(actual_pos[0] - anchor, normalized_rot_bond) * normalized_rot_bond + anchor;
  // geom::Vec3 o2 = geom::Dot(actual_pos[1] - anchor, normalized_rot_bond) * normalized_rot_bond + anchor;
  // geom::Vec3 o3 = geom::Dot(actual_pos[2] - anchor, normalized_rot_bond) * normalized_rot_bond + anchor;
  // geom::Vec3 f1 = target_pos[0] - o1;
  // geom::Vec3 f2 = target_pos[1] - o2;
  // geom::Vec3 f3 = target_pos[2] - o3;
  // geom::Vec3 r1 = actual_pos[0] - o1;
  // geom::Vec3 r2 = actual_pos[1] - o2;
  // geom::Vec3 r3 = actual_pos[2] - o3;
  // geom::Vec3 s1 = geom::Normalize(geom::Cross(normalized_rot_bond, r1));
  // geom::Vec3 s2 = geom::Normalize(geom::Cross(normalized_rot_bond, r2));
  // geom::Vec3 s3 = geom::Normalize(geom::Cross(normalized_rot_bond, r3));
  // Real b = geom::Dot(f1,r1) + geom::Dot(f2,r2) + geom::Dot(f3,r3);
  // Real c = geom::Dot(f1,s1)*geom::Length(r1)
  //        + geom::Dot(f2,s2)*geom::Length(r2)
  //        + geom::Dot(f3,s3)*geom::Length(r3);

  // same but faster... ;-)
  Real rbl2 = geom::Length2(rot_bond);
  Real rbl2_inv = (rbl2 == 0) ? 0 : 1 / rbl2;
  Real o1sc = geom::Dot(actual_pos[0] - anchor, rot_bond) * rbl2_inv;
  Real o2sc = geom::Dot(actual_pos[1] - anchor, rot_bond) * rbl2_inv;
  Real o3sc = geom::Dot(actual_pos[2] - anchor, rot_bond) * rbl2_inv;
  geom::Vec3 o1 = o1sc * rot_bond + anchor;
  geom::Vec3 o2 = o2sc * rot_bond + anchor;
  geom::Vec3 o3 = o3sc * rot_bond + anchor;
  geom::Vec3 f1 = target_pos[0] - o1;
  geom::Vec3 f2 = target_pos[1] - o2;
  geom::Vec3 f3 = target_pos[2] - o3;
  geom::Vec3 r1 = actual_pos[0] - o1;
  geom::Vec3 r2 = actual_pos[1] - o2;
  geom::Vec3 r3 = actual_pos[2] - o3;
  geom::Vec3 s1 = geom::Cross(rot_bond, r1);
  geom::Vec3 s2 = geom::Cross(rot_bond, r2);
  geom::Vec3 s3 = geom::Cross(rot_bond, r3);
  Real s1l2 = geom::Length2(s1);
  Real s2l2 = geom::Length2(s2);
  Real s3l2 = geom::Length2(s3);
  Real c1sc = (s1l2 == 0) ? 0 : std::sqrt(geom::Length2(r1) / s1l2);
  Real c2sc = (s2l2 == 0) ? 0 : std::sqrt(geom::Length2(r2) / s2l2);
  Real c3sc = (s3l2 == 0) ? 0 : std::sqrt(geom::Length2(r3) / s3l2);
  Real b = geom::Dot(f1, r1) + geom::Dot(f2, r2) + geom::Dot(f3, r3);
  Real c = geom::Dot(f1, s1) * c1sc + geom::Dot(f2, s2) * c2sc
           + geom::Dot(f3, s3) * c3sc;
  return std::atan2(c, b);
}

/////////////////////////////
// CCD operations on Vec3*
inline void ApplyTransform(geom::Vec3 * pos_ptr, const geom::Vec3 * pos_end,
                           const geom::Mat4& t) {
  // copied from Backbone
  Real a, b, c;
  while (pos_ptr < pos_end) {
    // benmchmarks show, that this is much faster than writing something like
    // (*pos_ptr) = geom::Vec3(t*(*pos_ptr));
    a = t(0,0)*(*pos_ptr)[0]+t(0,1)*(*pos_ptr)[1]+t(0,2)*(*pos_ptr)[2]+t(0,3);
    b = t(1,0)*(*pos_ptr)[0]+t(1,1)*(*pos_ptr)[1]+t(1,2)*(*pos_ptr)[2]+t(1,3);
    c = t(2,0)*(*pos_ptr)[0]+t(2,1)*(*pos_ptr)[1]+t(2,2)*(*pos_ptr)[2]+t(2,3);
    (*pos_ptr)[0] = a;
    (*pos_ptr)[1] = b;
    (*pos_ptr)[2] = c;
    ++pos_ptr;
  }
}

inline void ApplyTransform(geom::Vec3 * pos_ptr, uint num_pos,
                           const geom::Mat4& t) {
  ApplyTransform(pos_ptr, pos_ptr + num_pos, t);
}

inline Real GetTargetMSD(const geom::Vec3* actual_positions,
                         const geom::Vec3* target) {
  Real msd = geom::Length2(actual_positions[0] - target[0])
           + geom::Length2(actual_positions[1] - target[1])
           + geom::Length2(actual_positions[2] - target[2]);
  return msd / Real(3);
}

// assume 4 succesive positions needed
inline Real GetDihedral(const geom::Vec3 * pos_ptr) {
  return geom::DihedralAngle(pos_ptr[0], pos_ptr[1], pos_ptr[2], pos_ptr[3]);
}
// Phi = C-1, N, CA, C
inline Real GetPhiTorsion(const geom::Vec3 * positions, const uint res_index) {
  return GetDihedral(positions + 3*res_index - 1);
}
// Psi = N, CA, C, N+1
inline Real GetPsiTorsion(const geom::Vec3 * positions, const uint res_index) {
  return GetDihedral(positions + 3*res_index);
}

} // anon ns

///////////////////////////////////////////////////////////////////////////////
// CCD public
CCD::CCD(const String& sequence, const ost::mol::ResidueHandle& n_stem, 
         const ost::mol::ResidueHandle& c_stem,
         loop::TorsionSamplerPtr torsion_sampler,
         uint max_steps, Real rmsd_cutoff, int random_seed) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CCD::CCD", 2);
  if(sequence.empty()){
    throw promod3::Error("Cannot initialize CCD with empty sequence!");
  }

  SetStems_(n_stem, c_stem);
  torsion_sampler_.assign(sequence.size(),torsion_sampler);
  SetTargetPositions_(c_stem);
  SetTorsionParametrization_(n_stem, c_stem, sequence);
  max_steps_ = max_steps;
  rmsd_cutoff_ = rmsd_cutoff;
  constrained_ = true;
  random_seed_ = random_seed;
}

CCD::CCD(const String& sequence, const ost::mol::ResidueHandle& n_stem, 
         const ost::mol::ResidueHandle& c_stem,
         const loop::TorsionSamplerList& torsion_sampler,
         uint max_steps, Real rmsd_cutoff, int random_seed) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CCD::CCD", 2);

  if(sequence.empty()){
    throw promod3::Error("Cannot initialize CCD with empty sequence!");
  }

  if (torsion_sampler.size() != sequence.size()) {
    throw promod3::Error("Number of provided torsion samplers must be "
                         "consistent with provided sequence!");
  }

  SetTargetPositions_(c_stem);
  SetTorsionParametrization_(n_stem, c_stem, sequence);
  max_steps_ = max_steps;
  rmsd_cutoff_ = rmsd_cutoff;
  SetStems_(n_stem, c_stem);
  constrained_ = true;
  random_seed_ = random_seed;
}

CCD::CCD(const ost::mol::ResidueHandle& n_stem,
         const ost::mol::ResidueHandle& c_stem, 
         uint max_steps, Real rmsd_cutoff) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CCD::CCD", 2);

  SetTargetPositions_(c_stem);
  max_steps_ = max_steps;
  rmsd_cutoff_ = rmsd_cutoff;
  SetStems_(n_stem, c_stem);
  constrained_ = false;
  random_seed_ = 0;
}

bool CCD::Close(loop::BackboneList& bb_list) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CCD::Close", 2);
  // check
  const uint num_residues = bb_list.size();
  if (constrained_ && num_residues != torsion_sampler_.size()) {
    throw promod3::Error("Size of BackboneList must be consistent with the "
                         "sequence you initialized the CCD closer with!");
  }

  if(bb_list.empty()){
    throw promod3::Error("Cannot close empty BackboneList in CCD!");
  }

  // prepare list and target
  SuperposeOntoNStem(bb_list, n_stem_);

  if(bb_list.size() == 1){
    bool converged = GetTargetMSD(bb_list, target_positions_);
    if(converged){
      bb_list.ReconstructCStemOxygen(after_c_stem_);
    }
    return converged; // there is not more to do... 
  }

  FitCStem(target_positions_, bb_list);
  // allocate memory
  std::vector<geom::Vec3> positions(3*num_residues);
  std::vector<Real> rot_phi(num_residues, 0);
  std::vector<Real> rot_psi(num_residues, 0);
  // copy positions: N, CA, C only needed
  for (uint res_index = 0; res_index < num_residues; ++res_index) {
    positions[3*res_index]     = bb_list.GetN(res_index);
    positions[3*res_index + 1] = bb_list.GetCA(res_index);
    positions[3*res_index + 2] = bb_list.GetC(res_index);
  }
  // get rotations
  bool converged;
  if (constrained_) {
    converged = GetConstrainedCloseRotations_(&positions[0], &rot_phi[0],
                                              &rot_psi[0], num_residues);
  } else {
    converged = GetSimpleCloseRotations_(&positions[0], &rot_phi[0],
                                         &rot_psi[0], num_residues);
  }
  // apply rotations (note: illegal rot. are set to 0!)
  for (uint res_index = 0; res_index < bb_list.size(); ++res_index) {
    const bool apply_phi = std::abs(rot_phi[res_index]) > Real(1e-6);
    const bool apply_psi = std::abs(rot_psi[res_index]) > Real(1e-6);
    if (apply_phi && apply_psi) {
      bb_list.RotateAroundPhiPsiTorsion(res_index, rot_phi[res_index],
                                        rot_psi[res_index], true);
    } else if (apply_psi) {
      bb_list.RotateAroundPsiTorsion(res_index, rot_psi[res_index], true);
    } else if (apply_phi) {
      bb_list.RotateAroundPhiTorsion(res_index, rot_phi[res_index], true);
    }
  }
  // fix oxygen
  bb_list.ReconstructCStemOxygen(after_c_stem_);
  // did we converge?
  return converged;
}

///////////////////////////////////////////////////////////////////////////////
// CCD private closers
bool CCD::GetSimpleCloseRotations_(geom::Vec3* positions, Real* rot_phi,
                                   Real* rot_psi, const uint num_residues)
{
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CCD::GetSimpleCloseRotations", 2);

  // initialize
  const Real squared_cutoff = rmsd_cutoff_ * rmsd_cutoff_;
  const geom::Vec3 * positions_end = positions + 3*num_residues;
  geom::Vec3 * actual_positions = positions + 3*(num_residues-1);
  Real d_phi, d_psi;
  geom::Vec3 anchor, rotation_bond;
  bool apply_phi, apply_psi;
  geom::Mat4 t_psi, t_phi;

  // iterate
  for (uint counter = 0; counter < max_steps_; ++counter) {

    // handle res_index = 0
    rotation_bond = positions[2] - positions[1]; // C-CA
    d_psi = GetRotationAngle(actual_positions, positions[2],
                             rotation_bond, target_positions_);
    if (std::abs(d_psi) > Real(1e-6)) {
      t_psi = core::RotationAroundLine(rotation_bond, positions[2], d_psi);
      ApplyTransform(positions + 3, positions_end, t_psi);
      rot_psi[0] += d_psi;
    }

    // handle central residues
    for (uint res_index = 1; res_index < num_residues-1; ++res_index) {
      anchor = positions[3*res_index + 2];
      rotation_bond = anchor - positions[3*res_index + 1]; // C-CA
      d_psi = GetRotationAngle(actual_positions, anchor, rotation_bond,
                               target_positions_);
      apply_psi = std::abs(d_psi) > Real(1e-6);

      // shall we pre-rotate actual_positions?
      // -> observations showed that this starts being faster for 12 residues
      if (num_residues >= 12) {
        if (apply_psi) {
          t_psi = core::RotationAroundLine(rotation_bond, anchor, d_psi);
          ApplyTransform(actual_positions, positions_end, t_psi);
        }

        anchor = positions[3*res_index + 1];
        rotation_bond = anchor - positions[3*res_index]; // CA-N
        d_phi = GetRotationAngle(actual_positions, anchor, rotation_bond,
                                 target_positions_);
        apply_phi = std::abs(d_phi) > Real(1e-6); 

        if (apply_phi && apply_psi) {
          t_phi = core::RotationAroundLine(rotation_bond, anchor, d_phi);
          // transform C and actual_positions just by phi, all next ones by both
          ApplyTransform(positions + 3*res_index + 2, 1, t_phi);
          ApplyTransform(actual_positions, positions_end, t_phi);
          ApplyTransform(positions + 3*res_index + 3, actual_positions,
                         t_phi * t_psi);
          rot_psi[res_index] += d_psi;
          rot_phi[res_index] += d_phi;
        } else if (apply_psi) {
          // actual_positions already done!
          ApplyTransform(positions + 3*res_index + 3, actual_positions, t_psi);
          rot_psi[res_index] += d_psi;
        } else if (apply_phi) {
          t_phi = core::RotationAroundLine(rotation_bond, anchor, d_phi);
          ApplyTransform(positions + 3*res_index + 2, positions_end, t_phi);
          rot_phi[res_index] += d_phi;
        }
      } else {
        if (apply_psi) {
          t_psi = core::RotationAroundLine(rotation_bond, anchor, d_psi);
          ApplyTransform(positions + 3*res_index + 3, positions_end, t_psi);
          rot_psi[res_index] += d_psi;
        }

        anchor = positions[3*res_index + 1];
        rotation_bond = anchor - positions[3*res_index]; // CA-N
        d_phi = GetRotationAngle(actual_positions, anchor, rotation_bond,
                                 target_positions_);
        apply_phi = std::abs(d_phi) > Real(1e-6);

        if (apply_phi) {
          t_phi = core::RotationAroundLine(rotation_bond, anchor, d_phi);
          ApplyTransform(positions + 3*res_index + 2, positions_end, t_phi);
          rot_phi[res_index] += d_phi;
        }
      }
    }

    // handle last residue
    rotation_bond = actual_positions[1] - actual_positions[0];
    d_phi = GetRotationAngle(actual_positions, actual_positions[1],
                             rotation_bond, target_positions_);
    if (std::abs(d_phi) > Real(1e-6)) {
      // only need to rotate C
      t_phi = core::RotationAroundLine(rotation_bond, actual_positions[1], d_phi);
      ApplyTransform(actual_positions + 2, 1, t_phi);
      rot_phi[num_residues-1] += d_phi;
    }

    if (GetTargetMSD(actual_positions, target_positions_) < squared_cutoff) {
      return true;
    } 
  } 
  return false;
}

bool CCD::GetConstrainedCloseRotations_(geom::Vec3* positions, Real* rot_phi,
                                        Real* rot_psi, const uint num_residues)
{
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "CCD::GetConstrainedCloseRotations", 2);

  // initialize
  const Real squared_cutoff = rmsd_cutoff_ * rmsd_cutoff_;
  const uint last_idx = num_residues - 1;
  const geom::Vec3 * positions_end = positions + 3*num_residues;
  geom::Vec3 * actual_positions = positions + 3*last_idx;
  // fresh RNG each time (ensures reproducibility!)
  boost::mt19937 rng(random_seed_);
  boost::uniform_01<boost::mt19937&> zeroone(rng);

  // variables for all the math below
  Real d_phi, d_psi, prob_ratio;
  geom::Vec3 updated_actual_positions[3];
  geom::Vec3 anchor, rotation_bond;
  std::pair<Real,Real> dihedral_pair; 
  Real new_prob = 0;
  geom::Mat4 t_psi, t_phi;

  // caching for angles and probabilities (new/old)
  // -> useful as iteration can get stuck in local minima
  std::vector<Real> last_d_phi(num_residues);
  std::vector<Real> last_d_psi(num_residues);
  std::vector<Real> last_prob_ratio(num_residues);
  int last_rot_idx = 0;

  // do bookkeeping of dihedral pairs and fill them with initial angles
  std::vector<std::pair<Real,Real> > dihedral_pairs(num_residues);
  dihedral_pairs[0].first = n_stem_phi_;
  dihedral_pairs[0].second = GetPsiTorsion(positions, 0);
  for (uint i = 1; i < num_residues - 1; ++i) {
    dihedral_pairs[i].first = GetPhiTorsion(positions, i);
    dihedral_pairs[i].second = GetPsiTorsion(positions, i);
  }
  dihedral_pairs.back().first = GetPhiTorsion(positions, last_idx);
  dihedral_pairs.back().second = c_stem_psi_;
  // do bookkeeping of probabilities
  // we put lower bound on last prob.
  // it could be, that the input already is in the disallowed Ramachandran
  // region => p_new/0 = nan... assigning it a really low value, makes a
  // move into the allowed region possible
  const Real min_p = 0.00001;
  std::vector<Real> last_prob(num_residues);
  for (uint i = 0; i < num_residues; ++i) {
    last_prob[i] = std::max(GetProbability(dihedral_pairs[i], i), min_p);
  }

  // iterate
  for (uint counter = 0; counter < max_steps_; ++counter) {
    uint counter_rot_idx = counter * num_residues;
    // parse residues
    for (uint res_index = 0; res_index < num_residues; ++res_index) {
      // has anything changed since we were here last?
      const bool has_changed =
        last_rot_idx >= int(counter_rot_idx + res_index - num_residues);
      if (has_changed) {
        
        // get new d_phi, d_psi
        d_phi = 0.0;
        d_psi = 0.0;

        if (res_index < last_idx) {
          // get d_psi
          anchor = positions[3*res_index + 2];
          rotation_bond = anchor - positions[3*res_index + 1]; // C-CA
          d_psi = GetRotationAngle(actual_positions, anchor, rotation_bond,
                                   target_positions_);
        }

        if (res_index > 0) {
          // preview change of actual_positions?
          if (std::abs(d_psi) > Real(1e-6)) {
            // we still have anchor and rotation_bond from above
            updated_actual_positions[0] = actual_positions[0];
            updated_actual_positions[1] = actual_positions[1];
            updated_actual_positions[2] = actual_positions[2];
            t_psi = core::RotationAroundLine(rotation_bond, anchor, d_psi);
            ApplyTransform(updated_actual_positions, 3, t_psi);
            anchor = positions[3*res_index + 1];
            rotation_bond = anchor - positions[3*res_index]; // CA-N
            d_phi = GetRotationAngle(updated_actual_positions, anchor, 
                                     rotation_bond, target_positions_);
          } else {
            anchor = positions[3*res_index + 1];
            rotation_bond = anchor - positions[3*res_index]; // CA-N
            d_phi = GetRotationAngle(actual_positions, anchor, rotation_bond,
                                     target_positions_);
          }
        }

        // check probabilities for new change
        dihedral_pair = dihedral_pairs[res_index];
        dihedral_pair.first += d_phi;
        dihedral_pair.second += d_psi;
        new_prob = GetProbability(dihedral_pair, res_index);
        prob_ratio = new_prob / last_prob[res_index];

        // update cache
        last_d_phi[res_index] = d_phi;
        last_d_psi[res_index] = d_psi;
        last_prob_ratio[res_index] = prob_ratio;
      } else {
        // get from cache
        d_phi = last_d_phi[res_index];
        d_psi = last_d_psi[res_index];
        prob_ratio = last_prob_ratio[res_index];
      }

      // rotate?
      if (prob_ratio > zeroone()) {
        const bool apply_phi = (res_index != 0 && std::abs(d_phi) > Real(1e-6));
        const bool apply_psi = (res_index != last_idx &&
                                std::abs(d_psi) > Real(1e-6));

        // compute transforms and update caches
        if (apply_phi) {
          // NOTE: check code flow carefully before changing this!
          if (!has_changed) {
            anchor = positions[3*res_index + 1];
            rotation_bond = anchor - positions[3*res_index]; // CA-N
          }
          t_phi = core::RotationAroundLine(rotation_bond, anchor, d_phi);
          // cache update
          rot_phi[res_index] += d_phi;
          dihedral_pairs[res_index].first += d_phi;
          last_rot_idx = counter_rot_idx + res_index;
          if (!has_changed) {
            new_prob = GetProbability(dihedral_pairs[res_index], res_index);
          }
          last_prob[res_index] = std::max(new_prob, min_p);
        }
        if (apply_psi) {
          // NOTE: check code flow carefully before changing this!
          if (!has_changed || res_index == 0) {
            anchor = positions[3*res_index + 2];
            rotation_bond = anchor - positions[3*res_index + 1]; // C-CA
            t_psi = core::RotationAroundLine(rotation_bond, anchor, d_psi);
          }
          // cache update
          rot_psi[res_index] += d_psi;
          dihedral_pairs[res_index].second += d_psi;
          last_rot_idx = counter_rot_idx + res_index;
          if (!has_changed) {
            new_prob = GetProbability(dihedral_pairs[res_index], res_index);
          }
          last_prob[res_index] = std::max(new_prob, min_p);
        }

        // apply transforms
        if (apply_phi && apply_psi) {
          // transform C just by phi
          ApplyTransform(positions + 3*res_index + 2, 1, t_phi);
          // rest by both
          ApplyTransform(positions + 3*res_index + 3, positions_end,
                         t_phi * t_psi);
        } else if (apply_psi) {
          ApplyTransform(positions + 3*res_index + 3, positions_end, t_psi);
        } else if (apply_phi) {
          ApplyTransform(positions + 3*res_index + 2, positions_end, t_phi);
        }
      }
    } // for res_index
    // we done?
    if (GetTargetMSD(actual_positions, target_positions_) < squared_cutoff) {
      return true;
    } 
  } 
  return false;
}

///////////////////////////////////////////////////////////////////////////////
// CCD private working on internal data
void CCD::SetStems_(const ost::mol::ResidueHandle& n_stem,
                    const ost::mol::ResidueHandle& c_stem) {
  n_stem_ = n_stem;
  ost::mol::ResidueHandle after_c_stem = c_stem.GetNext();
  if (after_c_stem.IsValid()) {
    // is it really the next residue?
    if (ost::mol::InSequence(c_stem, after_c_stem)) {
      after_c_stem_ = after_c_stem;
    }
    // else: after_c_stem_ will be invalid
  }
}

void CCD::SetTargetPositions_(const ost::mol::ResidueHandle& c_stem) {
  ost::mol::AtomHandle n = c_stem.FindAtom("N");
  ost::mol::AtomHandle ca = c_stem.FindAtom("CA");
  ost::mol::AtomHandle c = c_stem.FindAtom("C");

  if (!n.IsValid() || !ca.IsValid() || !c.IsValid()) {
    throw promod3::Error("Could not find N, CA or C atom in 'after' residue "
                         "in CCD loop closing algorithm!");
  }
  target_positions_[0] = n.GetPos();
  target_positions_[1] = ca.GetPos();
  target_positions_[2] = c.GetPos();
}

void CCD::SetTorsionParametrization_(const ost::mol::ResidueHandle& before,
                                     const ost::mol::ResidueHandle& after,
                                     const String& sequence) {

  std::vector<ost::conop::AminoAcid> aa(sequence.size()+2, ost::conop::XXX);

  // set AA and angle for residue before before
  ost::mol::ResidueHandle before_before = before.GetPrev();
  if (!ost::mol::InSequence(before_before, before)) {
    // fall back: use dummy angle/residue for torsion sampler
    aa[0] = ost::conop::ALA;
    n_stem_phi_ = -1.0472; // from helix
  } else {
    aa[0] = ost::conop::ResidueToAminoAcid(before_before);
    if (aa[0] == ost::conop::XXX) aa[0] = ost::conop::ALA;
    ost::mol::AtomHandle before_before_c = before_before.FindAtom("C");
    if (!before_before_c.IsValid()) {
      // fall back: use dummy angle
      n_stem_phi_ = -1.0472; // from helix
    } else {
      n_stem_phi_ = geom::DihedralAngle(before_before_c.GetPos(),
                                        before.FindAtom("N").GetPos(),
                                        before.FindAtom("CA").GetPos(),
                                        before.FindAtom("C").GetPos());
    }
  }

  // set AA and angle for residue after after
  ost::mol::ResidueHandle after_after = after.GetNext();
  if (!ost::mol::InSequence(after, after_after)) {
    // fall back: use dummy angle/residue for torsion sampler
    aa.back() = ost::conop::ALA;
    c_stem_psi_ = -0.78540; // from helix
  } else {
    aa.back() = ost::conop::ResidueToAminoAcid(after_after);
    if(aa.back() == ost::conop::XXX) aa.back() = ost::conop::ALA;
    ost::mol::AtomHandle after_after_n = after_after.FindAtom("N");
    if (!after_after_n.IsValid()) {
      // fall back: use dummy angle
      c_stem_psi_ = -0.78540; // from helix
    } else {
      c_stem_psi_ = geom::DihedralAngle(after.FindAtom("N").GetPos(),
                                        after.FindAtom("CA").GetPos(),
                                        after.FindAtom("C").GetPos(),
                                        after_after_n.GetPos());
    }
  }

  // setup rest
  for (uint i = 0; i < sequence.size(); ++i) {
    aa[i+1] = ost::conop::OneLetterCodeToAminoAcid(sequence[i]);
    if (aa[i+1] == ost::conop::XXX) {
      throw promod3::Error("Observed invalid letter in input sequence!");
    }
  }
  
  dihedral_indices_.clear();
  for (uint i = 0; i < sequence.size(); ++i) {
    dihedral_indices_.push_back(
      torsion_sampler_[i]->GetHistogramIndex(aa[i], aa[i+1], aa[i+2]));
  }
}

}} // PM3 ns
