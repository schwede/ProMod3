// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROMOD_MODELLING_MOTIF_FINDER_HH
#define PROMOD_MODELLING_MOTIF_FINDER_HH

#include <ost/mol/entity_view.hh>
#include <ost/mol/residue_view.hh>
#include <ost/geom/vec3.hh>
#include <vector>

namespace promod3 { namespace modelling {    

struct Triangle{

  Triangle(uint16_t p1, uint16_t p2, uint16_t p3): p1(p1), p2(p2), p3(p3) { }

  uint16_t p1;
  uint16_t p2;
  uint16_t p3;
};


// forward declaration to keep stuff in source file
struct MotifQueryData;

struct MotifQuery{

  MotifQuery(const geom::Vec3List& positions, const String& identifier,
             Real min_triangle_edge_length,
             Real max_triangle_edge_length,
             Real bin_size,
             const std::vector<int>& flags = std::vector<int>());

  MotifQuery(const std::vector<MotifQuery>& queries);

  static MotifQuery Load(const String& filename);

  void Save(const String& filename) const;

  const geom::Vec3List& GetPositions(uint idx) const;

  const std::vector<uint8_t>& GetFlags(uint idx) const;

  const std::vector<String>& GetIdentifiers() const;

  size_t GetN() const;

  size_t GetQuerySize(uint idx) const;

  size_t GetNTriangles(uint idx) const;

  size_t GetNTriangles() const;

  Real GetMinTriangleEdgeLength() const;

  Real GetMaxTriangleEdgeLength() const;

  Real GetMinPosBound() const;

  Real GetMaxPosBound() const;

  Real GetBinSize() const;

  Triangle GetTriangle(uint idx, uint triangle_idx) const;

  void PrintBinSizes() const;

  void Prune(Real factor);

  std::shared_ptr<MotifQueryData> data_;

private:

  MotifQuery();
};


struct MotifMatch{

  MotifMatch(int q_idx, int t_idx, const geom::Mat4& m, 
              const std::vector<std::pair<int,int> >& a): query_idx(q_idx),
                                                          mat(m), aln(a) { }
  int query_idx;
  int triangle_idx;
  geom::Mat4 mat;
  std::vector<std::pair<int,int> > aln;
};


std::vector<MotifMatch> FindMotifs(const MotifQuery& query,
                                   const geom::Vec3List& positions,
                                   Real hash_thresh = 0.4,
                                   Real distance_thresh = 1.0,
                                   Real refine_thresh = 0.7,
                                   const std::vector<int>& flags = std::vector<int>(),
                                   bool swap_thresh=false);


}} // ns

#endif
