// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD3_MODELLING_SCORE_CONTAINER_HH
#define PROMOD3_MODELLING_SCORE_CONTAINER_HH

#include <boost/shared_ptr.hpp>
#include <map>
#include <vector>
#include <ost/base.hh>
#include <promod3/core/message.hh>

namespace promod3 { namespace modelling {

class ScoreContainer;
typedef boost::shared_ptr<ScoreContainer> ScoreContainerPtr;

// convenience class to keep multiple scores for loop candidates
class ScoreContainer {
public:
  // score vector type
  typedef std::vector<Real> ScoreVector;

  // construct empty container
  ScoreContainer(): num_candidates_(0) { }

  // check if there's anything
  bool IsEmpty() { return map_.empty(); }

  // check existance of scores
  bool Contains(const String& key) const { return map_.find(key) != map_.end(); }

  // r/o get access (throws exc. if nothing found)
  const ScoreVector& Get(const String& key) const {
    std::map<String, ScoreVector>::const_iterator found = map_.find(key);
    if (found == map_.end()) {
      throw promod3::Error("ScoreContainer does not contain scores for key '"
                           + key + "'!");
    } else {
      return found->second;
    }
  }

  // set new scores (consistency enforced)
  void Set(const String& key, const ScoreVector& scores) {
    if (!IsEmpty() && num_candidates_ != scores.size()) {
      throw promod3::Error("Inconsistent scores size in ScoreContainer::Set!");
    } else {
      num_candidates_ = scores.size();
      map_[key] = scores;
    }
  }

  // get number of candidates that are scored
  uint GetNumCandidates() const { return num_candidates_; }

  // calculate linear combination of scores
  ScoreVector LinearCombine(const std::map<String, Real>& linear_weights) const;

  // extract copy
  ScoreContainerPtr Copy() const {
    return ScoreContainerPtr(new ScoreContainer(*this));
  }
  // extract copy with subset of data
  ScoreContainerPtr Extract(const std::vector<uint>& indices) const;

  // extend scores
  void Extend(const ScoreContainer& other);

private:
  // DATA
  std::map<String, ScoreVector> map_;
  uint num_candidates_;
};

}} // ns

#endif
