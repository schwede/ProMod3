// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PM3_MODELLING_LOOP_CANDIDATE_HH
#define PM3_MODELLING_LOOP_CANDIDATE_HH

#include <promod3/loop/backbone.hh>
#include <promod3/loop/frag_db.hh>
#include <promod3/loop/torsion_sampler.hh>
#include <promod3/scoring/backbone_overall_scorer.hh>
#include <promod3/modelling/ccd.hh>
#include <promod3/modelling/model.hh>
#include <promod3/modelling/monte_carlo.hh>
#include <promod3/modelling/score_container.hh>

#include <ost/seq/profile_handle.hh>
#include <vector>

namespace promod3 { namespace modelling {

typedef std::vector<loop::BackboneList> LoopCandidateList;
class LoopCandidates;
typedef boost::shared_ptr<LoopCandidates> LoopCandidatesPtr;
typedef std::vector<LoopCandidatesPtr> LoopCandidatesList;

class LoopCandidates {
public:
  LoopCandidates( const String& seq);
  
  static LoopCandidatesPtr FillFromDatabase(
                              const ost::mol::ResidueHandle& n_stem, 
                              const ost::mol::ResidueHandle& c_stem,
                              const String& seq, loop::FragDBPtr frag_db, 
                              loop::StructureDBPtr structure_db,
                              bool extended_search = false);

  static LoopCandidatesPtr FillFromMonteCarloSampler(
                              const String& seq, uint num_loops, uint steps,
                              MonteCarloSamplerPtr sampler,
                              MonteCarloCloserPtr closer,
                              MonteCarloScorerPtr scorer,
                              MonteCarloCoolerPtr cooler, int random_seed = 0);

  static LoopCandidatesPtr FillFromMonteCarloSampler(
                              const loop::BackboneList& initial_bb,
                              const String& seq, uint num_loops, uint steps,
                              MonteCarloSamplerPtr sampler,
                              MonteCarloCloserPtr closer,
                              MonteCarloScorerPtr scorer,
                              MonteCarloCoolerPtr cooler, int random_seed = 0);

  const String& GetSequence() const { return sequence_; }

  std::vector<uint>
  ApplyCCD(CCD& closer, bool keep_non_converged);

  std::vector<uint>
  ApplyCCD(const ost::mol::ResidueHandle& n_stem,
           const ost::mol::ResidueHandle& c_stem,
           loop::TorsionSamplerList torsion_sampler,
           int max_iterations = 1000, Real rmsd_cutoff = 0.1,
           bool keep_non_converged = false, int random_seed=0);

  std::vector<uint>
  ApplyCCD(const ost::mol::ResidueHandle& n_stem,
           const ost::mol::ResidueHandle& c_stem,
           loop::TorsionSamplerPtr torsion_sampler,
           int max_iterations = 1000, Real rmsd_cutoff = 0.1,
           bool keep_non_converged = false, int random_seed=0);

  std::vector<uint>
  ApplyCCD(const ost::mol::ResidueHandle& n_stem,
           const ost::mol::ResidueHandle& c_stem,
           int max_iterations = 1000, Real rmsd_cutoff = 0.1, 
           bool keep_non_converged = false);

  std::vector<uint>
  ApplyKIC(const ost::mol::ResidueHandle& n_stem,
           const ost::mol::ResidueHandle& c_stem,
           uint pivot_one, uint pivot_two, uint pivot_three);

  void
  GetClusters(Real max_dist, std::vector< std::vector<uint> >& clusters, 
              bool superposed_rmsd = false) const;

  LoopCandidatesList
  GetClusteredCandidates(Real max_dist, bool neglect_size_one = true,
                         bool superposed_rmsd = false) const;

  LoopCandidatesPtr GetLargestCluster(Real max_dist, 
                                      bool superposed_rmsd = false) const;
  
  void CalculateBackboneScores(ScoreContainer& score_container,
                               scoring::BackboneOverallScorerPtr scorer,
                               scoring::BackboneScoreEnvPtr scorer_env,
                               uint start_resnum, uint chain_idx = 0) const;
  void CalculateBackboneScores(ScoreContainer& score_container,
                               scoring::BackboneOverallScorerPtr scorer,
                               scoring::BackboneScoreEnvPtr scorer_env,
                               const std::vector<String>& keys,
                               uint start_resnum, uint chain_idx = 0) const;
  void CalculateBackboneScores(ScoreContainer& score_container,
                               const ModellingHandle& mhandle,
                               uint start_resnum, uint chain_idx = 0) const;
  void CalculateBackboneScores(ScoreContainer& score_container,
                               const ModellingHandle& mhandle,
                               const std::vector<String>& keys,
                               uint start_resnum, uint chain_idx = 0) const;
  void CalculateAllAtomScores(ScoreContainer& score_container,
                              const ModellingHandle& mhandle,
                              uint start_resnum, uint chain_idx = 0) const;
  void CalculateAllAtomScores(ScoreContainer& score_container,
                              const ModellingHandle& mhandle,
                              const std::vector<String>& keys,
                              uint start_resnum, uint chain_idx = 0) const;

  std::vector<Real>
  CalculateSequenceProfileScores(loop::StructureDBPtr structure_db,
                                 const ost::seq::ProfileHandle& prof,
                                 uint offset = 0) const;
  void CalculateSequenceProfileScores(ScoreContainer& score_container,
                                      loop::StructureDBPtr structure_db,
                                      const ost::seq::ProfileHandle& prof,
                                      uint offset = 0) const;

  std::vector<Real>
  CalculateStructureProfileScores(loop::StructureDBPtr structure_db,
                                  const ost::seq::ProfileHandle& prof,
                                  uint offset = 0) const;
  void CalculateStructureProfileScores(ScoreContainer& score_container,
                                       loop::StructureDBPtr structure_db,
                                       const ost::seq::ProfileHandle& prof,
                                       uint offset = 0) const;

  std::vector<Real>
  CalculateStemRMSDs(const ost::mol::ResidueHandle& n_stem, 
                     const ost::mol::ResidueHandle& c_stem) const;
  void CalculateStemRMSDs(ScoreContainer& score_container,
                          const ost::mol::ResidueHandle& n_stem, 
                          const ost::mol::ResidueHandle& c_stem) const;

  // Add/Remove candidates
  void Add(const loop::BackboneList& candidate);
  void AddFragmentInfo(const loop::FragmentInfo& fragment);
  void Remove(uint index);

  // check if we have valid fragment infos
  bool HasFragmentInfos() const;
  // get info (throws if !HasFragmentInfos or index out-of-bounds)
  const loop::FragmentInfo& GetFragmentInfo(uint index) const;

  // extract subset of candidates
  LoopCandidatesPtr Extract(const std::vector<uint>& indices) const;

  // vector-like behavior
  size_t size() const { return candidates_.size(); }
  bool empty() const { return candidates_.empty(); }
  
  typedef LoopCandidateList::const_iterator const_iterator;
  typedef LoopCandidateList::iterator iterator;
  
  iterator begin() { return candidates_.begin(); }
  iterator end() { return candidates_.end(); }
  
  const_iterator begin() const { return candidates_.begin(); }
  const_iterator end() const { return candidates_.end(); }

  bool operator==(const LoopCandidates& rhs) const {
   return sequence_ == rhs.sequence_ && candidates_ == rhs.candidates_
       && fragments_ == rhs.fragments_;
  }  
  bool operator!=(const LoopCandidates& rhs) const {
    return !this->operator==(rhs);
  }
  
  // we use safe access which throws exceptions when out-of-bounds
  loop::BackboneList& operator[](size_t index) { 
    return candidates_.at(index); 
  }
  const loop::BackboneList& operator[](size_t index) const {
    return candidates_.at(index); 
  }
  
private:
  LoopCandidateList candidates_;
  std::vector<loop::FragmentInfo> fragments_; // filled by FillFromDatabase
  String sequence_;
};


}}

#endif

