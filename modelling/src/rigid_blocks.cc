// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/rigid_blocks.hh>
#include <promod3/core/cluster.hh>

#include <ost/mol/residue_view.hh>
#include <ost/mol/atom_view.hh>

#include <algorithm>

namespace {

inline Real IndexListSimilarity(const std::vector<uint>& v1, 
                                const std::vector<uint>& v2,
                                uint max_size){

  std::vector<uint> complete_v1(max_size, 0);
  std::vector<uint> complete_v2(max_size, 0);

  for(std::vector<uint>::const_iterator i = v1.begin();
      i != v1.end(); ++i){
    complete_v1[*i] = 1;
  }

  for(std::vector<uint>::const_iterator i = v2.begin();
      i != v2.end(); ++i){
    complete_v2[*i] = 1;
  }

  uint sum = 0;
  for(uint i = 0; i < max_size; ++i){
    sum += complete_v1[i] * complete_v2[i]; 
  }

  return static_cast<Real>(sum) / std::min(v1.size(),v2.size());
}

void DoIt(promod3::core::EMatX3& pos_mat_one, 
          promod3::core::EMatX3& pos_mat_two,
          Real window_length, Real max_iterations,
          Real distance_thresh, Real cluster_thresh,
          std::vector<std::vector<uint> >& indices, 
          std::vector<geom::Mat4>& transformations) {

  uint num_rows = pos_mat_one.rows();
  std::vector<std::vector<uint> > initial_indices;
  std::vector<geom::Mat4> initial_transformations;

  //get initial solution
  promod3::core::RigidBlocks(pos_mat_one, pos_mat_two,
                             window_length, max_iterations,
                             distance_thresh, initial_indices,
                             initial_transformations);

  uint num_blocks = initial_indices.size();
  ost::TriMatrix<Real> dist_mat(num_blocks, Real(0.0));

  //calculate the pairwise distances
  for(uint i = 0; i < num_blocks; ++i){
    for(uint j = i + 1; j < num_blocks; ++j){
      Real dist = IndexListSimilarity(initial_indices[i],
                                      initial_indices[j],
                                      num_rows);
      dist_mat.Set(i, j, dist);
    }
  }  

  std::vector<std::vector<uint> > clusters;
  promod3::core::DoClustering(dist_mat, promod3::core::MIN_DIST_METRIC,
                              cluster_thresh, '+', clusters);

  //map over the final indices
  indices.clear();
  std::vector<uint>::iterator it;
  for(uint i = 0; i < clusters.size(); ++i){

    std::vector<uint> current_indices = initial_indices[clusters[i][0]];

    for(uint j = 1; j < clusters[i].size(); ++j){

      std::vector<uint> union_result;

      std::set_union(current_indices.begin(),
                     current_indices.end(),
                     initial_indices[clusters[i][j]].begin(),
                     initial_indices[clusters[i][j]].end(),
                     std::back_inserter(union_result));

      current_indices = union_result;
    }
    indices.push_back(current_indices);
  }
 
  //extract the final transformations
  transformations.clear();
  for(uint i = 0; i < indices.size(); ++i){
    const std::vector<uint>& current_indices = indices[i];
  
    promod3::core::EMatX3 temp_pos_one = 
    promod3::core::EMatX3::Zero(current_indices.size(), 3);
    promod3::core::EMatX3 temp_pos_two = 
    promod3::core::EMatX3::Zero(current_indices.size(), 3);

    for(uint j = 0; j < current_indices.size(); ++j){
      temp_pos_one.row(j) = pos_mat_one.row(current_indices[j]);
      temp_pos_two.row(j) = pos_mat_two.row(current_indices[j]);
    }

    transformations.push_back(promod3::core::MinRMSDSuperposition(temp_pos_one, 
                                                                  temp_pos_two));
  }
}


}

namespace promod3{ namespace modelling{

void RigidBlocks(const ost::seq::AlignmentHandle& aln, uint seq_one_idx, 
	             uint seq_two_idx, uint window_length, uint max_iterations, 
	             Real distance_thresh, Real cluster_thresh, 
	             std::vector<std::vector<uint> >& indices,
               std::vector<geom::Mat4>& transformations){


  if(static_cast<int>(seq_one_idx) >= aln.GetCount() || 
  	 static_cast<int>(seq_two_idx) >= aln.GetCount()){
  	throw promod3::Error("Invalid sequence idx provided!");
  }

  ost::seq::ConstSequenceHandle seq_one = aln.GetSequence(seq_one_idx); 
  ost::seq::ConstSequenceHandle seq_two = aln.GetSequence(seq_two_idx); 

  if(!(seq_one.HasAttachedView() && seq_two.HasAttachedView())){
  	throw promod3::Error("Specified sequences must have view attached!");
  }

  std::vector<geom::Vec3> positions_one;
  std::vector<geom::Vec3> positions_two;
  std::vector<uint> index_mapper;

  uint aln_length = aln.GetLength();
  positions_one.reserve(aln_length);
  positions_two.reserve(aln_length);
  index_mapper.reserve(aln_length);

  ost::mol::ResidueView res_one, res_two;
  ost::mol::AtomView ca_one, ca_two;

  for(uint i = 0; i < aln_length; ++i){
    res_one = seq_one.GetResidue(i);
    res_two = seq_two.GetResidue(i);

    if(!(res_one.IsValid() && res_two.IsValid())) continue;

    ca_one = res_one.FindAtom("CA");  
    ca_two = res_two.FindAtom("CA");

    if(!(ca_one.IsValid() && ca_two.IsValid())) continue;

    positions_one.push_back(ca_one.GetPos());
    positions_two.push_back(ca_two.GetPos());  
    index_mapper.push_back(i);
  }

  promod3::core::EMatX3 pos_mat_one = 
  promod3::core::EMatX3::Zero(positions_one.size(),3);
  promod3::core::EMatX3 pos_mat_two = 
  promod3::core::EMatX3::Zero(positions_two.size(),3);

  for(uint i = 0; i < positions_one.size(); ++i){
  	promod3::core::EMatFillRow(pos_mat_one, i, positions_one[i]);
  	promod3::core::EMatFillRow(pos_mat_two, i, positions_two[i]);
  }

  std::vector<std::vector<uint> > solution;
  DoIt(pos_mat_one, pos_mat_two,
  	   window_length, max_iterations,
  	   distance_thresh, cluster_thresh,
  	   solution, transformations);

  indices.clear();
  for(uint i = 0; i < solution.size(); ++i){
  	const std::vector<uint>& current_solution = solution[i];
    std::vector<uint> current_indices(current_solution.size(), 0);
  	for(uint j = 0; j < current_solution.size(); ++j){
      current_indices[j] = index_mapper[current_solution[j]];
  	}
  	indices.push_back(current_indices);
  }
}


void RigidBlocks(const promod3::loop::BackboneList& bb_list_one, 
                 const promod3::loop::BackboneList& bb_list_two,
                 uint window_length, uint max_iterations, 
                 Real distance_thresh, Real cluster_thresh, 
                 std::vector<std::vector<uint> >& indices,
                 std::vector<geom::Mat4>& transformations){

  if (bb_list_one.size() != bb_list_two.size()) {
    throw promod3::Error("Sizes of backbone lists must match in order to "
                         "calculate the transformations between them!");
  }

  if(window_length > bb_list_one.size()){
    throw promod3::Error("window_length must be smaller than size of "
                         "BackboneLists!");
  }

  // superpose CA positions
  core::EMatX3 pos_one = core::EMatX3::Zero(bb_list_one.size(), 3);
  core::EMatX3 pos_two = core::EMatX3::Zero(bb_list_two.size(), 3);

  for (uint i = 0; i < bb_list_one.size(); ++i) {
    core::EMatFillRow(pos_one, i, bb_list_one.GetCA(i));
  }

  for(uint i = 0; i < bb_list_two.size(); ++i){
    core::EMatFillRow(pos_two, i, bb_list_two.GetCA(i));
  }

  indices.clear();

  DoIt(pos_one, pos_two, window_length,
       max_iterations, distance_thresh, 
       cluster_thresh, indices, transformations);
}


void RigidBlocks(const geom::Vec3List& pos_one, 
                 const geom::Vec3List& pos_two,
                 uint window_length, uint max_iterations, 
                 Real distance_thresh, Real cluster_thresh, 
                 std::vector<std::vector<uint> >& indices,
                 std::vector<geom::Mat4>& transformations){

  if (pos_one.size() != pos_two.size()) {
    throw promod3::Error("Sizes of positions must match in order to "
                         "calculate the transformations between them!");
  }

  if(window_length > pos_one.size()){
    throw promod3::Error("window_length must be smaller than size of "
                         "position lists!");
  }

  // superpose CA positions
  core::EMatX3 pos_one_mat = core::EMatX3::Zero(pos_one.size(), 3);
  core::EMatX3 pos_two_mat = core::EMatX3::Zero(pos_two.size(), 3);

  for(uint i = 0; i < pos_one.size(); ++i){
    core::EMatFillRow(pos_one_mat, i, pos_one[i]);
  }

  for(uint i = 0; i < pos_two.size(); ++i){
    core::EMatFillRow(pos_two_mat, i, pos_two[i]);
  }

  indices.clear();

  DoIt(pos_one_mat, pos_two_mat, window_length,
       max_iterations, distance_thresh, 
       cluster_thresh, indices, transformations);
}

}} //ns
