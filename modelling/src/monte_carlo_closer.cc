// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/monte_carlo_closer.hh>
#include <promod3/modelling/kic.hh>
#include <promod3/modelling/ccd.hh>

namespace promod3 { namespace modelling {

MonteCarloCloser::~MonteCarloCloser() { }

KICCloser::KICCloser(const ost::mol::ResidueHandle& n_stem, 
                     const ost::mol::ResidueHandle& c_stem,
                     uint seed): closer_(n_stem,c_stem),
                                 rgen_(seed),
                                 shuffler_(rgen_,shuffle_generator_){ 

  uint start_num = n_stem.GetNumber().GetNum();
  uint end_num = c_stem.GetNumber().GetNum();
  backbone_size_ = end_num-start_num+1;

  if (end_num - start_num < 4) {
    std::stringstream ss;
    ss << "Must have at least 3 residues between n_ter and c_ter for proper ";
    ss << "KICCloser initialization.";
    throw promod3::Error(ss.str());
  }

  for (uint i = 1; i < end_num-start_num; ++i) {
    possible_pivot_residues_.push_back(i);
  }
}


bool KICCloser::Close(const loop::BackboneList& actual_positions,
                      loop::BackboneList& closed_positions){

  if (actual_positions.size() != backbone_size_) {
    throw promod3::Error("Inconsistent size when closing loop!");
  }

  this->ShufflePivotResidues();
  std::vector<loop::BackboneList> kic_solutions;
  closer_.Close(actual_positions, pivot_one_, pivot_two_,
                pivot_three_, kic_solutions);
  if (!kic_solutions.empty()) {
    closed_positions = kic_solutions[0];
    return true;
  }
  return false;
}

void KICCloser::ShufflePivotResidues(){
  std::random_shuffle(possible_pivot_residues_.begin(),
                      possible_pivot_residues_.end(), shuffler_);
  pivot_one_ = possible_pivot_residues_[0];
  pivot_two_ = possible_pivot_residues_[1];
  pivot_three_ = possible_pivot_residues_[2];
  if (pivot_one_>pivot_two_) std::swap(pivot_one_, pivot_two_);
  if (pivot_two_>pivot_three_) std::swap(pivot_two_,pivot_three_);
  if (pivot_one_>pivot_two_) std::swap(pivot_one_,pivot_two_);
}

DirtyCCDCloser::DirtyCCDCloser(const ost::mol::ResidueHandle& n_stem, const ost::mol::ResidueHandle& c_stem): closer_(n_stem,c_stem,1000,0.1){ }

bool DirtyCCDCloser::Close(const loop::BackboneList& actual_positions,
                           loop::BackboneList& closed_positions){
  closed_positions = actual_positions;
  bool closed = closer_.Close(closed_positions);
  return closed;
}

CCDCloser::CCDCloser(const ost::mol::ResidueHandle& n_stem, 
                     const ost::mol::ResidueHandle& c_stem,
                     const String& sequence,
                     loop::TorsionSamplerPtr torsion_sampler, 
                     int seed)
: closer_(sequence, n_stem, c_stem, torsion_sampler, 1000, 0.1, seed) { }

CCDCloser::CCDCloser(const ost::mol::ResidueHandle& n_stem,
                     const ost::mol::ResidueHandle& c_stem,
                     const String& sequence,
                     const loop::TorsionSamplerList& torsion_sampler, 
                     int seed)
: closer_(sequence, n_stem, c_stem, torsion_sampler, 1000, 0.1, seed) { }

bool CCDCloser::Close(const loop::BackboneList& actual_positions,
                      loop::BackboneList& closed_positions){

  closed_positions = actual_positions;
  bool closed = closer_.Close(closed_positions);
  return closed;
}


NTerminalCloser::NTerminalCloser(const ost::mol::ResidueHandle& c_stem) {
  c_stem_.push_back(c_stem);  // list with 1 backbone
  // check res. after c_stem for oxygen reconstruction
  ost::mol::ResidueHandle after_c_stem = c_stem.GetNext();
  if (after_c_stem.IsValid()) {
    // is it really the next residue?
    if (ost::mol::InSequence(c_stem, after_c_stem)) {
      after_c_stem_ = after_c_stem;
    }
    // else: after_c_stem_ will be invalid
  }
}

bool NTerminalCloser::Close(const loop::BackboneList& actual_positions,
                            loop::BackboneList& closed_positions){

  if (actual_positions.empty()) {
    throw promod3::Error("Cannot close empty backbone!");
  }

  closed_positions = actual_positions;
  geom::Mat4 t = closed_positions.GetTransform(closed_positions.size()-1,
                                               c_stem_, 0);
  closed_positions.ApplyTransform(t);
  closed_positions.ReconstructCStemOxygen(after_c_stem_);
  return true;
}

CTerminalCloser::CTerminalCloser(const ost::mol::ResidueHandle& n_stem) {
  n_stem_.push_back(n_stem);  // list with 1 backbone
}

bool CTerminalCloser::Close(const loop::BackboneList& actual_positions,
                            loop::BackboneList& closed_positions){

  if (actual_positions.empty()) {
    throw promod3::Error("Cannot close empty backbone!");
  }

  closed_positions = actual_positions;
  geom::Mat4 t = closed_positions.GetTransform(0, n_stem_, 0);
  closed_positions.ApplyTransform(t);
  return true;
}


}}//ns

