// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_BACKBONE_RELAXER_HH
#define PROMOD_MODELLING_BACKBONE_RELAXER_HH

#include <promod3/loop/backbone.hh>
#include <promod3/loop/forcefield_lookup.hh>
#include <ost/conop/amino_acids.hh>
#include <ost/mol/mm/topology.hh>
#include <ost/mol/mm/simulation.hh>
#include <ost/geom/vec3.hh>
#include <vector>

namespace promod3 { namespace modelling {

struct BackboneRelaxer;
typedef boost::shared_ptr<BackboneRelaxer> BackboneRelaxerPtr;

struct  BackboneRelaxer {
  BackboneRelaxer(const loop::BackboneList& bb, 
                  promod3::loop::ForcefieldLookupPtr ff,
                  bool fix_nterm = true, bool fix_cterm = true);

  void AddNRestraint(uint idx, const geom::Vec3& pos, 
                     Real force_constant = 100000.0);

  void AddCARestraint(uint idx, const geom::Vec3& pos, 
                      Real force_constant = 100000.0);

  void AddCBRestraint(uint idx, const geom::Vec3& pos, 
                      Real force_constant = 100000.0);

  void AddCRestraint(uint idx, const geom::Vec3& pos, 
                     Real force_constant = 100000.0);

  void AddORestraint(uint idx, const geom::Vec3& pos, 
                     Real force_constant = 100000.0);

  void SetNonBondedCutoff(Real nonbonded_cutoff);
  Real GetNonBondedCutoff() const { return nonbonded_cutoff_; }

  Real Run(loop::BackboneList& bb_list, int steps = 100,
           Real stop_criterion = 0.01);

private:

  void Init();

  void SetSimulationPositions(const loop::BackboneList& bb_list);

  void ExtractSimulationPositions(loop::BackboneList& bb_list);

  uint GetResidueStartIdx(uint idx) const;

  ost::mol::mm::TopologyPtr CreateBBListTop(const loop::BackboneList& bb_list,
                                            promod3::loop::ForcefieldLookupPtr ff,
                                            bool fix_nterm, bool fix_cterm);
  
  std::vector<ost::conop::AminoAcid> s_;
  ost::mol::mm::TopologyPtr top_;
  ost::mol::mm::SimulationPtr simulation_;
  geom::Vec3List positions_;
  Real nonbonded_cutoff_;
};

}}

#endif
