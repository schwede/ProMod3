// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_MONTE_CARLO_COOLER_HH
#define PROMOD_MODELLING_MONTE_CARLO_COOLER_HH

#include <boost/shared_ptr.hpp>
#include <ost/base.hh>

namespace promod3 { namespace modelling {

class MonteCarloCooler;
class ExponentialCooler;

typedef boost::shared_ptr<MonteCarloCooler> MonteCarloCoolerPtr;
typedef boost::shared_ptr<ExponentialCooler> ExponentialCoolerPtr;

class MonteCarloCooler{
public:
  virtual Real GetTemperature() = 0;
  virtual void Reset() = 0;
  virtual ~MonteCarloCooler() = 0;
};

class ExponentialCooler : public MonteCarloCooler{

public:

  ExponentialCooler(int change_frequency, Real start_temperature, 
                    Real cooling_factor)
                    : start_temperature_(start_temperature)
                    , step_(0), temperature_(start_temperature/cooling_factor)
                    , change_frequency_(change_frequency)
                    , cooling_factor_(cooling_factor) { }

  Real GetTemperature();
  void Reset();

private:
  Real start_temperature_;
  int step_;
  Real temperature_;
  int change_frequency_;
  Real cooling_factor_;
};

}}//ns

#endif
