// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <ost/log.hh>

#include <promod3/modelling/gap.hh>

using namespace ost::mol;
using namespace ost;

namespace promod3 { namespace modelling {

String StructuralGap::GetChainName() const
{
  if (this->IsCTerminal()) {
    return before.GetChain().GetName();
  } else {
    return after.GetChain().GetName();
  }
}

ost::mol::ChainHandle StructuralGap::GetChain() const
{
  if (this->IsCTerminal()) {
    return before.GetChain();
  } else {
    return after.GetChain();
  }
}

uint StructuralGap::GetChainIndex() const
{
  if (this->IsCTerminal()) {
    ost::mol::ChainHandleList chain_list = before.GetEntity().GetChainList();
    ost::mol::ChainHandle chain = before.GetChain();
    for(uint i = 0; i < chain_list.size(); ++i){
      if(chain == chain_list[i]) return i;
    }
  }
  if (this->IsNTerminal()) {
    ost::mol::ChainHandleList chain_list = after.GetEntity().GetChainList();
    ost::mol::ChainHandle chain = after.GetChain();
    for(uint i = 0; i < chain_list.size(); ++i){
      if(chain == chain_list[i]) return i;
    }
  }
  ost::mol::ChainHandleList chain_list = before.GetEntity().GetChainList();
  ost::mol::ChainHandle chain = before.GetChain();
  for(uint i = 0; i < chain_list.size(); ++i){
    if(chain == chain_list[i]) return i;
  }
  return 0; //won't be reached anyway...
}

bool StructuralGap::ExtendAtCTerm()
{
  if (this->IsCTerminal()) {
    return false;
  }
  mol::ResidueHandle next=after.GetNext();
  if (!next.IsValid()) {
    return false;
  }
  ResNum next_num=next.GetNumber();
  ResNum exp_num=after.GetNumber()+1;
  if (next_num!=exp_num) {
    return false;
  }
  sequence+=after.GetOneLetterCode();
  after=next;
  return true;
}


bool StructuralGap::ExtendAtNTerm()
{
  if (this->IsNTerminal()) {
    return false;
  }
  ResidueHandle prev=before.GetPrev();
  if (!prev.IsValid()) {
    return false;
  }
  ResNum next_num=prev.GetNumber();
  ResNum exp_num=before.GetNumber()-1;
  if (next_num!=exp_num) {
    return false;
  }
  sequence=String(1, before.GetOneLetterCode())+sequence;
  before=prev;
  return true;
}

bool StructuralGap::ShiftCTerminal()
{
  ResidueHandle next1=after.GetNext();
  ResidueHandle next2=before.GetNext();
  if (!(next1.IsValid() && next2.IsValid())) {
    return false;
  }
  ResNum next_num=next1.GetNumber();
  ResNum exp_num=after.GetNumber()+1;
  if (next_num!=exp_num) {
    return false;
  }
  next_num=next2.GetNumber();
  exp_num=before.GetNumber()+1;
  if (next_num!=exp_num) {
    return false;
  }
  sequence=sequence.substr(1)+after.GetOneLetterCode();
  before=next2;
  after=next1;
  return true;
}

String StructuralGap::AsString() const
{
  std::stringstream ss;
  if (this->IsNTerminal()) {
    ss << "(" << sequence << ")-" << after.GetQualifiedName();
  } else if (this->IsCTerminal()) {
    ss << before.GetQualifiedName() << "-(" << sequence << ")";
  } else {
    ss << before.GetQualifiedName() << "-(" << sequence << ")-" 
       << after.GetQualifiedName();
  }
  return ss.str();
}

void StructuralGap::Transfer(const ost::mol::ChainHandle& chain)
{

  if(!this->IsNTerminal()){
    ost::mol::ResidueHandle new_before = chain.FindResidue(before.GetNumber()); 
    if(!new_before.IsValid()){
      throw promod3::Error("Could not find appropriate residue to transfer gap!");
    }

    if(before.GetName() != new_before.GetName()){
      throw promod3::Error("Inconsistent residues when transferring gap!");
    }
    before = new_before;
  }

  if(!this->IsCTerminal()){
    ost::mol::ResidueHandle new_after = chain.FindResidue(after.GetNumber()); 
    if(!new_after.IsValid()){
      throw promod3::Error("Could not find appropriate residue to transfer gap!");
    }

    if(after.GetName() != new_after.GetName()){
      throw promod3::Error("Inconsistent residues when transferring gap!");
    }
    after = new_after;    
  }
}

}}
