// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/sidechain_env_listener.hh>
#include <promod3/sidechain/sidechain_object_loader.hh>
#include <promod3/core/runtime_profiling.hh>

namespace promod3 { namespace modelling {

SidechainEnvListener::SidechainEnvListener(bool use_frm, bool use_bbdep_lib,
                                           bool add_cyd_rotamers,
                                           bool all_rotamers)
                                           : all_rotamers_(all_rotamers)
                                           , use_frm_(use_frm)
                                           , use_bbdep_lib_(use_bbdep_lib)
                                           , add_cyd_rotamers_(add_cyd_rotamers)
                                           , env_(20.0), env_data_(NULL) {
  // check
  if (use_frm_ && !use_bbdep_lib_) {
    throw promod3::Error("Cannot use non backbone dependent lib with FRMRotamers!");
  }
  // setup library
  if (use_bbdep_lib_) bbdep_library_ = sidechain::LoadBBDepLib();
  else library_ = sidechain::LoadLib();

}

SidechainEnvListener::SidechainEnvListener(bool use_frm,
                                           sidechain::BBDepRotamerLibPtr bbdep_library,
                                           bool add_cyd_rotamers,
                                           bool all_rotamers)
                                           : all_rotamers_(all_rotamers)
                                           , use_frm_(use_frm)
                                           , use_bbdep_lib_(true)
                                           , add_cyd_rotamers_(add_cyd_rotamers)
                                           , bbdep_library_(bbdep_library)
                                           , env_(20.0), env_data_(NULL) { }

SidechainEnvListener::SidechainEnvListener(bool use_frm, 
                                           sidechain::RotamerLibPtr library,
                                           bool add_cyd_rotamers,
                                           bool all_rotamers)
                                           : all_rotamers_(all_rotamers)
                                           , use_frm_(use_frm)
                                           , use_bbdep_lib_(false)
                                           , add_cyd_rotamers_(add_cyd_rotamers)
                                           , library_(library)
                                           , env_(20.0), env_data_(NULL) { }

SidechainEnvListener::~SidechainEnvListener() {
  if (env_data_ != NULL) delete [] env_data_;
}

void SidechainEnvListener::Init(const loop::AllAtomEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "SidechainEnvListener::Init", 2);

  if (env_data_ != NULL) {
    // the env listener has already been initialized at some point...
    // let's clean up first...
    delete [] env_data_;
    env_data_ = NULL;
    env_.Clear();
  }

  // set env. data
  loop::ConstAllAtomPositionsPtr all_pos = base_env.GetAllPosData();
  const uint num_residues = all_pos->GetNumResidues();
  // env_data: set fix res. indices
  env_data_ = new ScCBetaSpatialOrganizerItem[num_residues];
  for (uint res_idx = 0; res_idx < num_residues; ++res_idx) {
    env_data_[res_idx].res_idx = res_idx;
  }

  // set terminal flags
  loop::ConstIdxHandlerPtr idx_handler = base_env.GetIdxHandlerData();
  n_ter_.assign(num_residues, false);
  c_ter_.assign(num_residues, false);
  for (uint ch_idx = 0; ch_idx < idx_handler->GetNumChains(); ++ch_idx) {
    n_ter_[idx_handler->ToIdx(1, ch_idx)] = true;
    const uint c_res_num = idx_handler->GetChainSize(ch_idx);
    c_ter_[idx_handler->ToIdx(c_res_num, ch_idx)] = true;
  }

  // set rotamer IDs (fixed based on AA in all_pos)
  r_id_.assign(num_residues, sidechain::ALA);
  for (uint res_idx = 0; res_idx < num_residues; ++res_idx) {
    r_id_[res_idx] = sidechain::AAToRotID(all_pos->GetAA(res_idx));
  }

  // initialize rotamer data (all false / NULL for now)
  phi_angle_.assign(num_residues, 0);
  psi_angle_.assign(num_residues, 0);
  bb_frame_residue_.assign(num_residues, sidechain::FrameResiduePtr());
  sc_frame_residue_.assign(num_residues, sidechain::FrameResiduePtr());
  if (use_frm_) {
    frm_rotamer_group_.assign(num_residues, sidechain::FRMRotamerGroupPtr());
  } else {         
    rrm_rotamer_group_.assign(num_residues, sidechain::RRMRotamerGroupPtr());
  }
  if (add_cyd_rotamers_) {
    cyd_frm_rotamer_group_.assign(num_residues, sidechain::FRMRotamerGroupPtr());
  }
}

void SidechainEnvListener::ResetEnvironment(const loop::AllAtomEnv& base_env) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                        "SidechainEnvListener::ResetEnvironment", 2);

  loop::ConstAllAtomPositionsPtr all_pos = base_env.GetAllPosData();
  for (uint res_idx = 0; res_idx < all_pos->GetNumResidues(); ++res_idx) {
    SetResidue_(all_pos, res_idx);
  }
}

void SidechainEnvListener::UpdateEnvironment(
                                      const loop::AllAtomEnv& base_env, 
                                      const std::vector<uint>& res_indices) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                        "SidechainEnvListener::UpdateEnvironment", 2);
  
  // check what to update -> changed at index i affects also i-1,i+1!
  std::set<uint> to_change;
  for (uint i = 0; i < res_indices.size(); ++i) {
    if (!n_ter_[res_indices[i]]) to_change.insert(res_indices[i]-1);
    to_change.insert(res_indices[i]);
    if (!c_ter_[res_indices[i]]) to_change.insert(res_indices[i]+1);
  }
  // update it
  loop::ConstAllAtomPositionsPtr all_pos = base_env.GetAllPosData();
  for (std::set<uint>::const_iterator it = to_change.begin();
       it != to_change.end(); ++it) {
    SetResidue_(all_pos, *it);
  }
}

void SidechainEnvListener::SetResidue_(loop::ConstAllAtomPositionsPtr all_pos,
                                       const uint res_idx) {
  const sidechain::RotamerID r_id = r_id_[res_idx];
  // get atom indices for backbone
  const uint idx_N  = all_pos->GetIndex(res_idx, loop::BB_N_INDEX);
  const uint idx_CA = all_pos->GetIndex(res_idx, loop::BB_CA_INDEX);
  const uint idx_C  = all_pos->GetIndex(res_idx, loop::BB_C_INDEX);
  const uint idx_O  = all_pos->GetIndex(res_idx, loop::BB_O_INDEX);
  const uint idx_CB = all_pos->GetIndex(res_idx, loop::BB_CB_INDEX);
  const bool bb_set =   all_pos->IsSet(idx_N) && all_pos->IsSet(idx_CA)
                     && all_pos->IsSet(idx_C) && all_pos->IsSet(idx_O)
                     && (r_id == sidechain::GLY || all_pos->IsSet(idx_CB));
  const bool all_set = all_pos->IsAllSet(res_idx);
  // we only add data if BB is set
  if (bb_set) {
    // get positions
    const geom::Vec3& ca_pos = all_pos->GetPos(idx_CA);
    geom::Vec3 cb_pos;
    if (r_id != sidechain::GLY)  cb_pos = all_pos->GetPos(idx_CB);
    
    // update spatial organizer
    if (bb_frame_residue_[res_idx]) {
      // update
      const geom::Vec3 old_pos = env_data_[res_idx].pos;
      env_data_[res_idx].pos = (r_id == sidechain::GLY) ? ca_pos : cb_pos;
      env_.Reset(&env_data_[res_idx], old_pos, env_data_[res_idx].pos);
    } else {
      // set
      env_data_[res_idx].pos = (r_id == sidechain::GLY) ? ca_pos : cb_pos;
      env_.Add(&env_data_[res_idx], env_data_[res_idx].pos);
    }

    // dihedrals
    if (n_ter_[res_idx]) phi_angle_[res_idx] = -1.0472;
    else phi_angle_[res_idx] = all_pos->GetPhiTorsion(res_idx);
    if (c_ter_[res_idx]) psi_angle_[res_idx] = -0.7854;
    else psi_angle_[res_idx] = all_pos->GetPsiTorsion(res_idx);

    // set backbone frame res.
    bb_frame_residue_[res_idx]
      = rot_constructor_.ConstructBackboneFrameResidue(*all_pos, res_idx, 
                                                       r_id, res_idx, 
                                                       phi_angle_[res_idx], 
                                                       psi_angle_[res_idx],
                                                       n_ter_[res_idx], 
                                                       c_ter_[res_idx]);
    
    // set sidechain frame res. if all set
    if (all_set) {
      sc_frame_residue_[res_idx]
        = rot_constructor_.ConstructSidechainFrameResidue(*all_pos, res_idx, 
                                                           r_id, res_idx,
                                                           phi_angle_[res_idx],
                                                           psi_angle_[res_idx],
                                                           n_ter_[res_idx],
                                                           c_ter_[res_idx]);
    } else {
      sc_frame_residue_[res_idx].reset();
    }
    // set rotamer if needed
    if (all_rotamers_ || !all_set) {
      SetRotamer_(all_pos, r_id, res_idx);
    } else {
      if (use_frm_) frm_rotamer_group_[res_idx].reset();
      else          rrm_rotamer_group_[res_idx].reset();
    }
    // set extra cystein rotamer if needed
    if (add_cyd_rotamers_ && r_id == sidechain::CYS) {
      CreateRotamerGroup(cyd_frm_rotamer_group_[res_idx], all_pos,
                         sidechain::CYD, res_idx);
    }
    
  } else {
    // clear stuff
    if (bb_frame_residue_[res_idx]) {
      env_.Remove(&env_data_[res_idx], env_data_[res_idx].pos);
      bb_frame_residue_[res_idx].reset();
      sc_frame_residue_[res_idx].reset();
      cyd_frm_rotamer_group_[res_idx].reset();
      if (use_frm_) {
        frm_rotamer_group_[res_idx].reset();
      } else {
        rrm_rotamer_group_[res_idx].reset();
      }
    }
  }
}

void SidechainEnvListener::SetRotamer_(loop::ConstAllAtomPositionsPtr all_pos,
                                       const sidechain::RotamerID r_id,
                                       const uint res_idx) {
  // add rotamer anyways -> potentially need to change ID
  sidechain::RotamerID rg_r_id = r_id;
  if (r_id == sidechain::CYS) rg_r_id = sidechain::CYH;
  if (r_id == sidechain::PRO) {
    // check omega torsion
    Real omega = 4;
    if (!n_ter_[res_idx]) omega = all_pos->GetOmegaTorsion(res_idx, 4);
    if (omega != 4) {
      rg_r_id = (std::abs(omega) < 1.57) ? sidechain::CPR : sidechain::TPR;
    }
  }
  // note: most time spent here to construct new and destruct old
  // -> 4800 construct/destructs: 2.17s (CalculateInternalEnergies: 0.003s)
  //    -> same with RRM:         0.30s
  //    -> RRM w/o BBdep:         0.23s
  if (use_frm_) {
    CreateRotamerGroup(frm_rotamer_group_[res_idx], all_pos,
                       rg_r_id, res_idx);
  } else {
    CreateRotamerGroup(rrm_rotamer_group_[res_idx], all_pos,
                       rg_r_id, res_idx);
  }
}

}} // ns
