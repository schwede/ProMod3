// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/backbone_relaxer.hh>
#include <promod3/core/runtime_profiling.hh>
#include <promod3/loop/amino_acid_atoms.hh>
#include <ost/mol/xcs_editor.hh>
#include <ost/mol/mm/forcefield.hh>
#include <ost/mol/mm/system_creator.hh>
#include <ost/mol/mm/steep.hh>
#include <set>
#include <exception>
#include <limits>


namespace{

template<bool two_residues>
bool ValidIdx(uint ff_idx, uint start_idx, uint max_idx, uint split_idx,
              uint start_idx_2, uint max_idx_2, uint& result_idx){
  if(two_residues){
    if(ff_idx < split_idx){
      // it's in the first residue
      result_idx = start_idx + ff_idx;
      return ff_idx <= max_idx;
    }
    else{
      // it's in the second residue
      ff_idx -= split_idx;
      result_idx = start_idx_2 + ff_idx;
      return ff_idx <= max_idx_2;
    }
  }
  else{
    result_idx = start_idx + ff_idx;
    return ff_idx <= max_idx;
  }
}


template<bool two_residues>
void AddConnectivity(ost::mol::mm::TopologyPtr top, uint start_idx, uint max_idx,             
                     const promod3::loop::ForcefieldConnectivity& connectivity,
                     uint start_idx_2 = 0, uint max_idx_2 = 0, 
                     uint split_idx = 0){

  uint idx_1, idx_2, idx_3, idx_4;
  // need to keep track of exclusions while adding bonds / angles
  typedef ost::mol::mm::Index<2> ExclusionIndex;
  typedef std::set<ExclusionIndex> ExclusionSet;
  ExclusionSet exclusions;
  
  // do harmonic bonds
  for(std::vector<promod3::loop::ForcefieldBondInfo>::const_iterator i =
      connectivity.harmonic_bonds.begin(); 
      i != connectivity.harmonic_bonds.end(); ++i){

    bool all_indices_valid = 
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2);

    if(all_indices_valid){
      top->AddHarmonicBond(idx_1, idx_2, 
                           i->bond_length, i->force_constant);  
      exclusions.insert(ExclusionIndex(idx_1, idx_2));    
    }
  }

  // do harmonic angles
  for(std::vector<promod3::loop::ForcefieldHarmonicAngleInfo>::const_iterator i =
      connectivity.harmonic_angles.begin(); 
      i != connectivity.harmonic_angles.end(); ++i){

    bool all_indices_valid =
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2) &&
    ValidIdx<two_residues>(i->index_three, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_3);

    if(all_indices_valid){ 
      top->AddHarmonicAngle(idx_1, idx_2, idx_3,
                            i->angle, i->force_constant);
      exclusions.insert(ExclusionIndex(idx_1, idx_3));
    }
  }

  // do urey-bradley angles
  for(std::vector<promod3::loop::ForcefieldUreyBradleyAngleInfo>::const_iterator i =
      connectivity.urey_bradley_angles.begin(); 
      i != connectivity.urey_bradley_angles.end(); ++i){

    bool all_indices_valid =
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2) &&
    ValidIdx<two_residues>(i->index_three, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_3);

    if(all_indices_valid){ 
      top->AddUreyBradleyAngle(idx_1, idx_2, idx_3,
                               i->angle, i->angle_force_constant,
                               i->bond_length, i->bond_force_constant);
      exclusions.insert(ExclusionIndex(idx_1, idx_3));
    }
  }

  // do dihedrals
  for(std::vector<promod3::loop::ForcefieldPeriodicDihedralInfo>::const_iterator i =
      connectivity.periodic_dihedrals.begin(); 
      i != connectivity.periodic_dihedrals.end(); ++i){

    bool all_indices_valid =
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2) &&
    ValidIdx<two_residues>(i->index_three, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_3) &&
    ValidIdx<two_residues>(i->index_four, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_4);
                           
    if(all_indices_valid){ 
      top->AddPeriodicDihedral(idx_1, idx_2, idx_3, idx_4,
                               i->multiplicity, i->phase, i->force_constant);
    }
  }

  // do periodic impropers
  for(std::vector<promod3::loop::ForcefieldPeriodicDihedralInfo>::const_iterator i =
      connectivity.periodic_impropers.begin(); 
      i != connectivity.periodic_impropers.end(); ++i){

    bool all_indices_valid =
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2) &&
    ValidIdx<two_residues>(i->index_three, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_3) &&
    ValidIdx<two_residues>(i->index_four, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_4);
                           
    if(all_indices_valid){                                        
      top->AddPeriodicImproper(idx_1, idx_2, idx_3, idx_4,
                               i->multiplicity, i->phase, i->force_constant);
    }
  }

  // do harmonic impropers
  for(std::vector<promod3::loop::ForcefieldHarmonicImproperInfo>::const_iterator i =
      connectivity.harmonic_impropers.begin(); 
      i != connectivity.harmonic_impropers.end(); ++i){

    bool all_indices_valid =
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2) &&
    ValidIdx<two_residues>(i->index_three, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_3) &&
    ValidIdx<two_residues>(i->index_four, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_4);
                           
    if(all_indices_valid){ 
      top->AddHarmonicImproper(idx_1, idx_2, idx_3, idx_4,
                               i->angle, i->force_constant);
    }
  }

  // do lj-pairs
  for(std::vector<promod3::loop::ForcefieldLJPairInfo>::const_iterator i =
      connectivity.lj_pairs.begin(); 
      i != connectivity.lj_pairs.end(); ++i){

    bool all_indices_valid = 
    ValidIdx<two_residues>(i->index_one, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_1) &&
    ValidIdx<two_residues>(i->index_two, start_idx, max_idx, split_idx,
                           start_idx_2, max_idx_2, idx_2);

    if(all_indices_valid){
      top->AddLJPair(idx_1, idx_2, 
                     i->sigma, i->epsilon);
    }
  }

  // do exclusions
  for(ExclusionSet::iterator i = exclusions.begin(); i != exclusions.end(); ++i){
    top->AddExclusion((*i)[0], (*i)[1]);
  }
}

} // ns


namespace promod3 { namespace modelling {

BackboneRelaxer::BackboneRelaxer(const loop::BackboneList& bb_list,
                                 promod3::loop::ForcefieldLookupPtr ff,
                                 bool fix_nterm, bool fix_cterm)
                                 : nonbonded_cutoff_(-1) {

  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneRelaxer::BackboneRelaxer", 2);

  if(bb_list.size() < 2){
    throw promod3::Error("bb_list must be at least of length 2 for "
                         "BackboneRelaxer!");
  }

  s_.resize(bb_list.size());
  for (uint i = 0; i < bb_list.size(); ++i) {
    if (bb_list.GetAA(i) == ost::conop::XXX) {
      throw promod3::Error("Cannot Initialize relaxer with backbone item of aa UNKNOWN!");
    }
    s_[i] = bb_list.GetAA(i);
  }

  top_ = CreateBBListTop(bb_list, ff, fix_nterm, fix_cterm);
  positions_.resize(top_->GetNumParticles());
}

void BackboneRelaxer::AddNRestraint(uint idx, const geom::Vec3& pos, 
                                     Real force_constant){

  uint res_start_idx = this->GetResidueStartIdx(idx);
  top_->AddHarmonicPositionRestraint(res_start_idx + promod3::loop::BB_N_INDEX, 
                                     pos, force_constant);

  //invalidate simulation, so it will be newly created at the next run
  simulation_ = ost::mol::mm::SimulationPtr();
}

void BackboneRelaxer::AddCARestraint(uint idx, const geom::Vec3& pos, 
                                     Real force_constant){

  uint res_start_idx = this->GetResidueStartIdx(idx);
  top_->AddHarmonicPositionRestraint(res_start_idx + promod3::loop::BB_CA_INDEX, 
                                     pos, force_constant);

  //invalidate simulation, so it will be newly created at the next run
  simulation_ = ost::mol::mm::SimulationPtr();
}

void BackboneRelaxer::AddCBRestraint(uint idx, const geom::Vec3& pos, 
                                     Real force_constant){

  if(s_[idx] == ost::conop::GLY) {
    throw promod3::Error("Cannot add CB restraint for GLY!");
  }

  uint res_start_idx = this->GetResidueStartIdx(idx);
  top_->AddHarmonicPositionRestraint(res_start_idx + promod3::loop::BB_CB_INDEX, 
                                     pos, force_constant);

  //invalidate simulation, so it will be newly created at the next run
  simulation_ = ost::mol::mm::SimulationPtr();
}

void BackboneRelaxer::AddCRestraint(uint idx, const geom::Vec3& pos, 
                                    Real force_constant){

  uint res_start_idx = this->GetResidueStartIdx(idx);
  top_->AddHarmonicPositionRestraint(res_start_idx + promod3::loop::BB_C_INDEX, 
                                     pos, force_constant);

  //invalidate simulation, so it will be newly created at the next run
  simulation_ = ost::mol::mm::SimulationPtr();
}

void BackboneRelaxer::AddORestraint(uint idx, const geom::Vec3& pos, 
                                    Real force_constant){

  uint res_start_idx = this->GetResidueStartIdx(idx);
  top_->AddHarmonicPositionRestraint(res_start_idx + promod3::loop::BB_O_INDEX, 
                                     pos, force_constant);
  
  //invalidate simulation, so it will be newly created at the next run
  simulation_ = ost::mol::mm::SimulationPtr();
}

void BackboneRelaxer::SetNonBondedCutoff(Real nonbonded_cutoff) {
  // set value and invalidate simulation (will be newly created at the next run)
  nonbonded_cutoff_ = nonbonded_cutoff;
  simulation_ = ost::mol::mm::SimulationPtr();
}

Real BackboneRelaxer::Run(loop::BackboneList& bb_list, int steps,
                          Real stop_criterion) {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneRelaxer::Run", 2);

  if (bb_list.size() != s_.size()) {
    throw promod3::Error("Size of bblist is not consistent with the size the "
                         "optimizer has been initialized with.");
  }

  // check if the sequence is consistent
  for (uint i = 0; i < s_.size(); ++i) {
    if (s_[i] != bb_list.GetAA(i)) {
      throw promod3::Error("Amino Acid sequence is not consistent with what "
                           "the optimizer has been initialized with");
    }
  }

  //check, wether simulation has already been created
  if (!simulation_) this->Init();

  this->SetSimulationPositions(bb_list);

  // guard against OpenMM error that is thrown when positions become NaN
  // => typicall when you particles are (almost) on top of each other
  try {
    simulation_->ApplySD(stop_criterion, steps);
  } catch(std::exception& e) {
    return std::numeric_limits<Real>::infinity();
  }

  this->ExtractSimulationPositions(bb_list);

  return simulation_->GetPotentialEnergy();
}

void BackboneRelaxer::Init() {
  core::ScopedTimerPtr prof = core::StaticRuntimeProfiler::StartScoped(
                                "BackboneRelaxer::Init", 2);

  ost::mol::EntityHandle dummy_ent = ost::mol::CreateEntity();
  ost::mol::XCSEditor ed = dummy_ent.EditXCS(ost::mol::BUFFERED_EDIT);
  ost::mol::ChainHandle dummy_chain = ed.InsertChain("A"); 
  ost::mol::ResidueHandle dummy_res = ed.AppendResidue(dummy_chain,"RES");
  
  for (geom::Vec3List::iterator i = positions_.begin();
       i != positions_.end(); ++i) {
    ed.InsertAtom(dummy_res, "A", *i);
  }

  ed.UpdateICS();

  // create standard Settings
  ost::mol::mm::SettingsPtr settings(new ost::mol::mm::Settings);
  // shall we add a cutoff?
  if (nonbonded_cutoff_ < 0) {
    settings->nonbonded_method = ost::mol::mm::NoCutoff;
  } else {
    settings->nonbonded_method = ost::mol::mm::CutoffNonPeriodic;
    settings->nonbonded_cutoff = nonbonded_cutoff_;
  }
  // NOTE: for OpenMM 6.1 and 7.1.1, using CPU is slower so we don't do this...
  // settings->platform = ost::mol::mm::CPU;
  // if (ost::mol::mm::Simulation::IsPlatformAvailable(settings)) {
  //   settings->cpu_properties["CpuThreads"] = "1";
  // } else {
  //   // fallback to reference
  //   settings->platform = ost::mol::mm::Reference;
  // }

  simulation_ = ost::mol::mm::SimulationPtr(
                  new ost::mol::mm::Simulation(top_, dummy_ent, settings));
}

void BackboneRelaxer::SetSimulationPositions(const loop::BackboneList& bb_list)
{
  uint res_start_idx = 0;

  for (uint i = 0; i < bb_list.size(); ++i) {
    positions_[res_start_idx + promod3::loop::BB_N_INDEX] = bb_list.GetN(i); 
    positions_[res_start_idx + promod3::loop::BB_CA_INDEX] = bb_list.GetCA(i);
    positions_[res_start_idx + promod3::loop::BB_C_INDEX] = bb_list.GetC(i);
    positions_[res_start_idx + promod3::loop::BB_O_INDEX] = bb_list.GetO(i);
    if (s_[i] != ost::conop::GLY) {
      positions_[res_start_idx + promod3::loop::BB_CB_INDEX] = bb_list.GetCB(i);
      res_start_idx += 5;
    }
    else{
      res_start_idx += 4;
    }
  }
  simulation_->SetPositions(positions_);
}

void BackboneRelaxer::ExtractSimulationPositions(loop::BackboneList& bb_list) {

  uint res_start_idx = 0;
  positions_ = simulation_->GetPositions();

  for (uint i = 0; i < bb_list.size(); ++i) {
    bb_list.SetN(i, positions_[res_start_idx + promod3::loop::BB_N_INDEX]);
    bb_list.SetCA(i, positions_[res_start_idx + promod3::loop::BB_CA_INDEX]);
    bb_list.SetC(i, positions_[res_start_idx + promod3::loop::BB_C_INDEX]);
    bb_list.SetO(i, positions_[res_start_idx + promod3::loop::BB_O_INDEX]);
    if(s_[i] != ost::conop::GLY){
      bb_list.SetCB(i, positions_[res_start_idx + promod3::loop::BB_CB_INDEX]);
      res_start_idx += 5;
    }
    else{
      geom::Vec3 cb_coord;
      promod3::core::ConstructCBetaPos(bb_list.GetN(i), bb_list.GetCA(i), 
                                       bb_list.GetC(i), cb_coord);
      bb_list.SetCB(i, cb_coord);
      res_start_idx += 4;
    }
  }
}

uint BackboneRelaxer::GetResidueStartIdx(uint idx) const{
  
  if(idx >= s_.size()){
    throw promod3::Error("Invalid idx observed when processing BackboneRelaxer!");
  }
  uint residue_start_idx = 0;
  for(uint i = 0; i < idx; ++i){
    if(s_[i] == ost::conop::GLY) residue_start_idx += 4;
    else residue_start_idx += 5;
  }

  return residue_start_idx;
}

ost::mol::mm::TopologyPtr BackboneRelaxer::CreateBBListTop(
    const loop::BackboneList& bb_list, 
    promod3::loop::ForcefieldLookupPtr ff,
    bool fix_nterm, bool fix_cterm) {

  std::vector<Real> masses;
  std::vector<Real> charges;
  std::vector<Real> sigmas;
  std::vector<Real> epsilons;
  std::vector<uint> res_start_indices;
  std::vector<promod3::loop::ForcefieldAminoAcid> ff_s;

  const uint bb_list_size = bb_list.size();
  masses.reserve(5 * bb_list_size);
  charges.reserve(5 * bb_list_size);
  sigmas.reserve(5 * bb_list_size);
  epsilons.reserve(5 * bb_list_size);
  res_start_indices.reserve(bb_list_size);
  ff_s.reserve(bb_list_size);

  // fill masses / sigmas / epsilon
  for (uint i = 0; i < bb_list_size; ++i) {

    ff_s.push_back(promod3::loop::ForcefieldAminoAcid(s_[i]));
    res_start_indices.push_back(masses.size());

    const std::vector<Real>& ff_masses = 
    ff->GetMasses(ff_s.back(), false, false);
    const std::vector<Real>& ff_sigmas = 
    ff->GetSigmas(ff_s.back(), false, false);
    const std::vector<Real>& ff_epsilons = 
    ff->GetEpsilons(ff_s.back(), false, false);
    
    if (s_[i] == ost::conop::GLY) {
      masses.push_back(ff_masses[promod3::loop::BB_N_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_CA_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_C_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_O_INDEX]);

      sigmas.push_back(ff_sigmas[promod3::loop::BB_N_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_CA_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_C_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_O_INDEX]);

      epsilons.push_back(ff_epsilons[promod3::loop::BB_N_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_CA_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_C_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_O_INDEX]);
   
    } else {
      masses.push_back(ff_masses[promod3::loop::BB_N_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_CA_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_C_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_O_INDEX]);
      masses.push_back(ff_masses[promod3::loop::BB_CB_INDEX]); 

      sigmas.push_back(ff_sigmas[promod3::loop::BB_N_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_CA_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_C_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_O_INDEX]);
      sigmas.push_back(ff_sigmas[promod3::loop::BB_CB_INDEX]);    

      epsilons.push_back(ff_epsilons[promod3::loop::BB_N_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_CA_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_C_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_O_INDEX]);
      epsilons.push_back(ff_epsilons[promod3::loop::BB_CB_INDEX]);    
    }  
  }

  if (fix_nterm) {
    //set nterm rigid but let the c and o atoms move
    masses[promod3::loop::BB_N_INDEX] = 0.0;
    masses[promod3::loop::BB_CA_INDEX] = 0.0;
    if (s_[0] != ost::conop::GLY) masses[promod3::loop::BB_CB_INDEX] = 0.0;
  }

  if (fix_cterm) {
    //set cterm rigid but let the n atom move
    masses[res_start_indices.back() + promod3::loop::BB_CA_INDEX] = 0.0;
    masses[res_start_indices.back() + promod3::loop::BB_C_INDEX] = 0.0;
    masses[res_start_indices.back() + promod3::loop::BB_O_INDEX] = 0.0;
    if (s_.back() != ost::conop::GLY) {
      masses[res_start_indices.back() + promod3::loop::BB_CB_INDEX] = 0.0;
    }
  }

  //initialize mm-topology
  ost::mol::mm::TopologyPtr top(new ost::mol::mm::Topology(masses));
  //electrostatics get neglected with all charges set to zero
  charges.resize(sigmas.size(), 0.0);
  top->SetCharges(charges);
  top->SetSigmas(sigmas);
  top->SetEpsilons(epsilons);
  top->SetFudgeQQ(ff->GetFudgeQQ());
  top->SetFudgeLJ(ff->GetFudgeLJ());

  // do internal interactions
  for(uint i = 0; i < bb_list_size; ++i){

    uint max_idx = promod3::loop::BB_CB_INDEX;
    if(s_[i] == ost::conop::GLY) max_idx = promod3::loop::BB_O_INDEX;

    const promod3::loop::ForcefieldConnectivity& connectivity =
    ff->GetInternalConnectivity(ff_s[i],false,false);

    AddConnectivity<false>(top, res_start_indices[i], max_idx, connectivity);
  }


  // do inter residue interactions
  for(uint i = 0; i < bb_list_size - 1; ++i){

    uint max_idx = promod3::loop::BB_CB_INDEX;
    uint max_idx_2 = promod3::loop::BB_CB_INDEX;
    if(s_[i] == ost::conop::GLY) max_idx = promod3::loop::BB_O_INDEX;
    if(s_[i+1] == ost::conop::GLY) max_idx_2 = promod3::loop::BB_O_INDEX;

    const promod3::loop::ForcefieldConnectivity& connectivity =
    ff->GetPeptideBoundConnectivity(ff_s[i], ff_s[i+1], false, false);

    AddConnectivity<true>(top, res_start_indices[i], max_idx, connectivity,
                          res_start_indices[i+1], max_idx_2, 
                          ff->GetNumAtoms(ff_s[i], false, false));
  }

  return top;
}

}}
