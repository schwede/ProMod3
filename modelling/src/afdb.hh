// Copyright (c) 2013-2023, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PM3_MODELLING_AFDB_HH
#define PM3_MODELLING_AFDB_HH

#include <ost/mol/mol.hh>
#include <ost/seq/sequence_list.hh>
#include <promod3/core/message.hh>


namespace promod3 { namespace modelling {

void SeqToPentamerIndices(const String& seq, bool unique,
                          std::vector<int>& indices);

void CreatePentaMatch(const ost::seq::SequenceList& seq_list,
                      const String& db_path,
                      bool entries_from_seqnames);

uint64_t CreateAFDBIdx(const String& uniprot_ac, int fragment, int version);
}} //ns

#endif
