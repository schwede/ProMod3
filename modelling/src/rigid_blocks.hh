// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef PROMOD_MODELLING_RIGID_BLOCKS_HH
#define PROMOD_MODELLING_RIGID_BLOCKS_HH

#include <ost/seq/alignment_handle.hh>

#include <promod3/core/message.hh>
#include <promod3/core/superpose.hh>
#include <promod3/loop/backbone.hh>


namespace promod3 { namespace modelling {

void RigidBlocks(const ost::seq::AlignmentHandle& aln, uint seq_one_idx, 
	             uint seq_two_idx, uint window_length, uint max_iterations, 
	             Real distance_thresh, Real cluster_thresh, 
	             std::vector<std::vector<uint> >& indices,
	             std::vector<geom::Mat4>& transformations);

void RigidBlocks(const promod3::loop::BackboneList& bb_list_one, 
	             const promod3::loop::BackboneList& bb_list_two,
	             uint window_length, uint max_iterations, 
	             Real distance_thresh, Real cluster_thresh, 
	             std::vector<std::vector<uint> >& indices,
	             std::vector<geom::Mat4>& transformations);

void RigidBlocks(const geom::Vec3List& pos_one,
	             const geom::Vec3List& pos_two,
	             uint window_length, uint max_iterations,
	             Real distance_thresh, Real cluster_thresh,
	             std::vector<std::vector<uint> >& indices,
	             std::vector<geom::Mat4>& transformations);

}} //ns

#endif
