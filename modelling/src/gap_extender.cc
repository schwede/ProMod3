// Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
//                          Biozentrum - University of Basel
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <promod3/modelling/gap_extender.hh>
#include <algorithm>

using namespace ost::mol;
using namespace ost;


namespace{
  bool CompareScoredElements(const std::pair<promod3::modelling::StructuralGap,Real>& a,
                             const std::pair<promod3::modelling::StructuralGap,Real>& b){
    return a.second < b.second;
  }

}


namespace promod3{ namespace modelling{

bool GapExtender::Extend() {

  ShiftExtension old_scheme = scheme_;
  std::pair<int,int> new_resnums = scheme_.Extend();
  ost::mol::ResNum new_n_num = ost::mol::ResNum(new_resnums.first);
  ost::mol::ResNum new_c_num = ost::mol::ResNum(new_resnums.second);

  ost::mol::ResidueHandle n = gap.GetChain().FindResidue(new_n_num);
  ost::mol::ResidueHandle c = gap.GetChain().FindResidue(new_c_num);

  if (!(n.IsValid() && c.IsValid())) {
    // rollback
    scheme_ = old_scheme;
    return false;
  } else {
    // update gap
    gap.before = n;
    gap.after = c;

    int gap_length = new_resnums.second - new_resnums.first - 1;
    gap.sequence = seqres_.substr(new_resnums.first, gap_length);
    return true;
  }
}


FullGapExtender::FullGapExtender(StructuralGap& g,
                                 const String& seqres,
                                 const int max_length)
                                 : gap(g)
                                 , seqres_(seqres)
                                 , scheme_(gap.before.GetNumber().GetNum(),
                                           gap.after.GetNumber().GetNum())
{
  SetMaxLength_(max_length);
}

FullGapExtender::FullGapExtender(StructuralGap& g,
                                 const ost::seq::SequenceHandle seqres,
                                 const int max_length)
                                 : gap(g)
                                 , seqres_(seqres.GetString())
                                 , scheme_(gap.before.GetNumber().GetNum(),
                                           gap.after.GetNumber().GetNum())
{
  SetMaxLength_(max_length);
}

void FullGapExtender::SetMaxLength_(const int max_length)
{
  // note: -2 because we need valid N- and C-termini
  max_length_ = seqres_.length()-2;
  if (max_length >= 0) max_length_ = std::min(max_length_, (uint)max_length);
}

bool FullGapExtender::Extend()
{
  // find next acceptable gap
  std::pair<int,int> new_resnums;
  uint gap_length;
  ost::mol::ResidueHandle n,c;
  do {
    new_resnums = scheme_.Extend();
    ost::mol::ResNum new_n_num = ost::mol::ResNum(new_resnums.first);
    ost::mol::ResNum new_c_num = ost::mol::ResNum(new_resnums.second);
    n = gap.GetChain().FindResidue(new_n_num);
    c = gap.GetChain().FindResidue(new_c_num);
    gap_length = new_resnums.second - new_resnums.first - 1;
  } while (!(n.IsValid() && c.IsValid()) && (gap_length <= max_length_));
  // POST: (n.IsValid() && c.IsValid()) || (gap_length > max_length_)

  // check if we failed
  if (gap_length > max_length_) {
    return false;
  } else {
    // update gap
    gap.before = n;
    gap.after = c;
    gap.sequence = seqres_.substr(new_resnums.first, gap_length);
    return true;
  }
}


ScoringGapExtender::ScoringGapExtender(StructuralGap& g,
                                       Real extension_penalty,
                                       const std::vector<Real>& penalties,
                                       const String& seqres,
                                       const int max_length)
                                       : gap(g)
                                       , actual_gap_index_(0)
{
  this->Init(extension_penalty, penalties, seqres, max_length);
}

ScoringGapExtender::ScoringGapExtender(StructuralGap& g,
                                       Real extension_penalty,
                                       const std::vector<Real>& penalties,
                                       const ost::seq::SequenceHandle& seqres,
                                       const int max_length)
                                       : gap(g)
                                       , actual_gap_index_(0)
{
  this->Init(extension_penalty, penalties, seqres.GetString(), max_length);
}

void ScoringGapExtender::Init(Real extension_penalty,
                              const std::vector<Real>& penalties,
                              const String& seqres,
                              const int max_length)
{
  std::vector<std::pair<StructuralGap,Real> > initial_gaps;
  uint initial_length = gap.GetLength();

  StructuralGap extended_gap = gap.Copy();
  BaseGapExtenderPtr classical_extender;
  if (max_length == -2) {
    classical_extender = BaseGapExtenderPtr(
      new GapExtender(extended_gap, seqres));
  } else {
    classical_extender = BaseGapExtenderPtr(
      new FullGapExtender(extended_gap, seqres, max_length));
  }

  //get all possible gaps from the extender
  while(true){
    if(!classical_extender->Extend()) break;

    //we don't like terminal gaps...
    if(extended_gap.IsTerminal()) continue;

    //directly check, whether there is an invalid residue number
    //regarding the penalties input
    if(extended_gap.before.GetNumber().GetNum() < 1){
      std::stringstream ss;
      ss << "Invalid residue number observed when setting up ScoringGapExtender. ";
      ss << "Minimal residue number must be 1!";
      throw promod3::Error(ss.str());
    }
    if(static_cast<uint>(extended_gap.after.GetNumber().GetNum()) > penalties.size()){
      std::stringstream ss;
      ss << "Invalid residue number observed when setting up ScoringGapExtender. ";
      ss << "Maximal c-terminal residue number must be smaller than the size ";
      ss << "of the provided penalty scores!";
      throw promod3::Error(ss.str());
    }

    Real penalty = 0;

    //add extension penalty
    uint new_gap_length = extended_gap.GetLength();
    penalty += (new_gap_length-initial_length)*extension_penalty;

    //add penalty towards n-ter
    uint actual_num = extended_gap.before.GetNumber().GetNum()+1;
    uint end_num = gap.before.GetNumber().GetNum();
    for(; actual_num <= end_num; ++actual_num){
      penalty += penalties[actual_num - 1];
    }

    //add penalty towards c-ter
    actual_num = gap.after.GetNumber().GetNum();
    end_num = extended_gap.after.GetNumber().GetNum();
    for(; actual_num < end_num; ++actual_num){
      penalty += penalties[actual_num - 1];
    }

    initial_gaps.push_back(std::make_pair(extended_gap.Copy(), penalty));
  }

  std::sort(initial_gaps.begin(), initial_gaps.end(),CompareScoredElements);

  for(std::vector<std::pair<StructuralGap, Real> >::iterator i = initial_gaps.begin();
      i != initial_gaps.end(); ++i){
    gaps_.push_back(i->first);
  }

}

bool ScoringGapExtender::Extend(){
  if(actual_gap_index_ >= gaps_.size()) return false;
  gap = gaps_[actual_gap_index_];
  ++actual_gap_index_;
  return true;
}

}}//ns
