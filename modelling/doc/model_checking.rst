..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Model Checking
================================================================================

.. currentmodule:: promod3.modelling

This chapter describes additional functionality to check models. Some of this
functionality is used within the modelling pipeline.

Detecting Ring Punches
--------------------------------------------------------------------------------

.. autofunction:: GetRings

.. autofunction:: GetRingPunches

.. autofunction:: HasRingPunches

.. autofunction:: FilterCandidates

.. autofunction:: FilterCandidatesWithSC


Detecting Non-Planar Rings
--------------------------------------------------------------------------------

.. autofunction:: GetNonPlanarRings

.. autofunction:: HasNonPlanarRings


Model Checking With MolProbity
--------------------------------------------------------------------------------

.. autofunction:: RunMolProbity

.. autofunction:: RunMolProbityEntity

.. autofunction:: ReportMolProbityScores