..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Fitting Loops Into Gaps
================================================================================

.. currentmodule:: promod3.modelling

Loops often need to undergo conformational changes to fit into gaps defined by
stem residues. |project| implements two algorithms performing this task:

  * Cyclic coordinate descent (CCD) [canutescu2003]_
  * Kinematic closure (KIC) [coutsias2005]_

In case of small gaps or small issues in the loop you might also consider the
:class:`BackboneRelaxer`.


CCD
--------------------------------------------------------------------------------

The |project| implementation of the cyclic coordinate descent first superposes
the n-stem of the input loop with the provided n-stem positions. In every
iteration of the algorithm, we loop over all residues of the loop and find the
ideal phi/psi angles to minimize the RMSD between the c-stem and the target
c-stem positions. Iterations continue until a c-stem RMSD threshold is reached
or the number of iterations hits a limit. By performing CCD, unfavorable
backbone dihedral pairs can be introduced. It is therefore optionally possible
to use torsion samplers to guide the iterative process.  In this case, the
algorithm calculates the probability of observing the dihedral pair before and
after performing the phi/psi update. If the fraction after/before is smaller
than a uniform random number in the range [0,1[, the proposed dihedral pair gets
rejected. Please note, that this increases the probability of non-convergence.

.. class:: CCD

  Class, that sets up everything you need to perform a particular loop closing
  action.

  .. method:: CCD(sequence, n_stem, c_stem, torsion_sampler, max_steps, \
                  rmsd_cutoff, seed)

    All runs with this CCD object will be with application of torsion samplers 
    to avoid moving into unfavourable regions of the backbone dihedrals.

    :param sequence:    Sequence of the backbones to be closed
    :param n_stem:      Residue defining the n_stem.
                        If the residue before *n_stem* doesn't exist, the
                        torsion sampler will use a default residue (ALA) and
                        and phi angle (-1.0472) to evaluate the first angle.
    :param c_stem:      Residue defining the c_stem.
                        If the residue after *c_stem* doesn't exist, the
                        torsion sampler will use a default residue (ALA) and
                        psi angle (-0.7854) to evaluate the last angle.
    :param torsion_sampler: To extract probabilities for the analysis of the
                            backbone dihedrals. Either a list of torsion
                            samplers (one for for every residue of the loop to
                            be closed) or a single one (used for all residues).
    :param max_steps:   Maximal number of iterations
    :param rmsd_cutoff: The algorithm stops as soon as the c_stem of the loop to
                        be closed has RMSD below the *c_stem* 
    :param seed:        Seed of random number generator to decide whether new phi/psi pair
                        should be accepted.


    :type sequence:     :class:`str`
    :type n_stem:       :class:`ost.mol.ResidueHandle`
    :type c_stem:       :class:`ost.mol.ResidueHandle` 
    :type torsion_sampler: :class:`~promod3.loop.TorsionSampler` / :class:`list`
                           of :class:`~promod3.loop.TorsionSampler`
    :type max_steps:    :class:`int`
    :type rmsd_cutoff:  :class:`float`
    :type seed:         :class:`int`

    :raises:  :exc:`~exceptions.RuntimeError` if a list of torsion samplers is
              given with inconsistent length regarding the sequence. Another
              requirement is that all backbone atoms of the stems must be
              present.

  .. method:: CCD(n_stem, c_stem, max_steps, rmsd_cutoff)

    All runs with this CCD object will be without application of torsion samplers.
    This is faster but might lead to weird backbone dihedral pairs.

    :param n_stem:      Residue defining the n_stem
    :param c_stem:      Residue defining the c_stem
    :param max_steps:   Maximal number of iterations
    :param rmsd_cutoff: The algorithm stops as soon as the c_stem of the loop to
                        be  closed has RMSD below the given *c_stem*

    :type n_stem:       :class:`ost.mol.ResidueHandle`
    :type c_stem:       :class:`ost.mol.ResidueHandle` 
    :type max_steps:    :class:`int`
    :type rmsd_cutoff:  :class:`float`

  .. method:: Close(bb_list)

    Closes given *bb_list* with the settings set at initialization.

    :param bb_list: Loop to be closed
    :type bb_list:  :class:`~promod3.loop.BackboneList`      

    :return: Whether *rmsd_cutoff* has been reached  
    :rtype:  :class:`bool` 

    :raises:  :exc:`~exceptions.RuntimeError` if the CCD object has been
              initialized with :class:`~promod3.loop.TorsionSampler` support
              and the length of the *bb_list* is not consistent with the initial
              *sequence*.


KIC
--------------------------------------------------------------------------------
The kinematic closure leads to a fragmentation of the loop based on given
pivot residues. It then calculates possible relative orientations
of these fragments by considering constraints of bond length and bond angles
at these pivot residues. Due to the internal mathematical formalism, up to
16 solutions can be found for a given loop closing problem.


.. class:: KIC

  Class, that sets up everything you need to perform a particular loop closing
  action.

  .. method:: KIC(n_stem, c_stem)

    All runs of this KIC closing object will adapt the input loops to these
    stem residues.

    :param n_stem:      Residue describing the stem towards n_ter
    :param c_stem:      Residue describing the stem towards c_ter

  .. method:: Close(bb_list, pivot_one, pivot_two, pivot_three)

    Applies KIC algorithm, so that the output loops match the given stem residues

    :param bb_list:     Loop to be closed
    :param pivot_one:     Index of first pivot residue
    :param pivot_two:     Index of second pivot residue
    :param pivot_three:   Index of third pivot residue  

    :type bb_list:        :class:`~promod3.loop.BackboneList`
    :type pivot_one:      :class:`int`  
    :type pivot_two:      :class:`int`  
    :type pivot_three:    :class:`int`  

    :return: List of closed loops (maximum of 16 entries)
    :rtype:  :class:`list` of :class:`~promod3.loop.BackboneList`

    :raises: :exc:`~exceptions.RuntimeError` in case of invalid pivot indices.


Relaxing Backbones
--------------------------------------------------------------------------------

In many cases one wants to quickly relax a loop. This can be useful to close
small gaps in the backbone or resolve the most severe clashes. The
:class:`BackboneRelaxer` internally sets up a topology for the input loop. 
Once setup, every loop of the same length and sequence can be relaxed by 
the relaxer.

.. class:: BackboneRelaxer(bb_list, ff, fix_nterm=True, fix_cterm=True)

  Sets up a molecular mechanics topology for given *bb_list*. Every
  :class:`~promod3.loop.BackboneList` of same length and sequence can then be
  relaxed. The parametrization is taken from *ff*, simply neglecting all 
  interactions to non backbone atoms. The electrostatics get neglected
  completely by setting all charges to 0.0.

  :param bb_list:       Basis for topology creation
  :param ff:            Source for parametrization
  :param fix_nterm:     Whether N-terminal backbone positions (N, CA, CB) should
                        be kept rigid during relaxation 
                        (C and O are left to move).
  :param fix_cterm:     Whether C-terminal backbone positions (CA, CB, C, O)
                        should be kept rigid during relaxation
                        (N is left to move).
  :type bb_list:        :class:`~promod3.loop.BackboneList`
  :type ff:             :class:`promod3.loop.ForcefieldLookup`
  :type fix_nterm:      :class:`bool`
  :type fix_cterm:      :class:`bool`


.. class:: BackboneRelaxer(bb_list, density, resolution, fix_nterm=True, \
                           fix_cterm=True)

  :param bb_list:       Basis for topology creation
  :param fix_nterm:     Whether N-terminal backbone positions (N, CA, CB) should
                        be kept rigid during relaxation.
  :param fix_cterm:     Whether C-terminal backbone positions (CA, CB, C, O)
                        should be kept rigid during relaxation.

  :type bb_list:        :class:`~promod3.loop.BackboneList`
  :type fix_nterm:      :class:`bool`
  :type fix_cterm:      :class:`bool`

  :raises: :exc:`~exceptions.RuntimeError` if size of *bb_list* is below 2


  .. method:: Run(bb_list, steps=100, stop_criterion=0.01)

    Performs steepest descent on given BackboneList. The function possibly fails
    if there are particles (almost) on top of each other, resulting in NaN 
    positions in the OpenMM system. The positions of **bb_list** are not touched
    in this case and the function returns an infinit potential energy. 
    In Python you can check for that with float("inf").

    :param bb_list:     To be relaxed
    :param steps:       number of steepest descent steps
    :param stop_criterion:  If maximum force acting on a particle
                            falls below that threshold, the
                            relaxation aborts.

    :type bb_list:      :class:`~promod3.loop.BackboneList`
    :type steps:        :class:`int`
    :type stop_criterion: :class:`float`

    :return: Forcefield energy upon relaxation, infinity in case of OpenMM Error
    :rtype:  :class:`float`

    :raises:  :exc:`~exceptions.RuntimeError` if *bb_list* has not the same
              size or sequence as the initial one.


  .. method:: AddNRestraint(idx, pos, force_constant=100000)

    Adds harmonic position restraint for nitrogen atom at specified residue

    :param idx:         Idx of residue
    :param pos:         Restraint Position (in Angstrom)
    :param force_constant: Force constant in kJ/mol/nm^2 

    :type idx:          :class:`int`
    :type pos:          :class:`ost.geom.Vec3`
    :type force_constant: :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *idx* is too large

  .. method:: AddCARestraint(idx, pos, force_constant=100000)

    Adds harmonic position restraint for CA atom at specified residue

    :param idx:         Idx of residue
    :param pos:         Restraint Position (in Angstrom)
    :param force_constant: Force constant in kJ/mol/nm^2 

    :type idx:          :class:`int`
    :type pos:          :class:`ost.geom.Vec3`
    :type force_constant: :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *idx* is too large

  .. method:: AddCBRestraint(idx, pos, force_constant=100000)

    Adds harmonic position restraint for CB atom at specified residue,
    doesn't do anything if specified residue is a glycine

    :param idx:         Idx of residue
    :param pos:         Restraint Position (in Angstrom)
    :param force_constant: Force constant in kJ/mol/nm^2 

    :type idx:          :class:`int`
    :type pos:          :class:`ost.geom.Vec3`
    :type force_constant: :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *idx* is too large

  .. method:: AddCRestraint(idx, pos, force_constant=100000)

    Adds harmonic position restraint for C atom at specified residue

    :param idx:         Idx of residue
    :param pos:         Restraint Position (in Angstrom)
    :param force_constant: Force constant in kJ/mol/nm^2 

    :type idx:          :class:`int`
    :type pos:          :class:`ost.geom.Vec3`
    :type force_constant: :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *idx* is too large

  .. method:: AddORestraint(idx, pos, force_constant=100000)

    Adds harmonic position restraint for O atom at specified residue

    :param idx:         Idx of residue
    :param pos:         Restraint Position (in Angstrom)
    :param force_constant: Force constant in kJ/mol/nm^2 

    :type idx:          :class:`int`
    :type pos:          :class:`ost.geom.Vec3`
    :type force_constant: :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *idx* is too large


  .. method:: SetNonBondedCutoff(nonbonded_cutoff)

    Defines cutoff to set for non bonded interactions. By default, all atom-
    pairs are compared which is the fastest for small backbones (e.g. in
    :meth:`CloseSmallDeletions`). Otherwise, a cutoff around 5 is reasonable as
    we ignore electrostatics.

    :param nonbonded_cutoff: Cutoff to set for non bonded interactions. Negative
                             means no cutoff.
    :type nonbonded_cutoff:  :class:`float`

  .. method:: GetNonBondedCutoff(nonbonded_cutoff)

    :return: Cutoff for non bonded interactions.
    :rtype:  :class:`float`


Relaxing All Atom Loops
--------------------------------------------------------------------------------

After the reconstruction of loop sidechains with the
:meth:`~promod3.modelling.SidechainReconstructor.Reconstruct` method of
:class:`~promod3.modelling.SidechainReconstructor`, it may be desired to
quickly relax the loop. The :class:`AllAtomRelaxer` class takes care of that.

Example usage:

.. literalinclude:: ../../../tests/doc/scripts/modelling_allatomrelaxer.py

.. class:: AllAtomRelaxer(sc_data, mm_system_creator)

  Setup relaxer for a given sidechain reconstruction result and a given MM
  system creator, which takes care of generating a simulation object.

  N/C stems of loop are fixed if they are non-terminal.

  :param sc_data: Sidechain reconstruction result
  :type sc_data:  :class:`~promod3.modelling.SidechainReconstructionData`
  :param mm_system_creator: System creator to be used here
  :type mm_system_creator:  :class:`~promod3.loop.MmSystemCreator`

  .. method:: Run(sc_data, steps=100, stop_criterion=0.01)

    Performs steepest descent for this system and writes updated positions into
    *sc_data.env_pos.all_pos*. The function possibly fails
    if there are particles (almost) on top of each other, resulting in NaN 
    positions in the OpenMM system. The positions of *sc_data.env_pos.all_pos* 
    are not touched in this case and the function returns an infinit potential 
    energy. In Python you can check for that with float("inf").

    :param sc_data: Sidechain reconstruction result to be updated
    :type sc_data:  :class:`~promod3.modelling.SidechainReconstructionData`
    :param steps: Number of steepest descent steps
    :type steps:  :class:`int`
    :param stop_criterion: If maximum force acting on a particle falls below
                           that threshold, the relaxation aborts.
    :type stop_criterion:  :class:`float`

    :return: Potential energy after relaxation, infinity in case of OpenMM Error
    :rtype:  :class:`float`

    :raises: :exc:`~exceptions.RuntimeError` if *sc_data* is incompatible with
             the one given in the constructor.

  .. method:: UpdatePositions(sc_data)

    Resets simulation positions to a new set of positions. It is assumed that
    this *sc_data* object has the same amino acids as loops and surrounding and
    the same disulfid bridges as the one given in the constructor.

    :param sc_data: Get new positions from *sc_data.env_pos.all_pos*
    :type sc_data:  :class:`~promod3.modelling.SidechainReconstructionData`

    :raises: :exc:`~exceptions.RuntimeError` if *sc_data* is incompatible with
             the one given in the constructor.

  .. method:: GetSystemCreator()

    :return: MM system creator passed in the constructor
    :rtype:  :class:`~promod3.loop.MmSystemCreator`



