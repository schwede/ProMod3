..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


:mod:`~promod3.modelling` - Protein Modelling
================================================================================

.. module:: promod3.modelling
  :synopsis: Protein Modelling

.. currentmodule:: promod3.modelling

High-level functionality for protein modelling. The goal is to model a given
target sequence (or list of sequences for oligomers) given some template data.
Commonly, the template does not cover the full target. This module offers
capabilities to extract useful template data for the target and to fill the
remaining structural data to create a full model of the target. In its simplest
form, you can use a target-template alignment and a template structure to create
a model fully automatically as follows:

.. literalinclude:: ../../../tests/doc/scripts/modelling_all.py

The various steps involved in protein modelling are described here:

.. toctree::
  :maxdepth: 2

  pipeline
  model_checking
  gap_handling
  loop_candidates
  loop_closing
  monte_carlo
  sidechain_reconstruction
  algorithms
  
