..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Modelling Pipeline
================================================================================

.. currentmodule:: promod3.modelling

A protein homology modelling pipeline has the following main steps:

- Build a raw model from the template (see :func:`BuildRawModel` function)
- Perform loop modelling to close (or remove) all gaps (see functions
  :func:`CloseSmallDeletions`, :func:`RemoveTerminalGaps`,
  :func:`MergeGapsByDistance`, :func:`FillLoopsByDatabase`,
  :func:`FillLoopsByMonteCarlo`, :func:`CloseLargeDeletions` or
  :func:`CloseGaps` that calls all these functions using predefined
  heuristics)
- Build sidechains (see :func:`BuildSidechains` function)
- Minimize energy of final model using molecular mechanics
  (see :func:`MinimizeModelEnergy` function)

The last steps to go from a raw model to a final model can easily be executed
with the :func:`BuildFromRawModel` function. If you want to run and tweak the
internal steps, you can start with the  following code and adapt it to your
purposes:

.. _modelling_steps_example:

.. literalinclude:: ../../../tests/doc/scripts/modelling_steps.py


Build Raw Modelling Handle
--------------------------------------------------------------------------------

.. class:: ModellingHandle

  Handles the result for structure model building and provides high-level methods
  to turn an initial raw model (see :func:`~promod3.modelling.BuildRawModel`)
  into a complete protein model by removing any existing gaps.

  .. attribute:: model

    The resulting model. This includes one chain per target chain (in the same
    order as the sequences in :attr:`seqres`) and (if they were included) a
    chain named '_' for ligands. You can therefore access `model.chains` items
    and :attr:`seqres` items with the same indexing and the optional ligand
    chain follows afterwards.

    :type: :class:`~ost.mol.EntityHandle`

  .. attribute:: gaps

    List of gaps in the model that could not be copied from the template. These
    gaps may be the result of insertions/deletions in the alignment or due to
    missing or incomplete backbone coordinates in the template structure.
    Gaps of different chains are appended one after another.

    :type: :class:`StructuralGapList`

  .. attribute:: seqres

    List of sequences with one :class:`~ost.seq.SequenceHandle` for each chain 
    of the target protein.

    :type: :class:`~ost.seq.SequenceList`

  .. attribute:: profiles

    List of profiles with one :class:`ost.seq.ProfileHandle` for each chain of
    the target protein (same order as in :attr:`seqres`). Please note, that this
    attribute won't be set by simply calling :func:`BuildFromRawModel`. You have
    to fill it manually or even better by the convenient function
    :func:`SetSequenceProfiles`,  to ensure consistency with the seqres.

    :type: :class:`list` of :class:`ost.seq.ProfileHandle`

  .. attribute:: psipred_predictions

    List of predictions with one :class:`promod3.loop.PsipredPrediction` for
    each chain of the target protein (same order as in :attr:`seqres`). Please
    note, that this attribute won't be set by simply calling
    :func:`BuildFromRawModel`. You have to fill it manually or even better by
    the convenient function :func:`SetPsipredPredictions`,  to ensure
    consistency with the seqres.

    :type: :class:`list` of :class:`~promod3.loop.PsipredPrediction`

  .. attribute:: backbone_scorer_env

    Backbone score environment attached to this handle. A default environment is
    set with :func:`SetupDefaultBackboneScoring` when needed. Additional
    information can be added to the environment before running the pipeline
    steps.

    :type: :class:`~promod3.scoring.BackboneScoreEnv`

  .. attribute:: backbone_scorer

    Backbone scorer container attached to this handle. A default set of scorers
    is initialized with :func:`SetupDefaultBackboneScoring` when needed.

    :type: :class:`~promod3.scoring.BackboneOverallScorer`

  .. attribute:: all_atom_scorer_env

    All atom environment attached to this handle for scoring. A default
    environment is set with :func:`SetupDefaultAllAtomScoring` when needed. This
    environment is for temporary work only and is only updated to score loops.
    It is not to be updated when loops are chosen and added to the final model.

    :type: :class:`~promod3.loop.AllAtomEnv`

  .. attribute:: all_atom_scorer

    All atom scorer container attached to this handle. A default set of scorers
    is initialized with :func:`SetupDefaultAllAtomScoring` when needed.

    :type: :class:`~promod3.scoring.AllAtomOverallScorer`

  .. attribute:: all_atom_sidechain_env

    All atom environment attached to this handle for sidechain reconstruction. A
    default environment is set with :func:`SetupDefaultAllAtomScoring` when
    needed.

    :type: :class:`~promod3.loop.AllAtomEnv`

  .. attribute:: sidechain_reconstructor

    A sidechain reconstructor to add sidechains to loops prior to all atom
    scoring. A default one is set with :func:`SetupDefaultAllAtomScoring` when
    needed.

    :type: :class:`~promod3.modelling.SidechainReconstructor`

  .. attribute:: fragger_handles

    Optional attribute which is set in :meth:`SetFraggerHandles`. Use
    :meth:`hasattr` to check if it's available. If it's set, it is used in
    :meth:`BuildFromRawModel`.

    :type:  :class:`list` of :class:`FraggerHandle`

  .. attribute:: modelling_issues

    Optional attribute which is set in :meth:`AddModellingIssue`. Use
    :meth:`hasattr` to check if it's available. If it's set, it can be used to
    check issues which occurred in :meth:`BuildFromRawModel` (see
    :meth:`MinimizeModelEnergy` and :meth:`CheckFinalModel` for details).

    :type:  :class:`list` of :class:`ModellingIssue`

  .. method:: Copy()

    Generates a deep copy. Everything will be copied over to the returned
    :class:`ModellingHandle`, except the potentially set scoring members
    :attr:`~ModellingHandle.backbone_scorer`,
    :attr:`~ModellingHandle.backbone_scorer_env`,
    :attr:`~ModellingHandle.all_atom_scorer_env`,
    :attr:`~ModellingHandle.all_atom_scorer`,
    :attr:`~ModellingHandle.all_atom_sidechain_env` and
    :attr:`~ModellingHandle.sidechain_reconstructor`.

    :return: A deep copy of the current handle
    :rtype:  :class:`ModellingHandle`
    

.. autofunction:: BuildRawModel

The Default Pipeline
--------------------------------------------------------------------------------

.. autofunction:: BuildFromRawModel


Modelling Steps
--------------------------------------------------------------------------------


.. function:: SetupDefaultBackboneScoring(mhandle)

    Setup scorers and environment for meddling with backbones.
    This one is already tailored towards a certain modelling job.
    The scorers added (with their respective keys) are:

    - "cb_packing": :class:`~promod3.scoring.CBPackingScorer`
    - "cbeta": :class:`~promod3.scoring.CBetaScorer`
    - "reduced": :class:`~promod3.scoring.ReducedScorer`
    - "clash": :class:`~promod3.scoring.ClashScorer`
    - "hbond": :class:`~promod3.scoring.HBondScorer`
    - "torsion": :class:`~promod3.scoring.TorsionScorer`
    - "pairwise": :class:`~promod3.scoring.PairwiseScorer`

    :param mhandle: The modelling handle. This will set the properties
                    :attr:`~ModellingHandle.backbone_scorer` and
                    :attr:`~ModellingHandle.backbone_scorer_env` of `mhandle`.
    :type mhandle:  :class:`~promod3.modelling.ModellingHandle`

.. function:: IsBackboneScoringSetUp(mhandle)

  :return: True, if :attr:`~ModellingHandle.backbone_scorer` and 
           :attr:`~ModellingHandle.backbone_scorer_env` of `mhandle` are set.
  :rtype:  :class:`bool`
  :param mhandle: Modelling handle to check.
  :type mhandle:  :class:`ModellingHandle`

.. function:: SetupDefaultAllAtomScoring(mhandle)

    Setup scorers and environment to perform all atom scoring.
    This one is already tailored towards a certain modelling job, where we
    reconstruct sidechains for loop candidates and score them.
    The scorers added (with their respective keys) are:

    - "aa_interaction": :class:`~promod3.scoring.AllAtomInteractionScorer`
    - "aa_packing": :class:`~promod3.scoring.AllAtomPackingScorer`
    - "aa_clash": :class:`~promod3.scoring.AllAtomClashScorer`

    :param mhandle: The modelling handle. This will set the properties
                    :attr:`~ModellingHandle.all_atom_scorer_env`,
                    :attr:`~ModellingHandle.all_atom_scorer`,
                    :attr:`~ModellingHandle.all_atom_sidechain_env` and
                    :attr:`~ModellingHandle.sidechain_reconstructor`.
    :type mhandle:  :class:`~promod3.modelling.ModellingHandle`

.. function:: IsAllAtomScoringSetUp(mhandle)

  :return: True, if :attr:`~ModellingHandle.all_atom_scorer_env`,
           :attr:`~ModellingHandle.all_atom_scorer`,
           :attr:`~ModellingHandle.all_atom_sidechain_env` and
           :attr:`~ModellingHandle.sidechain_reconstructor` of `mhandle` are set.
  :rtype:  :class:`bool`
  :param mhandle: Modelling handle to check.
  :type mhandle:  :class:`ModellingHandle`

.. function:: InsertLoop(mhandle, bb_list, start_resnum, chain_idx)

  Insert loop into model and ensure consistent updating of scoring environments.
  Note that we do not update :attr:`~ModellingHandle.all_atom_scorer_env` as
  that one is meant to be updated only while scoring. To clear a gap while
  inserting a loop, use the simpler :meth:`InsertLoopClearGaps`.

  :param mhandle: Modelling handle on which to apply change.
  :type mhandle:  :class:`ModellingHandle`
  :param bb_list: Loop to insert (backbone only).
  :type bb_list:  :class:`~promod3.loop.BackboneList`
  :param start_resnum: Res. number defining the start position in the SEQRES.
  :type start_resnum:  :class:`int`
  :param chain_idx: Index of chain the loop belongs to.
  :type chain_idx:  :class:`int`

.. function:: RemoveTerminalGaps(mhandle)

  Removes terminal gaps without modelling them (just removes them from the list
  of gaps). This is useful for pipelines which lack the possibility to properly
  model loops at the termini.

  :param mhandle: Modelling handle on which to apply change.
  :type mhandle:  :class:`ModellingHandle`

  :return: Number of gaps which were removed.
  :rtype:  :class:`int`

.. function:: ReorderGaps(mhandle)

  Reorders all gaps to ensure sequential order by performing lexicographical 
  comparison on the sequence formed by chain index of the gap and 
  start residue number. 

.. function:: MergeMHandle(source_mhandle, target_mhandle, source_chain_idx, \
                           target_chain_idx, start_resnum, end_resnum, transform)

  Merges the specified stretch of **source_mhandle** into **target_mhandle** by
  replacing all structural information and gaps in the stretch 
  **start_resnum** and **end_resnum** (inclusive). The residues specified by 
  **start_resnum** and **end_resnum** must be valid in the source_mhandle, 
  i.e. not be enclosed by a gap. If a gap encloses **start_resnum** or 
  **end_resnum** in the **target_mhandle**, the gap gets replaced by a 
  shortened version not including the part overlapping with the defined stretch.
  If there is any scoring set up (backbone or all atom), the according
  environments get updated in **target_mhandle**.

  :param source_mhandle:  Source of structural information and gaps
  :param target_mhandle:  Structural information and gaps will be copied in here
  :param source_chain_idx:  This is the chain where the info comes from
  :param target_chain_idx:  This is the chain where the info goes to
  :param start_resnum:  First residue of the copied stretch
  :param end_resnum:    Last residue of the copied stretch
  :param transform:     Transformation to be applied to all atom positions when
                        they're copied over

  :type source_mhandle: :class:`ModellingHandle`
  :type target_mhandle: :class:`ModellingHandle`
  :type source_chain_idx: :class:`int`
  :type target_chain_idx: :class:`int`
  :type start_resnum:   :class:`int`
  :type end_resnum:     :class:`int`
  :type transform:      :class:`ost.geom.Mat4`

  :raises: A :exc:`RuntimeError` when: 
           
           - the chain indices are invalid
           - the SEQRES of the specified chains do not match
           - the start and end residue numbers are invalid or when the residues
             at the specified positions in the **source_mhandle** do not exist
           - a gap in the **source_mhandle** encloses the residues specified by
             **start_resnum** and **end_resnum**

.. function:: SetSequenceProfiles(mhandle, profiles)

  Sets the :attr:`sequence profiles <ModellingHandle.profiles>` of **mhandle**
  while ensuring consistency with the :attr:`~ModellingHandle.seqres`.

  :param mhandle:  Will have the profiles attached afterwards
  :param profiles: The sequence profiles to attach

  :type mhandle:   :class:`ModellingHandle`
  :type profiles:  :class:`list` of :class:`ost.seq.ProfileHandle`

  :raises: :class:`ValueError` when the given **profiles** are not consistent
           with seqres in **mhandle**


.. function:: SetPsipredPredictions(mhandle, predictions)

  Sets the :attr:`predictions <ModellingHandle.psipred_predictions>` of
  **mhandle** while ensuring consistency with the
  :attr:`~ModellingHandle.seqres`.

  :param mhandle:     Will have the predictions attached afterwards
  :param predictions: The predictions to attach

  :type mhandle:      :class:`ModellingHandle`
  :type predictions:  :class:`list` of :class:`~promod3.loop.PsipredPrediction`

  :raises: :class:`ValueError` when the given **predictions** are not consistent
           with seqres in **mhandle**


.. autofunction:: SetFraggerHandles

.. autofunction:: CloseGaps

.. autofunction:: CloseSmallDeletions

.. autofunction:: MergeGapsByDistance

.. autofunction:: FillLoopsByDatabase

.. autofunction:: FillLoopsByMonteCarlo

.. autofunction:: CloseLargeDeletions

.. autofunction:: ModelTermini

.. autofunction:: BuildSidechains

.. autofunction:: MinimizeModelEnergy

.. autofunction:: CheckFinalModel

.. autoclass:: ModellingIssue
  :members:

.. autofunction:: AddModellingIssue


Alignment Fiddling
--------------------------------------------------------------------------------

.. autofunction:: DeleteGapCols

.. autofunction:: PullTerminalDeletions
