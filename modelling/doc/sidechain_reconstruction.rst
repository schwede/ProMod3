..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Sidechain Reconstruction
================================================================================

.. currentmodule:: promod3.modelling

Two methods are provided to fully reconstruct sidechains of residues:

- the :func:`ReconstructSidechains` function handles a full OST
  :class:`~ost.mol.EntityHandle`
- the :class:`SidechainReconstructor` is linked to an all atom environment
  and used to reconstruct sidechains of single loops

Example usage:

.. literalinclude:: ../../../tests/doc/scripts/modelling_reconstruct_sidechains.py

.. literalinclude:: ../../../tests/doc/scripts/modelling_sidechain_reconstructor.py



Reconstruct Function
--------------------------------------------------------------------------------

.. autofunction:: ReconstructSidechains


SidechainReconstructor Class
--------------------------------------------------------------------------------

.. class:: SidechainReconstructor(keep_sidechains=True, build_disulfids=True, \
                                  optimize_subrotamers=False, \
                                  remodel_cutoff=20, \
                                  rigid_frame_cutoff=0, \
                                  graph_max_complexity=100000000, \
                                  graph_intial_epsilon=0.02, \
                                  disulfid_score_thresh=45)

  Reconstruct sidechains for single loops or residues. Must be linked to an 
  all atom env. (:meth:`AttachEnvironment`) containing the structural data. 
  Residues are identified as N- or C-terminal according to the seqres in the 
  environment. This means that residues preceeded / followed by gaps are not 
  treated as terminal! In the reconstruction procedure you can specify 
  residues that should be remodelled. Everything within *remodel_cutoff* will
  also be considered and potentially remodelled. To enforce the visibility
  of the rigid frame to all of those close residues you can specify the 
  *rigid_frame_cutoff*. In the example of *remodel_cutoff*\=20 and 
  *rigid_frame_cutoff*\=10, all residues within 20A of any of the input residues
  will be considered for remodelling. Everything further away than 20A but 
  within 20A + 10A = 30A will also be considered as rigid frame (all 
  backbone atoms and the sidechain if present). The distance criteria is the
  CB atom distance between residues (CA in case of glycine).

  :param keep_sidechains: Flag, whether complete sidechains in env. (i.e. 
                          containing all required atoms) should be kept rigid
                          and directly be added to the result.
  :type keep_sidechains: :class:`bool`

  :param build_disulfids: Flag, whether possible disulfid bonds should be 
                          searched. If a disulfid bond is found, the two
                          participating cysteins are fixed and added to
                          the result.
  :type build_disulfids: :class:`bool`

  :param optimize_subrotamers: Flag, whether the
                               :func:`~promod3.sidechain.SubrotamerOptimizer`
                               with default parametrization should be called 
                               if we're dealing with FRM rotamers.
  :type optimize_subrotamers:  :class:`bool`

  :param remodel_cutoff: Cutoff to identify all residues that need to be 
                         remodelled.
  :type remodel_cutoff:  :class:`float`

  :param rigid_frame_cutoff: Cutoff to control the visibility of the rigid
                             frame to the reconstruction procedure.
                             Everything within 
                             \[*remodel_cutoff*, 
                             *remodel_cutoff* + *rigid_frame_cutoff*\]
                             will be considered as ridig frame. Small sidenote:
                             if the *keep_sidechains* flag is true and all 
                             residues within *remodel_cutoff* already have a 
                             sidechain, the *rigid_frame_cutoff* won't have any
                             effect. 
  :type rigid_frame_cutoff: :class:`float`

  :param graph_max_complexity: Max. complexity for
                               :meth:`promod3.sidechain.RotamerGraph.TreeSolve`.
  :type graph_max_complexity:  :class:`int`
  :param graph_intial_epsilon: Initial epsilon for
                               :meth:`promod3.sidechain.RotamerGraph.TreeSolve`.
  :type graph_intial_epsilon:  :class:`float`

  :param disulfid_score_thresh: If :meth:`DisulfidScore` between two CYS is
                                below this threshold, we consider them to be
                                disulfid-bonded.
  :type disulfid_score_thresh:  :class:`float`


  .. method:: Reconstruct(start_resnum, num_residues, chain_idx=0)
              Reconstruct(start_resnum_list, num_residues_list, chain_idx_list)

    Reconstruct sidechains for one or several loops extracted from environment.
    Overlapping loops are merged and 0-length loops are removed. All residues in
    the loop(s) are expected to contain valid CB positions (or CA for GLY),
    which are used to look for other potentially relevant residues in the
    surrounding. The resulting structural data will contain all residues in the
    loop(s) and in the surrounding with all backbone and sidechain heavy atom
    positions set.

    Note that the structural data of the loop(s) is expected to be in the linked
    environment before calling this!

    :param start_resnum: Start of loop.
    :type start_resnum:  :class:`int` / :class:`ost.mol.ResNum`
    :param num_residues: Length of loop.
    :type num_residues:  :class:`int`
    :param chain_idx: Chain the loop belongs to.
    :type chain_idx:  :class:`int`
    
    :param start_resnum_list: Starts of loops.
    :type start_resnum_list:  :class:`list` of :class:`int`
    :param num_residues_list: Lengths of loops.
    :type num_residues_list:  :class:`list` of :class:`int`
    :param chain_idx_list: Chains the loops belong to.
    :type chain_idx_list:  :class:`list` of :class:`int`

    :return: A helper object with all the reconstruction results.
    :rtype:  :class:`SidechainReconstructionData`

    :raises: :exc:`~exceptions.RuntimeError` if reconstructor was never attached
             to an environment or if parameters lead to invalid / unset
             positions in environment.

  .. method:: AttachEnvironment(env, use_frm=True, use_bbdep_lib=True)
              AttachEnvironment(env, use_frm, rotamer_library)
    
    Link reconstructor to given *env*. A helper class is used in the background
    to provide sidechain-objects for the environment. As this class is reused by
    every reconstructor linked to *env*, the used parameters must be consistent
    if multiple reconstructors are used (or you must use a distinct *env*).

    :param env: Link to this environment.
    :type env:  :class:`~promod3.loop.AllAtomEnv`
    :param use_frm: If True, use flexible rotamer model, else rigid.
    :type use_frm:  :class:`bool`
    :param use_bbdep_lib: If True, use default backbone dependent rot. library
                          (:meth:`LoadBBDepLib`), else use
                          backbone independent one
                          (:meth:`LoadLib`).
    :type use_bbdep_lib:  :class:`bool`
    :param rotamer_library: Custom rotamer library to be used.
    :type rotamer_library:  :class:`BBDepRotamerLib` / :class:`RotamerLib`

    :raises: :exc:`~exceptions.RuntimeError` if *env* was already linked to
            another reconstructor with inconsistent parameters. Acceptable
            changes:

            - *keep_sidechains* = True, if previously False
            - *build_disulfids* = False, if previously True

The SidechainReconstructionData class
--------------------------------------------------------------------------------

.. class:: SidechainReconstructionData

  Contains the results of a sidechain reconstruction
  (:meth:`SidechainReconstructor.Reconstruct`). All attributes are read only!

  .. attribute:: env_pos

    Container for structural data and mapping to the internal residue indices
    of the used :class:`~promod3.loop.AllAtomEnv`. Useful for scoring and env.
    updates.

    :type: :class:`~promod3.loop.AllAtomEnvPositions`

  .. attribute:: loop_start_indices
                 loop_lengths

    The first *sum(loop_lengths)* residues in
    :attr:`~promod3.loop.AllAtomEnvPositions.res_indices` of *env_pos* are
    guaranteed to belong to the actual input, all the rest comes from the close
    environment.

    Each input loop (apart from overlapping and 0-length loops) is defined by an
    entry in *loop_start_indices* and *loop_lengths*. For loop *i_loop*,
    *res_indices[loop_start_indices[i_loop]]* is the N-stem and
    *res_indices[loop_start_indices[i_loop] + loop_lengths[i_loop] - 1]* is the
    C-stem of the loop. The loop indices are contiguous in *res_indices* between
    the stems.

    :type: :class:`list` of :class:`int`

  .. attribute:: rotamer_res_indices

    Indices of residues within *env_pos* for which we generated a new sidechain
    (in [*0, len(env_pos.res_indices)-1*]).

    :type: :class:`list` of :class:`int`

  .. attribute:: disulfid_bridges

    Pairs of residue indices within *env_pos* for which we generated a disulfid
    bridge (indices in [*0, len(env_pos.res_indices)-1*]).

    :type: :class:`list` of :class:`tuple` with two :class:`int`

  .. attribute:: is_n_ter

    True/False depending on whether a given residue in *env_pos* is N-terminal
    in the environment (same length as *env_pos.res_indices*)

    :type: :class:`list` of :class:`bool`

  .. attribute:: is_c_ter

    True/False depending on whether a given residue in *env_pos* is C-terminal
    in the environment (same length as *env_pos.res_indices*)

    :type: :class:`list` of :class:`bool`
