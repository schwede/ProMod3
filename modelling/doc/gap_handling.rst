..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Handling Gaps
================================================================================

.. currentmodule:: promod3.modelling

This chapter describes the gap classes and functionality attached to them. These
classes / functions are used within the modelling pipeline.

Gap classes
--------------------------------------------------------------------------------

.. class:: StructuralGap(before, after, seq)

  Describes a structural gap, i.e. a loop to be modeled. The gap may either be
  terminal or between two defined regions. The gap stores information of the
  last residue before and the first residue after the gap as well as the
  sequence of gap. Gaps at the N- and C-terminals can be defined by passing
  invalid residue handles to `before` or `after`.

  :param before: Fills :attr:`before`
  :type before:  :class:`ost.mol.ResidueHandle`
  :param after: Fills :attr:`after`
  :type after:  :class:`ost.mol.ResidueHandle`
  :param seq: Fills :attr:`seq`
  :type seq:  :class:`str`

  :raises: A :exc:`RuntimeError` if both residues are invalid or when both
           are valid and:
    
           - residues are from different chains (if both valid)
           - `before` is located after `after`
           - `seq` has a length which is inconsistent with the gap

  .. method:: GetChainIndex()

    :return: Index of chain, the gap is belonging to
    :rtype:  :class:`int`

  .. method:: GetChainName()

    :return: Name of chain, the gap is belonging to
    :rtype:  :class:`str`

  .. method:: GetChain()

    :return: Chain, the gap is belonging to
    :rtype:  :class:`ost.mol.ChainHandle`

  .. method:: IsNTerminal()

    :return: True, iff gap is N-terminal (i.e. :attr:`before` is invalid 
             and :attr:`after` is valid)
    :rtype:  :class:`bool`

  .. method:: IsCTerminal()

    :return: True, iff gap is C-terminal (i.e. :attr:`before` is valid 
             and :attr:`after` is invalid)
    :rtype:  :class:`bool`

  .. method:: IsTerminal()

    :return: True, iff gap is N- or C-terminal
    :rtype:  :class:`bool`

  .. method:: ShiftCTerminal()

    Try to shift gap by one position towards C-terminal. Only possible if new
    gap is not terminal and it doesn't try to shift the gap past another gap.

    :return: True, iff shift succeeded (gap is only updated in that case)
    :rtype:  :class:`bool`

  .. method:: ExtendAtNTerm()

    Try to extend gap at N-terminal end of gap.
    Only possible if the gap is not at N-terminal and it doesn't try to
    extend the gap past another gap.

    :return: True, iff extend succeeded (gap is only updated in that case)
    :rtype:  :class:`bool`

  .. method:: ExtendAtCTerm()

    Try to extend gap at C-terminal end of gap.
    Only possible if the gap is not at C-terminal and it doesn't try to
    extend the gap past another gap.

    :return: True, iff extend succeeded (gap is only updated in that case)
    :rtype:  :class:`bool`

  .. method:: GetLength()

    :return: Length of the gap.
    :rtype:  :class:`int`

  .. method:: Copy()

    :return: Copy of the gap.
    :rtype:  :class:`StructuralGap`

  .. attribute:: length

    Alias for :meth:`GetLength()` (read-only, :class:`int`)

  .. attribute:: seq

    Sequence string for the gap (read-only, :class:`str`)

  .. attribute:: before

    Residue before the gap (read-only, :class:`ost.mol.ResidueHandle`)

  .. attribute:: after

    Residue after the gap (read-only, :class:`ost.mol.ResidueHandle`)

  .. attribute:: full_seq

    Full sequence, including stem residues (read-only)

.. class:: StructuralGapList

  Represents a :class:`list` of :class:`StructuralGap`. 


Gap Extender classes
--------------------------------------------------------------------------------

The extender classes work on a given :class:`StructuralGap` and provide an
Extend() function to propose new gaps for loop modelling. The function returns
False if no new extension possible.

.. class:: GapExtender(gap, seqres)

  The extender cycles through the following steps:

  .. code-block:: none

       -
      --
       --
     ---
      ---
       ---
    ----
     ----
      ----
       ----

  :param gap: The gap which will be extended by :meth:`Extend`.
  :param seqres: The full sequence of the chain, the gap is associated with.
  :type gap:  :class:`StructuralGap`
  :type seqres: :class:`str` / :class:`ost.seq.SequenceHandle`

  :raises: An exception if a terminal gap is used to construct this.

  .. method:: Extend()

    Tries to extend *gap*.

    :return: False, if the *gap* cannot be extended any further. This happens
             if it reaches a terminal or another insertion gap.
             Otherwise, the *gap* passed to the constructor is changed.
             The gaps are extended with ascending length and will always have
             valid termini.
    :rtype:  :class:`bool`

.. class:: FullGapExtender(gap, seqres, max_length=-1)

  Cycles as GapExtender, but continues even if another gap was encountered.

  :param gap: The gap which will be extended by :meth:`Extend`.
  :param seqres: The full sequence of the chain, the gap is associated with.
  :param max_length: - If -1, all possible non-terminal gaps are returned.
                     - If >= 0, this restricts the max. gap-length
                       (w/o termini) producable by :meth:`Extend`.
  :type gap:  :class:`StructuralGap`
  :type seqres: :class:`str` / :class:`ost.seq.SequenceHandle`
  :type max_length: :class:`int`

  :raises: An exception if a terminal gap is used to construct this.

  .. method:: Extend()

    Tries to extend *gap*.

    :return: False, if the *gap* cannot be extended without exceeding *max_length*.
             Otherwise, the *gap* passed to the constructor is changed.
             The gaps are extended with ascending length and will always have
             valid termini.
    :rtype:  :class:`bool`

.. class:: ScoringGapExtender(gap, extension_penalty, penalties, seqres,\
                              max_length=-2)

  The extender scores possible gap extensions and returns them in order of
  their score when :meth:`Extend` is called.
  The score is penalized according to length and according to certain (well
  conserved) regions in the structure as defined by *penalties*.
  score = num_gap_extensions * `extension_penalty` + sum( `penalties` [i] )
  (i = resnum - 1 of residues in extension)

  :param gap: The gap which will be extended by :meth:`Extend`.
  :type gap:  :class:`StructuralGap`
  :param extension_penalty: Penalty for length of gap.
  :type extension_penalty:  :class:`float`
  :param penalties: Penalty for each residue added to gap.
  :type penalties:  :class:`list` of :class:`float`
  :param seqres: The full sequence of the chain, the gap is associated with.
  :type seqres: :class:`str` / :class:`ost.seq.SequenceHandle`
  :param max_length: 
    - If -2, :class:`GapExtender` is used instead of :class:`FullGapExtender`
      (i.e. it stops at gaps and termini).
    - If -1, all possible non-terminal gaps are returned.
    - If >= 0, this restricts the max. gap-length (w/o termini)
      producable by :meth:`Extend`.
  :type max_length: :class:`int`

  :raises: An exception if a terminal gap is used to construct this.

  .. method:: Extend()

    Tries to extend *gap*.

    :return: False, if the gap cannot be extended any further.
             Otherwise, *gap* is changed and returned in ascending score.
             The updated *gap* will always have valid termini.
    :rtype:  :class:`bool`

.. class:: ShiftExtension(n_num, c_num)

  Implements the underlying extension scheme of the :class:`GapExtender`.
  It is not associated to any structural data, it just spits out the
  residue numbers according to the extension scheme described above.

  :param n_num:         N residue number to start with
  :param c_num:         C residue number to start with
  :type n_num:          :class:`int`
  :type c_num:          :class:`int`

  .. method:: Extend()

    :returns:           The next residue numbers for n_stem and c_stem
    :rtype:             :class:`tuple`

    
Gap Handling Functions
--------------------------------------------------------------------------------

.. function:: CountEnclosedGaps(mhandle, gap)
              CountEnclosedInsertions(mhandle, gap)

  Counts all gaps from `mhandle` which are fully enclosed by given `gap`.
  This is either all gaps or only insertions.

  :param mhandle: Modelling handle on which to apply change.
  :type mhandle:  :class:`ModellingHandle`
  :param gap:     Gap defining range in which gaps are to be removed.
  :type gap:      :class:`StructuralGap`

  :return: Number of gaps.
  :rtype:  :class:`int`

.. function:: ClearGaps(mhandle, gap)

  Removes all gaps from `mhandle` which are fully enclosed by given `gap`.

  :param mhandle: Modelling handle on which to apply change.
  :type mhandle:  :class:`ModellingHandle`
  :param gap:     Gap defining range in which gaps are to be removed.
  :type gap:      :class:`StructuralGap`

  :return: Index of next gap in mhandle.gaps after removal.
           Returns -1 if last gap was removed or no gaps in *mhandle*.
  :rtype:  :class:`int`

  :raises: A :exc:`RuntimeError` if any gap in mhandle.gaps is only partially
           enclosed by given gap.

.. function:: InsertLoopClearGaps(mhandle, bb_list, gap)

  Insert loop into model, update scoring environments and remove all gaps from
  *mhandle* which are fully enclosed by given *gap* (see :meth:`InsertLoop` and
  :meth:`ClearGaps`).

  :param mhandle: Modelling handle on which to apply change.
  :type mhandle:  :class:`ModellingHandle`
  :param bb_list: Loop to insert (backbone only).
  :type bb_list:  :class:`~promod3.loop.BackboneList`
  :param gap:     Gap defining range of loop to insert (must be consistent!).
  :type gap:      :class:`StructuralGap`

  :return: Index of next gap in mhandle.gaps after removal.
           Returns -1 if last gap was removed or no gaps in *mhandle*.
  :rtype:  :class:`int`

  :raises: A :exc:`RuntimeError` if *bb_list* and *gap* are inconsistent or
           if any gap in mhandle.gaps is only partially enclosed by *gap*.

.. function:: MergeGaps(mhandle, index)

  Merges two gaps `mhandle.gaps[index]` and `mhandle.gaps[index+1]`.
  The residues in between the gaps are removed from `mhandle.model` and added
  to the new `mhandle.gaps[index]`.

  :param mhandle: Modelling handle on which to apply change.
  :type mhandle:  :class:`ModellingHandle`
  :param index:   Index of gap to merge with next one.
  :type index:    :class:`int`

  :raises: A :exc:`RuntimeError` if indices out of range or if trying to merge
           gaps of different chains or an N-terminal gap with a C-terminal gap.