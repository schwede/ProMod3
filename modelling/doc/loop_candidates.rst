..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Handling Loop Candidates
================================================================================

.. currentmodule:: promod3.modelling

For convenience, we provide the :class:`LoopCandidates` class as a container of
loop candidates with consistent length and sequence among them. It can either be
filled manually or generated using static filling functions using the
functionality from the :class:`~promod3.loop.FragDB` or Monte Carlo algorithms.
Once it contains candidates you can apply closing, scoring or clustering
algorithms on all loop candidates. Example:

.. literalinclude:: ../../../tests/doc/scripts/modelling_loop_candidates.py


The LoopCandidates class
--------------------------------------------------------------------------------

.. class:: LoopCandidates(seq)

  Initializes an empty container of loop candidates.

  Candidates can be accessed and iterated as if it was a :class:`list` of
  :class:`~promod3.loop.BackboneList`.

  :param seq: The sequence being enforced for all candidates
  :type seq:  :class:`str`

  .. staticmethod:: FillFromDatabase(n_stem, c_stem, seq, frag_db, \
                                     structural_db, extended_search=False)
    
    Searches for loop candidates matching the length (number of residues in
    *seq*) and geometry (of *n_stem* and *c_stem*) of the loop to be modelled in
    a fragment database.

    This call also adds fragment infos (:meth:`HasFragmentInfos` will be True).

    :param n_stem:         The residue at the N-terminal end of the loop 
    :param c_stem:         The residue at the C-terminal end of the loop 
    :param seq:            The sequence of residues to be added including the
                           *n_stem* and *c_stem*
    :param frag_db:        The fragment database
    :param structural_db:  The according structural database
    :param extended_search: Whether search should be extended to fragments
                            matching the geometry of the *n_stem* and *c_stem*
                            a bit less precisely.

    :type n_stem:          :class:`ost.mol.ResidueHandle`
    :type c_stem:          :class:`ost.mol.ResidueHandle`
    :type seq:             :class:`str`
    :type frag_db:         :class:`~promod3.loop.FragDB`
    :type structural_db:   :class:`~promod3.loop.StructureDB`
    :type extended_search: :class:`bool`

    :returns:              A list of loop candidates
    :rtype:                :class:`LoopCandidates`

    :raises: :exc:`~exceptions.RuntimeError` if stems do no contain N, CA and C
             atoms.

  .. staticmethod:: FillFromMonteCarloSampler(seq, num_loops, steps, \
                                              sampler, closer, scorer, cooler, \
                                              random_seed=0)
                    FillFromMonteCarloSampler(initial_bb, seq, num_loops, steps, \
                                              sampler, closer, scorer, cooler, \
                                              random_seed=0)
    
    Uses Monte Carlo simulated annealing to sample the loop to be modelled.
    If *initial_bb* is given, every Monte Carlo run starts from that configuration.

    .. warning:: The :class:`~promod3.scoring.BackboneScoreEnv`
                 attached to *scorer* will be altered in the specified stretch.
                 You might consider the Stash / Pop mechanism of the 
                 :class:`~promod3.scoring.BackboneScoreEnv` to restore to the 
                 original state once the sampling is done.    
 
    :param initial_bb: Initial configuration used for the sampling
    :param seq:        The sequence of residues to be sampled
    :param num_loops:  Number of loop candidates to return
    :param steps:      Number of MC steps to perform for each loop candidate
                       generated
    :param sampler:    Used to generate a new configuration at each MC step
    :param closer:     Used to close the loop after each MC step
    :param scorer:     Used to score the generated configurations at each MC step
    :param cooler:     Controls the temperature profile of the simulated annealing
    :param random_seed: Seed to feed the random number generator for 
                        accepting/rejecting proposed monte carlo steps.
                        For every monte carlo run, the random number generator 
                        gets refreshed and this seed gets increased by 1.

    :type initial_bb:  :class:`~promod3.loop.BackboneList`
    :type seq:         :class:`str`
    :type num_loops:   :class:`int`
    :type steps:       :class:`int`
    :type sampler:     :ref:`mc-sampler-object`
    :type closer:      :ref:`mc-closer-object`
    :type scorer:      :ref:`mc-scorer-object`
    :type cooler:      :ref:`mc-cooler-object`
    :type random_seed: :class:`int`

    :returns:          The resulting candidates
    :rtype:            :class:`LoopCandidates`

    :raises: :exc:`~exceptions.RuntimeError`, if *initial_bb* is not given and
             the Monte Carlo sampler failed to initialize (i.e. stems are too
             far apart) or if *initial_bb* is given and it is inconsistent with
             *seq*.


  .. method:: GetSequence()

    :returns:           The Sequence
    :rtype:             :class:`str`

  .. method:: ApplyCCD(n_stem, c_stem, max_iterations=1000, \
                       rmsd_cutoff=0.1, keep_non_converged=false, random_seed=0)
              ApplyCCD(n_stem, c_stem, torsion_sampler, max_iterations=1000, \
                       rmsd_cutoff=0.1, keep_non_converged=false, random_seed=0)
              ApplyCCD(n_stem, c_stem, torsion_samplers, max_iterations=1000, \
                       rmsd_cutoff=0.1, keep_non_converged=false, random_seed=0)

    Closes all loop candidates in this container using the CCD algorithm (i.e.
    modifies the candidates so that its stem residues match those of *n_stem*
    and *c_stem*). CCD (cyclic coordinate descent, see :class:`CCD`) is an
    iterative minimization algorithm.

    If *torsion_sampler* is given, it is used at each step of the closing to
    calculate the probability of the proposed move, which is then accepted or
    not depending on a metropolis criterium.

    :param n_stem:      Residue defining the n-stem positions every candidate
                        should match. See :meth:`~CCD.CCD()`.
    :param c_stem:      Residue defining the c-stem positions every candidate
                        should match. See :meth:`~CCD.CCD()`.
    :param torsion_sampler: A torsion sampler (used for all residues) or a list
                            of samplers (one per residue).
    :param max_iterations:  Maximum number of iterations
    :param rmsd_cutoff:     Cutoff in stem residue RMSD used to determine
                            convergence
    :param keep_non_converged: Whether to keep loop candidates for which the
                               closing did not converge
    :param random_seed:        seed for random number generator used to
                               accept/reject moves in CCD algorithm

    :type n_stem:          :class:`ost.mol.ResidueHandle`
    :type c_stem:          :class:`ost.mol.ResidueHandle`
    :type torsion_sampler: :class:`~promod3.loop.TorsionSampler` / :class:`list`
                           of :class:`~promod3.loop.TorsionSampler`
    :type max_iterations:  :class:`int`
    :type rmsd_cutoff:     :class:`float`
    :type keep_non_converged: :class:`bool`
    :type random_seed:        :class:`int`

    :return: An index for each kept candidate corresponding to the candidate
             index before this function was called. This is useful to keep track
             of scores and other data extracted before.
    :rtype:  :class:`list` of :class:`int`


  .. method:: ApplyKIC(n_stem, c_stem, pivot_one, pivot_two, pivot_three)

    Closes all loop candidates in this container (i.e. modifies the candidates
    so that its stem residues match those of *n_stem* and *c_stem*, which are
    the stem residues of the loop being modelled), using the KIC algorithm. This
    algorithm finds analytical solutions for closing the loop by modifying the
    torsion angles of just three pivot residues. Due to the underlying
    mathematical formalism in KIC, up to 16 solutions can be found for every
    candidate. This leads to an increase in number of loops.

    :param n_stem:      Residue defining the n-stem positions every candidate
                        should match
    :param c_stem:      Residue defining the c-stem positions every candidate
                        should match
    :param pivot_one:   First pivot residue
    :param pivot_two:   Second pivot residue 
    :param pivot_three: Third pivot residue 

    :type n_stem:       :class:`ost.mol.ResidueHandle`
    :type c_stem:       :class:`ost.mol.ResidueHandle`
    :type pivot_one:    :class:`int`
    :type pivot_two:    :class:`int`
    :type pivot_three:  :class:`int`

    :return: An index for each added candidate corresponding to the original
             candidate index before this function was called (each candidate can
             lead to multiple or no candidates after KIC is applied). This is
             useful to keep track of scores and other data extracted before.
    :rtype:  :class:`list` of :class:`int`

  .. method:: CalculateBackboneScores(score_container, scorer, scorer_env,\
                                      start_resnum, chain_idx=0)
              CalculateBackboneScores(score_container, scorer, scorer_env,\
                                      keys, start_resnum, chain_idx=0)
              CalculateBackboneScores(score_container, mhandle,\
                                      start_resnum, chain_idx=0)
              CalculateBackboneScores(score_container, mhandle,\
                                      keys, start_resnum, chain_idx=0)
              CalculateAllAtomScores(score_container, mhandle, \
                                     start_resnum, chain_idx=0)
              CalculateAllAtomScores(score_container, mhandle, keys, \
                                     start_resnum, chain_idx=0)

    Calculate backbone / all-atom scores for each loop candidate.
    Note that (unless otherwise noted) a lower "score" is better!

    The computed scores are in the same same order as the candidates in here.

    :param score_container: Add scores to this score container using the given
                            key names (or the ones from :class:`ScoringWeights`)
    :type score_container:  :class:`ScoreContainer`
    :param scorer: Backbone scoring object with set environment for the
                   particular loop modelling problem.
    :type scorer:  :class:`~promod3.scoring.BackboneOverallScorer`
    :param scorer_env: The scoring environment attached to *scorer*
    :type scorer_env: :class:`~promod3.scoring.BackboneScoreEnv`
    :param mhandle: Modelling handle set up scoring (see
                    :func:`SetupDefaultBackboneScoring`
                    :func:`SetupDefaultAllAtomScoring`).
    :type mhandle:  :class:`ModellingHandle`
    :param keys: Keys of the desired scorers. If not given, we use the set of
                 keys given by :meth:`ScoringWeights.GetBackboneScoringKeys` /
                 :meth:`ScoringWeights.GetAllAtomScoringKeys`.
    :type keys:  :class:`list` of :class:`str`
    :param start_resnum: Res. number defining the position in the SEQRES.
    :type start_resnum:  :class:`int`
    :param chain_idx: Index of chain the loops belong to.
    :type chain_idx:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if :func:`IsAllAtomScoringSetUp`
             is False, if *keys* has a key for which no scorer exists or if
             anything is raised when calculating the scores.


  .. method:: CalculateSequenceProfileScores(score_container, structure_db, \
                                             prof, offset=0)
              CalculateStructureProfileScores(score_container, structure_db, \
                                              prof, offset=0)

    Calculates a score comparing the given profile *prof* starting at *offset*
    with the sequence / structure profile of each candidate as extracted from
    *structure_db* (see :meth:`ost.seq.ProfileHandle.GetAverageScore` for
    details, *prof.null_model* is used for weighting).

    Note that for profile scores a higher "score" is better! So take care when
    combining this to other scores, where it is commonly the other way around.

    This requires that each candidate has a connected fragment info into the
    given *structure_db* (e.g. :meth:`FillFromDatabase` must have been called
    with this DB).

    The computed scores are in the same same order as the candidates in here.

    :param score_container: Add scores to this score container using the default
                            key name defined in :class:`ScoringWeights`
    :type score_container:  :class:`ScoreContainer`
    :param structural_db: Structural database used in :meth:`FillFromDatabase`
    :type structural_db:  :class:`~promod3.loop.StructureDB`
    :param prof: Profile information for target.
    :type prof:  :class:`ost.seq.ProfileHandle`
    :param offset: Loop starts at index *offset* in *prof*.
    :type offset:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if :meth:`HasFragmentInfos` is
             False, if *structure_db* is incompatible with the stored fragment
             infos or if *prof* has less than *offset+len* elements (len =
             length of loops stored in here).


  .. method:: CalculateStemRMSDs(score_container, n_stem, c_stem)

    Calculates RMSD between the given stems and the first and last residue of
    the loop candidates. This first superposes the first loop residue with
    *n_stem* and then computes the RMSD between the last residue's N, CA, C
    positions and the corresponding atoms in *c_stem*.

    Note that this score is only useful before calling :meth:`ApplyCCD` or
    :meth:`ApplyKIC`.

    The computed scores are in the same same order as the candidates in here.

    :param score_container: Add scores to this score container using the default
                            key name defined in :class:`ScoringWeights`
    :type score_container:  :class:`ScoreContainer`
    :param n_stem: The residue at the N-terminal end of the loop.
    :type n_stem:  :class:`ost.mol.ResidueHandle`
    :param c_stem: The residue at the C-terminal end of the loop.
    :type c_stem:  :class:`ost.mol.ResidueHandle`

    :raises: :exc:`~exceptions.RuntimeError` if stems do no contain N, CA and C
             atoms.


  .. method:: CalculateSequenceProfileScores(structure_db, prof, offset=0)
              CalculateStructureProfileScores(structure_db, prof, offset=0)
              CalculateStemRMSDs(n_stem, c_stem)

    Same as the *score_container* variant above, but here we directly return the
    score vector instead of storing it in a container.

    :return: Score for each candidate (same order as candidates in here).
    :rtype:  :class:`list` of :class:`float`


  .. method:: Add(bb_list)

    :param bb_list: The loop candidate to be added.
    :type bb_list:  :class:`~promod3.loop.BackboneList`

    :raises: :exc:`~exceptions.RuntimeError` if sequence of *bb_list* is not
             consistent with internal sequence
 
  .. method:: AddFragmentInfo(fragment)

    Adds a fragment info for the last candidate added with :meth:`Add`. Note
    that these infos are added automatically with :meth:`FillFromDatabase` and
    updated whenever the candidates are copied, removed, clustered etc.. So you
    will probably never need this function.

    :param fragment: The fragment info to add.
    :type fragment:  :class:`~promod3.loop.FragmentInfo`
 
  .. method:: Remove(index)

    Remove a loop candidate from the list of candidates.

    :param index: The index of the candidate that will be removed
    :type index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if *index* is out of bounds.
  
  .. method:: HasFragmentInfos()

    :return: True, if each loop candidate has a connected fragment info.
    :rtype:  :class:`bool`

  .. method:: GetFragmentInfo(index)

    :return: Fragment info connected to loop candidate at given *index*.
    :rtype:  :class:`~promod3.loop.FragmentInfo`
    :param index: The index of the candidate.
    :type index:  :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if :meth:`HasFragmentInfos` is
             False or if *index* is out of bounds.

  .. method:: Extract(indices)

    :return: Container with candidates with given *indices*.
    :rtype:  :class:`LoopCandidates`
    :param indices: Candidate indices to extract.
    :type indices:  :class:`list` of :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if any index is out of bounds.
  

  .. method:: GetClusters(max_dist, superposed_rmsd = False)

    Clusters the loop candidates according to their pairwise CA RMSD using an
    agglomerative hierarchical clustering algorithm. Every candidate gets 
    assigned a unique cluster. At every clustering step, the two clusters with 
    shortest distance get merged, with the distance definition being the average 
    CA RMSD between any of the members of the two clusters.

    :param max_dist: Maximal distance two clusters can have to be merged
    :type superposed_rmsd: :class:`bool`
    :param superposed_rmsd: Whether to calculate the RMSD based on a minimal
                            RMSD superposition or simply consider the current
                            positions
    :type max_dist:  :class:`float`

    :returns: Lists of loop candidate indices into this container
    :rtype:   :class:`list` of :class:`list` of :class:`int`

  .. method:: GetClusteredCandidates(max_dist, neglect_size_one=True, \
                                     superposed_rmsd=False)

    :returns: List of loop candidates clustered as in :meth:`GetClusters`.
    :rtype:   :class:`list` of :class:`LoopCandidates`

    :param max_dist: Maximal distance two clusters can have to be merged
    :type max_dist:  :class:`float`
    :param neglect_size_one: Whether clusters should be added to the returned
                             list if they only contain one loop candidate
    :type neglect_size_one:  :class:`bool`
    :param superposed_rmsd: Whether to calculate the RMSD based on a minimal
                            RMSD superposition or simply consider the current
                            positions
    :type superposed_rmsd: :class:`bool`

  .. method:: GetLargestCluster(max_dist, superposed_rmsd = False)

    Instead of performing a full clustering, the algorithm simply searches for
    the candidate with the most other candidates having a CA RMSD below
    **max_dist**. This candidate then serves as centroid for the return 
    cluster.

    :param max_dist:    Maximal CA RMSD to cluster centroid
    :type max_dist:     :class:`float`
    :param superposed_rmsd: Whether to calculate the RMSD based on a minimal
                            RMSD superposition or simply consider the current
                            positions
    :type superposed_rmsd: :class:`bool`
    
    :returns:           Largest possible cluster with all members having a
                        CA RMSD below **max_dist** to cluster centroid.
    :rtype:             :class:`LoopCandidates`


Keeping track of loop candidate scores
--------------------------------------------------------------------------------

Two helper classes are used to keep track and combine different scores computed
on loop candidates.

.. class:: ScoreContainer

  Container to keep vectors of scores (one for each loop candidate) for each
  scorer (one vector for each single scorer). Each score vector is guaranteed
  to have the same number of values.

  .. method:: IsEmpty()

    :return: True, if no loop candidates have been scored with any scorer yet.
    :rtype:  :class:`bool`

  .. method:: Contains(key)

    :return: True, if a score vector for this key was already added.
    :rtype:  :class:`bool`
    :param key: Key for desired scorer.
    :type key:  :class:`str`

  .. method:: Get(key)

    :return: Score vector for the given *key*.
    :rtype:  :class:`list` of :meth:`GetNumCandidates` :class:`float`
    :param key: Key for desired score vector.
    :type key:  :class:`str`
    :raises: :exc:`~exceptions.RuntimeError` if there are no scores for that
             *key*.

  .. method:: Set(key, scores)

    :param key: Set scores for that *key*.
    :type key:  :class:`str`
    :param scores: Score vector to set.
    :type scores:  :class:`list` of :class:`float`
    :raises: :exc:`~exceptions.RuntimeError` if this container contains other
             score vectors with a different number of entries.

  .. method:: GetNumCandidates()

    :return: Number of loop candidates that are scored here. This is the length
             of each score vector in this container.
    :rtype:  :class:`int`

  .. method:: LinearCombine(linear_weights)

    :return: Weighted, linear combination of scores.
    :rtype:  :class:`list` of :meth:`GetNumCandidates` :class:`float`

    :param linear_weights: Weights for each scorer.
    :type linear_weights:  :class:`dict` (keys: :class:`str`,
                           values: :class:`float`)

    :raises: :exc:`~exceptions.RuntimeError` if *linear_weights* has a key for
             which no scores were added.

  .. method:: Copy()

    :return: Full copy of this container.
    :rtype:  :class:`ScoreContainer`

  .. method:: Extract(indices)

    :return: Container with scores for a subset of loop candidates.
    :rtype:  :class:`ScoreContainer`

    :param indices: List of loop candidate indices to pick
                    (in [0, :meth:`GetNumCandidates`-1])
    :type indices:  :class:`list` of :class:`int`

    :raises: :exc:`~exceptions.RuntimeError` if any index is out of bounds.

  .. method:: Extend(other)

    Extend each score vector with the score vector of *other* (must have
    matching keys).

    :param other: Score container to be added to this one.
    :type other:  :class:`ScoreContainer`


.. class:: ScoringWeights

  Globally accessible set of weights to be used in scoring. This also defines
  a consistent naming of keys used for backbone and all atom scorers as set up
  by :func:`SetupDefaultBackboneScoring` and :func:`SetupDefaultAllAtomScoring`.

  Different sets of weights are available. You can get general weights 
  that have been optimized for a non redundant set of loops with lengths [3,14]
  (including stem residues). The alternative is to get weights that have only
  been optimized on a length specific subset of loops. By default you get
  different weights for following lengths: [0,1,2,3,4], [5,6], [7,8], [9,10],
  [11,12], [13,14]. 

  If you choose to modify the weights, please ensure to set consistently named
  keys in here and to use consistently named scorers and scoring computations!

  .. staticmethod:: GetWeights(with_db=False, with_aa=False,\
                               length_dependent=False, loop_length=-1)

    :return: Named weights to be used when scoring loop candidates. The default
             weights were optimized to give the best performance when choosing
             the loop candidate with the lowest combined score. Each set of
             weights includes (different) backbone scoring weights, that are
             optionally only trained on a subset of loops with corresponding 
             loop length.
    :rtype:  :class:`dict` (keys: :class:`str`, values: :class:`float`)

    :param with_db: True to choose a set of weights including DB specific scores
                    (stem RMSD and profile scores)
    :type with_db:  :class:`bool`
    :param with_aa: True to choose a set of weights including all atom scores
    :type with_aa:  :class:`bool`
    :param length_dependent: Whether to use general weights or their length 
                             length dependent counterparts.
    :type length_dependent: :class:`bool`
    :param loop_length: Length of full loop. If no weights are available for
                        this length or when *loop_length* is -1, the general
                        weights are returned.
    :type loop_length: :class:`int`

  .. staticmethod:: SetWeights(with_db, with_aa, weights,\
                               length_dependent=False, loop_length=-1)

    Overwrite a set of weights as returned by :meth:`GetWeights`.

  .. staticmethod:: GetBackboneWeights(with_db=False, with_aa=False,\
                                       length_dependent=False, loop_length=-1)

    :return: Subset of :meth:`GetWeights` for backbone scoring as in
             :meth:`scoring.BackboneOverallScorer.CalculateLinearCombination`.
    :rtype:  :class:`dict` (keys: :class:`str`, values: :class:`float`)

    :param with_db: As in :meth:`GetWeights`
    :type with_db:  :class:`bool`
    :param with_aa: As in :meth:`GetWeights`
    :type with_aa:  :class:`bool`
    :param length_dependent: As in :meth:`GetWeights`
    :type length_dependent:  :class:`bool`
    :param loop_length: As in :meth:`GetWeights`
    :type loop_length:  :class:`int`

  .. staticmethod:: GetAllAtomWeights(with_db=False, length_dependent=False,\
                                      loop_length=-1)

    :return: Subset of :meth:`GetWeights` for all atom scoring as in
             :meth:`scoring.AllAtomOverallScorer.CalculateLinearCombination`.
    :rtype:  :class:`dict` (keys: :class:`str`, values: :class:`float`)

    :param with_db: As in :meth:`GetWeights`
    :type with_db:  :class:`bool`
    :param length_dependent: As in :meth:`GetWeights`
    :type length_dependent:  :class:`bool`
    :param loop_length: As in :meth:`GetWeights`
    :type loop_length:  :class:`int`


  .. staticmethod:: GetStemRMSDsKey()
                    GetSequenceProfileScoresKey()
                    GetStructureProfileScoresKey()

    :return: Default key for stem RMSD / sequence profile / structure profile
             scores.
    :rtype:  :class:`str`

  .. staticmethod:: SetStemRMSDsKey(key)
                    SetSequenceProfileScoresKey(key)
                    SetStructureProfileScoresKey(key)

    :param key: New default key for stem RMSD / sequence profile / structure
                profile scores.
    :type key:  :class:`str`

  .. staticmethod:: GetBackboneScoringKeys()
                    GetAllAtomScoringKeys()

    :return: List of backbone / all-atom scorers to be computed for any set of
             weights defined in here.
    :rtype:  :class:`list` of :class:`str`

  .. staticmethod:: SetBackboneScoringKeys(keys)
                    SetAllAtomScoringKeys(keys)

    :param keys: New list of backbone / all-atom scorers to be computed for any
                 set of weights defined in here.
    :type keys:  :class:`list` of :class:`str`


Example: loop scoring in modelling
--------------------------------------------------------------------------------

In the example below, we show how we find and choose a loop candidate to close a
gap for a model. This shows the combined usage of :class:`ModellingHandle` to
keep a consistent modelling environment, :class:`LoopCandidates` with its
scoring routines, :class:`ScoreContainer` for keeping track of scores and
:class:`ScoringWeights` to combine scores:

.. literalinclude:: ../../../tests/doc/scripts/modelling_loop_scoring.py
