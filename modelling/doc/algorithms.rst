..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Modelling Algorithms
================================================================================

.. currentmodule:: promod3.modelling

A collection of algorithms that can be useful in modelling


Rigid Blocks
--------------------------------------------------------------------------------

RMSD is a typical measure for similarity of two structures. Given an atom atom
mapping between two structures, the minimum RMSD and the according superposition
can efficiently be calculated using an approach based on singular value 
decomposition. This approach is problematic if there are very dissimilar regions
or when domain movement events occur. We can therefore implement an iterative
superposition. The two structures undergo an initial superposition. For every
iteration we then select a subset of atoms that are within a certain distance
threshold that serve as input for the next superposition. This iterative
superpostion typically converges to the largest common subpart but is
non-deterministic since it depends on the initial superposition.
The RigidBlocks algorithm is based on only the CA positions and performs this 
iterative superposition multiple times by using a sliding window to select the 
initial subset and gathers all unique results. These results can be very 
similar and only differ by single positions. The algorithm therefore reduces
the amount of solutions by merging them based on a threshold of similarity.
The similarity is defined by the fraction of positions in solution A that are
also present in solution B. As a final result, the algorithm therefore detects 
common rigid subsets of positions.

.. method:: RigidBlocks(bb_list_one, bb_list_two, [window_length = 12, max_iterations=20, distance_thresh=3.0, cluster_thresh=0.9])

  Performs the RigidBlock algorithm on given input

  :param bb_list_one:   First piece structural information from which CA
                        positions will be extracted
  :param bb_list_two:   Second piece of structural information from which CA
                        positions will be extracted
  :param window_length: Length of sliding window to generate initial subsets
  :param max_iterations: Maximal numbers of iterations for every single
                         iterative superposition
  :param distance_thresh: Maximal distance two CA positions can have to be
                          considered in the same rigid block and to select
                          the common subset for the next iteration of the 
                          iterative superposition
  :param cluster_thresh: Threshold of similarity to perform the final merging
                         of the solutions

  :type bb_list_one:    :class:`promod3.loop.BackboneList`
  :type bb_list_two:    :class:`promod3.loop.BackboneList`
  :type window_length:  :class:`int`
  :type max_iterations: :class:`int`
  :type distance_thresh: :class:`float`
  :type cluster_thresh: :class:`float`

  :returns:             :class:`tuple` with the first element being a 
                        :class:`list` of :class:`list` defining the
                        indices of the common subsets (rigid blocks) relative
                        to the input :class:`promod3.loop.BackboneList` objects 
                        and the second element being a :class:`list` of 
                        :class:`ost.geom.Mat4` defining the transformations to 
                        superpose the according positions in **bb_list_one** 
                        onto **bb_list_two**


.. method:: RigidBlocks(aln, [seq_one_idx=0, seq_two_idx=1,window_length = 12, max_iterations=20, distance_thresh=3.0, cluster_thresh=0.9])

  Performs the RigidBlock algorithm on given input

  :param aln:           An alignment with attached :class:`ost.mol.EntityView`
                        objects from which the positions are extracted
  :param seq_idx_one:   The idx of the first sequence from which the CA 
                        positions will be extracted
  :param seq_idx_two:   The idx of the second sequence from which the CA
                        positions will be extracted
  :param window_length: Length of sliding window to generate initial subsets
  :param max_iterations: Maximal numbers of iterations for every single
                         iterative superposition
  :param distance_thresh: Maximal distance two CA positions can have to be
                          considered in the same rigid block and to select
                          the common subset for the next iteration of the 
                          iterative superposition
  :param cluster_thresh: Threshold of similarity to perform the final merging
                         of the solutions

  :type aln:            :class:`ost.seq.AlignmentHandle`
  :type seq_idx_one:    :class:`int`
  :type seq_idx_two:    :class:`int`
  :type window_length:  :class:`int`
  :type max_iterations: :class:`int`
  :type distance_thresh: :class:`float`
  :type cluster_thresh: :class:`float`

  :returns:             :class:`tuple` with the first element being a 
                        :class:`list` of :class:`list` defining the
                        column indices of the common subsets (rigid blocks) 
                        relative to the input :class:`ost.seq.AlignmentHandle` 
                        and the second element being a :class:`list` of 
                        :class:`ost.geom.Mat4` defining the transformations to 
                        superpose the according positions from the first
                        sequence onto the second sequence.

.. method:: RigidBlocks(pos_one, pos_two, [window_length = 12, max_iterations=20, distance_thresh=3.0, cluster_thresh=0.9])

  Performs the RigidBlock algorithm on given input

  :param pos_one:       First piece position information 
  :param pos_two:       Second piece of position information
  :param window_length: Length of sliding window to generate initial subsets
  :param max_iterations: Maximal numbers of iterations for every single
                         iterative superposition
  :param distance_thresh: Maximal distance two CA positions can have to be
                          considered in the same rigid block and to select
                          the common subset for the next iteration of the 
                          iterative superposition
  :param cluster_thresh: Threshold of similarity to perform the final merging
                         of the solutions

  :type pos_one:        :class:`ost.geom.Vec3List`
  :type pos_two:        :class:`ost.geom.Vec3List`
  :type window_length:  :class:`int`
  :type max_iterations: :class:`int`
  :type distance_thresh: :class:`float`
  :type cluster_thresh: :class:`float`

  :returns:             :class:`tuple` with the first element being a 
                        :class:`list` of :class:`list` defining the
                        indices of the common subsets (rigid blocks) relative
                        to the input :class:`ost.geom.Vec3List` objects 
                        and the second element being a :class:`list` of 
                        :class:`ost.geom.Mat4` defining the transformations to 
                        superpose the according positions in **pos_one** 
                        onto **pos_two**


De Novo Modelling
--------------------------------------------------------------------------------

|project| provides algorithms for sampling and fragment detection. 
Here we provide an object, that facilitates fragment detection and caching,
as well as a convenient function to combine the functionalities into an
example pipeline.

.. autoclass:: FraggerHandle
  :members:


.. autofunction:: GenerateDeNovoTrajectories


Motif Finder
--------------------------------------------------------------------------------

Distinct spatial arrangements of atoms or functional groups are key for protein 
function. For their detection, |project| implements the MotifFinder algorithm 
which is based on geometric hashing as described by Nussinov and Wolfson 
[nussinov1991]_. The algorithm consists of a learning stage, a detection stage 
and a refinement stage.

Learning Stage: A motif (query) is represented by a set of coordinates. Triplets
(p1, p2, p3) of coordinates are selected that define triangles. For each 
triangle one can define an orthogonal vector basis 
(in our case v1 = norm(p2-p1), v3 = norm(cross(v1,p3-p1), 
v2 = norm(cross(v1,v3)))). For each coordinate not in [p1,p2,p3], we add the 
identity of the query/triangle as value to a hash map. 
The corresponding key consists of discretized values describing the edge lengths 
of the triangle, as well as the coordinate transformed into the triangle 
specific orthogonal vector basis. That's 6 numbers in total.

Detection Stage: The goal is to identify one or several subsets of target 
coordinates that resemble an input query. 
We first setup an accumulator containing a counter for each triangle observed 
in the input query. We then iterate over each possible triangle with vertices 
p1, p2 and p3 in the target coordinates. At the beginning of each iteration, 
all counters in the accumulator are set to zero. Again, we build a vector basis 
given that triangle and transform all coordinates not in [p1,p2,p3] into that 
vector space. For each transformed coordinate we obtain a key for the query hash 
map. If there is one or several values at that location in the hash map, 
we increment the corresponding locations in the accumulator. 
Once all coordinates are processed, we search for high counts in the 
accumulator. Given *N* query coordinates, we keep a solution for further 
refinement if count/(*N*-3) >= *hash_tresh*. This is repeated until all 
triangles in the target are processed. One key problem with this approach is 
the discretization of floating point numbers that give raise to the hash map 
keys. Two extremely close values might end up in different bins just because
they are close to the bin boundaries. For each of the 6 relevant numbers
we estimate the actual bin as well as the closest neighbouring bin. Processing
all possible combinations results in 64 hash map lookups instead of only one.

Refinement Stage: Every potential solution identified in the detection stage is
further refined based on the *distance_thresh* and *refine_thresh* parameters.
A potential solution found in the detection stage is a pair of triangles, one
in the query and one in the target, for which we find many matching coordinates 
in their respective vector space. We start with a coordinate mapping based on 
the triangle vertices from the query and the target (3 pairs). 
This coordinate mapping is iteratively updated by estimating the minimum RMSD 
superposition of the mapped query coordinates onto the target, apply that 
superposition on the query, find the closest target coordinate for each 
coordinate in the query and redo the mapping by including all pairs with 
minimum distance < *distance_thresh*. Iteration stops if nothing changes 
anymore. The solution is returned to the user if the final fraction of mapped 
query coordinates is larger or equal *refine_thresh*.
The larger the mapping, the more accurate the superposition. As we start with 
only the three triangle vertices, *distance_thresh* is doubled for the initial 
iteration.

.. literalinclude:: ../../../tests/doc/scripts/modelling_motif_finder.py

.. class:: MotifQuery(positions, identifier, min_triangle_edge_length, \
                      max_triangle_edge_length, bin_size)
           MotifQuery(positions, identifier, min_triangle_edge_length, \
                      max_triangle_edge_length, bin_size, flags)
           MotifQuery(query_list)

  A single query or a container of queries.
  The constructor performs the learning stage of a single query or combines
  several queries, so they can be searched at once. 

  :param positions:     Coordinates of the query
  :param identifier:    Descriptor of the query
  :param min_triangle_edge_length: To avoid the full O(n^3) hell, triangles
                        with any edge length below *min_triangle_edge_length*
                        are skipped
  :param max_triangle_edge_length: Same as *min_triangle_edge_length* but 
                        upper bound
  :param bin_size:      Bin size in A, relevant to generate hash map keys
  :param flags:         Flag in range [0,63] for every coordinate in *positions*.
                        They're also added to the hash map keys (default: 0). 
                        This means that additionally to having a matching 
                        relative position, the coordinates must also have a 
                        matching flag in the detection/refinement stage. 
                        If not provided (in the query and in the search), 
                        only coordinates matter.
  :param query_list:    E pluribus unum

  :type positions:      :class:`ost.geom.Vec3List`
  :type identifier:     :class:`str`
  :type min_triangle_edge_length: :class:`float`
  :type max_triangle_edge_length: :class:`float`
  :type bin_size:       :class:`float`
  :type flags:          :class:`list` of :class:`int`
  :type query_list:     :class:`list` of :class:`MotifQuery`


  .. method:: Save(filename)

    Saves the query down to disk 

    :param filename:    filename
    :type filename:     :class:`str`

  .. staticmethod:: Load(filename)

    Load query from disk

    :param filename:    filename
    :type filename:     :class:`str`

  .. method:: GetPositions(query_idx)

    Returns coordinates of specified query

    :param query_idx:   Query from which you want the positions
    :type query_idx:    :class:`int`

  .. method:: GetIdentifiers()

    Returns a list of all query identifiers.

  .. method:: GetN()

    Returns the number of queries



.. class:: MotifMatch

  Object that holds information about a match found in :meth:`FindMotifs`

  .. attribute:: query_idx 

    Index of matching query

  .. attribute:: mat 

    Transformation matrix to superpose matching query onto target

  .. attribute:: alignment 

    List of tuples which define matching pairs of query/target coordinates


.. method:: FindMotifs(query, target_positions, hash_tresh=0.4, \
                       distance_thresh=1.0, refine_thresh=0.7, \
                       flags=list(), swap_thresh=False)

  Performs the detection and refinement stages of the geometric hashing 
  algorithm. 

  :param query:         Query to be searched
  :param target_positions: Coordinates of the target
  :param hash_thresh:   Parameter relevant for detection stage
  :param distance_thresh: Parameter relevant for refinement stage
  :param refine_thresh: Parameter relevant for refinement stage
  :param flags:         Equivalent to *flags* in :class:`MotifQuery`
                        constructor. If you didn't provide anything there,
                        this can be ignored. Only the actual coordinates
                        matter in this case.
  :param swap_thresh: *hash_thresh* and *refine_thresh* refer to fraction of
                      covered positions in *query*. When setting this to
                      True, they refer to the fraction of covered positions
                      in *target_positions*.

  :returns:             All found matches

  :rtype:               :class:`list` of :class:`MotifMatch`


AFDB Modelling
--------------------------------------------------------------------------------

Template based modelling using AFDB as template library comes with two
challenges for which |project| provides solutions:

* efficient structure storage for the whole AFDB: :class:`FSStructureServer`
* fast sequence searches with limited sensitivity: :class:`PentaMatch`

The creation of these two object requires extensive preprocessing. The required
scripts and documentation are available in
`<GIT_ROOT>/extras/data_generation/afdb_modelling`.

Basic modelling functionality is available in the following two functions:

* :func:`AFDBTPLSearch`
* :func:`AFDBModel`

.. autofunction:: AFDBTPLSearch

.. autofunction:: AFDBModel

.. autoclass:: FSStructureServer
  :members:
  :member-order: bysource

.. autoclass:: PentaMatch
  :members:
  :member-order: bysource
