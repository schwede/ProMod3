..  Copyright (c) 2013-2020, SIB - Swiss Institute of Bioinformatics and
..                           Biozentrum - University of Basel
..  
..  Licensed under the Apache License, Version 2.0 (the "License");
..  you may not use this file except in compliance with the License.
..  You may obtain a copy of the License at
..  
..    http://www.apache.org/licenses/LICENSE-2.0
..  
..  Unless required by applicable law or agreed to in writing, software
..  distributed under the License is distributed on an "AS IS" BASIS,
..  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
..  See the License for the specific language governing permissions and
..  limitations under the License.


Generating Loops De Novo
================================================================================

.. currentmodule:: promod3.modelling

The Monte Carlo capabilities of |project| are mainly targeted at generating de
novo structure candidates for loops or N-/C-Termini. Every iteration of the
sampling process consists basically of four steps and we define objects for
each step:

* :ref:`mc-sampler-object`: Propose new conformation
* :ref:`mc-closer-object`: Adapt new conformation to the environment
* :ref:`mc-scorer-object`: Score the new conformation
* :ref:`mc-cooler-object`: Accept/Reject new conformation based on the score and
  a temperature controlled Metropolis criterion

These steps can be arbitrarily combined to generate custom Monte Carlo sampling
pipelines. This combination either happens manually or by using the convenient
:func:`SampleMonteCarlo` function. For example, here we show how to apply Monte
Carlo sampling to the N-terminal part of crambin:

.. literalinclude:: ../../../tests/doc/scripts/modelling_monte_carlo.py

.. autofunction:: SampleMonteCarlo

.. _mc-sampler-object:

Sampler Object
--------------------------------------------------------------------------------

The sampler objects can be used to generate initial conformations and
propose new conformations for a sequence of interest. They build the basis
for any Monte Carlo sampling pipeline. You can either use one of the 
provided samplers or any object that implements the functionality of
:class:`SamplerBase`.


.. class:: SamplerBase

  Abstract base class defining the functions that must be implemented by any 
  sampler.

  .. method:: Initialize(bb_list)

    Supposed to initialize structural information from scratch. The sequence
    of the generated :class:`promod3.loop.BackboneList` is taken from *bb_list*.

    :param bb_list:     Passed by reference, so the resulting 
                        :class:`promod3.loop.BackboneList` is assigned to this 
                        parameter. Sequence / length stay the same.

    :type bb_list:      :class:`promod3.loop.BackboneList`

    :returns:           None

  .. method:: ProposeStep(actual_positions, proposed_positions)

    Takes current positions and proposes a new conformation. There is no 
    guarantee on maintining any special RT state. The :ref:`mc-closer-object`
    is supposed to sort that out. 

    :param actual_positions: Starting point, must not change when calling this
                             function.
    :param proposed_positions: Passed by reference, so the resulting 
                               :class:`promod3.loop.BackboneList` is assigned to 
                               this parameter.

    :type actual_positions: :class:`promod3.loop.BackboneList`
    :type proposed_positions: :class:`promod3.loop.BackboneList`

    :returns:           None


.. class:: PhiPsiSampler(sequence, torsion_sampler, n_stem_phi=-1.0472,\
                         c_stem_psi=-0.78540, prev_aa='A', next_aa='A', seed=0)

  The PhiPsiSampler randomly draws and sets phi/psi dihedral angles from
  a distribution provided by the *torsion_sampler*.

  :param sequence:      Sequence that should be sampled
  :param torsion_sampler: Sampler, from which the phi/psi pairs are drawn. It
                          is also possible to pass a list of samplers with same
                          size as the sequence to assign a specific sampler per
                          residue.
  :param n_stem_phi:    Phi angle of the n_stem. This angle is not defined in
                        the sampling region. If the first residue gets selected
                        for changing the dihedral angles, it draws a psi angle
                        given *n_stem_phi*.   
  :param c_stem_psi:    Psi angle of c_stem. This angle is not defined in
                        the sampling region. If the last residue gets selected
                        for changing the dihedral angles, it draws a phi angle
                        given *c_stem_psi*.
  :param prev_aa:       This parameter is necessary to extract the according
                        histogram index for the first residue from the 
                        *torsion_sampler*. (Remember: The torsion sampler
                        always considers triplets)   
  :param next_aa:       This parameter is necessary to extract the according
                        histogram index for the last residue from the 
                        *torsion_sampler*. (Remember: The torsion sampler
                        always considers triplets)   
  :param seed:          Seed for the internal random number generators.

  :type sequence:       :class:`str`
  :type torsion_sampler: :class:`~promod3.loop.TorsionSampler`
  :type n_stem_phi:     :class:`float`
  :type c_stem_psi:     :class:`float`
  :type prev_aa:        :class:`str`
  :type next_aa:        :class:`str`
  :type seed:           :class:`int`



  .. method:: Initialize(bb_list)

    Sets up a new :class:`~promod3.loop.BackboneList` by randomly drawing
    phi/psi dihedral angles.

    :param bb_list:     The newly created conformation gets stored in here
    :type bb_list:      :class:`~promod3.loop.BackboneList`

  .. method:: ProposeStep(actual_positions, proposed_position)

    Randomly selects one of the residues and resets its phi/psi values
    according to a random draw from the internal torsion samplers.
    In case of the first residue, only a psi given phi is drawn. Same
    principle also applies for the last residue.

    :param actual_positions: Conformation to be changed
    :param proposed_positions: Changed conformation gets stored in here  

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type proposed_positions: :class:`~promod3.loop.BackboneList`

    :raises:  :exc:`~exceptions.RuntimeError` If size of *actual_positions*
              is not consistent with the internal sequence. Note, that the
              sequence itself doesn't get checked for efficiency reasons.



.. class:: SoftSampler(sequence, torsion_sampler, max_dev, n_stem_phi=-1.0472,\
                       c_stem_psi=-0.78540, prev_aa='A', next_aa='A', seed=0)

  Instead of drawing completely new values for a residues phi/psi angles,
  only one angle gets altered by a maximum value of *max_dev* in the 
  SoftSampler.

  :param sequence:      Sequence that should be sampled
  :param torsion_sampler: Sampler, from which the phi/psi probablities are 
                          extracted. It is also possible to pass a list of 
                          samplers with same size as the sequence to assign 
                          a specific sampler per residue.
  :param max_dev:       Maximal deviation of dihedral angle from its original 
                        value per sampling step.
  :param n_stem_phi:    Phi angle of the n_stem. This angle is not defined in
                        the sampling region. If the psi angle of the first
                        residue gets selected to be changed, *n_stem_phi* is
                        used to calculate the phi/psi probability to estimate
                        the acceptance probability.  
  :param c_stem_psi:    Psi angle of c_stem. This angle is not defined in
                        the sampling region. If the phi angle of the last 
                        residue gets selected to be changed, *c_stem_psi* is
                        used to calculate the phi/psi probability to estimate
                        the acceptance probability.
  :param prev_aa:       This parameter is necessary to extract the according
                        histogram index for the first residue from the 
                        *torsion_sampler*. (Remember: The torsion sampler
                        always considers triplets)   
  :param next_aa:       This parameter is necessary to extract the according
                        histogram index for the last residue from the 
                        *torsion_sampler*. (Remember: The torsion sampler
                        always considers triplets)   
  :param seed:          Seed for the internal random number generators.

  :type sequence:       :class:`str`
  :type torsion_sampler: :class:`~promod3.loop.TorsionSampler`
  :type n_stem_phi:     :class:`float`
  :type c_stem_psi:     :class:`float`
  :type prev_aa:        :class:`str`
  :type next_aa:        :class:`str`
  :type seed:           :class:`int`



  .. method:: Initialize(bb_list)

    Sets up a new :class:`~promod3.loop.BackboneList` by randomly drawing
    phi/psi dihedral angles.

    :param bb_list:     The newly created conformation gets stored in here
    :type bb_list:      :class:`~promod3.loop.BackboneList`

  .. method:: ProposeStep(actual_positions, proposed_position)

    In an iterative process, the SoftSampler randomly selects one of the 
    possible dihedral angles in a conformation and changes it by a random value 
    in [-*max_dev*, *max_dev*]. The acceptance probability of this change is
    the fraction of the phi/psi probability before and after changing the 
    single angle in the particular residue. There is a maximum of 100 
    iterations. It is therefore theoretically possible, that nothing happens 
    when a new step should be proposed

    :param actual_positions: Conformation to be changed
    :param proposed_positions: Changed conformation gets stored in here  

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type proposed_positions: :class:`~promod3.loop.BackboneList`

    :raises:  :exc:`~exceptions.RuntimeError` If size of *actual_positions*
              is not consistent with the internal sequence. Note, that the
              sequence itself doesn't get checked for efficiency reasons.


.. class:: FragmentSampler(sequence, fraggers,\
                           init_bb_list=BackboneList(sequence), \
                           sampling_start_index=0, init_fragments=3, seed=0)

  The FragmentSampler samples by replacing full fragments originating from a
  list of :class:`~promod3.loop.Fragger` objects. The region, that actually gets
  sampled is determined by *sampling_start_index* and number of
  :class:`~promod3.loop.Fragger` objects  being available. All parts not covered
  by any fragger remain rigid.

  :param sequence: Overall sequence
  :param fraggers: A list of :class:`~promod3.loop.Fragger` objects. The first
                   fragger covers the region starting at the letter
                   *sampling_start_index* of the *sequence* and so on.
                   All fraggers must contain fragments of equal size.
  :param init_bb_list: Initial conformation, that serves as a starting point for
                       sampling. The default gets constructed using the default
                       constructor of :class:`~promod3.loop.BackboneList` and
                       results in a helix.
  :param sampling_start_index: Defines the beginning of the region, that actually
                               gets sampled.
  :param init_fragments: When calling the Initialize function, the positions get set
                         to the ones of *init_bb_list*. This is the number of 
                         fragments that gets randomly selected and inserted.
  :param seed:          Seed for the internal random number generators

  :type sequence:       :class:`str`
  :type fraggers:       :class:`list`
  :type init_bb_list:   :class:`~promod3.loop.BackboneList`
  :type sampling_start_index: :class:`int`
  :type init_fragments: :class:`int`
  :type seed:           :class:`int`


  .. method:: Initialize(bb_list)

    Sets up a new :class:`~promod3.loop.BackboneList` by setting the setting 
    bb_list = *init_bb_list* and randomly replace n fragments
    with n = *init_fragments*

    :param bb_list:     The newly created conformation gets stored in here
    :type bb_list:      :class:`~promod3.loop.BackboneList`


  .. method:: ProposeStep(actual_step, proposed_position)

    Randomly selects a position and selects a random fragment from the according
    fragger object to alter the conformation.

    :param actual_positions: Conformation to be changed
    :param proposed_positions: Changed conformation gets stored in here  

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type proposed_positions: :class:`~promod3.loop.BackboneList`   



.. _mc-closer-object:

Closer Object
--------------------------------------------------------------------------------

After the proposal of new conformations by the sampler objects, the
conformations typically have to undergo some structural changes, so they
fit to a given environment. This can either be structural changes, that
the stems of the sampled conformation overlap with given stem residues or
or simple stem superposition in case of terminal sampling. You can either
use any of the provided closers or any object that implements the 
functionality of :class:`CloserBase`.

.. class:: CloserBase

  Abstract base class defining the functions that must be implemented by any 
  closer.

  .. method:: Close(actual_positions, closed_positions)

    Takes current positions and proposes a new conformation that fits to a
    given environment.

    :param actual_positions: Starting point, must not change when calling this
                             function.
    :param closed_positions: Passed by reference, so the resulting 
                             :class:`promod3.loop.BackboneList` is assigned to 
                             this parameter.

    :type actual_positions: :class:`promod3.loop.BackboneList`
    :type closed_positions: :class:`promod3.loop.BackboneList`

    :returns:           Whether closing procedure was successful
    :rtype:             :class:`bool`


.. class:: CCDCloser(n_stem, c_stem, sequence, torsion_sampler, seed)

  The CCDCloser applies the CCD algorithm to the sampled conformation 
  to enforce the match between the conformations stem residue and
  the stems given by the closer. The *torsion_sampler* is used to
  avoid moving into unfavourable phi/psi ranges.

  :param n_stem:        Defining stem positions the closed conformation
                        should adapt. See :meth:`~CCD.CCD()`.

  :param c_stem:        Defining stem positions the closed conformation
                        should adapt. See :meth:`~CCD.CCD()`.

  :param sequence:      Sequence of the conformation to be closed.
  :param torsion_sampler: To enforce valid phi/psi ranges. Alternatively, you
                          can also pass a list of sampler objects to assign a
                          unique torsion sampler to every residue of the
                          conformation to be closed.
  :param seed:          Seed for internal random generators.

  :type n_stem:         :class:`ost.mol.ResidueHandle`
  :type c_stem:         :class:`ost.mol.ResidueHandle`
  :type sequence:       :class:`str`
  :type torsion_sampler: :class:`~promod3.loop.TorsionSampler` / :class:`list`
                         of :class:`~promod3.loop.TorsionSampler`
  :type seed:           :class:`int`

  .. method:: Close(actual_positions, closed_positions)
    
    :param actual_positions: Conformation to be closed.
    :param closed_positions: Closed conformation gets stored in here.

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type closed_positions: :class:`~promod3.loop.BackboneList`

    :returns: Whether CCD converged


.. class:: DirtyCCDCloser(n_stem, c_stem)

  The DirtyCCDCloser applies the CCD algorithm to the sampled conformation 
  to enforce the match between the conformations stem residue and
  the stems given by the closer. There is no check for reasonable backbone
  dihedral angles as it is the case for the :class:`CCDCloser`.

  :param n_stem:        Defining stem positions the closed conformation
                        should adapt.

  :param c_stem:        Defining stem positions the closed conformation
                        should adapt.

  :type n_stem:         :class:`ost.mol.ResidueHandle`
  :type c_stem:         :class:`ost.mol.ResidueHandle`

  .. method:: Close(actual_positions, closed_positions)
    
    :param actual_positions: Conformation to be closed.
    :param closed_positions: Closed conformation gets stored in here.

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type closed_positions: :class:`~promod3.loop.BackboneList`

    :returns: Whether CCD converged


.. class:: KICCloser(n_stem, c_stem, seed)

  The KIC closer randomly picks three pivot residues in the conformation
  to be closed and applies the KIC algorithm. KIC gives up to 16 possible
  solutions. The KICCloser simply picks the first one.

  :param n_stem:        Defining stem positions the closed conformation should
                        adapt.
  :param c_stem:        Defining stem positions the closed conformation should
                        adapt.
  :param seed:          Seed for internal random generators.

  :type n_stem:         :class:`ost.mol.ResidueHandle`
  :type c_stem:         :class:`ost.mol.ResidueHandle`
  :type seed:           :class:`int`

  .. method:: Close(actual_positions, closed_positions)
    
    :param actual_positions: Conformation to be closed.
    :param closed_positions: Closed conformation gets stored in here.

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type closed_positions: :class:`~promod3.loop.BackboneList`

    :returns: Whether KIC found a solution


.. class:: NTerminalCloser(c_stem)

  The :class:`NTerminalCloser` simply takes the conformation and closes by 
  superposing the c_stem with the desired positions.

  :param c_stem:        Defining stem positions the closed conformation should
                        adapt.
  :type c_stem:         :class:`ost.mol.ResidueHandle`


  .. method:: Close(actual_positions, closed_positions)

    :param actual_positions: Conformation to be closed (or in this case
                             transformed in space).
    :param closed_positions: Closed (transformed) conformation gets stored in 
                             here.

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type closed_positions: :class:`~promod3.loop.BackboneList`

    :returns:           Whether closing was successful


.. class:: CTerminalCloser(n_stem)

  The :class:`CTerminalCloser` simply takes the conformation and closes by 
  superposing the n_stem with the desired positions.

  :param n_stem:        Defining stem positions the closed conformation should
                        adapt.
  :type n_stem:         :class:`ost.mol.ResidueHandle`

  .. method:: Close(actual_positions,closed_positions)

    :param actual_positions: Conformation to be closed (or in this case
                             transformed in space).
    :param closed_positions: Closed (transformed) conformation gets stored in 
                             here.

    :type actual_positions: :class:`~promod3.loop.BackboneList`
    :type closed_positions: :class:`~promod3.loop.BackboneList`

    :returns:           Whether closing was successful


.. class:: DeNovoCloser

  In case of sampling a full stretch, you dont have external constraints. The 
  closer has a rather boring job in this case.

  .. method:: Close(actual_positions,closed_positions)

    Does absolutely nothing, except copying over the coordinates from
    *actual_positions* to *closed_positions* and return true.



        
.. _mc-scorer-object:

Scorer Object
--------------------------------------------------------------------------------

The scorer asses a proposed conformation and are intended to return a pseudo
energy, the lower the better. You can either use the provided scorer or any
object implementing the functionality defined in :class:`ScorerBase`. 


.. class:: ScorerBase

  Abstract base class defining the functions that must be implemented by any 
  scorer.

  .. method:: GetScore(bb_list)

    Takes coordinates and spits out a score given some internal structural 
    environment.

    :param bb_list:     Coordinates to be scored     
    :type bb_list: :class:`promod3.loop.BackboneList`

    :returns:           The score
    :rtype:             :class:`float`

.. class:: LinearScorer(scorer, scorer_env, start_resnum, num_residues,\
                        chain_idx, linear_weights)

  The LinearScorer allows to combine the scores available from
  :class:`~promod3.scoring.BackboneOverallScorer` in a linear manner. See
  :meth:`~promod3.scoring.BackboneOverallScorer.CalculateLinearCombination` for a
  detailed description of the arguments.

  .. warning:: The provided *scorer_env* will be altered in every 
               :func:`GetScore` call.
               You might consider the Stash / Pop mechanism of the 
               :class:`~promod3.scoring.BackboneScoreEnv` to restore to the 
               original state once the sampling is done.

  :param scorer: Scorer Object with set environment for the particular loop
                 modelling problem.
  :type scorer:  :class:`~promod3.scoring.BackboneOverallScorer`
  :param scorer_env: The environment that is linked to the *scorer*
  :type scorer_env: :class:`~promod3.scoring.BackboneScoreEnv`
  :param start_resnum: Res. number defining the position in the SEQRES.
  :type start_resnum:  :class:`int`
  :param num_residues: Number of residues to score
  :type num_residues: :class:`int`
  :param chain_idx: Index of chain the loop(s) belong to.
  :type chain_idx:  :class:`int`
  :param linear_weights: Weights for each desired scorer.
  :type linear_weights:  :class:`dict` (keys: :class:`str`,
                         values: :class:`float`)

  :raises: :exc:`~exceptions.RuntimeError` if *linear_weights* has a *key* for
           which no scorer exists

  .. method:: GetScore(bb_list)

    :param bb_list: Loop to be scored.
    :type bb_list:  :class:`~promod3.loop.BackboneList`

    :return: A linear combination of the scores
    :rtype:  :class:`float`


.. _mc-cooler-object:

Cooler Object
--------------------------------------------------------------------------------

The cooler objects control the temperature of the Monte Carlo trajectory.
They're intended to deliver steadily decreasing temperatures with calls
to their GetTemperature function. You can either use the provided cooler
or any object implementing the functionality defined in 
:class:`CoolerBase`.

.. class:: CoolerBase

  Abstract base class defining the functions that must be implemented by any 
  cooler.

  .. method:: GetTemperature()

    :returns:           The Temperature
    :rtype:             :class:`float`

  .. method:: Reset()

    Resets to original state, so a new Monte Carlo trajectory can be generated


.. class:: ExponentialCooler(change_frequency, start_temperature, cooling_factor)

  The exponential cooler starts with a given *start_temperature* and counts the
  calls to its :meth:`GetTemperature` function. According to the 
  *change_frequency*, the returned temperature gets multiplied by the 
  *cooling_factor*.

  :param change_frequency: Frequency to change temperature
  :param start_temperature: temperature to start with
  :param cooling_factor:    Factor to decrease temperature

  :type change_frequency: :class:`int`
  :type start_temperature: :class:`float`
  :type cooling_factor: :class:`float`

  .. method:: GetTemperature()

    :returns:           current temperature

  .. method:: Reset()

    Sets current temperature back to *start_temperature* and the 
    internal counter to 0
